/*
	reporte enviado por Juan Buonaffina, formateado.
*/
select distinct 
	loc.emp_codigo, 
	loc.CODIGO, 
	loc.LOC_COD_EMPRESA, 
	tabla.CAJ_CODIGO as "Caja", 
	tabla.config, 
	loc.DESCRIPCION as "Nombre Fantasia", 
	raz.DESCRIPCION as "Razon_Social", 
	loc.DIRECCION as "Direccion", 
	loc.TELEFONO as "Telefono", 
	dep.DESCRIPCION as "Departamento", 
	rccc.SALDO,rccc.SOBREGIRO, 
	case when paySafeCard.VALOR_ACTUAL is null 
		then 'No' 
		else 'Si' 
	end as "PaySafeCard", 
	case when recAntel.VALOR_ACTUAL is null 
		then 'No' 
		else 'Si' 
	end as "RecAntel", 
	case when crediCajas.TERMINAL like 'BR%' 
		then 'Si' 
		else 'No' 
	end as "BrouATM" 
from 
	distribuciones_pendientes tabla 
	join locales loc 
		on tabla.loc_codigo = loc.CODIGO
	full join LOCALIDADES loca 
		on loc.LOCA_CODIGO = loca.CODIGO
	full join DEPARTAMENTOS dep 
		on loca.DEPA_CODIGO = dep.CODIGO
	full join RAZONES_SOCIALES raz 
		on loc.RAZ_SOC_CODIGO = raz.CODIGO
	full join RC_CUENTAS_CORRIENTES_LOCALES rcCuentasLoc 
		on rcCuentasLoc.EMP_CODIGO = loc.emp_codigo 
		and rcCuentasLoc.LOC_CODIGO =  loc.CODIGO 
		and rcCuentasLoc."COMPAÑIA" = 'ANC'
	full join RC_CUENTAS_CORRIENTES rccc 
		on rcCuentasLoc.CUE_COR_CODIGO = rccc.CODIGO
	full join SV_SERVICIOS_AUDIT paySafeCard 
		on paySafeCard.LOC_CODIGO = loc.CODIGO 
		and paySafeCard.CAJ_CODIGO = tabla.CAJ_CODIGO 
		and paySafeCard.SV_CODIGO = 6 
		and paySafeCard.CAMPO = '9005/habilitado'
	full join SV_SERVICIOS_AUDIT recAntel 
		on recAntel.LOC_CODIGO = loc.CODIGO 
		and recAntel.CAJ_CODIGO = tabla.CAJ_CODIGO 
		and recAntel.SV_CODIGO = 17 
		and recAntel.CAMPO = '9029/habilitado'
	full join CREDITOS_CONF_CAJAS crediCajas 
		on crediCajas.LOC_CODIGO = loc.CODIGO 
		and crediCajas.CAJ_CODIGO = tabla.CAJ_CODIGO 
		and crediCajas.CRE_CODIGO = 638
where 	tabla.FECHA_ULT_CONSULTA > sysdate - 7 
and 	tabla.EMP_CODIGO not in (5000, 5001, 5002, 5003, 5004, 5005, 1) 
order by 
	Saldo desc, 
	1, 
	3, 
	4
