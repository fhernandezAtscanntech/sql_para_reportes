multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
		select 
			emp_codigo 	"Cod.Empresa", 
			loc_codigo 	"Cod.Local", 
			empresa 	"Empresa", 
			razon_social "Razon Social", 
			local 	"Local", 
			caja 	"Caja", 
			sum(mov_total) 	"Cant.Tickets", 
			sum(ccc) 		"Cant.Credito propio (viejo)", 
			sum(afinidad) 	"Afinidad (viejo)", 
			sum(credito) 	"Cant. x switch", 
			sum (comision) 	"Comisión", 
			sum (arancel) 	"Arancel", 
			sum(multimedia)/count(multimedia)  "Multimedia"
		from 
			iposs.fredy1 
		where fecha between 
			to_date('''||'11'||'''||to_char(add_months(sysdate, -1), '''||'mm/yyyy'||'''), '''||'dd/mm/yyyy'||''')
			and 
			to_date('''||'10'||'''||to_char(sysdate, '''||'mm/yyyy'||'''), '''||'dd/mm/yyyy'||''')
		group by 
			emp_codigo, 
			loc_codigo, 
			empresa, 
			razon_social, 
			local, 
			caja, 
			multimedia
			' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
	select 
		emp_codigo 	"Cod.Empresa", 
		loc_codigo 	"Cod.Local", 
		empresa 	"Empresa", 
		razon_social "Razon Social", 
		local 	"Local", 
		caja 	"Caja", 
		sum(mov_total) 	"Cant.Tickets", 
		sum(ccc) 		"Cant.Credito propio (viejo)", 
		sum(afinidad) 	"Afinidad (viejo)", 
		sum(credito) 	"Cant. x switch", 
		sum (comision) 	"Comisión", 
		sum (arancel) 	"Arancel", 
		sum(multimedia)/count(multimedia)  "Multimedia"
	from 
		iposs.fredy1 
	where 1 = 2
	group by 
	emp_codigo, 
		loc_codigo, 
		empresa, 
		razon_social, 
		local, 
		caja, 
		multimedia 
)

######################
consulta
######################
select 1 from #reco#


/*

select 
	emp_codigo 	"Cod.Empresa", 
	loc_codigo 	"Cod.Local", 
	empresa 	"Empresa", 
	razon_social "Razon Social", 
	local 	"Local", 
	caja 	"Caja", 
	sum(mov_total) 	"Cant.Tickets", 
	sum(ccc) 		"Cant.Credito propio (viejo)", 
	sum(afinidad) 	"Afinidad (viejo)", 
	sum(credito) 	"Cant. x switch", 
	sum (comision) 	"Comisión", 
	sum (arancel) 	"Arancel", 
	sum(multimedia)/count(multimedia)  "Multimedia"
from 
	iposs.fredy1 
where fecha between 
	to_date('11'||to_char(add_months(sysdate, -1), 'mm/yyyy'), 'dd/mm/yyyy')
	and 
	to_date('10'||to_char(sysdate, 'mm/yyyy'), 'dd/mm/yyyy')
group by 
	emp_codigo, 
	loc_codigo, 
	empresa, 
	razon_social, 
	local, 
	caja, 
	multimedia

*/
