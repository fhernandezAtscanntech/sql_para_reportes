/*
**********************************
Enzo y Daniel me piden un reporte que muestre, para un per�odo, los movimientos de cuentas corrientes de recargas que fueron del tipo traspaso bancario

Se supone que en alg�n momento se van a poder identificar de acuerdo al sello, aunque por ahora ese campo est� vac�o

Voy a usar una modificaci�n del reporte de Movimientos CC Recargas, filtrando este tipo de movimiento.

Quito toda referencia al local porque no hay
************************************
*/

select 
	rccc.emp_codigo 		"Emp.Codigo", 
	l.codigo 		"Loc.Codigo (I)", 
	l.loc_Cod_empresa 		"Loc.Codigo", 
	l.descripcion 		"Local", 
	rccc.codigo 		"CC Codigo", 
	rccc.descripcion 		"Cuenta Corriente", 
--	rccc.saldo		"Saldo Actual", 
--	rccc.sobregiro 		"Sobregiro", 
	rcmc.numero_mov_cc 		"Numero Mov CC", 
--	rcmc.caj_codigo 		"Caja", 
	rcmc.fecha 		"Fecha", 
	rcmc.Compañia 		"Compañia", 
	rcmc.nro_lote 		"Nro.Lote", 
	rcmc.audit_number 		"Nro.Audit", 
	rcmc.reference_number		"Nro.Ref", 
	rcmc.nro_tramite 		"Nro.Tram", 
	rctm.descripcion || decode 
		(rctm.codigo, 
		10, 
			decode (rcmc.estado, 
			0, ' - Pendiente', 
			1, ' - Confirmado', 
			2, ' - Reversado', 
			3, ' - Anulado', 
			4, ' - Borrado', 
			5, ' - Error', 
			' - Sin Definir'), 
		'')		"Tipo Movimiento", 
--	decode (
--		rcmc.tip_mov_cc, 
--			1, nvl (rcmc.importe_pos_mon, rcmc.importe_pos)*-1, 
--			2, 0, 
--			5, nvl (rcmc.importe_pos_mon, rcmc.importe_pos)*-1, 
--			decode ( 
--				nvl (rcmc.estado, 0), 
--				3, nvl (rcmc.importe_pos_mon, rcmc.importe_pos), 
--				nvl (rcmc.importe_pos_mon, rcmc.importe_pos) * nvl(rcmc.tip_ope_codigo, 1)) )	"Importe Pos", 
	decode (
		rcmc.tip_mov_cc, 
			1, rcmc.importe*-1, 
			2, 0, 
			5, rcmc.importe*-1, 
			decode ( 
				nvl (rcmc.estado, 0), 
				3, rcmc.importe, 
				rcmc.importe * nvl (rcmc.tip_ope_codigo, 1)) )	"Importe"
--	rcmc.arancel 		"Arancel", 
--	(select saldo from iposs.rc_saldos_mensuales rcsm where rcsm.emp_codigo = rccc.emp_codigo and rcsm.cue_cor_codigo = rccc.codigo and fecha = to_date('01/'||to_char(:Fecha, 'mm/yyyy'), 'dd/mm/yyyy'))		"Saldo Inicial", 
--	(select saldo from iposs.rc_saldos_mensuales rcsm where rcsm.emp_codigo = rccc.emp_codigo and rcsm.cue_cor_codigo = rccc.codigo and fecha = add_months (to_date ('01/' || to_char (:Fecha, 'mm/yyyy'), 'dd/mm/yyyy'), 1))		"Saldo Final"
from 
	iposs.locales l, 
	iposs.rc_cuentas_corrientes rccc, 
	iposs.rc_movimientos_cc rcmc, 
	iposs.rc_tipos_movimientos_cc rctm
where 	rccc.codigo = rcmc.cue_cor_codigo 
and 	rccc.emp_codigo = rcmc.emp_codigo
and 	rctm.codigo = rcmc.tip_mov_cc
and 	rcmc.loc_codigo = l.codigo(+) 
and 	rcmc.fecha between 
			to_date(:FechaDesde, 'dd/mm/yyyy')
			and 
			to_date (:FechaHasta, 'dd/mm/yyyy') + 1 - ( 1 / 24 / 60 / 60 )
-- and 	trim (upper(':CC')) = 'NULL' or rccc.codigo  in :CC
and 	nvl (rcmc.estado, 0) not in ( 2, 4, 5 )
and 	rcmc.tip_mov_cc = 7 		-- transferencias bancarias

/*
**********************************
**********************************
con recolector
**********************************
**********************************
*/

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select
		'select 
			rccc.emp_codigo 		"Emp.Codigo", 
			e.descripcion 		"Empresa", 
			rccc.codigo 		"CC Codigo", 
			rccc.descripcion 		"Cuenta Corriente", 
			rcmc.numero_mov_cc 		"Numero Mov CC", 
			rcmc.fecha 		"Fecha", 
			rcmc.Compa�ia 		"Compa�ia", 
			rcmc.nro_lote 		"Nro.Lote", 
			rcmc.audit_number 		"Nro.Audit", 
			rcmc.reference_number		"Nro.Ref", 
			rcmc.nro_tramite 		"Nro.Tram", 
			rctm.descripcion || decode 
				(rctm.codigo, 
				10, 
					decode (rcmc.estado, 
					0, '''||' - Pendiente'||''', 
					1, '''||' - Confirmado'||''', 
					2, '''||' - Reversado'||''', 
					3, '''||' - Anulado'||''', 
					4, '''||' - Borrado'||''', 
					5, '''||' - Error'||''', 
					'''||' - Sin Definir'||'''), 
				'''||''||''')		"Tipo Movimiento", 
			decode (
				rcmc.tip_mov_cc, 
					1, rcmc.importe*-1, 
					2, 0, 
					5, rcmc.importe*-1, 
					decode ( 
						nvl (rcmc.estado, 0), 
						3, rcmc.importe, 
						rcmc.importe * nvl (rcmc.tip_ope_codigo, 1)) )	"Importe"
		from 
			iposs.rc_cuentas_corrientes rccc, 
			iposs.rc_movimientos_cc rcmc, 
			iposs.rc_tipos_movimientos_cc rctm, 
			iposs.empresas e
		where 	rccc.codigo = rcmc.cue_cor_codigo 
		and 	rccc.emp_codigo = rcmc.emp_codigo
		and 	rctm.codigo = rcmc.tip_mov_cc
		and 	rccc.emp_codigo = e.codigo 
		and 	rcmc.fecha between 
					to_date('''||:FechaDesde||''', '''||'dd/mm/yyyy'||''')
					and 
					to_date ('''||:FechaHasta||''', '''||'dd/mm/yyyy'||''') + 1 - ( 1 / 24 / 60 / 60 )
		and 	nvl (rcmc.estado, 0) not in ( 2, 4, 5 )
		and 	rcmc.tip_mov_cc = 7
		' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
	select 
		rccc.emp_codigo 		"Emp.Codigo", 
		e.descripcion 		"Empresa", 
		rccc.codigo 		"CC Codigo", 
		rccc.descripcion 		"Cuenta Corriente", 
		rcmc.numero_mov_cc 		"Numero Mov CC", 
		rcmc.fecha 		"Fecha", 
		rcmc.Compa�ia 		"Compa�ia", 
		rcmc.nro_lote 		"Nro.Lote", 
		rcmc.audit_number 		"Nro.Audit", 
		rcmc.reference_number		"Nro.Ref", 
		rcmc.nro_tramite 		"Nro.Tram", 
		rctm.descripcion 		"Tipo Movimiento", 
		rcmc.importe	"Importe"
	from 
		iposs.rc_cuentas_corrientes rccc, 
		iposs.rc_movimientos_cc rcmc, 
		iposs.rc_tipos_movimientos_cc rctm
	where 	1 = 2
)

######################
consulta
######################
select 1 from #reco#
