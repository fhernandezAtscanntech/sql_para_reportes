/*
From: [mailto:nerrico@scanntech.com]
Sent: Thu, Jun 20, 2019 17:03 GMT-0300
To: 'Fredy Hernandez' <fhernandez@scanntech.com>, 'Silvia Hernández' <shernandez@scanntech.com>
Subject: Reporte primer ticket emitido / Nuevas cajas

> Fredy, como conversamos recién estaríamos necesitando un reporte que nos informe por tipo de caja (IPOS, mini, pagos, self) 
> las cajas nuevas en un determinado período, entendiendo como cajas nuevas aquellas que emitieron su primer ticket (según nos 
> confirmó POS el instalador emite un ticket cuando hace la instalación).
>  
> Los datos mínimos serían:
>
> RUT
> Razón Social
> Cliente
> Empresa local
> Tipo de caja
*/

multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
		select 
			l.loc_razon_social 		"Razón Social", 
			l.loc_ruc 				"RUT", 
			l.emp_codigo 			"Cod.Empresa", 
			l.loc_codigo 			"Cod.Local", 
			l.loc_cod_empresa 		"Sucursal", 
			l.emp_descripcion 		"Empresa", 
			l.loc_descripcion 		"Local", 
			cajas.caj_codigo 		"Caja",
			cajas.tipo 				"Tipo", 
			c.fecha_prim_mov 		"Primer Movimiento", 
			c.fecha_ult_mov 		"Ultimo Movimiento"
		from 	dwm_prod.l_ubicaciones l
			join dwm_prod.l_cajas c on c.emp_codigo = l.emp_codigo and c.loc_codigo = l.loc_codigo
			join (select 
					de.emp_codigo 	as emp_codigo, 
					de.loc_codigo 	as loc_codigo, 
					de.caj_codigo 	as caj_codigo, 
					case substr(de.config, 1, 11)
						when '''||'producto=IA'||''' then '''||'iPos Android'||'''
						when '''||'producto=IM'||''' then '''||'iPos Mini'||'''
						when '''||'producto=IP'||''' then '''||'iPos Pagos'||'''
						when '''||'producto=SC'||''' then '''||'Self Checkout'||'''
						when '''||'producto=TV'||''' then '''||'Terminal Vendedor'||'''
						when '''||'producto=VP'||''' then '''||'Verificador Precio'||'''
						else '''||'Otros - '||'''||substr (de.config, 1, 25)
					end 		as tipo, 
					de.fecha_ult_consulta 	fec_ult_consulta
				from 	iposs.distribuciones_pendientes de
				where 	de.fecha_ult_consulta is not null) cajas
				on cajas.emp_codigo = c.emp_codigo
				and cajas.loc_codigo = c.loc_codigo
				and cajas.caj_codigo = c.caj_codigo
		where 	c.fecha_prim_mov >= to_date('''||:FechaDesde||''', '''||'dd/mm/yyyy'||''')
		and 	c.fecha_prim_mov <= to_date('''||:FechaHasta||''', '''||'dd/mm/yyyy'||''') + 1 - 1/24/60/60
		and 	l.
		' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
	select l.loc_razon_social 		"Razón Social", 
			l.loc_ruc 				"RUT", 
			l.emp_codigo 			"Cod.Empresa", 
			l.loc_codigo 			"Cod.Local", 
			l.loc_cod_empresa 		"Sucursal", 
			l.emp_descripcion 		"Empresa", 
			l.loc_descripcion 		"Local", 
			cajas.caj_codigo 		"Caja",
			cajas.tipo 				"Tipo", 
			c.fecha_prim_mov 		"Primer Movimiento", 
			c.fecha_ult_mov 		"Ultimo Movimiento"
	from 	dwm_prod.l_ubicaciones l
		join dwm_prod.l_cajas c on c.emp_codigo = l.emp_codigo and c.loc_codigo = l.loc_codigo
		join (select 
				de.emp_codigo 	as emp_codigo, 
				de.loc_codigo 	as loc_codigo, 
				de.caj_codigo 	as caj_codigo, 
				case substr(de.config, 1, 11)
					when 'producto=IA' then 'iPos Android'
					when 'producto=IM' then 'iPos Mini'
					when 'producto=IP' then 'iPos Pagos'
					when 'producto=SC' then 'Self Checkout'
					when 'producto=TV' then 'Terminal Vendedor'
					when 'producto=VP' then 'Verificador Precio'
					else 'Otros - '||substr (de.config, 1, 25)
				end 		as tipo, 
				de.fecha_ult_consulta 	fec_ult_consulta
			from 	iposs.distribuciones_pendientes de) cajas
			on cajas.emp_codigo = c.emp_codigo
			and cajas.loc_codigo = c.loc_codigo
			and cajas.caj_codigo = c.caj_codigo
	where 1 = 2
)


######################
consulta
######################
select 1 from #reco#


