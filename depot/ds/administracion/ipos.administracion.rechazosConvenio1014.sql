multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select 
		'select 
			Cie_codigo, 
			Emp_codigo, 
			Cue_cor_codigo, 
			cedula, 
			importe, 
			de_cierre_ant, 
			numero_mov_cc, 
			linea
		from rc_cierres_detalles 
		where cie_codigo in 
			(select max(codigo) 
			from rc_cierres) 
		and acreditado = 0
		' as sql from dual
)
######################
tabla
######################
create table $$tabla$$ as (
select 
		Cie_codigo, 
		Emp_codigo, 
		Cue_cor_codigo, 
		cedula, 
		importe, 
		de_cierre_ant, 
		numero_mov_cc, 
		linea
	from rc_cierres_detalles 
	where 1 = 2
)

######################
consulta
######################
select 1 from #reco#
