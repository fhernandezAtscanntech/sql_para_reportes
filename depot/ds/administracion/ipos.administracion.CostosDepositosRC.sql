multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select
	'SELECT
		d.EMP_CODIGO,
		d.FECHA,
		d.NUMERO_CUENTA,
		d.NUMERO_DEPOSITO,
		d.IMPORTE,
		d.OBSERVACIONES,
		d.AUDIT_USER
	FROM
		IPOSS.RC_DEPOSITOS d
	WHERE 	d.FECHA >= last_day (add_months(trunc(SYSDATE), -2)) + 1
	AND 	d.FECHA < last_day (add_months(trunc(SYSDATE), -1)) + 1 
	AND 	d.OBSERVACIONES LIKE '''||'%COSTO%'||'''
	ORDER BY
		d.fecha
	' as sql from dual
)
######################
tabla
######################

create table $$tabla$$ as (
	SELECT
		d.EMP_CODIGO,
		d.FECHA,
		d.NUMERO_CUENTA,
		d.NUMERO_DEPOSITO,
		d.IMPORTE,
		d.OBSERVACIONES,
		d.AUDIT_USER
	FROM
		IPOSS.RC_DEPOSITOS d
	WHERE 1 = 2
)

######################
consulta
######################
select 1 from #reco#
