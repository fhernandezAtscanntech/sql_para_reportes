select 	
	err.descripcion, 
	sum(md.importe) Importe
from 	
	admin.mides_debitos_detalles md, 
	admin.mides_comisiones_errores err
where err.codigo = md.mid_com_err_codigo	
and md.cie_codigo = 	
	(select max(codigo)-1 
	from admin.mides_debitos)
group by 	
	err.descripcion
