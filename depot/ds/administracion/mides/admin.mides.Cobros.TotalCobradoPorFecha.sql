select 
	md.emp_codigo, 
	md.loc_codigo, 
	trunc(m.fecha),  
	sum (md.importe) importe
from 
	admin.mides_debitos_detalles md, 
	admin.mides_debitos m
where 	md.cie_codigo = m.codigo
and 	md.acreditado = 1
group by 
	md.emp_codigo, 
	md.loc_codigo, 
	trunc (m.fecha)
