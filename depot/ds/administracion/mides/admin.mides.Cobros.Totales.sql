select 	texto, monto
from 
	(select 1 as id, 'Total Que Deberiamos Haber Cobrado' as texto, sum(comision) monto
		from admin.mides_comisiones
		where cobrada <> -1
	UNION 
	select 2 as id, 'Total Que Llevamos Cobrado' as texto, sum(importe) monto
        from   
            (select md.emp_codigo, md.loc_codigo, sum(md.importe) importe, min(FECHA), max(FECHA)
            from admin.mides_debitos_total md
            where md.COD_ERROR=0
            group by md.emp_codigo,md.loc_codigo)
	UNION 
	select 3 as id, 'Total Que Mandamos Cobrar Y Recibimos Rechazos' as texto, sum(md.importe)	monto
		from admin.mides_debitos_detalles md, admin.mides_comisiones_errores err	
		where err.codigo = md.mid_com_err_codigo	
		and md.cie_codigo = 	
			(select max(codigo)-1 
			from admin.mides_debitos)
	UNION 
	select 4 as id, 'Total Que No Identificamos El Local' as texto, sum(comision) monto
		from admin.mides_comisiones mc 	
		where cedula is null 	
		and emp_codigo is null
	UNION 
	select 5 as id, 'Total Enviado A Cobrar Hoy' as texto, importe_total monto
		from admin.mides_debitos 	
		where codigo = 	
			(select max(codigo) 
			from admin.mides_debitos)
	UNION
	select 6 as id, 'Total Que Cobro De Mas Y Se Devolvio' as texto, sum(ya_devuelto) monto
		from admin.mides_devolucion_comisiones) tabla
order by id


