select 		
	trunc (fecha_lote) as FechaLote, 	
	sum (comision) Comision	
from 		
	admin.mides_comisiones	
where cedula is null		
and cobrada <> -1
group by trunc (fecha_lote)
