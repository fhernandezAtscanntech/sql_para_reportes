multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select 
		'select distinct m.emp_codigo||'''||';'||'''||m.loc_codigo||'''||';'||'''||m.caj_codigo as texto	
		from movimientos m,mi_movimientos_pagos mi
		where m.emp_codigo=mi.mov_emp_codigo
		and m.fecha_comercial = mi.fecha_comercial
		and m.numero_mov = mi.mov_numero_mov
		and m.fecha_comercial between to_date('''||:FechaDesde||''', '''||'dd/mm/yyyy'||''') 
		and to_date('''||:FechaHasta||''', '''||'dd/mm/yyyy'||''')
		' as sql from dual
)

######################
tabla
######################
create table $$tabla$$ as (
		select distinct m.emp_codigo||';'||m.loc_codigo||';'||m.caj_codigo as texto	
		from movimientos m,mi_movimientos_pagos mi
		where 1=2
)

######################
consulta
######################
select 1 from #reco#

