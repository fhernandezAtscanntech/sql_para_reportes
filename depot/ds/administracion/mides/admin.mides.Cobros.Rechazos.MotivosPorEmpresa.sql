select 
	mc.emp_codigo, 
	mc.loc_codigo, 
	err.descripcion texto, 
	sum(md.importe) importe, 
	min (fecha_lote) min_fecha_lote,
	max (fecha_lote) max_fecha_lote
from 
	admin.mides_debitos_detalles md, 
	admin.mides_comisiones mc, 
	admin.mides_comisiones_errores err
where 	md.MID_COM_CODIGO = mc.codigo
and 	md.acreditado != 1
and 	md.cie_codigo = 
		(select max (codigo) - 1 
		from admin.mides_debitos)
and 	md.mid_com_err_codigo = err.codigo
and mc.cobrada <> -1
group by 
	mc.emp_codigo, 
	mc.loc_codigo, 
	err.descripcion
UNION
select 
	mc.emp_codigo, 
	mc.loc_codigo, 
	'no configurado'  texto, 
	sum(mc.comision) importe, 
	min (fecha_lote) min_fecha_lote,
	max (fecha_lote) max_fecha_lote
from 
	admin.mides_comisiones mc
where cedula is null
and mc.cobrada <> -1
group by 
	mc.emp_codigo, 
	mc.loc_codigo
