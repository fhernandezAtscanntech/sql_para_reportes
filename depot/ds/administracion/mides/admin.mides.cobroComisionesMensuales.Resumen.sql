select 
	mdd.cie_codigo, 
	m.fecha, 
	mdd.emp_codigo, 
	mdd.loc_codigo, 
	mdd.cedula, 
	mdd.importe, 
	decode (mdd.acreditado, -1, 'Acreditado por otro medio o cierre no procesado', 0, 'No Acreditado', 1, 'Acreditado', 2, 'Falló la acreditación y se volvió a enviar', 'ERROR: ('||to_char(mdd.acreditado)||')') Acreditado, 
	mdd.mid_com_err_codigo, 
	mce.descripcion Error
from 
	admin.mides_comisiones mc,
	admin.mides_debitos_detalles mdd,
	admin.mides_debitos m, 
	admin.mides_comisiones_errores mce
where mc.ENTIDAD_RECEPTORA='MENS'
and mc.codigo = mdd.MID_COM_CODIGO
and m.codigo = mdd.cie_codigo
and mdd.emp_codigo = mc.emp_codigo
and mdd.MID_COM_ERR_CODIGO = mce.codigo(+)
and m.audit_date >= sysdate - 60
and mc.cobrada <> -1
