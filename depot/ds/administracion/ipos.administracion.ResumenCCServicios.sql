multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select
	'select 
	rccc.emp_codigo 		"Emp.Codigo", 
	e.descripcion 			"Empresa", 
	rccc.codigo 		"CC Codigo", 
	rccc.descripcion 		"Cuenta Corriente", 
	rccc.saldo		"Saldo Actual", 
	rccc.sobregiro 		"Sobregiro", 
	(select saldo 
		from iposs.rc_saldos_mensuales rcsm 
		where rcsm.emp_codigo = rccc.emp_codigo 
		and rcsm.cue_cor_codigo = rccc.codigo 
		and fecha = to_date('''||'01/'||:Fecha||''', '''||'dd/mm/yyyy'||'''))		"Saldo Inicial", 
	(select saldo 
		from iposs.rc_saldos_mensuales rcsm 
		where rcsm.emp_codigo = rccc.emp_codigo 
		and rcsm.cue_cor_codigo = rccc.codigo 
		and fecha = add_months (to_date ('''||'01/'||:Fecha||''', '''||'dd/mm/yyyy'||'''), 1))		"Saldo Final", 
	(iposs.rep_resumenCCRecargas(rccc.codigo, '''||'solo depositos'||''', 
		to_date('''||'01/'||:Fecha||''', '''||'dd/mm/yyyy'||'''), 
		add_months (to_date ('''||'01/'||:Fecha||''', '''||'dd/mm/yyyy'||'''), 1) - 1)) "Depósitos acreditados", 
	(iposs.rep_resumenCCRecargas(rccc.codigo, '''||'recargas'||''', 
		to_date('''||'01/'||:Fecha||''', '''||'dd/mm/yyyy'||'''), 
		add_months (to_date ('''||'01/'||:Fecha||''', '''||'dd/mm/yyyy'||'''), 1) - 1)) "Recargas y servicios", 
	(iposs.rep_resumenCCRecargas(rccc.codigo, '''||'solo traspasos'||''', 
		to_date('''||'01/'||:Fecha||''', '''||'dd/mm/yyyy'||'''), 
		add_months (to_date ('''||'01/'||:Fecha||''', '''||'dd/mm/yyyy'||'''), 1) - 1)) "Traspasos Rec.Celulares", 
	(iposs.rep_resumenCCRecargas(rccc.codigo, '''||'recargas pos'||''', 
		to_date('''||'01/'||:Fecha||''', '''||'dd/mm/yyyy'||'''), 
		add_months (to_date ('''||'01/'||:Fecha||''', '''||'dd/mm/yyyy'||'''), 1) - 1)
	-
	iposs.rep_resumenCCRecargas(rccc.codigo, '''||'recargas'||''', 
		to_date('''||'01/'||:Fecha||''', '''||'dd/mm/yyyy'||'''), 
		add_months (to_date ('''||'01/'||:Fecha||''', '''||'dd/mm/yyyy'||'''), 1) - 1)) "Comisiones"
from 
	iposs.rc_cuentas_corrientes rccc, 
	iposs.empresas e
where	rccc.emp_codigo = e.codigo
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
	select 
		rccc.emp_codigo 		"Emp.Codigo", 
		e.descripcion 			"Empresa", 
		rccc.codigo 		"CC Codigo", 
		rccc.descripcion 		"Cuenta Corriente", 
		rccc.saldo		"Saldo Actual", 
		rccc.sobregiro 		"Sobregiro", 
		0.0		"Saldo Inicial", 
		0.0 	"Saldo Final", 
		0.0 	"Depósitos acreditados", 
		0.0 	"Recargas y servicios", 
		0.0 	"Traspasos Rec.Celulares", 
		0.0 	"Comisiones"
	from 
		iposs.rc_cuentas_corrientes rccc, 
		iposs.empresas e
	where 1=2
)

######################
consulta
######################
select 1 from #reco#

/* select 
	rccc.emp_codigo 		"Emp.Codigo", 
	e.descripcion 			"Empresa", 
	rccc.codigo 		"CC Codigo", 
	rccc.descripcion 		"Cuenta Corriente", 
	rccc.saldo		"Saldo Actual", 
	rccc.sobregiro 		"Sobregiro", 
	(select saldo 
		from iposs.rc_saldos_mensuales rcsm 
		where rcsm.emp_codigo = rccc.emp_codigo 
		and rcsm.cue_cor_codigo = rccc.codigo 
		and fecha = to_date('''||'01/'||:Fecha||''', '''||'dd/mm/yyyy'||'''))		"Saldo Inicial", 
	(select saldo 
		from iposs.rc_saldos_mensuales rcsm 
		where rcsm.emp_codigo = rccc.emp_codigo 
		and rcsm.cue_cor_codigo = rccc.codigo 
		and fecha = add_months (to_date ('''||'01/'||:Fecha||''', '''||'dd/mm/yyyy'||'''), 1))		"Saldo Final", 
	(iposs.rep_resumenCCRecargas(rccc.codigo, 'solo depositos', 
		to_date('''||'01/'||:Fecha||''', '''||'dd/mm/yyyy'||'''), 
		add_months (to_date ('''||'01/'||:Fecha||''', '''||'dd/mm/yyyy'||'''), 1) - 1)) "Depósitos acreditados", 
	(iposs.rep_resumenCCRecargas(rccc.codigo, 'recargas', 
		to_date('''||'01/'||:Fecha||''', '''||'dd/mm/yyyy'||'''), 
		add_months (to_date ('''||'01/'||:Fecha||''', '''||'dd/mm/yyyy'||'''), 1) - 1)) "Recargas y servicios", 
	(iposs.rep_resumenCCRecargas(rccc.codigo, 'solo traspasos', 
		to_date('''||'01/'||:Fecha||''', '''||'dd/mm/yyyy'||'''), 
		add_months (to_date ('''||'01/'||:Fecha||''', '''||'dd/mm/yyyy'||'''), 1) - 1)) "Traspasos Rec.Celulares", 
	(iposs.rep_resumenCCRecargas(rccc.codigo, 'recargas pos', 
		to_date('''||'01/'||:Fecha||''', '''||'dd/mm/yyyy'||'''), 
		add_months (to_date ('''||'01/'||:Fecha||''', '''||'dd/mm/yyyy'||'''), 1) - 1)
	-
	iposs.rep_resumenCCRecargas(rccc.codigo, 'recargas', 
		to_date('''||'01/'||:Fecha||''', '''||'dd/mm/yyyy'||'''), 
		add_months (to_date ('''||'01/'||:Fecha||''', '''||'dd/mm/yyyy'||'''), 1) - 1)) "Comisiones"
from 
	iposs.rc_cuentas_corrientes rccc, 
	iposs.empresas e
where	rccc.emp_codigo = e.codigo
*/


