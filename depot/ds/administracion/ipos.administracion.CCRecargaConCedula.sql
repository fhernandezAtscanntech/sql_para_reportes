multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select 
		'select rcl.emp_codigo||'''||';'||'''||rcl.loc_codigo||'''||';'||'''||rcl.compañia||'''||';'||'''||rcc.cedula as texto
		from 
			rc_cuentas_corrientes_locales rcl, 
			rc_cuentas_corrientes rcc
		where 	rcl.cue_cor_codigo = rcc.codigo
		and 	rcc.cedula is not null
		and 	rcc.cedula != 0
	' as sql from dual
)

######################
tabla
######################
create table $$tabla$$ as (
	select rcl.emp_codigo||'''||';'||'''||rcl.loc_codigo||'''||';'||'''||rcl.compañia||'''||';'||'''||rcc.cedula as texto
	from 
		rc_cuentas_corrientes_locales rcl, 
		rc_cuentas_corrientes rcc
	where 1 = 2
)

######################
consulta
######################
select 1 from #reco#
