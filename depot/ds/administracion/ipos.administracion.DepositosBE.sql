multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
	select 
		e.codigo, 
		e.descripcion, 
		rcd.cue_cor_codigo, 
		b.descripcion banco, 
		rcd.numero_cuenta, 
		rcd.numero_deposito, 
		rcd.importe,
		rcd.fecha_acreditado, 
		rcd.audit_date, 
		rcd.fecha, 
		rcd.observaciones
	from 
			iposs.rc_depositos rcd, 
			iposs.bancos b, 
			iposs.empresas e
		where rcd.audit_date between trunc(sysdate-1) and trunc(sysdate)-1/24/60/60
		and rcd.ban_codigo = b.codigo
		and e.codigo = rcd.emp_codigo
		order by 
			emp_codigo desc' as sql from dual)
)

######################
tabla
######################

create table $$tabla$$ as (
	select 
	e.codigo, 
	e.descripcion, 
	rcd.cue_cor_codigo, 
	b.descripcion banco, 
	rcd.numero_cuenta, 
	rcd.numero_deposito, 
	rcd.importe,
	rcd.fecha_acreditado, 
	rcd.audit_date, 
	rcd.fecha, 
	rcd.observaciones
	from 
		iposs.rc_depositos rcd, 
		iposs.bancos b, 
		iposs.empresas e
	where
		1=2
)


######################
consulta
######################
select 1 from #reco#
