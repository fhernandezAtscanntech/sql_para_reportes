create or replace function iposs.rep_resumenCCRecargas (
cc_codigo rc_cuentas_corrientes.codigo%type, 
tipo_dato 	varchar2, 
fecha_desde date,
fecha_hasta date)
return number
is
	v_dep 		number := 0;
	v_tras		number := 0;

	v_recpos 	number := 0;
	v_recimp 	number := 0;
	v_anulpos 	number := 0;
	v_anulimp 	number := 0;
	
	v_ret number;
begin
	for linea in 
		(select m.tip_mov_cc, nvl (m.estado, 0) estado, sum (nvl (m.importe_pos_mon, m.importe_pos)) Importe_pos, sum (m.importe) importe
		from iposs.rc_movimientos_cc m
		where 	m.cue_cor_codigo = cc_codigo
		and 	m.tip_mov_cc in (1, 3, 4, 5, 7, 10)
		and 	nvl (m.estado, 0) not in ( 2, 3, 4, 5 ) 
		and 	m.fecha between fecha_desde and fecha_hasta + 1 - (1/24/60/60)
		group by m.tip_mov_cc, nvl (m.estado, 0))
	loop
		case linea.tip_mov_cc
		when 1 -- reserva de saldo para recarga. No siempre hay recarga pero la reserva es quien quita el saldo a la cc
		then 
			v_recpos := v_recpos + linea.importe_pos;
			v_recimp := v_recimp + linea.importe;
		when 3 -- depósito
		then
			v_dep := v_dep + linea.importe;
		when 4 -- anulación
		then
			v_anulpos := v_anulpos + linea.importe_pos;
			v_anulimp := v_anulimp + linea.importe;
		when 5 -- anulación automática
		then 
			v_anulpos := v_anulpos + linea.importe_pos;
			v_anulimp := v_anulimp + linea.importe;
		when 7 -- Traspaso
		then	
			v_tras := v_tras + linea.importe;
		when 10 -- recarga switch nuevo
		then 
			if linea.estado = 3
			then 
				v_anulpos := v_anulpos + linea.importe_pos;
				v_anulimp := v_anulimp + linea.importe;
			else 
				v_recpos := v_recpos + linea.importe_pos;
				v_recimp := v_recimp + linea.importe;
			end if;
		end case;
	end loop;
	
	case tipo_dato
	when 'depositos' 		then v_ret := v_dep + v_tras;
	when 'solo depositos' 	then v_ret := v_dep;
	when 'solo traspasos' 	then v_ret := v_tras;
	when 'recargas'			then v_ret := v_recimp - v_anulimp;
	when 'solo recargas'	then v_ret := v_recimp;
	when 'solo anuladas'	then v_ret := v_anulimp;
	when 'recargas pos' 	then v_ret := v_recpos - v_anulpos;
	when 'solo recargas pos'then v_ret := v_recpos;
	when 'solo anuladas pos'then v_ret := v_anulpos;
	when 'saldo'			then v_ret := v_dep + v_tras - v_recimp + v_anulimp; -- si, están al revés. La cc es acreedora
	else v_ret := null;
	end case;
	return v_ret;
end;
/

grant execute on iposs.rep_resumenCCRecargas to iposs_fx, iposs_rep
/
