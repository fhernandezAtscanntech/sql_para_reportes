with modifComp as 
	(select 
		codigo, 
		(case tip_com_codigo when 102 then -1 when 112 then -1 else 1 end) signo, 
		coalesce(substring(definicion from 'TpoCambio>(.*?)<')::numeric, 1) tipoCambio
		from te_comprobantes
		where timestamp_firma >= :FechaDesde
		and timestamp_firma < :FechaHasta + 1)
select
	com.fecha_emision,
	com.timestamp_firma,
	com.tip_com_codigo,
	com.serie,
	com.numero,   
	l.emp_codigo,
	l.codigo,
	l.loc_cod_empresa,
	com_adm.caj_codigo,
	modifComp.tipoCambio as tpoCambio, 
	modifComp.signo * modifComp.TipoCambio * coalesce(substring(definicion from 'MntNoGrv>(.*?)<')::numeric, 0) as totMntNoGrv,
	modifComp.signo * modifComp.TipoCambio * coalesce(substring(definicion from 'MntExpoyAsim>(.*?)<')::numeric, 0) as totMntExpyAsim,
	modifComp.signo * modifComp.TipoCambio * coalesce(substring(definicion from 'MntImpuestoPerc>(.*?)<')::numeric, 0) as totMntImpPerc,
	modifComp.signo * modifComp.TipoCambio * coalesce(substring(definicion from 'MntIVaenSusp>(.*?)<')::numeric, 0) as totMntIVAenSusp,
	modifComp.signo * modifComp.TipoCambio * coalesce(substring(definicion from 'MntNetoIvaTasaMin>(.*?)<')::numeric, 0) as totMntIVATasaMin,
	modifComp.signo * modifComp.TipoCambio * coalesce(substring(definicion from 'MntNetoIVATasaBasica>(.*?)<')::numeric, 0) as totMntIVATasaBas,
	modifComp.signo * modifComp.TipoCambio * coalesce(substring(definicion from 'MntNetoIVAOtra>(.*?)<')::numeric, 0) as totMntIVAOtra,
	modifComp.signo * modifComp.TipoCambio * coalesce(substring(definicion from 'MntIVATasaMin>(.*?)<')::numeric, 0) as mntIVATasaMin,
	modifComp.signo * modifComp.TipoCambio * coalesce(substring(definicion from 'MntIVATasaBasica>(.*?)<')::numeric, 0) as mntIVATasaBas,
	modifComp.signo * modifComp.TipoCambio * coalesce(substring(definicion from 'MntIVAOtra>(.*?)<')::numeric, 0) as mntIVAOtra,
	modifComp.signo * modifComp.TipoCambio * coalesce(substring(definicion from 'MntTotal>(.*?)<')::numeric, 0) as totMntTotal,
	modifComp.signo * modifComp.TipoCambio * coalesce(substring(definicion from 'MntTotRetenido>(.*?)<')::numeric, 0) as totMntRetenido,
	modifComp.signo * modifComp.TipoCambio * coalesce(substring(definicion from 'MontoNF>(.*?)<')::numeric, 0) as totMntNF,
	modifComp.signo * modifComp.TipoCambio * coalesce(substring(definicion from 'MntPagar>(.*?)<')::numeric, 0) as totMntPagar,
	modifComp.signo * modifComp.TipoCambio * coalesce(substring(definicion from 'MntTotal>(.*?)<')::numeric, 0) as totMntTotalContable
from
    te_envios_comprobantes env
    join te_envios_sobres env_sob on env_sob.codigo = env.env_sob_codigo
    join te_sobres s on s.men_codigo = env_sob.sob_codigo
    left join te_acks_sobres ack_sob on ack_sob.men_codigo = env_sob.ack_sob_codigo
    join te_comprobantes com on com.codigo = env.com_codigo
	join modifComp on modifComp.codigo = env.com_codigo
	join te_comprobantes_administrados com_adm on com_adm.com_codigo = com.codigo
    left join te_acks_comprobantes ack on s.men_codigo = ack.sob_codigo
    left join te_acks_comprobantes_detalles ack_det on ack_det.ack_com_codigo = ack.men_codigo and ack_det.com_codigo = com.codigo
    join locales l on l.codigo = com_adm.loc_codigo
    join te_sujetos emi on emi.codigo = s.suj_emisor_codigo
    left join te_sujetos rec on rec.codigo = s.suj_receptor_codigo
where
    com.timestamp_firma >= :FechaDesde
    and com.timestamp_firma < :FechaHasta + 1
    and :FechaHasta - :FechaDesde <= 31
    and rec.codigo = 0 -- DGI  --
    and emi.codigo in (select suj_codigo from te_sujetos_internos where emp_codigo = 1000) -- EL DORADO
--     and env.estado = 1
    and ( trim (upper (:Estado)) is null or (case :Estado when 'ENVIADO' then env.estado = 1 else env.estado <> 1 end))
    and l.emp_codigo = 1000
    and ( :Local is null or l.codigo in (:Local) )
order by
    env_sob.codigo desc
	