select 
	l.loc_cod_empresa 	"Sucursal", 
	l.descripcion 		"Local", 
	m.caj_codigo 		"Caja", 
	m.fecha_comercial 	"Fecha Comercial", 
	m.tipo_doc_ef 		"Tipo Doc. Ef.", 
	case m.tipo_doc_ef
		when 101 then 'e-Ticket'
		when 102 then 'NC de e-Ticket'
		when 103 then 'ND de e-Ticket'
		when 111 then 'e-Factura'
		when 112 then 'NC de e-Factura'
		when 113 then 'ND de e-Factura'
		when 121 then 'e-Factura Exp'
		when 122 then 'NC de e-Factura Exp'
		when 123 then 'ND de e-Factura Exp'
		when 182 then 'e-Resguardo'
		when 201 then 'e-Ticket Contingencia'
		when 211 then 'e-Factura Contingencia'
		when 212 then 'Nota de Crédito de e-Factura Contingencia'
		when 213 then 'Nota de Débito de e-Factura Contingencia'
		else to_char(m.tipo_doc_ef)||' - No identificado'
	end 				"Descripción", 
	m.serie_ef 			"Serie", 
	m.numero_operacion 	"Nro.Operación", 
	m.nombre_factura 	"Nombre Factura", 
	m.direccion_factura	"Dir. Factura", 
	m.ruc_factura 		"RUT Factura", 
	sum ( decode (md.iva_codigo, 2, md.monto_iva , 0) *
		  decode (md.tip_det_codigo, 4, 1, 6, 1, 17, 1, -1)) 	"IVA Mínimo", 
	sum ( decode (md.iva_codigo, 3, md.monto_iva , 0)*
		  decode (md.tip_det_codigo, 4, 1, 6, 1, 17, 1, -1)) 	"IVA Básico", 
	sum ( md.monto_imp_1 * 
		  decode (md.tip_det_codigo, 4, 1, 6, 1, 17, 1, -1)) 	"IMC", 
	sum ( md.monto_imp_2 *
		  decode (md.tip_det_codigo, 4, 1, 6, 1, 17, 1, -1)) 	"INAC", 
	sum ( md.monto_imp_3 *
		  decode (md.tip_det_codigo, 4, 1, 6, 1, 17, 1, -1)) 	"IVA Percepción", 
	m.total 			"Total"
from 
	iposs.movimientos m
	join iposs.movimientos_detalles md
		on md.mov_emp_codigo = m.emp_codigo
		and md.fecha_comercial = m.fecha_comercial
		and md.mov_numero_mov = m.numero_mov
	join iposs.locales l
		on l.codigo = m.loc_codigo
		and l.emp_codigo = m.emp_codigo
where	m.tipo_operacion = 'VENTA'
and 	m.tipo_doc_ef is not null
and 	m.fecha_comercial between :FechaDesde and :FechaHasta
and 	:FechaHasta - :FechaDesde <= 31
and 	l.codigo = :Local
and 	m.emp_codigo = $EMPRESA_LOGUEADA$
and 	md.mov_emp_codigo = $EMPRESA_LOGUEADA$
and 	l.emp_codigo = $EMPRESA_LOGUEADA$
group by 
	l.loc_cod_empresa,  
	l.descripcion, 
	m.caj_codigo, 
	m.fecha_comercial, 
	m.tipo_doc_ef, 
	m.serie_ef, 
	m.numero_operacion, 
	m.nombre_factura, 
	m.direccion_factura, 
	m.ruc_factura, 
	m.total
