/*
Esta consulta ya estaba en la base de FacturaElectrónica (https://efactura1.scanntech.com/IpossBE/ipossbe.jsp?database=switchfactura)

El nombre original del OD en la base es "Vencimiento/Terminación de CAEs "

La respaldo porque me están pidiendo modificarla.

*/
select
	rs.ruc as raz_soc_ruc, rs.descripcion as raz_soc_descripcion,
	case tc.codigo 
		when 101 then '101 - e-Ticket'
		when 102 then '102 - NC de e-Ticket'
		when 103 then '103 - ND de e-Ticket'
		when 111 then '111 - e-Factura'
		when 112 then '112 - NC de e-Factura'
		when 113 then '113 - ND de e-Factura'
		when 121 then '121 - e-Factura Exp'
		when 122 then '122 - NC de e-Factura Exp'
		when 123 then '123 - ND de e-Factura Exp'
		when 182 then '182 - e-Resguardo'
			end as tip_com_descripcion, 
	coalesce(sum(cae.numero_hasta - coalesce(cae.max_numero_asignado,cae.numero_desde)),0) as numeros_disponibles, 
	min(cae.fecha_vencimiento) as primer_vencimiento, max(cae.fecha_vencimiento) as ultimo_vencimiento
from 
	razones_sociales rs
	join te_tipos_comprobantes tc on tc.codigo in (101,102,103,111,112,113,121,122,123,182) and rs.emp_codigo <> $EMPRESA_MODELO$
	left join te_caes cae on cae.raz_soc_codigo = rs.codigo and cae.tip_com_codigo = tc.codigo and cae.fecha_vencimiento > current_timestamp
where
	($EMPRESA_MODELO$ =  $EMPRESA_LOGUEADA$ or rs.emp_codigo = $EMPRESA_LOGUEADA$)
and exists (
	select 1
	from
		te_comprobantes com join te_sujetos_internos suj_int on
			com.suj_emisor_codigo = suj_int.suj_codigo
			and com.audit_date >= current_date - interval '15 day'
			and suj_int.raz_soc_codigo = rs.codigo
	)
group by
	rs.ruc, rs.descripcion, tc.codigo, tc.descripcion
having
	(tc.codigo = 101 and sum(cae.numero_hasta - coalesce(cae.max_numero_asignado,cae.numero_desde)) <= :PARAM_MIN_101)
	or (tc.codigo = 102 and sum(cae.numero_hasta - coalesce(cae.max_numero_asignado,cae.numero_desde)) <= :PARAM_MIN_102)
	or (tc.codigo = 103 and sum(cae.numero_hasta - coalesce(cae.max_numero_asignado,cae.numero_desde)) <= :PARAM_MIN_103)
	or (tc.codigo = 111 and sum(cae.numero_hasta - coalesce(cae.max_numero_asignado,cae.numero_desde)) <= :PARAM_MIN_111)
	or (tc.codigo = 112 and sum(cae.numero_hasta - coalesce(cae.max_numero_asignado,cae.numero_desde)) <= :PARAM_MIN_112)
	or (tc.codigo = 113 and sum(cae.numero_hasta - coalesce(cae.max_numero_asignado,cae.numero_desde)) <= :PARAM_MIN_113)
	or (tc.codigo = 121 and sum(cae.numero_hasta - coalesce(cae.max_numero_asignado,cae.numero_desde)) <= :PARAM_MIN_121)
	or (tc.codigo = 122 and sum(cae.numero_hasta - coalesce(cae.max_numero_asignado,cae.numero_desde)) <= :PARAM_MIN_122)
	or (tc.codigo = 123 and sum(cae.numero_hasta - coalesce(cae.max_numero_asignado,cae.numero_desde)) <= :PARAM_MIN_123)
	or (tc.codigo = 182 and sum(cae.numero_hasta - coalesce(cae.max_numero_asignado,cae.numero_desde)) <= :PARAM_MIN_182)
	or (max(cae.fecha_vencimiento) <= current_date + :PARAM_DIAS)
order by 
	rs.descripcion, tc.codigo
