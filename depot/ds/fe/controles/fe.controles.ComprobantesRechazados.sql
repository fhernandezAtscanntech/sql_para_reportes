/*
Hubo que hacer cambios a la consulta original para que el largo no fuera mayor a 4065 caracteres:

cae_ran_caj - crc
ack_sob - aks
suj_int - si
tip_doc - td
elimino "as" 
comentarios y "true"
env_sob_ack_com - esac
ack_com_men - acm
ack_com_det - acd
eliminación tabuladores
env_sob - envSob
com_adm - comAdm
env_com - envCom
ack_com - ackCom
com_cor - comCor
sob_men - sobMen
aks_Men - aksMen

de 4659 se bajo a 4065

*/

select
distinct on (sob.men_codigo) sob.men_codigo sob_codigo,
loc.loc_cod_empresa as local,
crc.caj_codigo caja,
aks.id_emisor,
aks.id_receptor,
com.codigo com_codigo,
rcp.numero_documento rcp_numero_documento,
envSob.codigo envSob_codigo,
crc.emp_codigo, 
si.resolucion_dgi,
crc.caj_codigo,
com.audit_date com_audit_date,
com.tip_com_codigo,
com.serie com_serie,
com.numero com_numero,
com.timestamp_firma com_timestamp_firma,
com.fecha_emision com_fecha_emision,
comAdm.reportable_dgi comAdm_reportable_dgi,
substring(com.codigo_seguridad,1,6) com_codigo_seguridad,
td.descripcion rcp_td_descripcion,
rcp.nombre rcp_nombre,
sob.id_emisor sob_id_emisor,
case envSob.estado when 0 then 'PENDIENTE' when 1 then 'ENVIADO' when 2 then 'ACEPTADO' when 3 then 'COMPLETADO' when 4 then 'RECHAZADO' when 5 then 'ERROR' else null end envSob_estado,
envSob.fecha_estado envSob_fecha_estado,
case envCom.estado when 0 then 'PENDIENTE' when 1 then 'ENVIADO' when 2 then 'RECHAZADO' when 3 then 'ANULADO' else null end envCom_estado,
envCom.fecha_estado,
aks.men_codigo aks_codigo,
aks.token_consulta_estado_comprobante,
case aks.estado when 0 then 'ACEPTADO' when 1 then 'RECHAZADO' else null end aks_estado,
aks.audit_date aks_audit_date,
ackCom.men_codigo ackCom_codigo,
case acd.estado when 0 then 'ACEPTADO' when 1 then 'RECHAZADO' when 2 then 'ANULADO' else null end acd_estado,
ackCom.audit_date ackCom_audit_date,
comCor.codigo comCor_codigo, 
comCor.fecha comCor_fecha, 
cfe_tag_date('FchEmis',comCor.definicion,'YYYY-MM-DD') comCor_fecha_emision,
com_Cor_tipo_error(comCor.error) comCor_tipo_error,
comCor.error comCor_detalle_error,
com.definicion xml_cfe,
sobMen.definicion xml_sob,
aksMen.definicion xml_aks,
acm.definicion xml_ack_cfe
from 
razones_sociales ras
join te_caes cae on ras.codigo = cae.raz_soc_codigo
join te_caes_rangos_cajas crc on crc.cae_codigo = cae.codigo
join locales loc on loc.codigo = crc.loc_codigo
join te_sujetos_internos si on si.emp_codigo = cae.emp_codigo and si.raz_soc_codigo = cae.raz_soc_codigo
left join te_comprobantes com on com.tip_com_codigo = cae.tip_com_codigo and com.serie = cae.serie and com.suj_emisor_codigo = si.suj_codigo
left join te_comprobantes_administrados comAdm on comAdm.com_codigo = com.codigo and comAdm.emp_codigo = cae.emp_codigo and comAdm.loc_codigo = crc.loc_codigo and comAdm.caj_codigo = crc.caj_codigo
left join te_sujetos rcp on rcp.codigo = com.suj_receptor_codigo
left join te_tipos_documentos td on td.codigo = rcp.tip_doc_codigo
left join te_comprobantes_sobres com_sob on com_sob.com_codigo = com.codigo
left join te_sobres sob on sob.men_codigo = com_sob.sob_codigo
left join te_envios_sobres envSob on envSob.sob_codigo = sob.men_codigo
left join te_envios_comprobantes envCom on envCom.env_Sob_codigo = envSob.codigo and envCom.com_codigo = com.codigo
left join te_envios_sobres_acks_comprobantes esac on esac.env_Sob_codigo = envSob.codigo
left join te_acks_sobres aks on aks.men_codigo = envSob.ack_sob_codigo
left join te_acks_comprobantes ackCom on ackCom.men_codigo = esac.ack_Com_codigo
left join te_acks_comprobantes_detalles acd on acd.ack_Com_codigo = ackCom.men_codigo and acd.com_codigo = com.codigo
left join te_comprobantes_en_correccion comCor on comCor.emp_codigo = cae.emp_codigo and comCor.loc_codigo = crc.loc_codigo and comCor.caj_codigo = crc.caj_codigo and cfe_tag_num('TipoCFE',comCor.definicion) = cae.tip_com_codigo and cfe_tag('Serie',comCor.definicion) = cae.serie 
and cfe_tag_num('Nro',comCor.definicion) = com.numero
left join te_mensajes sobMen on sobMen.codigo = sob.men_codigo
left join te_mensajes acm on acm.codigo = ackCom.men_codigo
left join te_mensajes aksMen on aksMen.codigo = aks.men_codigo
where
ras.ruc = '100004430014'
and com.numero between crc.numero_desde and crc.numero_hasta
and (sob.suj_receptor_codigo is null or sob.suj_receptor_codigo = 0)
and com.timestamp_firma >= :FechaDesde
and com.timestamp_firma < :FechaHasta
and envCom.estado <> 1 
order by sob.men_codigo
