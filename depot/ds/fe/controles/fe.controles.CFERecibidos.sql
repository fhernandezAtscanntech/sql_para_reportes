select  
	res.audit_date as fecha_recibido,
	emi.numero_documento as ruc_emisor,
	emi.nombre as denominacion_emisor,
	se.correo_electronico,
	coalesce (tc.descripcion, com.tip_com_codigo::text) cfe_tipo,
	com.serie as cfe_serie,
	com.numero as cfe_numero,
	com.fecha_emision
from te_recepciones_sobres res
join te_recepciones_comprobantes rco on rco.rec_sob_codigo = res.codigo
join te_comprobantes com on com.codigo = rco.com_codigo
join te_sujetos rec on rec.codigo = res.suj_int_receptor_codigo
join te_sujetos emi on emi.codigo = com.suj_emisor_codigo
join te_sujetos_externos se on se.suj_codigo = emi.codigo
join te_sobres sob on sob.men_codigo = res.sob_codigo
left join te_acks_sobres acs on acs.men_codigo = res.ack_sob_codigo
left join te_recepciones_sobres_anulaciones rsa on rsa.rec_sob_codigo = res.codigo
left join te_acks_comprobantes acc on acc.sob_codigo = sob.men_codigo
left join te_tipos_comprobantes tc on tc.codigo = com.tip_com_codigo
where
	rec.numero_documento = :RUT
	and res.audit_date >= :FechaDesde
	and res.audit_date < (:FechaHasta + 1)
