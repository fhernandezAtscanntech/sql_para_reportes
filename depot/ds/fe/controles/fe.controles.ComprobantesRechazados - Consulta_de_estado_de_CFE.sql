/*
correo original:

Reporte agendado (diario) para obtener la lista de comprobantes rechazados, caja correspondiente y versión del software:
Se tiene que crear basándose en la consulta que adjunto como archivo, más el join con la tabla te_comunicaciones_cajas, 
que es la tabla donde se guarda la versión del software de la caja.



*/

select
  distinct on (sob.men_codigo) sob.men_codigo as sob_codigo,
	loc.loc_cod_empresa as local,
	cae_ran_caj.caj_codigo as caja,
	ack_sob.id_emisor,
	ack_sob.id_receptor,
	com.codigo as com_codigo,
  rcp.numero_documento as rcp_numero_documento,
  env_sob.codigo as env_sob_codigo,
	cae_ran_caj.emp_codigo, 
	suj_int.resolucion_dgi,
	cae_ran_caj.caj_codigo,
	com.audit_date as com_audit_date,
	com.tip_com_codigo,
	com.serie as com_serie,
	com.numero as com_numero,
	com.timestamp_firma as com_timestamp_firma,
	com.fecha_emision as com_fecha_emision,
	com_adm.reportable_dgi as com_adm_reportable_dgi,
	substring(com.codigo_seguridad,1,6) as com_codigo_seguridad,
	tip_doc.descripcion as rcp_tip_doc_descripcion,
	rcp.nombre as rcp_nombre,
	sob.id_emisor as sob_id_emisor,
	case env_sob.estado when 0 then 'PENDIENTE' when 1 then 'ENVIADO' when 2 then 'ACEPTADO' when 3 then 'COMPLETADO' when 4 then 'RECHAZADO' when 5 then 'ERROR' else null end as env_sob_estado,
	env_sob.fecha_estado as env_sob_fecha_estado,
  case env_com.estado when 0 then 'PENDIENTE' when 1 then 'ENVIADO' when 2 then 'RECHAZADO' when 3 then 'ANULADO' else null end as env_com_estado,
	env_com.fecha_estado,
	ack_sob.men_codigo as ack_sob_codigo,
	ack_sob.token_consulta_estado_comprobante,
	case ack_sob.estado when 0 then 'ACEPTADO' when 1 then 'RECHAZADO' else null end as ack_sob_estado,
	ack_sob.audit_date as ack_sob_audit_date,
	ack_com.men_codigo as ack_com_codigo,
	case ack_com_det.estado when 0 then 'ACEPTADO' when 1 then 'RECHAZADO' when 2 then 'ANULADO' else null end as ack_com_det_estado,
	ack_com.audit_date as ack_com_audit_date,
	com_cor.codigo as com_cor_codigo, 
	com_cor.fecha as com_cor_fecha, 
	cfe_tag_date('FchEmis',com_cor.definicion,'YYYY-MM-DD') as com_cor_fecha_emision,
	com_cor_tipo_error(com_cor.error) as com_cor_tipo_error,
	com_cor.error as com_cor_detalle_error,
	com.definicion as xml_cfe,
	sob_men.definicion as xml_sob,
	ack_sob_men.definicion as xml_ack_sob,
	ack_com_men.definicion as xml_ack_cfe
from 
	razones_sociales ras
	join te_caes cae on ras.codigo = cae.raz_soc_codigo
	join te_caes_rangos_cajas cae_ran_caj on cae_ran_caj.cae_codigo = cae.codigo
	join locales loc on loc.codigo = cae_ran_caj.loc_codigo
	join te_sujetos_internos suj_int on suj_int.emp_codigo = cae.emp_codigo and suj_int.raz_soc_codigo = cae.raz_soc_codigo
	left join te_comprobantes com on com.tip_com_codigo = cae.tip_com_codigo and com.serie = cae.serie and com.suj_emisor_codigo = suj_int.suj_codigo
	left join te_comprobantes_administrados com_adm on com_adm.com_codigo = com.codigo and com_adm.emp_codigo = cae.emp_codigo and com_adm.loc_codigo = cae_ran_caj.loc_codigo and com_adm.caj_codigo = cae_ran_caj.caj_codigo
	left join te_sujetos rcp on rcp.codigo = com.suj_receptor_codigo
	left join te_tipos_documentos tip_doc on tip_doc.codigo = rcp.tip_doc_codigo
	left join te_comprobantes_sobres com_sob on com_sob.com_codigo = com.codigo
	left join te_sobres sob on sob.men_codigo = com_sob.sob_codigo
	left join te_envios_sobres env_sob on env_sob.sob_codigo = sob.men_codigo
	left join te_envios_comprobantes env_com on env_com.env_sob_codigo = env_sob.codigo and env_com.com_codigo = com.codigo
	left join te_envios_sobres_acks_comprobantes env_sob_ack_com on env_sob_ack_com.env_sob_codigo = env_sob.codigo
	left join te_acks_sobres ack_sob on ack_sob.men_codigo = env_sob.ack_sob_codigo
	left join te_acks_comprobantes ack_com on ack_com.men_codigo = env_sob_ack_com.ack_com_codigo
	left join te_acks_comprobantes_detalles ack_com_det on ack_com_det.ack_com_codigo = ack_com.men_codigo and ack_com_det.com_codigo = com.codigo
	left join te_comprobantes_en_correccion com_cor on com_cor.emp_codigo = cae.emp_codigo and com_cor.loc_codigo = cae_ran_caj.loc_codigo and com_cor.caj_codigo = cae_ran_caj.caj_codigo and cfe_tag_num('TipoCFE',com_cor.definicion) = cae.tip_com_codigo and cfe_tag('Serie',com_cor.definicion) = cae.serie 
		and cfe_tag_num('Nro',com_cor.definicion) = com.numero
	left join te_mensajes sob_men on sob_men.codigo = sob.men_codigo
	left join te_mensajes ack_com_men on ack_com_men.codigo = ack_com.men_codigo
	left join te_mensajes ack_sob_men on ack_sob_men.codigo = ack_sob.men_codigo
where
	true
	and ras.ruc = '100004430014'
	and com.numero between cae_ran_caj.numero_desde and cae_ran_caj.numero_hasta

	-- RECEPTOR: DGI
	and (sob.suj_receptor_codigo is null or sob.suj_receptor_codigo = 0)

	and com.timestamp_firma >= '2017-11-01'
	and com.timestamp_firma < '2017-11-27'
	and env_com.estado <> 1 -- DISTINTO DE ENVIADO
order by sob.men_codigo asc