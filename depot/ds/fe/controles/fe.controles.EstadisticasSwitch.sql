/* consulta original 
select 
	Cargados.emp_codigo empresa, 
	co.CompTotal, 
	Cargados.cant Cargados, 
	Salvados.cant Salvados, 
	round (100*Salvados.cant/co.CompTotal, 1) Tot_vs_Salv, 
	round (100*Salvados.cant/Cargados.cant, 1) Carg_vs_Salv    
from 
	(select emp_codigo, count(*) CompTotal 
		from comprobantes 
		where FECHA_EMISION between '01-NOV-2018' and '01-FEB-2019' 
		and TIP_DOC_IN_CODIGO in ('1','2','4','7')
		group By emp_codigo) co,
	(select EMP_CODIGO, count (*) cant 
		from ESTADISTICAS_CFES 
		where AUDIT_DATE between '01-NOV-2018' and '01-FEB-2019'  
		group By emp_codigo) Cargados,
	(select emp_codigo, count (*) cant 
		from ESTADISTICAS_CFES 
		where esta_salvado = 1 
		and AUDIT_DATE between '01-NOV-2018' and '01-FEB-2019'  
		group by emp_codigo) Salvados
where 	Cargados.emp_codigo = Salvados.emp_codigo
and 	co.emp_codigo = Salvados.emp_codigo;

#########################
#########################
Consulta adaptada
#########################
#########################

select 
	Cargados.emp_codigo empresa, 
	co.CompTotal, 
	Cargados.cant Cargados, 
	Salvados.cant Salvados, 
	round (100*Salvados.cant/co.CompTotal, 1) Tot_vs_Salv, 
	round (100*Salvados.cant/Cargados.cant, 1) Carg_vs_Salv    
from 
	(select emp_codigo, count(*) CompTotal 
		from comprobantes 
		where TIP_DOC_IN_CODIGO in ('1','2','4','7')
		and FECHA_EMISION between nvl (to_date(':FechaDesde', 'dd/mm/yyyy'), trunc(sysdate)-31) and nvl (to_date(':FechaHasta', 'dd/mm/yyyy'), trunc(sysdate)-1)
		group By emp_codigo) co,
	(select EMP_CODIGO, count (*) cant 
		from ESTADISTICAS_CFES 
		where AUDIT_DATE between nvl (to_date(':FechaDesde', 'dd/mm/yyyy'), trunc(sysdate)-31) and nvl (to_date(':FechaHasta', 'dd/mm/yyyy'), trunc(sysdate)-1)
		group By emp_codigo) Cargados,
	(select emp_codigo, count (*) cant 
		from ESTADISTICAS_CFES 
		where esta_salvado = 1 
		and AUDIT_DATE between nvl (to_date(':FechaDesde', 'dd/mm/yyyy'), trunc(sysdate)-31) and nvl (to_date(':FechaHasta', 'dd/mm/yyyy'), trunc(sysdate)-1)
		group by emp_codigo) Salvados
where 	Cargados.emp_codigo = Salvados.emp_codigo
and 	co.emp_codigo = Salvados.emp_codigo;
*/


multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
		select 
			Cargados.emp_codigo empresa, 
			e.descripcion emp_descripcion, 
			co.CompTotal, 
			Cargados.cant Cargados, 
			Salvados.cant Salvados, 
			round (100*Salvados.cant/co.CompTotal, 1) Tot_vs_Salv, 
			round (100*Salvados.cant/Cargados.cant, 1) Carg_vs_Salv    
		from 
			(select emp_codigo, count(*) CompTotal 
				from comprobantes 
				where TIP_DOC_IN_CODIGO in ('''||'1'||''', '''||'2'||''', '''||'4'||''', '''||'7'||''')
				and FECHA_EMISION between trunc(sysdate)-31 and trunc(sysdate)-1
				group By emp_codigo) co,
			(select EMP_CODIGO, count (*) cant 
				from ESTADISTICAS_CFES 
				where AUDIT_DATE between trunc(sysdate)-31 and trunc(sysdate)-1
				group By emp_codigo) Cargados,
			(select emp_codigo, count (*) cant 
				from ESTADISTICAS_CFES 
				where esta_salvado = 1 
				and AUDIT_DATE  between trunc(sysdate)-31 and trunc(sysdate)-1
				group by emp_codigo) Salvados, 
			iposs.empresas e
		where 	Cargados.emp_codigo = Salvados.emp_codigo
		and 	co.emp_codigo = Salvados.emp_codigo
		and 	e.codigo = cargados.emp_codigo
		' as sql from dual
)
######################
tabla
######################

create table $$tabla$$ as (
	select 
		Cargados.emp_codigo empresa, 
		e.descripcion emp_descripcion, 
		co.CompTotal, 
		Cargados.cant Cargados, 
		Salvados.cant Salvados, 
		round (100*Salvados.cant/co.CompTotal, 1) Tot_vs_Salv, 
		round (100*Salvados.cant/Cargados.cant, 1) Carg_vs_Salv    
	from 
		(select emp_codigo, count(*) CompTotal 
			from comprobantes 
			where 1=2
			group by emp_codigo) co, 
		(select EMP_CODIGO, count (*) cant 
			from ESTADISTICAS_CFES 
			where 1 = 2
			group By emp_codigo) Cargados,
		(select emp_codigo, count (*) cant 
			from ESTADISTICAS_CFES 
			where 1 = 2
			group by emp_codigo) Salvados, 
		iposs.empresas e
	where 	1 = 2
)


######################
consulta
######################
select 1 from #reco#



########################################################################################################################################################################################################################################################################
########################################################################################################################################################################################################################################################################
########################################################################################################################################################################################################################################################################
########################################################################################################################################################################################################################################################################
########################################################################################################################################################################################################################################################################



en base local - si nulo -> null
				si valor -> 'valor'
		remota - si nulo -> ' null'
				 si valor -> 'valor'
				 
				 
decode 
	(nvl (:fechaDesde, trunc (sysdate)-31), 
		' null', trunc (sysdate) -31), 
		nvl (:fechaDesde, trunc (sysdate)-31)

case 
	when :FechaDesde is null then trunc(sysdate)-31
	when :FechaDesde = ' null' then trunc(sysdate)-31
	else to_date(:FechaDesde, 'dd/mm/yyyy'
end

original
				where AUDIT_DATE between 
					nvl (to_date(decode (trim (upper ('||:FechaDesde||')), '''||'NULL'||''', null, '||:FechaDesde||'), '''||'dd/mm/yyyy'||'''), trunc(sysdate)-31) 
					and nvl (to_date(decode (trim (upper ('||:FechaHasta||')), '''||'NULL'||''', null, '||:FechaHasta||'), '''||'dd/mm/yyyy'||'''), trunc(sysdate)-1)

no hay forma de observar todos los casos:
