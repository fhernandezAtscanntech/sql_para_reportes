select
	loc.loc_cod_empresa,
	loc.descripcion, 
	com_orig.timestamp_firma as fechahora_firma,
	com_orig.tip_com_codigo,
	com_orig.serie as com_orig_serie,
	com_orig.numero as com_orig_numero,
	com_nuev.serie as com_nuev_serie,
	com_nuev.numero as com_nuev_numero
from 
	te_comprobantes com_orig
join te_comprobantes_rechazados_corregidos com_rec_cor 
	on com_rec_cor.com_orig_codigo = com_orig.codigo
join te_comprobantes com_nuev 
	on com_nuev.codigo = com_rec_cor.com_nuev_codigo
join te_sujetos suj 
	on suj.codigo = com_orig.suj_emisor_codigo
join te_comprobantes_administrados com_adm 
	on com_adm.com_codigo = com_orig.codigo
join locales loc 
	on loc.codigo = com_adm.loc_codigo
where 	loc.emp_codigo = $EMPRESA_LOGUEADA$
--     suj.numero_documento = '100004430014'
and 	com_orig.timestamp_firma >= '2017-01-01'
and 	com_orig.timestamp_firma < '2018-01-01'
order by 
	com_orig.timestamp_firma asc
