﻿multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
		select 
			pr.ruc, 
			pr.RAZON_SOCIAL, 
			sum (ce_salvados.cant) salvados, 
			sum (ce_cargados.cant) cargados, 
			round (sum (ce_salvados.cant/ce_cargados.cant*100), 2) Ratio
		from  
			proveedores pr,
			(select ce.rut_emisor, count(*) cant
				from ESTADISTICAS_CFES ce
				where esta_salvado = 1
				and AUDIT_DATE between trunc(sysdate)-31 and trunc(sysdate)-1
				group by ce.rut_emisor) ce_salvados,
			(select ce.rut_emisor, count(*) cant
				from ESTADISTICAS_CFES ce
				where AUDIT_DATE between trunc(sysdate)-31 and trunc(sysdate)-1
				group by ce.rut_emisor) ce_cargados
		where 	ce_salvados.RUT_EMISOR = pr.RUC
		and 	ce_cargados.rut_emisor = pr.ruc
		group by 
			pr.ruc, 
			pr.RAZON_SOCIAL
		' as sql from dual
)
######################
tabla
######################

create table $$tabla$$ as (
	select 
			pr.ruc, 
			pr.RAZON_SOCIAL, 
			sum (ce_salvados.cant) salvados, 
			sum (ce_cargados.cant) cargados, 
			round (sum (ce_salvados.cant/ce_cargados.cant*100), 2) Ratio
		from  
			proveedores pr,
			(select ce.rut_emisor, count(*) cant
				from ESTADISTICAS_CFES ce
				where 1 = 2
				group by ce.rut_emisor) ce_salvados,
			(select ce.rut_emisor, count(*) cant
				from ESTADISTICAS_CFES ce
				where 1 = 2
				group by ce.rut_emisor) ce_cargados
		where 	1 = 2
		group by 
			pr.ruc, 
			pr.RAZON_SOCIAL
)


######################
consulta
######################
select 1 from #reco#



########################################################################################################################################################################################################################################################################
########################################################################################################################################################################################################################################################################
########################################################################################################################################################################################################################################################################
########################################################################################################################################################################################################################################################################
########################################################################################################################################################################################################################################################################



en base local - si nulo -> null
				si valor -> 'valor'
		remota - si nulo -> ' null'
				 si valor -> 'valor'
				 
				 
decode 
	(nvl (:fechaDesde, trunc (sysdate)-31), 
		' null', trunc (sysdate) -31), 
		nvl (:fechaDesde, trunc (sysdate)-31)

case 
	when :FechaDesde is null then trunc(sysdate)-31
	when :FechaDesde = ' null' then trunc(sysdate)-31
	else to_date(:FechaDesde, 'dd/mm/yyyy'
end

original
				where AUDIT_DATE between 
					nvl (to_date(decode (trim (upper ('||:FechaDesde||')), '''||'NULL'||''', null, '||:FechaDesde||'), '''||'dd/mm/yyyy'||'''), trunc(sysdate)-31) 
					and nvl (to_date(decode (trim (upper ('||:FechaHasta||')), '''||'NULL'||''', null, '||:FechaHasta||'), '''||'dd/mm/yyyy'||'''), trunc(sysdate)-1)

no hay forma de observar todos los casos:
