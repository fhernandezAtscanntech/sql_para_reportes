/*	
// razones sociales
*/
fe.generales.razonesSociales

select r.ruc RUT, r.descripcion
from razones_sociales r, funcionarios f
where r.emp_codigo = f.emp_codigo
and f.fecha_borrado is null
and f.emp_codigo = $EMPRESA_LOGUEADA$
and (f.codigo = $CODIGO_FUNCIONARIO_LOGUEADO$
OR ( $CODIGO_FUNCIONARIO_LOGUEADO$ in 
	(select codigo
			from funcionarios
			where emp_codigo = 1
			and login like 'SC%'
			and fecha_borrado is null)))
