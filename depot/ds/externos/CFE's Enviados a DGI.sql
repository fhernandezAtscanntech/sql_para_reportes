select 
	es.codigo as env_sob_codigo,
	es.fecha_estado as env_sob_fecha_estado,
	case es.estado 
		when 0 then 'PENDIENTE' 
		when 1 then 'ENVIADO (SIN RESPUESTA)'
		when 2 then 'ACEPTADO' 
		when 3 then 'ACEPTADO'
		when 4 then 'RECHAZADO' 
		when 5 then 'ERROR' 
		else 'INVALIDO' end as env_sob_estado,
	s.men_codigo as sob_codigo, 
	s.fecha as sob_fecha, 
	to_char(s.fecha,'DD/MM/YYYY HH24:MI:SS') as sob_fecha_string,
	ec.com_codigo, 
	ec.indice as com_indice, 
	ec.fecha_estado as com_fecha_estado, 
		case ec.estado 
		when 0 then 'PENDIENTE' 
		when 1 then 'ACEPTADO' 
		when 2 then 'RECHAZADO' 
		when 3 then 'ANULADO'
		else 'INVALIDO' end as env_estado,
	rs.ruc as raz_soc_rut,
	rs.descripcion as raz_soc_descripcion,
	ca.emp_codigo,
	l.loc_cod_empresa, 
	ca.caj_codigo, 
	c.tip_com_codigo,
	to_char(c.tip_com_codigo, '999999FM')||' - '||tc.descripcion as tip_com_descripcion,
	c.serie as com_serie, 
	c.numero as com_numero, 
	c.fecha_emision as com_fecha_emison,
	c.codigo_seguridad as com_codigo_seguridad,  
	case when cja.codigo is null then 'POS' else 'ERP' end as caj_tipo,
	coalesce(substring(definicion from 'TpoCambio>(.*?)<')::numeric, 1) as tot_TpoCambio,
	coalesce(substring(definicion from 'MntNoGrv>(.*?)<')::numeric, 0) as tot_MntNoGrv,
	coalesce(substring(definicion from 'MntExpoyAsim>(.*?)<')::numeric, 0) as tot_MntExpoyAsim,
	coalesce(substring(definicion from 'MntImpuestoPerc>(.*?)<')::numeric, 0) as tot_MntImpuestoPerc,
	coalesce(substring(definicion from 'MntIVaenSusp>(.*?)<')::numeric, 0) as tot_MntIVaenSusp,
	coalesce(substring(definicion from 'MntNetoIvaTasaMin>(.*?)<')::numeric, 0) as tot_MntNetoIvaTasaMin,
	coalesce(substring(definicion from 'MntNetoIVATasaBasica>(.*?)<')::numeric, 0) as tot_MntNetoIVATasaBasica,
	coalesce(substring(definicion from 'MntNetoIVAOtra>(.*?)<')::numeric, 0) as tot_MntNetoIVAOtra,
	coalesce(substring(definicion from 'MntIVATasaMin>(.*?)<')::numeric, 0) as tot_MntIVATasaMin,
	coalesce(substring(definicion from 'MntIVATasaBasica>(.*?)<')::numeric, 0) as tot_MntIVATasaBasica,
	coalesce(substring(definicion from 'MntIVAOtra>(.*?)<')::numeric, 0) as tot_MntIVAOtra,
	coalesce(substring(definicion from 'MntTotal>(.*?)<')::numeric, 0) as tot_MntTotal,
	coalesce(substring(definicion from 'MntTotRetenido>(.*?)<')::numeric, 0) as tot_MntTotRetenido,
	coalesce(substring(definicion from 'MontoNF>(.*?)<')::numeric, 0) as tot_MontoNF,
	coalesce(substring(definicion from 'MntPagar>(.*?)<')::numeric, 0) as tot_MntPagar
from
	te_envios_comprobantes ec
	join te_envios_sobres es on es.codigo = ec.env_sob_codigo
	join te_sobres s on s.men_codigo = es.sob_codigo
	join te_comprobantes c on c.codigo = ec.com_codigo
	join te_comprobantes_administrados ca on ca.com_codigo = c.codigo
	left join te_tipos_comprobantes tc on tc.codigo = c.tip_com_codigo
	left join te_cajas_administradas cja on cja.loc_codigo = ca.loc_codigo and cja.codigo = ca.caj_codigo
	join locales l on l.codigo = ca.loc_codigo
	join razones_sociales rs on rs.codigo = l.raz_soc_codigo
	join te_sujetos sj on sj.codigo = s.suj_receptor_codigo
where
	sj.codigo = 0
--	and c.tip_com_codigo in (101,102,103,111,112,113,181,182)
	and s.fecha >= date_trunc('day', :FEC_SOB_D)
	and s.fecha < date_trunc('day', :FEC_SOB_H)
	and l.emp_codigo = $EMPRESA_LOGUEADA$
	and (:RS_CODIGO is null or l.raz_soc_codigo = :RS_CODIGO)
	and (:LOC_COD_EMPRESA is null or l.loc_cod_empresa = :LOC_COD_EMPRESA)
	and (:CAJ_CODIGO is null or ca.caj_codigo = :CAJ_CODIGO)
	and ((:ERP = 1 and cja.codigo is not null) or (:POS = 1 and cja.codigo is null))
	and (:FEC_EMI is null or c.fecha_emision = :FEC_EMI)
	and (:TC_CODIGO is null or c.tip_com_codigo = :TC_CODIGO)
	and (:SERIE is null or c.serie = :SERIE)
	and (:NRO is null or c.numero = :NRO)
order by
es.fecha_estado
