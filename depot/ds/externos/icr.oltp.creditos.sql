select 
	mp.loc_codigo 		"Codigo Local", 
	l.descripcion 		"Local", 
	m.caj_codigo 		"Caja", 
	mp.fecha_comercial 	"Fecha", 
	m.hora_operacion 	"Hora", 
	mo.simbolo		"Moneda", 
	tp.descripcion 	"Tipo de Pago", 
	cr.codigo		"Codigo Credito (int)", 
	cr.descripcion 		"Credito", 
	cl.cre_codigo_Externo 	"Codigo Credito", 
	mp.numero_tarjeta 	"Numero Tarjeta", 
	mp.numero_documento 	"Numero Documento", 
	mp.lote_credito 	"Lote", 
	mp.cuotas 			"Cuotas", 
	nvl (to_char(mp.numero_autorizacion), mp.numero_autorizacion_chr) 	"Numero Autorizacion", 
	sum (mp.importe*mp.cotiza_compra) 	"Importe", 
	sum (mp.descuento_inc_financiera*mp.cotiza_compra) 	"Descuento Inc. Financiera", 
	sum (mp.descuento_afam*mp.cotiza_compra) 		"Descuento AFAM"
from
	iposs.movimientos_de_pagos mp,
	iposs.movimientos m, 
	iposs.monedas mo, 
	iposs.tipos_de_pagos tp,
	iposs.locales l,
	iposs.creditos cr,
	iposs.creditos_locales cl
where
	m.numero_mov = mp.mov_numero_mov
and m.emp_codigo = mp.mov_emp_codigo
and m.fecha_comercial = mp.fecha_comercial 
and 
	mp.mov_emp_codigo = $EMPRESA_LOGUEADA$ and
	mp.loc_codigo = l.codigo
and
	mp.mon_codigo = mo.codigo
and
	mp.cre_codigo = cl.cre_codigo_Externo
and	mp.loc_codigo = cl.loc_codigo
and
	tp.codigo = mp.tip_pag_codigo
and 
	cl.cre_codigo = cr.codigo
and
	mp.tip_pag_codigo in (10, 13)
and
	:FechaHasta - :FechaDesde <= 31
and
	mp.fecha_comercial between :FechaDesde and :FechaHasta
and
	l.codigo in :Local 
group by 
	mp.loc_codigo 		, 
	l.descripcion 		, 
	m.caj_codigo, 
	mp.fecha_comercial 	, 
	m.hora_operacion,
	mo.simbolo		, 
	tp.descripcion , 
	cr.codigo		, 
	cr.descripcion 		, 
	cl.cre_codigo_Externo 	, 
	mp.numero_tarjeta 	, 
	mp.numero_documento 	, 
	mp.lote_credito, 
	mp.cuotas, 
	nvl (to_char(mp.numero_autorizacion), mp.numero_autorizacion_chr)
