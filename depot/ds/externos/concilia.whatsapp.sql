-- consulta recibida por correo wmartinez@


with enviados as (
		select fecha::date fecha, numero_telefono telefono, id_template
		from whatsapp.mensajes_aprobados
		where fecha >= {FECHA DESDE} and fecha < {FECHA HASTA}
	)
	, enviados_sesion as (
		select fecha, telefono, count(*) total
		from enviados
		where id_template is null
		group by fecha, telefono
	)
	, enviados_template as (
		select fecha, telefono, count(*) total
		from enviados
		where id_template is not null
		group by fecha, telefono
	)
	, recibidos as (
		select fecha::date, numero_telefono telefono, count(*) total
		from whatsapp.mensajes_recibidos_wp
		where fecha >= {FECHA DESDE} and fecha < {FECHA HASTA}
		group by fecha::date, numero_telefono
	)
select COALESCE(es.fecha, et.fecha, r.fecha) as fecha,
	  COALESCE(es.telefono, et.telefono, r.telefono)::bigint telefono,
	  COALESCE(es.total, 0) total_sesion,
	  COALESCE(et.total, 0) total_template,
	  COALESCE(r.total, 0) total_recibidos
from enviados_sesion es
full join enviados_template et on es.fecha = et.fecha and et.telefono = es.telefono
full join recibidos r on coalesce(es.fecha, et.fecha) = r.fecha and coalesce(es.telefono, et.telefono) = r.telefono 
order by fecha asc, telefono asc