select 
	t.id_transaccion, 
	t.lote, 
	t.estado, 
	t.descripcion, 
	t.importe, 
	t.conciliada, 
	t.reimpresa, 
	t.fecha_en_comprobante,
	m.producto, 
	m.numero_operacion, 
	m.importe_pos,
	t.id_locacion, 
	l.descripcion as local,
	m.caj_codigo, 
	f.apellido||', '||f.nombre as funcionario
from 	iposs.transaccion_swpgof t
join 	iposs.rc_con_mov_caja m
	on 	m.con_mov_codigo = t.con_mov_caj_codigo
join 	iposs.locales l
	on 	m.loc_codigo = l.codigo
join 	iposs.funcionarios f
	on 	m.fun_codigo = f.codigo
where
		m.fecha_pos between :FechaDesde and :FechaHasta
and 	:FechaHasta - :FechaDesde <= 92
and 	l.codigo in :Local
