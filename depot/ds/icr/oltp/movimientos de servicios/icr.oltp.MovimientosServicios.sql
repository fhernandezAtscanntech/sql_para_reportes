select 
	m.loc_codigo	"Codigo Local", 
	l.descripcion	"Local", 
	m.caj_codigo	"Codigo Caja", 
	c.descripcion	"Caja", 
	m.fecha_comercial	"Fecha", 
	mo.Simbolo	"Moneda", 
	sl.ser_codigo_externo	"Codigo Servicio", 
	s.descripcion	"Servicio", 
	mds.numero_servicio	"Numero Servicio", 
sum (decode (mds.tip_det_codigo, 17, mds.cantidad, 18, -mds.cantidad, 0)) "Cantidad", 
sum (decode (mds.tip_det_codigo, 17, mds.importe, 18, -mds.importe, 0)) "Importe"
from 
	movimientos m, 
	movimientos_Det_servs mds,
	monedas mo,
	locales l, 
	cajas c,
	servicios s, 
	servicios_locales sl
where
		m.numero_mov = mds.mov_numero_mov
and 	m.emp_codigo = mds.mov_emp_codigo
and 	m.fecha_comercial = mds.fecha_comercial
and
	m.loc_codigo = l.codigo
and
	m.caj_codigo = c.codigo
and 	m.loc_Codigo = c.loc_Codigo
and
	m.mon_codigo = mo.codigo
and
	mds.ser_codigo = sl.ser_codigo_Externo
and	mds.loc_codigo = sl.loc_codigo
and 
	sl.ser_codigo = s.codigo
and
	l.codigo = c.loc_Codigo
and 
	m.tipo_operacion = 'VENTA'
and
	m.fecha_comercial between :FechaDesde and :FechaHasta
and	
--	(nvl (:Local, l.codigo) = l.codigo)
	( trim (upper (':Local')) = 'NULL' or l.codigo in :Local )
group by 
	m.loc_codigo, 
	l.descripcion, 
	m.caj_codigo, 
	c.descripcion, 
	m.fecha_comercial, 
	mo.simbolo, 
	sl.ser_codigo_Externo, 
	s.descripcion
