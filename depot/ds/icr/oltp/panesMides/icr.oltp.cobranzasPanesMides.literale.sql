select
	decode (to_char (smp.fecha_comercial, 'Dy'), 
		'Jue', smp.fecha_comercial+4, 
		'Vie', smp.fecha_comercial+4, 
		'S�b', smp.fecha_comercial+4, 
		'Dom', smp.fecha_comercial+3, 
			smp.fecha_comercial+2)			"Fecha Pago", 
	c.descripcion						"Caja", 
	cr.descripcion						"Cr�dito", 
	l.descripcion 						"Local", 
	l.emp_codigo 						"Cod. Empresa", 
	l.codigo 						"Cod. Local", 
	smp.fecha_comercial 					"Fecha Venta", 
	smp.cantidad_pagos 					"Cantidad Pagos", 
	sum (smp.cotiza_compra * smp.importe * 1.22)
		-( sum (smp.cotiza_compra * smp.importe * 1.22 * iposs.f_arancel_mides ('BROU', smp.fecha_comercial) * 1.22) )
		-( sum (smp.cotiza_compra * smp.importe * 1.22 * iposs.f_arancel_mides ('SCANNTECH', smp.fecha_comercial)) ) 	"Importe Cobro Mides Lit.E", 
	sum (smp.cotiza_compra * smp.importe * 1.22 * iposs.f_arancel_mides ('SCANNTECH', smp.fecha_comercial)) 			"DGI Literal E", 
	sum (smp.cotiza_compra * smp.importe * 1.22 * iposs.f_arancel_mides ('SCANNTECH', smp.fecha_comercial) * 1.22) 		"Mides Scanntech", 
	sum (smp.cotiza_compra * smp.importe * 1.22 * iposs.f_arancel_mides ('BROU', smp.fecha_comercial) * 1.22) 		"Mides BROU", 
	sum (smp.cotiza_compra * smp.importe * 1.22) 				"Importe Pago"
from
	iposs.cajas c, 
	iposs.creditos cr, 
	iposs.creditos_empresas ce, 
	iposs.locales l, 
	iposs.tipos_de_pagos tp, 
	iposs.sum_movimientos_de_pagos smp
where 
	( l.codigo = c.loc_codigo ) 
and 
	( c.codigo = smp.caj_codigo 
and 	  c.loc_codigo = smp.loc_codigo ) 
and 
	( tp.codigo = smp.tip_pag_codigo ) 
and 
	( ce.cre_cod_externo(+) = smp.cre_codigo 
and 	  ce.emp_codigo(+) = smp.mov_emp_codigo ) 
and 
	( cr.codigo = ce.cre_codigo )
and 
	( ce.cre_codigo in ( 453, 701 ) ) -- credito Panes/Mides
and 
	( smp.fecha_comercial between :FechaDesde and :FechaHasta ) 
and 
 	( c.Codigo in :Caja )
and 
 	( l.codigo in :Local ) 
and
	( tp.descripcion in ( 'CREDITO', 'DEBITO' ) ) 
group by 
	decode (to_char (smp.fecha_comercial, 'Dy'),
		'Jue', smp.fecha_comercial+4, 
		'Vie', smp.fecha_comercial+4, 
		'S�b', smp.fecha_comercial+4, 
		'Dom', smp.fecha_comercial+3, 
			smp.fecha_comercial+2), 
	c.descripcion, 
	cr.descripcion, 
	l.descripcion, 
	l.emp_codigo, 
	l.codigo, 
	smp.fecha_comercial, 
	smp.cantidad_pagos
