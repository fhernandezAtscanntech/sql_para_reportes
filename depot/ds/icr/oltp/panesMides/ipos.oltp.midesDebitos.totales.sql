select 
	l.emp_codigo 		"Cod.Empresa", 
	l.codigo 			"Cod.Local(I)", 
	l.loc_cod_empresa 	"Cod.Local", 
	l.descripcion 		"Local", 
	dt.cedula 			"Cédula", 
	dt.cie_codigo 		"Cod.Cierre", 
	dt.fecha 			"Fecha", 
	dt.importe 			"Importe", 
	decode (dt.cod_error, 0, 'Sin Errores', 'Con Errores, cod:'||to_char(dt.cod_error)) 	"Resultado", 
	e.descripcion 		"Mensaje"
from 
	admin.mides_debitos_total dt, 
	admin.mides_comisiones_errores e, 
	iposs.locales l
where	dt.emp_codigo = l.emp_codigo
and 	dt.loc_codigo = l.codigo
and 	dt.cod_error = e.codigo
and 	dt.fecha between :FechaDesde and :FechaHasta
and 	dt.emp_codigo = $EMPRESA_LOGUEADA$
