select 
	l.emp_codigo 		"Cod.Empresa", 
	l.codigo 			"Cod.Local(I)", 
	l.loc_cod_empresa 	"Cod.Local", 
	l.descripcion 		"Local", 
	dt.cedula 			"Cédula", 
-- 	dt.importe 			"Importe", 
-- 	decode (dt.cod_error, 0, 'Sin Errores', 'Con Errores, cod:'||to_char(dt.cod_error)) 	"Resultado"
	dt.cie_codigo 		"Cod.Cierre", 
	dd.importe 			"Importe", 
	case dd.acreditado
		when 1 then 'Debitado'
		when 2 then 'En proceso'
		when 0 then 'Pendiente débito'
		else 'Estado desconocido'
	end 				"Estado débito"
from 
	admin.mides_debitos_total dt, 
	admin.mides_debitos_detalles dd,  
	iposs.locales l
where	dt.emp_codigo = l.emp_codigo
and 	dt.loc_codigo = l.codigo
and 	dd.emp_codigo = l.emp_codigo
and 	dd.loc_codigo = l.codigo
and 	dd.cie_codigo = dt.cie_codigo
and 	dd.acreditado not in (-1)
and 	dt.fecha between :FechaDesde and :FechaHasta
and 	dt.emp_codigo = $EMPRESA_LOGUEADA$

