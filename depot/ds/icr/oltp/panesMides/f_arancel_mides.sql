create or replace function iposs.f_arancel_mides (p_entidad varchar2, p_fecha date) 
return number
is
begin
	if (p_entidad = 'BROU') 
	then 
		if p_fecha < to_date('01/04/2016', 'dd/mm/yyyy')
		then
			return (0.01);
		else
			return (0.015);
		end if;
	else 
		if (p_entidad = 'SCANNTECH')
		then
			if p_fecha < to_date('01/04/2016', 'dd/mm/yyyy')
			then	
				return (0.02);
			else 
				if p_fecha < to_date('01/01/2017', 'dd/mm/yyyy')
				then 
					return (0.015);
				else
					return (0.01);
				end if;
			end if;
		end if;
	end if;
	return (0);
end;
/

grant execute on iposs.f_arancel_mides to iposs_rep;
grant execute on iposs.f_arancel_mides to iposs_fx;