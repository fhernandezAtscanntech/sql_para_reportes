select 
	m.loc_codigo 		"Codigo Local", 
	l.descripcion 		"Local", 
	m.caj_codigo 		"Codigo Caja", 
	c.descripcion 		"Caja", 
	m.fecha_comercial 	"Fecha", 
	m.numero_operacion 	"Numero Operacion", 
	m.fun_codigo_cajera	"Codigo Cajera"	, 
		(select f.apellido||', '||f.nombre from funcionarios f, funcionarios_locales fl 
		where fl.fun_codigo = f.codigo 
		and fl.loc_codigo = m.loc_codigo 
		and f.fun_cod_empresa = m.fun_codigo_cajera)	"Cajera"	, 
	m.total_bonificable 	"Total Bonificable", 
	mo.simbolo		"Moneda", 
	mb.bon_codigo 		"Cod.Bonificacion", 
	mb.descripcion 	 	"Bonificacion", 
	mb.porcentaje 		"Porc. Bonificacion", 
	sum (mb.importe) 		"Importe Bonificacion"
from
	movimientos m,
	movimientos_de_bonificaciones mb,
	monedas mo, 
	locales l,
	cajas c
where
	m.numero_mov = mb.mov_numero_mov
and 	m.emp_codigo = mb.mov_emp_codigo
and 	m.fecha_comercial = mb.fecha_comercial
and
	m.loc_codigo = l.codigo
and
	m.caj_codigo = c.codigo
and 	m.loc_Codigo = c.loc_Codigo
and
	m.mon_codigo = mo.codigo
and 
	l.codigo = c.loc_codigo
and
	:FechaHasta - :FechaDesde <= 31
and
	m.fecha_comercial between :FechaDesde and :FechaHasta
and	
--	(nvl (:Local, l.codigo) = l.codigo)
	( trim (upper (':Local')) = 'NULL' or l.codigo in :Local )
and 
	( m.emp_codigo = $EMPRESA_LOGUEADA$
and 	  mb.mov_emp_codigo = $EMPRESA_LOGUEADA$
and 	  l.emp_codigo = $EMPRESA_LOGUEADA$)
group by 
	m.loc_codigo 		, 
	l.descripcion 		, 
	m.caj_codigo 		, 
	c.descripcion 		, 
	m.fecha_comercial 	, 
	m.numero_operacion 	, 
	m.fun_codigo_cajera	, 
	m.total_bonificable 	, 
	mo.simbolo		, 
	mb.bon_codigo 		, 
	mb.descripcion 	 	, 
	mb.porcentaje 		
/