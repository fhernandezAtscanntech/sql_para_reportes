select 
	m.loc_codigo 		"Codigo Local", 
	l.descripcion 		"Local", 
	m.caj_codigo 		"Codigo Caja", 
	c.descripcion 		"Caja", 
	m.fecha_comercial 	"Fecha", 
	m.numero_operacion 	"Numero Operacion", 
	m.fecha_operacion 	"Fecha Operacion", 
	to_char(m.fecha_operacion, 'hh24:mi:ss') 	"Hora Operacion", 	-- no hay formato de fecha que despliegue la hora a este momento
	m.fun_codigo_cajera	"Codigo Cajera"	, 
	(select f.apellido||', '||f.nombre  from funcionarios f, funcionarios_locales fl where fl.fun_codigo = f.codigo and fl.loc_codigo = l.codigo and f.fun_cod_empresa = m.fun_codigo_cajera)	"Cajera", 
	md.fun_codigo_autoriza 		"Codigo Autoriza", 
	(select f.apellido||', '||f.nombre  from funcionarios f, funcionarios_locales fl where fl.fun_codigo = f.codigo and fl.loc_codigo = l.codigo and f.fun_cod_empresa = md.fun_codigo_autoriza)	"Autoriza", 
	m.tipo_operacion 	"Operacion", 
	td.descripcion 		"Tipo Detalle", 
	(decode (td.signo, '+', md.cantidad, '-', md.cantidad*(-1))) 	"Cantidad", 
	(decode (td.signo, '+', md.importe, '-', md.importe*(-1))) 	"Importe Linea", 
	md.art_codigo 		"Cod.Articulo", 
		(select ae.descripcion 
		from articulos_empresas ae, articulos_locales al 
		where ae.art_codigo = al.art_codigo
		and ae.emp_codigo = al.emp_codigo
		and al.art_codigo_Externo = md.art_codigo
		and al.loc_codigo = md.loc_codigo) "Articulo", 
	md.rub_codigo		"Cod.Rubro", 
	md.descripcion_rubro 	"Rubro", 
	mo.simbolo		"Moneda", 
	m.total 		"Importe"
from 
	movimientos m, 
	movimientos_detalles md, 
	tipos_de_detalles td, 
	monedas mo,
	locales l, 
	cajas c
where
	m.numero_mov = md.mov_numero_mov
and 	m.fecha_comercial = md.fecha_comercial 
and 	m.emp_codigo = md.mov_emp_codigo
and
	m.loc_codigo = l.codigo
and
	m.caj_codigo = c.codigo
and 	m.loc_codigo = c.loc_codigo
and
	m.mon_codigo = mo.codigo
and
	md.tip_det_codigo = td.codigo
and
	m.total < 0
and
	:FechaHasta - :FechaDesde <= 31
and
	m.fecha_comercial between :FechaDesde and :FechaHasta
and	
	( trim (upper (':Local')) = 'NULL' or l.codigo in :Local )
