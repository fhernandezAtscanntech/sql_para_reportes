select 
	e.codigo 			"Cod.Empresa", 
	e.descripcion 		"Empresa", 
	rccc.codigo 		"Cod. Cuenta Corriente", 
	rccc.descripcion 	"Cuenta Corriente", 
	rcd.fecha 		"Fecha Ingreso", 
	rcd.numero_cuenta 	"Nro. Cuenta", 
	rcd.numero_deposito 	"Nro. Dep�sito", 
	rcd.importe 		"Importe", 
	rcd.Fecha_acreditado 	"Fecha Acred.", 
	rcd.observaciones 		"Observaciones", 
	rcd.audit_date 		"Fecha Audit.", 
	rcd.audit_user 		"Usuario Audit.", 
	rcd.modif_date 		"Fecha Modif.", 
	rcd.modif_user 		"Usuario Modif."
from 
	iposs.empresas e, 
	iposs.rc_cuentas_corrientes rccc, 
	iposs.rc_depositos rcd
where 
	( rccc.emp_codigo = e.codigo )
and 	  
	( rccc.codigo = rcd.cue_cor_codigo 
and   rccc.emp_codigo = rcd.emp_codigo )
and 
	( rcd.fecha_acreditado between :FechaDesde and :FechaHasta + 1-1/24/60/60 
and 	  :FechaHasta - :FechaDesde <= 62 ) 
