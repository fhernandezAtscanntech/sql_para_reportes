multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
		select
			e.codigo "Cod. Empresa",
			e.descripcion "Empresa",
			cc.descripcion "Cuenta Corriente",
			cc.saldo "Saldo",
			cc.sobregiro "Sobregiro",
			sum(mcc.importe) "Acumulado Per�odo",
			round(sum(mcc.importe)/:Periodo, 2) "Promedio diario Per�odo",
			round((cc.saldo + cc.sobregiro) / (sum(mcc.importe)/:Periodo), 2) "D�as Estimados"
		from
			iposs.empresas e,
			iposs.rc_cuentas_corrientes cc,
			iposs.rc_movimientos_cc mcc,
			iposs.rc_tipos_movimientos_cc tmcc
		where 	cc.codigo = mcc.cue_cor_codigo
		and 	tmcc.codigo = mcc.tip_mov_cc
		and 	e.codigo = cc.emp_codigo
		and 	mcc.fecha >= sysdate - :Periodo
		and 	tmcc.descripcion not in (''RESERVA POS'', ''CARGA SALDO'')
		group by
			e.codigo,
			e.descripcion,
			cc.descripcion,
			cc.saldo,
			cc.sobregiro
		having
			sum(mcc.importe) > 0
			and (cc.saldo + cc.sobregiro) / (sum(mcc.importe)/:Periodo) <= :Limite
		order by
			sum(mcc.importe)/:Periodo desc' as sql from dual)

######################
tabla
######################
create table $$tabla$$ as (
	select
		e.codigo "Cod. Empresa",
		e.descripcion "Empresa",
		cc.descripcion "Cuenta Corriente",
		cc.saldo "Saldo",
		cc.sobregiro "Sobregiro",
		sum(mcc.importe) "Acumulado Per�odo",
		round(sum(mcc.importe)/:Periodo, 2) "Promedio diario Per�odo",
		round((cc.saldo + cc.sobregiro) / (sum(mcc.importe)/:Periodo), 2) "D�as Estimados"
	from
		iposs.empresas e,
		iposs.rc_cuentas_corrientes cc,
		iposs.rc_movimientos_cc mcc,
		iposs.rc_tipos_movimientos_cc tmcc
	where 
		1 = 2
		group by
			e.codigo,
			e.descripcion,
			cc.descripcion,
			cc.saldo,
			cc.sobregiro)

######################
consulta
######################
select 1 from #rec#


######################
Comentarios:
######################

la consulta original incluia la l�nea 
		and 	tmcc.descripcion not in (''RESERVA POS'', ''CARGA SALDO'')
que fue sustituida por
		and 	(mcc.codigo in (2, 10) and ncl(mcc.estado, 1) = 1)