select 
	l.codigo 		"Loc.Codigo (I)",
	l.loc_Cod_empresa 	"Loc.Codigo", 
	l.descripcion 		"Local", 
	rccc.descripcion 	"Cuenta Corriente", 
	rcmc.numero_mov_cc 	"Numero Mov CC", 
	rcmc.fecha 		"Fecha", 
	rcmc.nro_lote 		"Nro.Lote", 
	rctm.descripcion 	"Tipo Movimiento", 
	rcmc.Compa�ia 		"Compa�ia", 
	sum (rccc.saldo) 	"Saldo", 
	sum ( decode (
		rcmc.tip_mov_cc, 
			1, rcmc.importe_pos*-1, 
			2, 0, 
			5, rcmc.importe_pos*-1, 
			decode ( 
				nvl (rcmc.estado, 0), 
				3, rcmc.importe_pos, 
				rcmc.importe_pos * nvl(rcmc.tip_ope_codigo, 1)) ) ) 	"Importe Pos", 
	sum ( decode (
		rcmc.tip_mov_cc, 
			1, rcmc.importe*-1, 
			2, 0, 
			5, rcmc.importe*-1, 
			decode ( 
				nvl (rcmc.estado, 0), 
				3, rcmc.importe, 
				rcmc.importe * nvl (rcmc.tip_ope_codigo, 1)) ) ) 	"Importe", 
	rccc.sobregiro 		"Sobregiro", 
	rcmc.arancel 		"Arancel"
from 
	iposs.locales l, 
	iposs.rc_cuentas_corrientes rccc, 
	iposs.rc_movimientos_cc rcmc, 
	iposs.rc_tipos_movimientos_cc rctm
where 
	( rccc.codigo = rcmc.cue_cor_codigo )
and 	  
	( rctm.codigo = rcmc.tip_mov_cc )
and 	
	( l.codigo(+) = rcmc.loc_codigo ) 
-- controlo que solamente vea las cc asociadas a los locales que puede ver
and 	  
	( rccc.codigo in 
		(select rcccl.cue_cor_Codigo
		 from iposs.rc_cuentas_corrientes_locales rcccl
		 where rcccl.loc_codigo in :Local ) )
and 
	( rcmc.fecha between :FechaDesde and :FechaHasta + 1-1/24/60/60 
and 	  :FechaHasta - :FechaDesde <= 62 ) 
and 
	( nvl (rcmc.estado, 0) not in ( 2, 4, 5 ) )
and 
	( rccc.emp_codigo = $EMPRESA_LOGUEADA$
and 	  rcmc.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.codigo, 
	l.loc_cod_empresa, 
	l.descripcion, 
	rccc.descripcion, 
	rcmc.numero_mov_cc, 
	rcmc.fecha, 
	rcmc.nro_lote, 
	rctm.descripcion, 
	rcmc.Compa�ia, 
	rccc.sobregiro, 
	rcmc.arancel
order by 
	rcmc.fecha asc, 
	rcmc.numero_mov_cc asc



/*

Null - equiv. 1
* 0 - Pendiente
* 1 - Confirmado
2 - Reversado
* 3 - Anulado
4 - Borrado
5 - Error


0, 'Pendiente'
1, 'Confirmado'
2, 'Reversado'
3, 'Anulado'
4, 'Borrado'
5, 'Error'

*/

