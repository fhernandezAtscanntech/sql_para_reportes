multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
	select
		e.codigo "Cod. Empresa",
		e.descripcion "Empresa",
		cc.descripcion "Cuenta Corriente",
		cc.saldo "Saldo",
		cc.sobregiro "Sobregiro"
	from
		iposs.empresas e,
		iposs.rc_cuentas_corrientes cc
	where 	e.codigo = cc.emp_codigo' as sql from dual)
	
######################
tabla
######################

create table $$tabla$$ as (
	select
		e.codigo "Cod. Empresa",
		e.descripcion "Empresa",
		cc.descripcion "Cuenta Corriente",
		cc.saldo "Saldo",
		cc.sobregiro "Sobregiro"
	from
		iposs.empresas e,
		iposs.rc_cuentas_corrientes cc
	where
		1=2)

######################
consulta
######################
select 1 from #rec#