multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select
	'select 
		emp_codigo, 
		cue_cor_codigo, 
		fecha, 
		fecha_acreditado, 
		numero_deposito, 
		importe 
	from 
		iposs.rc_depositos
	where 	observaciones = '''||'Automático'||'''
	and 	fecha between to_date('''||:FechaDesde||''', '''||'dd/mm/yyyy'||''') and 
			to_date('''||:FechaHasta||''', '''||'dd/mm/yyyy'||''')
	and 	fecha_acreditado is not null
	order by 
		fecha_acreditado desc
' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
select 
		emp_codigo, 
		cue_cor_codigo, 
		fecha, 
		fecha_acreditado, 
		numero_deposito, 
		importe 
	from 
		iposs.rc_depositos
	where 1 = 2
)

######################
consulta
######################
select 1 from #reco#
