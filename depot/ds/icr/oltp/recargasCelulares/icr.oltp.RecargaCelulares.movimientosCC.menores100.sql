select 
	rcmc.fecha 		"Fecha", 
	trunc( rcmc.fecha) 	"Fecha Entera", 
	rctm.descripcion 	"Tipo Movimiento", 
	rcmc.Compa�ia 		"Compa�ia", 
	sum ( decode (
		rcmc.tip_mov_cc, 
			1, rcmc.importe_pos*-1, 
			2, 0, 
			5, rcmc.importe_pos*-1, 
			decode ( 
				nvl (rcmc.estado, 0), 
				3, rcmc.importe_pos, 
				rcmc.importe_pos * nvl(rcmc.tip_ope_codigo, 1)) ) ) 	"Importe Pos", 
	sum ( decode (
		rcmc.tip_mov_cc, 
			1, rcmc.importe*-1, 
			2, 0, 
			5, rcmc.importe*-1, 
			decode ( 
				nvl (rcmc.estado, 0), 
				3, rcmc.importe, 
				rcmc.importe * nvl (rcmc.tip_ope_codigo, 1)) ) ) 	"Importe"
from 
	iposs.rc_movimientos_cc rcmc, 
	iposs.rc_tipos_movimientos_cc rctm
where 
	( rctm.codigo = rcmc.tip_mov_cc )
and 
	( rcmc.fecha between :FechaDesde and :FechaHasta + 1-1/24/60/60 
and 	  :FechaHasta - :FechaDesde <= 62 ) 
and 
	( nvl (rcmc.estado, 0) not in ( 2, 4, 5 ) )
and 
	abs (rcmc.importe_pos) < 100
group by 
	rcmc.fecha, 
	rctm.descripcion, 
	rcmc.Compa�ia
order by 
	rcmc.fecha asc



/*

Null - equiv. 1
* 0 - Pendiente
* 1 - Confirmado
2 - Reversado
* 3 - Anulado
4 - Borrado
5 - Error


0, 'Pendiente'
1, 'Confirmado'
2, 'Reversado'
3, 'Anulado'
4, 'Borrado'
5, 'Error'

*/

