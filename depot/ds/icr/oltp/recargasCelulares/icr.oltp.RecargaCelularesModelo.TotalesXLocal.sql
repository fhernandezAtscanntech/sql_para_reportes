select 
	e.descripcion 		"Empresa", 
	l.codigo 		"Loc.C�digo (I)",
	l.loc_Cod_empresa 	"Loc.C�digo", 
	l.descripcion 		"Local", 
	rccc.codigo 		"CC C�digo", 
	rccc.descripcion 	"Cuenta Corriente", 
	rcmc.compa�ia 		"Compa��a", 
	trunc(rcmc.fecha) 	"Fecha", 
	sum ( decode (
		rcmc.tip_mov_cc, 
			1, nvl (rcmc.importe_pos_mon, rcmc.importe_pos), 
			2, 0, 
			4, -nvl (rcmc.importe_pos_mon, rcmc.importe_pos), 
			5, nvl (rcmc.importe_pos_mon, rcmc.importe_pos), 
			decode ( 
				nvl (rcmc.estado, 0), 
				3, nvl (rcmc.importe_pos_mon, rcmc.importe_pos) * (nvl (rcmc.tip_ope_codigo, 1)), 
				nvl (rcmc.importe_pos_mon, rcmc.importe_pos)) ) ) 	"Importe Ticket", 
	sum ( decode (
		rcmc.tip_mov_cc, 
			1, rcmc.importe, 
			2, 0, 
			4, -rcmc.importe, 
			5, rcmc.importe, 
			decode ( 
				nvl (rcmc.estado, 0), 
				3, rcmc.importe * (nvl (rcmc.tip_ope_codigo, 1)), 
				rcmc.importe) ) ) 	"Importe S.Comisi�n"
from 
	iposs.empresas e, 
	iposs.locales l, 
	iposs.rc_cuentas_corrientes rccc, 
	iposs.rc_movimientos_cc rcmc, 
	iposs.rc_tipos_movimientos_cc rctm, 
	iposs.sa_recargas sar
where 	( l.emp_codigo = e.codigo )
and 	( rccc.codigo = rcmc.cue_cor_codigo )
and 	( rctm.codigo = rcmc.tip_mov_cc )
and 	( l.codigo(+) = rcmc.loc_codigo ) 
and 	  
-- 	original: RC_TIPOS_MOVIMIENTOS_CC.DESCRIPCION IN ('RECARGA CEL','ANULACION','ANULACION AUTOMATICA'))
	( rctm.codigo in (1, 4, 5, 10) ) 
and 
	( nvl (rcmc.estado, 0) not in ( 2, 4, 5 ) )
and 
	( rcmc.fecha between :FechaDesde and :FechaHasta + 1-1/24/60/60 
and 	  :FechaHasta - :FechaDesde <= 62 ) 
and 	( rcmc.importe <> 0 and rcmc.importe_pos <> 0 ) 
and 	rcmc.con_mov_codigo = sar.con_mov_caj_codigo (+)
and 	nvl (sar.recargada, 1) in (1, 10, 11, 20, 21)
group by 
	e.descripcion, 
	l.codigo, 
	l.loc_Cod_empresa, 
	l.descripcion, 
	rccc.codigo, 
	rccc.descripcion, 
	rcmc.compa�ia, 
	trunc(rcmc.fecha)