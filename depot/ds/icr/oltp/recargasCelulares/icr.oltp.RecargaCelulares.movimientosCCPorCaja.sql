select 
	l.codigo 		"Loc.Codigo (I)",
	l.loc_Cod_empresa 	"Loc.Codigo", 
	l.descripcion 		"Local", 
	c.descripcion 		"Caja", 
	rcmc.numero_mov_cc 	"Numero Mov CC", 
	rcmc.nro_lote 		"Nro.Lote", 
	to_char (rcmc.fecha,'dd/mm/yyyy') 	"Fecha", 
	case rcmc.compa�ia 
		when 'ANC' then
			(select substr (recan.numero_tel, length(recan.numero_tel) - 3)
			from iposs.antel_recargas recan
			where recan.con_mov_codigo = rcmc.con_mov_codigo)
		else substr (rcmc.nro_celular, length (rcmc.nro_celular) - 3) 	
	end "Celular", 
	rcmc.compa�ia 		"Compa�ia", 
	rctm.descripcion 	"Tipo Movimiento", 
	sum ( decode (
		rcmc.tip_mov_cc, 
			1, rcmc.importe_pos*-1, 
			2, 0, 
			5, rcmc.importe_pos*-1, 
			decode ( 
				nvl (rcmc.estado, 0), 
				3, rcmc.importe_pos, 
				rcmc.importe_pos * nvl (rcmc.tip_ope_codigo, 1)) ) ) 	"Importe Pos", 
	sum ( decode (
		rcmc.tip_mov_cc, 
			1, rcmc.importe*-1, 
			2, 0, 
			5, rcmc.importe*-1, 
			decode ( 
				nvl (rcmc.estado, 0), 
				3, rcmc.importe, 
				rcmc.importe * nvl(rcmc.tip_ope_codigo, 1)) ) ) 	"Importe"
from 
	iposs.cajas c, 
	iposs.locales l, 
	iposs.rc_movimientos_cc rcmc, 
	iposs.rc_tipos_movimientos_cc rctm
where 
	( rctm.codigo = rcmc.tip_mov_cc ) 
and 
	( l.codigo(+) = rcmc.loc_codigo ) 
and 
	( c.codigo = rcmc.caj_codigo(+) 
and 	  c.loc_codigo = rcmc.loc_codigo(+) )
and 
-- 	original: RC_TIPOS_MOVIMIENTOS_CC.DESCRIPCION NOT IN ('CARGA SALDO','RECARGA CEL')
	( rctm.codigo not in (3, 2) ) 
and 
	( nvl (rcmc.estado, 0) not in ( 2, 4, 5 ) )
and
	( rcmc.fecha(+) between :FechaDesde and :FechaHasta + 1-1/24/60/60 
and 	  :FechaHasta - :FechaDesde <= 62 ) 
and 
	( rcmc.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  l.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.codigo 		,
	l.loc_Cod_empresa 	, 
	l.descripcion 		, 
	c.descripcion 		, 
	rcmc.numero_mov_cc 	, 
	rcmc.nro_lote 		, 
	rcmc.con_mov_codigo ,
	to_char (rcmc.fecha,'dd/mm/yyyy') , 
	substr (rcmc.nro_celular, length (rcmc.nro_celular) - 3) , 
	rcmc.compa�ia 		, 
	rctm.descripcion 	 
order by 
	c.descripcion asc, 
	rcmc.numero_mov_cc asc
