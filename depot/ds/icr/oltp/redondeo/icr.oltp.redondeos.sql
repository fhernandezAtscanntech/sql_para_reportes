select  
	c.descripcion 		"Caja", 
	l.descripcion 		"Local", 
	m.fecha_comercial 	"Fecha Comercia", 
	sum(m.redondeo) 	"Redondeo", 
	sum(m.total) 		"Total"
from 
	iposs.cajas c,
	iposs.locales l,
	iposs.movimientos m
where 	c.codigo = m.caj_codigo and c.loc_codigo = m.caj_loc_codigo
and 	l.codigo = m.loc_codigo 
and 	m.tipo_operacion = 'VENTA')
and 	m.fecha_comercial between :FechaDesde and :FechaHasta
and 	:FechaHasta - :FechaDesde <= 31
and 	l.codigo in :Local
and 	l.emp_codigo = $EMPRESA_LOGUEADA$
and 	m.emp_codigo = $EMPRESA_LOGUEADA$
group by 
	c.descripcion, 
	l.descripcion, 
	m.fecha_comercial
