select 
	l.codigo	"Codigo Local", 
	l.descripcion	"Local", 
	m.caj_codigo	"Codigo Caja", 
	c.descripcion	"Caja", 
	m.fecha_comercial	"Fecha", 
	m.hora_operacion	"Hora", 
	mo.Simbolo	"Moneda", 
	m.numero_operacion 	"Numero Operacion", 
	mp.numero_autorizacion_chr 	"Numero Op. Original", 
	td.descripcion 		"Tipo Detalle", 
	mdd.rub_codigo	"Rub.Codigo", 
		(select r.descripcion 
		from rubros r, rubros_locales rl 
		where r.codigo = rl.rub_codigo and 
		rl.loc_codigo = l.codigo and 
		rl.rub_Codigo_Externo = mdd.rub_codigo)   "Rubro",
	mdd.art_codigo 	"Art.Codigo", 
		(select ae.descripcion  
		from articulos_empresas ae, articulos_locales al 
		where ae.art_codigo = al.art_codigo and 
		ae.emp_codigo = al.emp_codigo and 
		al.loc_codigo = l.codigo and 
		al.art_Codigo_Externo = mdd.art_codigo) 	"Articulo",
	m.fun_codigo_cajera	"Codigo Cajera"	, 
		(select f.apellido||', '||f.nombre  
		from funcionarios f, funcionarios_locales fl 
		where fl.fun_codigo = f.codigo and 
		fl.loc_codigo = l.codigo and 
		f.fun_cod_empresa = m.fun_codigo_cajera)	"Cajera", 
	mdd.fun_codigo_autoriza 		"Codigo Autoriza", 
		(select f.apellido||', '||f.nombre  
		from funcionarios f, funcionarios_locales fl 
		where fl.fun_codigo = f.codigo and 
		fl.loc_codigo = l.codigo and 
		f.fun_cod_empresa = mdd.fun_codigo_autoriza)	"Autoriza", 
	sum (mdd.cantidad) "Cantidad", 
	sum (mdd.importe) "Importe"
from 	iposs.movimientos m
join 	iposs.movimientos_Det_devs mdd
	on 	m.numero_mov = mdd.mov_numero_mov
	and m.emp_codigo = mdd.mov_emp_codigo
	and m.fecha_comercial = mdd.fecha_comercial
	and mdd.tip_det_codigo in (5, 7, 18) 		-- solo devoluciones, no auditoría
left join iposs.movimientos_de_pagos mp
	on 	m.numero_mov = mp.mov_numero_mov
	and m.emp_codigo = mp.mov_emp_codigo
	and m.fecha_comercial = mp.fecha_comercial
	and mp.tip_pag_codigo = 12
	and mp.val_codigo = to_number(iposs.f_param ('CFE_COD_VALE', mp.loc_codigo))
join 	iposs.tipos_de_detalles td
	on 	td.codigo = mdd.tip_det_codigo
join 	iposs.monedas mo 
	on 	m.mon_codigo = mo.codigo
join 	iposs.locales l
	on 	m.loc_codigo = l.codigo
join 	iposs.cajas c
	on 	m.caj_codigo = c.codigo
	and m.loc_Codigo = c.loc_Codigo
where
		m.tipo_operacion = 'VENTA'
and 	m.fecha_comercial between :FechaDesde and :FechaHasta
and	
--	(nvl (:Local, l.codigo) = l.codigo)
	( l.codigo in :Local )
and	
	m.emp_codigo = $EMPRESA_LOGUEADA$
and mdd.mov_emp_codigo = $EMPRESA_LOGUEADA$
and l.emp_codigo = $EMPRESA_LOGUEADA$
group by 
	l.codigo, 
	l.descripcion, 
	m.caj_codigo, 
	c.descripcion, 
	m.fecha_comercial, 
	m.hora_operacion, 
	mo.Simbolo	, 
	m.numero_operacion, 
	mp.numero_autorizacion_chr, 
	td.descripcion, 
	mdd.rub_codigo	, 
	mdd.art_codigo 	, 
	m.fun_codigo_cajera, 
	mdd.fun_codigo_autoriza
