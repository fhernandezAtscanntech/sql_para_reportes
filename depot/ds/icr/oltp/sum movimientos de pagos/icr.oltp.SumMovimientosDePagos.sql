select 		
	sum.fecha_comercial	"Fecha"	, 
	sum.mov_emp_codigo	"Codigo Empresa"	, 
	e.descripcion	"Empresa"	, 
	sum.loc_codigo	"Codigo Local"	, 
	l.descripcion	"Local"	, 
	sum.caj_codigo	"Codigo Caja"	, 
	c.descripcion	"Caja"	, 
	sum.fun_codigo_cajera	"Codigo Cajera"	, 
	(select f.apellido||', '||f.nombre from funcionarios f, funcionarios_locales fl where fl.fun_codigo = f.codigo and fl.loc_codigo = sum.loc_codigo and f.fun_cod_empresa = sum.fun_codigo_cajera)	"Cajera"	, 
	sum.tip_pag_codigo	"Codigo Tipo de Pago"	, 
	(select t.descripcion from tipos_de_pagos t where t.codigo = sum.tip_pag_codigo)	"Tipo De Pago"	, 
	nvl ((select c.descripcion from creditos c, creditos_locales cl where c.codigo = cl.cre_codigo and cl.cre_codigo_externo = sum.cre_codigo and cl.loc_codigo = sum.loc_codigo), 
		nvl ((select v.descripcion from vales_de_compras v, vales_de_compras_locales vl where v.codigo = vl.val_codigo and vl.val_codigo_Externo = sum.val_codigo and vl.loc_codigo = sum.loc_codigo), 
			nvl ((select b.descripcion from bancos b where b.codigo = sum.ban_codigo), ''))) "Sub tipo", 
	sum.mon_codigo	"Codigo Moneda", 
	m.simbolo 	"Moneda", 
	sum.Importe	"Importe"	, 
	(sum.importe_pago * sum.cotiza_compra) 	"Importe MN", 
	sum.cotiza_venta	"Cotiza Venta"	, 
	sum.cotiza_compra	"Cotiza Compra"	, 
	sum.importe_pago	"Importe Pago"	, 
	sum.ban_codigo	"Codigo Banco"	, 
	sum.val_codigo	"Codigo Vale"	, 
	sum.cre_codigo	"Codigo Credito"	, 
	sum.lote_credito	"Lote Credito"	, 
	sum.comercio_credito	"Comercio Credito"	, 
	sum.terminal_credito	"Terminal Credito"	, 
	sum.cantidad_pagos	"Cantidad Pagos"	, 
	sum.nro_cierre	"Num Cierre"	
from 		
	sum_movimientos_de_pagos sum, 	
	empresas e,	
	locales l,	
	cajas c, 
	monedas m
where		
	sum.mov_emp_codigo = e.codigo	
and	sum.loc_Codigo = l.codigo	
and	sum.caj_codigo = c.codigo	
and	sum.loc_Codigo = c.loc_Codigo	
and	sum.mon_codigo = m.codigo
and 
	fecha_comercial between :FechaDesde and :FechaHasta
and	
--	(nvl (:Local, l.codigo) = l.codigo)
	( trim (upper (':Local')) = 'NULL' or l.codigo in :Local ) 
and
	(l.emp_codigo = $EMPRESA_LOGUEADA$ and
	sum.mov_emp_codigo = $EMPRESA_LOGUEADA$)
