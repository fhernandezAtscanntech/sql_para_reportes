select 
	ae.art_codigo_externo 	"Art.Codigo",
	ae.descripcion 	"Articulo",
	l.descripcion 	"Local",
	imd.fecha_comercial 	"Fecha",
	im.observaciones 	"Observaciones", 
	imd.codigo_barras 	"C�digo Barra", 
	imd.aj_nueva_cantidad 	"Nueva Cantidad"
,
	imd.aj_diferencia 	"Diferencia", 
	imd.aj_stock_final 	"Stock Final", 
	nvl ((select pc.costo_imp
		from precios_de_costos_articulos pc
		where pc.art_codigo = ae.art_codigo
		and pc.loc_Codigo = l.codigo
		and pc.emp_codigo = ae.emp_codigo
		and im.fecha between pc.vigencia_desde and pc.vigencia_hasta), 0) "Costo Imp"
from 
	iposs.articulos_empresas ae, 
	iposs.locales l, 
	iposs.inv_movimientos im, 
	iposs.inv_movimientos_detalles imd
where 
	( ( im.fecha_comercial = imd.fecha_comercial 
and 	    im.emp_codigo = imd.emp_codigo 
and 	    im.numero = imd.inv_mov_numero ) 
and 
	  ( l.codigo = im.loc_codigo_origen ) 
and 
	  ( ae.art_codigo = imd.art_codigo 
and 	    ae.emp_codigo = imd.emp_codigo ) ) 
and 
	( im.inv_tip_mov_codigo in (4) ) 
and 
	( im.fecha_comercial between :FechaDesde and :FechaHasta ) 
and 
	( trim (upper (':Local')) = 'NULL' or l.codigo in :Local ) 
and
	( ae.emp_codigo = $EMPRESA_LOGUEADA$
and 	  l.emp_CODIGO =  $EMPRESA_LOGUEADA$
and 	  im.emp_codigo =  $EMPRESA_LOGUEADA$
and 	  imd.emp_codigo =  $EMPRESA_LOGUEADA$ )
order by 
	imd.fecha_comercial asc, 
	ae.art_codigo_Externo