select 
	ae.art_codigo 	"Art.Codigo(I)", 
	ae.art_codigo_externo 	"Art.Codigo", 
	ae.descripcion 	"Articulo", 
	r.descripcion 	"Rubro", 
	(select cb.codigo_barra 
		from iposs.codigos_barras cb 
		where cb.art_codigo = ae.art_codigo 
		and (cb.emp_Codigo = ae.emp_codigo or (cb.emp_codigo = 1 and ae.veo_barras_modelo = 1)) 
		and rownum = 1) "Codigo Barra", 
	d.codigo 	"Cod.Deposito", 
	d.descripcion 	"Deposito", 
	l.codigo 	"Loc.Codigo(I)", 
	l.loc_Cod_empresa 	"Loc.Codigo", 
	l.descripcion 	"Local", 
	sum(s.stock) 	"Stock"
from 
	iposs.articulos_empresas ae, 
	iposs.articulos_locales al, 
	iposs.rubros r, 
	iposs.depositos d, 
	iposs.locales l, 
	iposs.stocks s
where 
	-- Transcripto de Discoverer
	( ( d.loc_codigo = s.dep_loc_codigo and 
	    d.codigo = s.dep_codigo ) and 
	  ( l.codigo = d.loc_codigo ) and 
	  ( s.art_codigo = ae.art_codigo ) and 
	  ( ae.emp_codigo = al.emp_codigo and 
	    ae.art_codigo = al.art_codigo ) and 
	  ( al.art_codigo = s.art_codigo and 
	    al.dim_con_numero = s.dim_con_numero and 
	    al.loc_codigo = s.dep_loc_codigo ) and 
	  ( l.codigo = al.loc_codigo ) and 
	  ( r.codigo = ae.rub_codigo ) ) and
	( ae.emp_codigo <> 1 ) and
	( ae.est_codigo <> 2 ) and 
-- 	( al.fecha_borrado is null ) and
	( al.fecha_borrado is null or ( al.fecha_borrado is not null and s.stock <> 0 )) and
	( l.codigo in :Local ) and 
	( trim (upper (':Rubro')) = 'NULL' or r.codigo in :Rubro ) and 
	( :FechaStock between s.fecha and s.vigencia_hasta ) and
	ae.emp_codigo = $EMPRESA_LOGUEADA$ and
	al.emp_codigo = $EMPRESA_LOGUEADA$ and
	l.emp_codigo = $EMPRESA_LOGUEADA$
group by 
	ae.art_codigo, 
	ae.art_codigo_externo, 
	ae.descripcion, 
	ae.emp_codigo, 
	ae.veo_barras_modelo, 
	r.descripcion, 
	d.codigo, 
	d.descripcion, 
	l.codigo, 
	l.loc_Cod_empresa, 
	l.descripcion 
