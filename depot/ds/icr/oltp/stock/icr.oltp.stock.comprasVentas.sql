select 
	ae.art_codigo_externo 	"Cod.Artículo",
	ae.descripcion 		"Articulo",
	r.descripcion 		"Rubro", 
	(select f.descripcion 	
		from iposs.familias f
		where ae.fam_codigo = f.codigo) 	"Familia", 
	l.loc_cod_empresa 		"Cod.Local", 
	l.descripcion 		"Local", 
	d.codigo 		"Cod. Depósito", 
	d.descripcion 	"Depósito", 
--	(select s.stock
--		from iposs.stocks s
--		where s.art_codigo = al.art_codigo
--		and s.dep_loc_codigo = al.loc_codigo
--		and s.dep_loc_codigo = d.loc_codigo
--		and s.dep_codigo = d.codigo
--		and sysdate between s.fecha and s.vigencia_hasta
--		and s.vigencia_hasta > sysdate) 		"Stock Actual", 
--	(select sum(decode (d.mov_inv_tip_mov_in_codigo, 1, nvl(d.cant_destino, 0), 11, nvl (-d.cantidad_recibida, 0), 0))
--		from iposs.movimientos_inv_Detalles d
--		where d.mov_inv_emp_codigo = al.emp_codigo
--		and d.mov_inv_loc_codigo = al.loc_Codigo
--		and d.mov_inv_tip_mov_in_codigo in (1, 11)
--		and d.art_codigo = al.art_codigo
--		and d.fecha_comercial between :FechaDesde and :FechaHasta) 		"Compras", 
	(select sum(decode (d.mov_inv_tip_mov_in_codigo, 1, nvl(d.cant_destino*d.costo_unitario*c.valor, 0), 11, nvl (-d.cantidad_recibida*d.costo_unitario*c.valor, 0), 0))
		from iposs.movimientos_inv_Detalles d, iposs.movimientos_inventarios m, cambios c
		where m.numero = d.mov_inv_numero
		and m.emp_codigo = d.mov_inv_emp_codigo
		and m.fecha_comercial = d.fecha_comercial
		and d.mov_inv_emp_codigo = al.emp_codigo
		and d.mov_inv_loc_codigo = al.loc_Codigo
		and d.mov_inv_tip_mov_in_codigo in (1, 11)
		and d.art_codigo = al.art_codigo
		and m.mon_codigo = c.mon_codigo
		and m.fecha_comercial between c.fecha and c.vigencia_hasta
		and d.fecha_comercial between :FechaDesde and :FechaHasta) 		"Monto Compras", 		
--	(select sum(decode (d.mov_inv_tip_mov_in_codigo, 6, nvl(d.cantidad_origen, 0), 8, nvl (-d.cant_destino, 0), 0))
--		from iposs.movimientos_inv_Detalles d
--		where d.mov_inv_emp_codigo = al.emp_codigo
--		and d.mov_inv_loc_codigo = al.loc_Codigo
--		and d.mov_inv_tip_mov_in_codigo in (6, 8)
--		and d.art_codigo = al.art_codigo
--		and d.fecha_comercial between :FechaDesde and :FechaHasta) 		"Ventas", 
-- 	(select sum(nvl(d.importe_venta, 0))
--		from iposs.movimientos_inv_Detalles d
--		where d.mov_inv_emp_codigo = al.emp_codigo
--		and d.mov_inv_loc_codigo = al.loc_Codigo
--		and d.mov_inv_tip_mov_in_codigo in (6, 8)
--		and d.art_codigo = al.art_codigo
--		and d.fecha_comercial between :FechaDesde and :FechaHasta) 		"Monto Ventas"
--	lamentablemente, en la movimientos_inv_Detalles se acumulan igual las ventas en pesos que las ventas en dolares
--	(select sum (md.importe * m.cotiza_compra * (decode (md.tip_det_codigo, 4, 1, 5, -1, 0)))
--		from iposs.movimientos m, iposs.movimientos_detalles md
--		where m.numero_mov = md.mov_numero_mov
--		and m.fecha_comercial = md.fecha_comercial
--		and m.emp_codigo = md.mov_emp_codigo
--		and md.art_codigo = al.art_codigo_externo
--		and md.loc_Codigo = al.loc_Codigo
--		and md.tip_det_codigo in (4, 5)
--		and m.fecha_comercial between :FechaDesde and :FechaHasta) 		"Monto Ventas"
	(select sum (nvl (ven_nac_importe, 0)) - sum (nvl (dev_nac_importe))
		from dwm_prod.f_ventas f, dwm_prod.l_ubicaciones u, dwm_prod.l_productos p
		where f.l_ubi_codigo = u.codigo
		and f.l_pro_codigo = p.codigo
		and u.emp_codigo = al.emp_codigo
		and u.loc_codigo = al.loc_Codigo
		and p.prod_cod_interno = al.art_codigo
		and p.emp_codigo = al.emp_codigo
		and f.fecha_comercial between :FechaDesde and :FechaHasta) 		"Monto Venta"
from 
	iposs.articulos_empresas ae, 
	iposs.articulos_locales al, 
	iposs.rubros r, 
	iposs.locales l
-- 	iposs.depositos d
where
	ae.art_codigo = al.art_codigo
and ae.emp_codigo = al.emp_codigo
and 
	al.loc_codigo = l.codigo
-- and 
-- 	d.loc_codigo = l.codigo
and 
	ae.rub_codigo = r.codigo
and
	( trim (upper (':Proveedor')) = 'NULL' or ae.art_codigo in 
		(select pa.art_codigo
		from iposs.proveedores_articulos pa, articulos_locales_proveedores alp
		where alp.art_loc_art_codigo = pa.art_codigo
		and alp.prov_codigo = pa.prov_codigo
		and alp.emp_codigo = pa.emp_codigo
		and alp.art_loc_loc_codigo = al.loc_codigo
		and pa.emp_codigo = al.emp_codigo
		and pa.fecha_borrado is null
		and alp.fecha_borrado is null
		and pa.prov_codigo in :Proveedor))
and 
	( trim (upper (':Rubro')) = 'NULL' or ae.rub_codigo in :Rubro)
and 
	l.codigo in :Local
and 
	:FechaHasta - :FechaDesde <= 31
and
	ae.emp_codigo = $EMPRESA_LOGUEADA$
and l.emp_codigo = $EMPRESA_LOGUEADA$
