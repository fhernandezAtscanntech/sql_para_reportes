select 
	ae.art_codigo 	"Art.Codigo(I)", 
	ae.art_codigo_externo 	"Art.Codigo", 
	ae.descripcion 	 "Articulo", 
	r.descripcion 	 "Rubro", 
	d.codigo 	 "Cod.Deposito", 
	d.descripcion 	 "Deposito", 
	l.codigo 	"Loc.Codigo(I)", 
	l.loc_Cod_empresa 	 "Loc.Codigo", 
	l.descripcion 	 "Local", 
	lpv.descripcion 	 "Lista PV", 
	pv.precio 	 "Precio", 
	s.stock 	 "Stock"
from 	iposs.articulos_empresas ae
join 	iposs.articulos_locales al
	on 		al.emp_codigo = ae.emp_codigo
	and 	al.art_codigo = ae.art_codigo
	and 	al.loc_codigo in :Local
join 	iposs.rubros r
	on 		r.codigo = ae.rub_codigo
join 	iposs.locales l
	on 		l.codigo = al.loc_codigo
join 	iposs.depositos d
	on 		d.loc_codigo = l.codigo
join 	iposs.listas_de_precios_ventas lpv
	on 		ae.emp_codigo = lpv.emp_codigo
	and 	lpv.tipo_lista is null
join 	iposs.precios_de_articulos_ventas pv
	on 		pv.art_codigo = al.art_codigo
	and 	pv.loc_lis_loc_codigo = al.loc_codigo
	and 	pv.loc_lis_lis_pre_ve_codigo = lpv.codigo
	and 	pv.emp_codigo = al.emp_codigo
	and 	pv.vigencia_hasta >= :FechaStock
	and 	pv.vigencia <= :FechaStock
join 	iposs.stocks s
	on 		s.art_codigo = al.art_codigo
	and 	s.dep_loc_codigo = al.loc_codigo
	and 	s.dep_codigo = d.codigo
	and 	s.dim_con_numero = al.dim_con_numero
	and 	s.vigencia_hasta >= :FechaStock
	and 	s.fecha <= :FechaStock
where 
	( ae.est_codigo <> 2 ) and 
	( ae.emp_codigo <> 1 ) and
	( al.fecha_borrado is null or ( al.fecha_borrado is not null and s.stock <> 0 )) and
	( l.codigo in :Local ) and 
	( trim (upper (':Rubro')) = 'NULL' or r.codigo in :Rubro ) and 
	ae.emp_codigo = $EMPRESA_LOGUEADA$ and
	al.emp_codigo = $EMPRESA_LOGUEADA$ and
	l.emp_codigo = $EMPRESA_LOGUEADA$ and
	pv.emp_codigo = $EMPRESA_LOGUEADA$ 
