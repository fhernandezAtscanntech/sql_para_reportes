select 
	s.art_codigo  			"Art.Codigo(I)", 
	s.art_codigo_externo 	"Art.Codigo", 
	s.art_descripcion 		"Articulo", 
	s.rub_descripcion 		"Rubro", 
	s.fam_descripcion 		"Familia", 
	s.codigo_barra 			"Codigo Barra", 
	s.dep_codigo 			"Cod.Deposito", 
	s.dep_descripcion 		"Deposito", 
	s.loc_codigo 			"Loc.Codigo(I)", 
	s.loc_cod_empresa 		"Loc.Codigo", 
	s.loc_descripcion 		"Local", 
	sum (s.stock) 			"Stock", 
	sum (s.stock * s.costo_c_imp) 	"Costo c.Imp", 
	sum (s.stock * s.costo_s_imp) 	"Costo s.Imp", 
	sum (s.stock * s.venta) 		"Venta"
from 
	(select /*+ INDEX (AL ART_LOC_PK) FULL (AE) INDEX (S STK_VIG_I) */
		ae.art_codigo 	art_codigo, 
		ae.art_codigo_externo 	art_codigo_externo, 
		ae.descripcion 	art_descripcion, 
		r.descripcion 	rub_descripcion, 
		f.descripcion 	fam_descripcion, 
		(select cb.codigo_barra 
			from iposs.codigos_barras cb 
			where cb.art_codigo = ae.art_codigo 
			and (cb.emp_Codigo = ae.emp_codigo or (cb.emp_codigo = 1 and ae.veo_barras_modelo = 1)) 
			and rownum = 1) codigo_barra, 
		d.codigo 	dep_codigo, 
		d.descripcion 	dep_descripcion, 
		l.codigo 	loc_codigo, 
		l.loc_Cod_empresa 	loc_cod_empresa, 
		l.descripcion 	loc_descripcion, 
		sum(s.stock) 	Stock, 
		(select decode (pc.mon_codigo, 
				(select valor from parametros where codigo = 'MONEDA_DEF'), pc.costo_imp, 
				pc.costo_imp * (select c.valor 
								from cambios c 
								where c.mon_codigo = pc.mon_codigo
								and nvl (:FechaStock, sysdate) between c.fecha and c.vigencia_hasta
								and c.emp_codigo = $EMPRESA_LOGUEADA$))
			from precios_de_costos_articulos pc
			where pc.loc_codigo = l.codigo
			and pc.art_codigo = ae.art_codigo
			and nvl (:FechaStock, sysdate) between pc.vigencia_desde and pc.vigencia_hasta
			and pc.emp_codigo = $EMPRESA_LOGUEADA$ 
			and rownum = 1 ) 	costo_c_imp, 	
		(select decode (pc.mon_codigo, 
				(select valor from parametros where codigo = 'MONEDA_DEF'), pc.costo_ultimo,  
				pc.costo_ultimo * (select c.valor 
									from cambios c 
									where c.mon_codigo = pc.mon_codigo
									and nvl (:FechaStock, sysdate) between c.fecha and c.vigencia_hasta
									and c.emp_codigo = $EMPRESA_LOGUEADA$))
			from precios_de_costos_articulos pc
			where pc.loc_codigo = l.codigo
			and pc.art_codigo = ae.art_codigo
			and nvl (:FechaStock, sysdate) between pc.vigencia_desde and pc.vigencia_hasta
			and pc.emp_codigo = $EMPRESA_LOGUEADA$ 
			and rownum = 1 ) 	costo_s_imp, 
		(select decode (pv.mon_codigo, 
				(select  valor from parametros where codigo = 'MONEDA_DEF'), pv.precio, 
				pv.precio * (select c.valor 
							from cambios c 
							where c.mon_codigo = pv.mon_codigo
							and nvl (:FechaStock, sysdate) between c.fecha and c.vigencia_hasta
							and c.emp_codigo = $EMPRESA_LOGUEADA$))
			from precios_de_articulos_ventas pv
			where pv.loc_lis_lis_pre_ve_codigo = (select valor from parametros_empresas where par_codigo = 'LIS_PRE_VE_DEF' and emp_codigo = $EMPRESA_LOGUEADA$)
			and pv.loc_lis_loc_codigo = al.loc_codigo
			and pv.art_codigo = al.art_codigo
			and pv.emp_codigo = $EMPRESA_LOGUEADA$
			and nvl (:FechaStock, sysdate) between pv.vigencia and pv.vigencia_hasta) Venta
	from 
		iposs.articulos_empresas ae, 
		iposs.articulos_locales al, 
		iposs.rubros r, 
		iposs.familias f, 
		iposs.depositos d, 
		iposs.locales l, 
		iposs.stocks s
	where 
		-- Transcripto de Dis
		( ( d.loc_codigo = s.dep_loc_codigo and 
			d.codigo = s.dep_codigo ) and 
		  ( l.codigo = d.loc_codigo ) and 
		  ( s.art_codigo = ae.art_codigo ) and 
		  ( ae.emp_codigo = al.emp_codigo and 
			ae.art_codigo = al.art_codigo ) and 
		  ( al.art_codigo = s.art_codigo and 
-- 			al.dim_con_numero = s.dim_con_numero and 
			al.loc_codigo = s.dep_loc_codigo ) and 
		  ( l.codigo = al.loc_codigo ) and 
		  ( r.codigo = ae.rub_codigo ) and 
		  ( ae.fam_codigo = f.codigo (+) ) ) and
		( ae.emp_codigo <> 1 ) and
		( ae.est_codigo <> 2 ) and 
	-- 	( al.fecha_borrado is null ) and
		( :VerBorrados = 1 and (( al.fecha_borrado is null) or ( al.fecha_borrado is not null and s.stock <> 0 ))
		or (:VerBorrados = 0 and al.fecha_borrado is null)) and
-- 		( :VerStock0 = 1 or (:VerStock0 = 0 and s.stock <> 0) ) and 
		( l.codigo in :Local ) and 
		( trim (upper (':Rubro')) = 'NULL' or r.codigo in :Rubro ) and 
		( nvl (:FechaStock, sysdate) between s.fecha and s.vigencia_hasta ) and
		ae.emp_codigo = $EMPRESA_LOGUEADA$ and
		al.emp_codigo = $EMPRESA_LOGUEADA$ and
		l.emp_codigo = $EMPRESA_LOGUEADA$
	group by 
		ae.art_codigo, 
		ae.art_codigo_externo, 
		ae.descripcion, 
		ae.emp_codigo, 
		ae.veo_barras_modelo, 
		al.loc_codigo, 
		al.art_codigo, 
		r.descripcion, 
		f.descripcion, 
		d.codigo, 
		d.descripcion, 
		l.codigo, 
		l.loc_Cod_empresa, 
		l.descripcion 
	) s
group by 
	s.art_codigo, 
	s.art_codigo_externo, 
	s.art_descripcion, 
	s.rub_descripcion, 
	s.fam_descripcion, 
	s.codigo_barra, 
	s.dep_codigo, 
	s.dep_descripcion, 
	s.loc_codigo, 
	s.loc_cod_empresa, 
	s.loc_descripcion
