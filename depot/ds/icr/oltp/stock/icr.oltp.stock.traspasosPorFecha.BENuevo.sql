select 		
	ae.art_codigo_externo 	"Cod.Art�culo",
	ae.descripcion 	"Art�culo", 
	i.descripcion 	"Tipo IVA", 
	r.rub_Cod_empresa 	"Cod Rubro", 
	r.descripcion 		"Rubro", 
	dep_ori.descripcion 	"Dep�sito Origen", 
	dep_des.descripcion 	"Dep�sito Destino", 
	tmi.descripcion 	"Tipo Movimiento", 
	mi.fecha 	"Fecha", 
	mi.numero_documento 	"Numero", 
	mi.observaciones 		"Observaciones", 
-- 	mi.serie_documento 	"Serie", 
--	mid.costo_unitario 	"Costo Imp.", 
--	mid.costo_original 	"Costo s.Imp.", 
	mid.costo_imp 	"Precio Unitario",
	mid.costo 		"Costo", 
	sum(mid.cantidad_origen*mid.unidades) 	"Cantidad", 
--	sum(mid.cantidad_recibida * mid.costo_unitario) 	"Total Imp.", 
--	sum(mid.cantidad_recibida * mid.costo_original) 	"Total s.Imp.", 
	sum(mid.costo_imp * mid.cantidad_origen * mid.unidades) "Total", 	
	mi.total 	"Total Traspaso"
from 		
	iposs.articulos_empresas ae, 	
	iposs.ivas i, 
	iposs.rubros r, 
	iposs.depositos dep_ori, 	
	iposs.depositos dep_des, 	
	iposs.inv_movimientos mi, 	
	iposs.inv_movimientos_detalles mid, 	
	iposs.inv_tipos_movimientos tmi, 
	#LocalesVisibles# lv
where 		
	( ( dep_des.loc_codigo = mi.loc_codigo_destino and 	
	    dep_des.codigo = mi.dep_codigo_destino ) and 	
	  ( mi.fecha_comercial = mid.fecha_comercial and 	
	    mi.emp_codigo = mid.emp_codigo and 	
	    mi.numero = mid.inv_mov_numero ) and 	
	  ( dep_ori.loc_codigo = mi.loc_codigo_origen and 	
	    dep_ori.codigo = mi.dep_codigo_origen ) and 	
	  ( ae.art_codigo = mid.art_codigo and 	
	    ae.emp_codigo = mid.emp_codigo ) and 	
	  ( tmi.codigo = mi.inv_tip_mov_codigo ) and 
	  ( ae.rub_codigo = r.codigo ) and 	
	  ( ae.iva_codigo = i.codigo ) ) and
	  ( mi.fecha between :FechaDesde and :FechaHasta + (1-(1/24/60/60))) and 	
	( tmi.codigo in (( 2 )) ) and 	
	( mi.loc_codigo_origen = lv.loc_codigo and
	  ae.emp_codigo = $EMPRESA_LOGUEADA$ and	
	  mi.emp_codigo = $EMPRESA_LOGUEADA$	
	)	
group by 		
	ae.art_codigo_externo, 	
	ae.descripcion, 	
	i.descripcion , 
	r.rub_cod_empresa, 
	r.descripcion, 
	dep_ori.descripcion, 	
	dep_des.descripcion, 	
	tmi.descripcion, 	
	mi.fecha, 	
	mi.numero_documento, 	
	mi.observaciones, 
--	mi.serie_documento, 	
--	mid.costo_unitario, 	
--	mid.costo_original, 	
	mid.costo, 	
	mid.costo_imp, 
	mi.total	
