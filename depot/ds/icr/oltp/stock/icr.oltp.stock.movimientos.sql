select 
	ae.art_codigo_externo 	"Art.Codigo",
	ae.descripcion 	"Articulo",
	(select l.descripcion 
		from iposs.locales l 
		where l.codigo = im.loc_codigo_origen
		and ( trim (upper (':Local')) = 'NULL' or l.codigo in :Local ) 
		and l.emp_CODIGO =  $EMPRESA_LOGUEADA$) 	"Local Origen",
	(select l.descripcion 
		from iposs.locales l 
		where l.codigo = im.loc_codigo_destino
		and ( trim (upper (':Local')) = 'NULL' or l.codigo in :Local ) 
		and l.emp_CODIGO =  $EMPRESA_LOGUEADA$) 	"Local Destino",
	imd.fecha_comercial 	"Fecha",
	im.observaciones 	"Observaciones", 
	imd.codigo_barras 	"C�digo Barra", 
	tmi.descripcion 	"Tipo Movimiento", 
	imd.cantidad_origen 	"Cantidad Origen", 
	imd.cantidad_destino 	"Cantidad Destino", 
	imd.aj_nueva_cantidad 	"Nueva Cantidad",
	imd.aj_diferencia 	"Diferencia", 
	imd.aj_stock_final 	"Stock Final", 
	imd.precio_unitario 	"Precio Unitario", 
	imd.total_linea 	"Total L�nea", 
	imd.aj_nueva_cantidad 	"Nueva Cantidad (aj)", 
	imd.aj_diferencia 	"Diferencia (aj)", 
	imd.aj_stock_final 	"Stock Final (aj)"
from 
	iposs.articulos_empresas ae, 
	iposs.inv_movimientos im, 
	iposs.inv_movimientos_detalles imd, 
	iposs.inv_tipos_movimientos tmi
where 
	( im.fecha_comercial = imd.fecha_comercial 
and   im.emp_codigo = imd.emp_codigo 
and   im.numero = imd.inv_mov_numero ) 
and 
	( ae.art_codigo = imd.art_codigo 
and   ae.emp_codigo = imd.emp_codigo )
and 
	( tmi.codigo = im.inv_tip_mov_codigo )
and 
	( im.inv_tip_mov_codigo not in (6, 8) ) 
and 
	( im.fecha_comercial between :FechaDesde and :FechaHasta ) 
and
	( ae.emp_codigo = $EMPRESA_LOGUEADA$
and 	  im.emp_codigo =  $EMPRESA_LOGUEADA$
and 	  imd.emp_codigo =  $EMPRESA_LOGUEADA$ )
order by 
	imd.fecha_comercial asc, 
	ae.art_codigo_Externo