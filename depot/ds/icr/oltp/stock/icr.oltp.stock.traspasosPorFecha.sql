select 
	ae.art_codigo_externo 	"Cod.Art�culo", 
	ae.descripcion 		"Art�culo", 
	r.rub_Cod_empresa 	"Cod Rubro", 
	r.descripcion 		"Rubro", 
	dep_ori.descripcion 	"Dep�sito Origen", 
	dep_des.descripcion 	"Dep�sito Destino", 
	tmi.descripcion 	"Tipo Movimiento", 
	mi.fecha 		"Fecha", 
	mi.numero_documento 	"Numero", 
	mi.serie_documento 	"Serie", 
	mid.costo_unitario 	"Costo Imp.", 
	mid.costo_original 	"Costo s/Imp.", 
	sum(mid.cantidad_recibida) 	"Cantidad", 
	sum(mid.cantidad_recibida * mid.costo_unitario) 	"Total Imp.", 
	sum(mid.cantidad_recibida * mid.costo_original) 	"Total s/Imp.", 
	mi.total 		"Total Traspaso"
from 
	iposs.articulos_empresas ae, 
	iposs.rubros r, 
	iposs.depositos dep_ori, 
	iposs.depositos dep_des, 
	iposs.movimientos_inventarios mi, 
	iposs.movimientos_inv_detalles mid, 
	iposs.tipos_movimientos_inventarios tmi, 
	#LocalesVisibles# lv
where 
	( ( dep_des.loc_codigo = mi.dep_loc_codigo_destino and 
	    dep_des.codigo = mi.dep_codigo_destino ) and 
	  ( mi.fecha_comercial = mid.fecha_comercial and 
	    mi.emp_codigo = mid.mov_inv_emp_codigo and 
	    mi.numero = mid.mov_inv_numero ) and 
	  ( dep_ori.loc_codigo = mi.dep_loc_codigo_origen and 
	    dep_ori.codigo = mi.dep_codigo_origen ) and 
	  ( ae.art_codigo = mid.art_codigo and 
	    ae.emp_codigo = mid.mov_inv_emp_codigo ) and 
	  ( tmi.codigo = mi.tip_mov_in_codigo ) and 
	  ( ae.rub_codigo = r.codigo ) ) and 
	( mi.fecha_comercial between to_date(:FechaDesde) - 30 and to_date(:FechaHasta) + 30 ) and 
	( mi.fecha between :FechaDesde and :FechaHasta ) and 
-- 	( tmi.descripcion in (( 'traspaso interno' )) ) and 
	( tmi.codigo in (( 2 )) ) and 
	( mi.loc_codigo = lv.loc_codigo and
	  ae.emp_codigo = $EMPRESA_LOGUEADA$ and
	  mi.emp_codigo = $EMPRESA_LOGUEADA$
	)
group by 
	ae.art_codigo_externo, 
	ae.descripcion, 
	r.rub_cod_empresa, 
	r.descripcion, 
	dep_ori.descripcion, 
	dep_des.descripcion, 
	tmi.descripcion, 
	mi.fecha, 
	mi.numero_documento, 
	mi.serie_documento, 
	mid.costo_unitario, 
	mid.costo_original, 
	mi.total
