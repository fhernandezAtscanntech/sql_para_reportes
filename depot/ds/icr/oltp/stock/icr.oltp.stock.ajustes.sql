select 
	ae.art_codigo_externo 	"Art.Codigo",
	ae.descripcion 	"Articulo",
	l.descripcion 	"Local",
	mid.fecha_comercial 	"Fecha",
	mi.observaciones 	"Observaciones", 
	sum (decode (mid.mov_inv_tip_mov_in_codigo,
		9, nvl (mid.cantidad_recibida, 0),
		30, nvl (mid.cantidad_recibida, 0) - nvl (mid.cantidad_origen, 0),
		decode(tmi.acc_dep_origen,
			's', nvl (mid.cantidad_origen, 0),
			'r', nvl (mid.cantidad_origen, 0)*-1,
			nvl (mid.cantidad_recibida, 0)))) "Cantidad"
, 
	nvl ((select pc.costo_imp
		from precios_de_costos_articulos pc
		where pc.art_codigo = ae.art_codigo
		and pc.loc_Codigo = l.codigo
		and pc.emp_codigo = ae.emp_codigo
		and mi.fecha_comercial between pc.vigencia_desde and pc.vigencia_hasta), 0) "Costo Imp"
from 
	iposs.articulos_empresas ae, 
	iposs.locales l, 
	iposs.movimientos_inventarios mi, 
	iposs.movimientos_inv_detalles mid, 
	iposs.tipos_movimientos_inventarios tmi
where 
	( ( mi.fecha_comercial = mid.fecha_comercial and 
	    mi.emp_codigo = mid.mov_inv_emp_codigo and 
	    mi.numero = mid.mov_inv_numero ) and 
	  ( l.codigo = mi.loc_codigo ) and 
	  ( ae.art_codigo = mid.art_codigo and 
	    ae.emp_codigo = mid.mov_inv_emp_codigo ) and 
	  ( tmi.codigo = mi.tip_mov_in_codigo ) ) and 
	( mi.tip_aju_codigo in (0,1,2) ) and 
	( mid.mov_inv_tip_mov_in_codigo in (9,10,30,31,32) ) and 
	( mi.fecha_comercial between :FechaDesde and :FechaHasta ) and 
	( trim (upper (':Local')) = 'NULL' or l.codigo in :Local ) and
( ae.emp_codigo <> 1 )
group by 
	ae.art_codigo, 
	l.codigo, 
	ae.emp_codigo, 
	mi.fecha_comercial, 
	ae.art_codigo_externo, 
	ae.descripcion, 
	l.descripcion, 
	mid.fecha_comercial, 
	mi.observaciones
order by 
	mid.fecha_comercial asc, 
	sum (decode (mid.mov_inv_tip_mov_in_codigo, 
		9, mid.cantidad_recibida, 
		30, mid.cantidad_recibida - mid.cantidad_origen, 
		decode(tmi.acc_dep_origen,
			's',mid.cantidad_origen,
			'r',mid.cantidad_origen*-1, 
			mid.cantidad_recibida))) desc
