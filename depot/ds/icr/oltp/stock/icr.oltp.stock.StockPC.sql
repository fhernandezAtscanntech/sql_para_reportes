select 
	ae.art_codigo 	"Art.Codigo(I)", 
	ae.art_codigo_externo 	"Art.Codigo", 
	ae.descripcion 	"Articulo", 
	r.descripcion 	"Rubro", 
	l.codigo 	"Loc.Codigo(I)", 
	l.loc_Cod_empresa 	"Loc.Codigo", 
	l.descripcion 	"Local", 
	d.codigo 	"Cod.Deposito", 
	d.descripcion 	"Deposito", 
	(select decode (pc.mon_codigo, 
		(select valor from parametros where codigo = 'MONEDA_DEF'), pc.costo_imp, 
		pc.costo_imp * (select c.valor 
				from cambios c 
				where c.mon_codigo = pc.mon_codigo
				and :FechaStock between c.fecha and c.vigencia_hasta
				and c.emp_codigo = $EMPRESA_LOGUEADA$ ))
		from precios_de_costos_articulos pc, cambios c
		where pc.mon_codigo = c.mon_codigo
		and pc.loc_codigo = l.codigo
		and pc.art_codigo = ae.art_codigo
		and :FechaStock between pc.vigencia_desde and pc.vigencia_hasta
		and pc.emp_codigo = $EMPRESA_LOGUEADA$ 
		and rownum = 1 ) 	"Costo c.Imp.", 
	sum(s.stock) 	"Stock"
from 
	iposs.articulos_empresas ae, 
	iposs.articulos_locales al,
	iposs.depositos d, 
	iposs.rubros r, 
	iposs.locales l, 
	iposs.stocks s
where 	
		s.art_codigo = ae.art_codigo
and 	
		r.codigo = ae.rub_codigo 
and 	
		al.art_codigo = s.art_codigo 
and     al.loc_codigo = s.dep_loc_codigo 
and 
		s.dep_codigo = d.codigo  
and 	s.dep_loc_codigo = d.loc_codigo
and
		al.loc_codigo = l.codigo 
and
		ae.est_codigo <> 2 
and 	( al.fecha_borrado is null or ( al.fecha_borrado is not null and s.stock <> 0 )) 
and
		l.codigo in :Local 
and  	( trim (upper (':Rubro')) = 'NULL' or r.codigo in :Rubro ) 
and 	:FechaStock between s.fecha and s.vigencia_hasta  
and 
		ae.emp_codigo = $EMPRESA_LOGUEADA$ 
and		al.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	l.emp_codigo = $EMPRESA_LOGUEADA$ 
group by 
	ae.art_codigo,
	ae.art_codigo_externo, 
	ae.descripcion, 
	r.descripcion, 
	l.codigo,
	l.loc_Cod_empresa, 
	l.descripcion, 
	d.codigo, 
	d.descripcion
