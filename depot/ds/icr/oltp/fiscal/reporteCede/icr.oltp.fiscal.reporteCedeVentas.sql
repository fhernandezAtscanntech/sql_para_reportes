select 
	m.fecha_comercial 	"Fecha",
	m.ruc_factura  	"RUT", 
	m.nombre_factura	"Nombre", 
	i.descripcion 	"Tipo IVA", 
	l.loc_cod_empresa 	"Codigo Local", 
	l.descripcion 	"Local", 
	m.caj_codigo	"Codigo Caja", 
	c.descripcion 	"Caja", 
	mo.simbolo	"Moneda", 
	sum (mf.monto_iva) "Importe IVA"
from
	movimientos m,
	movimientos_facturas mf,
	monedas mo, 
	ivas i,
	locales l,
	cajas c,
	#LocalesVisibles# lv
where
	m.numero_mov = mf.mov_numero_mov
and 	m.emp_codigo = mf.mov_emp_codigo
and 	m.fecha_comercial = mf.fecha_comercial
and
	mf.iva_codigo = i.codigo
and
	m.loc_codigo = l.codigo
and
	m.caj_codigo = c.codigo
and 	m.loc_Codigo = c.loc_Codigo
and
	m.mon_codigo = mo.codigo
and
	l.codigo = lv.loc_codigo
and 
	l.codigo = c.loc_codigo
and
	m.ruc_factura is not null
and 
	m.fecha_comercial between :FechaDesde and :FechaHasta
and	( l.codigo in :Local )
group by 
	m.fecha_comercial 	, 
	m.ruc_factura		,
	m.nombre_factura	,
	i.descripcion		,
	l.loc_cod_empresa 	,
	l.descripcion 		, 
	m.caj_codigo 		, 
	c.descripcion 		, 
	mo.simbolo		
having sum (mf.monto_iva) <> 0
