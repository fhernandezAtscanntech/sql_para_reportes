select 
	trunc(c.fecha_emision)	"Fecha",	
	p.ruc 	"RUT", 			
	p.razon_social 	"Proveedor",	
	i.descripcion 	"Tipo IVA", 
	l.loc_cod_empresa 	"Codigo Local", 
	l.descripcion 	"Local", 
	mo.simbolo 	"Moneda", 
	sum (decode (tdi.alcance, 2, -1, 1)*cd.total_linea_calculado)/(1 + (i.tasa/100))*(i.tasa/100) 	"Importe IVA"	
from
	comprobantes c, 		
	comprobantes_detalles cd, 
	proveedores p, 
	monedas mo, 
	ivas i,
	tipos_documentos_inventarios tdi, 
	locales l,
	#LocalesVisibles# lv
where
	c.codigo = cd.comp_codigo
and 	c.emp_codigo = cd.comp_emp_codigo
and 	c.fecha_emision = cd.comp_fecha_emision
and
	c.prov_codigo = p.codigo
and
	cd.iva_codigo = i.codigo
and
	c.loc_codigo = l.codigo
and
	c.mon_codigo = mo.codigo
and
	l.codigo = lv.loc_codigo
and
	c.tip_doc_in_codigo = tdi.codigo
and
	c.tip_doc_in_codigo in (1, 2, 3, 4, 7)	-- Factura, Nota de cr�dito, Compra b2b, Boleta, Nota de Devoluci�n
and
	c.fecha_emision between :FechaDesde and (:FechaHasta + 1 - (1/24/60/60))
and	( l.codigo in :Local )
group by 
	trunc(c.fecha_emision) 	, 
	p.ruc			,
	p.razon_social		,
	i.descripcion		,
	l.loc_cod_empresa 	,
	l.descripcion 		, 
	mo.simbolo		,

	i.tasa			,
	tdi.alcance
having sum (cd.total_linea_calculado) <> 0
