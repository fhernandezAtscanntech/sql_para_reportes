select 	-- CLIENTES
	r.ruc 		"RUT_Informante", 
	'02181' 	"Formulario", 
	to_char (m.fecha_comercial, 'yyyymm') 	"Periodo",
	m.ruc_factura  	"RUT_Informado", 
	to_char (m.fecha_comercial, 'yyyymm') 	"Fecha_Factura",
	decode (i.codigo, 2, '503', 3, '502', '502') 	"Linea", 
	l.loc_cod_empresa 	"Codigo_Local", 
	l.descripcion 	"Local", 
	sum (nvl (mf.monto_iva, 0) * nvl (m.cotiza_compra, 0)) 	"Importe"
-- 	round(sum (nvl (mf.monto_iva, 0) * nvl (m.cotiza_compra, 0)), 0) "Importe"
from
	movimientos m,
	movimientos_facturas mf,
	razones_sociales r, 
	ivas i,
	locales l
where
	m.numero_mov = mf.mov_numero_mov
and 	m.emp_codigo = mf.mov_emp_codigo
and 	m.fecha_comercial = mf.fecha_comercial
and
	mf.iva_codigo = i.codigo
and
	m.loc_codigo = l.codigo
and
	l.raz_soc_codigo = r.codigo
and
	m.ruc_factura is not null
and 
	m.fecha_comercial between :FechaDesde and :FechaHasta
and
	( l.codigo in :Local )
group by 
	r.ruc , 
	'02181', 
	to_char (m.fecha_comercial, 'yyyymm'),
	m.ruc_factura, 
	to_char (m.fecha_comercial, 'yyyymm'), 
	decode (i.codigo, 2, '503', 3, '502', '502'), 
	l.loc_cod_empresa, 
	l.descripcion 
having sum (mf.monto_iva) <> 0

UNION 

select 	-- PROVEEDORES
	r.ruc 		"RUT_Informante", 
	'02181' 	"Formulario", 
	to_char (c.fecha_emision, 'yyyymm') 	"Periodo",
	p.ruc 		"RUT_Informado", 
	to_char (c.fecha_emision, 'yyyymm') 	"Fecha_Factura",
	decode (i.codigo, 1, '504', 2, '506', 3, '505', '505')  "Linea",
	l.loc_cod_empresa 	"Codigo_Local", 
	l.descripcion 	"Local", 
	sum (decode (tdi.alcance, 2, -1, 1) *
        decode (i.codigo,
        1, nvl (decode (c.mon_codigo,
		85, cd.total_linea_calculado,
			cd.total_linea_calculado *
				(select cam.valor
				from cambios cam
				where cam.emp_codigo = c.emp_codigo
				and cam.mon_codigo = c.mon_codigo and
				c.fecha_emision between fecha and vigencia_hasta)), 0),         
		nvl (decode (c.mon_codigo,
			85, cd.total_linea_calculado,
			cd.total_linea_calculado *
			(select cam.valor
			from cambios cam
			where cam.emp_codigo = c.emp_codigo
			and cam.mon_codigo = c.mon_codigo and
			c.fecha_emision between fecha and vigencia_hasta)), 0)
	/(1 + (i.tasa/100))*(i.tasa/100)))       "Importe"
from
	comprobantes c, 		
	comprobantes_detalles cd, 
	proveedores p, 
	razones_sociales r, 
	ivas i,
	tipos_documentos_inventarios tdi, 
	locales l
where
	c.codigo = cd.comp_codigo
and 	c.emp_codigo = cd.comp_emp_codigo
and 	c.fecha_emision = cd.comp_fecha_emision
and
	c.prov_codigo = p.codigo
and
	cd.iva_codigo = i.codigo
and
	c.loc_codigo = l.codigo
and
	l.raz_soc_codigo = r.codigo
and
	c.tip_doc_in_codigo = tdi.codigo
and
	c.tip_doc_in_codigo in (1, 2, 3, 4, 7)	-- Factura, Nota de cr�dito, Compra b2b, Boleta, Nota de Devoluci�n
and
	c.fecha_emision between :FechaDesde and (:FechaHasta + 1 - (1/24/60/60))
and	( l.codigo in :Local )
group by 
	r.ruc , 
	'02181', 
	to_char (c.fecha_emision, 'yyyymm'),
	p.ruc , 
	to_char (c.fecha_emision, 'yyyymm'), 
	decode (i.codigo, 1, '504', 2, '506', 3, '505', '505'), 
	i.tasa, 
	l.loc_cod_empresa, 
	l.descripcion
having sum (nvl (cd.total_linea_calculado, 0)) <> 0
