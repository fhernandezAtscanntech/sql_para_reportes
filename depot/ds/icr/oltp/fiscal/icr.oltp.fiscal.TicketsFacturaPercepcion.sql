select 
	l.descripcion 			"Local", 
	f.apellido||', '||f.nombre 	"Cajero", 
	m.fecha_comercial 		"Fecha",
	m.caj_codigo 			"Caja",  
	m.numero_operacion 		"Nro.Operaci�n", 
	m.nombre_factura 		"Nombre Factura", 
	m.ruc_factura 			"CUIT", 
	m.direccion_factura 		"Direcci�n Factura", 
	m.observacion_factura 		"Observaci�n", 
	m.tipo_de_factura 		"Tipo de Factura", 
	m.numero_operacion_fiscal 	"Numero Operacion Fiscal", 
	case when m.total >= 0
		then 'FACTURA'
		else 'NOTA DE CREDITO'
	end				"Documento", 
	iposs.f_iva_mov2 (m.numero_mov, m.fecha_comercial, m.emp_codigo) 	"IVA", 
	sum (nvl (m.total, 0)) 			"Total", 
	sum (nvl (m.importe_retencion, 0))	"Importe Retenci�n"
from 
	iposs.locales l, 
	iposs.funcionarios f, 
	iposs.movimientos m
where
	m.fun_codigo_cajera = f.fun_cod_empresa
and 	m.emp_codigo = f.emp_codigo
and 
	m.loc_codigo = l.codigo
and 	m.emp_codigo = l.emp_codigo
and 
	m.tipo_de_factura is not null
and 
	:FechaHasta - :FechaDesde <= 31
and 
	m.fecha_comercial between :FechaDesde and :FechaHasta
and
	l.codigo in :Local
and
	m.emp_codigo = $EMPRESA_LOGUEADA$
and 	l.emp_codigo = $EMPRESA_LOGUEADA$
group by 
	l.descripcion, 
	f.apellido||', '||f.nombre, 
	m.fecha_comercial, 
	m.caj_codigo,  
	m.numero_operacion, 
	m.nombre_factura, 
	m.ruc_factura, 
	m.direccion_factura, 
	m.observacion_factura, 
	m.tipo_de_factura, 
	m.numero_operacion_fiscal, 
	case when m.total >= 0
		then 'FACTURA'
		else 'NOTA DE CREDITO'
	end, 
	iposs.f_iva_mov2 (m.numero_mov, m.fecha_comercial, m.emp_codigo)
