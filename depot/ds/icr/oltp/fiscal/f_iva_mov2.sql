create or replace 
function F_IVA_MOV2 (
	p_numero_mov 	movimientos.numero_mov%type,
	p_fecha 	in date,
	p_emp_codigo 	in number,
	p_porc_iva 	movimientos_detalles.porcentaje_iva%type DEFAULT NULL)
return number
is
	v_iva number(20,5);
	v_total_bon number;
	v_importe_bon number;
	v_porc_bon number;
	v_porc_importe number;
	v_factor_bon number;
begin
	begin
		select total_bonificable
		into v_total_bon
		from movimientos
		where numero_mov = p_numero_mov
		and emp_codigo = p_emp_codigo
		and fecha_comercial = p_fecha;
	exception
	when no_data_found then
		v_total_bon := 0;
	end;

	v_importe_bon := 0;
	v_porc_bon := 0;

	for b in 
		(select importe, porcentaje
		from movimientos_de_bonificaciones
		where mov_numero_mov = p_numero_mov
		and mov_emp_codigo = p_emp_codigo
		and fecha_comercial = p_fecha ) 
	loop
		if b.porcentaje != 0 
		then
			v_porc_bon := v_porc_bon + b.porcentaje - (v_porc_bon * b.porcentaje/100);
                elsif b.importe != 0 
		then
			v_importe_bon := v_importe_bon + b.importe;
		end if;
	end loop;

	v_porc_importe := 0;

	if v_total_bon != 0 
	then
		v_porc_importe := v_importe_bon * 100 / v_total_bon;
		v_porc_bon := v_porc_bon + v_porc_importe;
	end if;

	v_factor_bon := 1- (v_porc_bon/100);
	v_iva := 0;

	for m in 
		(select * 
		from movimientos_detalles 
		where mov_numero_mov = p_numero_mov
		and mov_emp_codigo = p_emp_codigo
		and fecha_comercial = p_fecha)
	loop
		if p_porc_iva is null or m.porcentaje_iva = p_porc_iva
		then
			if m.bonificable = 1
			then
				if m.tip_det_codigo in (4, 6)
				then
					v_iva := v_iva + (m.monto_iva * v_factor_bon);
				elsif m.tip_det_codigo in (5, 7) 
				then
					v_iva := v_iva - (m.monto_iva * v_factor_bon);
				end if;
			else
				if m.tip_det_codigo in (4, 6)
				then
					v_iva := v_iva + m.monto_iva ;
				elsif m.tip_det_codigo in (5, 7) 
				then
					v_iva := v_iva - m.monto_iva ;
				end if;
			end if;
		end if;
	end loop;

	return (v_iva);
end;
/

grant execute on f_iva_mov2 to iposs_rep
/
