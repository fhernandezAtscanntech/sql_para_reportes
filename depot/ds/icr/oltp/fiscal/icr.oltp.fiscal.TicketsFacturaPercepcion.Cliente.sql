select 
	l.descripcion 			"Local", 
	f.apellido||', '||f.nombre 	"Cajero", 
	m.fecha_comercial 		"Fecha", 
	m.caj_codigo 			"Cod.Caja", 
	m.registradora			"Registradora", 
	m.fecha_operacion 		"Fecha Operación", 
	c.nombre 			"Cliente", 
	c.numero_doc 			"Nro.Documento", 
	m.numero_operacion 		"Nro.Operación", 
	m.tipo_de_factura 		"Tipo de Factura", 
	case when m.total >= 0
		then 'FACTURA'
		else 'NOTA DE CREDITO'
	end				"Documento", 
	iposs.f_iva_mov2 (m.numero_mov, m.fecha_comercial, m.emp_codigo) 	"IVA", 
	sum (nvl (m.total, 0)) 			"Total", 
	sum (nvl (m.importe_retencion, 0))	"Importe Retención"
from 
	iposs.locales l, 
	iposs.funcionarios f, 
	iposs.clientes c, 
	iposs.movimientos m
where
	m.fun_codigo_cajera = f.fun_cod_empresa
and 	m.emp_codigo = f.emp_codigo
and 
	m.loc_codigo = l.codigo
and 	m.emp_codigo = l.emp_codigo
and 
	m.cli_codigo = c.cli_cod_externo
and 	m.emp_codigo = c.emp_codigo
and
	:FechaHasta - :FechaDesde <= 31
and 
	m.fecha_comercial between :FechaDesde and :FechaHasta
and
	l.codigo in :Local
and
	m.emp_codigo = $EMPRESA_LOGUEADA$
and 	l.emp_codigo = $EMPRESA_LOGUEADA$
and 	c.emp_codigo = $EMPRESA_LOGUEADA$
group by 
	l.descripcion, 
	f.apellido||', '||f.nombre, 
	m.fecha_comercial, 
	m.caj_codigo, 
	m.registradora, 
	m.fecha_operacion, 
	c.nombre, 
	c.numero_doc, 
	m.numero_operacion, 
	m.tipo_de_factura, 
	case when m.total >= 0
		then 'FACTURA'
		else 'NOTA DE CREDITO'
	end, 
	iposs.f_iva_mov2 (m.numero_mov, m.fecha_comercial, m.emp_codigo)
