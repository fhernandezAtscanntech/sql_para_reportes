﻿select 
	l.codigo 		"Codigo Local (I)", 
	l.loc_Cod_empresa 	"Codigo Local", 
	l.descripcion 		"Local", 
	m.caj_codigo 		"Codigo Caja", 
	c.descripcion 		"Caja", 
	lp.lis_pre_ve_cod_empresa 	"Cod.Lista", 
	lp.descripcion 		"Lista PV", 
	m.numero_mov 		"Número Mov", 
	m.fecha_comercial 	"Fecha Comercial", 
	m.fecha_operacion 	"Fecha Operacion", 
	m.numero_operacion 	"Numero Operacion", 
	m.tipo_operacion 	"Tipo Operacion", 
	m.total 		"Total Venta", 
	md.art_codigo 		"Cod.Artículo", 
	md.descripcion 		"Artículo", 
-- 	sum (md.cantidad) 	"Cantidad", 
	decode (
		md.tip_det_codigo, 
		4, sum (md.cantidad), 
		6, sum (md.cantidad), 
		sum (md.cantidad) * (-1)) 	"Cantidad", 
	decode (
		md.tip_det_codigo, 
		4, sum (md.importe), 
		6, sum (md.importe), 
		sum (md.importe) * (-1)) 	"Importe"
from
	iposs.movimientos m,
	iposs.movimientos_detalles md, 
	iposs.listas_de_precios_ventas lp, 
	iposs.locales l, 
	iposs.cajas c
where
	m.numero_mov = md.mov_numero_mov
and 	m.fecha_comercial = md.fecha_comercial
and 	m.emp_codigo = md.mov_emp_codigo
and
	m.loc_codigo = l.codigo
and 
	m.loc_codigo = c.loc_codigo
and 	m.caj_codigo = c.codigo
and
	l.codigo = c.loc_codigo
and
	m.lis_pre_ve_codigo = lp.lis_pre_ve_cod_empresa
and 	m.emp_codigo = lp.emp_codigo
and 
	:FechaHasta - :FechaDesde <= 31
and
	m.fecha_comercial between :FechaDesde and :FechaHasta
and
	md.art_codigo = lpad(:Articulo, 15, '0')
and	
	l.codigo in :Local
and
	m.tipo_operacion = 'VENTA'
and 
	( m.emp_codigo = $EMPRESA_LOGUEADA$
and 	  md.mov_emp_codigo = $EMPRESA_LOGUEADA$
and 	  l.emp_codigo = $EMPRESA_LOGUEADA$)
group by 
	l.codigo, 
	l.loc_Cod_empresa, 
	l.descripcion, 
	m.caj_codigo, 
	c.descripcion, 
	lp.lis_pre_ve_cod_empresa, 
	lp.descripcion, 
	m.numero_mov, 
	m.fecha_comercial, 
	m.fecha_operacion, 
	m.numero_operacion, 
	m.tipo_operacion, 
	m.total, 
	md.art_codigo, 
	md.descripcion, 
	md.tip_det_codigo
