select 
	m.loc_codigo 		"Codigo Local (I)", 
	l.loc_Cod_empresa 	"Codigo Local", 
	l.descripcion 		"Local", 
	m.caj_codigo 		"Codigo Caja", 
	c.descripcion 		"Caja", 
	m.fecha_comercial 	"Fecha", 
	m.numero_operacion 	"Numero Operacion", 
	m.tipo_operacion 	"Tipo Operacion", 
	sum (m.total*p.cotiza_compra) 	"Importe"
from
	movimientos m,
	locales l, 
	cajas c
where
	m.loc_codigo = l.codigo
and 
	m.loc_codigo = c.loc_codigo
and 	m.caj_codigo = c.codigo
and
	l.codigo = c.loc_codigo
and
	:FechaHasta - :FechaDesde <= 31
and
	m.fecha_comercial between :FechaDesde and :FechaHasta
and
	m.total * m.cotiza_compra between :MontoDesde and :MontoHasta
and	
	( trim (upper (':Local')) = 'NULL' or l.codigo in :Local )
and
	( m.emp_codigo = $EMPRESA_LOGUEADA$
and 	  l.emp_codigo = $EMPRESA_LOGUEADA$)
group by 
	m.loc_codigo 		, 
	l.loc_Cod_empresa 	, 
	l.descripcion 		, 
	m.caj_codigo 		, 
	c.descripcion 		, 
	m.fecha_comercial 	, 
	m.numero_operacion 	,
	m.tipo_operacion 
