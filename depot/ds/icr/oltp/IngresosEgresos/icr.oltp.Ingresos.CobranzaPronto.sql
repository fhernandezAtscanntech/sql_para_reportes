select 
	l.descripcion 		"Local", 
	i.descripcion 		"Ingreso", 
	m.fecha_comercial 	"Fecha", 
	count(m.total) 		"Cantidad", 
	sum(m.total) 		"Importe"
from 
	iposs.locales l, 
	iposs.movimientos_eging_modelo m, 
	iposs.tipos_de_ingresos i, 
	iposs.tipos_de_ingresos_locales il
where 	l.emp_codigo = m.emp_codigo 
and 	l.codigo = m.loc_codigo 
and 	l.codigo = il.loc_codigo
and 	i.codigo = il.tip_ing_codigo 
and 	il.loc_codigo = m.loc_codigo 
and 	il.tip_ing_codigo_externo = m.ing_codigo 
and 	i.descripcion = 'COBRANZA PRONTO' 
and 	m.fecha_comercial between :FechaDesde and :FechaHasta 
and 	:FechaHasta - :FechaDesde <= 31
and 	l.codigo in :Local
group by 
	l.descripcion, 
	i.descripcion, 
	m.fecha_comercial
order by 
	l.descripcion asc, 
	m.fecha_comercial asc

