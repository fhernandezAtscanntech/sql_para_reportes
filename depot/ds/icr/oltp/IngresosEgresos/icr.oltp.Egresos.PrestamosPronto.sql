select 
	l.descripcion 		"Local", 
	e.descripcion 		"Egreso", 
	m.fecha_comercial 	"Fecha", 
	count(m.total) 		"Cantidad", 
	sum(m.total) 		"Importe"
from 
	iposs.locales l, 
	iposs.movimientos_eging_modelo m, 
	iposs.tipos_de_egresos e, 
	iposs.tipos_de_egresos_locales el
where 	l.emp_codigo = m.emp_codigo 
and 	l.codigo = m.loc_codigo 
and 	l.codigo = el.loc_codigo 
and 	e.codigo = el.tip_egr_codigo 
and 	el.loc_codigo = m.loc_codigo 
and 	el.tip_egr_codigo_externo = m.egr_codigo
and 	e.descripcion = 'PRESTAMO PRONTO'
and 	m.fecha_comercial between :FechaDesde and :FechaHasta 
and 	:FechaHasta - :FechaDesde <= 31
and 	l.codigo in :Local 
group by 
	l.descripcion, 
	e.descripcion, 
	m.fecha_comercial
order by 
	l.descripcion asc, 
	m.fecha_comercial asc
