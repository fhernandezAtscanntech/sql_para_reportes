select 
	m.loc_codigo 		"Codigo Local", 
	l.descripcion 		"Local", 
	m.caj_codigo 		"Codigo Caja", 
	c.descripcion 		"Caja", 
	lp.descripcion 		"Lista de Precios", 
	m.fecha_comercial 	"Fecha", 
	m.numero_operacion 	"Numero Operacion", 
	m.fecha_operacion 	"Fecha Operacion", 
	to_char(m.fecha_operacion, 'hh24:mi:ss') 	"Hora Operacion", 	-- no hay formato de fecha que despliegue la hora a este momento
	m.fun_codigo_cajera	"Codigo Cajera"	, 
	(select f.apellido||', '||f.nombre  from funcionarios f, funcionarios_locales fl where fl.fun_codigo = f.codigo and fl.loc_codigo = m.loc_codigo and f.fun_cod_empresa = m.fun_codigo_cajera)	"Cajera", 
	nvl (m.fun_codigo_autoriza, m.fun_codigo) 		"Codigo Autoriza", 
	(select f.apellido||', '||f.nombre  from funcionarios f, funcionarios_locales fl where fl.fun_codigo = f.codigo and fl.loc_codigo = m.loc_codigo and f.fun_cod_empresa = nvl (m.fun_codigo_autoriza, m.fun_codigo))	"Autoriza", 
	(select e.descripcion
		from tipos_de_egresos e, tipos_de_egresos_locales tel
		where e.codigo = tel.tip_egr_codigo
		and tel.loc_codigo = m.loc_codigo
		and tel.tip_egr_codigo_externo = m.egr_codigo) "Tipo de Egreso",
	nvl (
		(select s.descripcion
			from subegresos s, tipos_de_egresos e, tipos_De_egresos_locales tel
			where e.codigo = tel.tip_egr_codigo
			and tel.loc_codigo = m.loc_codigo
			and tel.tip_egr_codigo_externo = m.sub_egr_tip_egr_codigo
			and s.tip_egr_codigo = e.codigo
			and s.codigo = m.sub_egr_codigo), 
		' ')						"Sub Egreso", 
	m.observacion		"Observacion", 
	md.linea 		"L�nea", 
	al.art_codigo_Externo 	"Cod.Art�culo", 
	td.descripcion 		"Tipo detalle", 
	ae.descripcion 		"Art�culo", 
	md.cantidad	 	"Cantidad",
	p.precio 		"Precio Venta"	
from 
	movimientos m, 
	movimientos_det_eging md, 
	tipos_de_detalles td, 
	articulos_locales al, 
	articulos_empresas ae, 
	precios_de_articulos_ventas p, 
	listas_de_precios_ventas lp, 
	listas_pre_ventas_locales lpl, 
	locales l, 
	cajas c
where
	m.numero_mov = md.mov_numero_mov
and 	m.fecha_comercial = md.fecha_comercial
and 	m.emp_codigo = md.mov_emp_codigo
and
	md.tip_det_codigo = td.codigo
and
	m.loc_codigo = l.codigo
and 	m.emp_codigo = l.emp_codigo
and
	m.caj_codigo = c.codigo
and 	m.loc_codigo = c.loc_codigo
and
	md.art_codigo = al.art_codigo_externo 
and 	md.loc_codigo = al.loc_codigo
and
	al.art_codigo = ae.art_codigo
and 	al.emp_codigo = ae.emp_codigo
and
	lpl.lis_pre_ve_codigo = lp.codigo
and 	lpl.loc_codigo = l.codigo 
and
	lpl.lis_pre_ve_codigo_externo = m.lis_pre_ve_codigo
and 	lpl.loc_codigo = m.loc_codigo
and 
	lp.emp_codigo = m.emp_codigo
and
	p.loc_lis_loc_codigo = lpl.loc_codigo
and 	p.loc_lis_lis_pre_ve_codigo = lpl.lis_pre_ve_codigo
and 
	p.art_codigo = al.art_codigo
and 	m.fecha_operacion between p.vigencia and p.vigencia_hasta
-- and 
-- 	m.tipo_operacion = 'EGRESO'
and 
	(m.egr_codigo is not null)
and
	:FechaHasta - :FechaDesde <= 31
and
	m.fecha_comercial between :FechaDesde and :FechaHasta
and	
	( trim (upper (':Local')) = 'NULL' or l.codigo in :Local )
and
	( m.emp_codigo = $EMPRESA_LOGUEADA$
and 	  l.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  al.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  ae.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  p.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  lp.emp_codigo = $EMPRESA_LOGUEADA$ )
