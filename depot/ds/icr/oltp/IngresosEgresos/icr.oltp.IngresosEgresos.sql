select 
	m.loc_codigo 		"Codigo Local", 
	l.descripcion 		"Local", 
	m.caj_codigo 		"Codigo Caja", 
	c.descripcion 		"Caja", 
	m.fecha_comercial 	"Fecha", 
	m.numero_operacion 	"Numero Operacion", 
	m.fecha_operacion 	"Fecha Operacion", 
	to_char(m.fecha_operacion, 'hh24:mi:ss') 	"Hora Operacion", 	-- no hay formato de fecha que despliegue la hora a este momento
	m.fun_codigo_cajera	"Codigo Cajera"	, 
	(select f.apellido||', '||f.nombre  from funcionarios f, funcionarios_locales fl where fl.fun_codigo = f.codigo and fl.loc_codigo = l.codigo and f.fun_cod_empresa = m.fun_codigo_cajera)	"Cajera", 
	nvl (m.fun_codigo_autoriza, m.fun_codigo) 		"Codigo Autoriza", 
	(select f.apellido||', '||f.nombre  from funcionarios f, funcionarios_locales fl where fl.fun_codigo = f.codigo and fl.loc_codigo = l.codigo and f.fun_cod_empresa = nvl (m.fun_codigo_autoriza, m.fun_codigo))	"Autoriza", 
	m.tipo_operacion 	"Operacion", 
	nvl (
	(select e.descripcion
		from tipos_de_egresos e, tipos_de_egresos_locales tel
		where e.codigo = tel.tip_egr_codigo
		and tel.loc_codigo = m.loc_codigo
		and tel.tip_egr_codigo_externo = m.egr_codigo), 
        (select i.descripcion 
		from tipos_de_ingresos i, tipos_de_ingresos_locales til
	 	where i.codigo = til.tip_ing_codigo
		and til.loc_codigo = m.loc_codigo
		and til.tip_ing_codigo_Externo = m.ing_codigo)) "Descripcion",
	nvl (
		(select s.descripcion
			from subegresos s, tipos_de_egresos e, tipos_De_egresos_locales tel
			where e.codigo = tel.tip_egr_codigo
			and tel.loc_codigo = m.loc_codigo
			and tel.tip_egr_codigo_externo = m.sub_egr_tip_egr_codigo
			and s.tip_egr_codigo = e.codigo
			and s.codigo = m.sub_egr_codigo), 
		(nvl (
			(select s.descripcion
			from subingresos s, tipos_de_ingresos i, tipos_de_ingresos_locales til
		 	where i.codigo = til.tip_ing_codigo
			and til.loc_codigo = m.loc_codigo
			and til.tip_ing_codigo_Externo = m.sub_ing_tip_ing_codigo
			and s.tip_ing_codigo = i.codigo
			and s.codigo = m.sub_ing_codigo), 
			' ')))	"Subtipo", 
	m.observacion		"Observacion", 
	mo.simbolo		"Moneda", 
	m.total 		"Importe"
from 
	movimientos m, 
	monedas mo,
	locales l, 
	cajas c
where
	m.loc_codigo = l.codigo
and 	m.emp_codigo = l.emp_codigo
and
	m.caj_codigo = c.codigo
and 	m.loc_codigo = c.loc_codigo
and
	m.mon_codigo = mo.codigo
-- and
-- 	m.tipo_operacion in ('EGRESO','INGRESO')
and 
	(ing_codigo is not null or egr_codigo is not null)
and
	( trim (upper (:TipoOperacion)) is null or m.tipo_operacion = :TipoOperacion )
and
	:FechaHasta - :FechaDesde <= 31
and
	m.fecha_comercial between :FechaDesde and :FechaHasta
and	
	( trim (upper (':Local')) = 'NULL' or l.codigo in :Local )
and
	( m.emp_codigo = $EMPRESA_LOGUEADA$
and 	  l.emp_codigo = $EMPRESA_LOGUEADA$ )
