select 
	m.fecha_comercial	"Fecha"	, 
	m.emp_codigo	"Codigo Empresa"	, 
	e.descripcion	"Empresa"	, 
	m.loc_codigo	"Codigo Local"	, 
	l.descripcion	"Local"	, 
	m.caj_codigo	"Codigo Caja"	, 
	c.descripcion	"Caja"	, 
	m.numero_operacion 	"Nro.Operacion", 
	m.fecha_operacion 	"Fecha Operación", 
	m.fun_codigo_cajera	"Codigo Cajera"	, 
	(select f.apellido||', '||f.nombre from funcionarios f, funcionarios_locales fl where fl.fun_codigo = f.codigo and fl.loc_codigo = mp.loc_codigo and f.fun_cod_empresa = m.fun_codigo_cajera)	"Cajera"	, 
	mp.tip_pag_codigo	"Codigo Tipo de Pago"	, 
	(select t.descripcion from tipos_de_pagos t where t.codigo = mp.tip_pag_codigo)	"Tipo De Pago"	, 
	nvl ((select c.descripcion from creditos c, creditos_locales cl where c.codigo = cl.cre_codigo and cl.cre_codigo_externo = mp.cre_codigo and cl.loc_codigo = mp.loc_codigo), 
		nvl ((select v.descripcion from vales_de_compras v, vales_de_compras_locales vl where v.codigo = vl.val_codigo and vl.val_codigo_Externo = mp.val_codigo and vl.loc_codigo = mp.loc_codigo), 
			nvl ((select b.descripcion from bancos b where b.codigo = mp.ban_codigo), ''))) "Sub tipo", 
	mon.codigo	"Codigo Moneda", 
	mon.simbolo 	"Moneda", 
	mp.Importe	"Importe"	, 
	(mp.importe_pago * mp.cotiza_compra) 	"Importe MN", 
	mp.cotiza_venta	"Cotiza Venta"	, 
	mp.cotiza_compra	"Cotiza Compra"	, 
	mp.importe_pago	"Importe Pago"
from 
	iposs.movimientos_de_pagos mp, 	
	iposs.movimientos m, 
	iposs.empresas e,	
	iposs.locales l,	
	iposs.cajas c, 
	iposs.monedas mon
where m.numero_mov = mp.mov_numero_mov
and m.emp_codigo = mp.mov_emp_codigo
and m.fecha_comercial = mp.fecha_comercial
and mp.mov_emp_codigo = e.codigo	
and	mp.loc_Codigo = l.codigo	
and	m.caj_codigo = c.codigo	
and	mp.loc_Codigo = c.loc_Codigo	
and	mp.mon_codigo = mon.codigo
and mp.fecha_comercial = :Fecha
and mp.tip_pag_codigo in (35, 36, 37, 38, 39)
and l.codigo in :Local
and
	(l.emp_codigo = $EMPRESA_LOGUEADA$ and
	mp.mov_emp_codigo = $EMPRESA_LOGUEADA$)

