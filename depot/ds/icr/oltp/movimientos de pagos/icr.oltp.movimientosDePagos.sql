select /*+ ordered */
	m.loc_codigo 		"Codigo Local", 
	l.descripcion 		"Local", 
	m.caj_codigo 		"Codigo Caja", 
	c.descripcion 		"Caja", 
	m.fecha_comercial 	"Fecha", 
	mo.simbolo		"Moneda", 
	tp.descripcion 	"Tipo Pago", 
	cr.codigo		"Codigo Credito (int)", 
	cr.descripcion 		"Credito", 
	cl.cre_codigo_Externo 	"Codigo Credito", 
	m.numero_operacion 	"Numero Operacion", 
	m.fecha_operacion 	"Fecha Operacion", 
	mp.numero_tarjeta 	"Numero Tarjeta", 
	mp.numero_documento 	"Numero Documento", 
	mp.numero_autorizacion 	"Numero Autorizacion", 
	mp.numero_autorizacion_chr 	"Autorizacion Numero",  
	sum (mp.importe*mp.cotiza_compra) 	"Importe", 
	sum (mp.descuento_afam*mp.cotiza_compra) 	"Desc.Afam", 
	sum (mp.descuento_inc_financiera*mp.cotiza_compra) 	"Desc.Inc.Financiera", 
	count(*) "Cantidad" 
from
	movimientos m,
	movimientos_de_pagos mp,
	tipos_de_pagos tp, 
	monedas mo, 
	locales l,
	cajas c,
	creditos cr,
	creditos_locales cl, 
	#LocalesVisibles# lv
where
	m.numero_mov = mp.mov_numero_mov
and 	m.emp_codigo = mp.mov_emp_codigo
and 	m.fecha_comercial = mp.fecha_comercial
and
	m.loc_codigo = l.codigo
and
	m.caj_codigo = c.codigo
and 	m.loc_Codigo = c.loc_Codigo
and
	m.mon_codigo = mo.codigo
and
	mp.cre_codigo = cl.cre_codigo_Externo
and	mp.loc_codigo = cl.loc_codigo
and
	mp.tip_pag_codigo = tp.codigo
and 
	cl.cre_codigo = cr.codigo
and
	l.codigo = lv.loc_codigo
and 
	l.codigo = c.loc_codigo
and
	mp.tip_pag_codigo in (10, 13)
and
	:FechaHasta - :FechaDesde <= 31
and
	m.fecha_comercial between :FechaDesde and :FechaHasta
and	
--	(nvl (:Local, l.codigo) = l.codigo)
-- 	( trim (upper (':Local')) = 'NULL' or l.codigo in :Local )
	( l.codigo in :Local )
group by 
	m.loc_codigo 		, 
	l.descripcion 		, 
	m.caj_codigo 		, 
	c.descripcion 		, 
	m.fecha_comercial 	, 
	mo.simbolo		, 
	tp.descripcion	, 
	cr.codigo		, 
	cr.descripcion 		, 
	cl.cre_codigo_Externo 	, 
	m.numero_operacion 	, 
	m.fecha_operacion 	, 
	mp.numero_tarjeta 	, 
	mp.numero_documento 	, 
	mp.numero_autorizacion ,
	mp.numero_autorizacion_chr
