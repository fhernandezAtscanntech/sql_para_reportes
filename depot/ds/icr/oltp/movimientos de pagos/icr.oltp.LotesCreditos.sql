select 
	m.loc_codigo 		"Codigo Local", 
	l.descripcion 	"Local", 
	m.caj_codigo 		"Codigo Caja", 
	c.descripcion 		"Caja", 
	m.fecha_comercial 	"Fecha", 
	mo.simbolo		"Moneda", 
	tp.descripcion 		"Tipo Pago", 
	cr.codigo		"Codigo Credito (int)", 
	cr.descripcion 	"Credito", 
	cl.cre_codigo_Externo 	"Codigo Credito", 
	m.numero_operacion  	"Numero Operacion", 
	mp.numero_tarjeta 	"Numero Tarjeta", 
	mp.numero_documento 	"Numero Documento", 
	mp.numero_autorizacion 	"Numero Autorizacion", 
	mp.comercio_Credito 	"Comercio", 
	mp.terminal_Credito 	"Terminal", 
	mp.lote_credito 	"Lote", 
	sum (mp.importe*mp.cotiza_compra) 	"Importe",
	sum (mp.importe) 	"Importe Voucher"
from
	iposs.movimientos m, 
	iposs.movimientos_de_pagos mp,
	iposs.tipos_de_pagos tp, 
	iposs.monedas mo, 
	iposs.locales l,
	iposs.cajas c,
	iposs.creditos cr,
	iposs.creditos_locales cl
where
	m.numero_mov = mp.mov_numero_mov
and 	m.emp_codigo = mp.mov_emp_codigo
and 	m.fecha_comercial = mp.fecha_comercial
and
	m.loc_codigo = l.codigo
and
	m.caj_codigo = c.codigo
and 	m.loc_Codigo = c.loc_Codigo
and
	mp.mon_codigo = mo.codigo
and
	mp.cre_codigo = cl.cre_codigo_Externo
and	mp.loc_codigo = cl.loc_codigo
and
	cl.cre_codigo = cr.codigo
and 
	l.codigo = c.loc_codigo
and
	mp.tip_pag_codigo = tp.codigo
and
	mp.tip_pag_codigo in (10, 13)
and
	mp.comercio_credito is not null
and
	:FechaHasta - :FechaDesde <= 31
and
	m.fecha_comercial between :FechaDesde and :FechaHasta
and	
	( trim (upper (':Local')) = 'NULL' or l.codigo in :Local )
and	
	( trim (upper (':Credito')) = 'NULL' or cr.codigo in :Credito )
and 
		m.emp_codigo = $EMPRESA_LOGUEADA$
and 	mp.mov_emp_codigo = $EMPRESA_LOGUEADA$
and 	l.emp_codigo = $EMPRESA_LOGUEADA$
group by 
	m.loc_codigo 		, 
	l.descripcion 	, 
	m.caj_codigo 		, 
	c.descripcion 		, 
	m.fecha_comercial 	, 
	mo.simbolo		, 
	tp.descripcion 	, 
	cr.codigo		, 
	cr.descripcion 	, 
	cl.cre_codigo_Externo 	, 
       m.numero_operacion ,
	mp.numero_tarjeta 	, 
	mp.numero_documento 	, 
	mp.numero_autorizacion , 
	mp.comercio_Credito , 
	mp.terminal_Credito , 
	mp.lote_credito