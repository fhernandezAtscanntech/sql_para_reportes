select 
	m.loc_codigo 		"Codigo Local", 
	l.descripcion 		"Local", 
	m.caj_codigo 		"Codigo Caja", 
	c.descripcion 		"Caja", 
	m.fecha_comercial 	"Fecha", 
	mo.simbolo		"Moneda", 
	m.numero_operacion 	"Numero Operacion", 
	b.descripcion 		"Banco", 
	mp.fecha_cheque 	"Fecha Cheque", 
	mp.numero_cheque 	"Numero Cheque", 
	mp.numero_documento 	"Numero Documento", 
	mp.telefono 	 	"Tel�fono", 
	sum (mp.importe*mp.cotiza_compra) 	"Importe"
from
	movimientos m,
	movimientos_de_pagos mp,
	monedas mo, 
	bancos b, 
	locales l,
	cajas c
where
	m.numero_mov = mp.mov_numero_mov
and 	m.emp_codigo = mp.mov_emp_codigo
and 	m.fecha_comercial = mp.fecha_comercial
and
	m.loc_codigo = l.codigo
and
	m.caj_codigo = c.codigo
and 	m.loc_Codigo = c.loc_Codigo
and
	mp.mon_codigo = mo.codigo
and
	mp.ban_codigo = b.codigo
and 
	l.codigo = c.loc_codigo
and
	mp.tip_pag_codigo = 11
and
	:FechaHasta - :FechaDesde <= 31
and
	m.fecha_comercial between :FechaDesde and :FechaHasta
and	
	( l.codigo in :Local )
and 
	( m.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  mp.mov_emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  l.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	m.loc_codigo, 
	l.descripcion, 
	m.caj_codigo, 
	c.descripcion, 
	m.fecha_comercial, 
	mo.simbolo, 
	m.numero_operacion, 
	b.descripcion, 
	mp.fecha_cheque, 
	mp.numero_cheque, 
	mp.numero_documento, 
	mp.telefono
