select 
	l.loc_cod_empresa 	"Loc. Codigo", 
	l.descripcion 		"Local", 
	mtm.audit_number 	"Audit", 
	mttm.descripcion 	"Tipo Movimiento", 
	mtm.estado_lote 	"Estado Lote", 
	mtm.fecha_disponible 	"Fecha Diponible", 
	mtm.nro_lote 		"Nro. Lote", 
	mtm.numero_autorizacion "Nro. Autorización", 
	mtm.numero_documento 	"Nro. Documento", 
	mtm.numero_mov_cc_anu 	"Nro. Mov. CC Anu", 
	mtm.reference_number 	"Reference Number", 
	mtcc.sello 		"Sello", 
	trunc (mtm.fecha) 	"Fecha", 
	sum (mtm.importe_acr) 	"Importe Acreditado", 
	sum (mtm.importe_pos) 	"Importe Pos", 
	sum (mtm.importe_arancel) 	"Importe Arancel", 
	sum (mtm.importe_comision) 	"Importe Comisión", 
	sum (mtm.importe) "Importe"
from 
	iposs.locales l, 
	iposs.mt_cuentas_corrientes mtcc, 
	iposs.mt_movimientos_cc mtm, 
	iposs.mt_tipos_movimientos_cc mttm
where 
	l.codigo = mtm.loc_codigo 
and 	l.emp_codigo = mtm.emp_codigo 
and 
	mtcc.codigo = mtm.cue_cor_codigo 
and 	
	mttm.codigo = mtm.tip_mov_cc 
and 
	mtm.fecha between :FechaDesde and :FechaHasta + (1-(1/24/60/60)) 
and 
	l.codigo in :Local
and
	l.emp_codigo = $EMPRESA_LOGUEADA$
and 	mtcc.emp_codigo = $EMPRESA_LOGUEADA$
and 	mtm.emp_codigo = $EMPRESA_LOGUEADA$
group by 
	l.loc_cod_empresa, 
	l.descripcion, 
	mtm.audit_number, 
	mttm.descripcion, 
	mtm.estado_lote, 
	mtm.fecha_disponible, 
	mtm.nro_lote, 
	mtm.numero_autorizacion, 
	mtm.numero_documento, 
	mtm.numero_mov_cc_anu, 
	mtm.reference_number, 
	mtcc.sello, 
	trunc (mtm.fecha)
