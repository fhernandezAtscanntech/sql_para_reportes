create or replace function iposs.f_cantidad_prom (p_prom iposs.promociones.codigo%type, p_loc iposs.locales.codigo%type, p_emp iposs.empresas.codigo%type, p_fec_ini date default null, p_fec_fin date default null)
return number
is
	v_fec date;
	v_fec_ini	date;
	v_fec_fin	date;
	v_cant_buff	number;
	v_cant	number;
begin
	begin
		select trunc(vigencia_desde), trunc(vigencia_hasta)
		into v_fec_ini, v_fec_fin
		from iposs.prom_vigencias
		where prom_codigo = p_prom;
	exception
	when no_data_found then
		return null;
	end;

	if nvl (p_fec_ini, to_date('01/01/1900', 'dd/mm/yyyy')) > v_fec_ini
	then
		v_fec_ini := p_fec_ini;
	end if;
	if nvl (p_fec_fin, to_date('01/01/2100', 'dd/mm/yyyy')) < v_fec_fin
	then
		v_fec_fin := p_fec_fin;
	end if;

	
	if v_fec_fin > sysdate 
	then v_fec_fin := trunc(sysdate);
	end if;
	
	v_fec := v_fec_ini;
	v_cant := 0;

	while v_fec <= v_fec_fin
	loop
		select nvl (sum (decode (md.tip_det_codigo, 
				4, md.cantidad, 
				5, -md.cantidad, 
				0)), 0)
		into v_cant_buff
		from iposs.movimientos_Detalles md, iposs.movimientos m
		where m.numero_mov = md.mov_Numero_mov
		and m.emp_codigo = md.mov_emp_codigo
		and m.fecha_comercial = md.fecha_comercial
		and m.emp_codigo = p_emp
		and m.loc_Codigo = p_loc
		and m.fecha_comercial = v_fec
		and m.tipo_operacion = 'VENTA'
		and md.art_codigo = md.combo_padre
		and md.art_Codigo in 
--			(select art_codigo_combo
--			from iposs.prom_datos_movimientos
--			where prom_codigo = p_prom);
			(select distinct al.art_codigo_Externo
			from iposs.articulos_locales al, iposs.prom_datos pd
			where al.art_codigo = pd.art_codigo
			and md.loc_codigo = al.loc_codigo
			and md.mov_emp_codigo = al.emp_codigo
			and pd.prom_codigo = p_prom);
		v_cant := v_cant + v_cant_buff;

		v_fec := v_fec + 1;

	end loop;
	
	return v_cant;
end;
/

grant execute on f_cantidad_prom to iposs_rep;