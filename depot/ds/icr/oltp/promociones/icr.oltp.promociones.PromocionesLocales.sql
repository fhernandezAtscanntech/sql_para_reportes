select 
	p.prom_cod_empresa 		"Cod. Promoci�n", 
	p.descripcion 			"Promoci�n", 
	p.max_x_ticket 			"Max. por ticket", 
	decode (p.pide_telefono, 
			1, 'SI', 
			'NO') 		"Pide Tel�fono", 
	cp.descripcion 			"Campa�a", 
	tp.descripcion 			"Tipo Promoci�n", 
	l.loc_cod_empresa 		"Cod. Local", 
	l.codigo 				"Cod. Local (I)", 
	l.emp_codigo 			"Cod. Empresa", 
	l.descripcion 			"Local", 
	l.direccion 			"Direcci�n", 
	de.descripcion 			"Departamento", 
	lo.descripcion 			"Localidad", 
	pv.vigencia_desde 		"Vigencia Desde", 
	pv.vigencia_hasta 		"Vigencia Hasta", 
	lh.fecha_borrado		"Fecha Borrado", 
	decode (lh.estado, 
			1, 'Pendiente', 
			2, 'Aceptada', 
			3, 'Rechazada', 
			4, 'Finalizada', 
			5, 'A reprocesar', 
			6, 'Pendiente de reprocesar', 
			'Desconocido: '||lh.estado) 	"Estado Promoci�n", 
-- 	* intent� sacar la �ltma fecha del stock, pero los combos no se procesan en el inventario
-- 	* tampoco se actualiza la fecha del �ltimo movimiento en el DW
--	, (select max(fecha_comercial)
--		from iposs.movimientos_detalles md, iposs.prom_datos_movimientos pdm, iposs.prom_vigencias pv
--		where md.mov_emp_codigo = l.emp_codigo
--		and md.loc_codigo = l.codigo
--		and pv.prom_codigo = pdm.prom_codigo
--		and pv.prom_codigo = p.codigo
--		and pdm.art_codigo_combo = md.art_codigo
--		and md.fecha_comercial between pv.vigencia_desde and pv.vigencia_hasta)
-- 	* esta opci�n es muy pesada, la saco a una funci�n: sigue siendo pesada pero funciona mejor
	decode (:ImprimeUltimaFecha, 1, iposs.f_fecha_ult_prom (p.codigo, l.codigo, l.emp_codigo), NULL) 	"Fecha Ult.Promo", 
	decode (:ImprimeCantidad, 1, iposs.f_cantidad_prom (p.codigo, l.codigo, l.emp_codigo), NULL) 	"Cantidad Promos"	
from 
	iposs.promociones p, 
	iposs.campa�as cp, 
	iposs.prom_locales_habilitados lh, 
	iposs.prom_vigencias pv, 
	iposs.tipos_de_promociones tp, 
	iposs.locales l, 
	iposs.departamentos de, 
	iposs.localidades lo
where
	p.cpa_codigo = cp.codigo
and p.emp_codigo = cp.emp_codigo
and
	p.tip_pro_codigo = tp.codigo
and
	p.codigo = pv.prom_codigo 
and p.emp_codigo = pv.emp_codigo
and 
	p.codigo = lh.prom_codigo
and 
	lh.emp_codigo = l.emp_codigo
and lh.loc_Codigo = l.codigo
and	
	l.depa_codigo = de.codigo
and l.loca_codigo = lo.codigo
and 
	cpa_codigo in :Campana
