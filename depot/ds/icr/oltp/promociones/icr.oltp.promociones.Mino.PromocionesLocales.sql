select 
	p.prom_cod_empresa 		"Cod. Promoci�n", 
	p.descripcion 			"Promoci�n", 
	p.max_x_ticket 			"Max. por ticket", 
	decode (p.pide_telefono, 
			1, 'SI', 
			'NO') 		"Pide Tel�fono", 
	cp.descripcion 			"Campa�a", 
	tp.descripcion 			"Tipo Promoci�n", 
	l.loc_cod_empresa 		"Cod. Local", 
	l.emp_codigo 			"Cod. Empresa", 
	l.descripcion 			"Local", 
	l.direccion 			"Direcci�n", 
	pv.vigencia_desde 		"Vigencia Desde", 
	pv.vigencia_hasta 		"Vigencia Hasta", 
	lh.fecha_borrado		"Fecha Borrado", 
	decode (lh.estado, 
			1, 'Pendiente', 
			2, 'Aceptada', 
			3, 'Rechazada', 
			4, 'Finalizada', 
			5, 'A reprocesar', 
			6, 'Pendiente de reprocesar', 
			'Desconocido: '||lh.estado) 	"Estado Promoci�n", 
-- 	decode (:ImprimeUltimaFecha, 1, iposs.f_fecha_ult_prom (p.codigo, l.codigo, l.emp_codigo), NULL) 	"Fecha Ult.Promo", 
-- 	decode (:ImprimeCantidad, 1, iposs.f_cantidad_prom (p.codigo, l.codigo, l.emp_codigo), NULL) 	"Cantidad Promos"	
	iposs.f_fecha_ult_prom (p.codigo, l.codigo, l.emp_codigo) 	"Fecha Ult.Promo", 
	iposs.f_cantidad_prom (p.codigo, l.codigo, l.emp_codigo) 	"Cantidad Promos"	

from 
	iposs.promociones p, 
	iposs.campa�as cp, 
	iposs.prom_locales_habilitados lh, 
	iposs.prom_vigencias pv, 
	iposs.tipos_de_promociones tp, 
	iposs.locales l
where
	p.cpa_codigo = cp.codigo (+)
and p.emp_codigo = cp.emp_codigo (+)
and
	p.tip_pro_codigo = tp.codigo
and
	p.codigo = pv.prom_codigo 
and p.emp_codigo = pv.emp_codigo
and 
	p.codigo = lh.prom_codigo
and 
	lh.emp_codigo = l.emp_codigo
and lh.loc_Codigo = l.codigo
and	
	:Fecha between pv.vigencia_desde and pv.vigencia_hasta
and 
	l.codigo in :Local
and 
	l.emp_codigo = $EMPRESA_LOGUEADA$
	