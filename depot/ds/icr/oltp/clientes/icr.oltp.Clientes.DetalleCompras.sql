select 
	cre.descripcion		"Cr�dito", 
	mo.Simbolo 		"Moneda", 
	cc.numero_tarjeta 	"N�mero Tarjeta", 
	cli.cli_cod_externo 	"C�d.Cliente", 
	cli.nombre 		"Cliente", 
	trim (cli.dir_calle ||' '||cli.dir_numero||' '||cli.dir_apto) "Direccion", 
	cli.telefono 		"Tel�fono",
	( select con.descripcion 
		from convenios con 
		where cc.conve_codigo = con.codigo) 	"Convenio", 
	md.art_codigo 					"C�d.Art�culo", 
	(select ae.art_Codigo_Externo 
		from articulos_empresas ae, articulos_locales al
		where ae.art_codigo = al.art_codigo
		and ae.emp_codigo = al.emp_codigo
		and al.loc_codigo = md.loc_Codigo
		and al.art_Codigo_Externo = md.art_codigo) 	"Art�culo", 
	md.rub_codigo 					"C�d.Rubro", 
	(select r.descripcion
		from rubros r, rubros_locales rl
		where r.codigo = rl.rub_codigo
		and md.loc_codigo = rl.loc_codigo
		and rl.rub_codigo_Externo = md.rub_codigo) 	"Rubro", 
	m.numero_operacion 		"Nro.Operaci�n", 
	m.fecha_comercial 		"Fecha", 
	m.caj_codigo 			"Caja", 
	sum (decode (
		md.tip_det_codigo, 
		4, md.cantidad, 
		5, -md.cantidad, 
		6, md.cantidad, 
		7, -md.cantidad)) 	"Cantidad", 
	sum (decode (
		md.tip_det_codigo, 
		4, md.importe, 
		5, -md.importe, 
		6, md.importe, 
		7, -md.importe)) 	"Importe"
from 
	iposs.clientes cli, 
	iposs.cuentas_corrientes cc, 
	iposs.movimientos_cc mcc, 
	iposs.monedas mo, 
	iposs.creditos cre, 
	iposs.movimientos_Detalles md, 
	iposs.movimientos m
where
	cc.cli_codigo = cli.codigo
and 	cc.cre_codigo = cre.codigo
and 	cc.mon_codigo = mo.codigo
and
	cc.cli_codigo = mcc.cue_cor_cli_codigo
and 	cc.numero_tarjeta = mcc.cue_cor_numero_tarjeta
and 	cc.cre_codigo = mcc.cue_cor_cre_codigo
and 	cc.mon_codigo = mcc.cue_cor_mon_codigo
and
	mcc.mov_numero_mov = md.mov_numero_mov
and 	mcc.mov_fecha_comercial = md.fecha_comercial
and 	mcc.mov_emp_codigo = md.mov_emp_codigo
and
	md.mov_numero_mov = m.numero_mov
and 	md.mov_emp_codigo = m.emp_codigo
and 	md.fecha_comercial = m.fecha_comercial 
and 
	md.tip_det_codigo in (4, 5, 6, 7)
and 
	cli.codigo = :Cliente
and
	mcc.fecha between :FechaDesde and :FechaHasta +1-(1/24/60/60)
and
	cre.codigo <>
		nvl (
			(select valor
			from parametros_empresas pe
			where pe.emp_codigo = $EMPRESA_LOGUEADA$
			and pe.par_codigo = 'BONUS_CREDITO')
		,
			(select valor
			from parametros p
			where p.codigo = 'BONUS_CREDITO'))
and
	cli.emp_codigo = $EMPRESA_LOGUEADA$
and 	cc.emp_codigo = $EMPRESA_LOGUEADA$
and 	mcc.mov_emp_codigo = $EMPRESA_LOGUEADA$
and 	md.mov_emp_codigo = $EMPRESA_LOGUEADA$
group by
	cre.descripcion		, 
	mo.Simbolo 		, 
	cc.numero_tarjeta 	, 
	cli.cli_cod_externo 	, 
	cli.nombre 		, 
	trim (cli.dir_calle ||' '||cli.dir_numero||' '||cli.dir_apto) , 
	cli.telefono 		,
	cc.conve_codigo		, 
	md.art_codigo 		, 
	md.rub_codigo 		, 
	md.loc_codigo 		, 
	md.tip_det_codigo 	, 
	m.numero_operacion 	,  
	m.fecha_comercial 	, 
	m.caj_codigo 		