select 
	cre.descripcion		"Cr�dito", 
	mo.Simbolo 		"Moneda", 
	cc.numero_tarjeta 	"N�mero Tarjeta", 
	cli.cli_cod_externo 	"Cli.C�digo", 
	cli.nombre 		"Cliente", 
	trim (cli.dir_calle ||' '||cli.dir_numero||' '||cli.dir_apto) "Direccion", 
	cli.telefono 		"Tel�fono",
	(select con.descripcion from convenios con where cc.conve_codigo = con.codigo) "Convenio", 
	cc.tope 		"Tope", 
	mcc.fecha 		"Fecha", 
	mcc.comprobante 	"Comprobante", 
	tmcc.descripcion 	"Detalle", 
	mcc.notas 		"Notas", 
	mcc.importe		"Importe", 
	cc.saldo_contable 	"Saldo Actual", 
	cc.saldo_contable - 
		(select nvl (sum (importe), 0)
		from movimientos_cc mcc_aux
		where mcc_aux.cue_cor_cli_codigo = cc.cli_codigo
		and mcc_aux.cue_cor_numero_tarjeta = cc.numero_tarjeta
		and mcc_aux.cue_cor_cre_codigo = cc.cre_codigo
		and mcc_aux.cue_cor_mon_codigo = cc.mon_codigo
		and mcc_aux.fecha > mcc.fecha) "Saldo", 
	cc.saldo_contable - 
		(select nvl (sum (importe), 0)
		from movimientos_cc mcc_aux
		where mcc_aux.cue_cor_cli_codigo = cc.cli_codigo
		and mcc_aux.cue_cor_numero_tarjeta = cc.numero_tarjeta
		and mcc_aux.cue_cor_cre_codigo = cc.cre_codigo
		and mcc_aux.cue_cor_mon_codigo = cc.mon_codigo
		and mcc_aux.fecha > :FechaDesde) "Saldo Inicial"

from 
	clientes cli, 
	cuentas_corrientes cc, 
	movimientos_cc mcc, 
	tipos_movimientos_cc tmcc, 
	monedas mo, 
	creditos cre
where
	cc.cli_codigo = cli.codigo
and 	cc.cre_codigo = cre.codigo
and 	cc.mon_codigo = mo.codigo
and
	cc.cli_codigo = mcc.cue_cor_cli_codigo
and 	cc.numero_tarjeta = mcc.cue_cor_numero_tarjeta
and 	cc.cre_codigo = mcc.cue_cor_cre_codigo
and 	cc.mon_codigo = mcc.cue_cor_mon_codigo
and
	mcc.tmcc_codigo = tmcc.codigo
and
	mcc.fecha between :FechaDesde and :FechaHasta + 1-(1/24/60/60)
and
	cre.codigo <>
		nvl (
			(select valor
			from parametros_empresas pe
			where pe.emp_codigo = $EMPRESA_LOGUEADA$
			and pe.par_codigo = 'BONUS_CREDITO')
		,
			(select valor
			from parametros p
			where p.codigo = 'BONUS_CREDITO'))
and
	cli.emp_codigo = $EMPRESA_LOGUEADA$
and 	cc.emp_codigo = $EMPRESA_LOGUEADA$
and 	mcc.mov_emp_codigo = $EMPRESA_LOGUEADA$
order by
	cre.codigo, 
	cc.numero_tarjeta, 
	mcc.fecha
