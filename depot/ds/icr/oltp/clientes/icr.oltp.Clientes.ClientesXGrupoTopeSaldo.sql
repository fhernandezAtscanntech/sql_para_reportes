select 
	cre.descripcion		"Cr�dito", 
	l.codigo 		"Cod.Local (I)", 
	l.loc_cod_empresa 	"Cod.Local", 
	l.descripcion 		"Local", 
	cli.cli_cod_externo 		"Cli.C�digo", 
	cli.nombre 			"Cliente", 
	cli.numero_doc 		"N�mero Doc.", 
	cli.telefono 			"Tel�fono",
	cc.numero_tarjeta 		"N�mero Tarjeta", 
	g.descripcion 		"Grupo de Clientes", 
	mo.Simbolo 			"Moneda", 
	cc.vencimiento 		"Vencimiento", 
	cc.tope 		"Tope", 
	decode (
		cc.cre_codigo, 
		nvl (
			(select valor
			from parametros_empresas pe
			where pe.emp_codigo = $EMPRESA_LOGUEADA$
			and pe.par_codigo = 'BONUS_CREDITO')
			,
			(select valor
			from parametros p
			where p.codigo = 'BONUS_CREDITO')), 
		cc.saldo, 
		cc.saldo_contable )	"Saldo"
from 
	clientes cli, 
	clientes_locales cl, 
	grupos_de_clientes g, 
	locales l, 
	cuentas_corrientes cc, 
	monedas mo, 
	creditos cre
where
	g.codigo = cli.gru_cli_codigo
and
	cc.cli_codigo = cli.codigo
and 	cc.cre_codigo = cre.codigo
and 	cc.mon_codigo = mo.codigo
and
	cli.codigo = cl.cli_codigo
and 	l.codigo = cl.loc_codigo
and
	cc.mon_codigo = mo.codigo
and 
	( trim (upper (':Credito')) = 'NULL' or cre.codigo in :Credito )
and 
	( trim (upper (':GrupoClientes')) = 'NULL' or g.codigo in :GrupoClientes )
and
	l.codigo in :Local
and
	cli.emp_codigo = $EMPRESA_LOGUEADA$
and 	cc.emp_codigo = $EMPRESA_LOGUEADA$
order by
	cre.codigo, 
	cli.nombre
