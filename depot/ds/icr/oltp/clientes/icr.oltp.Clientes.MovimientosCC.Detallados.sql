select 
	l.codigo 		"C�digo Local (I)", 
	l.loc_cod_empresa 	"C�digo Local", 
	l.descripcion 		"Local", 
	cre.descripcion		"Cr�dito", 
	mo.Simbolo 		"Moneda", 
	cc.numero_tarjeta 	"N�mero Tarjeta", 
	cli.cli_cod_externo 	"Cli.C�digo", 
	cli.nombre 		"Cliente", 
	trim (cli.dir_calle ||' '||cli.dir_numero||' '||cli.dir_apto) "Direccion", 
	cli.telefono 		"Tel�fono",
	(select con.descripcion from convenios con where cc.conve_codigo = con.codigo) "Convenio", 
	cc.tope 		"Tope", 
	mcc.fecha 		"Fecha", 
	mcc.comprobante 	"Comprobante", 
	tmcc.descripcion 	"Detalle", 
	mcc.notas 		"Notas", 
	sum (mcc.importe) 	"Importe"
from 
	clientes cli, 
	clientes_locales cl, 
	locales l, 
	cuentas_corrientes cc, 
	movimientos_cc mcc, 
	tipos_movimientos_cc tmcc, 
	monedas mo, 
	creditos cre
where
	cli.codigo = cl.cli_codigo
and 	l.codigo = cl.loc_codigo
and 
	cc.cli_codigo = cli.codigo
and 	cc.cre_codigo = cre.codigo
and 	cc.mon_codigo = mo.codigo
and
	cc.cli_codigo = mcc.cue_cor_cli_codigo
and 	cc.numero_tarjeta = mcc.cue_cor_numero_tarjeta
and 	cc.cre_codigo = mcc.cue_cor_cre_codigo
and 	cc.mon_codigo = mcc.cue_cor_mon_codigo
and
	mcc.tmcc_codigo = tmcc.codigo
and
	mcc.fecha between :FechaDesde and :FechaHasta + 1-(1/24/60/60)
and
	l.codigo in :Local
and 
	l.emp_codigo = $EMPRESA_LOGUEADA$
and 	cli.emp_codigo = $EMPRESA_LOGUEADA$
and 	cc.emp_codigo = $EMPRESA_LOGUEADA$
and 	mcc.mov_emp_codigo = $EMPRESA_LOGUEADA$

group by
	l.codigo, 
	l.loc_cod_empresa, 
	l.descripcion, 
	cre.descripcion, 
	mo.Simbolo, 
	cc.numero_tarjeta, 
	cli.cli_cod_externo, 
	cli.nombre, 
	trim (cli.dir_calle ||' '||cli.dir_numero||' '||cli.dir_apto), 
	cli.telefono,
	cc.conve_codigo, 
	cc.tope, 
	mcc.fecha, 
	mcc.comprobante, 
	tmcc.descripcion, 
	mcc.notas
