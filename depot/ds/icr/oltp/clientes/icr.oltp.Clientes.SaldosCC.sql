select 
	cre.descripcion		"Cr�dito", 
	mo.Simbolo 			"Moneda", 
	cc.numero_tarjeta 		"N�mero Tarjeta", 
	cli.cli_cod_externo 		"Cli.C�digo", 
	cli.nombre 				"Cliente", 
	trim (cli.dir_calle ||' '||cli.dir_numero||' '||cli.dir_apto) "Direccion", 
	g.descripcion  			"Grupo de Clientes", 
	cli.telefono 			"Tel�fono",
	(select con.descripcion from convenios con where cc.conve_codigo = con.codigo) "Convenio", 
	cc.tope 			"Tope", 
	cc.saldo_contable - 
		(select nvl (sum (importe), 0)
		from movimientos_cc mcc
		where mcc.cue_cor_cli_codigo = cc.cli_codigo
		and mcc.cue_cor_numero_tarjeta = cc.numero_tarjeta
		and mcc.cue_cor_cre_codigo = cc.cre_codigo
		and mcc.cue_cor_mon_codigo = cc.mon_codigo
		and mcc.fecha > trunc(:Fecha)+1-(1/24/60/60)) "Saldo", 
	(select fecha
		from movimientos_cc mcc
		where mcc.cue_cor_cli_codigo = cc.cli_codigo
		and mcc.cue_cor_numero_tarjeta = cc.numero_tarjeta
		and mcc.cue_cor_cre_codigo = cc.cre_codigo
		and mcc.cue_cor_mon_codigo = cc.mon_codigo
		and mcc.tmcc_codigo = 70 -- Pago
		and mcc.fecha = 
			(select max(mcc2.fecha)
			from movimientos_cc mcc2
			where mcc2.cue_cor_cli_codigo = cc.cli_codigo
			and mcc2.cue_cor_numero_tarjeta = cc.numero_tarjeta
			and mcc2.cue_cor_cre_codigo = cc.cre_codigo
			and mcc2.cue_cor_mon_codigo = cc.mon_codigo
			and mcc2.tmcc_codigo = 70
			and mcc2.fecha < :Fecha)
		and rownum = 1) "Fecha Ult.Pag.", 
	(select importe
		from movimientos_cc mcc
		where mcc.cue_cor_cli_codigo = cc.cli_codigo
		and mcc.cue_cor_numero_tarjeta = cc.numero_tarjeta
		and mcc.cue_cor_cre_codigo = cc.cre_codigo
		and mcc.cue_cor_mon_codigo = cc.mon_codigo
		and mcc.tmcc_codigo = 70 -- Pago
		and mcc.fecha = 
			(select max(mcc2.fecha)
			from movimientos_cc mcc2
			where mcc2.cue_cor_cli_codigo = cc.cli_codigo
			and mcc2.cue_cor_numero_tarjeta = cc.numero_tarjeta
			and mcc2.cue_cor_cre_codigo = cc.cre_codigo
			and mcc2.cue_cor_mon_codigo = cc.mon_codigo
			and mcc2.tmcc_codigo = 70
			and mcc2.fecha < :Fecha)
		and rownum = 1) "Monto Ult.Pag."
from 
	clientes cli, 
	iposs.grupos_de_clientes g, 
	cuentas_corrientes cc, 
	monedas mo, 
	creditos cre
where
	cc.cli_codigo = cli.codigo
and 	cc.cre_codigo = cre.codigo
and 	cc.mon_codigo = mo.codigo
and 	cli.gru_cli_codigo = g.codigo(+)
and
	( trim (upper (':Credito')) = 'NULL' or cre.codigo in :Credito )
and
	cli.nombre between 
		decode (upper(:ClienteDesde), '', chr(1), :ClienteDesde) and 
		decode (upper(:ClienteHasta), '', chr(255), :ClienteHasta)
and
	cre.codigo <>
		nvl (
			(select valor
			from parametros_empresas pe
			where pe.emp_codigo = 4690
			and pe.par_codigo = 'BONUS_CREDITO')
		,
			(select valor
			from parametros p
			where p.codigo = 'BONUS_CREDITO'))
and
	cli.emp_codigo = $EMPRESA_LOGUEADA$
and 	cc.emp_codigo = $EMPRESA_LOGUEADA$
order by
	cre.codigo, 
	cli.nombre
