select 
	l.descripcion 	"Local", 
	m.caj_codigo 	"Caja", 
	m.fecha_comercial 	"Fecha", 
	m.numero_operacion 	"Nro Operaci�n", 
	m.numero_operacion_fiscal 	"Nro.Op.Fiscal", 
	m.numero_de_cupon 		"Nro. Cupon", 
	m.serie_ECF 		"ECF", 
	m.nombre_factura 	"Nombre", 
	m.ruc_factura 	"CPF", 
	m.direccion_factura 	"Direcci�n", 
	m.total 			"Total"
from 
	movimientos m, 
	locales l
where
	m.loc_codigo = l.codigo
and
	m.ruc_factura is not null
and	
	m.tipo_operacion = 'VENTA'
and 
	m.fecha_comercial between :FechaDesde and :FechaHasta
and 	
	l.codigo in :Local
and
	( m.emp_codigo = $EMPRESA_LOGUEADA$
and 	  l.emp_codigo = $EMPRESA_LOGUEADA$)
