select 
	l.descripcion 	"Local", 
	mf.caj_codigo 	"Caja", 
	mo.simbolo 	"Moneda", 
	mf.fecha_comercial 	"Fecha", 
	tdi.descripcion 	"Tipo Documento", 
	mf.serie_factura 	"Serie", 
	mf.numero_factura 	"Numero", 
	m.nombre_factura 	"Nombre", 
	m.ruc_factura 	"RUT", 
	m.direccion_factura 	"Dirección", 
	m.numero_operacion 	"Numero Operación", 
	i.descripcion 		"IVA", 
	mf.importe 	"Importe", 
	mf.monto_iva 	"Monto IVA", 
	tp.descripcion 	"Tipo Pago", 
	mo2.simbolo 	 "Moneda Pago", 
	mp.importe_pago 	"Importe Pago"
from 
	iposs.movimientos_facturas mf, 
	iposs.movimientos m, 
	iposs.movimientos_de_pagos mp, 
	iposs.tipos_de_pagos tp, 
	iposs.locales l, 
	iposs.monedas mo, 
	iposs.monedas mo2, 
	iposs.ivas i, 
	iposs.tipos_documentos_inventarios tdi
where
	( mf.mov_numero_mov = m.numero_mov
and 	  mf.mov_emp_codigo = m.emp_codigo
and 	  mf.fecha_comercial = m.fecha_comercial )
and
	( mf.mov_numero_mov = mp.mov_numero_mov
and 	  mf.mov_emp_codigo = mp.mov_emp_codigo
and 	  mf.fecha_comercial = mp.fecha_comercial )
and 
	mp.tip_pag_codigo = tp.codigo
and 
	mf.loc_codigo = l.codigo
and
	mf.mon_codigo = mo.codigo
and
	mp.mon_codigo = mo2.codigo
and 
	mf.iva_codigo = i.codigo
and
	mf.tip_doc_in_codigo = tdi.codigo
and
	mf.fecha_comercial between :FechaDesde and :FechaHasta
and 	
	l.codigo in :Local
and
	( mf.mov_emp_codigo = $EMPRESA_LOGUEADA$
and 	  m.emp_codigo = $EMPRESA_LOGUEADA$
and 	  l.emp_codigo = $EMPRESA_LOGUEADA$)
