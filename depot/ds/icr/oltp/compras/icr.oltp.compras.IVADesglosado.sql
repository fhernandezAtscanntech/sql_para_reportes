select 
	l.descripcion 			"Local", 
	tdi.descripcion 		"Tipo Documento",  
	c.serie 			"Serie", 
	mo.simbolo 			"Moneda", 
	nvl (ca.valor, 1) "Cotiza",  	
	trunc(c.fecha_emision) 		"Fecha",
	p.razon_social 			"Proveedor", 
	p.ruc 				"CUIT",  
	c.total_descuentos 		"Total Descuentos", 
	c.iva_percibido 		"IVA Percibido", 
	c.ing_brutos_1 			"Ing.Brutos 1", 
	c.ing_brutos_2 			"Ing.Brutos 2", 
	c.iva_percibido_2 		"IVA Percibido 2", 
	i.descripcion 		"Tipo IVA", 
	i.tasa 				"Tasa", 
	c.numero 			"Numero", 
	sum(cd.imp_interno) 		"Imp.Interno", 
	decode (
		c.tip_doc_in_codigo, 
		2, -c.redondeo, 
		6, -c.redondeo, 
		7, -c.redondeo, 
		c.redondeo) 		"Redondeo", 
	sum ((decode (
		cd.comp_tip_doc_in_codigo, 
		2, -cd.total_linea_calculado, 
		6, -cd.total_linea_calculado, 
		7, -cd.total_linea_calculado, 
		cd.total_linea_calculado))) 		"Total Linea Calculado", 
	decode (
		c.tip_doc_in_codigo, 
		2, -c.sub_total, 
		6, -c.sub_total, 
		7, -c.sub_total, 
		c.sub_total) 		"Sub Total", 
	decode (
		c.tip_doc_in_codigo, 
		2, -c.total_ingresado, 
		6, -c.total_ingresado, 
		7, -c.total_ingresado, 
		c.total_ingresado) 	"Total Ingresado", 
	sum ( decode (
		cd.comp_tip_doc_in_codigo, 
		2, -cd.total_linea_calculado, 
		6, -cd.total_linea_calculado, 
		7, -cd.total_linea_calculado, 
		cd.total_linea_calculado) * 100 / ( 100 + (decode (pe.factura_sin_impuestos, 1, 0, 1) * i.tasa )))		"Total Sin Imp.", 
	sum ((decode (
		cd.comp_tip_doc_in_codigo, 
		2, -cd.total_linea_calculado, 
		6, -cd.total_linea_calculado, 
		7, -cd.total_linea_calculado, 
		cd.total_linea_calculado) ) - 
	((decode (
		cd.comp_tip_doc_in_codigo, 
		2, -cd.total_linea_calculado, 
		6, -cd.total_linea_calculado, 
		7, -cd.total_linea_calculado, 
		cd.total_linea_calculado) ) * 100 / ( 100 + (decode (pe.factura_sin_impuestos, 1, 0, 1) * i.tasa ) ))) 		"IVA"
from 
	iposs.comprobantes c, 
	iposs.comprobantes_detalles cd, 
	iposs.locales l, 
	iposs.monedas mo, 
	iposs.ivas i, 
	iposs.tipos_documentos_inventarios tdi, 
	iposs.proveedores p, 
	iposs.proveedores_empresas pe, 
	(select ca.mon_codigo, ca.fecha, ca.vigencia_hasta, ca.valor
		from iposs.cambios ca
		where ca.emp_codigo = $EMPRESA_LOGUEADA$
		and :FechaDesde <= ca.vigencia_hasta
		and :FechaHasta >= ca.fecha) ca
where 
	c.fecha_emision = cd.comp_fecha_emision 
and     c.emp_codigo = cd.comp_emp_codigo 
and     c.codigo = cd.comp_codigo 
and 
	p.codigo = c.prov_codigo 
and 
	pe.emp_codigo = c.emp_codigo 
and 	pe.prov_codigo = p.codigo
and
	tdi.codigo = c.tip_doc_in_codigo 
and 	
	cd.iva_codigo = i.codigo 
and 
	l.codigo = c.loc_codigo 
and 
	mo.codigo = cd.mon_codigo 
and 
	pe.fecha_borrado is null 
and 
		(c.fecha_emision between ca.fecha and ca.vigencia_hasta or ca.fecha is null)
and 	c.mon_codigo = ca.mon_codigo(+)
and 
		cd.comp_tip_doc_in_codigo in ('1','2','4','7')
and 
		nvl (pe.de_gastos, 0) = 0
and 
	nvl (c.fecha_contabilidad, c.fecha_emision) between :FechaDesde and :FechaHasta + 1 - (1/24/60/60) 
and 
	l.codigo in :Local 
and
	( c.emp_codigo = $EMPRESA_LOGUEADA$
and 	  cd.comp_emp_codigo = $EMPRESA_LOGUEADA$
and 	  l.emp_codigo = $EMPRESA_LOGUEADA$
and 	  pe.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.descripcion 		, 
	tdi.descripcion		, 
	c.serie			, 
	mo.simbolo		, 
	nvl (ca.valor, 1), 
	trunc(c.fecha_emision)	, 
	p.razon_social		, 
	decode (
		c.tip_doc_in_codigo, 
		2, -c.redondeo, 
		6, -c.redondeo, 
		7, -c.redondeo, 
		c.redondeo)	, 
	p.ruc			, 
	decode (
		c.tip_doc_in_codigo, 
		2, -c.sub_total, 
		6, -c.sub_total, 
		7, -c.sub_total, 
		c.sub_total)	, 
	decode (
		cd.iva_codigo, 
		1, 0, 
		2, 10.5, 
		3, 21, 
		8, 27, 
		9, 5.65)	, 
	decode (c.tip_doc_in_codigo, 
		2, -c.total_ingresado, 
		6, -c.total_ingresado, 
		7, -c.total_ingresado, 
		c.total_ingresado), 
	c.total_descuentos	, 
	c.iva_percibido		, 
	c.ing_brutos_1		, 
	c.ing_brutos_2		, 
	c.iva_percibido_2	, 
	i.descripcion 		, 
	i.tasa			, 
	c.numero
