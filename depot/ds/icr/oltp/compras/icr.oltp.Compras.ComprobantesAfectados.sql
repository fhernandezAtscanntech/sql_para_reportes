-- Comprobantes Afectados - Transcripto de Discoverer

select 
	ae.art_codigo_externo 		"Cod.Artículo", 
	ae.descripcion 				"Artículo", 
	l.descripcion				"Local", 
	tdi1.descripcion 			"Tipo Documento 1", 
	c1.fecha_emision 			"Fecha Comp. 1", 
	c1.serie 					"Serie 1", 
	c1.numero 					"Nro.Comp. 1", 
	p.razon_social 				"Proveedor", 
	ca.tipo_afectacion 			"Tipo afectación", 
	c2.serie 					"Serie 2", 
	c2.numero 					"Nro.Comp. 2", 
	c2.fecha_emision 			"Fecha Comp. 2", 
	tdi2.descripcion 			"Tipo Documento 2", 
	sum(ca.cantidad) 			"Cantidad"
from 
	iposs.articulos_empresas ae, 
	iposs.comprobantes c1, 
	iposs.locales l, 
	iposs.proveedores p, 
	iposs.tipos_documentos_inventarios tdi1, 
	iposs.comprobantes_afectados ca, 
	iposs.comprobantes c2, 
	iposs.tipos_documentos_inventarios tdi2
where 
		l.codigo = c1.loc_codigo
and 
		p.codigo = c1.prov_codigo
and 
		tdi1.codigo = c1.tip_doc_in_codigo
and 	
		c1.codigo = ca.codigo 
and 	c1.emp_codigo = ca.emp_codigo 
and 	c1.fecha_emision = ca.fecha_emision
and 
		ae.emp_codigo = ca.emp_codigo 
and 	ae.art_codigo = ca.art_codigo 
and 
		c2.codigo = ca.comp_codigo 
and 	c2.fecha_emision = ca.comp_fecha_emision 
and 	c2.emp_codigo = ca.emp_codigo 
and 
		tdi2.codigo = c2.tip_doc_in_codigo 
and 
		ca.cantidad <> 0
and 	l.codigo in :Local 
and 	c1.fecha_emision between :FechaDesde and :FechaHasta + 1 - (1/24/60/60)
and 	p.codigo in :Proveedor
and 
		ae.emp_codigo = $EMPRESA_LOGUEADA$
and 	l.emp_codigo = $EMPRESA_LOGUEADA$
and 	ca.emp_codigo = $EMPRESA_LOGUEADA$
and 	c1.emp_codigo = $EMPRESA_LOGUEADA$
and 	c2.emp_codigo = $EMPRESA_LOGUEADA$
group by 
	ae.art_codigo_externo, 
	ae.descripcion, 
	l.descripcion, 
	tdi1.descripcion, 
	c1.fecha_emision, 
	c1.serie, 
	c1.numero, 
	p.razon_social, 
	ca.tipo_afectacio, 
	c2.serie, 
	c2.numero, 
	c2.fecha_emision, 
	tdi2.descripcion
