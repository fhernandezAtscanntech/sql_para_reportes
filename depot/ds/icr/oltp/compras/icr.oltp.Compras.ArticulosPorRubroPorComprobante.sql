select 
	/* transcripto de discoverer con corrrecciones*/
	l.descripcion 			"Local", 
	ae.art_codigo_externo 	"Cod. Artículo", 
	ae.descripcion 			"Artículo", 
	r.descripcion 			"Rubro", 
	cd.comp_fecha_emision 	"Fecha Emisión", 
	cd.comp_serie 			"Serie", 
	cd.comp_numero 			"Número", 
	p.razon_social 			"Razón Social", 
	m.simbolo 				"Moneda", 
	coalesce (
		(select cam.valor_compra
		from iposs.cambios cam
		where cam.mon_codigo = cd.mon_codigo
		and emp_codigo = l.emp_codigo
		and cd.comp_fecha_emision between cam.fecha and cam.vigencia_hasta),  	-- cotización en la empresa
		(select cam.valor_compra
		from iposs.cambios cam
		where cam.mon_codigo = cd.mon_codigo
		and emp_codigo = 1
		and cd.comp_fecha_emision between cam.fecha and cam.vigencia_hasta), 	-- cotización en la modelo
		1																		-- cotización para que no caiga
	) 						"Factor ME a MN", 
	sum ( cd.total_linea_calculado * decode (cd.comp_tip_doc_in_codigo, 2, -1, 6, -1, 7, -1, 1) / ( 100 + iposs.k_pre2000.f_tasa_imp (cd.iva_codigo, cd.comp_fecha_emision)) * (iposs.k_pre2000.f_tasa_imp (cd.iva_codigo, cd.comp_fecha_emision) ) ) "Monto Iva", 
	cd.cantidad * decode (cd.comp_tip_doc_in_codigo, 2, -1, 6, -1, 7, -1, 1) 				"Cantidad", 
	cd.total_linea_calculado * decode (cd.comp_tip_doc_in_codigo, 2, -1, 6, -1, 7, -1, 1) 	"Total Línea Calculado", 
	cd.total_linea_ingresado * decode (cd.comp_tip_doc_in_codigo, 2, -1, 6, -1, 7, -1, 1) 	"Total Línea Ingresado"
from 
	iposs.articulos_empresas ae, 
	iposs.comprobantes_detalles cd, 
	iposs.locales l, 
	iposs.monedas m, 
	iposs.proveedores p, 
	iposs.proveedores_empresas pe, 
	iposs.rubros r
where 	r.codigo = ae.rub_codigo  
and 	l.codigo = cd.loc_codigo 
and 	m.codigo = cd.mon_codigo 
and 	p.codigo = cd.comp_prov_codigo 
and 	pe.prov_codigo = p.codigo
and 	pe.emp_codigo = l.emp_codigo
and 	nvl (pe.de_gastos, 0) = 0
and 	ae.emp_codigo = cd.comp_emp_codigo 
and 	ae.art_codigo = cd.art_codigo 
and 	cd.comp_tip_doc_in_codigo in ('1','2','4','7') 
and 	cd.comp_fecha_emision between :FechaDesde and to_date(:FechaHasta) + 1 - (1/24/60/60)
-- and 	( trim (upper (':Local')) = 'NULL' or l.codigo in :Local )
and 	( l.codigo in :Local )
and 	( r.codigo in :Rubro )
and 	ae.emp_codigo = $EMPRESA_LOGUEADA$
and 	cd.comp_emp_codigo = $EMPRESA_LOGUEADA$
and 	l.emp_codigo = $EMPRESA_LOGUEADA$
group by 
	ae.art_codigo_externo, 
	cd.comp_fecha_emision, 
	cd.comp_numero, 
	cd.comp_serie, 
	ae.descripcion, 
	l.descripcion, 
	r.descripcion, 
	p.razon_social, 
	m.simbolo, 
	cd.mon_codigo, 
	cd.comp_tip_doc_in_codigo, 
	cd.cantidad, 
	cd.total_linea_calculado, 
	cd.total_linea_ingresado, 
	l.emp_codigo

	
--
grant execute on iposs.k_pre2000 to iposs_rep;
