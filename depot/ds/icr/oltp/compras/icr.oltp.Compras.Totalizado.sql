select 
	m.simbolo 			"Moneda", 
	p.razon_social		"Raz�n Social",  
	p.ruc 				"RUT", 
	trunc (c.fecha_emision) 	"Fecha", 
	sum(c.iric) 		"IRIC", 
	sum(c.iva_percibido) 	"IVA Percibido", 
	sum(c.imeba) 		"IMEBA", 
	sum ( decode (
				c.tip_doc_in_codigo, 
				2, -c.total_calculado, 
				6, -c.total_calculado, 
				7, -c.total_calculado,
				c.total_calculado) ) 	"Total Calculado", 
	sum ( decode (
				c.tip_doc_in_codigo, 
				2, -c.total_ingresado, 
				6, -c.total_ingresado, 
				7, -c.total_ingresado, 
				c.total_ingresado) ) 	"Total Ingresado", 
	sum ( decode (
				c.tip_doc_in_codigo, 
				2, -c.redondeo, 
				6, -c.redondeo, 
				7, -c.redondeo, 
				c.redondeo) ) 			"Redondeo", 
	sum ( decode (
				c.tip_doc_in_codigo, 
				2, -c.total_iva, 
				6, -c.total_iva, 
				7, -c.total_iva, 
				c.total_iva) ) 			"Total IVA"
from 
	iposs.comprobantes c, 
	iposs.locales l, 
	iposs.monedas m, 
	iposs.proveedores p, 
	iposs.proveedores_empresas pe
where 	( ( l.codigo = c.loc_codigo ) 
and 	  ( m.codigo = c.mon_codigo ) 
and 	  ( p.codigo = c.prov_codigo 
and 	    pe.emp_codigo = c.emp_codigo ) ) 
and 	( c.tip_doc_in_codigo in ('1','2','4','7') ) 
and 	( l.codigo in :Local ) 
and 	nvl (c.fecha_contabilidad, c.fecha_emision) between :FechaDesde and :FechaHasta + 1 - (1/24/60/60) 
and 	( p.codigo = pe.prov_codigo ) 
and 	( pe.fecha_borrado is null  )
and 	nvl (pe.de_gastos, 0) = 0
and 	( pe.emp_codigo = $EMPRESA_LOGUEADA$
and 	  c.emp_codigo = $EMPRESA_LOGUEADA$
and 	  l.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	m.simbolo, 
	p.razon_social, 
	p.ruc, 
	trunc (c.fecha_emision) 
order by 
	sum ( decode (
			c.tip_doc_in_codigo, 
			2, -c.total_ingresado, 
			6, -c.total_ingresado, 
			7, -c.total_ingresado, 
			c.total_ingresado)) desc
