-- Comprobantes Pendientes - Transcripto de Discoverer

select 
	c.audit_date 		"Fecha Ingreso", 
	c.audit_user 		"Usuario Ingreso", 
	l.descripcion		"Local", 
	tdi.descripcion 	"Tipo Documento", 
	c.numero 			"Número", 
	c.serie 			"Serie", 
	p.razon_social 		"Proveedor", 
	p.ruc 				"RUT", 
	cp.comp_fecha_emision 	"Fecha Emisión", 
	c.total_ingresado 		"Total Ingresado", 
	c.total_iva 			"Total IVA", 
	c.imeba 				"Imeba", 
	c.iva_percibido 		"IVA Percibido"
from 
	iposs.comprobantes c, 
	iposs.locales l, 
	iposs.tipos_documentos_inventarios tdi, 
	iposs.proveedores p, 
	iposs.proveedores_empresas pe, 
	iposs.comprobantes_pendientes cp
where 
		l.codigo = c.loc_codigo 
and 
		cp.comp_fecha_emision = c.fecha_emision 
and 	cp.comp_emp_codigo = c.emp_codigo 
and		cp.comp_codigo = c.codigo 
and 	cp.comp_prov_codigo = c.prov_codigo 
and 	cp.comp_tip_doc_in_codigo = c.tip_doc_in_codigo
and 
		p.codigo = cp.comp_prov_codigo 
and     pe.emp_codigo = cp.comp_emp_codigo 
and 
		c.prov_codigo = pe.prov_codigo 
and 	c.emp_codigo = pe.emp_codigo
and
		tdi.codigo = cp.comp_tip_doc_in_codigo 
and 	pe.fecha_borrado is null 
and 	
		nvl (pe.de_gastos, 0) = 0
and 
		cp.comp_fecha_emision between :FechaDesde and :FechaHasta + 1 - (1/24/60/60)
and 	tdi.codigo = :TipoDocumento 
and 	l.codigo in :Local 
and 
		l.emp_codigo = $EMPRESA_LOGUEADA$
and 	c.emp_codigo = $EMPRESA_LOGUEADA$
and 	pe.emp_codigo = $EMPRESA_LOGUEADA$
and 	cp.comp_emp_codigo = $EMPRESA_LOGUEADA$
