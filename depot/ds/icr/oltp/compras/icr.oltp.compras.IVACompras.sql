select 
	l.codigo 		"Cod.Local (I)", 
	l.descripcion 		"Local", 
	mo.simbolo 		"Moneda", 
	nvl (ca.valor, 1) "Cotiza",  	
	r.descripcion 	"Rubro", 
	i.descripcion 	"Tipo IVA", 
	i.tasa 			"% IVA", 
	sum ( decode (
		cd.comp_tip_doc_in_codigo, 
		2, -cd.total_linea_calculado, 
		6, -cd.total_linea_calculado, 
		7, -cd.total_linea_calculado, 
		cd.total_linea_calculado) ) / (1 + ((decode (pe.factura_sin_impuestos, 1, 0, 1) * i.tasa )/100)) 	"Total sin Imp.", 
	sum ( decode (
		cd.comp_tip_doc_in_codigo, 
		2, -cd.total_linea_calculado, 
		6, -cd.total_linea_calculado, 
		7, -cd.total_linea_calculado, 
		cd.total_linea_calculado) ) / 
		(1 + ((decode (pe.factura_sin_impuestos, 1, 0, 1) * i.tasa )/100)) * 
		((decode (pe.factura_sin_impuestos, 1, 0, 1) * i.tasa )/100)	"IVA compra", 
	sum ( decode (
		cd.comp_tip_doc_in_codigo, 
		2, -cd.total_linea_calculado, 
		6, -cd.total_linea_calculado, 
		7, -cd.total_linea_calculado, 
		cd.total_linea_calculado) ) 	"Total"			-- tomado de la consulta original
FROM 
	iposs.comprobantes c, 
	iposs.comprobantes_detalles cd, 
	iposs.locales l, 
	iposs.monedas mo, 
	iposs.ivas i, 
	iposs.articulos_empresas ae, 
	iposs.rubros r, 
	iposs.proveedores_empresas pe, 
	(select ca.mon_codigo, ca.fecha, ca.vigencia_hasta, ca.valor
		from iposs.cambios ca
		where ca.emp_codigo = $EMPRESA_LOGUEADA$
		and :FechaDesde <= ca.vigencia_hasta
		and :FechaHasta >= ca.fecha) ca
where 	cd.comp_fecha_emision = c.fecha_emision
and 	cd.comp_emp_codigo = c.emp_codigo
and 	cd.comp_codigo = c.codigo
and 
		l.codigo = cd.loc_codigo
and 
		mo.codigo = cd.mon_codigo
and 
		cd.iva_codigo = i.codigo
and 
		ae.art_codigo = cd.art_codigo
and 	ae.emp_codigo = cd.comp_emp_codigo
and 	ae.rub_codigo = r.codigo
and 	
		c.emp_codigo = pe.emp_codigo
and 	c.prov_codigo = pe.prov_codigo 
and 
		(c.fecha_emision between ca.fecha and ca.vigencia_hasta or ca.fecha is null)
and 	c.mon_codigo = ca.mon_codigo(+)
and 
		cd.comp_tip_doc_in_codigo in ('1','2','4','7') 		 -- tomado de la consulta original
and		( trim (upper (':Rubro')) = 'NULL' or r.codigo in :Rubro )
and 	
		nvl (pe.de_gastos, 0) = 0
-- 	cd.comp_fecha_emision between :FechaDesde and :FechaHasta + 1 - (1/24/60/60) 
and 	nvl (c.fecha_contabilidad, c.fecha_emision) between :FechaDesde and :FechaHasta + 1 - (1/24/60/60) 
and 	l.codigo in :Local
and 
		c.emp_codigo = $EMPRESA_LOGUEADA$
and 	cd.comp_emp_codigo = $EMPRESA_LOGUEADA$
and 	l.emp_codigo = $EMPRESA_LOGUEADA$
and 	ae.emp_codigo = $EMPRESA_LOGUEADA$
and 	pe.emp_codigo = $EMPRESA_LOGUEADA$
group by 
	l.codigo, 
	l.descripcion, 
	mo.simbolo, 
	nvl (ca.valor, 1), 
	r.descripcion, 	
	i.tasa, 
	i.descripcion, 
	pe.factura_sin_impuestos
