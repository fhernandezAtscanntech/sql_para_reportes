select 
	l.codigo 		"Cod.Local (I)", 
	l.descripcion 		"Local", 
	mo.simbolo 		"Moneda", 
	i.descripcion 	"Tipo IVA", 
	ae.art_codigo_Externo 	"Cod.Artículo", 
	ae.descripcion 		"Artículo", 
	r.descripcion 		"Rubro", 
	sum ( decode 
		(cd.comp_tip_doc_in_codigo, 
		2, -cd.cantidad, 
		6, -cd.cantidad, 
		7, -cd.cantidad, 
		cd.cantidad) ) 		"Cantidad", 
	sum ( decode (
		cd.comp_tip_doc_in_codigo, 
		2, -cd.total_linea_calculado, 
		6, -cd.total_linea_calculado, 
		7, -cd.total_linea_calculado, 
		cd.total_linea_calculado) ) / (1 + (i.tasa/100)) * (i.tasa/100)	*
		coalesce (
			(select ca.valor_compra
			from iposs.cambios ca
			where ca.mon_codigo = cd.mon_codigo
			and cd.comp_fecha_emision between ca.fecha and ca.vigencia_hasta
			and ca.emp_codigo = cd.comp_emp_codigo
			and rownum = 1),  							-- cotización de la empresa
			(select ca.valor_compra
			from iposs.cambios ca
			where ca.mon_codigo = cd.mon_codigo
			and cd.comp_fecha_emision between ca.fecha and ca.vigencia_hasta
			and ca.emp_codigo = 1
			and rownum = 1),  							-- cotización de la modelo
			1 )				"IVA compra", 
	sum ( decode (
		cd.comp_tip_doc_in_codigo, 
		2, -cd.total_linea_ingresado, 
		6, -cd.total_linea_ingresado, 
		7, -cd.total_linea_ingresado, 
		cd.total_linea_ingresado) )*
		coalesce (
			(select ca.valor_compra
			from iposs.cambios ca
			where ca.mon_codigo = cd.mon_codigo
			and cd.comp_fecha_emision between ca.fecha and ca.vigencia_hasta
			and ca.emp_codigo = cd.comp_emp_codigo
			and rownum = 1),  							-- cotización de la empresa
			(select ca.valor_compra
			from iposs.cambios ca
			where ca.mon_codigo = cd.mon_codigo
			and cd.comp_fecha_emision between ca.fecha and ca.vigencia_hasta
			and ca.emp_codigo = 1
			and rownum = 1),  							-- cotización de la modelo
			1 )				 	"Total Ingresado", 
	sum ( decode (
		cd.comp_tip_doc_in_codigo, 
		2, -cd.total_linea_calculado, 
		6, -cd.total_linea_calculado, 
		7, -cd.total_linea_calculado, 
		cd.total_linea_calculado) )*
		coalesce (
			(select ca.valor_compra
			from iposs.cambios ca
			where ca.mon_codigo = cd.mon_codigo
			and cd.comp_fecha_emision between ca.fecha and ca.vigencia_hasta
			and ca.emp_codigo = cd.comp_emp_codigo
			and rownum = 1),  							-- cotización de la empresa
			(select ca.valor_compra
			from iposs.cambios ca
			where ca.mon_codigo = cd.mon_codigo
			and cd.comp_fecha_emision between ca.fecha and ca.vigencia_hasta
			and ca.emp_codigo = 1
			and rownum = 1),  							-- cotización de la modelo
			1 )				 	"Total Calculado"			-- tomado de la consulta original
from 
	iposs.comprobantes_detalles cd, 
	iposs.locales l, 
	iposs.monedas mo, 
	iposs.ivas i, 
	iposs.articulos_empresas ae, 
	iposs.rubros r, 
	iposs.proveedores_empresas pe
where 	l.codigo = cd.loc_codigo
and		ae.art_codigo = cd.art_codigo
and 	ae.emp_codigo = cd.comp_emp_codigo
and		ae.rub_codigo = r.codigo
and		mo.codigo = cd.mon_codigo
and 	cd.comp_prov_codigo = pe.prov_codigo
and 	cd.comp_emp_codigo = pe.emp_codigo
and 	nvl (pe.de_gastos, 0) = 0
and		cd.iva_codigo = i.codigo
and		cd.comp_tip_doc_in_codigo in ('1','2','4','7') 		 -- tomado de la consulta original
and 	cd.comp_fecha_emision between :FechaDesde and :FechaHasta + 1 - (1/24/60/60) 
and		( trim (upper (':Rubro')) = 'NULL' or r.codigo in :Rubro )
and 	l.codigo in :Local
and	
		( cd.comp_emp_codigo = $EMPRESA_LOGUEADA$
and 	l.emp_codigo = $EMPRESA_LOGUEADA$
and 	ae.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.codigo, 
	l.descripcion, 
	cd.comp_emp_codigo, 
	cd.mon_codigo, 
	cd.comp_fecha_emision, 
	mo.simbolo, 
	i.descripcion, 
	ae.art_codigo_Externo, 
	ae.descripcion, 
	r.descripcion, 
	i.tasa
