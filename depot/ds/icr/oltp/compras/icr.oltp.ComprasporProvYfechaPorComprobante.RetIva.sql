select 
	c.audit_user 		"Usuario",
	l.descripcion 		"Local",
	tdi.descripcion 	"Tipo Doc.", 
	c.notas 			"Notas", 
	c.numero 			"Numero", 
	pe.prov_cod_externo "Cod.Proveedor",
	p.razon_social 		"Razon Social", 
	c.serie 			"Serie", 
	mo.simbolo 			"Moneda", 
	trunc (c.fecha_emision) "Fecha Emision", 
	trunc (c.fecha_contabilidad) "Fecha Contabilidad", 
	decode (
		c.tip_doc_in_codigo, 
		2, -c.total_calculado, 
		6, -c.total_calculado, 
		7, -c.total_calculado, 
		c.total_calculado) 	"Total Calculado", 
	decode (
		c.tip_doc_in_codigo, 
		2, -c.total_ingresado,
		6, -c.total_ingresado,
		7, -c.total_ingresado, 
		c.total_ingresado) 	"Total Ingresado",
	decode (
		c.tip_doc_in_codigo, 
		2, -c.total_iva_calculado, 
		6, -c.total_iva_calculado, 
		7, -c.total_iva_calculado, 
		c.total_iva_calculado) "Total Iva Calculado", 
	c.imeba "Imeba", 
	c.iva_percibido "Iva Percibido", 
	c.iric "Iric", 
	nvl ((select mcp.importe 
		from iposs.movimientos_cc_prov mcp 
		where mcp.emp_codigo = c.emp_codigo
		and mcp.comp_codigo = c.codigo
		and mcp.comp_fecha_emision = c.fecha_emision
		and mcp.prov_codigo = c.prov_codigo
		and mcp.tip_doc_in_codigo = 17), 0) 	"IVA Resguardo"
from iposs.proveedores p,
	iposs.proveedores_empresas pe,
	iposs.comprobantes c,
	iposs.locales l, 
	iposs.monedas mo, 
	iposs.tipos_documentos_inventarios tdi
where 	p.codigo = pe.prov_codigo 
and 	pe.emp_codigo <> 1 
and 	pe.fecha_borrado is null 
and 	p.codigo = c.prov_codigo 
and 	l.codigo = c.loc_codigo 
and 	mo.codigo = c.mon_codigo 
and 	tdi.codigo = c.tip_doc_in_codigo 
and 	c.tip_doc_in_codigo in ('1','2','4','7') 
and 	nvl (pe.de_gastos, 0) = 0
and 	l.codigo in :Local 
and 	(trim (upper (':Proveedor')) = 'NULL' or pe.prov_codigo in :Proveedor)
and 	c.fecha_emision(+) between :FechaDesde and :FechaHasta +1-1/24/60/60
and 	pe.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	c.emp_codigo = $EMPRESA_LOGUEADA$
