select 		
	m.fecha_comercial	"Fecha"	, 
	m.emp_codigo	"Codigo Empresa"	, 
	e.descripcion	"Empresa"	, 
	m.loc_codigo	"Codigo Local"	, 
	l.descripcion	"Local"	, 
	m.caj_codigo	"Codigo Caja"	, 
	c.descripcion	"Caja"	, 
	m.fun_codigo_cajera	"Codigo Cajera"	, 
	(select f.apellido||', '||f.nombre from funcionarios f, funcionarios_locales fl where fl.fun_codigo = f.codigo and fl.loc_codigo = m.loc_codigo and f.fun_cod_empresa = m.fun_codigo_cajera)	"Cajera"	, 
	mp.tip_pag_codigo	"Codigo Tipo de Pago"	, 
	(select t.descripcion from tipos_de_pagos t where t.codigo = mp.tip_pag_codigo)	"Tipo De Pago"	, 
	nvl ((select c.descripcion from creditos c, creditos_locales cl where c.codigo = cl.cre_codigo and cl.cre_codigo_externo = mp.cre_codigo and cl.loc_codigo = m.loc_codigo), 
		nvl ((select v.descripcion from vales_de_compras v, vales_de_compras_locales vl where v.codigo = vl.val_codigo and vl.val_codigo_Externo = mp.val_codigo and vl.loc_codigo = m.loc_codigo), 
		nvl ((select b.descripcion from bancos b where b.codigo = mp.ban_codigo), 
		''))) "Sub tipo", 
	mp.mon_codigo	"Codigo Moneda"	, 
	mp.cotiza_venta	"Cotiza Venta"	, 
	mp.cotiza_compra	"Cotiza Compra"	, 
	mp.ban_codigo	"Codigo Banco"	, 
-- (select b.descripcion from bancos b where b.codigo = mp.ban_codigo)	"Banco"	, 
	mp.val_codigo	"Codigo Vale"	, 
-- (select v.descripcion from vales_de_compras v, vales_de_compras_locales vl where v.codigo = vl.val_codigo and vl.val_codigo_Externo = mp.val_codigo and vl.loc_codigo = m.loc_codigo)	"Vale"	, 
	mp.cre_codigo	"Codigo Credito"	, 
-- (select c.descripcion from creditos c, creditos_locales cl where c.codigo = cl.cre_codigo and cl.cre_codigo_externo = mp.cre_codigo and cl.loc_codigo = m.loc_codigo)	"Credito"	, 
	mp.lote_credito	"Lote Credito"	, 
	mp.comercio_credito	"Comercio Credito"	, 
	mp.terminal_credito	"Terminal Credito"	, 
-- mp.nro_cierre	"Num Cierre"	,
	sum (mp.Importe)	"Importe"	, 
	sum (mp.importe_pago)	"Importe Pago" ,
	count (1)	"Cantidad Pagos" 
from 		
	movimientos m, 
	movimientos_De_pagos mp,
	empresas e,	
	locales l,	
	cajas c
where		
	( m.numero_mov = mp.mov_numero_mov
and 	  m.emp_codigo = mp.mov_emp_codigo
and 	  m.fecha_comercial = mp.fecha_comercial )
and 
	( m.emp_codigo = e.codigo )
and 
	( m.loc_Codigo = l.codigo )
and	
	( m.caj_codigo = c.codigo	
and	  m.loc_Codigo = c.loc_Codigo )
and
	m.fecha_comercial between trunc(:FechaDesde) and :FechaHasta
and	
-- 	( trim (upper (':Local')) = 'NULL' or l.codigo in :Local )
	( l.codigo in :Local )
and
	( :FechaHasta - :FechaDesde <= 31 )
and 
	( (to_char(:FechaDesde, 'hh24:mi:ss') = '00:00:00' 
and  	   to_char(:FechaHasta, 'hh24:mi:ss') = '00:00:00' )
	or
	  (m.fecha_operacion between :FechaDesde and :FechaHasta) 
	)
-- permisos por empresa
and 
	( m.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  mp.mov_emp_codigo = $EMPRESA_LOGUEADA$
and 	  e.codigo = $EMPRESA_LOGUEADA$
and 	  l.emp_codigo = $EMPRESA_LOGUEADA$ )

group by 
	m.fecha_comercial	, 
	m.emp_codigo	, 
	e.descripcion	, 
	m.loc_codigo	, 
	l.descripcion	, 
	m.caj_codigo	, 
	c.descripcion	, 
	m.fun_codigo_cajera, 
	mp.tip_pag_codigo, 
	mp.mon_codigo	, 
	mp.cotiza_venta	, 
	mp.cotiza_compra, 
	mp.ban_codigo	, 
	mp.val_codigo	, 
	mp.cre_codigo	, 
	mp.lote_credito	, 
	mp.comercio_credito, 
	mp.terminal_credito
