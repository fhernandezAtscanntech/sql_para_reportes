select 
	l.descripcion 		"Local", 
	l.loc_cod_empresa 	"Cod.Local", 
	l.emp_codigo 		"Emp.Codigo", 
	s.fecha_comercial 	"Fecha", 
	(select t.descripcion from tipos_de_pagos t where t.codigo = s.tip_pag_codigo)	"Tipo M.Pago"	, 
	nvl ((select c.descripcion from creditos c, creditos_locales cl where c.codigo = cl.cre_codigo and cl.cre_codigo_externo = s.cre_codigo and cl.loc_codigo = s.loc_codigo), 
		nvl ((select v.descripcion from vales_de_compras v, vales_de_compras_locales vl where v.codigo = vl.val_codigo and vl.val_codigo_Externo = s.val_codigo and vl.loc_codigo = s.loc_codigo), 
			nvl ((select b.descripcion from bancos b where b.codigo = s.ban_codigo), ''))) "Subtipo M.Pago", 
	s.mon_codigo	"Codigo Moneda", 
	m.simbolo 	"Moneda", 
	s.Importe	"Importe", 
	s.importe_pago	"Importe Pago", 
	s.cotiza_compra 	"Cotiza Compra", 
	(s.importe_pago * s.cotiza_compra) 	"Importe MN" 
from 
	iposs.locales l, 
	iposs.monedas m, 
	iposs.sum_movimientos_de_pagos s
where	l.codigo = s.loc_codigo
and 	l.emp_codigo = s.mov_emp_codigo 
and		
		m.codigo = s.mon_codigo
and 
		s.fecha_comercial between :FechaDesde and :FechaHasta
and		:FechaHasta - :FechaDesde <= 31
and
		( trim (upper (':Local')) = 'NULL' or l.codigo in :Local )
and 
		( s.mov_emp_codigo = $EMPRESA_LOGUEADA$
and 	  l.emp_codigo  = $EMPRESA_LOGUEADA$ )
