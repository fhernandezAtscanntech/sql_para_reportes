 select 
	tdi.descripcion "Tipo mov.", 
	c.serie 		"Serie", 
	c.numero 		"Numero", 
	p.razon_social 	"Razon Social", 
	m.simbolo 		"Moneda", 
	mcc.fecha_vto 	"Fecha Vto", 
	g.descripcion 	"Grupo", 
	sum (mcc.importe) 	"Importe", 
	sum (mcc.importe_pago) 	"Importe Pago", 
	case 
		when ( nvl (sum (mcc.rec_importe_mon1),0) + nvl (sum (mcc.rec_importe_mon2),0) <> 0 ) 
	then 
		nvl (sum (mcc.rec_importe_mon1), 0) + nvl (sum (mcc.rec_importe_mon2), 0)
	else 
		sum (mcc.importe) 
	end 		"Importe Sin Desc.", 
	sum (mcc.rec_importe_mon2) 	"Recibo me", 
	sum (mcc.rec_importe_mon1) 	"Recibo mn"
from 
	iposs.comprobantes c, 
	iposs.proveedores p, 
	iposs.monedas m, 
	iposs.grupos_de_pagos g, 
	iposs.tipos_documentos_inventarios tdi, 
	iposs.movimientos_cc_prov mcc
where 	m.codigo = mcc.mon_codigo
and 	tdi.codigo = mcc.tip_doc_in_codigo
and 	p.codigo = mcc.prov_codigo
and 	g.codigo = mcc.gru_pag_codigo
and 	(c.codigo(+) = mcc.comp_codigo and c.fecha_emision(+) = mcc.comp_fecha_emision)
and 	mcc.fecha_vto between :FechaDesde and :FechaHasta + (1-(1/24/60/60))
and 	c.emp_codigo = $EMPRESA_LOGUEADA$
and 	mcc.emp_codigo = $EMPRESA_LOGUEADA$
and 	g.emp_codigo = $EMPRESA_LOGUEADA$
group by 
	tdi.descripcion, 
	c.numero, 
	p.razon_social, 
	c.serie, 
	m.simbolo, 
	mcc.fecha_vto, 
	g.descripcion
order by 
	p.razon_social, 
	mcc.fecha_vto, 
	c.serie, 
	c.numero, 
	tdi.descripcion