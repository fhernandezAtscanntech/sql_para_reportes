select 
	trunc(chp.fecha_vto) 	"Fecha Vencimiento", 
	mon.descripcion 		"Moneda", 
	prov.razon_social 		"Proveedor", 
	gp.descripcion 			"Grupo de pago", 
	chp.fecha_emision 		"Fecha Emision", 
	chp.numero_cuenta 		"Numero Cuenta", 
	chp.serie 				"Serie", 
	chp.numero 				"N�mero", 
	ban.descripcion 		"Banco", 
	chp.importe 			"Importe", 
	chp.tipo_cambio 		"Tipo Cambio"
from 
	iposs.monedas mon, 
	iposs.proveedores prov, 
	iposs.grupos_de_pagos gp, 
	iposs.cheques_prov chp, 
	iposs.bancos ban, 
	iposs.bancos_cuentas bc
where 
	( ( mon.codigo = chp.mon_codigo ) and 
	  ( prov.codigo = chp.prov_codigo ) and 
	  ( gp.codigo = chp.gru_pag_codigo ) and 
	  ( ban.codigo = bc.ban_codigo ) and 
	  ( bc.ban_codigo = chp.ban_codigo and 
	    bc.emp_codigo = chp.emp_codigo and 
		bc.gru_pag_codigo = chp.gru_pag_codigo and 
		bc.numero_cuenta = chp.numero_cuenta ) ) and 
	( ban.descripcion <> 'EFECTIVO' ) and 
-- magic
	(( chp.fecha_vto between :FechaDesde and to_date(:FechaHasta) + 1 - (1/24/60/60) and :TipoFecha = 'VENCIMIENTO')
		or
	( chp.fecha_emision between :FechaDesde and to_date(:FechaHasta) + 1 - (1/24/60/60) and :TipoFecha = 'EMISION')) and
	( chp.emp_codigo = $EMPRESA_LOGUEADA$ ) and 
	( gp.emp_codigo = $EMPRESA_LOGUEADA$ ) 
order by 
	trunc(chp.fecha_vto) asc, 
	ban.descripcion asc, 
	chp.numero_cuenta asc, 
	chp.fecha_emision asc
