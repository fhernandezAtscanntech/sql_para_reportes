select
	p.razon_social 		"Razon Social", 
	p.ruc 			"RUT/CUIT", 
	mcc.fecha 		"Fecha", 
	gp.Codigo 		"Cod.Grupo de Pago", 
	gp.descripcion 		"Grupo de Pago", 
	mccrr.numero 		"Doc.Retención", 
	mccrr.tipo_retencion 	"Tipo Retención", 
	mccrr.porcentaje 	"Porcentaje", 
	mon.simbolo 	"Moneda", 
	mccr.importe 	"Importe", 
	sum (mccrr.imp_retencion) 	"Importe Retención", 
	sum (mccrr.imp_retencion * 100 / mccrr.porcentaje) 	"Base Imponible" 
from
	iposs.proveedores p, 
	iposs.grupos_de_pagos gp, 
	iposs.movimientos_cc_prov mcc, 
-- 	iposs.movimientos_cc_prov_recibos mccr, 
-- debo hacer esto por los recibos con dos líneas en movimientos_cc_prov_recibos
		(select rec_numero, rec_fecha, emp_codigo, sum(Importe) Importe
		from iposs.movimientos_cc_prov_recibos
		group by rec_numero, rec_fecha, emp_codigo) mccr, 
	iposs.mov_cc_prov_rec_ret mccrr, 
	iposs.monedas mon
where
	mcc.fecha = mccr.rec_fecha
and 	mcc.numero = mccr.rec_numero
and 	mcc.emp_codigo = mccr.emp_codigo
and
	mcc.fecha = mccrr.mov_cc_pr_fecha
and 	mcc.numero = mccrr.mov_cc_pr_numero
and 	mcc.emp_codigo = mccrr.emp_codigo
and 
	mcc.gru_pag_codigo = gp.codigo
and 	mcc.emp_codigo = gp.emp_codigo
and 
	mcc.prov_codigo = p.codigo
and
	mcc.mon_codigo = mon.codigo
and 
	mcc.fecha between :FechaDesde and :FechaHasta + 1 - (1/24/60/60)
and
	gp.emp_codigo = $EMPRESA_LOGUEADA$
and 	mcc.emp_codigo = $EMPRESA_LOGUEADA$
and 	mccr.emp_codigo = $EMPRESA_LOGUEADA$
and 	mccrr.emp_codigo = $EMPRESA_LOGUEADA$
group by 
	p.razon_social, 
	p.ruc, 
	mcc.fecha, 
	gp.Codigo, 
	gp.descripcion, 
	mccrr.numero, 
	mccrr.tipo_retencion, 
	mccrr.porcentaje, 
	mon.simbolo, 
	mccr.importe
