select 
	gp.descripcion 		"Grupo de Pago", 
	mo.simbolo 		"Moneda", 
	p.razon_social 		"Proveedor", 
	mccp.fecha_vto		"Fecha Vto", 
	tdi.descripcion 	"Tipo Documento", 
	c.serie 		"Serie", 
	c.numero 		"Numero", 
	sum (mccp.importe) 		"Importe", 
	sum (mccp.importe_pago)		"Importe Pago", 
	sum (mccp.rec_importe_mon1) 	"Recibo mn", 
	sum (mccp.rec_importe_mon2) 	"Recibo me", 
	case when 
		(nvl (sum (mccp.rec_importe_mon1), 0) + nvl (sum (mccp.rec_importe_mon2), 0) <> 0 )
	then
		(nvl (sum (mccp.rec_importe_mon1), 0) + nvl (sum (mccp.rec_importe_mon2), 0))
	else
		sum (mccp.importe)
	end 				"Importe Sin Desc."
from 
	iposs.comprobantes c, 
	iposs.monedas mo, 
	iposs.proveedores p, 
	iposs.tipos_documentos_inventarios tdi, 
	iposs.grupos_de_pagos gp, 
	iposs.movimientos_cc_prov mccp
where
	mccp.comp_codigo = c.codigo
and 	mccp.emp_codigo = c.emp_codigo
and 	mccp.comp_fecha_emision = c.fecha_emision
and 
	mccp.mon_codigo = mo.codigo
and 
	mccp.tip_doc_in_codigo = tdi.codigo
and 
	mccp.prov_codigo = p.codigo
and 
	mccp.gru_pag_codigo = gp.codigo
and 
	mccp.comp_codigo is not null
and 
	nvl (mccp.importe, 0) <> nvl (importe_pago, 0)
and 
	mccp.comp_fecha_emision 
		between nvl (:FechaDesde, to_date('01/01/0001', 'dd/mm/yyyy')) 
		and nvl (:FechaHasta, sysdate + 360)
and
	(c.emp_codigo = $EMPRESA_LOGUEADA$
and 	gp.emp_codigo = $EMPRESA_LOGUEADA$
and 	mccp.emp_codigo = $EMPRESA_LOGUEADA$)
group by 
	gp.descripcion, 
	mo.simbolo, 
	p.razon_social, 
	mccp.fecha_vto, 
	tdi.descripcion, 
	c.serie, 
	c.numero
