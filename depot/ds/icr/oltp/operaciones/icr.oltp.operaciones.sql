select 
	o.loc_codigo 		"Codigo Local", 
	l.descripcion 		"Local", 
	o.caj_codigo 		"Codigo Caja", 
	c.descripcion 		"Caja", 
	o.fecha_comercial 	"Fecha", 
	o.numero_operacion 	"Numero Operacion", 
	o.fecha_operacion 	"Fecha Operacion", 
	o.fun_codigo_cajera	"Codigo Cajera"	, 
	(select f.apellido||', '||f.nombre  from funcionarios f, funcionarios_locales fl where fl.fun_codigo = f.codigo and fl.loc_codigo = l.codigo and f.fun_cod_empresa = o.fun_codigo_cajera)	"Cajera", 
	o.fun_codigo_cajera	"Codigo Funcionario"	, 
	(select f.apellido||', '||f.nombre  from funcionarios f, funcionarios_locales fl where fl.fun_codigo = f.codigo and fl.loc_codigo = l.codigo and f.fun_cod_empresa = o.fun_codigo)	"Funcionario", 
	o.descripcion 		"Operacion", 
	o.nro_z 		"Nro.Z", 
	o.importe		"Importe"
from 
	operaciones o, 
	locales l, 
	cajas c
where
	o.loc_codigo = l.codigo
and
	o.caj_codigo = c.codigo
and 	o.loc_codigo = c.loc_codigo
and
	:FechaHasta - :FechaDesde <= 31
and
	o.fecha_comercial between :FechaDesde and :FechaHasta
and	
	( trim (upper (':Local')) = 'NULL' or l.codigo in :Local )
