select 
	l.descripcion 		"Local", 
	l.codigo 			"Cod.Local (I)", 
	l.loc_cod_empresa 	"Cod.Local", 
	l.emp_codigo 		"Emp.Codigo", 
	e.descripcion 		"Empresa", 
	c.codigo 			"Cod.Caja", 
	c.descripcion 		"Caja",
	f_caj.apellido||', '||f_caj.nombre 	"Cajero", 
	f_aut.apellido||', '||f_aut.nombre 	"Autorizador", 
	m.fecha_comercial 	"Fecha", 
	m.observacion 		"Observación", 
	m.numero_operacion 	"Nro.Operacion", 
	m.fecha_operacion 	"Fecha Operación", 
	m.total 			"Total"
from 
	iposs.locales l, 
	iposs.empresas e, 
	iposs.cajas c, 
	IPOSS.movimientos m, 
	iposs.funcionarios f_caj, 
	iposs.funcionarios f_aut
where
		( l.codigo = m.loc_codigo 
and 	  l.emp_codigo = m.emp_codigo )
and 
		( c.codigo = m.caj_codigo
and 	  c.loc_codigo = m.loc_codigo )
and 	( e.codigo = l.emp_codigo 
and 	  e.codigo = m.emp_codigo )
and 
		( m.fun_codigo_cajera = f_caj.fun_cod_empresa
and 	  m.emp_codigo = f_caj.emp_codigo )
and 	
		( m.fun_codigo_autoriza = f_aut.fun_cod_empresa (+)
and 	  m.emp_codigo = f_aut.emp_codigo (+) )
and 
		( m.tipo_operacion = 'VENTA ANULADA' )
and  
		( m.fecha_comercial between :FechaDesde and :FechaHasta 
and 	  :FechaHasta - :FechaDesde <= 31 )
and 
	( l.codigo in :Local )
and 
	l.emp_codigo = $EMPRESA_LOGUEADA$
and 	e.codigo = $EMPRESA_LOGUEADA$
and 	m.emp_codigo = $EMPRESA_LOGUEADA$
