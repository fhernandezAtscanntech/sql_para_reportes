select 
	l.codigo 		"Cod.Local (I)", 
	l.loc_cod_empresa 	"Cod.Local", 
	l.descripcion 		"Local", 
	frv.categoria 		"categoria", 
	frv.categoria_provincial 	"Categoria Provinicial", 
	sum (frv.percepcion_iva) 	"Percepción IVA", 
	sum (frv.importe_ingresos_brutos) 	"Importe Ingresos Brutos", 
	sum (frv.importe_retencion) 		"Importe Retención",  
	sum (frv.importe_alicuota_iva) 		"Importe Alicuota IVA", 
	sum (frv.importe_imp_internos) 		"Importe Imp Internos", 
	sum (frv.neto_iva_0) 	"Neto Sin IVA", 
	sum (frv.neto_iva_1) 	"Neto IVA 1", 
	sum (frv.neto_iva_2) 	"Neto IVA 2", 
	sum (frv.neto_iva_3) 	"Neto IVA 3", 
	sum (frv.neto_iva_4) 	"Neto IVA 4", 
	sum (frv.neto_iva_5) 	"Neto IVA 5", 
	sum (frv.iva_1) 	"IVA 1", 
	sum (frv.iva_2) 	"IVA 2", 
	sum (frv.iva_3) 	"IVA 3", 
	sum (frv.iva_4) 	"IVA 4", 
	sum (frv.iva_5) 	"IVA 5", 
	nvl (sum (frv.iva_1), 0) + 
		nvl (sum (frv.iva_2), 0) + 
		nvl (sum (frv.iva_3), 0) + 
		nvl (sum (frv.iva_4), 0) + 
		nvl (sum (frv.iva_5), 0) + 
		nvl (sum (frv.neto_iva_1), 0) + 
		nvl (sum (frv.neto_iva_2), 0) +
		nvl (sum (frv.neto_iva_3), 0) + 
		nvl (sum (frv.neto_iva_4), 0) + 
		nvl (sum (frv.neto_iva_5), 0) + 
		nvl (sum (frv.neto_iva_0), 0) + 
		nvl (sum (frv.importe_imp_internos), 0) 	"Total" 
from 
	iposs.locales l, 
	iposs.fis_resumenes_ventas frv
where 
	l.emp_codigo = frv.emp_codigo 
and 
	l.codigo = frv.loc_codigo 
and 
	frv.fecha_comercial between :FechaDesde and :FechaHasta
-- and 
-- 	and :FechaHasta - :FechaDesde <= 31
and
	l.codigo in :Local
group by 
	l.codigo, 
	l.loc_cod_empresa, 
	l.descripcion, 
	frv.categoria, 
	frv.categoria_provincial
