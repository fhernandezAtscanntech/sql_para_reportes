select 
	l.codigo 		"Cod.Local (I)", 
	l.loc_cod_empresa 	"Cod.Local", 
	l.descripcion 		"Local", 
	c.codigo 		"Cod.Caja", 
	c.descripcion 		"Caja", 
	frv.fecha_comercial 	"Fecha comercial", 
	frv.registradora 	"Registradora", 
	frv.ruc_factura 	"CUIT", 
	frv.nombre_factura 	"Nombre", 
	case when ruc_factura is null then frv.nro_cierre else frv.nro_ope_desde end 	"Numero", 
	frv.tipo_de_documento 	"Tipo de Documento", 
	frv.tipo_de_factura 	"Tipo de Factura", 
	frv.categoria 		"Categor�a", 
	frv.categoria_provincial 	"Categoria Provinicial", 
	sum (frv.percepcion_iva) 	"Percepci�n IVA", 
	sum (frv.importe_ingresos_brutos) 	"Importe Ingresos Brutos", 
	sum (frv.importe_retencion) 		"Importe Retenci�n",  
	sum (frv.importe_alicuota_iva) 		"Importe Alicuota IVA", 
	sum (frv.importe_imp_internos) 		"Importe Imp Internos", 
	sum (frv.neto_iva_0) 	"Neto Sin IVA", 
	sum (frv.neto_iva_1) 	"Neto IVA 1", 
	sum (frv.neto_iva_2) 	"Neto IVA 2", 
	sum (frv.neto_iva_3) 	"Neto IVA 3", 
	sum (frv.neto_iva_4) 	"Neto IVA 4", 
	sum (frv.neto_iva_5) 	"Neto IVA 5", 
	sum (frv.iva_1) 	"IVA 1", 
	sum (frv.iva_2) 	"IVA 2", 
	sum (frv.iva_3) 	"IVA 3", 
	sum (frv.iva_4) 	"IVA 4", 
	sum (frv.iva_5) 	"IVA 5", 
	nvl (sum (frv.iva_1), 0) + 
		nvl (sum (frv.iva_2), 0) + 
		nvl (sum (frv.iva_3), 0) + 
		nvl (sum (frv.iva_4), 0) + 
		nvl (sum (frv.iva_5), 0) + 
		nvl (sum (frv.neto_iva_1), 0) + 
		nvl (sum (frv.neto_iva_2), 0) +
		nvl (sum (frv.neto_iva_3), 0) + 
		nvl (sum (frv.neto_iva_4), 0) + 
		nvl (sum (frv.neto_iva_5), 0) + 
		nvl (sum (frv.neto_iva_0), 0) + 
		nvl (sum (frv.importe_imp_internos), 0) 	"Total" 
from 
	iposs.locales l, 
	iposs.cajas c, 
	iposs.fis_resumenes_ventas frv
where 
	l.emp_codigo = frv.emp_codigo 
and 
	l.codigo = frv.loc_codigo 
and 
	c.codigo = frv.caj_codigo
and 	c.loc_Codigo = frv.loc_codigo
and 
	frv.fecha_comercial between :FechaDesde and :FechaHasta
and 
	:FechaHasta - :FechaDesde <= 31
and
	l.codigo in :Local
and
	( l.emp_codigo = $EMPRESA_LOGUEADA$
and 	  frv.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.codigo, 
	l.loc_cod_empresa, 
	l.descripcion, 
	c.codigo, 
	c.descripcion, 
	frv.fecha_comercial, 
	frv.registradora, 
	frv.ruc_factura, 
	frv.nombre_factura, 
	case when ruc_factura is null then frv.nro_cierre else frv.nro_ope_desde end, 
	frv.tipo_de_documento, 
	frv.tipo_de_factura, 
	frv.categoria, 
	frv.categoria_provincial
