select
	c.descripcion 	"Caja",
	c.codigo 		"Caj.Codigo", 
	l.descripcion 	"Local",
	l.codigo 	"Cod.Local (I)", 
	l.loc_cod_empresa 	"Cod.Local", 	
	ml.fecha_hasta 	"Fecha", 
	ml.nro_ope_hasta "Nro.Operacion Hasta",
	ml.nro_z 	"Nro. Z",
	sum(ml.gran_total_bon) 	"GTBon",
	sum(ml.gran_total_dev_ser) 	"GTDev.Serv",
	sum(ml.gran_total_dev) 	"GTDev",
	sum(ml.gran_total_ser) 	"GTServ",
	sum(ml.gran_total) 	"GTotal"
from
	iposs.cajas c, 
	iposs.locales l,
	iposs.movimientos_lotes ml
where 
	l.codigo = c.loc_codigo
and 
	c.codigo = ml.caj_codigo 
and 	c.loc_codigo = ml.caj_loc_codigo 
and 
	ml.fecha_hasta >= :Fecha - 1 
and 
	( trim (upper (':Local')) = 'NULL' or l.codigo in :Local )
and 	
	( l.emp_codigo = $EMPRESA_LOGUEADA$
and 	  ml.emp_codigo = $EMPRESA_LOGUEADA$ )
group by
	c.descripcion 		,
	c.codigo 		, 
	l.descripcion 		,
	l.codigo 		, 
	l.loc_cod_empresa 	, 	
	ml.fecha_hasta 		, 
	ml.nro_ope_hasta 	,
	ml.nro_z 		,
