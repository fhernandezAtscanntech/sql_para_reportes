select
	distinct tdi.codigo "Codigo", tdi.descripcion "Tipo Documento"
from 
	iposs.tipos_documentos_inventarios tdi
order by 
	tdi.descripcion