select
	distinct ae.art_codigo, ae.art_codigo_externo || ' - ' || ae.descripcion descripcion
from 
	iposs.articulos_empresas ae 
	articulos_locales al, 
	#LocalesVisibles# lv
where 	ae.art_codigo = al.art_codigo
and 	al.loc_codigo = lv.loc_codigo
and 	al.fecha_borrado is null 
and 	ae.fecha_borrado is null
