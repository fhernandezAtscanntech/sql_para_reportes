select
	distinct c.codigo, c.cre_cod_empresa || ' - ' || c.descripcion descripcion
from 
	creditos c, 
	creditos_locales cl, 
	#LocalesVisibles# lv
where 
	c.codigo = cl.cre_codigo
and 	cl.loc_codigo = lv.loc_codigo
and
	c.tcr_codigo = 99 		-- Tipo cr�dito = Externo

