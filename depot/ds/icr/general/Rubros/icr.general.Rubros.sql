select
	distinct r.codigo, r.rub_cod_empresa || ' - ' || r.descripcion descripcion
from 
	rubros r, 
	rubros_locales rl, 
	#LocalesVisibles# lv
where 
	(r.codigo = rl.rub_codigo
and 	rl.loc_codigo = lv.loc_codigo
and 	rl.fecha_borrado is null )
