select
	distinct g.codigo 	"Codigo", 
	g.gru_cli_cod_empresa 	"Cod. Empresa", 
	g.gru_cli_cod_empresa || ' - ' || g.Descripcion "Grupo"
from 
	grupos_de_clientes g
where 
	g.emp_codigo = $EMPRESA_LOGUEADA$
order by 
	g.gru_cli_cod_Empresa