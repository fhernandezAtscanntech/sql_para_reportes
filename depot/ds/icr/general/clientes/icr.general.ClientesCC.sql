select
	distinct c.codigo, c.cli_cod_Externo, c.cli_cod_externo || ' - ' || cc.numero_tarjeta || ' - '|| c.nombre Cliente
from 
	clientes c, 
	clientes_locales cl, 
	cuentas_corrientes cc, 
	#LocalesVisibles# lv
where 
	(c.codigo = cl.cli_codigo
and 	 c.codigo = cc.cli_codigo
and 	 cl.fecha_borrado is null
and 	 cl.loc_codigo = lv.loc_codigo)
order by 
	c.cli_cod_Externo