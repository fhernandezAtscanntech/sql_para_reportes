select
	distinct c.codigo, c.cli_cod_externo, c.cli_cod_externo || ' - ' || c.nombre Cliente
from 
	clientes c, 
	clientes_locales cl, 
	#LocalesVisibles# lv
where 
	(c.codigo = cl.cli_codigo
and 	 cl.loc_codigo = lv.loc_codigo
and 	 cl.fecha_borrado is null )
order by 
	c.cli_cod_Externo