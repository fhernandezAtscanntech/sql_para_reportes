select
	distinct s.codigo, s.ser_cod_empresa || ' - ' || s.descripcion descripcion
from 
	iposs.servicios s, 
	iposs.servicios_locales sl, 
	#LocalesVisibles# lv
where 
	(s.codigo = sl.ser_codigo
and 	sl.loc_codigo = lv.loc_codigo
and 	sl.fecha_borrado is null )
