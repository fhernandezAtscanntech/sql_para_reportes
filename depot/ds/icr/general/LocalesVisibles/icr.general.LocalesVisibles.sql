select 
	distinct loc_codigo
from 
	funcionarios_locales
where 
	(emp_codigo = $EMPRESA_LOGUEADA$)
and 	((fun_codigo = $CODIGO_FUNCIONARIO_LOGUEADO$
	and 	sel = 1
	and fecha_borrado is null) 
	OR
		($CODIGO_FUNCIONARIO_LOGUEADO$ in 
			(select codigo
			from funcionarios
			where emp_codigo = 1
			and login like 'SC%'
			and fecha_borrado is null)))
