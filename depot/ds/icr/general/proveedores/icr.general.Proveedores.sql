select
	distinct p.codigo,
  p.razon_social ||' - ' || p.ruc ||' ( '|| pe.prov_Cod_externo ||')'
from 
	proveedores p, 
	proveedores_empresas pe, 
	proveedores_locales pl, 
	#LocalesVisibles lv
where 
	( p.codigo = pe.prov_Codigo )
and
	( p.codigo = pl.prov_codigo )
and
	( pl.fecha_borrado is null )
and 
	( pl.loc_codigo = lv.loc_codigo)
and
	( pe.emp_codigo = $EMPRESA_LOGUEADA$ )
order by 
	p.razon_social ||' - ' || p.ruc ||' ( '|| pe.prov_Cod_externo ||')'