select
	l.codigo	"Codigo",
  
	l.lis_pre_ve_cod_empresa || ' - ' || l.descripcion descripcion 	"Descripción
from 
	listas_de_precios_ventas l, 
	listas_pre_ventas_locales lp, 
	#LocalesVisibles# lv
where 
	lp.lis_pre_ve_codigo = l.codigo
and 	lp.loc_codigo = lv.loc_codigo
and 	lp.fecha_borrado is null

