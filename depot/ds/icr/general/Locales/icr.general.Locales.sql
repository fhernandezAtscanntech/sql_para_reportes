select 
	l.codigo "Codigo", 
	l.loc_cod_empresa || ' - ' || l.descripcion "Descripcion"
from 
	locales l, 
	#LocalesVisibles# lv
where 	l.codigo = lv.loc_codigo
and 	l.fecha_borrado is null
