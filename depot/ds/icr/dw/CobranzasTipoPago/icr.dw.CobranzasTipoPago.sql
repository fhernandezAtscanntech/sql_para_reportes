select 
-- 	l.loc_modelo_descripcion 	"Local", 
	l.loc_descripcion 		"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	(select distinct f.ano_nombre 
		from dwm_prod.l_fechas f 
		where f.mes_codigo = fvm.mes_codigo) 	"A�o", 
	(select distinct f.mes_nombre_corto 
		from dwm_prod.l_fechas f 
		where f.mes_codigo = fvm.mes_codigo) 	"Mes", 
	lis.lis_pre_descripcion  	"Lista de Precios", 
	m.tip_descripcion 		"Tipo M.Pago", 
	m.sub_descripcion 		"Subtipo M.Pago", 
	m.cuo_cantidad 			"Cant.Cuotas", 
	sum (nvl (fVm.ven_nac_importe, 0)) - 
		sum (nvl (fvm.dev_nac_importe, 0))  	"Venta"
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.l_listas_de_precios lis, 
	dwm_prod.l_medios m, 
	dwm_prod.f_ventas_mensual fvm
where
	( l.codigo = fvm.l_ubi_codigo 
and 	l.emp_codigo = fvm.emp_codigo ) 
and 
	( lis.codigo = fvm.l_lis_pre_codigo 
and 	  lis.emp_codigo = fvm.emp_codigo )
and 	
	( m.codigo = fvm.l_med_codigo 
and 	  m.emp_codigo = fvm.emp_codigo )
and
	( trim (upper (':MES')) = 'NULL' or fvm.mes_codigo in :MES )
and
	( trim (upper (':LOCAL')) = 'NULL' or l.loc_codigo in :LOCAL )
and 
	( fvm.emp_codigo = $EMPRESA_LOGUEADA$
and 	  l.emp_codigo  = $EMPRESA_LOGUEADA$
and 	  m.emp_codigo  = $EMPRESA_LOGUEADA$
and 	  lis.emp_codigo  = $EMPRESA_LOGUEADA$ )

group by 
	l.loc_descripcion 	, 
	l.loc_codigo 			, 
	l.emp_codigo 			, 
	fvm.mes_codigo 			, 
	lis.lis_pre_descripcion  	, 
	m.tip_descripcion 		, 
	m.sub_descripcion 		, 
	m.cuo_cantidad
