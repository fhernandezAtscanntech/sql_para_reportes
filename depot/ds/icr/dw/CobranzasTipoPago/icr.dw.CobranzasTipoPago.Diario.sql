select /*+ ordered  */
-- 	l.loc_modelo_descripcion 	"Local", 
	l.loc_descripcion 		"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	f.ano_nombre 			"A�o", 
	f.mes_nombre_corto 		"Mes", 
	f.dia_del_mes 			"D�a", 
	lis.lis_pre_descripcion  	"Lista de Precios", 
	m.tip_descripcion 		"Tipo M.Pago", 
	m.sub_descripcion 		"Subtipo M.Pago", 
	m.cuo_cantidad 			"Cant.Cuotas", 
	sum (nvl (fvd.ven_nac_importe, 0)) - 
		sum (nvl (fvd.dev_nac_importe, 0))  	"Venta"
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.l_fechas f, 
	dwm_prod.l_listas_de_precios lis, 
	dwm_prod.l_medios m, 
	dwm_prod.f_ventas_diario_min fvd
where
	( l.codigo = fvd.l_ubi_codigo 
and 	l.emp_codigo = fvd.emp_codigo ) 
and 
	( lis.codigo = fvd.l_lis_pre_codigo 
and 	  lis.emp_codigo = fvd.emp_codigo )
and 	
	( m.codigo = fvd.l_med_codigo 
and 	  m.emp_codigo = fvd.emp_codigo )
and
	( fvd.fecha_comercial = f.codigo )
and	( fvd.fecha_comercial between :FechaDesde and :FechaHasta)
and	( :FechaHasta - :FechaDesde <= 31 )
and
	( trim (upper (':Local')) = 'NULL' or l.loc_codigo in :Local )
and 
	( fvd.emp_codigo = $EMPRESA_LOGUEADA$
and 	  l.emp_codigo  = $EMPRESA_LOGUEADA$
and 	  m.emp_codigo  = $EMPRESA_LOGUEADA$
and 	  lis.emp_codigo  = $EMPRESA_LOGUEADA$ )

group by 
	l.loc_descripcion 	, 
	l.loc_codigo 			, 
	l.emp_codigo 			, 
	f.ano_nombre 			, 
	f.mes_nombre_corto 		, 
	f.dia_del_mes 			, 	
	lis.lis_pre_descripcion , 
	m.tip_descripcion 		, 
	m.sub_descripcion 		, 
	m.cuo_cantidad
