select 
	l.loc_descripcion 	"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	f.ano_nombre 			"A�o", 
	f.mes_nombre_corto 		"Mes", 
	f.dia_del_mes 			"D�a", 
	c.cli_cod_empresa 		"Cod.Cliente", 
	c.cli_descripcion 		"Cliente", 
	c.gru_cli_cod_empresa 		"Cod.Grupo", 
	c.gru_cli_descripcion 		"Grupo de Clientes", 
	sum (nvl (flcvd.mov_cantidad, 0)) 	"Cant. Tickets", 
	sum (nvl (flcvd.ven_cantidad, 0)) - sum (nvl (flcvd.dev_cantidad, 0)) 	"Unidades", 
	sum (nvl (flcvd.ven_nac_importe, 0)) 	"Venta Total", 
	sum (nvl (flcvd.dev_nac_importe, 0)) 	"Devol", 
	sum (nvl (flcvd.bon_nac_importe, 0)) 	"Bonif", 
	sum (nvl (flcvd.des_prom_nac_importe, 0)) 	"Prom", 
	sum (nvl (flcvd.ven_nac_importe, 0)) - 
		sum (nvl (flcvd.dev_nac_importe, 0)) -
		sum (nvl (flcvd.cos_nac_ult_impuestos, 0)) 	"Utilidad", 
	sum (nvl (flcvd.cos_nac_ult_impuestos, 0))	"Costo Ultimo", 
	sum (nvl (flcvd.iva_importe, 0)) 	"IVA", 
	sum (nvl (flcvd.ven_nac_importe, 0)) - 
		sum (nvl (flcvd.dev_nac_importe, 0))  	"Venta"
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.l_fechas f, 
	dwm_prod.l_clientes c, 
	dwm_prod.l_productos p, 
	dwm_prod.f_ventas_local_cli_diario_min flcvd
where
	( l.codigo = flcvd.l_ubi_codigo 
and 	l.emp_codigo = flcvd.emp_codigo ) 
and	
	( p.codigo = flcvd.l_pro_codigo 
and 	  p.emp_codigo in (flcvd.emp_codigo, $EMPRESA_MODELO$) )
and
	( c.codigo = flcvd.l_cli_codigo 
and 	  c.emp_codigo = flcvd.emp_codigo )
and 
	( flcvd.fecha_comercial = f.codigo )
and
	( flcvd.fecha_comercial between :FechaDesde and :FechaHasta)
and
	( :FechaHasta - :FechaDesde <= 31 )
and (
	p.tip_prod_codigo in (1, 3) 
	or 
	( p.tip_prod_codigo = 2 
	and ( trim (upper (':ExclServicios')) = 'NULL' or to_number(p.prod_codigo) not in :ExclServicios ))
	) 
and
	( l.loc_codigo in :Local )
and 
	( l.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  c.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  flcvd.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.loc_descripcion , 
	l.loc_codigo 		, 
	l.emp_codigo 		, 
	f.ano_nombre 		, 
	f.mes_nombre_corto 	, 
	f.dia_del_mes 		,
	c.cli_cod_empresa 	, 
	c.cli_descripcion 	, 
	c.gru_cli_cod_empresa 	, 
	c.gru_cli_descripcion
