select 
	-- transcripto de Discoverer
	p.prod_descripcion 	"Producto", 
	prom.prom_descripcion 	"Promocion", 
	prom.cpa_descripcion 	"Campa�a", 
	l.loc_descripcion 	"Local", 
	sum (nvl(fvpd.prom_cantidad, 0)) 	"Cantidad", 
	sum (nvl(fvpd.ven_cantidad, 0)) 	"Cantidad Producto", 
	sum (nvl(fvpd.bon_nac_importe, 0)) 	"Bonificacion", 
	sum (nvl(fvpd.des_prom_nac_importe, 0)) 	"Desc. Promocion", 
	sum (nvl(fvpd.ven_nac_importe, 0)) 	"Ventas"
from 
	dwm_prod.f_ventas_promo_diario_min fvpd, 
	dwm_prod.l_productos p, 
	dwm_prod.l_promociones prom, 
	dwm_prod.l_ubicaciones l
where 
	( ( p.emp_codigo = fvpd.emp_codigo 
and 	    p.codigo = fvpd.l_pro_codigo ) 
and 	  ( prom.codigo = fvpd.l_prom_codigo ) 
and 	  ( l.codigo = fvpd.l_ubi_codigo ) ) 
and 
	( p.tip_prod_codigo in (1, 3) )
and 
        ( prom.prom_codigo <> 0 ) 
and 
        ( fvpd.fecha_comercial between :FechaDesde and :FechaHasta ) 
and 
        ( l.loc_codigo in :Local )
and 
	( l.emp_codigo = $EMPRESA_LOGUEADA$
and 	  p.emp_codigo = $EMPRESA_LOGUEADA$
and 	  fvpd.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	p.prod_descripcion, 
	prom.prom_descripcion, 
	prom.cpa_descripcion, 
	l.loc_descripcion
