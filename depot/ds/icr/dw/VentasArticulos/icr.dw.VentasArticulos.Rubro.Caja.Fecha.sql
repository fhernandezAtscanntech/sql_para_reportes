select 
	l.loc_descripcion 	"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	c.caj_codigo 			"Cod.Caja", 
	c.caj_descripcion 		"Caja", 
	f.ano_nombre 			"A�o", 
	f.mes_nombre_corto 		"Mes", 
	f.dia_del_mes 			"D�a", 
	fu.fun_descripcion 		"Cajero", 
 	p.prod_codigo 			"Codigo", 
 	p.prod_descripcion 		"Articulo", 
	p.rub_descripcion 		"Rubro", 
	p.tip_prod_descripcion 		"Tipo producto", 
	sum (nvl (fvd.ven_cantidad, 0)) - sum (nvl (fvd.dev_cantidad, 0)) 	"Unidades", 
	sum (nvl (fvd.ven_nac_importe, 0)) 	"Venta Total", 
	sum (nvl (fvd.dev_nac_importe, 0)) 	"Devol", 
	sum (nvl (fvd.bon_nac_importe, 0)) 	"Bonif", 
	sum (nvl (fvd.des_prom_nac_importe, 0)) 	"Prom", 
	sum (nvl (fvd.ven_nac_importe, 0)) - 
		sum (nvl (fvd.dev_nac_importe, 0))  -
		(sum (nvl (fvd.cos_nac_ult_impuestos, 0))) 	"Utilidad", 
	sum (nvl (fvd.cos_nac_ult_impuestos, 0))	"Costo Ultimo", 
	sum (nvl (fvd.iva_importe, 0)) 	"IVA", 
	sum (nvl (fvd.ven_nac_importe, 0)) - 
		sum (nvl (fvd.dev_nac_importe, 0))  	"Venta"
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.l_cajas c, 
	dwm_prod.l_fechas f, 
	dwm_prod.l_productos p, 
	dwm_prod.l_funcionarios fu, 
	dwm_prod.f_ventas_diario fvd
where
	( l.codigo = fvd.l_ubi_codigo 
and 	  l.emp_codigo = fvd.emp_codigo ) 
and
	( c.codigo = fvd.l_caj_codigo 
and 	  c.emp_codigo = fvd.emp_codigo)
and 
	( p.codigo = fvd.l_pro_codigo 
and 	  p.emp_codigo = fvd.emp_codigo )
and
	( fu.codigo = fvd.l_fun_codigo_cajero 
and 	  fu.emp_codigo = fvd.emp_codigo )
and 
	( fvd.fecha_comercial = f.codigo )
and
	( fvd.fecha_comercial between :FechaDesde and :FechaHasta)
and
	:FechaHasta - :FechaDesde <= 31
and 
	( p.rub_codigo in :Rubro )
and
	( l.loc_codigo in :Local )
and
	( p.tip_prod_codigo in (1, 3) )
and 
	( l.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  c.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  p.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  fvd.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.loc_descripcion , 
	l.loc_codigo 		, 
	l.emp_codigo 		, 
	c.caj_codigo 		, 
	c.caj_descripcion 	, 
	f.ano_nombre 		, 
	f.mes_nombre_corto 	, 
	f.dia_del_mes 		,
	fu.fun_descripcion  	, 
	p.prod_codigo 		, 
 	p.prod_descripcion 	, 
	p.rub_descripcion 	, 
 	p.tip_prod_descripcion
