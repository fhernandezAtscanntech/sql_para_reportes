select 
	l.loc_descripcion 	"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	f.ano_nombre 			"A�o", 
	f.mes_nombre_corto 		"Mes", 
	f.dia_del_mes 			"D�a", 
	p.prod_codigo 			"Codigo", 
	p.prod_descripcion 		"Articulo", 
	p.rub_descripcion 		"Rubro", 
	p.fam_descripcion 		"Familia", 
	p.sub_descripcion 		"Subfamilia", 
	sum (nvl (fvpu.ven_cantidad, 0)) - sum (nvl (fvpu.dev_cantidad, 0)) 	"Unidades", 
	sum (nvl (fvpu.ven_nac_importe, 0)) 	"Venta Total", 
	sum (nvl (fvpu.dev_nac_importe, 0)) 	"Devol", 
	sum (nvl (fvpu.bon_nac_importe, 0)) 	"Bonif", 
	sum (nvl (fvpu.des_prom_nac_importe, 0)) 	"Prom", 
	sum (nvl (fvpu.ven_nac_importe, 0)) - 
		sum (nvl (fvpu.dev_nac_importe, 0))  -
		(sum (nvl (fvpu.cos_nac_ult_impuestos, 0))) 	"Utilidad", 
	sum (nvl (fvpu.cos_nac_ult_impuestos, 0))	"Costo Ultimo", 
	sum (nvl (fvpu.iva_importe, 0)) 	"IVA", 
	sum (nvl (fVpu.ven_nac_importe, 0)) - 
		sum (nvl (fvpu.dev_nac_importe, 0))  	"Venta"
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.l_fechas f, 
	dwm_prod.l_productos p, 
	dwm_prod.f_ventas_pu_diario fvpu
where
	( l.codigo = fvpu.l_ubi_codigo 
and 	l.emp_codigo = fvpu.emp_codigo ) 
and
	( p.codigo = fvpu.l_pro_codigo 
and 	  p.emp_codigo = fvpu.emp_codigo )
and
	( fvpu.fecha_comercial = f.codigo )
and
	( fvpu.fecha_comercial between :FechaDesde and :FechaHasta)
and
	( :FechaHasta - :FechaDesde <= 31 )
and
	( l.loc_codigo in :Local )
and 
	( p.fam_codigo in :Familia )
and
	( p.tip_prod_codigo in (1, 3) )
and 
	( l.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  p.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  fvpu.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.loc_descripcion , 
	l.loc_codigo 		, 
	l.emp_codigo 		, 
	f.ano_nombre 		, 
	f.mes_nombre_corto 	, 
	f.dia_del_mes 		,
	p.prod_codigo 		, 
	p.prod_descripcion 	, 
	p.rub_descripcion 	, 
 	p.fam_descripcion 	, 
	p.sub_descripcion
