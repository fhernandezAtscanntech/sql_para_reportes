select 
	l.loc_descripcion 	"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	f.ano_nombre 			"A�o", 
	f.mes_nombre_corto 		"Mes", 
	f.dia_del_mes 			"D�a", 
	h.hor_hora			"Hora", 
	p.prod_codigo 			"Codigo", 
	p.prod_descripcion 		"Articulo", 
	p.rub_descripcion 		"Rubro", 
	p.tip_prod_descripcion 		"Tipo producto", 
	sum (nvl (fv.ven_cantidad, 0)) - sum (nvl (fv.dev_cantidad, 0)) 	"Unidades", 
	sum (nvl (fv.ven_nac_importe, 0)) 	"Venta Total", 
	sum (nvl (fv.dev_nac_importe, 0)) 	"Devol", 
	sum (nvl (fv.bon_nac_importe, 0)) 	"Bonif", 
	sum (nvl (fv.des_prom_nac_importe, 0)) 	"Prom", 
	sum (nvl (fv.ven_nac_importe, 0)) - 
		sum (nvl (fv.dev_nac_importe, 0))  -
		(sum (nvl (fv.cos_nac_ult_impuestos, 0)))	"Utilidad", 
	sum (nvl (fv.cos_nac_ult_impuestos, 0)) 	"Costo Ultimo", 
	sum (nvl (fv.iva_importe, 0)) 	"IVA", 
	sum (nvl (fv.ven_nac_importe, 0)) - 
		sum (nvl (fv.dev_nac_importe, 0))  	"Venta"
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.l_fechas f, 
	dwm_prod.l_horas h, 
	dwm_prod.l_productos p, 
	dwm_prod.f_ventas fv
where
	( l.codigo = fv.l_ubi_codigo 
and 	l.emp_codigo = fv.emp_codigo ) 
and
	( p.codigo = fv.l_pro_codigo 
and 	  p.emp_codigo = fv.emp_codigo )
and
	( fv.fecha_comercial = f.codigo )
and
	( fv.l_hor_codigo = h.codigo )
and
	( f.mes_codigo = :MES )
and
	( l.loc_codigo in :LOCAL )
and 
-- 	( p.rub_codigo in nvl (:RUBRO, p.rub_codigo) )
	( trim (upper (':RUBRO')) = 'NULL' or p.rub_codigo in :RUBRO )
and
	( p.tip_prod_codigo in (1, 3) )
and 
	( l.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  p.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  fv.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.loc_descripcion , 
	l.loc_codigo 		, 
	l.emp_codigo 		, 
	f.ano_nombre 		, 
	f.mes_nombre_corto 	, 
	f.dia_del_mes 		,
	h.hor_hora 		, 
	p.prod_codigo 		, 
	p.prod_descripcion 	, 
	p.rub_descripcion	, 
	p.tip_prod_descripcion 
 