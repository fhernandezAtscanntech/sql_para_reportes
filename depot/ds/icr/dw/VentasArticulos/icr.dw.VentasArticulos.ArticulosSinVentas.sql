select
	ae.art_codigo_Externo 	"Cod.Art�culo", 
	ae.descripcion 		"Art�culo", 
	r.rub_Cod_empresa 	"Cod.Rubro", 
	r.descripcion 		"Rubro", 
	nvl (
		(select cb.codigo_barra
		from codigos_barras cb
		where cb.art_codigo = ae.art_codigo
		and (cb.emp_codigo = ae.emp_codigo or (cb.emp_codigo = 1 and ae.veo_barras_modelo = 1))
		and rownum = 1), '') 	"Cod.Barra", 
	ae.audit_date 		"Fecha creaci�n", 
	p.fecha_ult_mov 	"�ltimo Movimiento", 
	decode (p.vendido, 0, 'Sin Ventas', 1, 'Vendido', 'Excepcion') "Vendido"
from 
	iposs.articulos_empresas ae, 
	iposs.rubros r, 
	(select lp.prod_cod_interno prod_cod_interno, lp.emp_codigo, max(lp.fecha_ult_mov) fecha_ult_mov, 1 as vendido
		from dwm_prod.l_productos lp
		where lp.emp_codigo = $EMPRESA_LOGUEADA$
		group by lp.prod_cod_interno, lp.emp_codigo
	union
	select ae.art_codigo prod_cod_interno, ae.emp_codigo, null fecha_ult_mov, 0 as vendido
		from iposs.articulos_empresas ae
		where ae.emp_codigo = $EMPRESA_LOGUEADA$
		and ae.fecha_borrado is null
		and ae.audit_date < trunc(sysdate) - :Dias
		and not exists
			(select 1
			from dwm_prod.l_productos lp
			where lp.emp_codigo = ae.emp_codigo
			and lp.prod_cod_interno = ae.art_codigo) ) p
where 
		ae.art_codigo = p.prod_cod_interno
and 	ae.emp_codigo = p.emp_codigo
and 
		ae.fecha_borrado is null
and 
		ae.rub_codigo = r.codigo
and 	-- solo articulos del/los local del funcionario
		ae.art_codigo in 
			(select distinct al.art_codigo
			from iposs.articulos_locales al, iposs.funcionarios_locales fl
			where al.loc_codigo = fl.loc_codigo
			and al.emp_codigo = ae.emp_codigo
			and al.fecha_borrado is null
			and (fl.fun_codigo = $CODIGO_FUNCIONARIO_LOGUEADO$ and fl.sel = 1) 
				or ($CODIGO_FUNCIONARIO_LOGUEADO$ in (select codigo from funcionarios where login like 'SC%' and gru_fun_codigo = 0))
			)
and 
		(p.fecha_ult_mov < trunc(sysdate) - :Dias or ( p.vendido = 0 and nvl (:IncluyeNoVendidos, 0) = 1) )
and
	( trim (upper (':Rubro')) = 'NULL' or r.codigo in :Rubro )
and 	
	( ae.emp_codigo  = $EMPRESA_LOGUEADA$
and 	  r.emp_codigo IN ($EMPRESA_LOGUEADA$, $EMPRESA_MODELO$) )
