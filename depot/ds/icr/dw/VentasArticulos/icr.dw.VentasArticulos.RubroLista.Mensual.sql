select 
	l.loc_descripcion 	"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	li.lis_pre_descripcion 			"Lista de Precios", 
	(select distinct f.ano_nombre 
		from dwm_prod.l_fechas f 
		where f.mes_codigo = fvm.mes_codigo) 	"A�o", 
	(select distinct f.mes_nombre_corto 
		from dwm_prod.l_fechas f 
		where f.mes_codigo = fvm.mes_codigo) 	"Mes", 
	p.prod_codigo 			"Codigo", 
	p.prod_descripcion 		"Articulo", 
	p.rub_descripcion 		"Rubro", 
	p.tip_prod_descripcion 		"Tipo producto", 
	sum (nvl (fvm.ven_cantidad, 0)) - sum (nvl (fvm.dev_cantidad, 0)) 	"Unidades", 
	sum (nvl (fvm.ven_nac_importe, 0)) 	"Venta Total", 
	sum (nvl (fvm.dev_nac_importe, 0)) 	"Devol", 
	sum (nvl (fvm.bon_nac_importe, 0)) 	"Bonif", 
	sum (nvl (fvm.des_prom_nac_importe, 0)) 	"Prom", 
	sum (nvl (fvm.ven_nac_importe, 0)) - 
		sum (nvl (fvm.dev_nac_importe, 0)) -
		(sum (nvl (fvm.cos_nac_ult_impuestos, 0)))	"Utilidad", 
	sum (nvl (fvm.cos_nac_ult_impuestos, 0)) 	"Costo Ultimo", 
	sum (nvl (fvm.iva_importe, 0)) 	"IVA", 
	sum (nvl (fvm.ven_nac_importe, 0)) - 
		sum (nvl (fvm.dev_nac_importe, 0))  	"Venta"
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.l_productos p, 
	dwm_prod.l_listas_de_precios li, 
	dwm_prod.f_ventas_mensual fvm
where
	( l.codigo = fvm.l_ubi_codigo 
and 	  l.emp_codigo = fvm.emp_codigo ) 
and
	( li.codigo = fvm.l_lis_pre_codigo
and 	  li.emp_codigo = fvm.emp_codigo)
and 
	( p.codigo = fvm.l_pro_codigo 
and 	  p.emp_codigo = fvm.emp_codigo )
and
	( fvm.mes_codigo in :MES )
and
	( l.loc_codigo in :LOCAL )
and 
-- 	( p.rub_codigo in nvl (:RUBRO, p.rub_codigo) )
--	( trim (upper (':RUBRO')) = 'NULL' or p.rub_codigo in :RUBRO )
-- and
	( p.tip_prod_codigo in (1, 3) )
and 
	( l.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  p.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  fvm.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.loc_descripcion , 
	l.loc_codigo 		, 
	l.emp_codigo 		, 
	li.lis_pre_descripcion 	, 
	fvm.mes_codigo 	, 
	p.prod_codigo 		, 
	p.prod_descripcion 	, 
	p.rub_descripcion 	, 
 	p.tip_prod_descripcion 
