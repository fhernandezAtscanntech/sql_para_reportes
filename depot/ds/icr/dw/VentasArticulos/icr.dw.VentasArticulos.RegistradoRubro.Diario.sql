select 
	l.loc_descripcion 	"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	f.ano_nombre 			"A�o", 
	f.mes_nombre_corto 		"Mes", 
	f.dia_del_mes 			"D�a", 
	p.prod_codigo 			"Codigo", 
	p.rub_descripcion 		"Rubro", 
	fun.fun_descripcion 		"Cajero", 
	sum (nvl (fvd.ven_cantidad, 0)) - sum (nvl (fvd.dev_cantidad, 0)) 	"Unidades", 
	sum (nvl (fvd.ven_nac_importe, 0)) 	"Venta Total", 
	sum (nvl (fvd.dev_nac_importe, 0)) 	"Devol", 
	sum (nvl (fvd.bon_nac_importe, 0)) 	"Bonif", 
	sum (nvl (fvd.des_prom_nac_importe, 0)) 	"Prom", 
	sum (nvl (fvd.ven_nac_importe, 0)) - 
		sum (nvl (fvd.dev_nac_importe, 0))  -
		((sum (nvl (fvd.cos_nac_ult_impuestos, 0))))	"Utilidad", 
	sum (nvl (fvd.cos_nac_ult_impuestos, 0))	"Costo Ultimo", 
	sum (nvl (fvd.iva_importe, 0)) 	"IVA", 
	sum (nvl (fvd.ven_nac_importe, 0)) - 
		sum (nvl (fvd.dev_nac_importe, 0))   	"Venta"
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.l_productos p, 
	dwm_prod.l_fechas f, 
	dwm_prod.l_funcionarios fun, 
	dwm_prod.f_ventas_diario fvd
where
	( l.codigo = fvd.l_ubi_codigo 
and 	l.emp_codigo = fvd.emp_codigo ) 
and
	( p.codigo = fvd.l_pro_codigo 
and 	  p.emp_codigo = fvd.emp_codigo )
and 
	( fun.codigo = fvd.l_fun_codigo_cajero 
and 	  fun.emp_codigo = fvd.emp_codigo )
and 
	( f.codigo = fvd.fecha_comercial )
and 
	( p.tip_prod_codigo = 3 ) 		-- Ventas a rubro
and 
	( :FechaHasta - :Fecha_desde <= 31 )
and
	( fvd.fecha_comercial between :FechaDesde and :FechaHasta )
and
	( l.loc_codigo in :LOCAL )
and
	( l.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  p.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  fun.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  fvd.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.loc_descripcion , 
	l.loc_codigo 		, 
	l.emp_codigo 		, 
	f.ano_nombre 		, 
	f.mes_nombre_corto 	, 
	f.dia_del_mes 		, 
	p.prod_codigo 		, 
	p.rub_descripcion 	, 
	fun.fun_descripcion
 