select 
	l.loc_descripcion 	"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	f.ano_nombre 			"A�o", 
	f.mes_nombre_corto 		"Mes", 
	f.dia_del_mes 			"D�a", 
	p.prod_codigo 			"Codigo", 
	p.prod_descripcion 		"Articulo", 
	p.rub_descripcion 		"Rubro", 
	pr.razon_social 		"Proveedor", 
	pm.prov_descripcion_dueno  "Proveedor Due�o",
	c.comb_descripcion 		"Combo", 
	sum (nvl (fvpu.l_prod_cantidad, 0)) 	"Cant. Tickets Prod", 
	sum (nvl (fvpu.ven_cantidad, 0)) - sum (nvl (fvpu.dev_cantidad, 0)) "Unidades", 
	sum (nvl (fvpu.ven_nac_importe, 0)) "Venta Total", 
	sum (nvl (fvpu.dev_nac_importe, 0)) "Devol", 
	sum (nvl (fvpu.bon_nac_importe, 0)) "Bonif", 
	sum (nvl (fvpu.des_prom_nac_importe, 0)) "Prom", 
	sum (
		nvl (fvpu.ven_nac_importe, 0) - 
		nvl (fvpu.dev_nac_importe, 0) -
		(nvl (fvpu.cos_nac_ult_impuestos, 0))) "Utilidad", 
	sum (nvl (fvpu.cos_nac_ult_impuestos, 0)) "Costo Ultimo", 
	sum (nvl (fvpu.iva_importe, 0)) "IVA", 
	sum (
		nvl (fVpu.ven_nac_importe, 0) - 
		nvl (fvpu.dev_nac_importe, 0) ) 	"Venta"
from
	dwm_prod.l_ubicaciones l,
	iposs.proveedores pr,
	dwm_prod.l_fechas f,
	iposs.proveedores_articulos pa,
	dwm_prod.l_productos p,
	dwm_prod.l_productos_modelo pm,
	dwm_prod.l_combos c, 
	dwm_prod.f_ventas_pu_diario_min fvpu
where
	( l.codigo = fvpu.l_ubi_codigo 
and 	l.emp_codigo = fvpu.emp_codigo ) 
and
	( p.codigo = fvpu.l_pro_codigo 
and 	  p.emp_codigo = fvpu.emp_codigo )
and 
	( pm.codigo = fvpu.l_pro_mod_codigo )
and 
	( c.codigo = fvpu.l_comb_codigo ) -- va sin empresa porque el combo por defecto es de la 1
and 
	( f.codigo = fvpu.fecha_comercial )
and
	( pa.art_codigo = p.prod_cod_interno 
and 	  pa.emp_codigo = p.emp_codigo
and 	  pa.fecha_borrado is null
and 	  pa.prov_codigo = pr.codigo
and 	  pa.prov_codigo in :Proveedor )
and (nvl(:UltProv, 0) = 0 or (nvl (:UltProv, 0) = 1 and pa.proveedor_principal = 1))
and 
	( fvpu.fecha_comercial between :FechaDesde and :FechaHasta)
and
	( l.loc_codigo in :Local )
and
	( p.tip_prod_codigo in (1, 3) )
and
	( fvpu.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  p.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  pa.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  l.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.loc_descripcion , 
	l.loc_codigo 		, 
	l.emp_codigo 		, 
	f.ano_nombre 		, 
	f.mes_nombre_corto 	, 
	f.dia_del_mes 		, 
	p.prod_codigo 		, 
	p.prod_descripcion 	, 
	p.rub_descripcion 	, 
	pr.razon_social		, 
	pm.prov_descripcion_dueno ,
	c.comb_descripcion
