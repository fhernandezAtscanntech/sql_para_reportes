select 
	l.loc_descripcion 	"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	to_number (to_char (fld.fecha_comercial, 'yyyy')) 	"A�o", 
	to_char (fld.fecha_comercial, 'mon') 			"Mes", 
	to_number (to_char (fld.fecha_comercial, 'dd')) 	"D�a", 
	fld.fecha_comercial 		"Fecha", 
	nvl (sum (fld.ven_nac_importe), 0) - 
		nvl (sum (fld.dev_nac_importe), 0)  	"Venta", 
	sum (nvl (fld.cos_nac_ult_impuestos, 0)) "Costo", 
	sum (nvl (fld.ven_nac_importe, 0) - 
		nvl (fld.dev_nac_importe, 0) ) -
	nvl (fld.cos_nac_ult_impuestos, 0)	"Utilidad", 
	sum (nvl (fld.mov_cantidad, 0)) 	"Cant. Tickets"
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.f_local_hora_diario fld
where
	( l.codigo = fld.l_ubi_codigo ) 
and
	( fld.fecha_comercial between :FechaDesde and :FechaHasta )
and
	( l.loc_codigo in :Local )
and
	( l.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.loc_descripcion, 
	l.loc_codigo, 
	l.emp_codigo, 
	fld.fecha_comercial
