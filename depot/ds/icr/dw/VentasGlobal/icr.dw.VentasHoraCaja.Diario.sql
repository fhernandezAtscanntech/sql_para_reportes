select 
	l.loc_descripcion 	"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	c.caj_codigo 			"Cod.Caja", 
	c.caj_descripcion 		"Caja", 
	f.ano_nombre 			"A�o", 
	f.mes_nombre_corto 		"Mes", 
	f.dia_del_mes			"Dia", 
	h.hor_hora 			"Hora", 
	nvl (sum (fv.ven_nac_importe), 0) 	"Venta Total", 
	nvl (sum (fv.dev_nac_importe), 0) 	"Devol", 
	nvl (sum (fv.bon_nac_importe), 0) 	"Bonific", 
	nvl (sum (fv.ven_nac_importe), 0) - 
		nvl (sum (fv.dev_nac_importe), 0) 	"Venta", 
	sum (nvl (fv.mov_cantidad, 0)) 			"Cant. Tickets"
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.l_productos p, 
	dwm_prod.l_cajas c, 
	dwm_prod.l_horas h, 
	dwm_prod.l_fechas f, 
	dwm_prod.f_ventas fv
where
	( l.codigo = fv.l_ubi_codigo 
and 	  l.emp_codigo = fv.emp_codigo ) 
and 
	( c.codigo = fv.l_caj_codigo
and 	  c.emp_codigo = fv.emp_codigo )
and
	( h.codigo = fv.l_hor_codigo )
and 
	( f.codigo = fv.fecha_Comercial )
and 
	( p.codigo = fv.l_pro_codigo
and 	  p.emp_codigo = fv.emp_codigo )
and
	( p.tip_prod_codigo in (1, 3) )		-- Productos y Rubros
and 
	( f.codigo = :Fecha )
and 
	( trim (upper (':Hora')) = 'NULL' or h.codigo in :Hora )
and
	( trim (upper (':Local')) = 'NULL' or l.loc_codigo in :Local )
and 
	( l.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  c.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  p.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  fv.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.loc_descripcion, 
	l.loc_codigo, 
	l.emp_codigo, 
	c.caj_codigo, 
	c.caj_descripcion, 
	f.ano_nombre, 
	f.mes_nombre_corto, 
	f.dia_del_mes, 
	h.hor_hora

