select 
	l.loc_descripcion 	"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	fvpum.mes_codigo  		"Cod.Mes", 
	(select distinct f.ano_nombre 
		from dwm_prod.l_fechas f 
		where f.mes_codigo = fvpum.mes_codigo) 	"A�o", 
	(select distinct f.mes_nombre_corto 
		from dwm_prod.l_fechas f 
		where f.mes_codigo = fvpum.mes_codigo) 	"Mes", 
	P.tip_prod_descripcion 		"Tipo Prducto", 
	nvl (sum (fVpum.ven_nac_importe), 0) - 
		nvl (sum (fvpum.dev_nac_importe), 0)  	"Venta"
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.l_productos p, 
	dwm_prod.f_ventas_pu_mensual_min fvpum
where
	( p.emp_codigo = fvpum.emp_codigo 
and 	p.codigo = fvpum.l_pro_codigo 
and 	l.codigo = fvpum.l_ubi_codigo 
and 	l.emp_codigo = fvpum.emp_codigo ) 
and
	( trim (upper (':MES')) = 'NULL' or fvpum.mes_codigo in :MES )
and
	( trim (upper (':LOCAL')) = 'NULL' or l.loc_codigo in :LOCAL )
and
	( l.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  p.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  fvpum.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.loc_descripcion, 
	l.loc_codigo, 
	l.emp_codigo, 
	fvpum.mes_codigo, 
	P.tip_prod_descripcion  
