select 
	l.loc_descripcion 	"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	f.ano_nombre 			"A�o", 
	f.mes_nombre_corto 		"Mes", 
	f.dia_del_mes			"Dia", 
	p.tip_prod_descripcion 		"Tipo Producto", 
	nvl (sum (fvd.ven_nac_importe), 0) - 
		nvl (sum (fvd.dev_nac_importe), 0) 	"Venta"
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.l_productos p, 
	dwm_prod.l_fechas f, 
	dwm_prod.f_ventas_diario fvd
where
	( p.emp_codigo = fvd.emp_codigo 
and 	p.codigo = fvd.l_pro_codigo 
and 	l.codigo = fvd.l_ubi_codigo 
and 	l.emp_codigo = fvd.emp_codigo ) 
and 
	( fvd.fecha_Comercial = f.codigo )
and
	( f.mes_codigo = :MES )
and
	( trim (upper (':LOCAL')) = 'NULL' or l.loc_codigo in :LOCAL )
and 
	( l.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  p.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  fvd.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.loc_descripcion, 
	l.loc_codigo, 
	l.emp_codigo, 
	f.ano_nombre, 
	f.mes_nombre_corto, 
	f.dia_del_mes, 
	p.tip_prod_descripcion  
