select 
	l.loc_descripcion 	"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	(select distinct f.ano_nombre 
		from dwm_prod.l_fechas f 
		where f.mes_codigo = flm.mes_codigo) 	"A�o", 
	(select distinct f.mes_nombre_corto 
		from dwm_prod.l_fechas f 
		where f.mes_codigo = flm.mes_codigo) 	"Mes", 
	flm.mes_codigo 				"Cod.Mes", 
	nvl (sum (flm.ven_nac_importe), 0) - 
		nvl (sum (flm.dev_nac_importe), 0)  	"Venta", 
	sum (nvl (flm.cos_nac_ult_impuestos, 0)) "Costo Ultimo", 	
	sum (nvl (flm.mov_cantidad, 0)) 	"Cant. Tickets"
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.f_local_mensual flm
where
	( l.codigo = flm.l_ubi_codigo ) 
and
	( flm.mes_codigo in :Mes )
and
	( l.loc_codigo in :Local )
and
	( l.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.loc_descripcion, 
	l.loc_codigo, 
	l.emp_codigo, 
	flm.mes_codigo
