select 
	c.descripcion 	"Caja", 
	l.descripcion 	"Local", 
	fis.fecha_comercial 	"Fecha", 
	fis.nro_cierre 	"Nro Cierre", 
	fis.nro_linea 	"Nro Linea", 
	fis.nro_ope_desde 	"Nro Ope Desde", 
	fis.nro_ope_hasta 	"Nro Ope Hasta", 
	fis.tipo_de_factura 	"Tipo Factura", 
	fis.categoria 	"Categoria", 
	fis.estado 	"Estado", 
	fis.nombre_factura 	"Nombre Factura", 
	fis.ruc_factura 	"CUIT Factura", 
	fis.tipo_de_documento "Tipo Documento", 
	fis.categoria_provincial 	"Categoria Provincial", 
	fis.registradora 	"Registradora", 
	sum(fis.percepcion_iva) 	"Percepcion IVA", 
	sum(fis.importe_ingresos_brutos) 	"Ingresos Brutos", 
	sum(fis.importe_retencion) 	"Retencion", 
	sum(fis.importe_alicuota_iva) 	"Alicuota IVA", 
		nvl(( sum(fis.iva_1) ),0) + 
		nvl(( sum(fis.iva_2) ),0) + 
		nvl(( sum(fis.iva_3) ),0) + 
		nvl(( sum(fis.iva_4) ),0) + 
		nvl(( sum(fis.iva_5) ),0) + 
		nvl(( sum(fis.neto_iva_1) ),0) + 
		nvl(( sum(fis.neto_iva_2) ),0) + 
		nvl(( sum(fis.neto_iva_3) ),0) + 
		nvl(( sum(fis.neto_iva_4) ),0) + 
		nvl(( sum(fis.neto_iva_5) ),0) + 
		nvl(( sum(fis.neto_iva_0) ),0) + 
		nvl(( sum(fis.importe_imp_internos) ),0) 	"Total", 
	sum(fis.importe_imp_internos) 	"Impuestos Internos", 
	sum(fis.neto_iva_0) 	"Neto IVA 0", 
	sum(fis.neto_iva_1) 	"Neto IVA 1", 
	sum(fis.neto_iva_3) 	"Neto IVA 3", 
	sum(fis.neto_iva_2) 	"Neto IVA 2", 
	sum(fis.neto_iva_4) 	"Neto IVA 4", 
	sum(fis.neto_iva_5) 	"Neto IVA 5", 
	sum(fis.iva_1) 	"IVA 1", 
	sum(fis.iva_2) 	"IVA 2", 
	sum(fis.iva_3) 	"IVA 3", 
	sum(fis.iva_4) 	"IVA 4", 
	sum(fis.iva_5) 	"IVA 5"
from 
	iposs.cajas c, 
	iposs.locales l, 
	iposs.fis_resumenes_ventas fis
where 
	-- permisos por empresa
	($EMPRESA_LOGUEADA$ = $EMPRESA_MODELO$ or l.emp_codigo = $EMPRESA_LOGUEADA$) and

	-- transcripto de Discoverer
	( ( l.codigo = c.loc_codigo ) and 
	  ( l.emp_codigo = fis.emp_codigo and 
	    l.codigo = fis.loc_codigo ) and 
	  ( c.codigo = fis.caj_codigo and 
	    c.loc_codigo = fis.loc_codigo ) ) and 
	( fis.fecha_comercial between :FechaDesde and :FechaHasta ) and 
 	( l.codigo in :Local ) 
-- 	( l.codigo in (nvl(:Local, l.codigo) ) 
group by 
	c.descripcion, 
	l.descripcion, 
	fis.fecha_comercial, 
	fis.nro_cierre, 
	fis.nro_linea, 
	fis.nro_ope_desde, 
	fis.nro_ope_hasta, 
	fis.tipo_de_factura, 
	fis.categoria, 
	fis.estado, 
	fis.nombre_factura, 
	fis.ruc_factura, 
	fis.tipo_de_documento, 
	fis.categoria_provincial, 
	fis.registradora
order by 
	fis.fecha_comercial asc, 
	fis.nro_cierre asc, 
	fis.nro_linea asc
