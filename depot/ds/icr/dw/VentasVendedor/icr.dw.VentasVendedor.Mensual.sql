select 
	l.loc_descripcion 	"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	(select distinct f.ano_nombre 
		from dwm_prod.l_fechas f 
		where f.mes_codigo = fvm.mes_codigo) 	"A�o", 
	(select distinct f.mes_nombre_corto 
		from dwm_prod.l_fechas f 
		where f.mes_codigo = fvm.mes_codigo) 	"Mes", 
	v.fun_descripcion 		"Vendedor", 
	v.fun_tipo_vendedor 		"Tipo Vendedor", 
	nvl (sum (fvm.ven_nac_importe), 0) - 
		nvl (sum (fvm.dev_nac_importe), 0)  	"Venta", 
	nvl (sum (fvm.mov_cantidad), 0) 		"Cantidad Ticket", 
	nvl (sum (fvm.iva_importe), 0) 			"IVA"
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.l_funcionarios v, 
	dwm_prod.f_ventas_mensual fvm
where
	v.codigo = fvm.l_fun_codigo_vendedor 
-- si limito los vendedores a los de la empresa no veo las ventas hechas sin vendedor que van al l_fun_codigo_vendedor = 0
-- and 	v.emp_codigo = fvm.emp_codigo 			
and 
	l.codigo = fvm.l_ubi_codigo 
and 	l.emp_codigo = fvm.emp_codigo 
and
	( trim (upper (':MES')) = 'NULL' or fvm.mes_codigo in :MES )
and
	( trim (upper (':LOCAL')) = 'NULL' or l.loc_codigo in :LOCAL )
and
	( l.emp_codigo = $EMPRESA_LOGUEADA$ 
-- and 	  v.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  fvm.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.loc_descripcion, 
	l.loc_codigo, 
	l.emp_codigo, 
	fvm.mes_codigo, 
	v.fun_descripcion, 
	v.fun_tipo_vendedor
