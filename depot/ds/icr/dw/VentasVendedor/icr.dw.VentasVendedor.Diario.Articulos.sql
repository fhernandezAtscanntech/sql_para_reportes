select 
	l.loc_descripcion 	"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	fvdm.fecha_comercial 	"Fecha comercial", 
	v.fun_descripcion 		"Vendedor", 
	v.fun_tipo_vendedor 	"Tipo Vendedor", 
	p.prod_codigo 			"Cod.Artículo", 
	p.prod_descripcion 		"Artículo", 
	p.rub_descripcion 		"Rubro", 
	p.codigo_barras 		"EAN", 
	nvl (sum (fvdm.ven_cantidad), 0) - nvl (sum (fvdm.dev_cantidad), 0) 		"Cantidad", 
	nvl (sum (fvdm.ven_nac_importe), 0) - nvl (sum (fvdm.dev_nac_importe), 0)  	"Venta", 
	nvl (sum (fvdm.iva_importe), 0) 		"IVA", 
	nvl (sum (fvdm.imp_1_importe), 0) +
		nvl (sum (fvdm.imp_2_importe), 0) +
		nvl (sum (fvdm.imp_3_importe), 0) +
		nvl (sum (fvdm.imp_4_importe), 0) +
		nvl (sum (fvdm.imp_5_importe), 0) 	"Otros Impuestos"
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.l_funcionarios v, 
	dwm_prod.l_productos p, 
	dwm_prod.f_ventas_diario_min fvdm
where
	v.codigo = fvdm.l_fun_codigo_vendedor 
-- si limito los vendedores a los de la empresa no veo las ventas hechas sin vendedor que van al l_fun_codigo_vendedor = 0
-- and 	v.emp_codigo = fvdm.emp_codigo 			
and 
	l.codigo = fvdm.l_ubi_codigo 
and 	l.emp_codigo = fvdm.emp_codigo 
and
	p.codigo = fvdm.l_pro_codigo
and 	p.emp_codigo = fvdm.emp_codigo
and 
	( fvdm.fecha_comercial >= :FechaDesde and fvdm.fecha_comercial <= :FechaHasta
and 	:FechaHasta - :FechaDesde <= 31	)
and
	( trim (upper (':Local')) = 'NULL' or l.loc_codigo in :Local )
and
	( l.emp_codigo = $EMPRESA_LOGUEADA$ 
-- and 	  v.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  fvdm.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  p.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.loc_descripcion, 
	l.loc_codigo, 
	l.emp_codigo, 
	p.prod_codigo, 
	p.prod_descripcion, 
	p.rub_descripcion, 
	p.codigo_barras, 
	fvdm.fecha_comercial, 
	v.fun_descripcion, 
	v.fun_tipo_vendedor
