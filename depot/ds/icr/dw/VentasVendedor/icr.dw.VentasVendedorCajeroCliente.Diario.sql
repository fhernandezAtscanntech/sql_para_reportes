select 		
	l.loc_descripcion 	"Local", 
	l.loc_codigo 	"Cod.Local", 
	l.emp_codigo 	"Emp.Codigo", 
	f.ano_nombre 	"Año", 
	f.mes_nombre_corto 	"Mes", 
	f.dia_del_mes 	"Día", 
	fv.fecha_comercial 	"Fecha Comercial", 
	p.prod_codigo 	"Cod.Articulo", 
	p.prod_descripcion 	"Articulo", 
	p.codigo_barras 	"Cod.Barra", 
	p.rub_descripcion 	"Rubro", 
	c.cli_cod_empresa 	"Cod.Cliente", 
	c.cli_descripcion 	"Cliente", 
	c.depa_descripcion 	"Departamento", 
	c.loca_descripcion 	"Localidad", 
	fuc.fun_cod_empresa 	"Cod.Cajero", 
	fuc.fun_descripcion 	"Cajero", 
	fuv.fun_cod_empresa 	"Cod.Vendedor", 
	fuv.fun_descripcion 	"Vendedor", 
	sum (nvl (fv.ven_cantidad, 0)) - sum (nvl (fv.dev_cantidad, 0)) 	"Unidades", 
	sum (nvl (fv.ven_nac_importe, 0)) - sum (nvl (fv.dev_nac_importe, 0)) 	"Venta", 
	sum (nvl (fv.cos_nac_ult_impuestos, 0)) "Costo último"
from 		
	dwm_prod.l_ubicaciones l, 	
	dwm_prod.l_fechas f, 	
	dwm_prod.l_productos p, 	
	dwm_prod.l_clientes c, 
	dwm_prod.l_funcionarios fuc, 
	dwm_prod.l_funcionarios fuv, 
	dwm_prod.f_ventas_local_cli_diario_min fv
where	
	( l.codigo = fv.l_ubi_codigo 
and 	l.emp_codigo = fv.emp_codigo ) 
and	
	( p.codigo = fv.l_pro_codigo 
and 	  p.emp_codigo in (fv.emp_codigo, $EMPRESA_MODELO$) )
and	
	( c.codigo = fv.l_cli_codigo
and	  c.emp_codigo in (fv.emp_codigo, $EMPRESA_MODELO$) )
and	
	( fuc.codigo = fv.l_fun_codigo_cajero 
and	  fuc.emp_codigo in (fv.emp_codigo, $EMPRESA_MODELO$) )
and	
	( fuv.codigo = fv.l_fun_codigo_vendedor 
and	  fuv.emp_codigo in (fv.emp_codigo, $EMPRESA_MODELO$) )
and	
	( fv.fecha_comercial = f.codigo )	
and		
	( fv.fecha_comercial between :FechaDesde and :FechaHasta
and 	  :FechaHasta - :FechaDesde <= 62 )	
and (
	p.tip_prod_codigo in (1, 3) 
	or 
	( p.tip_prod_codigo = 2 
	and ( trim (upper (':ExclServicios')) = 'NULL' or to_number(p.prod_codigo) not in :ExclServicios ))
	) 
and 
	( l.loc_codigo in :Local 
and 	  l.emp_codigo = $EMPRESA_LOGUEADA$ )	
group by 		
	l.loc_descripcion , 	
	l.loc_codigo 	, 
	l.emp_codigo 	, 
	f.ano_nombre 	, 
	f.mes_nombre_corto 	, 
	f.dia_del_mes 	,
	fv.fecha_comercial 	, 
	p.prod_codigo 	, 
	p.prod_descripcion 	, 
	p.codigo_barras 	, 
	p.rub_descripcion	, 
	c.cli_cod_empresa 	, 
	c.cli_descripcion 	, 
	c.depa_descripcion 	, 
	c.loca_descripcion 	, 
	fuc.fun_cod_empresa 	, 
	fuc.fun_descripcion 	, 
	fuv.fun_cod_empresa 	, 
	fuv.fun_descripcion