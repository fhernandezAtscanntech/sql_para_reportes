select 
	Local 				"Local", 
-- 	loc_codigo 			"Cod.Local", 
	emp_codigo 			"Emp.Codigo", 
	ano_nombre 			"A�o", 
	mes_nombre_corto 		"Mes", 
	prod_codigo 			"Codigo", 
	prod_descripcion 		"Articulo", 
	prod_Rubro			"Rubro", 
	Unidades 			"Unidades", 
	Venta_Total 			"Venta Total", 
	Devol 				"Devol", 
	Bonif 				"Bonif", 
	Prom 				"Prom", 
	Utilidad 			"Utilidad", 
	Costo_Ultimo 			"Costo Ultimo", 
	IVA 				"IVA", 
	Venta 				"Venta", 
	ranking_pesos 			"Ranking Pesos", 
	ranking_unidades 		"Ranking Unidades"
from 
(
	select 
		l.loc_modelo_descripcion 	as Local, 
-- 		l.loc_codigo 			as Loc_Codigo, 
		l.emp_codigo 			as Emp_Codigo, 
		(select distinct f.ano_nombre 
			from dwm_prod.l_fechas f 
			where f.mes_codigo = fvpu.mes_codigo) as Ano_Nombre, 
		(select distinct f.mes_nombre_corto 
			from dwm_prod.l_fechas f 
			where f.mes_codigo = fvpu.mes_codigo) as Mes_Nombre_Corto, 
		p.prod_codigo 			as Prod_Codigo, 
		p.prod_descripcion 		as Prod_Descripcion, 
		p.rub_descripcion 		as Prod_Rubro, 
		sum (nvl (fvpu.ven_cantidad, 0)) - sum (nvl (fvpu.dev_cantidad, 0)) as Unidades, 
		sum (nvl (fvpu.ven_nac_importe, 0)) as Venta_Total, 
		sum (nvl (fvpu.dev_nac_importe, 0)) as Devol, 
		sum (nvl (fvpu.bon_nac_importe, 0)) as Bonif, 
		sum (nvl (fvpu.des_prom_nac_importe, 0)) as Prom, 
		sum (
			nvl (fvpu.ven_nac_importe, 0) - 
			nvl (fvpu.dev_nac_importe, 0)  -
			(nvl (fvpu.cos_nac_ult_impuestos, 0))) as Utilidad, 
		sum (nvl (fvpu.cos_nac_ult_impuestos, 0)) as Costo_Ultimo, 
		sum (nvl (fvpu.iva_importe, 0)) as IVA, 
		sum (
			nvl (fVpu.ven_nac_importe, 0) - 
			nvl (fvpu.dev_nac_importe, 0) ) as Venta, 
		rank () over (partition by p.rub_descripcion
			order by sum (
				nvl (fVpu.ven_nac_importe, 0) - 
				nvl (fvpu.dev_nac_importe, 0) ) desc)	as ranking_pesos, 
		rank () over (partition by p.rub_descripcion
			order by sum (
				nvl (fVpu.ven_cantidad, 0) - 
				nvl (fvpu.dev_cantidad, 0)) desc)		as ranking_unidades
	from
		dwm_prod.l_ubicaciones l, 
		dwm_prod.l_productos p, 
		dwm_prod.f_ventas_pu_mensual_min fvpu
	where
		( l.codigo = fvpu.l_ubi_codigo 
	and 	l.emp_codigo = fvpu.emp_codigo ) 
	and
		( p.codigo = fvpu.l_pro_codigo 
	and 	  p.emp_codigo = fvpu.emp_codigo )
	and
		( fvpu.mes_codigo in :Mes )
	and
		( l.loc_codigo in :Local )
	and 
-- 		( p.tip_prod_codigo in (1, 3) )
		( p.tip_prod_codigo = 1 )
	and 
		( l.emp_codigo = $EMPRESA_LOGUEADA$
	and 	  p.emp_codigo = $EMPRESA_LOGUEADA$
	and 	  fvpu.emp_codigo = $EMPRESA_LOGUEADA$ )
	group by 
		l.loc_modelo_descripcion , 
-- 		l.loc_codigo 		, 
		l.emp_codigo 		, 
		fvpu.mes_codigo 	, 
		p.prod_codigo 		, 
		p.prod_descripcion 	, 
		p.rub_descripcion)
where
	ranking_pesos <= :Ranking
