select 
	l.loc_descripcion 	"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	f.ano_nombre 			"A�o", 
	f.mes_nombre_corto 		"Mes", 
	f.dia_del_mes 			"Dia", 
	f.dia_nombre_largo 		"Dia Nombre", 
	f.dia_de_la_semana		"Nro Dia Semana", 
	to_char(h.hor_hora, '09') 		"Hora", 
	m.tip_descripcion 		"Tipo Pago", 
	m.sub_descripcion 		"Subtipo Pago", 
	nvl (sum (flhd.ven_nac_importe), 0) 	"Venta Total", 
	nvl (sum (flhd.dev_nac_importe), 0) 	"Devol Total", 
	nvl (sum (flhd.bon_nac_importe), 0) 	"Bon Total", 
	nvl (sum (flhd.des_nac_importe), 0) 	"Desc  Total", 
	nvl (sum (flhd.des_prom_Nac_importe), 0) 	"Prom Total", 
--	nvl (sum (flhd.mov_cantidad), 0) 	"Cant. Tickets", -- al abrir por medio de pago, la cantidad de tickets se corrompe
	nvl (sum (flhd.ven_nac_importe), 0) - 
		nvl (sum (flhd.dev_nac_importe), 0)  	"Venta"
-- 	nvl (sum (flhd.ven_nac_importe), 0) - 
-- 		nvl (sum (flhd.dev_nac_importe), 0)  /
--		nvl (sum (flhd.mov_cantidad), 0)  	"Ticket Promedio"
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.l_fechas f, 
	dwm_prod.l_horas h, 
	dwm_prod.l_medios m, 
	dwm_prod.f_ventas_local_hora_diario_min flhd
where
	( l.codigo = flhd.l_ubi_codigo )
-- and 	l.emp_codigo = flhd.emp_codigo ) 		-- la flhd no tiene codigo de empresa
and
	( h.codigo = flhd.l_hor_codigo )
and 
	( flhd.fecha_comercial = f.codigo )
and
	( flhd.l_med_codigo = m.codigo )
and 
	( flhd.fecha_comercial between :FechaDesde and :FechaHasta 
and 	:FechaHasta - :FechaDesde <= 31 )
-- and
-- 	( flhd.tip_prod_codigo in (1, 3) ) 		-- Articulos y rubros
and
	( l.loc_codigo in :Local )
and 
	( l.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.loc_descripcion, 
	l.loc_codigo, 
	l.emp_codigo, 
	f.ano_nombre, 
	f.mes_nombre_corto, 
	f.dia_del_mes, 
	f.dia_nombre_largo, 
	f.dia_de_la_semana,
	to_char(h.hor_hora, '09'), 
	m.tip_descripcion, 
    m.sub_descripcion