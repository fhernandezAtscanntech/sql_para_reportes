select 
	l.descripcion 			"Local", 
	rccc.descripcion 	"Cuenta Corriente", 
	rcmcc.numero_mov_cc 	"Numero Mov", 
	rcmcc.fecha 		"Fecha", 
	rcmcc.nro_lote 		"Lote", 
	rctmcc.descripcion 	"Tipo Mov", 
	rccc.sobregiro 	"Sobregiro", 
	rccc.saldo 		"Saldo", 
	sum ( decode (rcmcc.tip_mov_cc,
			1, rcmcc.importe_pos*-1, 
			2, 0, 
			5, rcmcc.importe_pos*-1,
			   rcmcc.importe_pos) ) 	"Importe POS", 
	sum ( decode (rcmcc.tip_mov_cc, 
			1, rcmcc.importe*-1, 
			2, 0, 
			5, rcmcc.importe*-1, 
			   rcmcc.importe) ) 	"Importe",  
	rcmcc.arancel 		"Arancel"
from 
	iposs.locales l, 
	iposs.rc_cuentas_corrientes rccc, 
	iposs.rc_movimientos_cc rcmcc, 
	iposs.rc_tipos_movimientos_cc rctmcc
where 
	( ( rccc.codigo = rcmcc.cue_cor_codigo ) and 
	  ( rctmcc.codigo = rcmcc.tip_mov_cc ) and 
	  ( l.codigo(+) = rcmcc.loc_codigo ) ) and 
	( rcmcc.fecha between :FechaDesde and :FechaHasta +1-1/24/60/60 ) and
	( rcmcc.emp_codigo = $EMPRESA_LOGUEADA$ )and
	( rccc.emp_codigo  = $EMPRESA_LOGUEADA$ ) 
group by 
	l.descripcion, 
	rccc.descripcion, 
	rcmcc.numero_mov_cc, 
	rcmcc.fecha, 
	rcmcc.nro_lote, 
	rctmcc.descripcion, 
	rccc.sobregiro, 
	rcmcc.arancel, 
	rccc.saldo
order by 
	rcmcc.fecha asc, 
	rcmcc.numero_mov_cc asc
