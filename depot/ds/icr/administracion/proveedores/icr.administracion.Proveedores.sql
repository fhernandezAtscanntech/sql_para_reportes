select
	pe.prov_Cod_Externo 	"Cod.Proveedor", 
	p.razon_social		"Raz�n Social", 
	p.ruc 			"CUIT/RUT", 
	p.direccion 		"Direcci�n", 
	p.telefono 		"Tel�fono", 
	decode (pe.incluye_iva, 1, 'SI', 'NO') 	"Incluye IVA", 
	decode (nvl(pe.de_gastos, 0), 1, 'SI', 'NO') 	"De Gastos", 
	pe.nro_cuenta 		"N�mero Cuenta", 
	pe.audit_date 	"Fecha alta"
from 
	iposs.proveedores p, 
	iposs.proveedores_empresas pe
where 
	p.codigo = pe.prov_codigo
and 	
	pe.fecha_borrado is null
and 
	p.codigo in 
		(select pl.prov_codigo
		from iposs.proveedores_locales pl, #LocalesVisibles# lv
		where pl.loc_codigo = lv.loc_codigo
		and pl.fecha_borrado is null)
and
	pe.emp_codigo = $EMPRESA_LOGUEADA$
