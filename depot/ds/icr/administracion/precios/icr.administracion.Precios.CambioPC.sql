select
	l.codigo 		"Cod.Local (I)", 
	l.loc_cod_empresa	"Cod.Local", 
	l.descripcion 		"Local", 
	r.descripcion  		"Rubro", 
	al.art_codigo_Externo 	"Cod.Art�culo", 
	ae.descripcion 		"Art�culo", 
	nvl (
		(select cb.codigo_barra
		from codigos_barras cb
		where cb.art_codigo = ae.art_codigo
		and (cb.emp_codigo = ae.emp_codigo or (cb.emp_codigo = 1 and ae.veo_barras_modelo = 1))
		and rownum = 1), '') 	"Cod.Barra", 
	mo.simbolo 		"Moneda", 
	pc.costo_ultimo 		"Costo �ltimo", 
	pc.costo_imp 		"Costo c/imp.", 
	pc.vigencia_desde 	"Vigencia", 
	pc.vigencia_hasta 	"Vigencia Hasta", 
	pc.audit_user 		"Usuario"
from 
	iposs.articulos_locales al
	join iposs.locales l
		on al.loc_Codigo = l.codigo 
		and al.emp_codigo = l.emp_codigo
	join iposs.articulos_empresas ae
		on ae.art_codigo = al.art_Codigo 
		and ae.emp_codigo = al.emp_codigo
	join iposs.rubros r 
		on ae.rub_codigo = r.codigo
	join iposs.precios_de_costos_articulos pc
		on al.art_codigo = pc.art_codigo
		and al.loc_Codigo = pc.loc_codigo
		and al.emp_codigo = pc.emp_codigo
	join iposs.monedas mo
		on pc.mon_codigo = mo.codigo
where	al.fecha_borrado is null
and 	ae.fecha_borrado is null
and 
		pc.vigencia_desde >= :FechaDesde
and 	pc.vigencia_desde <= :FechaHasta 
and 	pc.vigencia_hasta > :FechaDesde )		-- por eficiencia
and 
	( trim (upper (':Local')) = 'NULL' or l.codigo in :Local )
and
	( trim (upper (':Rubro')) = 'NULL' or r.codigo in :Rubro )
and 	
	( ae.emp_codigo  = $EMPRESA_LOGUEADA$
and 	  al.emp_codigo  = $EMPRESA_LOGUEADA$
and 	  l.emp_codigo   = $EMPRESA_LOGUEADA$
and 	  pc.emp_codigo  = $EMPRESA_LOGUEADA$ )
order by 
	al.art_codigo_Externo, 
	ae.descripcion, 
	pc.vigencia_desde
