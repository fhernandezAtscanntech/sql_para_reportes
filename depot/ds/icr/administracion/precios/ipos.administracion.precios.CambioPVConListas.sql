select
	l.codigo 		"Cod.Local (I)", 
	l.loc_cod_empresa	"Cod.Local", 
	l.descripcion 		"Local", 
	r.descripcion  		"Rubro", 
	lis.descripcion 	"Lista de precios", 
	al.art_codigo_Externo 	"Cod.Artículo", 
	ae.descripcion 		"Artículo", 
	nvl (
		(select cb.codigo_barra
		from codigos_barras cb
		where cb.art_codigo = ae.art_codigo
		and (cb.emp_codigo = ae.emp_codigo or (cb.emp_codigo = 1 and ae.veo_barras_modelo = 1))
		and rownum = 1), '') 	"Cod.Barra", 
	mo.simbolo 		"Moneda", 
	pv.precio 		"Precio Venta", 
	pv.vigencia 		"Vigencia", 
	pv.vigencia_hasta 	"Vigencia Hasta", 
	pv.audit_user 		"Usuario", 
	lis2.descripcion 	"Listas adicionales", 
	iposs.f_precio_venta_2 (al.art_codigo, al.loc_codigo, lis2.codigo, pv.vigencia, al.emp_codigo) 	"Precio lista adic"
from 
	iposs.locales l, 
	iposs.monedas mo, 
	iposs.rubros r, 
	iposs.articulos_locales al, 
	iposs.articulos_empresas ae, 
	iposs.listas_de_precios_ventas lis, 
	iposs.listas_de_precios_ventas lis2, 
	iposs.precios_de_articulos_ventas pv	
where
		ae.art_codigo = al.art_Codigo
and 	ae.emp_codigo = al.emp_codigo
and 
		ae.rub_codigo = r.codigo
and 	
		al.loc_Codigo = l.codigo
and 
		al.art_codigo = pv.art_codigo
and 	al.loc_Codigo = pv.loc_lis_loc_codigo
and 
		lis.codigo = pv.loc_lis_lis_pre_ve_codigo
and 	
		lis2.emp_codigo = ae.emp_codigo
and 	lis2.codigo <> lis.codigo
and 
		mo.codigo = pv.mon_codigo
and 
		al.fecha_borrado is null
and 	ae.fecha_borrado is null
and 
		( pv.vigencia >= :FechaDesde
and 	  pv.vigencia <= :FechaHasta 
and 	  pv.vigencia_hasta > :FechaDesde )		-- por eficiencia
and 
		l.codigo in :Local 
and
		( trim (upper (':Rubro')) = 'NULL' or r.codigo in :Rubro )
and 	
		( ae.emp_codigo  = $EMPRESA_LOGUEADA$
and 	  al.emp_codigo  = $EMPRESA_LOGUEADA$
and 	  l.emp_codigo   = $EMPRESA_LOGUEADA$
and 	  lis.emp_codigo = $EMPRESA_LOGUEADA$
and 	  lis2.emp_codigo = $EMPRESA_LOGUEADA$
and 	  pv.emp_codigo  = $EMPRESA_LOGUEADA$ )
order by 
	al.art_codigo_Externo, 
	ae.descripcion, 
	pv.vigencia
