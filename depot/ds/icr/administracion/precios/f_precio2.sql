create or replace 
function iposs.F_PRECIO_VENTA_2 (
	p_art_codigo 	iposs.articulos_empresas.art_codigo%type, 
	p_loc_codigo 	iposs.locales.codigo%type, 
	p_lis_codigo 	iposs.listas_de_precios_ventas.codigo%type, 
	p_fecha 		in date,
	p_emp_codigo 	in number)
return number
is
	v_tipo		number;
	v_porc 		number;
	v_pre		iposs.precios_de_articulos_ventas.precio%type;
	v_por_lis 	number;
	v_par		iposs.parametros_empresas.par_codigo%type;
	v_lis_emp	number;
	v_lis_base	iposs.listas_de_precios_ventas.lis_pv_base_codigo%type;
	v_mon 		iposs.monedas.codigo%type;
	v_mon_def 	iposs.monedas.codigo%type;
	v_cotiza	number 	:= 1;
begin
	begin
		select nvl (tipo_lista, 0), lis_pre_ve_Cod_empresa, lis_pv_base_codigo, porcentaje_dto
		into v_tipo, v_lis_emp, v_lis_base, v_por_lis
		from iposs.listas_de_precios_ventas
		where codigo = p_lis_codigo
		and emp_codigo = p_emp_codigo;
	exception
	when no_data_found then
		return (0);
	end;

	if (v_tipo = 1) and (v_lis_base is not null)		-- tipo de lista es porcentual y hay lista base
	then
		-- ######################################
		-- ####### Averiguo el porcentaje ####### 
		-- ######################################
		begin			-- busco en la tabla del BE nuevo
			select porcentaje
			into v_porc
			from iposs.porcentajes_articulos_ventas
			where art_codigo = p_art_codigo
			and loc_lis_lis_pre_ve_codigo = p_lis_codigo
			and loc_lis_loc_codigo = p_loc_codigo
			and p_fecha between vigencia and vigencia_hasta
			and rownum = 1; 		-- seguridad
			
		exception
		when no_data_found then		-- si no está, uso el porcentaje de la lista
			
			v_porc := nvl (v_por_lis, 0);

		end;
		
		-- ######################################
		-- ##### Averiguo sobre qué precio ###### 
		-- ######################################
			
		v_pre := iposs.f_precio_venta_2 (p_art_codigo, p_loc_codigo, v_lis_base, p_fecha, p_emp_codigo);
		
		-- ######################################
		-- ############## RETORNO ###############
		-- ######################################

		-- BE nuevo
		return (v_pre * (1 - (v_porc / 100)));

	else					-- Es lista fija (o es porcentual pero no indicaron la lista base, caso en el cual la asumo como fija)
		begin
			select valor
			into v_par
			from iposs.parametros_empresas
			where par_codigo = 'DIS_LIS_PC_CI'
			and emp_codigo = p_emp_codigo;
		exception
		when no_data_found then
			v_par := '0';
		end;
		
		-- siempre hay valor, no funciona el sistema sin este parámetro
		select to_number(valor)
		into v_mon_def
		from iposs.parametros
		where codigo = 'MONEDA_DEF';
		
		if to_number(v_par) = p_lis_codigo 		-- es la lista de costo con impuesto (no busco sin impuestos porque ya no se usa)
		then
			begin
				select costo_imp, mon_codigo
				into v_pre, v_mon
				from iposs.precios_de_costos_articulos
				where art_codigo = p_art_codigo
				and loc_codigo = p_loc_codigo
				and emp_codigo = p_emp_codigo
				and p_fecha between vigencia_desde and vigencia_hasta
				and rownum = 1;		-- seguridad
			exception
			when no_data_found then
				v_pre := 0;
				v_mon := v_mon_def;
			end;
		else							-- es una lista fija común
			begin
				select precio, mon_codigo
				into v_pre, v_mon
				from iposs.precios_de_articulos_ventas
				where art_codigo = p_art_codigo
				and loc_lis_lis_pre_ve_codigo = p_lis_codigo
				and loc_lis_loc_codigo = p_loc_codigo
				and emp_codigo = p_emp_codigo
				and p_fecha between vigencia and vigencia_hasta
				and rownum = 1;		-- seguridad
			exception
			when no_data_found then
				v_pre := 0;
				v_mon := v_mon_def;
			end;
		end if;
		
		if v_mon <> v_mon_def
		then
			begin
				select valor_compra
				into v_cotiza
				from iposs.cambios
				where mon_codigo = v_mon
				and p_fecha between fecha and vigencia_hasta
				and emp_codigo = p_emp_codigo;
			exception
			when no_data_found then
				v_cotiza := 0;		-- si no hay cotización, no muestro datos
			end;
		end if;
		
		return v_pre * v_cotiza;
		
	end if;
			
end;
/

grant execute on iposs.f_precio_venta_2 to iposs_rep
/
