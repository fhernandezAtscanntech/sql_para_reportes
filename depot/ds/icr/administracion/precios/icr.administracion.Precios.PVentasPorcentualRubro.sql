select
	l.codigo 		"Cod.Local (I)", 
	l.loc_cod_empresa	"Cod.Local", 
	l.descripcion 		"Local", 
	r.descripcion  		"Rubro", 
	lis.descripcion 	"Lista de precios", 
	al.art_codigo_Externo 	"Cod.Art�culo", 
	ae.descripcion 		"Art�culo", 
	nvl (
		(select cb.codigo_barra
		from codigos_barras cb
		where cb.art_codigo = ae.art_codigo
		and (cb.emp_codigo = ae.emp_codigo or (cb.emp_codigo = 1 and ae.veo_barras_modelo = 1))
		and rownum = 1), '') 	"Cod.Barra", 
	mo.simbolo 		"Moneda", 
	pv.vigencia 		"Vigencia", 
 	pv.precio 		"Precio Venta Base", 
--
-- esto es demencial: tengo que ver qu� n�mero de lista de precios de venta es para saber qu� par�metro buscar 
-- y as� saber en qu� porcentaje est� grabado el descuento
--
	(1 + decode (lis.lis_pre_ve_cod_empresa, 
			2, decode (nvl ((select valor from parametros_empresas pe where pe.par_codigo = 'LISTA_DOS' and pe.emp_codigo = ae.emp_codigo), 0), 
				2, (nvl(al.porc_lista_2, 0) / 100), 
				3, (nvl(al.porc_lista_3, 0) / 100), 
				4, (nvl(al.porc_lista_4, 0) / 100), 
				5, (nvl(al.porc_lista_5, 0) / 100), 
				6, (nvl(al.porc_lista_6, 0) / 100), 
				7, (nvl(al.porc_lista_7, 0) / 100), 
				8, (nvl(al.porc_lista_8, 0) / 100), 
				9, (nvl(al.porc_lista_9, 0) / 100), 
				10, (nvl(al.porc_lista_10, 0) / 100), 
				0), 
			3, decode (nvl ((select valor from parametros_empresas pe where pe.par_codigo = 'LISTA_TRE' and pe.emp_codigo = ae.emp_codigo), 0), 
				2, (nvl(al.porc_lista_2, 0) / 100), 
				3, (nvl(al.porc_lista_3, 0) / 100), 
				4, (nvl(al.porc_lista_4, 0) / 100), 
				5, (nvl(al.porc_lista_5, 0) / 100), 
				6, (nvl(al.porc_lista_6, 0) / 100), 
				7, (nvl(al.porc_lista_7, 0) / 100), 
				8, (nvl(al.porc_lista_8, 0) / 100), 
				9, (nvl(al.porc_lista_9, 0) / 100), 
				10, (nvl(al.porc_lista_10, 0) / 100), 
				0), 
			4, decode (nvl ((select valor from parametros_empresas pe where pe.par_codigo = 'LISTA_CUA' and pe.emp_codigo = ae.emp_codigo), 0), 
				2, (nvl(al.porc_lista_2, 0) / 100), 
				3, (nvl(al.porc_lista_3, 0) / 100), 
				4, (nvl(al.porc_lista_4, 0) / 100), 
				5, (nvl(al.porc_lista_5, 0) / 100), 
				6, (nvl(al.porc_lista_6, 0) / 100), 
				7, (nvl(al.porc_lista_7, 0) / 100), 
				8, (nvl(al.porc_lista_8, 0) / 100), 
				9, (nvl(al.porc_lista_9, 0) / 100), 
				10, (nvl(al.porc_lista_10, 0) / 100), 
				0), 
			5, decode (nvl ((select valor from parametros_empresas pe where pe.par_codigo = 'LISTA_CIN' and pe.emp_codigo = ae.emp_codigo), 0), 
				2, (nvl(al.porc_lista_2, 0) / 100), 
				3, (nvl(al.porc_lista_3, 0) / 100), 
				4, (nvl(al.porc_lista_4, 0) / 100), 
				5, (nvl(al.porc_lista_5, 0) / 100), 
				6, (nvl(al.porc_lista_6, 0) / 100), 
				7, (nvl(al.porc_lista_7, 0) / 100), 
				8, (nvl(al.porc_lista_8, 0) / 100), 
				9, (nvl(al.porc_lista_9, 0) / 100), 
				10, (nvl(al.porc_lista_10, 0) / 100), 
				0), 
			6, decode (nvl ((select valor from parametros_empresas pe where pe.par_codigo = 'LISTA_SEI' and pe.emp_codigo = ae.emp_codigo), 0), 
				2, (nvl(al.porc_lista_2, 0) / 100), 
				3, (nvl(al.porc_lista_3, 0) / 100), 
				4, (nvl(al.porc_lista_4, 0) / 100), 
				5, (nvl(al.porc_lista_5, 0) / 100), 
				6, (nvl(al.porc_lista_6, 0) / 100), 
				7, (nvl(al.porc_lista_7, 0) / 100), 
				8, (nvl(al.porc_lista_8, 0) / 100), 
				9, (nvl(al.porc_lista_9, 0) / 100), 
				10, (nvl(al.porc_lista_10, 0) / 100), 
				0), 
			7, decode (nvl ((select valor from parametros_empresas pe where pe.par_codigo = 'LISTA_SIE' and pe.emp_codigo = ae.emp_codigo), 0), 
				2, (nvl(al.porc_lista_2, 0) / 100), 
				3, (nvl(al.porc_lista_3, 0) / 100), 
				4, (nvl(al.porc_lista_4, 0) / 100), 
				5, (nvl(al.porc_lista_5, 0) / 100), 
				6, (nvl(al.porc_lista_6, 0) / 100), 
				7, (nvl(al.porc_lista_7, 0) / 100), 
				8, (nvl(al.porc_lista_8, 0) / 100), 
				9, (nvl(al.porc_lista_9, 0) / 100), 
				10, (nvl(al.porc_lista_10, 0) / 100), 
				0), 
			8, decode (nvl ((select valor from parametros_empresas pe where pe.par_codigo = 'LISTA_OCH' and pe.emp_codigo = ae.emp_codigo), 0), 
				2, (nvl(al.porc_lista_2, 0) / 100), 
				3, (nvl(al.porc_lista_3, 0) / 100), 
				4, (nvl(al.porc_lista_4, 0) / 100), 
				5, (nvl(al.porc_lista_5, 0) / 100), 
				6, (nvl(al.porc_lista_6, 0) / 100), 
				7, (nvl(al.porc_lista_7, 0) / 100), 
				8, (nvl(al.porc_lista_8, 0) / 100), 
				9, (nvl(al.porc_lista_9, 0) / 100), 
				10, (nvl(al.porc_lista_10, 0) / 100), 
				0), 
			9, decode (nvl ((select valor from parametros_empresas pe where pe.par_codigo = 'LISTA_NUE' and pe.emp_codigo = ae.emp_codigo), 0), 
				2, (nvl(al.porc_lista_2, 0) / 100), 
				3, (nvl(al.porc_lista_3, 0) / 100), 
				4, (nvl(al.porc_lista_4, 0) / 100), 
				5, (nvl(al.porc_lista_5, 0) / 100), 
				6, (nvl(al.porc_lista_6, 0) / 100), 
				7, (nvl(al.porc_lista_7, 0) / 100), 
				8, (nvl(al.porc_lista_8, 0) / 100), 
				9, (nvl(al.porc_lista_9, 0) / 100), 
				10, (nvl(al.porc_lista_10, 0) / 100), 
				0), 
			10, decode (nvl ((select valor from parametros_empresas pe where pe.par_codigo = 'LISTA_DIE' and pe.emp_codigo = ae.emp_codigo), 0), 
				2, (nvl(al.porc_lista_2, 0) / 100), 
				3, (nvl(al.porc_lista_3, 0) / 100), 
				4, (nvl(al.porc_lista_4, 0) / 100), 
				5, (nvl(al.porc_lista_5, 0) / 100), 
				6, (nvl(al.porc_lista_6, 0) / 100), 
				7, (nvl(al.porc_lista_7, 0) / 100), 
				8, (nvl(al.porc_lista_8, 0) / 100), 
				9, (nvl(al.porc_lista_9, 0) / 100), 
				10, (nvl(al.porc_lista_10, 0) / 100), 
				0), 
		0)
	) * pv.precio		"Precio Venta"
from 
	iposs.locales l, 
	iposs.monedas mo, 
	iposs.rubros r, 
	iposs.articulos_locales al, 
	iposs.articulos_empresas ae, 
	iposs.listas_de_precios_ventas lis, 
	iposs.precios_de_articulos_ventas pv
where 						-- articulos_empresas - articulos_locales
	ae.art_codigo = al.art_Codigo
and 	ae.emp_codigo = al.emp_codigo
and  						-- articulos_empresas - rubros
	ae.rub_codigo = r.codigo
and   						-- articulos_locales - locales
	al.loc_Codigo = l.codigo
and   						-- articulos_locales - precios (pventa lista base)
	al.art_codigo = pv.art_codigo
and 	al.loc_Codigo = pv.loc_lis_loc_codigo
and 						-- lista - precios (tomo la lista base)
	lis.lis_pv_base_codigo = pv.loc_lis_lis_pre_ve_codigo
and 						-- moneda - precios
	mo.codigo = pv.mon_codigo
and   						-- precios actuales
	sysdate between pv.vigencia and pv.vigencia_hasta
and 						-- solo art�culos activos
	al.fecha_borrado is null
and 	ae.fecha_borrado is null
and 						-- solo listas porcentuales
	nvl (lis.tipo_lista, 0) = 1
and
	( trim (upper (':Local')) = 'NULL' or l.codigo in :Local )
and
	( trim (upper (':Rubro')) = 'NULL' or r.codigo in :Rubro )
and 	
	( ae.emp_codigo  = $EMPRESA_LOGUEADA$
and 	  al.emp_codigo  = $EMPRESA_LOGUEADA$
and 	  l.emp_codigo   = $EMPRESA_LOGUEADA$
and 	  lis.emp_codigo = $EMPRESA_LOGUEADA$
and 	  pv.emp_codigo  = $EMPRESA_LOGUEADA$ )

