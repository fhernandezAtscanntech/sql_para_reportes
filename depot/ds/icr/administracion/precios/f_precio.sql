create or replace 
function iposs.F_PRECIO_VENTA (
	p_art_codigo 	iposs.articulos_empresas.art_codigo%type, 
	p_loc_codigo 	iposs.locales.codigo%type, 
	p_lis_codigo 	iposs.listas_de_precios_ventas.codigo%type, 
	p_fecha 		in date,
	p_emp_codigo 	in number)
return number
is
	v_tipo		number;
	v_porc 		number;
	v_pre		iposs.precios_de_articulos_ventas.precio%type;
	v_ben		number;
	v_par		iposs.parametros_empresas.par_codigo%type;
	v_lis_emp	number;
	v_lis_base	iposs.listas_de_precios_ventas.lis_pv_base_codigo%type;
begin
	begin
		select nvl (tipo_lista, 0), lis_pre_ve_Cod_empresa, lis_pv_base_codigo
		into v_tipo, v_lis_emp, v_lis_base
		from iposs.listas_de_precios_ventas
		where codigo = p_lis_codigo
		and emp_codigo = p_emp_codigo;
	exception
	when no_data_found then
		return (0);
	end;

	if (v_tipo = 1) and (v_lis_base is not null)		-- tipo de lista es porcentual y hay lista base
	then
		-- ######################################
		-- ####### Averiguo el porcentaje ####### 
		-- ######################################
		begin			-- busco en la tabla del BE nuevo
			select porcentaje
			into v_porc
			from iposs.porcentajes_articulos_ventas
			where art_codigo = p_art_codigo
			and loc_lis_lis_pre_ve_codigo = p_lis_codigo
			and loc_lis_loc_codigo = p_loc_codigo
			and p_fecha between vigencia and vigencia_hasta
			and rownum = 1; 		-- seguridad
			
			v_ben := 1;
		exception
		when no_data_found then		-- si no está, busco en las viejas
			begin					-- primero averiguo en qué parámetro está la lista
				select par_codigo
				into v_par
				from iposs.parametros_empresas
				where valor = to_char(v_lis_emp)
				and par_codigo like 'LISTA_%'
				and emp_codigo = p_emp_codigo;
			exception
			when no_data_found then
				return (0);
			end;
			
			v_porc := null;
			v_ben := 0;
			
			begin
				if v_par = 'LISTA_DOS'	-- dependiendo del parámetro, es el porcentaje a usar
				then
					select nvl(porc_lista_2, 0)
					into v_porc
					from iposs.articulos_locales
					where art_codigo = p_art_codigo
					and loc_codigo = p_loc_codigo
					and emp_codigo = p_emp_codigo;
				elsif v_par = 'LISTA_TRE'
				then
					select nvl(porc_lista_3, 0)
					into v_porc
					from iposs.articulos_locales
					where art_codigo = p_art_codigo
					and loc_codigo = p_loc_codigo
					and emp_codigo = p_emp_codigo;
				elsif v_par = 'LISTA_CUA'
				then
					select nvl(porc_lista_4, 0)
					into v_porc
					from iposs.articulos_locales
					where art_codigo = p_art_codigo
					and loc_codigo = p_loc_codigo
					and emp_codigo = p_emp_codigo;
				elsif v_par = 'LISTA_CIN'
				then
					select nvl(porc_lista_5, 0)
					into v_porc
					from iposs.articulos_locales
					where art_codigo = p_art_codigo
					and loc_codigo = p_loc_codigo
					and emp_codigo = p_emp_codigo;
				elsif v_par = 'LISTA_SEI'
				then
					select nvl(porc_lista_6, 0)
					into v_porc
					from iposs.articulos_locales
					where art_codigo = p_art_codigo
					and loc_codigo = p_loc_codigo
					and emp_codigo = p_emp_codigo;
				elsif v_par = 'LISTA_SIE'
				then
					select nvl(porc_lista_7, 0)
					into v_porc
					from iposs.articulos_locales
					where art_codigo = p_art_codigo
					and loc_codigo = p_loc_codigo
					and emp_codigo = p_emp_codigo;
				elsif v_par = 'LISTA_OCH'
				then
					select nvl(porc_lista_8, 0)
					into v_porc
					from iposs.articulos_locales
					where art_codigo = p_art_codigo
					and loc_codigo = p_loc_codigo
					and emp_codigo = p_emp_codigo;
				elsif v_par = 'LISTA_NUE'
				then
					select nvl(porc_lista_9, 0)
					into v_porc
					from iposs.articulos_locales
					where art_codigo = p_art_codigo
					and loc_codigo = p_loc_codigo
					and emp_codigo = p_emp_codigo;
				elsif v_par = 'LISTA_DIE'
				then
					select nvl(porc_lista_10, 0)
					into v_porc
					from iposs.articulos_locales
					where art_codigo = p_art_codigo
					and loc_codigo = p_loc_codigo
					and emp_codigo = p_emp_codigo;
				end if;
			exception
			when no_data_found then
				return (0);
			end;

		end;
		
		-- ######################################
		-- ##### Averiguo sobre qué precio ###### 
		-- ######################################
			
		v_pre := iposs.f_precio_venta (p_art_codigo, p_loc_codigo, v_lis_base, p_fecha, p_emp_codigo);
		
		-- ######################################
		-- ############## RETORNO ###############
		-- ######################################

		if v_ben = 0		-- BE viejo
		then
			return (v_pre * (1 + (v_porc / 100)));
		else				-- BE nuevo
			return (v_pre * (1 - (v_porc / 100)));
		end if;

	else					-- Es lista fija (o es porcentual pero no indicaron la lista base, caso en el cual la asumo como fija)
		begin
			select valor
			into v_par
			from iposs.parametros_empresas
			where par_codigo = 'DIS_LIS_PC_CI'
			and emp_codigo = p_emp_codigo;
		exception
		when no_data_found then
			v_par := '0';
		end;
			
		if to_number(v_par) = p_lis_codigo 		-- es la lista de costo con impuesto (no busco sin impuestos porque ya no se usa)
		then
			begin
				select costo_imp
				into v_pre
				from iposs.precios_de_costos_articulos
				where art_codigo = p_art_codigo
				and loc_codigo = p_loc_codigo
				and emp_codigo = p_emp_codigo
				and p_fecha between vigencia_desde and vigencia_hasta
				and rownum = 1;		-- seguridad
			exception
			when no_data_found then
				v_pre := 0;
			end;
		else							-- es una lista fija común
			begin
				select precio
				into v_pre
				from iposs.precios_de_articulos_ventas
				where art_codigo = p_art_codigo
				and loc_lis_lis_pre_ve_codigo = p_lis_codigo
				and loc_lis_loc_codigo = p_loc_codigo
				and emp_codigo = p_emp_codigo
				and p_fecha between vigencia and vigencia_hasta
				and rownum = 1;		-- seguridad
			exception
			when no_data_found then
				v_pre := 0;
			end;
		end if;
		return v_pre;
		
	end if;
			
end;
/

grant execute on f_precio_venta to iposs_rep
/
