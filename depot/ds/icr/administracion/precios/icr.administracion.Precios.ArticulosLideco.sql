select 
	ae.art_codigo_externo 	"Cod.Artículo", 
	(select cb.codigo_barra
		from iposs.codigos_barras cb
		where cb.art_codigo = ae.art_codigo
		and (cb.emp_codigo = ae.emp_codigo or (cb.emp_codigo = 1 and ae.veo_barras_modelo = 1))
		and rownum = 1) 	"Cod.Barra", 
	ae.descripcion 			"Artículo", 
	lpv.descripcion 		"Lista Precio", 
	l.descripcion 			"Local", 
	r.descripcion 			"Rubro", 
	pv.precio 				"Precio Venta"
from 
	iposs.articulos_empresas ae, 
	iposs.articulos_locales al, 
	iposs.listas_de_precios_ventas lpv, 
	iposs.locales l, 
	iposs.precios_de_articulos_ventas pv, 
	iposs.rubros r
where 	-- transcripto de Discoverer
		( ( al.loc_codigo = pv.loc_lis_loc_codigo 
and 	    al.art_codigo = pv.art_codigo ) 
and 	  ( l.codigo = al.loc_codigo ) 
and 	  ( r.codigo = ae.rub_codigo ) 
and 	  ( ae.emp_codigo = al.emp_codigo 
and 	    ae.art_codigo = al.art_codigo ) 
and 	  ( lpv.codigo = pv.loc_lis_lis_pre_ve_codigo ) ) 
and 	( ae.grupo_codigo = 3 )  		-- Lideco
and 	( pv.vigencia <= sysdate ) 
and 	( pv.vigencia_hasta > sysdate ) 
and 	( ae.fecha_borrado is null and al.fecha_borrado is null)
and 	( l.codigo = :"Local" ) 
and 	( ae.emp_codigo = $EMPRESA_LOGUEADA$
and 	  al.emp_codigo = $EMPRESA_LOGUEADA$
and 	  lpv.emp_codigo = $EMPRESA_LOGUEADA$
and 	  l.emp_codigo = $EMPRESA_LOGUEADA$
and 	  pv.emp_codigo = $EMPRESA_LOGUEADA$ )