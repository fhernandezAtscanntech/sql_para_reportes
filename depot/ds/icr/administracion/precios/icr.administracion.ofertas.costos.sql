SELECT DISTINCT 
	ARTICULOS_EMPRESAS.ART_CODIGO_EXTERNO "Art Codigo Externo",  
	ARTICULOS_EMPRESAS.DESCRIPCION "Articulo", 
	LOCALES.DESCRIPCION "Local", 
	OFERTAS.INICIO "Inicio",
	OFERTAS.FIN "Fin", 
	OFERTAS.PRECIO "Precio", 
	(select  pc.costo_imp
		from iposs.precios_de_costos_articulos pc
		where pc.art_codigo = ARTICULOS_EMPRESAS.ART_CODIGO
		and pc.loc_codigo = LOCALES.CODIGO
		and pc.emp_codigo = ARTICULOS_EMPRESAS.EMP_CODIGO
		and :FechaOferta between pc.vigencia_desde and pc.vigencia_hasta) "Costo Fecha", 
	(select  pc.costo_imp
		from iposs.precios_de_costos_articulos pc
		where pc.art_codigo = ARTICULOS_EMPRESAS.ART_CODIGO
		and pc.loc_codigo = LOCALES.CODIGO
		and pc.emp_codigo = ARTICULOS_EMPRESAS.EMP_CODIGO
		and sysdate between pc.vigencia_desde and pc.vigencia_hasta) "Último Costo", 
FROM
	IPOSS.ARTICULOS_EMPRESAS ARTICULOS_EMPRESAS, 
	IPOSS.LOCALES LOCALES, 
	IPOSS.OFERTAS OFERTAS
WHERE ( ( ARTICULOS_EMPRESAS.ART_CODIGO = OFERTAS.ART_CODIGO )
		AND ( ARTICULOS_EMPRESAS.EMP_CODIGO = $EMPRESA_LOGUEADA$ )
		AND ( LOCALES.EMP_CODIGO =$EMPRESA_LOGUEADA$ ) 
		AND ( LOCALES.CODIGO = OFERTAS.LOC_CODIGO ) 
		)
		AND ( :FechaOferta BETWEEN OFERTAS.INICIO AND OFERTAS.FIN ) 
		AND ( LOCALES.CODIGO in :local ) 
		AND ( ARTICULOS_EMPRESAS.FECHA_BORRADO IS NULL )


