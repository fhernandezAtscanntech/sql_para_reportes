select
	r.rub_Cod_empresa 	"Rub.Codigo", 
	r.descripcion 		"Rubro", 
	l.loc_cod_empresa 	"Loc.Codigo", 
	l.descripcion 	"Local"
from 
	iposs.rubros r, 
	iposs.rubros_locales rl, 
	iposs.locales l, 
	#LocalesVisibles# lv
where 
	( r.codigo = rl.rub_codigo )
and 
	( l.codigo = rl.loc_codigo )
and
	( l.codigo = lv.loc_codigo )
and 	
	( r.fecha_borrado is null 
and 	  rl.fecha_borrado is null)
and 
	( r.emp_codigo in ($EMPRESA_LOGUEADA$, $EMPRESA_MODELO$) 
and 	  l.emp_codigo in ($EMPRESA_LOGUEADA$, $EMPRESA_MODELO$) )
