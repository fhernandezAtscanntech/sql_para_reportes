select
	f.fam_Cod_empresa 	"Cod. Familia", 
	f.descripcion 		"Familia", 
	s.codigo 			"Cod. Subfamilia", 
	s.descripcion 	"Subfamilia", 
	l.descripcion 	"Local"
from 
	iposs.familias f, 
	iposs.familias_locales fl, 
	iposs.subfamilias s, 
	iposs.locales l -- , 
	#LocalesVisibles# lv
where 	( f.codigo = fl.fam_codigo )
and 	( l.codigo = fl.loc_codigo )
and
	( l.codigo = lv.loc_codigo )
and 	( f.codigo = s.fam_codigo (+))
and 	( f.fecha_borrado is null 
and 	  fl.fecha_borrado is null)
and 	( l.codigo in :Local )
and 	( f.emp_codigo in ($EMPRESA_LOGUEADA$, $EMPRESA_MODELO$) 
and 	  l.emp_codigo in ($EMPRESA_LOGUEADA$, $EMPRESA_MODELO$) )
