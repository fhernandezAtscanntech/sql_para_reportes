select
	c.cli_cod_externo	"Cod.Cliente", 
	c.nombre 		"Cliente", 
	trim (case when p.nombre_1 is null then null else p.nombre_1||' ' end||
	case when p.nombre_2 is null then null else p.nombre_2||' ' end||
	case when p.apellido_1 is null then null else p.apellido_1||' ' end||
	case when p.apellido_2 is null then null else p.apellido_2 end) "Persona",       
	c.numero_doc 		"Numero Documento", 
	c.dir_calle 		"Calle Dir.", 
	c.dir_numero 		"N�mero Dir.", 
	c.dir_apto 		"Apto Dir.", 
	case c.estado
		when 0 then 'Inactivo'
		when 1 then 'Activo'
		when 2 then 'Moroso Afinidad Activo'
		when 3 then 'Moroso Afinidad Inactivo'
		when 4 then 'Bloqueado Afinidad Activo'
		when 5 then 'Bloqueado Afinidad Inactivo'
		when 6 then 'Captura'
		else 		'Desconocido'
	end 		"Estado", 
	c.telefono 		"Tel�fono", 
	(select g.descripcion
		from grupos_De_clientes g
		where g.codigo = c.gru_cli_codigo) 	"Grupo", 
	lp.descripcion 		"Lista Precio Venta", 
	c.desc_rango_desde 	"Dto x Cant Desde", 
	c.desc_rango_hasta 	"Dto x Cant Hasta", 
	c.audit_date 		"Fecha alta", 
	c.nacimiento 		"Nacimiento", 
	c.e_mail 		"E-Mail"
from 
	iposs.clientes c 
	left join iposs.listas_de_precios_ventas lp on lp.codigo = c.lis_pre_ve_codigo
	left join iposs.personas p on p.codigo = c.per_codigo
where 
	c.fecha_borrado is null
and 
	c.codigo in 
		(select cl.cli_codigo
		from iposs.clientes_locales cl, #LocalesVisibles# lv
		where cl.loc_codigo = lv.loc_codigo
		and cl.fecha_borrado is null)
and
	c.emp_codigo = $EMPRESA_LOGUEADA$

	
/*
	ACTIVO = 1;
	INACTIVO = 0;
	MOROSO_AFINIDAD_ACTIVO = 2;
	MOROSO_AFINIDAD_INACTIVO = 3;
	BLOQUEADO_AFINIDAD_ACTIVO = 4;
	BLOQUEADO_AFINIDAD_INACTIVO = 5;
	CAPTURA = 6;

*/
