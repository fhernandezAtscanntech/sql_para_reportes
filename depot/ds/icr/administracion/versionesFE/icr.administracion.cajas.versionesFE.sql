select
	l.codigo 		"Cod.Local (I)", 
	l.loc_cod_empresa	"Cod.Local", 
	l.emp_codigo 		"Cod.Empresa", 
	l.descripcion 		"Local", 
	(select descripcion 
		from departamentos de 
		where de.codigo = l.depa_codigo)	"Departamento", 
	(select descripcion 
		from localidades lo 
		where lo.codigo = l.loca_codigo)	"Localidad", 
	r.descripcion 		"Razón social", 
	r.ruc 			"RUT/CUIT/CNPJ", 
	c.codigo 		"Cod.Caja", 
	c.descripcion 		"Caja", 
	decode (c.inactiva, 
		1, 'INACTIVA', 
		'ACTIVA') 	"Inactiva", 
	dp.fecha_disponible 	"Fecha Disponible", 
	dp.fecha_ult_consulta 	"Fecha Ult.Consulta", 
	dp.cant_consultas 	"Cant. Consultas", 
	dp.version 		"Versión", 
	dp.config		"Configuración"
from 
	iposs.locales l, 
	iposs.razones_sociales r, 
	iposs.cajas c, 
	iposs.distribuciones_pendientes dp
where
	c.loc_codigo = l.Codigo
and 
	r.codigo = l.raz_soc_codigo
and 	r.emp_codigo = l.emp_codigo
and 	
	dp.emp_codigo = l.emp_codigo
and 	dp.loc_codigo = l.codigo
and 	dp.caj_codigo = c.codigo
and 
	( trim (upper (':Local')) = 'NULL' or l.codigo in :Local )

