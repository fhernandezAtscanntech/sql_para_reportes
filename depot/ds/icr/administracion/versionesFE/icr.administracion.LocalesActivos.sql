multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select
		'select
			l.codigo 		"Cod.Local(I)", 
			l.loc_cod_empresa	"Cod.Local", 
			l.emp_codigo 		"Cod.Empresa", 
			l.descripcion 		"Local", 
			(select descripcion 
				from departamentos de 
				where de.codigo = l.depa_codigo)	"Departamento", 
			(select descripcion 
				from localidades lo 
				where lo.codigo = l.loca_codigo)	"Localidad", 
			r.descripcion 		"Razon social", 
			r.ruc 			"RUT_CUIT_CNPJ", 
			l.audit_date 		"Fecha Creación", 			
			max(dp.fecha_ult_consulta) 	"Fecha Ult.Consulta"
		from 
			iposs.locales l, 
			iposs.empresas e, 
			iposs.razones_sociales r, 
			iposs.distribuciones_pendientes dp
		where
			l.emp_codigo = e.codigo
		and 
			r.codigo = l.raz_soc_codigo
		and 	r.emp_codigo = l.emp_codigo
		and 	
			dp.emp_codigo = l.emp_codigo
		and 	dp.loc_codigo = l.codigo
		and 
			( trim (upper ('':Local'')) = ''NULL'' or l.codigo in :Local )
		and 
			l.emp_codigo <> 1
		and 	e.descripcion not like ''%NO USAR%''
		and 	e.descripcion not like ''%BASE2%''
		group by 
			l.codigo, 
			l.loc_cod_empresa, 
			l.emp_codigo, 
			l.descripcion, 
			l.depa_codigo, 
			l.loca_codigo, 
			r.descripcion,
			r.ruc, 
			l.audit_date' as sql from dual)

######################
tabla
######################

create table $$tabla$$ as (
	select
		l.codigo 		"Cod.Local (I)", 
		l.loc_cod_empresa	"Cod.Local", 
		l.emp_codigo 		"Cod.Empresa", 
		l.descripcion 		"Local", 
		(select descripcion 
			from departamentos de 
			where de.codigo = l.depa_codigo)	"Departamento", 
		(select descripcion 
			from localidades lo 
			where lo.codigo = l.loca_codigo)	"Localidad", 
		r.descripcion 		"Razon social", 
		r.ruc 			"RUT_CUIT_CNPJ", 
		l.audit_date 		"Fecha Creación", 		
		max(dp.fecha_ult_consulta) 	"Fecha Ult.Consulta"
	from 
		iposs.locales l, 
		iposs.razones_sociales r, 
		iposs.distribuciones_pendientes dp
	where
		1 = 2		
	group by 
		l.codigo, 
		l.loc_cod_empresa, 
		l.emp_codigo, 
		l.descripcion, 
		l.depa_codigo, 
		l.loca_codigo, 
		r.descripcion,
		r.ruc, 
		l.audit_date
		
######################
consulta
######################
select 1 from #rec#