select
	l.codigo 		"Cod.Local (I)", 
	l.loc_cod_empresa	"Cod.Local", 
	l.descripcion 		"Local", 
	al.art_codigo_Externo 	"Cod.Art�culo padre", 
	al2.art_codigo_Externo 	"Cod.Art�culo hijo", 
	ae.descripcion 		"Art�culo padre", 
	ae2.descripcion 	"Art�culo hijo", 
	re.cantidad 		"Cantidad en combo", 
	t.descripcion 		"Tipo art�culo padre", 
	t2.descripcion 		"Tipo art�culo hijo", 
	(select i.descripcion 
		from iposs.ivas i 
		where i.codigo = ae2.iva_codigo) "IVA Compra", 
	(select i.descripcion 
		from iposs.ivas i 
		where i.codigo = nvl (ae2.iva_vta_codigo, ae2.iva_codigo)) "IVA Venta", 
	r.rub_Cod_empresa 	"Cod.Rubro", 
	r.descripcion 		"Rubro", 
	(select f.fam_cod_empresa
		from iposs.familias f
		where f.codigo = ae2.fam_codigo) "Cod.Familia", 		
	(select f.descripcion 
		from iposs.familias f 
		where f.codigo = ae2.fam_codigo) "Familia", 
	ae2.sub_codigo  			"Cod.Sub Familia", 
	(select s.descripcion 
		from iposs.subfamilias s 
		where s.codigo = ae2.sub_codigo 
		and s.fam_codigo = ae2.fam_codigo) "SubFamilia", 
	nvl (
		(select cb.codigo_barra
		from codigos_barras cb
		where cb.art_codigo = ae2.art_codigo
		and (cb.emp_codigo = ae2.emp_codigo or (cb.emp_codigo = 1 and ae2.veo_barras_modelo = 1))
		and rownum = 1), '') 	"Cod.Barra", 
	aes.descripcion 		"Estado", 
	nvl (al2.fecha_borrado, ae2.fecha_borrado) 		"Fecha Borrado", 
	ae2.audit_date 		"Fecha creaci�n", 
	ae2.modif_date 		"�ltima modificaci�n", 
	ae2.unidades_compra 	"Unidades Compra", 
	decode (ae2.bonificable, 1, 'BONIFICABLE', 'NO BONIFICABLE') 	"Bonificable"
from 
	iposs.locales l, 
	iposs.articulos_locales al, 
	iposs.articulos_locales al2, 
	iposs.articulos_empresas ae, 
	iposs.articulos_empresas ae2, 
	iposs.articulos_estados aes, 
	iposs.rubros r, 
	iposs.recetas re, 
	iposs.tipos_de_articulos t, 
	iposs.tipos_de_articulos t2
	
where 	ae.art_codigo = al.art_Codigo
and 	ae.emp_codigo = al.emp_codigo
and 
		ae2.art_codigo = al2.art_Codigo
and 	ae2.emp_codigo = al2.emp_codigo
and 	
		ae.tipo = t.codigo
and 	ae2.tipo = t2.codigo
and 
		ae.art_codigo = re.art_codigo
and 	ae2.art_codigo = re.art_codigo_hijo
and 
		al.loc_Codigo = l.codigo
and 	al2.loc_codigo = l.codigo
and
		ae2.rub_codigo = r.codigo
and 
		ae2.est_codigo = aes.art_est_cod_empresa
and 	al2.emp_codigo = aes.emp_codigo
and 	
		(nvl(:Borrados, 0) = 1 or (al2.fecha_borrado is null and ae2.fecha_borrado is null))	
and 
		l.codigo in :Local 
and 	
		ae.emp_codigo  = $EMPRESA_LOGUEADA$
and 	al.emp_codigo  = $EMPRESA_LOGUEADA$
and 	ae2.emp_codigo  = $EMPRESA_LOGUEADA$
and 	al2.emp_codigo  = $EMPRESA_LOGUEADA$
and 	l.emp_codigo   = $EMPRESA_LOGUEADA$
and 	r.emp_codigo IN ($EMPRESA_LOGUEADA$, $EMPRESA_MODELO$) 
and 	re.emp_codigo = $EMPRESA_LOGUEADA$ 
