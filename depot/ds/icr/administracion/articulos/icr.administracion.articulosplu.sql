select 
	ae.art_codigo_externo 	"Codigo", 
	ae.descripcion 			"Articulo", 
	lpv.descripcion 		"Listas De Precios", 
	l.descripcion 			"Local", 
	r.descripcion 			"Rubro", 
	ae.PLU 					"PLU", 
	ae.venta_fraccionada 	"Venta Fraccionada", 
	decode (ae.exportable, 1, 'SI', 'NO') "Exportable", 
	pv.vigencia 			"Vigencia", 
	pv.precio 				"Precio"
from 
	iposs.articulos_empresas ae, 
	iposs.listas_de_precios_ventas lpv, 
	iposs.listas_pre_ventas_locales lpvl, 
	iposs.locales l, 
	iposs.precios_de_articulos_ventas pv, 
	iposs.rubros r
where 	lpv.codigo = lpvl.lis_pre_ve_codigo 
and 	l.codigo = pv.loc_lis_loc_codigo 
and 	r.codigo = ae.rub_codigo 
and 	ae.emp_codigo = pv.emp_codigo 
and 	ae.art_codigo = pv.art_codigo 
and 	lpvl.loc_codigo = pv.loc_lis_loc_codigo 
and 	lpvl.lis_pre_ve_codigo = pv.loc_lis_lis_pre_ve_codigo 
and 
		ae.plu is not null  
and 	r.fecha_borrado is null  
and 	
		sysdate between pv.vigencia and pv.vigencia_hasta 
and 	l.codigo in :local 
and 	r.codigo in :rubro
and 
		ae.emp_codigo = $EMPRESA_LOGUEADA$
and 	lpv.emp_codigo = $EMPRESA_LOGUEADA$
and 	pv.emp_codigo = $EMPRESA_LOGUEADA$
