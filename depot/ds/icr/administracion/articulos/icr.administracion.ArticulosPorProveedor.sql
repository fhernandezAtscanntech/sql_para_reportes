select
	l.codigo 		"Cod.Local (I)", 
	l.loc_cod_empresa	"Cod.Local", 
	l.descripcion 		"Local", 
	lis.descripcion 	"Lista de precios", 
	al.art_codigo_Externo 	"Cod.Art�culo", 
	ae.descripcion 		"Art�culo", 
	r.rub_cod_empresa 	"Cod.Rubro", 
	r.descripcion 		"Rubro", 
	(select i.descripcion from iposs.ivas i where i.codigo = ae.iva_codigo) "IVA Compra", 
	(select i.descripcion from iposs.ivas i where i.codigo = nvl (ae.iva_vta_codigo, ae.iva_codigo)) "IVA Venta", 
	pe.prov_cod_externo 	"Cod.Proveedor", 
	pa.codigo_art_proveedor	"Cod.Art.Proveedor", 
	p.razon_social 		"Raz�n Social", 
	nvl (
		(select cb.codigo_barra
		from codigos_barras cb
		where cb.art_codigo = ae.art_codigo
		and (cb.emp_codigo = ae.emp_codigo or (cb.emp_codigo = 1 and ae.veo_barras_modelo = 1))
		and rownum = 1), '') 	"Cod.Barra", 
	mo.simbolo 		"Moneda", 
	pv.precio 		"Precio Venta", 
	pv.vigencia 		"Vigencia"
from 
	iposs.locales l, 
	iposs.monedas mo, 
	iposs.articulos_locales al, 
	iposs.articulos_empresas ae, 
	iposs.rubros r, 
	iposs.listas_de_precios_ventas lis, 
	iposs.proveedores_articulos pa, 
	iposs.articulos_locales_proveedores alp, 
	iposs.proveedores_empresas pe, 
	iposs.proveedores p, 
	iposs.precios_de_articulos_ventas pv	
where 
	ae.art_codigo = al.art_Codigo
and 	ae.emp_codigo = al.emp_codigo
and 
	ae.rub_codigo = r.codigo
and 
	al.loc_Codigo = l.codigo
and
	al.art_codigo = pa.art_codigo
and 	al.emp_codigo = pa.emp_codigo
and 
	pe.prov_codigo = pa.prov_codigo
and 	pe.emp_codigo = pa.emp_codigo
and 	
	al.art_codigo = alp.art_loc_art_codigo
and 	al.loc_codigo = alp.art_loc_loc_codigo
and 	al.emp_codigo = alp.emp_codigo
and 
	pe.prov_codigo = alp.prov_codigo
and 	pe.emp_codigo = alp.emp_codigo
and 
	pe.prov_codigo = p.codigo
and 
	al.art_codigo = pv.art_codigo
and 	al.loc_Codigo = pv.loc_lis_loc_codigo
and 
	lis.codigo = pv.loc_lis_lis_pre_ve_codigo
and 
	mo.codigo = pv.mon_codigo
and 
	sysdate between pv.vigencia and pv.vigencia_hasta
and 
	al.fecha_borrado is null
and 	ae.fecha_borrado is null
and 	pa.fecha_borrado is null
and 	alp.fecha_borrado is null
and 	pe.fecha_borrado is null
and 
	(nvl(:UltProv, 0) = 0 or (nvl (:UltProv, 0) = 1 and pa.proveedor_principal = 1))
and 
	( trim (upper (':Local')) = 'NULL' or l.codigo in :Local )
and
	( trim (upper (':Proveedor')) = 'NULL' or p.codigo in :Proveedor )
and 	
	( ae.emp_codigo  = $EMPRESA_LOGUEADA$
and 	  al.emp_codigo  = $EMPRESA_LOGUEADA$
and 	  l.emp_codigo   = $EMPRESA_LOGUEADA$
and 	  lis.emp_codigo = $EMPRESA_LOGUEADA$
and 	  pv.emp_codigo  = $EMPRESA_LOGUEADA$
and 	  pa.emp_codigo  = $EMPRESA_LOGUEADA$
and 	  alp.emp_codigo = $EMPRESA_LOGUEADA$
and 	  pe.emp_codigo  = $EMPRESA_LOGUEADA$ )
