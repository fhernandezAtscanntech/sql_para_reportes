select 
	ae.art_codigo_externo 	"Codigo", 
	ae.descripcion 			"Articulo", 
	lpv.descripcion 		"Listas De Precios", 
	l.descripcion 			"Local", 
	r.descripcion 			"Rubro", 
	ae.PLU 					"PLU", 
	ae.venta_fraccionada 	"Venta Fraccionada", 
	decode (ae.exportable, 1, 'SI', 'NO') "Exportable", 
	pv.vigencia 			"Vigencia", 
	pv.precio 				"Precio", 
	pe.prov_cod_externo 	"Cod.Proveedor", 
	pa.codigo_art_proveedor	"Cod.Art.Proveedor", 
	p.razon_social 			"Razón Social", 
	p.RUC 					"RUT", 
	decode (pa.proveedor_principal, 0, 'NO', 'SI') 	"Proveedor Principal"
from 
	iposs.articulos_empresas ae, 
	iposs.listas_de_precios_ventas lpv, 
	iposs.listas_pre_ventas_locales lpvl, 
	iposs.locales l, 
	iposs.precios_de_articulos_ventas pv, 
	iposs.rubros r, 
	iposs.proveedores_articulos pa, 
	iposs.proveedores_empresas pe, 
	iposs.proveedores p
where 	lpv.codigo = lpvl.lis_pre_ve_codigo 
and 	l.codigo = pv.loc_lis_loc_codigo 
and 	r.codigo = ae.rub_codigo 
and 	ae.emp_codigo = pv.emp_codigo 
and 	ae.art_codigo = pv.art_codigo 
and 	lpvl.loc_codigo = pv.loc_lis_loc_codigo 
and 	lpvl.lis_pre_ve_codigo = pv.loc_lis_lis_pre_ve_codigo 
and 	ae.emp_codigo = pa.emp_codigo
and 	ae.art_codigo = pa.art_codigo
and 	pa.prov_codigo = pe.prov_codigo
and 	pa.emp_codigo = pe.emp_codigo
and 	pe.prov_codigo = p.codigo
and 
		ae.plu is not null  
and 	r.fecha_borrado is null  
and 	pa.fecha_borrado is null
and 	
		sysdate between pv.vigencia and pv.vigencia_hasta 
and 	l.codigo in :Local 
and 	r.codigo in :Rubro
and		( trim (upper (':Proveedor')) = 'NULL' or p.codigo in :Proveedor )
and 	(nvl(:UltProv, 0) = 0 or (nvl (:UltProv, 0) = 1 and pa.proveedor_principal = 1))
and 
		ae.emp_codigo = $EMPRESA_LOGUEADA$
and 	lpv.emp_codigo = $EMPRESA_LOGUEADA$
and 	pv.emp_codigo = $EMPRESA_LOGUEADA$
and 	pa.emp_codigo = $EMPRESA_LOGUEADA$
and 	pe.emp_codigo = $EMPRESA_LOGUEADA$