/*
select f.art_codigo_Externo, f.descripcion, r.descripcion, f.est_mer_codigo, 
(select e.descripcion from estructuras_mercadologicas e where codigo = f.est_mer_1) "Est.Mer.1", 
(select e.descripcion from estructuras_mercadologicas e where codigo = f.est_mer_2) "Est.Mer.2", 
(select e.descripcion from estructuras_mercadologicas e where codigo = f.est_mer_3) "Est.Mer.3", 
(select e.descripcion from estructuras_mercadologicas e where codigo = f.est_mer_4) "Est.Mer.4", 
(select e.descripcion from estructuras_mercadologicas e where codigo = f.est_mer_5) "Est.Mer.5", 
(select e.descripcion from estructuras_mercadologicas e where codigo = f.est_mer_6) "Est.Mer.6", 
(select e.descripcion from estructuras_mercadologicas e where codigo = f.est_mer_7) "Est.Mer.7", 
(select e.descripcion from estructuras_mercadologicas e where codigo = f.est_mer_8) "Est.Mer.8", 
(select e.descripcion from estructuras_mercadologicas e where codigo = f.est_mer_9) "Est.Mer.9", 
(select e.descripcion from estructuras_mercadologicas e where codigo = f.est_mer_10) "Est.Mer.10"
from fredy_10153 f, articulos_empresas ae, rubros r
where f.art_codigo = ae.art_codigo
and ae.rub_codigo = r.codigo
and ae.emp_codigo = 10153
and f.est_mer_codigo is not null
/
*/
         1         2         3         4         5         6         7         8         9        10
1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
create or replace function iposs.getEstMer 
	( p_est_mer iposs.estructuras_mercadologicas.codigo%type, 
	  p_level iposs.estructuras_mercadologicas.nivel%type, 
	  p_ret_def iposs.estructuras_mercadologicas.descripcion%type default null) 
return varchar2
is
	v_est 		iposs.estructuras_mercadologicas%rowtype;
begin
	if p_level > 10 or p_level < 0
	then 
		return p_ret_def;
	end if;
	
	if p_est_mer is null
	then
		return p_ret_def;
	end if;
	
	begin
		select *
		into v_est
		from iposs.estructuras_mercadologicas
		where codigo = p_est_mer;

		if v_est.nivel = p_level
		then
			return (v_est.descripcion);
		else
			if v_est.nivel > p_level
			then
				return (getEstMer (v_Est.est_mer_codigo, p_level));
			else
				return p_ret_def;
			end if;
		end if;
	exception
	when no_data_found then
		return p_ret_def;
	end;
end;
/

grant execute on iposs.getEstMer to public;

select
	l.descripcion 				"Local", 
	al.art_codigo_Externo 		"Cod.Artículo", 
	ae.descripcion 				"Artículo", 
	r.descripcion 				"Rubro", 
	ae.est_mer_codigo 			"Cod.Est.Mer", 
	substr (iposs.getEstMer (ae.est_mer_codigo, 1), 1, 60) 	"Est.Mer.1", 
    substr (iposs.getEstMer (ae.est_mer_codigo, 2), 1, 60)  "Est.Mer.2", 
    substr (iposs.getEstMer (ae.est_mer_codigo, 3), 1, 60)  "Est.Mer.3", 
    substr (iposs.getEstMer (ae.est_mer_codigo, 4), 1, 60)  "Est.Mer.4", 
    substr (iposs.getEstMer (ae.est_mer_codigo, 5), 1, 60)  "Est.Mer.5", 
    substr (iposs.getEstMer (ae.est_mer_codigo, 6), 1, 60)  "Est.Mer.6", 
    substr (iposs.getEstMer (ae.est_mer_codigo, 7), 1, 60)  "Est.Mer.7", 
    substr (iposs.getEstMer (ae.est_mer_codigo, 8), 1, 60)  "Est.Mer.8", 
    substr (iposs.getEstMer (ae.est_mer_codigo, 9), 1, 60)  "Est.Mer.9", 
    substr (iposs.getEstMer (ae.est_mer_codigo, 10), 1, 60)   "Est.Mer.10"
from 
	iposs.articulos_empresas ae, 
	iposs.articulos_locales al, 
	iposs.locales l, 
	iposs.rubros r
where 	ae.art_codigo = al.art_codigo
and 	ae.emp_codigo = al.emp_codigo
and 	ae.rub_codigo = r.codigo
and 	al.loc_codigo = l.codigo
and 	al.emp_codigo = l.emp_codigo
and 	ae.fecha_borrado is null
and 	al.fecha_borrado is null
and 	( l.codigo = :Local )
and 	( trim (upper (':Rubro')) = 'NULL' or r.codigo in :Rubro )
and 	(nvl (:SoloEst, 0) = 0 or (:SoloEst = 1 and ae.est_mer_codigo is not null))
and 	ae.emp_codigo = $EMPRESA_LOGUEADA$
and 	al.emp_codigo = $EMPRESA_LOGUEADA$



