select 
	l.loc_descripcion "Local", 
	fc.fun_descripcion 	"Cajero", 
	p.prod_codigo 	 	"Codigo", 
	p.codigo_barras 	"Barra", 
	p.prod_descripcion 	"Producto", 
	nvl (sum (v.ven_nac_importe), 0) - nvl (sum (v.dev_nac_importe), 0) - nvl (sum (v.bon_nac_importe), 0) "Importe", 
	nvl (sum (v.ven_cantidad), 0) - nvl (sum (v.dev_cantidad), 0) "Cantidad"
from 
	dwm_prod.F_VENTAS_DIARIO v, 
	dwm_prod.l_funcionarios fc, 
	dwm_prod.l_productos p, 
	dwm_prod.l_ubicaciones l
where 
	v.l_fun_codigo_cajero = fc.codigo 
	and v.l_pro_codigo = p.codigo 
	and v.emp_codigo = p.emp_codigo 
	and v.l_ubi_codigo = l.codigo
	and l.loc_codigo in :Local
	and v.fecha_comercial = :Fecha
group by  
	l.loc_descripcion, 
	fc.fun_descripcion, 
	p.prod_codigo, 
	p.codigo_barras, 
	p.prod_descripcion
