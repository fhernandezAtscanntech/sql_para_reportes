select 
	l.loc_descripcion 	"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	f.ano_nombre 			"A�o", 
	f.mes_nombre_corto 		"Mes", 
	f.dia_del_mes 			"D�a", 
	p.prod_codigo 			"Codigo", 
	p.prod_descripcion 		"Articulo", 
	p.codigo_barras 		"Cod.Barra", 
	c.comb_descripcion 		"Combo", 
	r.rub_cod_empresa 		"Cod.Rubro", 		-- pedido de un cliente
	p.rub_descripcion 		"Rubro", 
	p.fam_descripcion 		"Familia", 
	p.sub_descripcion 		"Sub Familia", 
	p.tip_prod_descripcion 		"Tipo producto", 
	sum (nvl (fvpu.ven_cantidad, 0)) - sum (nvl (fvpu.dev_cantidad, 0)) 	"Unidades", 
	sum (nvl (fvpu.ven_nac_importe, 0)) 	"Venta Total", 
	sum (nvl (fvpu.dev_nac_importe, 0)) 	"Devol", 
	sum (nvl (fvpu.bon_nac_importe, 0)) 	"Bonif", 
	sum (nvl (fvpu.des_prom_nac_importe, 0)) 	"Prom", 
	sum (nvl (fvpu.ven_nac_importe, 0)) - 
		sum (nvl (fvpu.dev_nac_importe, 0))  -
		(sum (nvl (fvpu.cos_nac_ult_impuestos, 0))) 	"Utilidad", 
	decode (sum (nvl (fvpu.cos_nac_ult_impuestos, 0)), 
		0, null, 
		(((sum (nvl (fvpu.ven_nac_importe, 0)) - sum (nvl (fvpu.dev_nac_importe, 0))) 
		/ sum (nvl (fvpu.cos_nac_ult_impuestos, 0))) 
		- 1) * 100) "Utilidad Porc.", 
	sum (nvl (fvpu.cos_nac_ult_impuestos, 0))	"Costo Ultimo", 
	sum (nvl (fvpu.cos_nac_ult_importe, 0)) 	"Costo Ultimo S/I", 
	sum (nvl (fvpu.iva_importe, 0)) 	"IVA", 
	sum (nvl (fVpu.ven_nac_importe, 0)) - 
		sum (nvl (fvpu.dev_nac_importe, 0)) "Venta"
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.l_fechas f, 
	dwm_prod.l_productos p, 
	dwm_prod.l_combos c, 
	iposs.rubros r, 
	dwm_prod.f_ventas_pu_diario_min fvpu
where
	( l.codigo = fvpu.l_ubi_codigo 
and 	l.emp_codigo = fvpu.emp_codigo ) 
and
	( p.codigo = fvpu.l_pro_codigo 
and 	  p.emp_codigo = fvpu.emp_codigo )
and 
	( c.codigo = fvpu.l_comb_codigo ) -- va sin empresa porque el combo por defecto es de la 1
and
	( p.rub_codigo = r.codigo )
and
	( fvpu.fecha_comercial = f.codigo )
and
	( fvpu.fecha_comercial >= :FechaDesde and 
	  fvpu.fecha_comercial < :FechaHasta )
and
	( l.loc_codigo in :Local )
and
	( p.tip_prod_codigo in (1, 3) )
and 
--	( p.rub_codigo in nvl (:Rubro, p.rub_codigo) )
	( trim (upper (':Rubro')) = 'NULL' or p.rub_codigo in :Rubro )
and 
	( l.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  p.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  fvpu.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.loc_descripcion , 
	l.loc_codigo 		, 
	l.emp_codigo 		, 
	f.ano_nombre 		, 
	f.mes_nombre_corto 	, 
	f.dia_del_mes 		,
	p.prod_codigo 		, 
	p.prod_descripcion 	, 
	p.codigo_barras 	, 
	p.rub_descripcion 	, 
	p.fam_descripcion 	, 
	p.sub_descripcion 	, 
 	p.tip_prod_descripcion, 
	c.comb_descripcion 	, 
	r.rub_cod_empresa
	