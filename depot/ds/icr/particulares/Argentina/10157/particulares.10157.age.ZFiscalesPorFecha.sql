select 
	l.codigo 		"Cod.Local (I)", 
	l.loc_cod_empresa 	"Cod.Local", 
	l.descripcion 		"Local", 
	c.codigo 		"Cod.Caja", 
	c.descripcion 		"Caja", 
	frv.registradora 	"Registradora", 
	frv.fecha_comercial 	"Fecha Comercial", 
	frv.nro_cierre 		"Nro.Cierre", 
	frv.nro_linea 		"Nro.Linea", 
	frv.nro_ope_desde 	"Nro.Desde", 
	frv.nro_ope_hasta 	"Nro.Hasta", 
	frv.tipo_de_documento 	"Tipo de Documento", 
	frv.tipo_de_factura 	"Tipo de Factura", 
	frv.categoria 		"Categor�a", 
	frv.categoria_provincial 	"Categoria Provincial", 
	frv.nombre_factura 	"Nombre Factura", 
	frv.ruc_factura 	"CUIT", 
	frv.estado 		"Estado", 
	sum (frv.percepcion_iva) 	"Percepcion IVA", 
	sum (frv.importe_ingresos_brutos) 	"Ingresos Brutos", 
	sum (frv.importe_retencion) 	"Importe Retencion", 
	sum (frv.importe_alicuota_iva) 	"Alicuota IVA", 
	nvl (sum (frv.iva_1), 0) +
		nvl (sum (frv.iva_2), 0) +
		nvl (sum (frv.iva_3), 0) +
		nvl (sum (frv.iva_4), 0) +
		nvl (sum (frv.iva_5), 0) +
		nvl (sum (frv.neto_iva_1), 0) +
		nvl (sum (frv.neto_iva_2), 0) +
		nvl (sum (frv.neto_iva_3), 0) +
		nvl (sum (frv.neto_iva_4), 0) +
		nvl (sum (frv.neto_iva_5), 0) +
		nvl (sum (frv.neto_iva_0), 0) +
		nvl (sum (frv.importe_imp_internos), 0) 	"Total", 
	sum(frv.importe_imp_internos) 	"Impuestos Internos", 
	sum(frv.neto_iva_0) 	"Neto sin IVA", 
	sum(frv.neto_iva_1) 	"Neto IVA 1", 
	sum(frv.neto_iva_2) 	"Neto IVA 2", 
	sum(frv.neto_iva_3) 	"Neto IVA 3", 
	sum(frv.neto_iva_4) 	"Neto IVA 4", 
	sum(frv.neto_iva_5) 	"Neto IVA 5", 
	sum(frv.iva_1) 		"IVA 1", 
	sum(frv.iva_2) 		"IVA 2", 
	sum(frv.iva_3) 		"IVA 3", 
	sum(frv.iva_4) 		"IVA 4", 
	sum(frv.iva_5) 		"IVA 5"
from 
	iposs.cajas c, 
	iposs.locales l, 
	iposs.fis_resumenes_Ventas frv
where 
	l.codigo = c.loc_codigo 
and 
	l.emp_codigo = frv.emp_codigo 
and 	l.codigo = frv.loc_codigo 
and 
	c.codigo = frv.caj_codigo 
and 	c.loc_codigo = frv.loc_codigo 
and
	c.loc_codigo = l.codigo 
and
	( :FechaHasta - :FechaDesde <= 31
and 	frv.fecha_comercial >= :FechaDesde 
and 	frv.fecha_comercial < :FechaHasta )
and 
	( l.codigo in :Local )
and 
	( trim (upper (':Caja')) = 'NULL' or c.codigo in :Caja )
and
	( l.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  frv.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.codigo 		, 
	l.loc_cod_empresa 	, 
	l.descripcion 		, 
	c.codigo 		, 
	c.descripcion 		, 
	frv.registradora 	, 
	frv.fecha_comercial 	, 
	frv.nro_cierre 		, 
	frv.nro_linea 		, 
	frv.nro_ope_desde 	, 
	frv.nro_ope_hasta 	, 
	frv.tipo_de_documento 	, 
	frv.tipo_de_factura 	, 
	frv.categoria 		, 
	frv.categoria_provincial, 
	frv.nombre_factura 	, 
	frv.ruc_factura 	, 
	frv.estado 		 
