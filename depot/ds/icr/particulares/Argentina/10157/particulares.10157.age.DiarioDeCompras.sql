select 
	to_char (fecha_emision,'YYYY/MM/DD') "Fec. Emision",
	razon_social "Raz�n Social",
	ruc	"CUIT",
	doc_descripcion	"Tipo Doc",
	serie "Serie",
	numero "N�mero",
	moneda "Moneda", 
	sub_total * signo	"Sub Total",
	total_descuentos * signo "Total Desc.",
	redondeo * signo "Redondeo",
	total_ingresado * signo  "Total Ingresado",
	iva_percibido * signo "IVA Percibido",
	iva_percibido_2 * signo "IVA Percibido 2",
	ing_brutos_1 * signo "Ing. Brutos 1",
	ing_brutos_2 * signo "Ing. Brutos 2",
	tasa_iva "Tasa IVA",
	sum (monto_iva * signo ) "Monto IVA",
	sum (imp_interno * signo ) "Imp. Interno",
	sum (total_linea_calculado * signo ) "Tot Linea Calc",
	sum (importe * signo ) "Importe", 
	no_gravado * signo "No Gravado",
	ret_ganancias * signo "Ret. Ganancias",
	ret_ib_bsas * signo "Ret IB BSAS",
	imp_deb_cre * signo "Imp. Deb. Cre.",
	ret_iva * signo "Ret. IVA",
	ret_suss * signo "Ret. SUSS", 
	iric * signo "Percep. Ganancias"
from
	(select 
		fecha_emision,
		prov.razon_social,
		prov.ruc,
		tdi.descripcion as doc_descripcion,
		serie,
		numero,
		mon.descripcion as moneda,
		sub_total,
		total_descuentos,
		decode (tdi.alcance, 1, 1, 2, -1, 0) Signo, 
		comp.redondeo, 
		total_ingresado,
		iva_percibido,
		iva_percibido_2,
		ing_brutos_1,
		ing_brutos_2,
		tip_doc_in_codigo,
		(select tasa 
			from impuestos_hists 
			where comp.fecha_emision between fecha and vigencia_hasta 
			and imp_codigo=cdet.iva_codigo 
			and rownum=1) as tasa_iva, 
		decode (
			serie, 
			'C', 0, 
			((select tasa 
				from impuestos_hists 
				where comp.fecha_emision between fecha and vigencia_hasta 
				and imp_codigo=cdet.iva_codigo 
				and rownum=1) * (total_linea_ingresado - imp_interno)) / 
			(100 + (select tasa 
				from impuestos_hists 
				where comp.fecha_emision between fecha and vigencia_hasta 
				and imp_codigo=cdet.iva_codigo 
				and rownum=1))) as monto_iva, 
		total_linea_calculado
			- 
			( ( (select tasa 
				from impuestos_hists 
				where comp.fecha_emision between fecha and vigencia_hasta 
				and imp_codigo=cdet.iva_codigo 
				and rownum=1) * (total_linea_ingresado - imp_interno)) / 
			(100 + (select tasa 
				from impuestos_hists 
				where comp.fecha_emision between fecha and vigencia_hasta 
				and imp_codigo=cdet.iva_codigo 
				and rownum=1))) as importe, 
		imp_interno as imp_interno,
		total_linea_calculado,
		no_gravado,
		ret_ganancias,
		ret_ib_bsas,
		imp_deb_cre,
		ret_iva,
		ret_suss,
		iric
	from
		comprobantes_detalles cdet,
		comprobantes comp,
		proveedores prov,
		tipos_documentos_inventarios tdi,
		monedas mon
	where cdet.comp_codigo = comp.codigo 
	and cdet.comp_fecha_emision = comp.fecha_emision 
	and cdet.comp_emp_codigo=comp.emp_codigo 
	and comp.prov_codigo = prov.codigo 
	and cdet.comp_tip_doc_in_codigo = tdi.codigo 
	and mon.codigo = comp.mon_codigo
	and tdi.codigo in (1, 2, 4, 7) 
	and comp.est_codigo = 1		
-- 	and nvl (fecha_contabilidad, comp.fecha_emision) between :FechaDesde and :FechaHasta + (1 - (1/24/60/60)) 
	and nvl (fecha_contabilidad, comp.fecha_emision) between :FechaDesde and :FechaHasta - (1/24/60/60)
	and comp.fecha_emision >= :FechaDesde - 365 
	and comp.loc_codigo in :Local
	and comp.emp_codigo = $EMPRESA_LOGUEADA$ 
	)
group by 
fecha_emision,
razon_social,
ruc,
doc_descripcion,
serie,
numero,
moneda, 
sub_total,
total_descuentos,
redondeo,
total_ingresado,
iva_percibido,
iva_percibido_2,
ing_brutos_1,
ing_brutos_2,
tasa_iva,
tip_doc_in_codigo,
no_gravado,
ret_ganancias,
ret_ib_bsas,
imp_deb_cre, 
ret_iva, 
ret_suss, 
iric, 
signo
