select
	l.codigo 		"Cod.Local (I)", 
	l.loc_cod_empresa	"Cod.Local", 
	l.emp_codigo 		"Cod.Empresa", 
	e.descripcion 		"Empresa", 
	l.descripcion 		"Local", 
	(select descripcion 
		from departamentos de 
		where de.codigo = l.depa_codigo)	"Departamento", 
	(select descripcion 
		from localidades lo 
		where lo.codigo = l.loca_codigo)	"Localidad", 
	r.descripcion 		"Razón social", 
	r.ruc 			"RUT/CUIT/CNPJ", 
	c.codigo 		"Cod.Caja", 
	c.descripcion 		"Caja", 
	decode (c.inactiva, 
		1, 'INACTIVA', 
		'ACTIVA') 	"Inactiva", 
	dp.fecha_disponible 	"Fecha Disponible", 
	dp.fecha_ult_consulta 	"Fecha Ult.Consulta", 
	dp.cant_consultas 	"Cant. Consultas", 
	dp.version 		"Versión", 
	dp.config		"Configuración", 
	fvd.ven_nac_importe 	"Venta Total", 
	fvd.dev_nac_importe 	"Devol Total", 
	fvd.bon_nac_importe 	"Bon Total", 
	fvd.des_nac_importe 	"Desc  Total", 
	fvd.des_prom_Nac_importe 	"Prom Total", 
	fvd.mov_cantidad 	"Cant. Tickets", 
	fvd.ven_nac_importe - 
		fvd.dev_nac_importe - 
		fvd.des_prom_nac_importe +
		fvd.redondeo_importe 	"Venta", 
	(fvd.ven_nac_importe - 
		fvd.dev_nac_importe - 
		fvd.des_prom_nac_importe +
		fvd.redondeo_importe) /
		fvd.mov_cantidad  	"Ticket Promedio"
from 
	iposs.locales l, 
	iposs.empresas e, 
	iposs.razones_sociales r, 
	iposs.cajas c, 
	iposs.distribuciones_pendientes dp, 
	(select c.loc_codigo, c.caj_codigo, c.emp_codigo, 
		nvl (sum (fvd.ven_nac_importe), 0) ven_nac_importe, 
		nvl (sum (fvd.dev_nac_importe), 0) dev_nac_importe, 
		nvl (sum (fvd.bon_nac_importe), 0) bon_nac_importe, 
		nvl (sum (fvd.des_nac_importe), 0) des_nac_importe, 
		nvl (sum (fvd.des_prom_Nac_importe), 0) des_prom_nac_importe, 
		nvl (sum (fvd.mov_cantidad), 0) mov_cantidad, 
		nvl (sum (fvd.redondeo_importe), 0) redondeo_importe
	from 
		dwm_prod.l_ubicaciones l, 
		dwm_prod.l_cajas c, 
		dwm_prod.l_productos p, 
		dwm_prod.f_ventas_diario fvd
	where
		( l.codigo = fvd.l_ubi_codigo 
	and 	  l.emp_codigo = fvd.emp_codigo )
	and 
		( c.codigo = fvd.l_caj_codigo
	and 	  c.emp_codigo = fvd.emp_codigo )
	and ( p.codigo = fvd.l_pro_codigo
	and 	p.emp_codigo = fvd.emp_codigo )
	and 	
		( fvd.fecha_comercial between :FechaDesde and :FechaHasta 
	and 	  :FechaHasta - :FechaDesde <= 31 )
	and ( nvl (:SinServicios, 0) = 0 or ( :SinServicios = 1 and p.tip_prod_codigo <> 2))
	and 
		( l.loc_descripcion = l.loc_modelo_descripcion )
	group by 
		c.loc_codigo, 
		c.caj_codigo, 
		c.emp_codigo
	) fvd
where
	c.loc_codigo = l.Codigo
and 
	r.codigo = l.raz_soc_codigo
and 	r.emp_codigo = l.emp_codigo
and 	
	dp.emp_codigo = l.emp_codigo
and 	dp.loc_codigo = l.codigo
and 	dp.caj_codigo = c.codigo
and 
	( dp.emp_codigo = fvd.emp_codigo (+)
and   dp.loc_codigo = fvd.loc_codigo (+)
and   dp.caj_codigo = fvd.caj_codigo  (+))
and 
	( trim (upper (':Local')) = 'NULL' or l.codigo in :Local )
and 
	( c.inactiva = 0)
