select 
	m.fecha_comercial 		"Fecha", 
	nvl (m.nombre_factura, 'CONSUMIDOR FINAL') 	"Nombre", 
	m.ruc_factura 		"CUIT",  
	m.tipo_de_factura 	"Tipo", 
	nvl (m.numero_operacion_fiscal, m.numero_operacion) 	"Nro.Op", 
	sum ((md.importe - md.monto_iva) * 
		case md.tip_det_codigo
			when 4 then 1
			when 6 then 1
			when 18 then 1
			else -1
		end) 				"Neto"
from 	iposs.movimientos m
join 	iposs.movimientos_detalles md
	on 		md.mov_numero_mov = m.numero_mov
	and 	md.fecha_comercial = m.fecha_comercial
	and 	md.mov_emp_codigo = m.emp_codigo
	and 	md.mov_emp_codigo = $EMPRESA_LOGUEADA$
where 	m.tipo_operacion = 'VENTA'
and 	m.emp_codigo = $EMPRESA_LOGUEADA$
and 	m.loc_codigo in :Local 
and 	m.fecha_comercial between :FechaDesde and :FechaHasta
and 	:FechaHasta - :FechaDesde <= 31
group by 
	m.fecha_comercial, 
	m.nombre_factura, 
	m.ruc_factura, 
	m.tipo_de_factura, 
	nvl (m.numero_operacion_fiscal, m.numero_operacion), 
	md.tip_det_codigo