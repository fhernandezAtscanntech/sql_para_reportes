select 
	l.loc_descripcion 	"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	f.ano_nombre 			"A�o", 
	f.mes_nombre_corto 		"Mes", 
	f.dia_del_mes 			"D�a", 
	v.fun_descripcion 		"Vendedor", 
	v.fun_tipo_vendedor 		"Tipo Vendedor", 
	c.cli_descripcion 		"Cliente", 
	nvl (sum (fvd.ven_nac_importe), 0) - 
		nvl (sum (fvd.dev_nac_importe), 0)  	"Venta", 
	nvl (sum (fvd.mov_cantidad), 0) 		"Cantidad Ticket", 
	nvl (sum (fvd.iva_importe), 0) 			"IVA"
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.l_funcionarios v, 
	dwm_prod.l_clientes c, 
	dwm_prod.l_fechas f, 
	dwm_prod.f_ventas_local_cli_diario_min fvd
where	v.codigo = fvd.l_fun_codigo_vendedor 
and 	v.emp_codigo in (1, fvd.emp_codigo)
and 
		c.codigo = fvd.l_cli_codigo
and 	c.emp_codigo in (1, fvd.emp_codigo)
and 
		l.codigo = fvd.l_ubi_codigo 
and 	l.emp_codigo = fvd.emp_codigo 
and
	f.codigo = fvd.fecha_comercial 
and
	fvd.fecha_comercial between :FechaDesde and :FechaHasta
and
	( trim (upper (':Local')) = 'NULL' or l.loc_codigo in :Local )
and
	( l.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  v.emp_codigo in (1, $EMPRESA_LOGUEADA$)
and 	  c.emp_codigo in (1, $EMPRESA_LOGUEADA$)
and 	  fvd.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.loc_descripcion, 
	l.loc_codigo, 
	l.emp_codigo, 
	f.ano_nombre, 
	f.mes_nombre_corto, 
	f.dia_del_mes, 
	v.fun_descripcion, 
	v.fun_tipo_vendedor, 
	c.cli_descripcion
