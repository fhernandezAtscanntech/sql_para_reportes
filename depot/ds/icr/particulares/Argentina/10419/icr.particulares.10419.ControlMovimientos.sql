select 
	l.codigo 		"Loc.Codigo", 
	l.emp_codigo 		"Emp.Codigo", 
	l.descripcion 		"Local", 
	m.caj_codigo 		"Caja", 
	m.fecha_coemrcial 	"Fecha Comercial", 
	m.fecha_operacion 	"Fecha Operaci�n", 
	m.numero_operacion 	"N�mero Operaci�n", 
	m.tipo_operacion 	"Tipo Operaci�n", 
	m.numero_operacion_fiscal 	"N�mero Operaci�n Fiscal", 
	m.total 		"Total"
from 
	movimientos m, 
	locales l
where
	m.loc_codigo = l.codigo
and 
	m.fecha_comercial between :FechaDesde and :FechaHasta 
and 	
	:Fecha_hasta - :FechaDesde <= 7
and
	m.emp_codigo = $EMPRESA_LOGUEADA$
and 
	m.loc_codigo = :Local
