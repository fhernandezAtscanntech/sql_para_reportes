select
	l.emp_codigo 		"Cod.Empresa", 
	l.codigo 			"Cod.Local (I)", 
	l.loc_cod_empresa 	"Sucursal", 
	l.descripcion 		"Local", 
	c.cli_cod_externo	"Cod.Cliente", 
	c.nombre 			"Cliente", 
	c.numero_doc 		"N�mero Documento", 
	c.dir_calle 		"Calle Dir.", 
	c.dir_numero 		"N�mero Dir.", 
	c.dir_apto 			"Apto Dir.", 
	c.telefono 			 "Tel�fono", 
	(select g.descripcion
		from grupos_De_clientes g
		where g.codigo = c.gru_cli_codigo) 	"Grupo", 
	lp.descripcion 		"Lista Precio Venta", 
	c.desc_rango_desde 	"Dto x Cant Desde", 
	c.desc_rango_hasta 	"Dto x Cant Hasta", 
-- 	c.nacimiento 		"Fecha Nacimiento", hay un problema con la exportaci�n de fechas viejas.
	to_char(c.nacimiento, 'dd/mm/yyyy') 	"Fecha Nacimiento", 
	c.e_mail 			"E-Mail", 
	case c.estado
		when 0 then 'INACTIVO'
		when 1 then 'ACTIVO'
		when 2 then 'MOROSO'
		when 3 then 'CAPTURA'
		else 'NO IDENTIFICADO ('||to_char(c.estado)||')'
	end 				"Estado Cliente", 
	c.audit_date 		"Fecha Alta Cli", 
	c.modif_date 		"Fecha ult.modif. Cli", 
	c.fecha_borrado 	"Fecha Borrado (emp)", 
	cl.fecha_borrado 	"Fecha Borrado (loc)", 
	cr.descripcion 		"Cr�dito", 
	cc.numero_tarjeta 	"Numero Tarjeta CC", 
	cc.audit_date 		"Fecha Alta CC", 
	cc.observaciones 	"Observaciones CC", 
	case cc.estado
		when 0 then 'MOROSO'
		when 1 then 'ACTIVA'
		when 2 then 'BLOQUEADO'
		when 3 then 'INACTIVO'
		else 'NO IDENTIFICADO ('||to_char(cc.estado)||')'
	end 				"Estado CC", 
	(select max(nvl (mcc.mov_fecha_comercial, mcc.fecha)) 
		from iposs.movimientos_cc mcc
		where mcc.cue_cor_mon_codigo = cc.mon_codigo
		and mcc.cue_cor_cre_codigo = cc.cre_codigo
		and mcc.cue_cor_cli_codigo = cc.cli_codigo
		and mcc.cue_cor_numero_tarjeta = cc.numero_tarjeta
		and mcc.tmcc_codigo= 20
		and nvl (mcc.mov_fecha_comercial, mcc.fecha) >= trunc(sysdate-365)) 		"�ltima compra"
from 
	iposs.clientes c, 
	iposs.clientes_locales cl, 
	iposs.creditos cr, 
	iposs.creditos_empresas cre, 
	iposs.locales l, 
	iposs.listas_de_precios_ventas lp, 
	iposs.cuentas_corrientes cc
where 	c.codigo = cl.cli_codigo
and 	l.codigo = cl.loc_codigo
and 
		cc.cli_codigo = c.codigo
and 	cc.emp_codigo = c.emp_codigo
and 	cr.codigo = cre.cre_codigo
and 	cc.cre_codigo = cre.cre_codigo
and 	cc.mon_codigo = cre.mon_codigo
and 	lp.codigo (+)= c.lis_pre_ve_codigo
-- and 	c.fecha_borrado is null
-- and 	cl.fecha_borrado is null
and 	l.codigo in :Local
and 	cr.codigo in :Credito
and		c.emp_codigo = $EMPRESA_LOGUEADA$
and 	l.emp_codigo = $EMPRESA_LOGUEADA$
and 	cc.emp_codigo = $EMPRESA_LOGUEADA$
and 	cre.emp_codigo = $EMPRESA_LOGUEADA$