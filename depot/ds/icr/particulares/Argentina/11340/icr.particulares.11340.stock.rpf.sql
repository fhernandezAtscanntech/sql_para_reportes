select 
	ae.art_codigo 	"Art.Codigo(I)", 
	ae.art_codigo_externo 	"Art.Codigo", 
	ae.descripcion 	"Articulo", 
	r.descripcion 	"Rubro", 
	f.descripcion 	"Familia", 
	(select cb.codigo_barra 
		from iposs.codigos_barras cb 
		where cb.art_codigo = ae.art_codigo 
		and (cb.emp_Codigo = ae.emp_codigo or (cb.emp_codigo = 1 and ae.veo_barras_modelo = 1)) 
		and rownum = 1) "Codigo Barra", 
	(select p.razon_social
		from iposs.proveedores p
		join iposs.proveedores_empresas pe on pe.prov_codigo = p.codigo
		join iposs.proveedores_articulos pa on pa.prov_codigo = pe.prov_codigo and pa.emp_codigo = pe.emp_codigo
		where :Prov is not null
		and 	pe.prov_codigo = :Prov
		and 	pe.emp_codigo = ae.emp_codigo
		and 	pa.art_codigo = ae.art_codigo) "Proveedor", 
	d.codigo 	"Cod.Deposito", 
	d.descripcion 	"Deposito", 
	l.codigo 	"Loc.Codigo(I)", 
	l.loc_Cod_empresa 	"Loc.Codigo", 
	l.descripcion 	"Local", 
	sum(s.stock) 	"Stock"
from 
	iposs.articulos_empresas ae
	join iposs.articulos_locales al on ae.art_codigo = al.art_codigo and ae.emp_codigo = al.emp_codigo and al.dim_con_numero = 0
	join iposs.locales l on l.codigo = al.loc_codigo
	join iposs.rubros r on ae.rub_codigo = r.codigo
	join iposs.stocks s on al.art_codigo = s.art_codigo and al.loc_codigo = s.dep_loc_codigo and s.dim_con_numero = 0
	join iposs.depositos d on s.dep_loc_codigo = d.loc_codigo and s.dep_codigo = d.codigo
	left join iposs.familias f on f.codigo = ae.fam_codigo
where 
	( ae.emp_codigo <> 1 ) and
	( ae.est_codigo <> 2 ) and 
	( al.fecha_borrado is null or ( al.fecha_borrado is not null and s.stock <> 0 )) and
	( l.codigo in :Local ) and 
	( trim (upper (':Rubro')) = 'NULL' or r.codigo in :Rubro ) and 
	( trim (upper (':Familia')) = 'NULL' or f.codigo = :Familia ) and 
	( :FechaStock between s.fecha and s.vigencia_hasta ) and
	( :Prov is null 
		or 
		ae.art_codigo in 
			(select pa.art_codigo
			from iposs.proveedores_articulos pa
			where pa.emp_codigo = ae.emp_codigo
			and pa.prov_codigo = :Prov)
	) and 
	ae.emp_codigo = $EMPRESA_LOGUEADA$ and
	al.emp_codigo = $EMPRESA_LOGUEADA$ and
	l.emp_codigo = $EMPRESA_LOGUEADA$
group by 
	ae.art_codigo, 
	ae.art_codigo_externo, 
	ae.descripcion, 
	ae.emp_codigo, 
	ae.veo_barras_modelo, 
	r.descripcion, 
	f.descripcion, 
	d.codigo, 
	d.descripcion, 
	l.codigo, 
	l.loc_Cod_empresa, 
	l.descripcion 
