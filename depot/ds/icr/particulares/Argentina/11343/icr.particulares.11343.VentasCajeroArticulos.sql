select 
	l.loc_descripcion "Local", 
	fc.fun_descripcion 	"Cajero", 
	c.cli_cod_empresa 	"Cli.Codigo", 
	c.cli_descripcion 	"Cli.Nombre", 
	nvl (sum (v.ven_nac_importe), 0) - nvl (sum (v.dev_nac_importe), 0) - nvl (sum (v.bon_nac_importe), 0) "Importe", 
	nvl (sum (v.mov_cantidad), 0) "Mov.Cantidad"
from 
	dwm_prod.F_VENTAS_DIARIO v, 
	dwm_prod.l_funcionarios fc, 
	dwm_prod.l_clientes c, 
	dwm_prod.l_ubicaciones l
where 
	v.l_fun_codigo_cajero = fc.codigo 
	and v.l_cli_codigo = c.codigo 
	and v.emp_codigo = c.emp_codigo 
	and v.l_ubi_codigo = l.codigo
	and l.loc_codigo in :Local
	and v.fecha_comercial = :Fecha
group by  
	l.loc_descripcion, 
	fc.fun_descripcion, 
	c.cli_cod_empresa, 
	c.cli_descripcion
