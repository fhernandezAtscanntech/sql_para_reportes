select 
	mcc.comprobante 	"Comprobante", 
	cr.descripcion 		"Cr�dito", 
	mcc.fecha 			"Fecha", 
	cl.nombre 			"Cliente", 
	cc.numero_tarjeta 	"Tarjeta", 
	md.linea 			"L�nea", 
	nvl (md.descripcion, md.descripcion_rubro) 	"Producto", 
	nvl (md.art_codigo, 'Rubro - '||md.rub_codigo) 	"C�d. Producto", 
	case 
		when md.tip_det_codigo in (6, 7, 17, 18) then md.importe
		when md.tip_det_codigo in (4, 5) then
			(select p.precio
			from precios_de_articulos_ventas p, articulos_locales al
			where 	al.art_codigo = p.art_codigo
			and 	al.loc_codigo = p.loc_lis_Loc_Codigo
			and 	al.emp_codigo = p.emp_codigo
			and 	p.emp_codigo = md.mov_emp_codigo
			and 	al.art_codigo_Externo = md.art_codigo
			and 	al.loc_Codigo = md.loc_codigo
			and 	al.emp_codigo = md.mov_emp_codigo
-- 			and 	p.loc_lis_lis_pre_ve_codigo = k_param.f_param ('LIS_PRE_VE_DEF')
			and 	p.loc_lis_lis_pre_ve_codigo = NVL (
														(select to_number(pe.valor) 
														from iposs.parametros_empresas pe 
														where pe.par_codigo = 'LIS_PRE_VE_DEF'
														and pe.emp_codigo = $EMPRESA_LOGUEADA$), 
														(select lp.codigo
														from iposs.listas_de_precios_ventas lp
														where lp.emp_codigo = $EMPRESA_LOGUEADA$
														and lp.lis_pre_ve_cod_empresa = 1))
			and 	p.vigencia_hasta > sysdate) * md.cantidad
		else 0
	end * 
	case 
		when md.tip_det_codigo in (4, 6, 17) then 1
		else -1
	end 			"Importe", 
	md.cantidad * 
	case 
		when md.tip_det_codigo in (4, 6, 17) then 1
		else -1
	end 			"Cantidad"
from 
	iposs.clientes cl, 
	iposs.creditos cr, 
	iposs.cuentas_corrientes cc, 
	iposs.movimientos m, 
	iposs.movimientos_Detalles md, 
	iposs.movimientos_cc mcc
where 	
	cl.codigo = cc.cli_codigo
and 
	cr.codigo = cc.cre_codigo
and 
	cc.numero_tarjeta = mcc.cue_cor_numero_tarjeta 
AND cc.cli_codigo = mcc.cue_cor_cli_codigo 
AND cc.cre_codigo = mcc.cue_cor_Cre_codigo 
AND cc.mon_codigo = mcc.cue_cor_mon_codigo 
and 
	m.fecha_comercial = mcc.mov_fecha_comercial 
AND m.emp_codigo = mcc.mov_emp_codigo 
AND m.numero_mov = mcc.mov_numero_mov
and 
	m.fecha_comercial = md.fecha_comercial 
AND m.emp_codigo = md.mov_emp_codigo 
AND m.numero_mov = md.mov_numero_mov
and 
	cl.codigo in (:CodigoCliente)
and 
	mcc.fecha BETWEEN :FechaDesde AND :FechaHasta + 1 - (1/24/60/60)
and
	cl.codigo in 
		(select cllo.cli_codigo	
		from iposs.clientes_locales cllo
		where cllo.loc_codigo in (:Local))
and 
	cl.emp_codigo = $EMPRESA_LOGUEADA$
and cc.emp_codigo = $EMPRESA_LOGUEADA$
and m.emp_codigo = $EMPRESA_LOGUEADA$
and mcc.mov_emp_codigo = $EMPRESA_LOGUEADA$
