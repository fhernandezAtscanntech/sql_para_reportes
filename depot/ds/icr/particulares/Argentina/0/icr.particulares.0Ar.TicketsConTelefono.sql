select 
	l.codigo 			"Cod.Local", 
	l.loc_cod_empresa 	"Cod.Sucursal", 
	l.emp_codigo 		"Cod.Empresa", 
	e.descripcion 		"Empresa", 
	l.descripcion 		"Local", 
	m.fecha_comercial	"Fecha", 
	m.telefono 			"Tel�fono", 
	m.total 			"Total", 
	m.numero_operacion 	"Nro.Operacion"
from 
	iposs.movimientos m, 
	iposs.locales l, 
	iposs.empresas e
where 	l.emp_codigo = e.codigo
and 	m.emp_codigo = e.codigo
and 	m.emp_codigo = l.emp_codigo
and 	m.loc_codigo = l.codigo
and 	m.fecha_comercial between :FechaDesde and :FechaHasta
and 	:FechaHasta - :FechaDesde <= 31
and 	m.telefono is not null

	
	
/*

lo que estar�an necesitando ser�a un reporte que les diga, para un per�odo de tiempo dado, una lista de locales y, para cada uno de ellos, 
los tel�fonos ingresados, n�mero de ticket y total del ticket ?

Algo del estilo

Local 1
    telf. xxxxxxxxx
        ticket nnnnn $120
        ticket mmmm $ 90
        ticket oooooo $ 210
    telf. yyyyyyyyy
        ticket pppppp $30
    telf. zzzzzzzzzz
:
:
Local 2
:
:
:
*/
