select 
	l.loc_modelo_descripcion 			"Local", 
	l.loc_codigo 					"Cod.Local", 
	l.emp_codigo 					"Emp.Codigo", 
	(select distinct f.ano_nombre 
		from dwm_prod.l_fechas f 
		where f.mes_codigo = fvpu.mes_codigo) 	"A�o", 
	(select distinct f.mes_nombre_corto 
		from dwm_prod.l_fechas f 
		where f.mes_codigo = fvpu.mes_codigo) 	"Mes", 
	p.prod_codigo 					"C�digo", 
	p.prod_descripcion 				"Art�culo", 
	p.codigo_barras 				"C�digo Barras", 
	p.rub_descripcion 				"Rubro", 
	p.tip_prod_descripcion 				"Tipo producto", 
	(select /*+ INDEX(p PRE_ART_VEN_VIG_I) */ distinct p.precio
		from precios_de_articulos_ventas p
		where p.art_codigo = p.prod_cod_interno
		and   p.dim_con_numero = 0
		and   p.loc_lis_lis_pre_ve_codigo = nvl ((select valor from parametros_empresas pe where pe.emp_codigo = l.emp_codigo and par_codigo = 'LIS_PRE_VE_DEF'), (select codigo from listas_de_precios_ventas where emp_codigo = l.emp_codigo and lis_pre_ve_cod_empresa = 1))
		and   p.loc_lis_loc_codigo = l.loc_codigo
		and   p.emp_codigo = l.emp_codigo
		and   (select distinct f.mes_fecha_fin - f.mes_time_span - 1 from dwm_prod.l_fechas f where f.mes_codigo = fvpu.mes_codigo) between p.vigencia  AND p.vigencia_hasta
		and   p.vigencia_hasta >= (select distinct f.mes_fecha_fin - f.mes_time_span - 1 from dwm_prod.l_fechas f where f.mes_codigo = fvpu.mes_codigo)
	)  									"Precio venta", 
	sum (nvl (fvpu.ven_cantidad, 0)) - sum (nvl (fvpu.dev_cantidad, 0)) 	"Unidades"
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.l_productos p, 
	dwm_prod.f_ventas_pu_mensual fvpu
where
	( l.codigo = fvpu.l_ubi_codigo 
and 	l.emp_codigo = fvpu.emp_codigo ) 
and
	( p.codigo = fvpu.l_pro_codigo 
and 	  p.emp_codigo = fvpu.emp_codigo )
and
	( fvpu.mes_codigo = :Mes )
and
	( l.loc_codigo in :Local )
and 
	( trim (upper (':Rubro')) = 'NULL' or p.rub_codigo in :Rubro )
and
	( p.tip_prod_codigo in (1, 3) )
and 
	( sysdate > 				-- no dejo sacar el mes actual
		( select distinct f.mes_fecha_fin 	
		from dwm_prod.l_fechas f 
		where f.mes_codigo = fvpu.mes_codigo ) )
and 
	( p.fecha_prim_mov < 			-- solo art�culos que se vendieron por primera vez antes del mes que consulto
		( select distinct f.mes_fecha_fin - f.mes_time_span - 1 from 
		dwm_prod.l_fechas f
		where f.mes_codigo = fvpu.mes_codigo ) )
and 
	( l.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  p.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  fvpu.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.loc_modelo_descripcion , 
	l.loc_codigo 		, 
	l.emp_codigo 		, 
	fvpu.mes_codigo 	, 
	p.prod_cod_interno 	, 
	p.prod_codigo 		, 
	p.prod_descripcion 	, 
	p.rub_descripcion 	, 
	p.codigo_barras 	, 
 	p.tip_prod_descripcion 
