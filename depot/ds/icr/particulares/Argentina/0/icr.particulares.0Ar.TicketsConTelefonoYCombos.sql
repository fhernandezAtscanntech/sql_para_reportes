select 
	l.codigo 			"Cod.Local", 
	l.loc_cod_empresa 	"Cod.Sucursal", 
	l.emp_codigo 		"Cod.Empresa", 
	e.descripcion 		"Empresa", 
	l.descripcion 		"Local", 
	m.fecha_comercial	"Fecha", 
	m.telefono 			"Tel�fono", 
	m.total 			"Total", 
	m.numero_operacion 	"Nro.Operacion", 
	sum (decode (md.tip_det_codigo, 4, md.cantidad, 5, -md.cantidad)) 	"Cantidad"
from 
	iposs.movimientos m, 
	iposs.movimientos_detalles md, 
	iposs.locales l, 
	iposs.empresas e
where 	m.numero_mov = md.mov_numero_mov
and 	m.emp_codigo = md.mov_emp_codigo
and 	m.fecha_comercial = md.fecha_comercial
and 	l.emp_codigo = e.codigo
and 	m.emp_codigo = e.codigo
and 	m.emp_codigo = l.emp_codigo
and 	m.loc_codigo = l.codigo
and 	md.combo_padre is not null
and 	md.tip_det_codigo in (4, 5)
and 	m.fecha_comercial between :FechaDesde and :FechaHasta
and 	:FechaHasta - :FechaDesde <= 31
and 	(:Combo is null or md.art_codigo = :Combo)
and 	m.telefono is not null
group by 
	l.codigo 			,
	l.loc_cod_empresa 	, 
	l.emp_codigo 		,
	e.descripcion 		,
	l.descripcion 		,
	m.fecha_comercial	,
	m.telefono 			,
	m.total 			,
	m.numero_operacion 	 
