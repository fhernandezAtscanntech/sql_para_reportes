select 
	e.codigo 		"Cod.Empresa", 
	e.descripcion 	"Empresa", 
	l.codigo 		"Cod.Local", 
	l.loc_cod_empresa 	"Nro.Sucursal", 
	l.descripcion 	"Local", 
	m.caj_codigo 	"Caja", 
	m.fecha_comercial 	"Fecha", 
	m.numero_operacion 	"Nro.Operacion", 
	m.numero_operacion_fiscal 	"Nro.Op. Fiscal", 
	m.total 		"Total"
from 
	iposs.locales l, 
	iposs.empresas e, 
	iposs.movimientos m
where 	m.loc_codigo = l.codigo
and 	m.emp_codigo = l.emp_codigo
and 	l.emp_codigo = e.codigo
and 	m.fecha_comercial = :Fecha
and 	nvl (l.coord_x, l.codigo) in :Local
