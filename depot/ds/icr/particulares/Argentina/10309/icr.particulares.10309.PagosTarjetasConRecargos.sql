select 
	l.codigo 		"Loc.Codigo", 
	l.emp_codigo 		"Emp.Codigo", 
	l.descripcion 		"Local", 
	c.descripcion 		"Cr�dito", 
	mp.cuotas 		"Cuotas", 
	count (*) 		"Cant.Credito", 
	sum (mp.importe) 	"Importe c/Rec", 
	(select sum (mb.importe) * -1
		from iposs.movimientos_de_bonificaciones mb
		where mb.mov_Numero_mov = mp.mov_numero_mov
		and mb.mov_emp_codigo = mp.mov_emp_codigo
		and mb.fecha_comercial = mp.fecha_comercial
		and mb.bon_codigo = 254
		and mb.importe < 0) "Recargo", 
	sum (mp.importe) - 
		(select sum (mb.importe) * -1
		from iposs.movimientos_de_bonificaciones mb
		where mb.mov_Numero_mov = mp.mov_numero_mov
		and mb.mov_emp_codigo = mp.mov_emp_codigo
		and mb.fecha_comercial = mp.fecha_comercial
		and mb.bon_codigo = 254
		and mb.importe < 0) "Importe s/rec", 
	(select sum (mb.importe) * -1
		from iposs.movimientos_de_bonificaciones mb
		where mb.mov_Numero_mov = mp.mov_numero_mov
		and mb.mov_emp_codigo = mp.mov_emp_codigo
		and mb.fecha_comercial = mp.fecha_comercial
		and mb.bon_codigo = 254
		and mb.importe < 0) * 100 / (sum (mp.importe) - 
			(select sum (mb.importe) * -1
			from iposs.movimientos_de_bonificaciones mb
			where mb.mov_Numero_mov = mp.mov_numero_mov
			and mb.mov_emp_codigo = mp.mov_emp_codigo
			and mb.fecha_comercial = mp.fecha_comercial
			and mb.bon_codigo = 254
			and mb.importe < 0)) "% Recargo"
from 
	movimientos_de_pagos mp, 
	creditos c, 
	creditos_empresas ce, 
	locales l
where
	( mp.loc_codigo = l.codigo )
and
	( mp.cre_codigo = ce.cre_cod_externo )
and
	( ce.cre_codigo = c.codigo
and 	  ce.emp_codigo = mp.mov_emp_codigo )
and 
	( mp.tip_pag_codigo = 10 
and 	  mp.cuotas > 1 
and 	  mp.mov_emp_codigo = $EMPRESA_LOGUEADA$
and 	  mp.fecha_comercial between :FechaDesde and :FechaHasta 
and 	  mp.loc_codigo = :Local)
group by 
	l.codigo 		, 
	l.emp_codigo 		, 
	l.descripcion 		, 
	c.descripcion 		, 
	mp.cuotas 		,
	mp.mov_numero_mov 	,
	mp.mov_emp_codigo 	, 
	mp.fecha_comercial 

-- CREDITO CANT. CUPONES   S/RECECARGO     % RECARGO       RECARGO $ TOTAL CON RECARGO
-- VISA               10      $ 100,00     10%               $ 10,00          $ 110,00


