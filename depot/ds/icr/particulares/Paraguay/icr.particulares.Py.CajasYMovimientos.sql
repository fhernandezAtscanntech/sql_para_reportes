select
	l.codigo 		"Cod.Local (I)", 
	l.loc_cod_empresa	"Cod.Local", 
	l.emp_codigo 		"Cod.Empresa", 
	e.descripcion 		"Empresa", 
	l.descripcion 		"Local", 
	(select descripcion from departamentos de where de.codigo = l.depa_codigo)	"Departamento", 
	(select descripcion from localidades lo where lo.codigo = l.loca_codigo)	"Localidad", 
	r.descripcion 		"Raz�n social", 
	r.ruc 			"RUT/CUIT/CNPJ", 
	c.codigo 		"Cod.Caja", 
	c.descripcion 		"Caja", 
	decode (c.inactiva, 1, 'INACTIVA', 'ACTIVA') 	"Inactiva", 
	(select nvl (sum (fvd.mov_cantidad), 0)
		from 
			dwm_prod.l_cajas caj, 
			dwm_prod.f_ventas_diario fvd
		where ( fvd.l_caj_codigo = caj.codigo )
		and ( fvd.emp_codigo = e.codigo )
		and ( caj.caj_codigo = c.codigo 
		and   caj.loc_codigo in 
			(select l2.codigo 
			from iposs.locales l2 
			where l2.emp_codigo = l.emp_codigo 
			and l.codigo = nvl (l2.coord_x, l2.codigo)))
		and ( fvd.fecha_comercial between :FechaDesde and :FechaHasta and :FechaHasta - :FechaDesde <= 31 )
		) "Cant. Tickets", 
	(select nvl (sum (fvd.ven_nac_importe), 0) - nvl (sum (dev_nac_importe), 0) + nvl (sum (redondeo_importe), 0)
		from 
			dwm_prod.l_cajas caj, 
			dwm_prod.f_ventas_diario fvd
		where ( fvd.l_caj_codigo = caj.codigo )
		and ( fvd.emp_codigo = e.codigo )
		and ( caj.caj_codigo = c.codigo 
		and   caj.loc_codigo in 
			(select l2.codigo 
			from iposs.locales l2 
			where l2.emp_codigo = l.emp_codigo 
			and l.codigo = nvl (l2.coord_x, l2.codigo)))
		and ( fvd.fecha_comercial between :FechaDesde and :FechaHasta and :FechaHasta - :FechaDesde <= 31 )
		)	"Venta"
from 
	iposs.locales l, 
	iposs.empresas e, 
	iposs.razones_sociales r, 
	iposs.cajas c
where
	c.loc_codigo = l.Codigo
and r.codigo = l.raz_soc_codigo
and r.emp_codigo = l.emp_codigo
and e.codigo = l.emp_codigo
and l.coord_x is null
and ( trim (upper (':Local')) = 'NULL' or l.codigo in :Local )
