create or replace 
function iposs.resumenDeEntrada_calculoCST (
	p_csosn 	in iposs.comprobantes_det_impuestos_br.csosn%type,
	p_cst 		in iposs.comprobantes_det_impuestos_br.cst%type)
return number
is
	cstRet 	iposs.comprobantes_det_impuestos_br.cst%type;
begin
	if p_csosn is not null
	then
		if p_csosn in (101, 102, 103, 300, 400)
		then
			cstRet := 41;
		else
			if p_csosn in (201, 202, 203, 500)
			then
				cstRet := 60;
			elses
				cstRet := 90;
			end if;
		end if;
	else
		if p_cst in (10, 30, 70)
		then
			cstRet := 60;
		else
			cstRet := p_cst;
		end if;
	end if;
	return cstRet;
end;
/

grant execute on iposs.resumenDeEntrada_calculoCST to iposs_rep
/

