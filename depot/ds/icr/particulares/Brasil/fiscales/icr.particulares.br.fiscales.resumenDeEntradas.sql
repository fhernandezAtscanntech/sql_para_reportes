select 
	l.loc_cod_empresa 	"Loj.Codigo", 
	l.descripcion 	"Loja", 
	trunc (c.fecha_entrega)	"Data Entrada", 
	'NF'			"Especie", 
	'55'			"Modelo", 
	c.serie			"Serie", 
	c.numero		"N�mero", 
	trunc (c.fecha_emision)	"Data Emiss�o", 
	nvl ( 
		(select per.nome 
		from 	personas_br per 
		where 	per.per_codigo = pe.per_codigo), 
		p.razon_social)	"Raz�o Social", 
	(select per.insc_estadual 
		from 	personas_br per 
		where 	per.per_codigo = pe.per_codigo)	"Estadual", 
	(select per.cnpj 
		from 	personas_br per 
		where 	per.per_codigo = pe.per_codigo)	"CNPJ", 
	(select descripcion 
		from 	departamentos 
		where 	codigo = pe.depa_codigo)	"Estado", 
	cdb.cfop			"CFOP", 
-- 	cdbi.cst			"CST", 
	iposs.resumenDeEntrada_calculoCST (cdbi.csosn, cdbi.cst) 	"CST", 
	sum (cd.total_linea_ingresado) 	"Valor Contabil", 
	sum (cdbi.v_bc)		"Base de Calculo", 
	cdbi.p_icms			"Aliquota", 
	sum (cdbi.v_icms)		"Imposto", 
	decode 
-- 		(cdbi.cst, 
		(iposs.resumenDeEntrada_calculoCST (cdbi.csosn, cdbi.cst), 
		40, (nvl (sum (cd.total_linea_ingresado), 0) - 
			nvl (sum (cdbi.v_bc), 0) - 
			nvl (sum (cdbi.v_ipi), 0) - 
			nvl (sum (cdbi.v_icmsst), 0)), 
		20, (nvl (sum (cd.total_linea_ingresado), 0) -  
			nvl (sum (cdbi.v_bc), 0) - 
			nvl (sum (cdbi.v_ipi), 0) - 
			nvl (sum (cdbi.v_icmsst), 0)), 
		0, 0, 
		(nvl (sum (cd.total_linea_ingresado), 0) - 
			nvl (sum (cdbi.v_ipi), 0) - 
			nvl (sum (cdbi.v_icmsst), 0)))		"Isentas/NT", 
	nvl (sum (cd.total_linea_ingresado), 0) - 
		nvl (sum (cdbi.v_bc), 0) - 
		nvl (sum (cdbi.v_ipi), 0) - 
		nvl (sum (cdbi.v_icmsst), 0) - 
		decode 
-- 			(cdbi.cst, 
			(iposs.resumenDeEntrada_calculoCST (cdbi.csosn, cdbi.cst), 
			40, (nvl (sum (cd.total_linea_ingresado), 0) - 
				nvl (sum (cdbi.v_bc), 0) - 
				nvl (sum (cdbi.v_ipi), 0) - 
				nvl (sum (cdbi.v_icmsst), 0)), 
			20, (nvl (sum (cd.total_linea_ingresado), 0) - 
				nvl (sum (cdbi.v_bc), 0) - 
				nvl (sum (cdbi.v_ipi), 0) - 
				nvl (sum (cdbi.v_icmsst), 0)), 
			0, 0, 
			(nvl (sum (cd.total_linea_ingresado), 0) - 
				nvl (sum (cdbi.v_ipi), 0) - 
				nvl (sum (cdbi.v_icmsst), 0)))		"Outras", 
	sum (cdbi.v_ipi)	"IPI", 
	sum (cdbi.v_bcst)	"BASE ST", 
	sum (cdbi.v_icmsst)	"ST", 
	0			"Fronteira" 
from 
	proveedores p, 
	proveedores_empresas pe, 
	comprobantes c, 
	locales l, 
	comprobantes_detalles cd, 
	comprobantes_Detalles_br cdb, 
	comprobantes_det_impuestos_br cdbi 
where 
	pe.prov_codigo = p.codigo 
and 
	( c.fecha_emision = cd.comp_fecha_emision 
and 	c.emp_codigo = cd.comp_emp_codigo 
and 	c.codigo = cd.comp_codigo) 
and 
	( c.fecha_emision = cdb.comp_fecha_emision 
and 	c.emp_codigo = cdb.comp_emp_codigo 
and 	c.codigo = cdb.comp_codigo ) 
and 
	( c.fecha_emision = cdbi.comp_fecha_emision 
and 	c.emp_codigo = cdbi.comp_emp_codigo 
and 	c.codigo = cdbi.comp_codigo ) 
and 
	( cdb.linea = cdbi.linea 
and 	cdb.linea = cd.linea ) 
and 
	c.prov_codigo = pe.prov_codigo 
and 	c.emp_codigo = pe.emp_codigo 
and 
	c.loc_codigo = l.codigo 
and 
	l.codigo in :Local 
and 
-- 	c.fecha_emision between :FechaDesde and to_date(:FechaHasta) + (1 - (1/24/60/60)) 
	c.fecha_entrega between :FechaDesde and to_date(:FechaHasta) + (1 - (1/24/60/60)) 
and 
	pe.emp_codigo = $EMPRESA_LOGUEADA$ 
group by 
	l.loc_cod_empresa 	, 
	l.descripcion 	, 
	trunc (c.fecha_emision)	, 
	trunc (c.fecha_entrega)	, 
	c.serie	, 
	c.numero	, 
	pe.prov_codigo	, 
	p.razon_social	, 
	pe.emp_codigo	, 
	pe.depa_codigo	, 
	pe.per_codigo 	, 
	cdb.cfop	, 
	cdbi.cst	, 
	cdbi.p_icms 	,
	cdbi.csosn
