select
	l.codigo 		"Cod.Local (I)", 
	l.loc_cod_empresa	"Cod.Local", 
	l.descripcion 		"Local", 
	lis.descripcion 	"Lista de precios", 
	al.art_codigo_Externo 	"Cod.Art�culo", 
	ae.descripcion 		"Art�culo", 
	ae.descripcion_corta	"Art�culo (corto)", 
	r.rub_Cod_empresa 	"Cod.Rubro", 
	r.descripcion 		"Rubro", 
	decode (ae.exportable, 
		1, 'SI', 
		'NO') 		"Exportable",
	decode (ae.venta_fraccionada, 
		1, 'SI', 
		'NO') 		"Venta Fraccionada", 
	aeb.med_ven_br_codigo 		"Medida", 
	ae.audit_date 		"Fecha Creaci�n", 
	ae.modif_date 		"Fecha Modificaci�n", 
	(select f.fam_cod_empresa
		from familias f
		where f.emp_codigo = ae.emp_codigo
		and f.codigo = ae.fam_codigo) 	"Cod.Familia", 
	(select f.descripcion
		from familias f
		where f.emp_codigo = ae.emp_codigo
		and f.codigo = ae.fam_codigo) 	"Familia", 
-- 	m.descripcion 		"Medida", 
	ae.unidades_compra 		"Unidades Compra", 	
	ae.dias_vencimiento 	"D�as Vencimiento", 
	nvl (
		(select cb.codigo_barra
		from codigos_barras cb
		where cb.art_codigo = ae.art_codigo
		and (cb.emp_codigo = ae.emp_codigo or (cb.emp_codigo = 1 and ae.veo_barras_modelo = 1))
		and rownum = 1), '') 	"Cod.Barra", 
	aeb.ncm			"NCM", 
	aeb.tipo_trib_salida 	"Tipo Tributacao", 
	(select i.descripcion
		from iposs.ivas i
		where i.codigo = ae.iva_codigo) 	"ICMS Venda", 
	sit_pis.cst_compra 		"CST PIS Compra", 
	sit_pis.alicuota_compra 	"Porc. PIS Compra", 
	sit_cofins.cst_compra 		"CST COFINS Compra", 
	sit_cofins.alicuota_compra 	"Porc. COFINS Compra", 
	sit_pis.cst_venta 		"CST PIS Venda", 
	sit_pis.alicuota_venta 		"Porc. PIS Venda", 
	sit_cofins.cst_venta 		"CST COFINS Venda", 
	sit_cofins.alicuota_venta 	"Porc. COFINS Venda", 
	mo.simbolo 		"Moneda", 
	pv.precio 		"Precio Venta", 
	pv.vigencia 		"Vigencia", 
	(select pc.costo_imp
		from precios_de_costos_articulos pc
		where pc.art_codigo = al.art_codigo
		and pc.loc_codigo = al.loc_codigo
		and pc.emp_codigo = al.emp_codigo
		and pc.vigencia_hasta > sysdate) 	"Precio Costo"
from 
	iposs.locales l, 
	iposs.monedas mo, 
	iposs.articulos_locales al, 
	iposs.articulos_empresas ae, 
	iposs.articulos_empresas_br aeb, 
	iposs.sit_trib_piscofins_br sit_pis, 
	iposs.sit_trib_piscofins_br sit_cofins, 
	iposs.rubros r, 
-- 	iposs.medidas m, 
	iposs.listas_de_precios_ventas lis, 
	iposs.precios_de_articulos_ventas pv	
where 
	ae.art_codigo = al.art_Codigo
and 	ae.emp_codigo = al.emp_codigo
and 
	aeb.art_codigo = al.art_codigo
and 	aeb.emp_codigo = al.emp_codigo
and
	aeb.stpc_pis_codigo = sit_pis.codigo
and 	aeb.stpc_cofins_codigo = sit_cofins.codigo
and 
	al.loc_Codigo = l.codigo
and
	ae.rub_codigo = r.codigo
-- and 
-- 	ae.med_codigo = m.codigo
and
	al.art_codigo = pv.art_codigo
and 	al.loc_Codigo = pv.loc_lis_loc_codigo
and 
	lis.codigo = pv.loc_lis_lis_pre_ve_codigo
and 
	mo.codigo = pv.mon_codigo
and 
	sysdate between pv.vigencia and pv.vigencia_hasta
and 
	al.fecha_borrado is null
and 
	( l.codigo in :Local )
and
	( trim (upper (':Rubro')) = 'NULL' or r.codigo in :Rubro )
and 	
	( ae.emp_codigo  = $EMPRESA_LOGUEADA$
and 	  al.emp_codigo  = $EMPRESA_LOGUEADA$
and 	  l.emp_codigo   = $EMPRESA_LOGUEADA$
and 	  lis.emp_codigo = $EMPRESA_LOGUEADA$
and 	  pv.emp_codigo  = $EMPRESA_LOGUEADA$
and 	  r.emp_codigo IN ($EMPRESA_LOGUEADA$, $EMPRESA_MODELO$) )
