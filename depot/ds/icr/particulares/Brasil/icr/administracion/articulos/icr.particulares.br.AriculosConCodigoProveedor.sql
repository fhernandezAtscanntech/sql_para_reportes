select 		
	pe.prov_codigo	"Cod.Fornecedor (I)", 
	pe.prov_cod_externo 	"Cod.Fornecedor", 
	per.nome	"Fornecedor", 
	per.cnpj	"CNPJ", 
	per.insc_estadual	"Ins. Estadual", 
	pa.codigo_art_proveedor	"Cod. Produto forn.", 
	ae.descripcion 	"Produto", 
	r.descripcion 	"Categoria", 
	cb.codigo_barra 	"EAN"
from		
	proveedores_empresas pe, 	
	personas_br per, 	
	proveedores_articulos pa, 	
	articulos_empresas ae, 	
	rubros r, 	
	codigos_barras cb	
where	
	pe.per_codigo = per.per_codigo	
and 	
	pe.emp_codigo = pa.emp_codigo	
and pe.prov_codigo = pa.prov_codigo	
and 	
	pa.art_codigo = ae.art_codigo	
and pa.emp_codigo = ae.emp_codigo	
and 	
	ae.rub_codigo = r.codigo	
and 	
	ae.art_codigo = cb.art_codigo	
and cb.emp_codigo in (1, ae.emp_codigo)	
and 	
	( trim(upper(':Proveedor')) = 'NULL' or pe.prov_codigo in :Proveedor)
and 
	pa.codigo_art_proveedor is not null	
and ae.fecha_borrado is null	
and pa.fecha_borrado is null	
and pe.fecha_borrado is null	
and 
	pe.emp_codigo = $EMPRESA_LOGUEADA$
and pa.emp_codigo = $EMPRESA_LOGUEADA$
and ae.emp_codigo = $EMPRESA_LOGUEADA$
