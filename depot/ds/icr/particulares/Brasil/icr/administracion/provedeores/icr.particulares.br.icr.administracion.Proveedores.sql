select
	pe.prov_Cod_Externo 	"Cod.Proveedor", 
	p.razon_social		"Raz�n Social", 
	p.ruc 			"CUIT/RUT", 
	p.direccion 		"Direcci�n", 
	p.telefono 		"Tel�fono", 
	(select descripcion from iposs.departamentos where codigo = p.depa_codigo) 	"Estado", 
	(select descripcion from iposs.localidades where codigo = p.loca_codigo) 	"Localidad", 
	decode (pe.incluye_iva, 1, 'SI', 'NO') 	"Incluye IVA", 
	pe.nro_cuenta 		"N�mero Cuenta", 
	m.codigo_ibge 	"IBGE" , 
	m.nombre 		"Municipio"
from 
	iposs.proveedores p, 
	iposs.proveedores_empresas pe, 
	iposs.municipios_br m, 
	iposs.direcciones_br d, 
	iposs.personas_br pb
where 
	p.codigo = pe.prov_codigo
and 	
	m.codigo(+) = d.mun_codigo
and
	d.codigo = pb.dir_br_codigo
and
	pb.per_codigo = pe.per_codigo
and 
	pe.fecha_borrado is null
and 
	p.codigo in 
		(select pl.prov_codigo
		from iposs.proveedores_locales pl, #LocalesVisibles# lv
		where pl.loc_codigo = lv.loc_codigo
		and pl.fecha_borrado is null)
and
	pe.emp_codigo = $EMPRESA_LOGUEADA$
