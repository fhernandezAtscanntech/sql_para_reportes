select
	c.cli_cod_externo	"Cod.Cliente", 
	p.nombre_1||' '||p.nombre_2||' '||p.apellido_1||' '||p.apellido_2 	"Cliente", 
	c.numero_doc 		"N�mero Documento", 
	c.dir_calle 		"Calle Dir.", 
	c.dir_numero 		"N�mero Dir.", 
	c.dir_apto 		"Apto Dir.", 
	c.telefono 		"Tel�fono", 
	(select g.descripcion
		from iposs.grupos_De_clientes g
		where g.codigo = c.gru_cli_codigo) 	"Grupo", 
	(select lp.descripcion 	
		from iposs.listas_de_precios_ventas lp
		where lp.codigo = c.lis_pre_ve_codigo)	"Lista Precio Venta", 
	c.desc_rango_desde 	"Dto x Cant Desde", 
	c.desc_rango_hasta 	"Dto x Cant Hasta", 
	(select lo.descripcion from iposs.localidades lo where lo.codigo = c.loca_codigo_dir) "Ciudad", 
	(select de.descripcion from iposs.departamentos de where de.codigo = c.depa_codigo_dir) "Estado", 
	(select m.codigo_ibge 	
		from iposs.municipios_br m, iposs.direcciones_br d, iposs.personas_br pb
		where d.codigo = pb.dir_br_codigo
		and m.codigo = d.mun_codigo
		and pb.per_codigo = c.per_codigo) 		"IBGE" , 
	(select m.nombre 		
		from iposs.municipios_br m, iposs.direcciones_br d, iposs.personas_br pb
		where d.codigo = pb.dir_br_codigo
		and m.codigo = d.mun_codigo
		and pb.per_codigo = c.per_codigo) "Municipio", 
	(select cc.tope 		
		from iposs.cre_cuentas_corrientes cc, iposs.cre_clientes ccl
		where cc.cli_codigo = ccl.codigo
		and ccl.per_codigo = c.per_codigo
		and cc.cre_sel_codigo = ccl.cre_sel_codigo)		"Tope CC", 
	(select cc.saldo
		from iposs.cre_cuentas_corrientes cc, iposs.cre_clientes ccl
		where cc.cli_codigo = ccl.codigo
		and ccl.per_codigo = c.per_codigo
		and cc.cre_sel_codigo = ccl.cre_sel_codigo)		"Saldo CC"
from 
	iposs.clientes c, 
	iposs.personas p
where 
	p.codigo = c.per_codigo
and 
	c.fecha_borrado is null
and 
	c.codigo in 
		(select cl.cli_codigo
		from iposs.clientes_locales cl, #LocalesVisibles# lv
		where cl.loc_codigo = lv.loc_codigo
		and cl.fecha_borrado is null)
and
	c.emp_codigo = $EMPRESA_LOGUEADA$
