/* Original:
-------- Mensaje original --------
Asunto: Re: Para verificar inserción de mail
De: Soledad Fernandez <sfernandez@scanntech.com>
Para: Mateo Perez <maperez@scanntech.com>, Fredy Hernandez <fhernandez@scanntech.com>
CC: Enerys Mesa <emesa@scanntech.com>, Cecilia Apa <capa@scanntech.com>, Maria Perez <mperez@scanntech.com>
Fecha: 10/04/2015 11:16 a.m.
> Copio a Fredy para hacer el reporte. Fredy: la promo esta empieza el lunes en br.
>
> Fredy: solo para ejecutar desde la empresa modelo, entre 2 fechas mostrar los datos de:
>
> empresa, local, caja, fecha, hora, numero_operacion_fiscal, total ticket,  campo , valor
> donde campo y valor sales de movimientos_datos_asociados y los demas campos de movimientos.

*/

select 
	e.codigo 			"Codigo Empresa", 
	e.descripcion 		"Empresa", 
	m.loc_codigo 		"Codigo Local", 
	l.descripcion 		"Local", 
	m.caj_codigo 		"Codigo Caja", 
	c.descripcion 		"Caja", 
	m.fecha_comercial 	"Fecha", 
	m.hora_operacion 	"Hora", 
	m.numero_operacion 	"Numero Operacion", 
	m.numero_operacion_fiscal 	"Numero Operacion Fiscal", 
	m.total 			"Total", 
	mda.linea 			"Linea Asociada", 
	mda.campo 			"Campo", 
	mda.valor 			"Valor"
from
	movimientos m,
	movimientos_datos_asociados mda,
	empresas e, 
	locales l,
	cajas c
where
	m.numero_mov = mda.mov_numero_mov
and 	m.emp_codigo = mda.mov_emp_codigo
and 	m.fecha_comercial = mda.fecha_comercial
and
	m.emp_codigo = e.codigo
and 
	m.loc_codigo = l.codigo
and
	m.caj_codigo = c.codigo
and 	m.loc_Codigo = c.loc_Codigo
and
	:FechaHasta - :FechaDesde <= 31
and
	m.fecha_comercial between :FechaDesde and :FechaHasta
and	
	( trim (upper (':Local')) = 'NULL' or l.codigo in :Local )
and 
	exists 
		(select 1
		from iposs.funcionarios
		where codigo = $CODIGO_FUNCIONARIO_LOGUEADO$
		and emp_codigo = 1
		and login like 'SC%') 
