/*

-------- Mensaje original --------
Asunto: Para controlar la promo Master
De: Soledad Fernandez <sfernandez@scanntech.com>
Para: Fredy Hernandez <fhernandez@scanntech.com>
CC: Mateo P�rez <maperez@scanntech.com>, Mar�a P�rez <mperez@scanntech.com>, Enerys Mesa <emesa@scanntech.com>, Gustavo Leites <gleites@scanntech.com>
Fecha: 04/05/2015 10:50 a.m.
> necesitamos un reporte que diga, por local, para un rango de fechas:
>
> cantidad de pagos contado, credito, debito y cantidad de pagos Master, la meta y el valor actual de cumplimiento de la meta.
>
> Gustavo: pasale a Fredy como contas los pagos Master asi el los cuenta igual.
>
> Saludos

Gustavo pas�:

SELECT SUM(sumMov.cantidad_pagos) as cantidadPagos 
FROM sum_movimientos_de_pagos sumMov 
WHERE fecha_comercial >= :FECHA_DESDE 
AND fecha_comercial <= :FECHA_HASTA 
AND mov_emp_codigo = :EMP_CODIGO 
AND loc_codigo = :LOC_CODIGO 
AND caj_codigo IN(:CAJAS) 
AND (
	(cre_codigo = 0 AND 
		(nombre_tarjeta = 'MAESTROCP' OR nombre_tarjeta = 'MASTERCARD DEBIT' OR nombre_tarjeta = 'Master Maestro' OR nombre_tarjeta = 'MASTERCARD')
	) OR cre_CODIGO = :CRE_CODIGO)

	
Por otro lado, apareci� una tabla prom_metas:	

04/06/2015
Me piden mostrar los pagos de Master a cr�dito separados de los de d�bito. Consulto con Soledad y me confirma que puedo mostrar por un lado las l�neas tipo 10 
para los pagos a cr�edito y las 13 para los de d�bito


22/06/2015
Se quejan que no aparecen los d�bitos de Master: me fijo y hay nuevos tipos de cr�ditos para los d�bitos: en particular el 20001 (int. 56) es MAESTRO
Adem�s, algunos vienen con espacios a la derecha y otros no

24/06/2015
Me dicen que no tome en cuenta las transacciones offline. Para poder incluir el filtro por off_line, tengo que achicar la consulta de alguna forma 
porque estoy en el l�mite

*/



select 
	s.mov_emp_codigo 	"Cod.Empresa", 
	e.descripcion 	"Empresa", 
	s.loc_codigo 	"Cod.Local", 
	l.descripcion 	"Local", 
	s.caj_codigo	"Caja", 
	s.fecha_comercial 	"Fecha", 
	iposs.rep_sum_pagos (s.fecha_comercial, s.mov_emp_codigo, s.loc_codigo, s.caj_codigo,  9, 0) "Cantidad Pagos Efectivo", 
	iposs.rep_sum_pagos (s.fecha_comercial, s.mov_emp_codigo, s.loc_codigo, s.caj_codigo, 10, 1) "Cantidad Pagos Cr�dito", 
	iposs.rep_sum_pagos (s.fecha_comercial, s.mov_emp_codigo, s.loc_codigo, s.caj_codigo, 11, 0) "Cantidad Pagos Cheques", 
	iposs.rep_sum_pagos (s.fecha_comercial, s.mov_emp_codigo, s.loc_codigo, s.caj_codigo, 12, 0) "Cantidad Pagos Vales", 
	iposs.rep_sum_pagos (s.fecha_comercial, s.mov_emp_codigo, s.loc_codigo, s.caj_codigo, 13, 1) "Cantidad Pagos D�bito", 
	nvl ((select sum (s2.cantidad_pagos) 
		from iposs.sum_movimientos_de_pagos s2
		where s2.fecha_comercial = s.fecha_comercial
		and s2.mov_emp_codigo = s.mov_emp_codigo
		and s2.loc_codigo = s.loc_codigo
		and s2.caj_codigo = s.caj_codigo
		and s2.tip_pag_codigo in (10, 13)
		and s2.off_line = 0
-- 		and ((s2.cre_codigo = 0 and (upper(trim(s2.nombre_tarjeta)) in ('MAESTRO', 'MAESTROCP', 'MASTERCARD DEBIT', 'MASTER MAESTRO', 'MASTERCARD')))
 		and ((s2.cre_codigo = 0 and (upper(trim(s2.nombre_tarjeta)) like 'MAS%' OR upper(trim(s2.nombre_tarjeta)) like 'MAE%'))
			or
			s2.cre_codigo in (select cl.cre_codigo_externo from creditos_locales cl where cl.loc_codigo = s2.loc_codigo and cl.cre_codigo in (38, 56)))), 0)
			"Cantidad Pagos Master", 
	nvl ((select sum (s2.cantidad_pagos) 
		from iposs.sum_movimientos_de_pagos s2
		where s2.fecha_comercial = s.fecha_comercial
		and s2.mov_emp_codigo = s.mov_emp_codigo
		and s2.loc_codigo = s.loc_codigo
		and s2.caj_codigo = s.caj_codigo
		and s2.tip_pag_codigo in (10)
		and s2.off_line = 0
-- 		and ((s2.cre_codigo = 0 and (upper(trim(s2.nombre_tarjeta)) in ('MAESTRO', 'MAESTROCP', 'MASTERCARD DEBIT', 'Master Maestro', 'MASTERCARD')))
 		and ((s2.cre_codigo = 0 and (upper(trim(s2.nombre_tarjeta)) like 'MAS%' OR upper(trim(s2.nombre_tarjeta)) like 'MAE%'))
			or
			s2.cre_codigo = (select cl.cre_codigo_externo from creditos_locales cl where cl.loc_codigo = s2.loc_codigo and cl.cre_codigo = 38))), 0)
			"Cantidad Pagos Cr�dito Master", 
	nvl ((select sum (s2.cantidad_pagos) 
		from iposs.sum_movimientos_de_pagos s2
		where s2.fecha_comercial = s.fecha_comercial
		and s2.mov_emp_codigo = s.mov_emp_codigo
		and s2.loc_codigo = s.loc_codigo
		and s2.caj_codigo = s.caj_codigo
		and s2.tip_pag_codigo in (13)
		and s2.off_line = 0
-- 		and ((s2.cre_codigo = 0 and (upper(trim(s2.nombre_tarjeta)) in ('MAESTRO', 'MAESTROCP', 'MASTERCARD DEBIT', 'Master Maestro', 'MASTERCARD')))
 		and ((s2.cre_codigo = 0 and (upper(trim(s2.nombre_tarjeta)) like 'MAS%' OR upper(trim(s2.nombre_tarjeta)) like 'MAE%'))
			or
			s2.cre_codigo in  (select cl.cre_codigo_externo from creditos_locales cl where cl.loc_codigo = s2.loc_codigo and cl.cre_codigo in (38, 56)))), 0)
			"Cantidad Pagos D�bito Master", 
	nvl ((select m.cantidad
		from iposs.prom_metas m
		where m.emp_codigo = s.mov_emp_codigo
		and m.loc_codigo = s.loc_codigo), 0) 		"Meta Master"
from 
	iposs.sum_movimientos_de_pagos s, 
	iposs.locales l, 
	iposs.empresas e, 
	iposs.tipos_de_pagos t
where
	s.mov_emp_codigo = e.codigo 
and 	s.loc_codigo = l.codigo
and 	s.tip_pag_codigo = t.codigo
and 
	s.mov_emp_codigo = l.emp_codigo
and 	(upper (trim (':Local')) = 'NULL' or s.loc_codigo in (:Local))
and  	(upper (trim (':Caja')) = 'NULL' or s.caj_codigo in (:Caja))
and 	s.fecha_comercial between :FechaDesde and :FechaHasta
and 	:FechaHasta - :FechaDesde <= 31
group by 
	s.mov_emp_codigo, 
	e.descripcion, 
	s.loc_codigo, 
	l.descripcion, 
	s.caj_codigo, 
	s.fecha_comercial


	
create or replace function iposs.rep_sum_pagos (
	p_fecha_comercial date, 
	p_emp_codigo 		iposs.empresas.codigo%type, 
	p_loc_codigo 		iposs.locales.codigo%type, 
	p_caj_codigo 		iposs.cajas.codigo%type, 
	p_tipo_pago			iposs.tipos_de_pagos.codigo%type, 
	p_solo_on_line 		number)
return number
is 
	v_ret 	number;
begin
	begin
		select 	sum (cantidad_pagos)
		into 	v_ret
		from 	iposs.sum_movimientos_de_pagos
		where 	fecha_comercial = p_fecha_comercial
		and 	mov_emp_codigo = p_emp_codigo
		and 	loc_codigo = p_loc_codigo
		and 	caj_codigo = p_caj_codigo
		and 	tip_pag_codigo = p_tipo_pago
		and 	(nvl (off_line, 0) = 0 or p_solo_on_line = 0);
	exception
	when no_data_found then
		v_ret := 0;
	end;
	
	return v_ret;
end;
/

grant execute on iposs.rep_sum_pagos to iposs_rep;

