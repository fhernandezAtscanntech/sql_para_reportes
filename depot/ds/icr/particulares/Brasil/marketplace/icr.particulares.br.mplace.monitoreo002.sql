select
	tipo_promocion, 
	id, 
	codigo_interno_be, 
	descripcion, 
	estado, 
	estado_be, 
	estereotipo, 
	fecha_creacion, 
	fecha_borrado, 
-- 	img, 
-- 	img_url, 
	titulo, 
	vigencia_desde, 
	vigencia_hasta, 
	id_externo, 
	id_autor, 
	id_campana, 
	id_parametros_promocion, 
	terminos, 
	fecha_aceptacion_limite
from
 iposs_mp.promociones
where
 vigencia_hasta > sysdate
and 
  fecha_borrado is null
and estado_be not in ('DISTRIBUIDA', 'PENDIENTE')
and id not in (116, 117, 118, 119)