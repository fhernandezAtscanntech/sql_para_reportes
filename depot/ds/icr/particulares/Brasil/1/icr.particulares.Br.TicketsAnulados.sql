select 
	l.descripcion 		"Local", 
	l.codigo 			"Cod.Local", 
	l.emp_codigo 		"Emp.Codigo", 
	e.descripcion 		"Empresa", 
	c.codigo 			"Cod.Caja", 
	c.descripcion 		"Caja", 
	m.fecha_comercial 	"Fecha", 
	m.observacion 		"Observación", 
	count(*) 			"Cantidad"
from 
	iposs.locales l, 
	iposs.empresas e, 
	iposs.cajas c, 
	IPOSS.movimientos m
where
		( l.codigo = m.loc_codigo 
and 	  l.emp_codigo = m.emp_codigo )
and 
		( c.codigo = m.caj_codigo
and 	  c.loc_codigo = m.loc_codigo )
and 	( e.codigo = l.emp_codigo 
and 	  e.codigo = m.emp_codigo )
and 
		( m.tipo_operacion = 'VENTA ANULADA' )
and  
		( m.fecha_comercial between :FechaDesde and :FechaHasta 
and 	  :FechaHasta - :FechaDesde <= 31 )
and 
	exists 
		(select 1
		from iposs.funcionarios
		where codigo = $CODIGO_FUNCIONARIO_LOGUEADO$
		and emp_codigo = 1
		and login like 'SC%') 
group by 
	l.descripcion 		, 
	l.codigo 			, 
	l.emp_codigo 		, 
	e.descripcion 		, 
	c.codigo 			, 
	c.descripcion 		, 
	m.fecha_comercial 	, 
	m.observacion 		
