select 
	cr.emp_codigo,
	cr.loc_codigo,
	cr.caj_codigo,
	crd.fecha,
	crd.version,
	crd.free_space
from
	iposs.cajas_reportadas cr,
	(select
		caj_rep_codigo,
		version,
		fecha as fecha, 
		decode (instr (datos_extra, 'ipos.storage.internal.free'), 
			0, -1, -- si no est�, devuelvo -1 para que se identifique de alguna forma el caso
			to_number (substr (
					datos_extra, 
					instr (datos_extra, 'ipos.storage.internal.free') + 30, -- a partir del primer n�mero despu�s del patr�n
					(instr (						-- hasta la posici�n de la siguiente coma - 3 lugares
						datos_extra, 
						',', 
						instr (
							datos_extra, 
							'ipos.storage.internal.free') + 30)) - 
						((instr (datos_extra, 'ipos.storage.internal.free') + 30) +3)))) as free_space
	from cajas_reportadas_datos) crd
WHERE
	crd.caj_rep_codigo = cr.codigo
and
	crd.fecha > sysdate - 2
and 
	crd.free_space < 200
/
