select 
	l.descripcion 		"Local", 
	l.codigo 		"Cod.Local", 
	l.emp_codigo 		"Emp.Codigo", 
	e.descripcion 		"Empresa", 
	c.codigo 		"Cod.Caja", 
	c.descripcion 		"Caja", 
	m.fecha_comercial 	"Fecha", 
	m.fecha_operacion 	"Fecha Operaci�n", 
	m.numero_operacion 	"N�mero Operaci�n", 
	m.total 		"Total", 
	m.observacion 		"Observaci�n", 
	dp.version 		"Versi�n", 
	dp.config		"Configuraci�n"
from 
	iposs.locales l, 
	iposs.empresas e, 
	iposs.cajas c, 
	iposs.distribuciones_pendientes dp, 
	iposs.movimientos m 
where
	( l.codigo = m.loc_codigo 
and 	  l.emp_codigo = m.emp_codigo )
and 
	( c.codigo = m.caj_codigo
and 	  c.loc_codigo = m.loc_codigo )
and 	( e.codigo = l.emp_codigo 
and 	  e.codigo = m.emp_codigo )
and 
	( m.loc_codigo = dp.loc_codigo 
and 	  m.caj_codigo = dp.caj_codigo )
and
	( m.tipo_operacion = 'VENTA ANULADA' )
and  
	( m.fecha_comercial between :FechaDesde and :FechaHasta 
and 	  :FechaHasta - :FechaDesde <= 31 )
and 
	exists 
		(select 1
		from iposs.funcionarios
		where codigo = $CODIGO_FUNCIONARIO_LOGUEADO$
		and emp_codigo = 1
		and login like 'SC%') 
