select 
	e.codigo 		"Cod.Empresa", 
	e.descripcion 	"Empresa", 
	l.codigo 		"Cod.Loja", 
	l.descripcion 	"Loja", 
	cr.caj_codigo 	"Caixa", 
	trunc(crd.fecha) 	"Data",
	sum (
		decode (instr(crd.datos_extra, 'reporte.caja.app.crash'), 
			0, 0, 
			TO_NUMBER (TRIM (REPLACE (REPLACE (REPLACE (SUBSTR (crd.DATOS_EXTRA, INSTR (crd.DATOS_EXTRA, 'reporte.caja.app.crash') + 24 ,6),'"'),','), CHR(10)))))) "APP_CRASH",
	sum (
		decode (instr(crd.datos_extra, 'reporte.caja.ticket.total'), 
			0, 0, 
			TO_NUMBER (TRIM (REPLACE (REPLACE (REPLACE (SUBSTR (crd.DATOS_EXTRA, INSTR (crd.DATOS_EXTRA, 'reporte.caja.ticket.total') + 27 ,6),'"'),','), CHR(10)))))) "TICKET_TOTALES",
	sum (
		decode (instr(crd.datos_extra, 'reporte.caja.ticket.cancelaciones'), 
			0, 0, 
			TO_NUMBER (TRIM (REPLACE (REPLACE (REPLACE (SUBSTR (crd.DATOS_EXTRA, INSTR (crd.DATOS_EXTRA, 'reporte.caja.ticket.cancelaciones') + 35, 6),'"'),','), CHR(10)))))) "TICKET_CANCELACIONES", 
	sum (
		decode (instr(crd.DATOS_EXTRA,'caja.mm.conexion.error'), 
			0, 0, 
			to_number (TRIM (REPLACE (REPLACE (REPLACE (SUBSTR (crd.DATOS_EXTRA, INSTR (crd.DATOS_EXTRA, 'caja.mm.conexion.error') + 25, 6), '"'), ','), Chr(10)))))) "ERRORES_MULTIMEDIA"
from 
	empresas e, 
	locales l, 
	cajas_reportadas cr, 
	cajas_reportadas_datos crd
where 
	cr.emp_codigo = e.codigo
and 
	cr.loc_codigo = l.codigo
and 	cr.emp_codigo = l.emp_codigo
and 
	crd.caj_rep_codigo = cr.codigo
and 
	crd.fecha between :FechaDesde and (:FechaHasta + 1 - (1/24/60/60))
and 	:FechaHasta - :FechaDesde <= 62
group by 
	e.codigo, 
	e.descripcion, 
	l.codigo, 
	l.descripcion, 
	cr.caj_codigo, 
	trunc(crd.fecha)
order by 
	e.codigo, 
	l.codigo, 
	cr.caj_codigo, 
	trunc(crd.fecha) desc



--	sum (
--		decode (instr(crd.DATOS_EXTRA,'caja.mm.conexion.error'), 
--			0, 0, 
--			to_number (SUBSTR (crd.DATOS_EXTRA, INSTR (crd.DATOS_EXTRA, 'caja.mm.conexion.error') + 25, (instr (crd.DATOS_EXTRA, '"', instr (crd.DATOS_EXTRA, 'caja.mm.conexion.error') + 25)) - (instr (crd.DATOS_EXTRA, 'caja.mm.conexion.error') + 25))))) "ERRORES_MULTIMEDIA"
