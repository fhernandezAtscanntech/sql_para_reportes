select 
	l.loc_modelo_descripcion 	"Local(M)", 
	l.loc_descripcion 		"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	l.emp_descripcion 		"Empresa", 
	c.caj_codigo 			"Cod.Caja", 
	c.caj_descripcion 		"Caja", 
	f.ano_nombre 			"A�o", 
	f.mes_nombre_corto 		"Mes", 
	f.dia_del_mes 			"Dia", 
	f.dia_nombre_largo 		"Dia Nombre", 
	f.dia_de_la_semana		"Nro Dia Semana", 
	nvl (sum (fvd.ven_nac_importe), 0) 	"Venta Total", 
	nvl (sum (fvd.dev_nac_importe), 0) 	"Devol Total", 
	nvl (sum (fvd.bon_nac_importe), 0) 	"Bon Total", 
	nvl (sum (fvd.des_nac_importe), 0) 	"Desc  Total", 
	nvl (sum (fvd.des_prom_Nac_importe), 0) 	"Prom Total", 
	nvl (sum (fvd.mov_cantidad), 0) 	"Cant. Tickets", 
	nvl (sum (fvd.ven_nac_importe), 0) - 
		nvl (sum (fvd.dev_nac_importe), 0)  	"Venta", 
	(nvl (sum (fvd.ven_nac_importe), 0) - 
		nvl (sum (fvd.dev_nac_importe), 0)) /
		nvl (sum (fvd.mov_cantidad), 0)  	"Ticket Promedio"
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.l_cajas c, 
	dwm_prod.l_fechas f, 
	dwm_prod.f_ventas_diario fvd
where
	( l.codigo = fvd.l_ubi_codigo 
and 	  l.emp_codigo = fvd.emp_codigo )
and 
	( c.codigo = fvd.l_caj_codigo
and 	  c.emp_codigo = fvd.emp_codigo )
and 
	( fvd.fecha_comercial = f.codigo )
and 
	( fvd.fecha_comercial between :FechaDesde and :FechaHasta 
and 	  :FechaHasta - :FechaDesde <= 31 )
and 
	exists 
		(select 1
		from iposs.funcionarios
		where codigo = $CODIGO_FUNCIONARIO_LOGUEADO$
		and emp_codigo = 1
		and login like 'SC%') 
group by 
	l.loc_modelo_descripcion, 
	l.loc_descripcion, 
	l.loc_codigo, 
	l.emp_codigo, 
	l.emp_descripcion, 
	c.caj_codigo, 
	c.caj_descripcion, 
	f.ano_nombre, 
	f.mes_nombre_corto, 
	f.dia_del_mes, 
	f.dia_nombre_largo, 
	f.dia_de_la_semana
having nvl (sum (fvd.mov_cantidad), 0) <> 0