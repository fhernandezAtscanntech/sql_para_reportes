select 
	m.emp_codigo, 
	m.loc_Codigo, 
	m.caj_codigo, 
	m.numero_operacion, 
	m.numero_operacion_fiscal, 
	m.fecha_operacion, 
	m.fecha_comercial, 
	mp.linea, 
	tp.descripcion 		"Tipo de Pago", 
	mp.importe, 
	mp.importe_pago, 
	mp.SERIE_RECIBO, 
	mp.NUMERO_RECIBO, 
	(select b.descripcion from iposs.bancos b where b.codigo = mp.BAN_CODIGO) 	"Banco", 
	(select v.descripcion from iposs.vales_de_compras v where v.codigo = mp.VAL_CODIGO) 	"Vale de compra", 
	(select f.apellido||', '||f.nombre from iposs.funcionarios f where f.codigo = mp.FUN_CODIGO) "Funcionario", 
	(select c.descripcion from iposs.creditos c, iposs.creditos_locales cl where c.codigo = cl.cre_codigo and cl.loc_codigo = m.loc_codigo and cl.cre_codigo_Externo = mp.CRE_CODIGO) 	"Financiera", 
	mp.NUMERO_DOCUMENTO, 
	mp.NUMERO_TARJETA, 
	mp.VENCIMIENTO, 
	mp.NUMERO_AUTORIZACION, 
	mp.CI, 
	mp.PLAN_DE_PAGO, 
	mp.CUOTAS, 
	mp.FECHA_CHEQUE, 
	mp.NUMERO_CHEQUE, 
	mp.TELEFONO, 
	mp.LOTE_CREDITO, 
	mp.COMERCIO_CREDITO, 
	mp.TERMINAL_CREDITO, 
	mp.OFF_LINE, 
	(select p.descripcion from iposs.promociones p where p.emp_codigo = m.emp_codigo and p.prom_cod_empresa = mp.PROM_CODIGO) 	"Promocion", 
	mp.NOMBRE_TARJETA, 
	mp.IMPORTE_PROMOCION, 
--	LOC_CODIGO
	mp.MANUAL, 
	mp.NUMERO_VALE, 
	mp.TIPO_PAGO, 
	mp.ES_DEBITO, 
	mp.NUMERO_FACTURA, 
	mp.DESCUENTO_AFAM, 
	mp.CUENTA_CHEQUE, 
	m.observacion, 
	m.audit_user, 
	m.fecha_llego_pos
from 
	iposs.movimientos m, 
	iposs.movimientos_De_pagos mp, 
	iposs.tipos_de_pagos tp
where
	m.numero_mov = mp.mov_numero_mov
and m.emp_codigo = mp.mov_emp_codigo
and m.fecha_comercial = mp.fecha_comercial
and 
	mp.tip_pag_codigo = tp.codigo
and 
	m.loc_codigo = :Local
and m.emp_codigo = (select emp_codigo from iposs.locales where codigo = :Local)
and m.fecha_comercial between :FechaDesde and :FechaHasta
and :FechaHasta - :FechaDesde <= 31
order by
	m.emp_codigo, 
	m.loc_Codigo, 
	m.caj_codigo, 
	m.fecha_operacion, 
	m.numero_operacion, 
	mp.linea