select 	
	r.descripcion 		"Raz�o Social", 
	r.ruc 			"CNPJ", 
	l.direccion 		"Endere�o", 
	(select de.descripcion 
		from departamentos de 
		where de.codigo = l.depa_codigo) 	"Estado", 
	(select lo.descripcion 
		from localidades lo 
		where lo.codigo = l.loca_codigo) 	"Localidad", 
	rz.fecha 			"Data", 
	rz.serie_ecf 			"Serie ECF", 
	rz.contador_z 			"Contador Z", 
	rz.coo_reporte_z 		"COO Z", 
	rz.totalizador_final 		"GT Final", 
	rz.totalizador_final - rz.venta_bruta_diaria 	"GT Inicial", 
	rz.venta_bruta_diaria		"Movimiento do Dia", 
	rz.cancelaciones		"Cancelamento", 
	rz.descuentos 			"Desconto", 
	rz.venta_bruta_diaria - rz.cancelaciones - rz.descuentos 	"Valor Cont�bil", 	
	rz.sustitucion_tributaria 	"Substitu��o Trib", 	
	rz.exentas 			"Isenta", 	
	rz.no_tributados 		"N�o Tributado", 	
	(select nvl (sum (rzi.base_calculo), 0) 
		from reporte_z_impuestos rzi 
		where rzi.repz_codigo = rz.codigo 
		and rzi.tasa = 7) 	"7%", 
	(select nvl (sum (rzi.base_calculo), 0) 		
		from reporte_z_impuestos rzi 
		where rzi.repz_codigo = rz.codigo 
		and rzi.tasa = 10) 	"10%", 
	(select nvl (sum (rzi.base_calculo), 0)
		from reporte_z_impuestos rzi 
		where rzi.repz_codigo = rz.codigo 
		and rzi.tasa = 12) 	"12%", 
	(select nvl (sum (rzi.base_calculo), 0) 
		from reporte_z_impuestos rzi where 
		rzi.repz_codigo = rz.codigo 
		and rzi.tasa = 17) 	"17%", 
	(select nvl (sum (rzi.base_calculo), 0)
		from reporte_z_impuestos rzi 
		where rzi.repz_codigo = rz.codigo 
		and rzi.tasa = 18) 	"18%", 
	(select nvl (sum (rzi.base_calculo), 0)
		from reporte_z_impuestos rzi 
		where rzi.repz_codigo = rz.codigo 
		and rzi.tasa = 22) 	"22%", 
	(select nvl (sum (rzi.base_calculo), 0) 
		from reporte_z_impuestos rzi 
		where rzi.repz_codigo = rz.codigo 
		and rzi.tasa = 25) 	"25%", 
	(select nvl (sum (rzi.base_calculo * (rzi.tasa / 100)), 0) 
		from reporte_z_impuestos rzi 
		where rzi.repz_codigo = rz.codigo) 	"Imposto Debitado"
from 
	reporte_z_br rz, razones_sociales r, locales l
where 
	l.codigo = rz.loc_codigo
and 
	r.codigo = l.raz_soc_codigo
and 
	l.codigo = :Local
and 
-- 	fecha between :Fecha and to_date(:Fecha)+1-1/24/60/60
	fecha between :FechaDesde and to_date(:FechaHasta)+1-1/24/60/60
and 	:FechaHasta - :FechaDesde <= 31
and 
	rz.emp_codigo = $EMPRESA_LOGUEADA$
and 	l.emp_codigo = $EMPRESA_LOGUEADA$
