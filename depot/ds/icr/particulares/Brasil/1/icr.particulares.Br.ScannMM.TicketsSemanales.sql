select 
	l.descripcion 	"Local", 
	l.codigo 		"Cod.Local", 
	l.emp_codigo 		"Emp.Codigo", 
	e.descripcion 	"Empresa", 
	i.descripcion 	"Integrador", 
	c.codigo 		"Cod.Caja", 
	c.descripcion 	"Caja", 
	m.fecha_comercial 	"Fecha", 
	count(*) 		"Cantidad", 
	sum (total)		"Total", 
	avg(total) 		"Ticket Promedio"
from 
	iposs.locales l, 
	iposs.empresas e, 
	iposs.cajas c, 
	iposs.integradores i, 
	IPOSS.movimientos m
where
	( l.codigo = m.loc_codigo 
and 	  l.emp_codigo = m.emp_codigo )
and 
	( c.codigo = m.caj_codigo
and 	  c.loc_codigo = m.loc_codigo )
and 	
	( e.codigo = l.emp_codigo 
and 	  e.codigo = m.emp_codigo )
and 
	( e.int_codigo = i.codigo(+) )
and
	( m.tipo_operacion = 'VENTA' )
and  
	( m.fecha_comercial between trunc(sysdate - 8) and trunc(sysdate - 1))
and 	
	( e.codigo between 28000 and 28998 or e.codigo = 29104 )
group by 
	l.descripcion 	, 
	l.codigo 		, 
	l.emp_codigo 		, 
	e.descripcion 	, 
	i.descripcion 	, 
	c.codigo 		, 
	c.descripcion 	, 
	m.fecha_comercial 	 
