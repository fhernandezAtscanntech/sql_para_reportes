select
	al.emp_codigo 	"Cod.Empresa",
	al.loc_codigo 	"Cod.Local",
	l.loc_cod_empresa "Sucursal",
	e.descripcion 	"Empresa",
	l.descripcion 	"Local",
	trunc (al.audit_date) 	"Fecha",
	count(*) 	"Cantidad"
from
	iposs.articulos_locales al,
	iposs.locales l,
	iposs.empresas e
where 	l.codigo = al.loc_codigo
and 	e.codigo = al.emp_codigo
and 	(upper (trim(':Empresa')) = 'NULL' or al.emp_codigo in :Empresa)
and 	(upper (trim(':Local')) = 'NULL' or al.emp_codigo in :Local)
and 	al.emp_codigo <> 1
group by
	al.emp_codigo,
	al.loc_codigo,
	l.loc_cod_empresa,
	e.descripcion,
	trunc (al.audit_date),
	l.descripcion
