/*

original

Daniel Y Fredy, buenas tardes.
 
Hoy en d�a, hacemos un reporte API en Brasil con base en otros 3 reportes existentes, que son �Cantidad Articulos API�, �Diferencia Reporte Z API_JL� y 
�Tickets por d�a mes actual� y algunas f�rmulas Excel para determinar campos espec�ficos.

(pasa una imagen marcando los reportes de /FS/Soporte "Cantidad Art�culos API", "Diferencia Reporte Z API_JL", "Tickets por d�a mes actual")

Les pido por favor, armar un nuevo reporte �nico de los locales API que se pueda mirar en el nuestro BE (y exportar XLS) , de acuerdo con el ejemplo abajo:


Consulta 1:
Cantidad Art�culos API

od: soporte.api.cantArticulos

SELECT al.EMP_CODIGO, l.LOC_COD_EMPRESA, COUNT(*) cantidad
FROM iposs.articulos_locales al, iposs.locales l
WHERE  l.CODIGO = al.loc_codigo 
GROUP BY al.EMP_CODIGO, l.LOC_COD_EMPRESA


Consulta 2:
Diferencia Reporte Z API_JL

od: soporte.api.reportezDif

SELECT RZ.EMP_CODIGO,RZ.LOC_COD_EMPRESA,
RZ.FECHA_COMERCIAL,
Sum(CTRL.MOV_VTA_BRUTA) as VENTA_MOVIMIENTOS,
Sum(CTRL.Z_VTA_BRUTA) as VENTA_Z,
Sum(CTRL.DIF_VTA_BRUTA) as DIFERENCIA,
COUNT(*) as cajas
FROM IPOSS.CTRL_RESULTADOS_ZS ctrl 
  INNER JOIN iposs.REPORTE_Z_BR rz on RZ.CODIGO =CTRL.REPZ_CODIGO 
  INNER JOIN iposs.empresas e on (e.CODIGO = rz.EMP_CODIGO)
WHERE
    rz.FECHA_COMERCIAL >= :FECHA_DESDE AND 
    rz.FECHA_COMERCIAL  <= :FECHA_HASTA 
GROUP BY RZ.EMP_CODIGO,RZ.LOC_COD_EMPRESA,rz.FECHA_COMERCIAL
order by rz.EMP_CODIGO,RZ.LOC_COD_EMPRESA,  RZ.FECHA_COMERCIAL desc


Consulta 3:
Tickets por dia mes actual

od: soporte.ticketsPorDia

select
	e.descripcion,
	e.codigo,
       l.loc_cod_empresa,
       l.descripcion local,
	i.descripcion integrador,
	m.fecha_comercial,
	count(*) Cantidad
from
	movimientos m,
	empresas e,
	integradores i,
       locales l
where 	m.fecha_comercial between trunc(sysdate-31) and trunc(sysdate-1) 
and 	m.emp_codigo = e.codigo
and 	e.int_codigo = i.codigo (+)
and 	nvl(e.tip_emp_codigo, 0) not in (4,5)
and 	e.fecha_borrado is null
and    l.codigo = m.loc_codigo
and    l.emp_codigo = m.emp_codigo
group by
	e.descripcion,
	e.codigo,
       l.loc_cod_empresa,
       l.descripcion,
	i.descripcion,
	m.fecha_comercial

	
ergo
le agrego a este �ltimo od los datos que faltan

23/06/2016
La consulta sobre movimientos es demasiado pesada, la cambio por una sumarizada nueva del DW.

*/

select
	e.descripcion 	"Empresa",
	e.codigo 		"Cod.Empresa",
    l.loc_cod_empresa 	"Cod. Sucursal", 
	(e.codigo || l.loc_cod_empresa) 	"Clave Empresa", 
    l.codigo 		"Cod.Local", 
	l.descripcion 	"Local",
	i.descripcion 	"Integrador",
	m.fecha_comercial 	"Fecha",
	(select count(*)
		from iposs.articulos_locales al
		where al.emp_codigo = e.codigo
		and al.loc_codigo = l.codigo) 	"Cantidad Art�culos API", 
	(SELECT sum (ctrl.diferencia)
		FROM iposs.CTRL_CIERRES_DIARIOS ctrl 
		WHERE
			ctrl.fecha_comercial >= trunc (sysdate - 4)
			AND ctrl.fecha_comercial <= trunc (sysdate - 1)
			AND	ctrl.loc_codigo = l.codigo
			AND ctrl.emp_codigo = e.codigo) "Diferencia Z (ult.4 d�as)", 
--	(select max(m2.fecha_comercial)
--		from iposs.movimientos m2
--		where m2.emp_codigo = e.codigo
--		and m2.loc_codigo = l.codigo
--		and m2.fecha_comercial between trunc(sysdate-31) and trunc(sysdate-1)) 		"�ltima Fecha Ticket", 
	(select max(m2.fecha_comercial)
		from dwm_prod.f_ventas_caja_cli_diario_min m2, dwm_prod.l_ubicaciones u2
		where m2.l_ubi_codigo = u2.codigo
		and m2.emp_codigo = e.codigo
		and u2.emp_codigo = e.codigo
		and u2.loc_codigo = l.codigo
		and m2.fecha_comercial between trunc(sysdate-31) and trunc(sysdate-1)) 		"�ltima Fecha Ticket", 
-- 	count(*) Cantidad
	sum (m.mov_cantidad) Cantidad
from
	dwm_prod.f_ventas_caja_cli_diario_min m,
	dwm_prod.l_ubicaciones u, 
	empresas e,
	integradores i,
	locales l
where 	m.fecha_comercial between trunc(sysdate-31) and trunc(sysdate-1) 
and 	m.emp_codigo = e.codigo
and 	e.int_codigo = i.codigo (+)
and 	nvl(e.tip_emp_codigo, 0) not in (4,5)
and 	e.fecha_borrado is null
and 	u.codigo = m.l_ubi_codigo
and 	u.loc_codigo = l.codigo
and 	u.emp_codigo = l.emp_codigo
group by
	e.descripcion,
	e.codigo,
	l.codigo, 
	l.loc_cod_empresa,
	l.descripcion,
	i.descripcion,
	m.fecha_comercial
