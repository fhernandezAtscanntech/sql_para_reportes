select 									
-- extra�do de la base de Per�, no lo ten�a guardado o no lo hice yo
	Emp_codigo, 								
	Cod_Local, 								
	des_local, 								
	Fecha, 								
-- 	case when (enPromo = 0) then 'NO' else 'SI' end EnPromo, 
	decode (enPromo, 0, 'NO', 'SI') EnPromo, 
	sum (RegistradoSKU) RegistradoSKU, 								
	sum (RegistradoInterno) RegistradoInterno, 								
	sum (RegistradoRubro) RegistradoRubro 								
from 									
	(select								
		l.emp_codigo                    			Emp_Codigo,				
		l.loc_codigo                    			Cod_Local,				
		l.loc_modelo_descripcion 			Des_Local, 				
	   	f0.codigo  			Fecha,				
		(select nvl (sum (f.ven_nac_importe), 0) - nvl (sum (f.dev_nac_importe), 0)							
			from dwm_prod.f_ventas_min f, dwm_prod.l_productos p						
			where f.l_ubi_codigo = l.codigo						
			and 	f.emp_codigo = l.emp_codigo					
			and 	f.l_pro_codigo = p.codigo					
			and 	f.emp_codigo = p.emp_codigo					
			and 	f.fecha_comercial = f0.codigo					
			and 	p.tip_prod_codigo = 1					
			and 	f_es_ean_2(p.codigo_barras) = 1) 	RegistradoSKU, 				
		(select nvl (sum (f.ven_nac_importe), 0) - nvl (sum (f.dev_nac_importe), 0)							
			from dwm_prod.f_ventas_min f, dwm_prod.l_productos p						
			where f.l_ubi_codigo = l.codigo						
			and 	f.emp_codigo = l.emp_codigo					
			and 	f.l_pro_codigo = p.codigo					
			and 	f.emp_codigo = p.emp_codigo					
			and 	f.fecha_comercial = f0.codigo					
			and 	p.tip_prod_codigo = 1					
			and 	f_es_ean_2(p.codigo_barras) = 0) 	RegistradoInterno, 				
		(select nvl (sum (f.ven_nac_importe), 0) - nvl (sum (f.dev_nac_importe), 0)							
			from dwm_prod.f_ventas_min f, dwm_prod.l_productos p						
			where f.l_ubi_codigo = l.codigo						
			and 	f.emp_codigo = l.emp_codigo					
			and 	f.l_pro_codigo = p.codigo					
			and 	f.emp_codigo = p.emp_codigo					
			and 	f.fecha_comercial = f0.codigo					
			and 	p.tip_prod_codigo = 3) 	RegistradoRubro, 
		(select count(*)			
			from iposs.prom_locales_habilitados lh, iposs.prom_vigencias pv		
			where 	lh.prom_codigo = pv.prom_codigo	
			and 	lh.emp_codigo = l.emp_codigo	
			and 	lh.loc_codigo = l.loc_codigo	
			and 	f0.codigo between pv.vigencia_desde and pv.vigencia_hasta)	EnPromo
	from								
		dwm_prod.l_ubicaciones l, 							
		dwm_prod.l_fechas f0							
	where 								
		f0.codigo between :FechaDesde and :FechaHasta
	and								
		:FechaHasta - :FechaDesde <= 15							
	and 								
		l.loc_codigo in							
			(select distinct loc_codigo						
			from iposs.cajas						
			where inactiva = 0)						
	group by								
		l.emp_codigo,							
		l.loc_codigo, 							
		l.loc_modelo_descripcion, 							
		l.codigo, 							
		f0.codigo) original							
group by 									
	Emp_Codigo, 								
	Cod_Local, 								
	des_local, 								
	EnPromo, 
	Fecha								
having									
	sum (RegistradoSKU) + sum (RegistradoInterno) + sum (RegistradoRubro) <> 0								
