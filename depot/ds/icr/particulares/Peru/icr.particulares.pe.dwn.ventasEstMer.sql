select 
	u.emp_codigo 		"Cod.Empresa", 
	u.loc_codigo		"Cod.Local", 
	u.emp_descripcion 	"Empresa", 
	u.loc_modelo_descripcion 	"Local", 
	(select distinct f.ano_nombre 
		from dwm_prod.l_fechas f 
		where f.mes_codigo = fvm.mes_codigo) 	"A�o", 
	(select distinct f.mes_nombre_corto 
		from dwm_prod.l_fechas f
		where f.mes_codigo = fvm.mes_codigo) 	"Mes", 
	p.est_mer_3_descripcion 	"Est.Mer.3", 
	p.est_mer_4_descripcion 	"Est.Mer.4", 
	p.est_mer_5_descripcion 	"Est.Mer.5", 
	nvl (sum (fvm.ven_nac_importe), 0) - nvl (sum (fvm.dev_nac_importe), 0) "Venta"
from 
	dwm_prod.f_ventas_pu_mensual fvm, 
	dwm_prod.l_productos_modelo p, 
	dwm_prod.l_ubicaciones u
where 
	fvm.l_ubi_codigo = u.codigo
and 	
	fvm.l_pro_mod_codigo = p.codigo
and 	
	fvm.mes_codigo = :Mes
and 
	exists 
		(select 1
		from iposs.funcionarios
		where codigo = $CODIGO_FUNCIONARIO_LOGUEADO$
		and emp_codigo = 1
		and fecha_borrado is null
		and login like 'SC%') 	
group by 
	u.emp_codigo, 
	u.loc_codigo, 
	u.emp_descripcion, 
	u.loc_modelo_descripcion, 
	fvm.mes_codigo, 
	p.est_mer_3_descripcion, 
	p.est_mer_4_descripcion, 
	p.est_mer_5_descripcion
