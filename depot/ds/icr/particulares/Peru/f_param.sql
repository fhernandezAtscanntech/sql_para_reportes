/* Función extraida de Uy */

create or replace function f_param
	(p_param in IPOSS.PARAMETROS.CODIGO%type,
	p_loc in IPOSS.LOCALES.CODIGO%type)
return varchar2
is
	v_valor IPOSS.PARAMETROS.VALOR%type;
	v_emp   IPOSS.EMPRESAS.CODIGO%type;
	v_pe    IPOSS.PARAMETROS.POR_EMPRESA%type;
begin
	-- averiguo si el local existe. Sino, devuelvo null
	begin
		select emp_codigo
		into v_emp
		from iposs.locales
		where codigo = p_loc;
	exception
	when no_data_found then
		return null;
	end;
	
	-- busco el parametro para el local. Si existe, lo devuelvo
	begin
		select valor
		into v_valor
		from iposs.parametros_locales
		where par_codigo = upper(p_param)
		and loc_codigo = p_loc;
		
		return v_valor;
	exception
	when no_data_found then
		v_valor := null;
	end;
	
	-- busco el parametro y verifico si el valor va por empresa. Si no existe, salgo con null
	begin
		select valor, por_empresa
		into v_valor, v_pe
		from iposs.parametros
		where codigo = upper(p_param);
	exception
	when no_data_found then
		return null;
	end;
	
	-- si el parÃ¡metro es por empresa, lo busco y sustituyo el valor. Si no estÃ¡, devuelvo el valor actual.
	if v_pe = 1
	then
		begin
			select valor
			into v_valor
			from iposs.parametros_empresas
			where par_codigo = upper(p_param)
			and emp_codigo = v_emp;
		exception
		when no_data_found then
			null;
		end;
	
	end if;
	
	return v_valor;
end f_param;
/

grant execute on f_param to iposs_fx, iposs_rep, iposs_sel;
