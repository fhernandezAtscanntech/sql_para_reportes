select
	l.codigo 		"Cod.Local (I)", 
	l.loc_cod_empresa	"Cod.Local", 
	l.descripcion 		"Local", 
	al.art_codigo_Externo 	"Cod.Art�culo", 
	ae.descripcion 		"Art�culo",
  ae.plu "PLU",
  aext.peso "Peso (gr)",
  aext.tolerancia "Tolerancia",
	(select i.descripcion 
		from iposs.ivas i 
		where i.codigo = ae.iva_codigo) "IVA Compra", 
	(select i.descripcion 
		from iposs.ivas i 
		where i.codigo = nvl (ae.iva_vta_codigo, ae.iva_codigo)) "IVA Venta",
	(select i.descripcion 
		from iposs.ivas i 
		where i.codigo = nvl( nvl (aext.iva_factura_codigo, ae.iva_vta_codigo), ae.iva_codigo)) "IVA Factura", 
	r.rub_Cod_empresa 	"Cod.Rubro", 
	r.descripcion 		"Rubro", 
	(select f.fam_cod_empresa
		from iposs.familias f
		where f.codigo = ae.fam_codigo) "Cod.Familia", 		
	(select f.descripcion 
		from iposs.familias f 
		where f.codigo = ae.fam_codigo) "Familia", 
	ae.sub_codigo  			"Cod.Sub Familia", 
	(select s.descripcion 
		from iposs.subfamilias s 
		where s.codigo = ae.sub_codigo 
		and s.fam_codigo = ae.fam_codigo) "SubFamilia", 
	nvl (
		(select cb.codigo_barra
		from codigos_barras cb
		where cb.art_codigo = ae.art_codigo
		and (cb.emp_codigo = ae.emp_codigo or (cb.emp_codigo = 1 and ae.veo_barras_modelo = 1))
		and rownum = 1), '') 	"Cod.Barra", 
	(select al2.art_codigo_Externo
		from articulos_locales al2
		where al2.loc_codigo = l.codigo
		and al2.emp_codigo = ae.emp_codigo
		and al2.art_codigo = ae.representante) 	"Representante", 
	aes.descripcion 		"Estado", 
	nvl (al.fecha_borrado, ae.fecha_borrado) 		"Fecha Borrado", 
	ae.audit_date 		"Fecha creaci�n", 
	ae.modif_date 		"�ltima modificaci�n", 
	ae.unidades_compra 	"Unidades Compra" , 
	decode (ae.bonificable, 1, 'BONIFICABLE', 'NO BONIFICABLE') 	"Bonificable",
  decode (ae.venta_fraccionada, 1, 'SI', 'NO') 	"Vta.Fraccionada",
	ae.dias_vencimiento 	"D�as Vencimiento", 
	(select b.descripcion
		from iposs.bonificaciones b
		where b.codigo = ae.bon_codigo) 	"Bonif. Autom�tica", 
	pv.vigencia 	"Vigencia PV", 
	pv.precio 		"Precio Venta"
from 
	iposs.locales l, 
	iposs.articulos_locales al, 
	iposs.articulos_empresas ae,
	iposs.articulos_empresas_ext aext,
	iposs.articulos_estados aes,
	iposs.rubros r, 
	iposs.precios_de_articulos_ventas pv
where 
	ae.art_codigo = al.art_Codigo
and 	ae.emp_codigo = al.emp_codigo
and 
	al.loc_Codigo = l.codigo
and
	ae.rub_codigo = r.codigo
and 
	ae.est_codigo = aes.art_est_cod_empresa
and al.emp_codigo = aes.emp_codigo
and
	ae.art_codigo = aext.art_codigo (+)
and
	ae.emp_codigo = aext.emp_codigo (+)
and 
	(al.art_codigo = pv.art_codigo (+)
	and al.emp_codigo = pv.emp_codigo (+)
	and al.loc_codigo = pv.loc_lis_loc_codigo (+)
	and nvl (pv.loc_lis_lis_pre_ve_codigo, 1) = nvl (iposs.f_param ('LIS_PRE_VE_DEF', pv.loc_lis_loc_codigo), 1) 
	and sysdate between nvl (pv.vigencia, sysdate) and nvl  (pv.vigencia_hasta, sysdate))
and 
	(nvl(:Borrados, 0) = 1 or (al.fecha_borrado is null and ae.fecha_borrado is null))	
and 
	( l.codigo in :Local )
and
	( trim (upper (':Rubro')) = 'NULL' or r.codigo in :Rubro )
and 	
	( ae.emp_codigo  = $EMPRESA_LOGUEADA$
and 	  nvl (aext.emp_codigo, $EMPRESA_LOGUEADA$)  = $EMPRESA_LOGUEADA$
and 	  al.emp_codigo  = $EMPRESA_LOGUEADA$
and 	  l.emp_codigo   = $EMPRESA_LOGUEADA$
and 	  r.emp_codigo IN ($EMPRESA_LOGUEADA$, $EMPRESA_MODELO$) )
