select 
	l.descripcion 	"Local", 
	m.numero_mov	"N�mero Mov", 
	m.numero_operacion	"N�mero Operaci�n", 
	c.descripcion	"Caja", 
	m.tipo_operacion	"Tipo Operaci�n", 
	m.fecha_operacion	"Fecha Operaci�n", 
	m.fecha_llego_pos	"Fecha llega POS", 
	m.numero_operacion_fiscal	"N�mero Operaci�n Fiscal", 
	m.nro_z	"Nro Z", 
	m.total	"Total", 
	tp.descripcion	"Tipo de Pago", 
	decode (nvl (mp.cre_codigo, 0), 
		0, decode (nvl (mp.val_codigo, 0), 
			0, decode (nvl (mp.ban_codigo, 0), 
				0, '', 
				(select b.descripcion from bancos b where b.codigo = mp.ban_codigo)), 
			(select v.descripcion from vales_de_compras v, vales_de_compras_locales vl where v.codigo = vl.val_codigo and vl.loc_Codigo = mp.loc_codigo and vl.val_codigo_externo = mp.val_codigo)), 
		(select c.descripcion from creditos c, creditos_locales cl where c.codigo = cl.cre_codigo and cl.loc_codigo = mp.loc_codigo and cl.cre_codigo_externo = mp.cre_codigo))	"Descripci�n Tipo de Pago", 
	mo.simbolo	"Moneda", 
	sum (mp.importe_pago)	"Importe Pago", 
	sum (mp.importe)	"Importe"
from 
	movimientos m, 
	movimientos_De_pagos mp, 
	tipos_de_pagos tp, 
	monedas mo, 
	locales l, 
	cajas c	
where
	m.numero_mov = mp.mov_numero_mov	
and 	m.fecha_comercial = mp.fecha_comercial	
and 	m.emp_codigo = mp.mov_emp_codigo	
and 
	mp.tip_pag_codigo = tp.codigo	
and 
	mp.mon_codigo = mo.codigo	
and 
	m.loc_Codigo = l.codigo
and 
	m.caj_codigo = c.codigo	
and 
	m.loc_codigo in :Local	
and 
	m.fecha_comercial = :Fecha	
and 
	m.emp_codigo = $EMPRESA_LOGUEADA$
and 	mp.mov_emp_codigo = $EMPRESA_LOGUEADA$
and 	l.emp_codigo = $EMPRESA_LOGUEADA$
group by 
	l.descripcion, 
	m.numero_mov,
	m.numero_operacion,
	c.descripcion,
	m.tipo_operacion,
	m.fecha_operacion,
	m.fecha_llego_pos,
	m.numero_operacion_fiscal,
	m.nro_z,
	m.total,
	tp.descripcion,
	mo.simbolo, 
	mp.cre_codigo, 
	mp.val_codigo, 
	mp.ban_codigo, 
	mp.loc_codigo
