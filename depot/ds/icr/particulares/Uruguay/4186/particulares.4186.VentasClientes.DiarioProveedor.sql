/*
No me gusta la resoluci�n de la consulta, pero funciona.
*/
select 
	l.loc_descripcion 	"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	f.ano_nombre 			"A�o", 
	f.mes_nombre_corto 		"Mes", 
	f.dia_del_mes 			"D�a", 
	c.cli_cod_empresa 		"Cod.Cliente", 
	c.cli_descripcion 		"Cliente", 
	c.gru_cli_cod_empresa 		"Cod.Grupo", 
	c.gru_cli_descripcion 		"Grupo de Clientes", 
	(select p.razon_social
		from 
			iposs.proveedores p, 
			iposs.proveedores_empresas pe, 
			iposs.proveedores_articulos pa
		where 	p.codigo = pe.prov_codigo
		and 	pe.prov_codigo = pa.prov_codigo
		and 	pe.emp_codigo = pa.emp_codigo
		and 	pa.art_codigo = prod.prod_cod_interno
		and 	pa.emp_codigo = prod.emp_codigo 
		and 	pa.proveedor_principal = 1
		and 	pa.fecha_borrado is null
		and 	pe.emp_codigo = $EMPRESA_LOGUEADA$ 
		and 	pa.emp_codigo = $EMPRESA_LOGUEADA$ 
	) 	"Proveedor", 
	sum (nvl (flcvd.mov_cantidad, 0)) 	"Cant. Tickets", 
	sum (nvl (flcvd.ven_cantidad, 0)) - sum (nvl (flcvd.dev_cantidad, 0)) 	"Unidades", 
	sum (nvl (flcvd.ven_nac_importe, 0)) 	"Venta Total", 
	sum (nvl (flcvd.dev_nac_importe, 0)) 	"Devol", 
	sum (nvl (flcvd.bon_nac_importe, 0)) 	"Bonif", 
	sum (nvl (flcvd.des_prom_nac_importe, 0)) 	"Prom", 
	sum (nvl (flcvd.ven_nac_importe, 0)) - 
		sum (nvl (flcvd.dev_nac_importe, 0)) -
		sum (nvl (flcvd.cos_nac_ult_impuestos, 0)) 	"Utilidad", 
	sum (nvl (flcvd.cos_nac_ult_impuestos, 0))	"Costo Ultimo", 
	sum (nvl (flcvd.iva_importe, 0)) 	"IVA", 
	sum (nvl (flcvd.ven_nac_importe, 0)) - 
		sum (nvl (flcvd.dev_nac_importe, 0))  	"Venta"
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.l_fechas f, 
	dwm_prod.l_clientes c, 
	dwm_prod.l_productos prod, 
	dwm_prod.f_ventas_local_cli_diario_min flcvd
where
	( l.codigo = flcvd.l_ubi_codigo 
and 	l.emp_codigo = flcvd.emp_codigo ) 
and
	( c.codigo = flcvd.l_cli_codigo 
and 	  c.emp_codigo = flcvd.emp_codigo )
and 
		flcvd.l_pro_codigo = prod.codigo 
and 	flcvd.emp_codigo = prod.emp_codigo
and 
	( flcvd.fecha_comercial = f.codigo )
and
	( flcvd.fecha_comercial between :FechaDesde and :FechaHasta)
and
	( :FechaHasta - :FechaDesde <= 31 )
and
	( l.loc_codigo in :Local )
and 
		( trim (upper (':Proveedor')) = 'NULL' or 
		(select p.codigo
			from 
				iposs.proveedores p, 
				iposs.proveedores_empresas pe, 
				iposs.proveedores_articulos pa
			where 	p.codigo = pe.prov_codigo
			and 	pe.prov_codigo = pa.prov_codigo
			and 	pe.emp_codigo = pa.emp_codigo
			and 	pa.art_codigo = prod.prod_cod_interno
			and 	pa.emp_codigo = prod.emp_codigo 
			and 	pa.proveedor_principal = 1
			and 	pa.fecha_borrado is null
			and 	pe.emp_codigo = $EMPRESA_LOGUEADA$ 
			and 	pa.emp_codigo = $EMPRESA_LOGUEADA$ ) in :Proveedor ) 
and 
	( l.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  c.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  flcvd.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.loc_descripcion , 
	l.loc_codigo 		, 
	l.emp_codigo 		, 
	f.ano_nombre 		, 
	f.mes_nombre_corto 	, 
	f.dia_del_mes 		,
	prod.prod_cod_interno, 
	prod.emp_codigo, 
	c.cli_cod_empresa 	, 
	c.cli_descripcion 	, 
	c.gru_cli_cod_empresa 	, 
	c.gru_cli_descripcion
