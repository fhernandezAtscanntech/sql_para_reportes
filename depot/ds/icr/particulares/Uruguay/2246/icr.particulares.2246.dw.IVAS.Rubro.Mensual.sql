select 
	l.loc_descripcion 	"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	(select distinct f.ano_nombre 
		from dwm_prod.l_fechas f 
		where f.mes_codigo = fvpu.mes_codigo) 	"A�o", 
	(select distinct f.mes_nombre_corto 
		from dwm_prod.l_fechas f 
		where f.mes_codigo = fvpu.mes_codigo) 	"Mes", 
	i.iva_descripcion 		"IVA", 
	p.rub_descripcion 		"Rubro", 
	sum (nvl (fvpu.ven_cantidad, 0)) - sum (nvl (fvpu.dev_cantidad, 0)) 	"Unidades", 
	sum (nvl (fvpu.ven_nac_importe, 0)) 	"Venta Total", 
	sum (nvl (fvpu.dev_nac_importe, 0)) 	"Devol", 
	sum (nvl (fvpu.bon_nac_importe, 0)) 	"Bonif", 
	sum (nvl (fvpu.des_prom_nac_importe, 0)) 	"Prom", 
	sum (nvl (fvpu.ven_nac_importe, 0)) - 
		sum (nvl (fvpu.dev_nac_importe, 0)) -
		(sum (nvl (fvpu.cos_nac_ult_impuestos, 0)))	"Utilidad", 
	sum (nvl (fvpu.cos_nac_ult_impuestos, 0)) 	"Costo Ultimo", 
	sum (nvl (fvpu.iva_importe, 0)) 	"Monto IVA", 
	sum (nvl (fvpu.imp_1_importe, 0)) 	"Monto Imp1", 
	sum (nvl (fvpu.imp_2_importe, 0)) 	"Monto Imp2", 
	sum (nvl (fvpu.imp_3_importe, 0)) 	"Monto Imp3", 
	sum (nvl (fvpu.imp_4_importe, 0)) 	"Monto Imp4", 
	sum (nvl (fvpu.imp_5_importe, 0)) 	"Monto Imp5", 
	sum (nvl (fVpu.ven_nac_importe, 0)) - 
		sum (nvl (fvpu.dev_nac_importe, 0))  	"Venta"
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.l_productos p, 
	dwm_prod.l_impuestos i, 
	dwm_prod.f_ventas_pu_mensual fvpu
where
	( l.codigo = fvpu.l_ubi_codigo 
and 	l.emp_codigo = fvpu.emp_codigo ) 
and
	( i.codigo = fvpu.l_imp_codigo 
and 	  i.emp_codigo = fvpu.emp_codigo )
and
	( p.codigo = fvpu.l_pro_codigo 
and 	  p.emp_codigo = fvpu.emp_codigo )
and
	( fvpu.mes_codigo in :Mes )
and
	( l.loc_codigo in :Local )
and
	( trim (upper (':Rubro')) = 'NULL' or p.rub_codigo in :Rubro )
and 
	( p.tip_prod_codigo in (1, 3) )
and 
	( l.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  p.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  i.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  fvpu.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.loc_descripcion , 
	l.loc_codigo 		, 
	l.emp_codigo 		, 
	p.rub_descripcion 	, 
	fvpu.mes_codigo 	, 
	i.iva_descripcion
 