/*

https://scanntech.atlassian.net/browse/REP-428

Se pide un reporte de facturas emitidas con medios de pago.

Se toma el reporte actual, y al origen de datos se le agregan 5 columnas m�s, son la sumarizaci�n de los pagos de cada tipo (efectivo, 
credito, debito, vale y banco).

Hay un problema, y es que las  l�neas se repiten por tipo de IVA, pero el pago no est� discriminado, por lo que se va a repetir. De todas formas, 
lo que el cliente m�s indica es que necesita saber la forma de pago y no el desglose por tipo de pago.

*/

-- Original
select 
	l.descripcion 	"Local", 
	mf.caj_codigo 	"Caja", 
	mo.simbolo 	"Moneda", 
	mf.fecha_comercial 	"Fecha", 
	tdi.descripcion 	"Tipo Documento", 
	mf.serie_factura 	"Serie", 
	mf.numero_factura 	"Numero", 
	m.nombre_factura 	"Nombre", 
	m.ruc_factura 	"RUT", 
	m.direccion_factura 	"Direcci�n", 
	m.numero_operacion 	"Numero Operaci�n", 
	i.descripcion 		"IVA", 
	mf.importe 	"Importe", 
	mf.monto_iva 	"Monto IVA", 
	(select sum (mp.importe_pago * mp.cotiza_compra) 
		from iposs.movimientos_de_pagos mp 
		where mp.mov_numero_mov = m.numero_mov
		and mp.fecha_comercial = m.fecha_comercial
		and mp.mov_emp_codigo = m.emp_codigo
		and mp.tip_pag_codigo = 9) "Efectivo", 
	(select sum (mp.importe_pago * mp.cotiza_compra) 
		from iposs.movimientos_de_pagos mp 
		where mp.mov_numero_mov = m.numero_mov
		and mp.fecha_comercial = m.fecha_comercial
		and mp.mov_emp_codigo = m.emp_codigo
		and mp.tip_pag_codigo = 10) "Cr�dito", 
	(select sum (mp.importe_pago * mp.cotiza_compra) 
		from iposs.movimientos_de_pagos mp 
		where mp.mov_numero_mov = m.numero_mov
		and mp.fecha_comercial = m.fecha_comercial
		and mp.mov_emp_codigo = m.emp_codigo
		and mp.tip_pag_codigo = 13) "D�bito", 
	(select sum (mp.importe_pago * mp.cotiza_compra) 
		from iposs.movimientos_de_pagos mp 
		where mp.mov_numero_mov = m.numero_mov
		and mp.fecha_comercial = m.fecha_comercial
		and mp.mov_emp_codigo = m.emp_codigo
		and mp.tip_pag_codigo = 12) "Vale de compra", 
	(select sum (mp.importe_pago * mp.cotiza_compra) 
		from iposs.movimientos_de_pagos mp 
		where mp.mov_numero_mov = m.numero_mov
		and mp.fecha_comercial = m.fecha_comercial
		and mp.mov_emp_codigo = m.emp_codigo
		and mp.tip_pag_codigo = 11) "Banco"
from 
	movimientos_facturas mf, 
	movimientos m, 
	locales l, 
	monedas mo, 
	ivas i, 
	tipos_documentos_inventarios tdi
where
	( mf.mov_numero_mov = m.numero_mov
and 	  mf.mov_emp_codigo = m.emp_codigo
and 	  mf.fecha_comercial = m.fecha_comercial)
and
	mf.loc_codigo = l.codigo
and
	mf.mon_codigo = mo.codigo
and
	mf.iva_codigo = i.codigo
and
	mf.tip_doc_in_codigo = tdi.codigo
and
	mf.fecha_comercial between :FechaDesde and :FechaHasta
and 	
	l.codigo in :Local
and
	( mf.mov_emp_codigo = $EMPRESA_LOGUEADA$
and 	  m.emp_codigo = $EMPRESA_LOGUEADA$
and 	  l.emp_codigo = $EMPRESA_LOGUEADA$)
