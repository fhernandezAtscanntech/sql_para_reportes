select 
	movimientos_cc.comprobante 	"Comprobante", 
	creditos.descripcion		"Cr�dito", 
	grupos_de_clientes.descripcion 	"Grupo Clientes", 
	tipos_movimientos_cc.descripcion 	"Tipo Mov.CC", 
	movimientos_cc.fecha 		"Fecha", 
	clientes.nombre			"Cliente", 
	cuentas_corrientes.numero_tarjeta 	"Numero Tarjeta", 
	sum(movimientos_cc.importe) 	"Importe"
from 
	iposs.clientes, 
	iposs.creditos creditos, 
	iposs.cuentas_corrientes cuentas_corrientes, 
	iposs.grupos_de_clientes grupos_de_clientes, 
	iposs.movimientos_cc movimientos_cc, 
	iposs.tipos_movimientos_cc tipos_movimientos_cc
where 
	( ( clientes.codigo = cuentas_corrientes.cli_codigo ) and 
	  ( creditos.codigo = cuentas_corrientes.cre_codigo ) and 
	  ( grupos_de_clientes.codigo = clientes.gru_cli_codigo ) and 
	  ( tipos_movimientos_cc.codigo = movimientos_cc.tmcc_codigo ) and 
	  ( cuentas_corrientes.numero_tarjeta = movimientos_cc.cue_cor_numero_tarjeta and 
	    cuentas_corrientes.cli_codigo = movimientos_cc.cue_cor_cli_codigo and 
	    cuentas_corrientes.cre_codigo = movimientos_cc.cue_cor_cre_codigo and 
	    cuentas_corrientes.mon_codigo = movimientos_cc.cue_cor_mon_codigo ) ) and 
	( movimientos_cc.fecha between :FechaDesde and :FechaHasta + 1 ) and
	( movimientos_cc.mov_fecha_comercial between :FechaDesde and :FechaHasta ) and
	movimientos_cc.mov_emp_codigo = 2338
	and clientes.emp_codigo = 2338
group by 
	movimientos_cc.comprobante, 
	creditos.descripcion, 
	grupos_de_clientes.descripcion, 
	tipos_movimientos_cc.descripcion, 
	movimientos_cc.fecha, 
	clientes.nombre, 
	cuentas_corrientes.numero_tarjeta
order by 
	grupos_de_clientes.descripcion asc, 
	clientes.nombre asc, 
	cuentas_corrientes.numero_tarjeta asc, 
	movimientos_cc.fecha asc
