select 
	l.loc_descripcion 	"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	f.ano_nombre 			"Año", 
	f.mes_nombre_corto 		"Mes", 
	f.dia_del_mes 			"Día", 
	p.prod_codigo 			"Codigo", 
	p.prod_descripcion 		"Articulo", 
	p.rub_descripcion 		"Rubro", 
	pr.razon_social 		"Proveedor", 
	(select sum (nvl (fvpu.ven_cantidad, 0)) - sum (nvl (fvpu.dev_cantidad, 0)) 
		from dwm_prod.f_ventas_pu_diario_min fvpu
		where 	fvpu.l_ubi_codigo = l.codigo
		and 	fvpu.emp_codigo = l.emp_codigo
		and 	fvpu.l_pro_codigo = p.codigo
		and 	fvpu.emp_codigo = p.emp_codigo
		and 	fvpu.fecha_comercial = f.codigo) 		"Unidades", 
	(select sum (nvl (fvpu.ven_nac_importe, 0)) - sum (nvl (fvpu.dev_nac_importe, 0))
		from dwm_prod.f_ventas_pu_diario_min fvpu
		where 	fvpu.l_ubi_codigo = l.codigo
		and 	fvpu.emp_codigo = l.emp_codigo
		and 	fvpu.l_pro_codigo = p.codigo
		and 	fvpu.emp_codigo = p.emp_codigo
		and 	fvpu.fecha_comercial = f.codigo) 		"Venta"
from
	dwm_prod.l_ubicaciones l, 
	dwm_prod.l_productos p, 
	dwm_prod.l_fechas f, 
	iposs.proveedores_articulos pa, 
	iposs.articulos_locales_proveedores alp, 
	iposs.proveedores pr
where
	( pa.art_codigo = p.prod_cod_interno 
and 	  pa.emp_codigo = p.emp_codigo
and 	  pa.fecha_borrado is null
and 	  pa.prov_codigo = pr.codigo
and 	  pa.prov_codigo in :Proveedor )
and
	( alp.art_loc_art_codigo = p.prod_cod_interno
and 	  alp.art_loc_loc_codigo = l.loc_codigo
and 	  alp.prov_codigo = pr.codigo
and 	  alp.emp_codigo = p.emp_codigo )
and 
	( alp.fecha_borrado is null )
and 
	( f.codigo between :FechaDesde and :FechaHasta)
and
	( l.loc_codigo in :Local )
and
	( p.tip_prod_codigo in (1, 3) )
and
	(  	  p.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  pa.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  alp.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  l.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.codigo, 
	l.emp_codigo, 
	p.codigo, 
	p.emp_codigo, 
	f.codigo, 
	l.loc_descripcion , 
	l.loc_codigo 		, 
	l.emp_codigo 		, 
	f.ano_nombre 		, 
	f.mes_nombre_corto 	, 
	f.dia_del_mes 		, 
	p.prod_codigo 		, 
	p.prod_descripcion 	, 
	p.rub_descripcion 	, 
	pr.razon_social
