/*

El reporte se llama Maestro de Precios por Proveedor y est� en la carpeta precios.

Necesitar�a si fuera posible que tuviera el precio de compra con impuestos no sin impuestos (sacarlo) tampoco necesito 
la columna costo y me dijera la vigencia de precios de compra y del precio de venta y agregarle c�d. articulo del 
proveedor si lo tiene, a su vez si lo sacas para un local te repite el articulo 6 veces no s� por qu� debe ser 1 vez 
por local aunque selecciones 1 solo.

*/
select
	l.codigo 		"Cod.Local (I)", 
	l.loc_cod_empresa	"Cod.Local", 
	l.descripcion 		"Local", 
	p.razon_social 		"Proveedor", 
	lis.descripcion 	"Lista de precios", 
	al.art_codigo_Externo 	"Cod.Art�culo", 
	pa.codigo_art_proveedor "Cod.Art. Proveedor", 
	ae.descripcion 		"Art�culo", 
	r.descripcion 		"Rubro", 
	(select i.descripcion from iposs.ivas i where i.codigo = ae.iva_codigo) "IVA Compra", 
	(select i.descripcion from iposs.ivas i where i.codigo = nvl (ae.iva_vta_codigo, ae.iva_codigo)) "IVA Venta", 
	nvl (
		(select cb.codigo_barra
		from iposs.codigos_barras cb
		where cb.art_codigo = ae.art_codigo
		and (cb.emp_codigo = ae.emp_codigo or (cb.emp_codigo = 1 and ae.veo_barras_modelo = 1))
		and rownum = 1), '') 	"Cod.Barra", 
	pv.precio 			"Precio Venta", 
	pv.vigencia 		"Vigencia PVenta", 
	pc.precio_sin_imp * (1 + (i.tasa/100)) 	"Precio compra c/imp", 
	pc.vigencia 		"Vigencia PCompra", 
	(select alpd.margen_ganancia
		from iposs.artlocprov_descuentos alpd
		where alpd.emp_codigo = $EMPRESA_LOGUEADA$ 
		and alpd.art_loc_pr_art_codigo = al.art_codigo
		and alpd.art_loc_pr_prov_Codigo = p.codigo
		and alpd.art_loc_pr_loc_codigo = al.loc_codigo
		and alpd.loc_lis_lis_pre_ve_codigo = lis.codigo
		and alpd.art_loc_pr_dim_con_numero = 0
		and nvl(:Fecha, sysdate) between alpd.vigencia and alpd.vigencia_hasta
		and rownum = 1) 		"Margen Ganancia", 
	(select  pco.costo_imp
		from iposs.precios_de_costos_articulos pco
		where 	pco.art_codigo = al.art_codigo
		and 	pco.loc_codigo = al.loc_Codigo
		and 	pco.emp_codigo = al.emp_codigo
		and 	nvl(:Fecha, sysdate) between pco.vigencia_desde and pco.vigencia_hasta
		and 	rownum = 1) 	"Precio Costo c/imp"
from 
	iposs.locales l, 
	iposs.proveedores p, 
	iposs.articulos_locales al, 
	iposs.articulos_empresas ae, 
	iposs.rubros r, 
	iposs.proveedores_empresas pe, 
	iposs.proveedores_articulos pa, 
	iposs.articulos_locales_proveedores alp, 
	iposs.listas_de_precios_ventas lis, 
	iposs.listas_pre_ventas_locales lisl, 
	iposs.precios_de_articulos_ventas pv, 
	iposs.precios_de_compras_articulos pc, 
	iposs.ivas 	i
where 
	ae.art_codigo = al.art_Codigo
and 	ae.emp_codigo = al.emp_codigo
and 
	al.loc_Codigo = l.codigo
and 
	ae.art_codigo = pa.art_codigo
and 	ae.emp_codigo = pe.emp_codigo 
and 
	al.art_codigo = alp.art_loc_art_codigo
and 	al.loc_codigo = alp.art_loc_loc_codigo
and 	al.emp_codigo = alp.emp_codigo
and 
	ae.rub_codigo = r.codigo
and 
	pa.prov_Codigo = pe.prov_codigo
and 	pa.emp_codigo = pe.emp_codigo
and 
	alp.prov_codigo = pe.prov_codigo
and 	alp.emp_codigo = pe.emp_codigo
and 
	pe.prov_codigo = p.codigo
and 
	ae.iva_codigo = i.codigo 
and 			-- precios de venta
		lis.codigo = lisl.lis_pre_ve_codigo
and 	l.codigo = lisl.loc_Codigo
and 	pv.art_codigo = al.art_codigo
and 	pv.loc_lis_lis_pre_ve_codigo = lis.codigo
and 	pv.loc_lis_loc_codigo = al.loc_codigo
and 	pv.dim_con_numero = 0
and 	nvl(:Fecha, sysdate) between pv.vigencia and pv.vigencia_hasta
and 	pv.vigencia_hasta > nvl(:Fecha, sysdate)
and 	pv.emp_codigo = $EMPRESA_LOGUEADA$
and 			-- precio de compra
		pc.prov_art_art_codigo = al.art_codigo
		and pc.loc_codigo = al.loc_codigo
		and pc.prov_art_prov_Codigo = p.codigo
		and pc.prov_art_dim_con_numero = 0
		and nvl(:Fecha, sysdate) between pc.vigencia and pc.vigencia_hasta
		and pc.vigencia_hasta > nvl(:Fecha, sysdate)
		and pc.prov_art_emp_codigo = $EMPRESA_LOGUEADA$
and 
	al.fecha_borrado is null
and	pa.fecha_borrado is null
and 	alp.fecha_borrado is null
and 	ae.fecha_borrado is null
and 	pe.fecha_borrado is null
and 
	( trim (upper (':Local')) = 'NULL' or l.codigo in :Local )
and
	( trim (upper (':Proveedor')) = 'NULL' or p.codigo in :Proveedor )
and 	
	( ae.emp_codigo  = $EMPRESA_LOGUEADA$
and 	  al.emp_codigo  = $EMPRESA_LOGUEADA$
and 	  pe.emp_codigo  = $EMPRESA_LOGUEADA$
and 	  l.emp_codigo   = $EMPRESA_LOGUEADA$
and 	  pa.emp_codigo  = $EMPRESA_LOGUEADA$
and 	  alp.emp_codigo = $EMPRESA_LOGUEADA$
and 	  lis.emp_codigo = $EMPRESA_LOGUEADA$ )
