select 
	l.descripcion 	"Local", 
	mf.caj_codigo 	"Caja", 
	mo.simbolo 	"Moneda", 
	mf.fecha_comercial 	"Fecha", 
	tdi.descripcion 	"Tipo Documento", 
	mf.serie_factura 	"Serie factura", 
	mf.numero_factura 	"Numero factura", 
	m.nombre_factura 	"Nombre factura", 
	m.ruc_factura 	"RUT factura", 
	m.direccion_factura 	"Dirección factura", 
	m.numero_operacion 	"Numero Operación", 
	i.descripcion 	"IVA", 
	p.codigo 		"Cod.Prov(I)", 
	p.razon_social 	"Proveedor", 
	p.direccion 		"Dirección proveedor", 
	mf.importe 	"Importe", 
	mf.monto_iva 	"Monto IVA"
from 
	iposs.movimientos_facturas mf, 
	iposs.movimientos m, 
	iposs.locales l, 
	iposs.monedas mo, 
	iposs.ivas i, 
	iposs.tipos_documentos_inventarios tdi, 
	iposs.proveedores p
where
	( mf.mov_numero_mov = m.numero_mov
and 	  mf.mov_emp_codigo = m.emp_codigo
and 	  mf.fecha_comercial = m.fecha_comercial)
and
	mf.loc_codigo = l.codigo
and
	mf.mon_codigo = mo.codigo
and
	mf.iva_codigo = i.codigo
and
	mf.tip_doc_in_codigo = tdi.codigo
and
	mf.fecha_comercial between :FechaDesde and :FechaHasta
and 	
	m.ruc_factura = p.ruc 
and 
	( trim (upper (':Proveedor')) = 'NULL'  or p.codigo in :Proveedor )
and 
	l.codigo in :Local
and
	( mf.mov_emp_codigo = $EMPRESA_LOGUEADA$
and 	  m.emp_codigo = $EMPRESA_LOGUEADA$
and 	  l.emp_codigo = $EMPRESA_LOGUEADA$)
