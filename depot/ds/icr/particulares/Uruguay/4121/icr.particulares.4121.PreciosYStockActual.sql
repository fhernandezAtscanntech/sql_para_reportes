select 
	ae.art_codigo 			"Art.Codigo(I)", 
	ae.art_codigo_externo 		"Art.Codigo", 
	ae.descripcion 			"Articulo", 
	(select cb.codigo_barra
		from codigos_barras cb
		where cb.art_Codigo = ae.art_codigo
		and (cb.emp_codigo = ae.emp_codigo or (cb.emp_codigo = 1 and ae.veo_barras_modelo = 1))
		and rownum = 1) 	"Barra", 
	l.codigo 			"Loc.Codigo(I)", 
	l.loc_Cod_empresa 		"Loc.Codigo", 
	l.descripcion 			"Local", 
	lpv.descripcion 		"Lista PV", 
	p.razon_social 			"Proveedor", 
	pc.costo_imp 			"Costo c.Imp", 
	pv.precio 			"Precio", 
	alpd.margen_ganancia 		"Margen", 
	pv.precio - pc.costo_imp 	"Utilidad", 
	sum(s.stock) 			"Stock"
from 
	iposs.articulos_empresas ae, 
	iposs.articulos_locales al, 
	iposs.locales l, 
	iposs.proveedores_articulos pa, 
	iposs.proveedores p, 
	iposs.listas_de_precios_ventas lpv,
	iposs.precios_de_articulos_ventas pv, 
	iposs.precios_de_costos_articulos pc, 
	iposs.artlocprov_descuentos alpd, 
	iposs.stocks s
where 
	ae.art_codigo = al.art_codigo
and 	ae.emp_codigo = al.emp_codigo
and
	al.loc_codigo = l.codigo
and 	al.emp_codigo = l.emp_codigo
and
	al.art_codigo = pa.art_codigo
and 	al.emp_codigo = pa.emp_codigo
and 
	pa.prov_codigo = p.codigo
and
	al.art_codigo = pc.art_Codigo
and 	al.loc_Codigo = pc.loc_codigo
and 	al.emp_codigo = pc.emp_codigo
and 
	al.art_codigo = pv.art_Codigo
and 	al.loc_codigo = pv.loc_lis_loc_codigo
and 	al.emp_codigo = pv.emp_codigo
and
	alpd.art_loc_pr_art_codigo = al.art_codigo
and 	alpd.art_loc_pr_loc_codigo = al.loc_codigo
and 	alpd.art_loc_pr_prov_Codigo = p.codigo
and 	alpd.emp_codigo = al.emp_codigo
and
	lpv.codigo = pv.loc_lis_lis_pre_ve_codigo
and
	s.art_codigo = al.art_codigo
and 	s.dep_loc_codigo = al.loc_codigo
and
	al.fecha_borrado is null
and 	ae.fecha_borrado is null
and 
	( l.codigo in :Local ) 
and 
	( p.codigo in :Proveedor ) 
and 
	sysdate between s.fecha and s.vigencia_hasta
and 	
	sysdate between pv.vigencia and pv.vigencia_hasta
and 
	sysdate between pc.vigencia_desde and pc.vigencia_hasta
and 
	sysdate between alpd.vigencia and alpd.vigencia_hasta
and 
	ae.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	al.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	l.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	pv.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	pc.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	pa.emp_codigo = $EMPRESA_LOGUEADA$ 
group by 
	ae.emp_codigo 
	ae.art_codigo, 
	ae.art_codigo_externo, 
	ae.descripcion, 
	ae.veo_barras_modelo, 
	l.codigo, 
	l.loc_Cod_empresa, 
	l.descripcion, 
	lpv.descripcion, 
	p.razon_social, 
 	pc.costo_imp, 
	pv.precio, 
	alpd.margen_ganancia, 
	pv.precio - pc.costo_imp
/
