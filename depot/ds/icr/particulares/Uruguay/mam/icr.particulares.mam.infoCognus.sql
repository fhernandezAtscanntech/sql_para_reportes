select 
	r.ruc 		"RUT", 
	l.loc_cod_empresa 	"Local", 
	m.caj_codigo 		"Caja", 
	to_char(m.fecha_comercial, 'yyyymmdd') 	"Fecha", 
	to_char(m.fecha_operacion, 'hh24mi') 	"Hora", 
	to_char(m.numero_operacion) 		"Id Ticket", 
	nvl (md.rub_codigo, 
		(select r.rub_cod_Empresa 
		from rubros r, articulos_locales al, articulos_empresas ae
		where al.art_codigo = ae.art_codigo
		and al.emp_codigo = ae.emp_codigo 
		and al.loc_codigo = m.loc_codigo
		and al.art_codigo_Externo = md.art_codigo
		and ae.rub_codigo = r.codigo))		"CodRubro", 
	nvl (md.descripcion_rubro, 
		(select r.descripcion
		from rubros r, articulos_locales al, articulos_empresas ae
		where al.art_codigo = ae.art_codigo
		and al.emp_codigo = ae.emp_codigo 
		and al.loc_codigo = m.loc_codigo
		and al.art_codigo_Externo = md.art_codigo
		and ae.rub_codigo = r.codigo)) 		"Rubro", 
	md.importe * (decode (md.tip_det_codigo, 4, 1, 6, 1, 18, 1, -1)) 		"Monto c/Imp", 
	(md.importe - 
		nvl (md.monto_iva, 0) -
		nvl (md.monto_imp_1, 0) -
		nvl (md.monto_imp_2, 0) -
		nvl (md.monto_imp_3, 0) -
		nvl (md.monto_imp_4, 0) -
		nvl (md.monto_imp_5, 0)) * (decode (md.tip_det_codigo, 4, 1, 6, 1, 18, 1, -1))	"Monto s/Imp"
from 
	movimientos m, 
	movimientos_detalles md, 
	razones_sociales r, 
	locales l
where 
	m.numero_mov = md.mov_numero_mov
and 	m.fecha_comercial = md.fecha_comercial
and 	m.emp_codigo = md.mov_emp_codigo
and 
	m.loc_codigo = l.codigo
and 
	l.raz_soc_codigo = r.codigo
and	
	m.tipo_operacion = 'VENTA'
and 
	m.loc_codigo = :Local
and 
	m.fecha_comercial between :FechaDesde and :FechaHasta
and 	:FechaHasta - :FechaDesde <= 31
and 
	m.emp_codigo = $EMPRESA_LOGUEADA$
and 	md.mov_emp_codigo = $EMPRESA_LOGUEADA$
and 	l.emp_codigo = $EMPRESA_LOGUEADA$
and 	r.emp_codigo = $EMPRESA_LOGUEADA$
