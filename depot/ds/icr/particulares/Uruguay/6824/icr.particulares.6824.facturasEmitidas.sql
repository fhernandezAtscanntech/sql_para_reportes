select 
	l.descripcion 	"Local", 
	mf.caj_codigo 	"Caja", 
	mo.simbolo 	"Moneda", 
	mf.fecha_comercial 	"Fecha", 
	tdi.descripcion 	"Tipo Documento", 
	mf.serie_factura 	"Serie", 
	mf.numero_factura 	"Numero", 
	m.nombre_factura 	"Nombre", 
	m.ruc_factura 	"RUT", 
	m.direccion_factura 	"Direcci�n", 
	m.numero_operacion 	"Numero Operaci�n", 
	m.fecha_operacion 	"Fecha Operaci�n",
	i.descripcion 		"IVA", 
	mf.importe 	"Importe", 
	mf.monto_iva 	"Monto IVA"
from 
	movimientos_facturas mf, 
	movimientos m, 
	locales l, 
	monedas mo, 
	ivas i, 
	tipos_documentos_inventarios tdi
where
	( mf.mov_numero_mov = m.numero_mov
and 	  mf.mov_emp_codigo = m.emp_codigo
and 	  mf.fecha_comercial = m.fecha_comercial)
and
	mf.loc_codigo = l.codigo
and
	mf.mon_codigo = mo.codigo
and
	mf.iva_codigo = i.codigo
and
	mf.tip_doc_in_codigo = tdi.codigo
and
	mf.fecha_comercial between :FechaDesde and :FechaHasta
and 	
	l.codigo in :Local
and
	( mf.mov_emp_codigo = $EMPRESA_LOGUEADA$
and 	  m.emp_codigo = $EMPRESA_LOGUEADA$
and 	  l.emp_codigo = $EMPRESA_LOGUEADA$)
