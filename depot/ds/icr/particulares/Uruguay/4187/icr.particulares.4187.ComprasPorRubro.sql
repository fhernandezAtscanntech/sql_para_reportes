select distinct
	c.audit_user 	"Usuario",
	l.descripcion 	"Local",
	tdi.descripcion "Tipo Documento", 
	c.notas 	"Notas", 
	c.numero 	"Numero", 
	p.razon_social "Razon social", 
	c.serie 	"Serie", 
	m.simbolo 	"Moneda", 
	trunc (c.fecha_emision) 	"Fecha Emision", 
	decode
		(c.tip_doc_in_codigo, 
		2, -c.total_calculado, 
		6, -c.total_calculado, 
		7, -c.total_calculado, 
		c.total_calculado) 	"Total Calculado", 
  decode 
		(c.tip_doc_in_codigo, 
		2, -c.total_ingresado, 
		6, -c.total_ingresado, 
		7, -c.total_ingresado, 
		c.total_ingresado) 	"Total Ingresado",
  decode 
		(c.tip_doc_in_codigo, 
		2, -c.total_iva_calculado, 
		6, -c.total_iva_calculado, 
		7, -c.total_iva_calculado, 
		c.total_iva_calculado) 	"Total IVA Calculado"
from 
	iposs.proveedores p,
	iposs.proveedores_empresas pe,
	iposs.comprobantes c,
	iposs.locales l, 
	iposs.monedas m, 
	iposs.tipos_documentos_inventarios tdi
where 	( p.codigo = pe.prov_codigo ) 
and 	( pe.emp_codigo <> 1 ) 
and 	( pe.fecha_borrado is null  ) 
and 	( p.codigo = c.prov_codigo ) 
and 	( l.codigo = c.loc_codigo ) 
and 	( m.codigo = c.mon_codigo ) 
and 	( tdi.codigo = c.tip_doc_in_codigo ) 
and 	( c.tip_doc_in_codigo in ('1','2','4','7') ) 
and 	( c.codigo, c.fecha_emision, c.emp_codigo ) in
		(select distinct cd.comp_codigo, cd.comp_fecha_emision, cd.comp_emp_codigo
		from iposs.comprobantes_detalles cd, iposs.articulos_empresas ae
		where cd.art_codigo = ae.art_codigo
		and cd.comp_emp_codigo = ae.emp_codigo
		and cd.comp_emp_codigo = $EMPRESA_LOGUEADA$
		and cd.comp_fecha_emision between :FechaDesde and :FechaHasta +1-1/24/60/60
		and ae.rub_codigo = :Rubro )
and 	( l.codigo in :Local ) 
and 	( c.fecha_emision(+) between :FechaDesde and :FechaHasta +1-1/24/60/60 )
-- agregado por fredy .. la consulta de discoverer confia en las politicas
and 	( pe.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  c.emp_codigo = $EMPRESA_LOGUEADA$ )
-- agregado por fredy .. necesitan parametrizar el proveedor
and (trim (upper (':Proveedor')) = 'NULL' or pe.prov_codigo in :Proveedor)

/*
basado en icr.oltp.ComprasporProvYfechaPorComprobante
*/

