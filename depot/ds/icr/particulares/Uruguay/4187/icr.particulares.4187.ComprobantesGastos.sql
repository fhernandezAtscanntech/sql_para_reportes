select 
	c.audit_user "Usuario",
	l.descripcion "Local",
	tdi.descripcion "Tipo Documento", 
	c.notas "Notas", 
	c.numero "Numero", 
	pe.prov_cod_externo "Cod.Proveedor", -- agregado por fredy .. pedido de issue rep-465
	p.ruc "RUT", 
	p.razon_social "Razon Social", 
	c.serie "Serie", 
	mo.simbolo "Moneda", 
	trunc(c.fecha_emision) "Fecha Emision", 
	trunc(c.fecha_contabilidad)	"Fecha Contabilidad", 
	c.audit_date "Fecha Ingreso", -- agregado por fredy a pedido de alvaro martinez
	c.fecha_vencimiento 	"Fecha Vencimiento", 
	decode(c.tip_doc_in_codigo,2,-c.total_calculado,6,-c.total_calculado,7,-c.total_calculado,c.total_calculado)"Total Calculado", 
	decode(c.tip_doc_in_codigo,2,-c.total_ingresado,6,-c.total_ingresado,7,-c.total_ingresado,c.total_ingresado)"Total Ingresado",
	decode(c.tip_doc_in_codigo,2,-c.total_iva_calculado,6,-c.total_iva_calculado,7,-c.total_iva_calculado,c.total_iva_calculado) "Total Iva Calculado", 
	c.imeba "IMEBA", 
	c.iva_percibido "IVA Percibido", 
	c.iric "IRIC"
from iposs.proveedores p,
 iposs.proveedores_empresas pe,
   iposs.comprobantes c,
    iposs.locales l, 
	iposs.monedas mo, 
	iposs.tipos_documentos_inventarios tdi
where ( p.codigo = pe.prov_codigo ) 
and ( pe.emp_codigo <> 1 ) 
and ( pe.fecha_borrado is null  ) 
and ( p.codigo = c.prov_codigo ) 
and ( l.codigo = c.loc_codigo ) 
and ( mo.codigo = c.mon_codigo ) 
and ( tdi.codigo = c.tip_doc_in_codigo ) 
and ( c.tip_doc_in_codigo in ('1','2','4','7') ) 
and ( l.codigo in :Local ) 
and ( c.fecha_emision(+) between :FechaDesde and to_date(:FechaHasta)+1-1/24/60/60 )
-- agregado por fredy .. la consulta de discoverer confia en las politicas
and ( pe.emp_codigo = $EMPRESA_LOGUEADA$ 
and   c.emp_codigo = $EMPRESA_LOGUEADA$ )
-- agregado por fredy .. necesitan parametrizar el proveedor
and (trim (upper (':Proveedor')) = 'NULL' or pe.prov_codigo in :Proveedor)
-- agregado por fredy .. rme-6616 se filtran los proveedores de gastos
and ( nvl (pe.de_gastos, 0) = 1 )

