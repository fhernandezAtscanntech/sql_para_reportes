select 
	ae.art_codigo_externo 		"C�digo", 
	ae.descripcion 			"Art�culo", 
	l.descripcion 			"Local", 
	p.razon_social 			"Proveedor", 
	r.rub_cod_empresa 		"Cod.Rubro", 
	r.descripcion 			"Rubro", 
	f.fam_cod_empresa 		"Cod.Familia", 
	f.descripcion 			"Familia", 
	sum ( (decode 
		(cd.comp_tip_doc_in_codigo, 
		2, -cd.cantidad, 
		6, -cd.cantidad, 
		7, -cd.cantidad, 
		cd.cantidad) )) 	"Cantidad", 
	sum ( decode (
		cd.comp_tip_doc_in_codigo, 
		2, -cd.total_linea_ingresado, 
		6, -cd.total_linea_ingresado, 
		7, -cd.total_linea_ingresado, 
		cd.total_linea_ingresado) ) 	"Total Ingresado", 
	sum ( decode (
		cd.comp_tip_doc_in_codigo, 
		2, -cd.total_linea_calculado, 
		6, -cd.total_linea_calculado, 
		7, -cd.total_linea_calculado, 
		cd.total_linea_calculado) ) 	"Total Calculado", 			-- tomado de la consulta original, 
	(select precio
		from iposs.precios_de_articulos_ventas pv, iposs.listas_pre_ventas_locales lp
		where pv.art_codigo = ae.art_codigo
		and pv.emp_codigo = ae.emp_codigo
		and pv.loc_lis_loc_codigo = lp.loc_codigo
		and pv.loc_lis_lis_pre_ve_codigo = lp.lis_pre_ve_codigo
		and cd.comp_fecha_emision between pv.vigencia and vigencia_hasta
		and lp.loc_codigo = cd.loc_codigo
		and lp.lis_pre_ve_codigo_externo = 1
		and rownum = 1) 				"Precio Venta"
from 
	iposs.articulos_empresas ae, 
	iposs.comprobantes_detalles cd, 
	iposs.rubros r, 
	iposs.familias f, 
	iposs.locales l, 
	iposs.proveedores p
where 	ae.rub_codigo = r.codigo
and 	ae.fam_codigo = f.codigo (+)
and 	l.codigo = cd.loc_codigo
and 	p.codigo = cd.comp_prov_codigo
and 	ae.emp_codigo = cd.comp_emp_codigo 
and 	ae.art_codigo = cd.art_codigo
and 
		cd.comp_tip_doc_in_codigo in ('1','2','4','7') 
and 	p.codigo in :Proveedor
and 	cd.comp_fecha_emision between :FechaDesde and :FechaHasta + 1 - (1/24/60/60)
and 	l.codigo in :Local
and 	
		ae.emp_codigo = $EMPRESA_LOGUEADA$
and 	l.emp_codigo = $EMPRESA_LOGUEADA$
and 	cd.comp_emp_codigo = $EMPRESA_LOGUEADA$
group by 
	ae.art_codigo_externo, 
	ae.descripcion, 
	l.descripcion, 
	p.razon_social, 
	r.rub_cod_empresa, 
	r.descripcion, 
	f.fam_cod_empresa, 
	f.descripcion
