select 
	l1.loc_cod_empresa 	"Sucursal 1", 
	l1.descripcion 		"Local 1", 
	l2.loc_cod_empresa 	"Sucursal 2", 
	l2.descripcion 		"Local 2", 
	lis.descripcion 	"Lista Precios", 
	ae.art_codigo_Externo 	"Cod.articulo",
	ae.descripcion 		"Articulo",
	r.descripcion 		"Rubro", 
	iposs.f_precio_venta_2 (ae.art_codigo, al1.loc_codigo, lis.codigo, :Fecha, ae.emp_codigo) 	"Precio 1", 
	iposs.f_precio_venta_2 (ae.art_codigo, al2.loc_codigo, lis.codigo, :Fecha, ae.emp_codigo) 	"Precio 2"
from 
	iposs.articulos_empresas ae
	join iposs.rubros r
		on ae.rub_codigo = r.codigo
	join iposs.articulos_locales al1
		on ae.art_codigo = al1.art_codigo
		and ae.emp_codigo = al1.emp_codigo
		and al1.fecha_borrado is null
	join iposs.articulos_locales al2
		on ae.art_codigo = al2.art_codigo
		and ae.emp_codigo = al2.emp_codigo
		and al2.fecha_borrado is null
	join iposs.listas_pre_ventas_locales lisloc1
		on al1.loc_codigo = lisloc1.loc_codigo
	join iposs.listas_pre_ventas_locales lisloc2
		on al2.loc_codigo = lisloc2.loc_codigo
	join iposs.listas_de_precios_ventas lis
		on lisloc1.lis_pre_ve_codigo = lis.codigo
		and lisloc2.lis_pre_ve_codigo = lis.codigo
	join iposs.locales l1
		on al1.loc_codigo = l1.codigo
		and al1.emp_codigo = l1.emp_codigo
		and l1.coord_x is null
	join iposs.locales l2
		on al2.loc_codigo = l2.codigo
		and al2.emp_codigo = l2.emp_codigo
		and l2.coord_x is null
where	ae.fecha_borrado is null
and 	lis.codigo = :ListaPreVen
and 	al1.loc_codigo < al2.loc_codigo
and iposs.f_precio_venta_2 (ae.art_codigo, al1.loc_codigo, lis.codigo, :Fecha, ae.emp_codigo) 	
	<>
	iposs.f_precio_venta_2 (ae.art_codigo, al2.loc_codigo, lis.codigo, :Fecha, ae.emp_codigo) 	
and 	l1.codigo in :Locales
and 	l2.codigo in :Locales
and 	( trim (upper (':Rubros')) = 'NULL' or r.codigo in :Rubros )
and 	ae.emp_codigo = $EMPRESA_LOGUEADA$
and 	al1.emp_codigo = $EMPRESA_LOGUEADA$
and 	al2.emp_codigo = $EMPRESA_LOGUEADA$
and 	lis.emp_codigo = $EMPRESA_LOGUEADA$
and 	l1.emp_codigo  = $EMPRESA_LOGUEADA$
and 	l2.emp_codigo  = $EMPRESA_LOGUEADA$
