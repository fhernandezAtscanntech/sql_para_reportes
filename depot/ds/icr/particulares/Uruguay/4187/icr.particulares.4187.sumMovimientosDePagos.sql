select 		
	sum.fecha_comercial	"Fecha"	, 
	sum.mov_emp_codigo	"Codigo Empresa"	, 
	e.descripcion	"Empresa"	, 
	sum.loc_codigo	"Codigo Local"	, 
	l.descripcion	"Local"	, 
	sum.caj_codigo	"Codigo Caja"	, 
	c.descripcion	"Caja"	, 
	(case sum.tip_pag_codigo 
		when 9 then sum.importe_pago * sum.cotiza_compra
		else 0
	end) 			"Contado", 
	(case sum.tip_pag_codigo 
		when 10 then sum.importe_pago * sum.cotiza_compra
		else 0
	end)			"Crédito", 
	(case sum.tip_pag_codigo 
		when 13 then sum.importe_pago * sum.cotiza_compra
		else 0
	end)			"Débito", 
	(case sum.tip_pag_codigo 
		when 12 then sum.importe_pago * sum.cotiza_compra
		else 0
	end) 			"Vale", 
	(case sum.tip_pag_codigo 
		when 11 then sum.importe_pago * sum.cotiza_compra
		else 0
	end) 			"Cheques", 
	(case when sum.tip_pag_codigo in (9, 10, 11, 12, 13)
		then sum.importe_pago * sum.cotiza_compra
		else 0
	end) 			"Total Caja"
from 		
	sum_movimientos_de_pagos sum, 	
	empresas e,	
	locales l,	
	cajas c, 
	monedas m
where		
	sum.mov_emp_codigo = e.codigo	
and	sum.loc_Codigo = l.codigo	
and	sum.caj_codigo = c.codigo	
and	sum.loc_Codigo = c.loc_Codigo	
and	sum.mon_codigo = m.codigo
and 	fecha_comercial = :Fecha
and		l.codigo = :Local 
and
	(l.emp_codigo = $EMPRESA_LOGUEADA$ and
	sum.mov_emp_codigo = $EMPRESA_LOGUEADA$)
