select
	l.codigo 		"Cod.Local (I)", 
	l.loc_cod_empresa	"Cod.Local", 
	l.descripcion 		"Local", 
	r.descripcion  		"Rubro", 
	al.art_codigo_Externo 	"Cod.Artículo", 
	ae.descripcion 		"Artículo", 
	nvl (
		(select cb.codigo_barra
		from codigos_barras cb
		where cb.art_codigo = ae.art_codigo
		and (cb.emp_codigo = ae.emp_codigo or (cb.emp_codigo = 1 and ae.veo_barras_modelo = 1))
		and rownum = 1), '') 	"Cod.Barra", 
	mo.simbolo 		"Moneda", 
	pca.costo_ultimo 		"Costo anterior", 
	pca.vigencia_desde 		"Vigencia anterior", 
	pc.costo_ultimo 		"Costo Último", 
	pc.costo_imp 		"Costo c/imp.", 
	pc.vigencia_desde 	"Vigencia", 
	pc.vigencia_hasta 	"Vigencia Hasta", 
	pc.audit_user 		"Usuario", 
	iposs.f_precio_venta_2 (pc.art_codigo, pc.loc_codigo, lis.codigo, pc.vigencia_desde, pc.emp_codigo) "Precio Venta", 
	pe.prov_cod_externo "Cod.Prov", 
	p.ruc 	"RUT", 
	p.razon_social "Proveedor"
from 
	iposs.articulos_locales al
	join iposs.locales l
		on al.loc_Codigo = l.codigo 
		and al.emp_codigo = l.emp_codigo
	join iposs.articulos_empresas ae
		on ae.art_codigo = al.art_Codigo 
		and ae.emp_codigo = al.emp_codigo
	join iposs.rubros r 
		on ae.rub_codigo = r.codigo
	join iposs.listas_pre_ventas_locales lisloc
		on lisloc.loc_codigo = l.codigo
	join iposs.listas_de_precios_ventas lis
		on lis.codigo = lisloc.lis_pre_ve_codigo
		and lis.emp_codigo = l.emp_codigo
	join iposs.precios_de_costos_articulos pc
		on al.art_codigo = pc.art_codigo
		and al.loc_Codigo = pc.loc_codigo
		and al.emp_codigo = pc.emp_codigo
		and pc.vigencia_desde >= :FechaDesde
		and pc.vigencia_desde <= :FechaHasta 
		and pc.vigencia_hasta > :FechaDesde  -- por eficiencia
	left join iposs.precios_de_costos_articulos pca
		on pca.art_codigo = pc.art_codigo
		and pca.loc_Codigo = pc.loc_codigo
		and pca.emp_codigo = pc.emp_codigo
		and pca.vigencia_hasta = pc.vigencia_desde - (1/24/60/60)
	join iposs.monedas mo
		on pc.mon_codigo = mo.codigo
	left join iposs.proveedores_empresas pe
		on pe.prov_codigo = pc.prov_codigo_dw
		and pe.emp_codigo = pc.emp_codigo
	join iposs.proveedores p
		on p.codigo = pe.prov_codigo
where	al.fecha_borrado is null
and 	ae.fecha_borrado is null
and 	abs(pc.costo_ultimo - pca.costo_ultimo) > 0.01
and ( trim (upper (':Local')) = 'NULL' or l.codigo in :Local )
and	( trim (upper (':Rubro')) = 'NULL' or r.codigo in :Rubro )
and ( lis.codigo = :Lista) 
and	( ae.emp_codigo  = $EMPRESA_LOGUEADA$
and 	  al.emp_codigo  = $EMPRESA_LOGUEADA$
and 	  l.emp_codigo   = $EMPRESA_LOGUEADA$
and 	  pc.emp_codigo  = $EMPRESA_LOGUEADA$ )
order by 
	al.art_codigo_Externo, 
	ae.descripcion, 
	pc.vigencia_desde
