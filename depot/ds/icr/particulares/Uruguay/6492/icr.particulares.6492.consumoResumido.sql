select
	nvl (md.art_codigo, 'R-'||md.rub_codigo) "Cod.Artículo",
	l.loc_cod_empresa "Cod.Local",
	l.descripcion "Local",
	nvl ((select ae.descripcion
		from iposs.articulos_empresas ae, iposs.articulos_locales al
		where ae.art_codigo = al.art_codigo
		and ae.emp_codigo = m.emp_codigo
		and al.emp_codigo = m.emp_codigo
		and al.loc_codigo = m.loc_codigo
		and al.art_Codigo_Externo = md.art_codigo),
		'RUBRO - '||md.descripcion_rubro) "Artículo",
	per.nombre_1 "Cliente",
	cli.cli_cod_externo "Cod.Cliente",
	ctar.numero_tarjeta "Nro.Tarjeta",
	(select fam.descripcion
		from iposs.familias fam, iposs.articulos_empresas ae, iposs.articulos_locales al
		where ae.art_codigo = al.art_codigo
		and ae.emp_codigo = m.emp_codigo
		and al.emp_codigo = m.emp_codigo
		and al.loc_codigo = m.loc_codigo
		and al.art_Codigo_Externo = md.art_codigo
		and ae.fam_codigo = fam.codigo) "Familia",
	gcli.descripcion "Grupo de Clientes",
	md.fecha_comercial "Fecha",
	sum (md.cantidad* decode (md.tip_det_codigo, 4, 1, 6, 1, 17, 1, -1)) "Cantidad",
	sum (md.importe * decode (md.tip_det_codigo, 4, 1, 6, 1, 17, 1, -1)) "Importe"
from
   iposs.clientes cli,
   iposs.locales l,
   iposs.personas per,
   (select * from iposs.movimientos_detalles d
   union
   select * from iposs.movimientos_det_servs s) md,
   iposs.movimientos m,
   iposs.grupos_de_clientes gcli,
   iposs.cre_clientes ccli,
   iposs.cre_sellos_empresas csel,
   iposs.cre_tarjetas ctar,
   iposs.cre_movimientos_cc cmovcc
where 	cli.per_codigo = per.codigo 
and cli.per_codigo = ccli.per_codigo 
and cli.emp_codigo = csel.emp_codigo 
and ccli.cre_sel_codigo = csel.cre_sel_codigo 
and ctar.cre_sel_codigo = csel.cre_sel_codigo
and ctar.cli_codigo = ccli.codigo 
and cmovcc.emp_codigo = csel.emp_codigo
and cmovcc.cre_tar_codigo = ctar.codigo 
and m.loc_codigo = l.codigo
and m.emp_codigo = l.emp_codigo
and m.emp_codigo = cmovcc.emp_codigo
and m.fecha_comercial = trunc(cmovcc.fecha_comercial)
and m.loc_codigo = cmovcc.loc_codigo
and m.caj_codigo = cmovcc.caj_codigo
and m.numero_operacion = cmovcc.numero_operacion
and nvl (m.v0_numero_operacion, 0) = nvl(cmovcc.v0_numero_operacion, 0)
and m.emp_codigo = md.mov_emp_codigo
and m.numero_mov = md.mov_numero_mov
and m.fecha_comercial = md.fecha_comercial
and cli.gru_cli_codigo = gcli.codigo(+)
and m.fecha_comercial between :FechaDesde and :FechaHasta 
and l.codigo = :Local
and cmovcc.cre_tm_codigo in (1, 3)
and cli.emp_codigo = $EMPRESA_LOGUEADA$
and md.mov_emp_codigo = $EMPRESA_LOGUEADA$
and m.emp_codigo = $EMPRESA_LOGUEADA$
and csel.emp_codigo = $EMPRESA_LOGUEADA$
and cmovcc.emp_codigo = $EMPRESA_LOGUEADA$
and l.emp_codigo = $EMPRESA_LOGUEADA$
group by
m.emp_codigo, m.loc_codigo, l.loc_cod_empresa, l.descripcion, md.art_codigo, md.rub_codigo, md.descripcion_rubro, per.nombre_1, cli.cli_cod_externo, ctar.numero_tarjeta, md.tip_det_codigo, md.fecha_comercial, gcli.descripcion
union
select 
	to_char(cmovcc.cre_tm_codigo), null, null, substr(cmovcc.notas, 1, 60), per.nombre_1, cli.cli_cod_externo, ctar.numero_tarjeta, null, gcli.descripcion, cmovcc.fecha_comercial, 0, cmovcc.importe * decode (cmovcc.cre_tm_codigo, 10, 1, 11, -1, 0)
from
	iposs.clientes cli, iposs.locales l, iposs.personas per, iposs.grupos_de_clientes gcli, iposs.cre_clientes ccli, iposs.cre_sellos_empresas csel, iposs.cre_tarjetas ctar, iposs.cre_movimientos_cc cmovcc
where 	cli.per_codigo = per.codigo
and cli.per_codigo = ccli.per_codigo
and cli.emp_codigo = csel.emp_codigo
and ccli.cre_sel_codigo = csel.cre_sel_codigo
and ctar.cre_sel_codigo = csel.cre_sel_codigo
and ctar.cli_codigo = ccli.codigo
and cli.gru_cli_codigo = gcli.codigo(+)
and cmovcc.emp_codigo = csel.emp_codigo
and cmovcc.cre_tar_codigo = ctar.codigo
and cmovcc.fecha_comercial between :FechaDesde and :FechaHasta
and cmovcc.cre_tm_codigo in (10, 11)
and cli.emp_codigo = $EMPRESA_LOGUEADA$
and csel.emp_codigo = $EMPRESA_LOGUEADA$
and cmovcc.emp_codigo = $EMPRESA_LOGUEADA$
and l.emp_codigo = $EMPRESA_LOGUEADA$
