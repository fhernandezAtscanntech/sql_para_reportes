select 		
-- es el mismo origen que ipos.stock.IngresosEgresosBENuevo, pero se agrega el precio de venta a la l�nea
	ae.art_codigo_externo 	"Cod.Art�culo",
	ae.descripcion 	"Art�culo", 
	r.rub_cod_empresa 	"Cod.Rubro", 
	r.descripcion 		"Rubro", 
	dep_ori.descripcion 	"Dep�sito Origen", 
	dep_des.descripcion 	"Dep�sito Destino", 
	tmi.descripcion 	"Tipo Movimiento", 
	mi.fecha 	"Fecha", 
	mi.numero_documento 	"Numero", 
	mi.observaciones 		"Observaciones", 
	mid.costo_imp 	"Costo c.Imp",
	mid.costo 		"Costo s.Imp", 
	mid.precio_unitario 	"Precio Unitario", 
	(iposs.f_precio_venta(
		mid.art_codigo, 
		nvl (mi.loc_codigo_origen, mi.loc_codigo_destino), 
		nvl ((select to_number(valor) from iposs.parametros_empresas pe where pe.emp_codigo = mi.emp_codigo and par_codigo = 'LIS_PRE_VE_DEF'), 
			 (select codigo from iposs.listas_de_precios_ventas l where l.emp_codigo = mi.emp_codigo and lis_pre_ve_cod_empresa = 1)), 
		mi.fecha, 
		mi.emp_codigo)) 	"Precio Venta", 
	sum (decode (
			tmi.codigo, 
			1, mid.cantidad_destino*mid.unidades, 
			3, mid.cantidad_origen*mid.unidades)) "Cantidad", 
--	sum (decode (
--			tmi.codigo, 
--			1, mid.costo_imp * mid.cantidad_destino * mid.unidades, 
--			3, mid.costo_imp * mid.cantidad_origen * mid.unidades)) "Total c.Imp", 
--	sum (decode (
--			tmi.codigo, 
--			1, mid.costo * mid.cantidad_destino * mid.unidades, 
--			3, mid.costo * mid.cantidad_origen * mid.unidades)) "Total s.Imp", 
	mi.total 	"Total Movimiento"
from 		
	iposs.articulos_empresas ae, 	
	iposs.rubros r, 
	iposs.depositos dep_ori, 	
	iposs.depositos dep_des, 	
	iposs.inv_movimientos mi, 	
	iposs.inv_movimientos_detalles mid, 	
	iposs.inv_tipos_movimientos tmi, 
	#LocalesVisibles# lv
where 		
	( ( dep_des.loc_codigo(+) = mi.loc_codigo_destino and 	
	    dep_des.codigo(+) = mi.dep_codigo_destino ) and 	
	  ( dep_ori.loc_codigo(+) = mi.loc_codigo_origen and 	
	    dep_ori.codigo(+) = mi.dep_codigo_origen ) and 	
	  ( mi.fecha_comercial = mid.fecha_comercial and 	
	    mi.emp_codigo = mid.emp_codigo and 	
	    mi.numero = mid.inv_mov_numero ) and 	
	  ( ae.art_codigo = mid.art_codigo and 	
	    ae.emp_codigo = mid.emp_codigo ) and 	
	  ( tmi.codigo = mi.inv_tip_mov_codigo ) and 
	  ( ae.rub_codigo = r.codigo )) and 	
	( mi.fecha_comercial between :FechaDesde and :FechaHasta ) and 	
	( tmi.codigo in ( 1, 3 ) ) and 	
	( mi.comp_codigo is null ) and 		-- ignoro los ingresos/egresos con comprobantes asociados
	( nvl (mi.loc_codigo_origen, mi.loc_codigo_destino) = lv.loc_codigo and
	  ae.emp_codigo = $EMPRESA_LOGUEADA$ and	
	  mi.emp_codigo = $EMPRESA_LOGUEADA$	
	)
group by 		
	ae.art_codigo_externo, 	
	ae.descripcion, 	
	mid.art_codigo, 
	r.rub_cod_empresa, 
	r.descripcion, 
	dep_ori.descripcion, 	
	dep_des.descripcion, 	
	mi.loc_codigo_origen, 
	mi.loc_codigo_destino, 
	mi.emp_codigo, 
	tmi.descripcion, 	
	mi.fecha, 	
	mi.numero_documento, 	
	mi.observaciones, 
	mid.costo, 	
	mid.costo_imp, 
	mid.precio_unitario, 
	mi.total	
