select 		
	l.loc_descripcion 	"Local", 
	l.loc_codigo 	"Cod.Local", 
	l.emp_codigo 	"Emp.Codigo", 
	f.ano_nombre 	"A�o", 
	f.mes_nombre_corto 	"Mes", 
	f.dia_del_mes 	"D�a", 
	p.prod_codigo 	"Cod.Articulo", 
	p.prod_descripcion 	"Articulo", 
	p.codigo_barras 	"Cod.Barra", 
	p.rub_descripcion 	"Rubro", 
	c.cli_cod_empresa 	"Cod.Cliente", 
	c.cli_descripcion 	"Cliente", 
	fu.fun_cod_empresa 	"Cod.Cajero", 
	fu.fun_descripcion 	"Cajero", 
	sum (nvl (fv.ven_cantidad, 0)) - sum (nvl (fv.dev_cantidad, 0)) 	"Unidades", 
	sum (nvl (fv.ven_nac_importe, 0)) - 	
	sum (nvl (fv.dev_nac_importe, 0)) 	"Venta"
from 		
	dwm_prod.l_ubicaciones l, 	
	dwm_prod.l_fechas f, 	
	dwm_prod.l_productos p, 	
	dwm_prod.l_clientes c, 
	dwm_prod.l_funcionarios fu, 
	dwm_prod.f_local_cli_ven_diario fv
where	
	( l.codigo = fv.l_ubi_codigo 
and 	l.emp_codigo = fv.emp_codigo ) 
and	
	( p.codigo = fv.l_pro_codigo 
and 	  p.emp_codigo in (fv.emp_codigo, $EMPRESA_MODELO$) )
and	
	( c.codigo = fv.l_cli_codigo
and	  c.emp_codigo in (fv.emp_codigo, $EMPRESA_MODELO$) )
and	
	( fu.codigo = fv.l_fun_codigo_cajero 
and	  fu.emp_codigo in (fv.emp_codigo, $EMPRESA_MODELO$) )
and	
	( fv.fecha_comercial = f.codigo )	
and		
	( fv.fecha_comercial between :FechaDesde and :FechaHasta
and 	  :FechaHasta - :FechaDesde <= 62 )	
and		
	( l.loc_codigo in :Local 
and 	  l.emp_codigo = $EMPRESA_LOGUEADA$ )	
group by 		
	l.loc_descripcion , 	
	l.loc_codigo 	, 
	l.emp_codigo 	, 
	f.ano_nombre 	, 
	f.mes_nombre_corto 	, 
	f.dia_del_mes 	,
	p.prod_codigo 	, 
	p.prod_descripcion 	, 
	p.codigo_barras 	, 
	p.rub_descripcion	, 
	c.cli_cod_empresa 	, 
	c.cli_descripcion 	, 
	fu.fun_cod_empresa 	, 
	fu.fun_descripcion 	
