multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select
	'select 
		md.mov_numero_mov 	"Nro.Movimiento", 
		md.linea 	"L�nea", 
		md.art_codigo 	"Art.Codigo", 
		md.descripcion 	"Art�culo", 
		md.rub_codigo 	"Rub.Codigo", 
		md.descripcion_rubro 	"Rubro", 
		decode (md.tip_det_codigo, 
			4, '''||'VENTA'||''', 
			5, '''||'DEVOLUCION'||''', 
			6, '''||'VENTA RUBRO'||''', 
			7, '''||'DEVOLUCION RUBRO'||''', 
			17, '''||'SERVICIO'||''', 
			18, '''||'DEVOLUCION SERVICIO'||''', 
			'''||'DESCONOCIDO'||''')	"Tipo l�nea", 
		md.cantidad 	"Cantidad", 
		md.importe 	"Importe"
	from
		iposs.movimientos_detalles md, 
		iposs.mi_movimientos_pagos mimp
	where
		md.mov_numero_mov = mimp.mov_numero_mov
	and 	md.fecha_comercial = mimp.fecha_comercial
	and 	md.mov_emp_codigo = mimp.mov_emp_codigo
	and
		md.fecha_comercial =  to_date('''||:Fecha||''', '''||'dd/mm/yyyy'||''')
	and 	mimp.numero_tarjeta = '''||:TUS||'''
	order by 
		md.mov_numero_mov, 
		md.linea
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
select 
	md.mov_numero_mov 	"Nro.Movimiento", 
	md.linea 	"L�nea", 
	md.art_codigo 	"Art.Codigo", 
	md.descripcion 	"Art�culo", 
	md.rub_codigo 	"Rub.Codigo", 
	md.descripcion_rubro 	"Rubro", 
	decode (md.tip_det_codigo,
		4, 'VENTA', 
		5, 'DEVOLUCION', 
		6, 'VENTA RUBRO', 
		7, 'DEVOLUCION RUBRO',  
		17, 'SERVICIO', 
		18, 'DEVOLUCION SERVICIO', 
		'DESCONOCIDO')	"Tipo l�nea", 
	md.cantidad 	"Cantidad", 
	md.importe 	"Importe"
from
	iposs.movimientos_detalles md, 
	iposs.mi_movimientos_pagos mimp
where
	1=2
)

######################
consulta
######################
select 1 from #reco#
