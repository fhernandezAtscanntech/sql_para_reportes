select 
	rccc.emp_codigo 		"Emp.Codigo", 
	l.codigo 		"Loc.Codigo (I)", 
	l.loc_Cod_empresa 		"Loc.Codigo", 
	l.descripcion 		"Local", 
	rccc.codigo 		"CC Codigo", 
	rccc.descripcion 		"Cuenta Corriente", 
	rccc.saldo		"Saldo Actual", 
	rccc.sobregiro 		"Sobregiro", 
	rcmc.numero_mov_cc 		"Numero Mov CC", 
	rcmc.caj_codigo 		"Caja", 
	rcmc.fecha 		"Fecha", 
	rcmc.Compañia 		"Compañia", 
	rcmc.nro_lote 		"Nro.Lote", 
	rcmc.audit_number 		"Nro.Audit", 
	rcmc.reference_number		"Nro.Ref", 
	rcmc.nro_tramite 		"Nro.Tram", 
	rctm.descripcion || decode 
		(rctm.codigo, 
		10, 
			decode (rcmc.estado, 
			0, ' - Pendiente', 
			1, ' - Confirmado', 
			2, ' - Reversado', 
			3, ' - Anulado', 
			4, ' - Borrado', 
			5, ' - Error', 
			' - Sin Definir'), 
		'')		"Tipo Movimiento", 
	decode (
		rcmc.tip_mov_cc, 
			1, nvl (rcmc.importe_pos_mon, rcmc.importe_pos)*-1, 
			2, 0, 
			5, nvl (rcmc.importe_pos_mon, rcmc.importe_pos)*-1, 
			decode ( 
				nvl (rcmc.estado, 0), 
				3, nvl (rcmc.importe_pos_mon, rcmc.importe_pos), 
				nvl (rcmc.importe_pos_mon, rcmc.importe_pos) * nvl(rcmc.tip_ope_codigo, 1)) )	"Importe Pos", 
	decode (
		rcmc.tip_mov_cc, 
			1, rcmc.importe*-1, 
			2, 0, 
			5, rcmc.importe*-1, 
			decode ( 
				nvl (rcmc.estado, 0), 
				3, rcmc.importe, 
				rcmc.importe * nvl (rcmc.tip_ope_codigo, 1)) )	"Importe", 
	rcmc.arancel 		"Arancel", 
	(select saldo from iposs.rc_saldos_mensuales rcsm where rcsm.emp_codigo = rccc.emp_codigo and rcsm.cue_cor_codigo = rccc.codigo and fecha = to_date('01/'||to_char(:Fecha, 'mm/yyyy'), 'dd/mm/yyyy'))		"Saldo Inicial", 
	(select saldo from iposs.rc_saldos_mensuales rcsm where rcsm.emp_codigo = rccc.emp_codigo and rcsm.cue_cor_codigo = rccc.codigo and fecha = add_months (to_date ('01/' || to_char (:Fecha, 'mm/yyyy'), 'dd/mm/yyyy'), 1))		"Saldo Final"
from 
	iposs.locales l, 
	iposs.rc_cuentas_corrientes rccc, 
	iposs.rc_movimientos_cc rcmc, 
	iposs.rc_tipos_movimientos_cc rctm
where 
	( rccc.codigo = rcmc.cue_cor_codigo 
and 	  rccc.emp_codigo = rcmc.emp_codigo )
and 
	( rctm.codigo = rcmc.tip_mov_cc )
and 
	( l.codigo(+) = rcmc.loc_codigo ) 
and 
	( rcmc.fecha between 
		to_date('01/'||to_char(:Fecha, 'mm/yyyy'), 'dd/mm/yyyy') 
	and 	(add_months (to_date ('01/' || to_char (:Fecha, 'mm/yyyy'), 'dd/mm/yyyy'), 1) - 1/24/60/60 ) )
and 
	(trim (upper(':CC')) = 'NULL' or rccc.codigo  in :CC)
and 
	( nvl (rcmc.estado, 0) not in ( 2, 4, 5 ) )
