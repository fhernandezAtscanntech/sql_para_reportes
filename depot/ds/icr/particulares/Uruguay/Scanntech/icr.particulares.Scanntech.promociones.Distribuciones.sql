select 
	l.emp_codigo 			"Cod.Empresa (I)", 
	l.codigo 			"Cod.Local (I)", 
	p.prom_cod_empresa 		"Prom. Codigo", 
	p.descripcion 		"Promocion", 
	l.loc_Cod_empresa 		"Loc. Codigo", 
	l.descripcion 		"Local", 
	pl.audit_date 		"Fecha Alta Promo en Local", 
	c.codigo 			"Cod.Caja",
	c.descripcion 		"Caja", 
	dp.fecha_ult_consulta 	"Ult. Consulta"
from 
	locales l, 
	cajas c, 
	promociones p, 
	prom_vigencias pv, 
	prom_locales_habilitados pl, 
	distribuciones_pendientes dp
where
	( l.codigo = c.loc_codigo )
and
	( l.codigo = dp.loc_Codigo
and 	  c.codigo = dp.caj_codigo )
and
	( c.inactiva = 0 )
and
	( l.codigo = pl.loc_codigo )
and 	
	( p.codigo = pl.prom_codigo )
and
	( p.codigo = pv.prom_codigo )
and					-- Promos vigentes a una fecha dada
	( :Fecha between pv.vigencia_desde and pv.vigencia_hasta ) 	
and 					-- �ltima consulta de la caja anterior a que se dio de alta el local en la promo
	( dp.fecha_ult_consulta < pl.audit_date )
order by 
	dp.fecha_ult_consulta, 
	l.codigo
