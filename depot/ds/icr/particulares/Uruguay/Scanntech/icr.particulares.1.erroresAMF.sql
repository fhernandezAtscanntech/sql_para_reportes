select 
	amfs.codigo,  
	amfs.servidor, 
	amfs.base, 
	amfs.servicio,
	amfs.emp_codigo, 
	amfs.fecha, 
	amfs.cantidad
from 
	iposs.amf_errores_sum amfs
where 
	amfs.fecha >= trunc (sysdate - :Dias)
