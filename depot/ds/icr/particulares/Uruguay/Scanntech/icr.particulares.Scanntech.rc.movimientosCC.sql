select 
	l.codigo 		"Loc.Codigo (I)",
	l.loc_Cod_empresa 	"Loc.Codigo", 
	l.descripcion 		"Local", 
	e.codigo 			"Cod.Empresa", 
	e.descripcion  		"Empresa", 
	rccc.codigo 		"Cod. Cuenta Corriente", 
	rccc.descripcion 	"Cuenta Corriente", 
	rcmc.numero_mov_cc 	"Numero Mov CC", 
	rcmc.fecha 		"Fecha", 
	trunc (rcmc.fecha) 	"Fecha Entera", 
	rcmc.nro_lote 		"Nro.Lote", 
	rctm.descripcion || decode 
		(rctm.codigo, 
		10, 
			decode (rcmc.estado, 
			0, ' - Pendiente', 
			1, ' - Confirmado', 
			2, ' - Reversado', 
			3, ' - Anulado', 
			4, ' - Borrado', 
			5, ' - Error', 
			' - Sin Definir'), 
		'')		"Tipo Movimiento", 
	rcmc.Compa�ia 		"Compa�ia", 
	sum (rccc.saldo) 	"Saldo", 
	sum ( decode (
		rcmc.tip_mov_cc, 
			1, nvl (rcmc.importe_pos_mon, rcmc.importe_pos)*-1, 
			2, 0, 
			5, nvl (rcmc.importe_pos_mon, rcmc.importe_pos)*-1, 
			decode ( 
				nvl (rcmc.estado, 0), 
				3, nvl (rcmc.importe_pos_mon, rcmc.importe_pos), 
				nvl (rcmc.importe_pos_mon, rcmc.importe_pos) * nvl(rcmc.tip_ope_codigo, 1)) ) ) 	"Importe Pos", 
	sum ( decode (
		rcmc.tip_mov_cc, 
			1, rcmc.importe*-1, 
			2, 0, 
			5, rcmc.importe*-1, 
			decode ( 
				nvl (rcmc.estado, 0), 
				3, rcmc.importe, 
				rcmc.importe * nvl (rcmc.tip_ope_codigo, 1)) ) ) 	"Importe", 
	rccc.sobregiro 		"Sobregiro", 
	rcmc.arancel 		"Arancel"
from 
	iposs.locales l, 
	iposs.empresas e, 
	iposs.rc_cuentas_corrientes rccc, 
	iposs.rc_movimientos_cc rcmc, 
	iposs.rc_tipos_movimientos_cc rctm
where 
	( rccc.codigo = rcmc.cue_cor_codigo )
and 	  
	( rctm.codigo = rcmc.tip_mov_cc )
and 	
	( l.codigo(+) = rcmc.loc_codigo ) 
and 
	( e.codigo = rccc.emp_codigo )
and 
	( rcmc.fecha between :FechaDesde and :FechaHasta + 1-1/24/60/60 
and 	  :FechaHasta - :FechaDesde <= 62 ) 
and 
	( nvl (rcmc.estado, 0) not in ( 2, 4, 5 ) )
group by 
	l.codigo, 
	l.loc_cod_empresa, 
	l.descripcion, 
	e.codigo,  
	e.descripcion, 
	rccc.codigo, 
	rccc.descripcion, 
	rcmc.numero_mov_cc, 
	rcmc.fecha, 
	rcmc.nro_lote, 
	rctm.descripcion, 
	rctm.codigo, 
	rcmc.estado, 
	rcmc.Compa�ia, 
	rccc.sobregiro, 
	rcmc.arancel
order by 
	rcmc.fecha asc, 
	rcmc.numero_mov_cc asc

/*

Null - equiv. 1
* 0 - Pendiente
* 1 - Confirmado
2 - Reversado
* 3 - Anulado
4 - Borrado
5 - Error


0, 'Pendiente'
1, 'Confirmado'
2, 'Reversado'
3, 'Anulado'
4, 'Borrado'
5, 'Error'

*/

