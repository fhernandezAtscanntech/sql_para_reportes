select 
	mcc.emp_codigo 		"Cod. Empresa", 
	sum (mcc.importe_comision) "Comisión"
from 
	iposs.mt_moviminteos_cc mcc
where 	decode ( mcc.tip_mov_cc,
				2, mcc.importe_comision,
				4, -1 * mcc.importe_comision,
				1, mcc.importe_comision,
				3, -1 * mcc.importe_comision,
				7, mcc.importe_comision) <> 0 ) 
and 	trunc(mcc.fecha) between :FechaDesde and :FechaHasta ) 
and 	mcc.emp_codigo not between 5000 and 5010
group by 
	mcc.emp_codigo