select 
	e.descripcion 	"Empresa", 
	e.codigo 		"Cod.Empresa", 
	t.fecha 		"Fecha", 
	sum (t.importe) 	"Importe"
from 
	iposs.empresas e, 
	iposs.mt_rc_traspasos t
where 	e.codigo = t.emp_codigo
and 	t.fecha between :FechaDesde and :FechaHasta+1-(1/24/60/60)
group by 
	e.descripcion, 
	e.codigo, 
	t.fecha
