select 
	mtmcc.descripcion 		"Tipo Movimiento", 
	sum(mcc.importe_pos) 	"Importe POS", 
	sum(mcc.importe_arancel) 	"Importe Arancel", 
	sum(mcc.importe_comision) 	"Importe Comisión", 
	sum (mcc.importe) - nvl (sum (mcc.importe_acr), 0) 	"Pendiente Pago", 
	sum (mcc.importe) 	"Importe"
from 
	iposs.mt_movimientos_cc mcc, 
	iposs.mt_tipos_movimientos_cc mtmcc
where 
	( ( mtmcc.codigo = mcc.tip_mov_cc ) ) and 
	( mcc.sello = 'PRBPS' ) and 
	( mcc.emp_codigo not between 5000 and 5010 ) and 
	( trunc (mcc.fecha) between :FechaDesde and to_date(:FechaHasta)+1-1/24/60/60 )
group by 
	mtmcc.descripcion
	