select 
	l.descripcion 		"Local", 
	mtmcc.descripcion 	"Tipo Movimiento", 
	mcc.emp_codigo 		"Cod.Empresa", 
	cc.sello 			"Sello", 
	count (mcc.numero_mov_cc) 	"Cantidad Transacciones", 
	sum (mcc.importe_acr) 		"Importe Acreditado", 
	sum (mcc.importe_acr) 		"Importe Acreditado", 
	sum (mcc.importe_pos) 		"Importe Pos", 
	sum (mcc.importe_arancel) 	"Importe Arancel", 
	sum (mcc.importe_comision) 	"Importe Comisión",  
	sum(mcc.importe) 		"Importe"
from 
	iposs.locales l, 
	iposs.mt_cuentas_corrientes cc, 
	iposs.mt_movimientos_cc mcc, 
	iposs.mt_tipos_movimientos_cc mtmcc
where 	l.codigo = mcc.loc_codigo 
and 	l.emp_codigo = mcc.emp_codigo 
and 	cc.codigo = mcc.cue_cor_codigo
and 	mtmcc.codigo = mcc.tip_mov_cc 
and 	mcc.fecha between :FechaDesde and to_date(:FechaHasta)+1-1/24/60/60
and 	mcc.emp_codigo not between 5000 and 5010
group by 
	l.descripcion, 
	mtmcc.descripcion, 
	mcc.emp_codigo, 
	cc.sello
