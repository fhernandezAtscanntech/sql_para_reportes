multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select
	'select 
		l.emp_codigo 	"Emp.Codigo", 
		l.codigo 	"Loc.Codigo", 
		r.descripcion 	"Raz�n Social", 
		l.descripcion 	"Local", 
		m.caj_codigo 	"Caja", 
		m.numero_mov 	"Nro.Movimiento", 
		m.numero_operacion 	"Nro.Operaci�n", 
		m.fecha_operacion 	"Fecha Operaci�n", 
		m.total 	"Total Ticket", 
		mimp.importe_pago 	"Importe Pago", 
		mimp.numero_tarjeta 	"TUS", 
		mimp.CI 		"CI"
	from
		iposs.movimientos m, 
		iposs.mi_movimientos_pagos mimp, 
		iposs.locales l, 
		iposs.razones_sociales r
	where 	l.raz_soc_codigo = r.codigo
	and		m.loc_codigo = l.codigo
	and 	m.emp_codigo = l.emp_codigo
	and		m.numero_mov = mimp.mov_numero_mov
	and 	m.fecha_comercial = mimp.fecha_comercial
	and 	m.emp_codigo = mimp.mov_emp_codigo
	and		m.fecha_comercial = to_date('''||:Fecha||''', '''||'dd/mm/yyyy'||''')
	and		mimp.numero_tarjeta = '''||:TUS||'''
	order by 
		m.numero_mov, 
		m.loc_codigo, 
		m.caj_codigo, 
		m.fecha_operacion, 
		m.numero_operacion
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
select 
	l.emp_codigo 	"Emp.Codigo", 
	l.codigo 	"Loc.Codigo", 
	r.descripcion 	"Raz�n Social", 
	l.descripcion 	"Local", 
	m.caj_codigo 	"Caja", 
	m.numero_mov 	"Nro.Movimiento", 
	m.numero_operacion 	"Nro.Operaci�n", 
	m.fecha_operacion 	"Fecha Operaci�n", 
	m.total 	"Total Ticket", 
	mimp.importe_pago 	"Importe Pago", 
	mimp.numero_tarjeta 	"TUS", 
	mimp.CI 		"CI"
from
	iposs.movimientos m, 
	iposs.mi_movimientos_pagos mimp, 
	iposs.locales l, 
	iposs.razones_sociales r
where
	1=2
)

######################
consulta
######################
select 1 from #reco#
