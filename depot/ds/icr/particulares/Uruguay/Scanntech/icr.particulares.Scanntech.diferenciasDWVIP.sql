multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select
		'select 
			e.codigo 	as Cod_Empresa, 
			e.descripcion 	as Empresa, 
			ece.fecha_comercial 	as Fecha_Comercial, 
			ece.audit_date 	as Fecha_Control, 
			ece.resultado 	as Resultado 
		from 
			empresas e, 
			empresas_controles ec, 
			empresas_controles_ejecuciones ece 
		where 	e.codigo = ec.emp_codigo 
		and 	ec.codigo = ece.emp_con_codigo 
		and 	resultado like '''||'ERROR%'||'''
		and 	ece.fecha_comercial = trunc(sysdate)-:Dias
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
select 
		e.codigo 	as Cod_Empresa, 
		e.descripcion 	as Empresa, 
		ece.fecha_comercial 	as Fecha_Comercial, 
		ece.audit_date 	as Fecha_Control, 
		ece.resultado 	as Resultado
	from 
		empresas e, 
		empresas_controles ec, 
		empresas_controles_ejecuciones ece 
	where 	1 = 2
	)

######################
consulta
######################
select 1 from #reco#
