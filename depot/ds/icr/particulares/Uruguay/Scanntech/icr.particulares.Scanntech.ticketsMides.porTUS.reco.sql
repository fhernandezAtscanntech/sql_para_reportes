multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select
	'select 
		l.emp_codigo 	"Emp.Codigo", 
		l.codigo 	"Loc.Codigo", 
		r.descripcion 	"Raz�n Social", 
		l.descripcion 	"Local", 
		m.caj_codigo 	"Caja", 
		m.numero_operacion 	"Nro.Operaci�n", 
		m.fecha_operacion 	"Fecha Operaci�n", 
		m.total 	"Total Ticket", 
		mimp.importe_pago 	"Importe Pago", 
		mimp.numero_tarjeta 	"TUS", 
		mimp.CI 		"CI", 
		md.linea 	"L�nea", 
		md.art_codigo 	"Art.Codigo", 
		md.descripcion 	"Art�culo", 
		md.rub_codigo 	"Rub.Codigo", 
		md.descripcion_rubro 	"Rubro", 
		decode (md.tip_det_codigo, 
			4, '||chr(39)||'VENTA'||chr(39)||', 
			5, '||chr(39)||'DEVOLUCION'||chr(39)||', 
			6, '||chr(39)||'VENTA RUBRO'||chr(39)||', 
			7, '||chr(39)||'DEVOLUCION RUBRO'||chr(39)||', 
			17, '||chr(39)||'SERVICIO'||chr(39)||', 
			18, '||chr(39)||'DEVOLUCION SERVICIO'||chr(39)||', 
			'||chr(39)||'DESCONOCIDO'||chr(39)||')	"Tipo l�nea", 
		md.cantidad 	"Cantidad", 
		md.importe 	"Importe"
	from
		iposs.movimientos m, 
		iposs.movimientos_detalles md, 
		iposs.mi_movimientos_pagos mimp, 
		iposs.locales l, 
		iposs.razones_sociales r
	where
		l.raz_soc_codigo = r.codigo
	and
		m.loc_codigo = l.codigo
	and 	m.emp_codigo = l.emp_codigo
	and 
		m.numero_mov = md.mov_numero_mov
	and 	m.fecha_comercial = md.fecha_comercial
	and 	m.emp_codigo = md.mov_emp_codigo
	and
		m.numero_mov = mimp.mov_numero_mov
	and 	m.fecha_comercial = mimp.fecha_comercial
	and 	m.emp_codigo = mimp.mov_emp_codigo
	and
		m.fecha_comercial between 
			to_date('''||:FechaDesde||''', '''||'dd/mm/yyyy'||''') and 
			to_date('''||:FechaHasta||''', '''||'dd/mm/yyyy'||''')
	and 	(to_date('''||:FechaHasta||''', '''||'dd/mm/yyyy'||''')) - (to_date('''||:FechaDesde||''', '''||'dd/mm/yyyy'||''')) <= 31
	and
		mimp.numero_tarjeta = '||chr(39)||:TUS||chr(39)||'
--	and 
--		exists 
--			(select 1
--			from iposs.funcionarios
--			where codigo = $CODIGO_FUNCIONARIO_LOGUEADO$
--			and emp_codigo = 1
--			and fecha_borrado is null
--			and login like '||chr(39)||'SC%'||chr(39)||') 
	order by 
		m.loc_codigo, 
		m.caj_codigo, 
		m.fecha_operacion, 
		m.numero_operacion, 
		md.linea
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
select 
	l.emp_codigo 	"Emp.Codigo", 
	l.codigo 	"Loc.Codigo", 
	r.descripcion 	"Raz�n Social", 
	l.descripcion 	"Local", 
	m.caj_codigo 	"Caja", 
	m.numero_operacion 	"Nro.Operaci�n", 
	m.fecha_operacion 	"Fecha Operaci�n", 
	m.total 	"Total Ticket", 
	mimp.importe_pago 	"Importe Pago", 
	mimp.numero_tarjeta 	"TUS", 
	mimp.CI 		"CI", 
	md.linea 	"L�nea", 
	md.art_codigo 	"Art.Codigo", 
	md.descripcion 	"Art�culo", 
	md.rub_codigo 	"Rub.Codigo", 
	md.descripcion_rubro 	"Rubro", 
	decode (md.tip_det_codigo, 
		4, 'VENTA', 
		5, 'DEVOLUCION', 
		6, 'VENTA RUBRO', 
		7, 'DEVOLUCION RUBRO',  
		17, 'SERVICIO', 
		18, 'DEVOLUCION SERVICIO', 
		'DESCONOCIDO')	"Tipo l�nea", 
	md.cantidad 	"Cantidad", 
	md.importe 	"Importe"
from
	iposs.movimientos m, 
	iposs.movimientos_detalles md, 
	iposs.mi_movimientos_pagos mimp, 
	iposs.locales l, 
	iposs.razones_sociales r
where
	1=2
)

######################
consulta
######################
select 1 from #reco#
