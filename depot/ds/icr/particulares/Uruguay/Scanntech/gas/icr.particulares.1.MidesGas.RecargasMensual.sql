select 
	mcc.cedula		"Cedula", 
	round(mcc.monto) 	"Monto",
	mcc.numero_autorizacion "Autorizaci�n",
	mcc.audit_date 		"Fecha",
	mcc.audit_user		"Usuario", 
	e.descripcion 		"Distribuidora"
from 
	mi_movimientos_cc mcc, 
	funcionarios f, 
	empresas e
where 
	f.emp_codigo = e.codigo
and 	
	mcc.audit_user = f.login
and 	
	mcc.mi_tip_mov_cc = 1 
and 	
	mcc.reversada = 0
and 		-- primer d�a del mes pasado
	mcc.audit_date >= trunc (to_date ('01/'||to_char (add_months (sysdate, -1), 'mm/yyyy')))
and 		-- �ltimo d�a del mes pasado, al �ltimo segundo
	mcc.audit_date <= (trunc (last_day (add_months (sysdate, -1))) + 1) - (1/24/60/60) 
and 	
	mcc.cedula != '12345678'
order by 
	mcc.audit_date
