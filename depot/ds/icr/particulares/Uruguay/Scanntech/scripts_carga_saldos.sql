declare								
	p_fechaAProcesar	date	:= to_date('&1', 'ddmmyyyy');
	v_fechaSaldoMasReciente 	date;
	v_saldoMasReciente 	iposs.rc_cuentas_corrientes.saldo%type;
	v_acumPeriodo 	iposs.rc_cuentas_corrientes.saldo%type;
begin								
	for cc in (select * from rc_cuentas_corrientes where audit_date < p_fechaAProcesar) 
	loop
		-- busco un saldo más reciente
		begin
			select fecha, saldo
			into v_fechaSaldoMasReciente, v_saldoMasReciente
			from iposs.rc_saldos_mensuales
			where emp_codigo = cc.emp_codigo
			and cue_cor_codigo = cc.codigo
			and fecha = 
				(select min(fecha) 
				from iposs.rc_saldos_mensuales 
				where emp_codigo = cc.emp_codigo 
				and cue_cor_codigo = cc.codigo 
				and fecha >= p_fechaAProcesar);
		exception
		when no_data_found then
		-- no hay saldos posteriores
			v_fechaSaldoMasReciente := sysdate;
			v_saldoMasReciente := cc.saldo;
		end;
		
		begin 					
			select 				
				nvl (sum ( decode (			
					rcmc.tip_mov_cc, 		
						1, 0, 	
						2, -rcmc.importe, 	
						3, rcmc.importe, 	
						4, rcmc.importe, 	
						5, -rcmc.importe, 	
						7, rcmc.importe, 	
						- rcmc.importe) ), 0)
			into v_acumPeriodo				
			from iposs.rc_movimientos_cc rcmc, iposs.sa_recargas sar				
			where rcmc.cue_cor_codigo = cc.codigo				
			and rcmc.emp_codigo = cc.emp_codigo				
			and rcmc.tip_mov_cc in (2, 3, 4, 5, 7, 10) 				
			and nvl (rcmc.estado, 0) not in ( 2, 3, 4, 5 ) 				
			and rcmc.con_mov_codigo = sar.con_mov_caj_codigo (+)				
			and nvl (sar.recargada, 1) in (1, 10, 11, 20, 21)				
			and rcmc.fecha >= p_fechaAProcesar and rcmc.fecha < v_fechaSaldoMasReciente;				
		exception 					
		when no_data_found then 					
			v_acumPeriodo := 0; 				
		end; 					

		begin
			insert into iposs.rc_saldos_mensuales 
				(emp_codigo, cue_cor_codigo, fecha, saldo) 
			values 
				(cc.emp_codigo, cc.codigo, p_fechaAProcesar, (v_saldoMasReciente - v_acumPeriodo));
		exception
		when others then
			insert into iposs.rc_saldos_mensuales_logs
				(emp_codigo, cue_cor_codigo, fecha, texto) 
			values
				(cc.emp_codigo, cc.codigo, p_fechaAProcesar, 'ERROR');
		end;
		commit;
	end loop;
end;
/
