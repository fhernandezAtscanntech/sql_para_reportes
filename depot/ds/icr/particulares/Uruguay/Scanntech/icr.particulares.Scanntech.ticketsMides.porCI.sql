select 
	l.emp_codigo 	"Emp.Codigo", 
	l.codigo 	"Loc.Codigo", 
	r.descripcion 	"Raz�n Social", 
	l.descripcion 	"Local", 
	m.caj_codigo 	"Caja", 
	m.numero_operacion 	"Nro.Operaci�n", 
	m.fecha_operacion 	"Fecha Operaci�n", 
	m.total 	"Total Ticket", 
	mimp.importe_pago 	"Importe Pago", 
	mimp.numero_tarjeta 	"TUS", 
	mimp.CI 		"CI", 
	md.linea 	"L�nea", 
	md.art_codigo 	"Art.Codigo", 
	md.descripcion 	"Art�culo", 
	md.rub_codigo 	"Rub.Codigo", 
	md.descripcion_rubro 	"Rubro", 
	decode (md.tip_det_codigo, 
		4, 'VENTA', 
		5, 'DEVOLUCION', 
		6, 'VENTA RUBRO', 
		7, 'DEVOLUCION RUBRO', 
		17, 'SERVICIO', 
		18, 'DEVOLUCION SERVICIO', 
		'DESCONOCIDO')	"Tipo l�nea", 
	md.cantidad 	"Cantidad", 
	md.importe 	"Importe"
from
	iposs.movimientos m, 
	iposs.movimientos_detalles md, 
	iposs.mi_movimientos_pagos mimp, 
	iposs.locales l, 
	iposs.razones_sociales r
where
	l.raz_soc_codigo = r.codigo
and
	m.loc_codigo = l.codigo
and 	m.emp_codigo = l.emp_codigo
and 
	m.numero_mov = md.mov_numero_mov
and 	m.fecha_comercial = md.fecha_comercial
and 	m.emp_codigo = md.mov_emp_codigo
and
	m.numero_mov = mimp.mov_numero_mov
and 	m.fecha_comercial = mimp.fecha_comercial
and 	m.emp_codigo = mimp.mov_emp_codigo
and
	m.fecha_comercial between :FechaDesde and :FechaHasta
and 	:FechaHasta - :FechaDesde <= 31
and
	mimp.ci = :CI
and 
	exists 
		(select 1
		from iposs.funcionarios
		where codigo = $CODIGO_FUNCIONARIO_LOGUEADO$
		and emp_codigo = 1
		and fecha_borrado is null
		and login like 'SC%') 
order by 
	m.loc_codigo, 
	m.caj_codigo, 
	m.fecha_operacion, 
	m.numero_operacion, 
	md.linea