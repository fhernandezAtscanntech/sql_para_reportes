multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select
	'select 
		cuenta, 
		factura, 
		importe_total, 
		importe_mides, 
		codigo_agencia, 
		fecha_estado, 
		id_confirmacion 
	from 
		rc_movimientos_cc rmc 
	join 	ute_pagos_factura upf 
	on 	rmc.con_mov_codigo = upf.con_mov_caj_codigo 
	where 
		COMPA�IA = '||chr(39)||'UTE'||chr(39)||'
	and 
		estado = 1 
	and 		-- primer d�a del mes pasado
		rmc.fecha_estado >= trunc (to_date ('||chr(39)||'01/'||chr(39)||'||to_char (add_months (sysdate, -1), '||chr(39)||'mm/yyyy'||chr(39)||')))
	and 		-- �ltimo d�a del mes pasado, al �ltimo segundo
		rmc.fecha_estado < (trunc (last_day (add_months (sysdate, -1))) + 1) - (1/24/60/60) 
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
	select 
		cuenta, 
		factura, 
		importe_total, 
		importe_mides, 
		codigo_agencia, 
		fecha_estado, 
		id_confirmacion 
	from 
		rc_movimientos_cc rmc 
	join 	ute_pagos_factura upf 
	on 	rmc.con_mov_codigo = upf.con_mov_caj_codigo 
	where 
		1 = 2
)


######################
consulta
######################
select 1 from #rec#