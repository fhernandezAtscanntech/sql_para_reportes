multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select
	'select 	
		trunc(rmc.fecha_estado) Fecha_Estado, 
		count(*) cantidad	,  
		sum(upf.importe_total) importe_total, 
		sum(upf.importe_mides) monto_subsidio, 
		sum(rmc.importe_pos) cobrado_efectivo 
	from 
		rc_movimientos_cc rmc 
	join 	ute_pagos_factura upf 
	on 	rmc.con_mov_codigo = upf.con_mov_caj_codigo 
	where 
		rmc.compa�ia = '||chr(39)||'UTE'||chr(39)||'
	and 
		rmc.estado = 1 
	and 
		rmc.fecha_estado >= trunc(sysdate - 1) 
	and 
		rmc.fecha_estado < trunc(sysdate) 
	group by trunc(rmc.fecha_estado)
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
	select 	
		trunc(rmc.fecha_estado) Fecha_Estado, 
		count(*) cantidad	,  
		sum(upf.importe_total) importe_total, 
		sum(upf.importe_mides) monto_subsidio, 
		sum(rmc.importe_pos) cobrado_efectivo 
	from 
		rc_movimientos_cc rmc 
	join 	ute_pagos_factura upf 
	on 	rmc.con_mov_codigo = upf.con_mov_caj_codigo 
	where 
		1 = 2
	group by trunc (rmc.fecha_estado)
)


######################
consulta
######################
select 1 from #rec#