multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select
	'select 
		cmc.EMP_CODIGO 	"Emp.Codigo", 
		cmc.LOC_CODIGO 	"Loc.Codigo", 
		cmc.CAJ_CODIGO 	"Caj.Codigo", 
		com.serial_number 	"Nro.Serial", 
		com.order_id 		"Id Orden", 
		cmc.fecha_pos 		"Fecha Pos", 
		cmc.importe_pos 	"Importe Pos"
	from 
		psc_compras com 
	JOIN 	rc_con_mov_caja cmc on com.CON_MOV_CAJ_CODIGO = cmc.CON_MOV_CODIGO
	where 
		com.tipo_tran = '||chr(39)||'DELIVERED'||chr(39)||'
	and 
		cmc.fecha_pos between trunc(sysdate) - :Dias and trunc(sysdate)
	' as sql from dual
)

/*
-- no puedo usar parámetros de fecha porque se sustituyen por to_date de strings y aparecen comillas sencillas
	and 
		cmc.fecha_pos >= :FechaDesde
	and 
		cmc.fecha_pos <= :FechaHasta
*/


######################
tabla
######################

create table $$tabla$$ as (
select 
		cmc.EMP_CODIGO 	"Emp.Codigo", 
		cmc.LOC_CODIGO 	"Loc.Codigo", 
		cmc.CAJ_CODIGO 	"Caj.Codigo", 
		com.serial_number 	"Nro.Serial", 
		com.order_id 	"Id Orden", 
		cmc.fecha_pos 	"Fecha Pos", 
		cmc.importe_pos 	"Importe Pos"
	from 
		psc_compras com 
	JOIN 	rc_con_mov_caja cmc on com.CON_MOV_CAJ_CODIGO = cmc.CON_MOV_CODIGO
	where 
		1 = 2
)


######################
consulta
######################
select 1 from #rec#