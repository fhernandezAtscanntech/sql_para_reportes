select 
	m.numero_mov 		"Numero mov.", 
	m.fecha_comercial 	"Fecha Comercial", 
	m.emp_codigo 		"Cod.Empresa", 
	m.loc_codigo 		"Cod.Local", 
	m.caj_codigo 		"Caja", 
	m.numero_operacion 	"Nro. Operaci�n", 
	m.fecha_operacion 	"Fecha Operaci�n", 
	mda.linea 			"L�nea", 
	mda.campo 			"Campo", 
	mda.valor 			"Valor"
from 
	iposs.movimientos m, 
	iposs.movimientos_datos_asociados mda
where	m.emp_codigo = mda.mov_emp_codigo
and 	m.fecha_comercial = mda.fecha_comercial
and 	m.numero_mov = mda.mov_numero_mov
and 	m.fecha_comercial = :Fecha
and 	(trim (upper (:Empresa)) = 'NULL' or (m.emp_codigo in :Empresa))
and 	(trim (upper (:Local)) = 'NULL' or (m.loc_Codigo in :Local))
