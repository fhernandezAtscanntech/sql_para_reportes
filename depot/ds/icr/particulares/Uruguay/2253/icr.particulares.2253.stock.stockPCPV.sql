select 
	ae.art_codigo 	"Art.Codigo(I)", 
	ae.art_codigo_externo 	"Art.Codigo", 
	ae.descripcion 	"Articulo", 
	r.descripcion 	"Rubro", 
	(select cb.codigo_barra 
		from iposs.codigos_barras cb 
		where cb.art_codigo = ae.art_codigo 
		and (cb.emp_Codigo = ae.emp_codigo or (cb.emp_codigo = 1 and ae.veo_barras_modelo = 1)) 
		and rownum = 1) "Codigo Barra", 
	d.codigo 	"Cod.Deposito", 
	d.descripcion 	"Deposito", 
	l.codigo 	"Loc.Codigo(I)", 
	l.loc_Cod_empresa 	"Loc.Codigo", 
	l.descripcion 	"Local", 
	sum(s.stock) 	"Stock", 
	(select decode (pc.mon_codigo, 
					(select valor from parametros where codigo = 'MONEDA_DEF'), pc.costo_imp, 
					pc.costo_imp * (select c.valor 
									from cambios c 
									where c.mon_codigo = pc.mon_codigo
									and :FechaStock between c.fecha and c.vigencia_hasta
									and c.emp_codigo = $EMPRESA_LOGUEADA$))
		from precios_de_costos_articulos pc
		where pc.loc_codigo = l.codigo
		and pc.art_codigo = ae.art_codigo
		and :FechaStock between pc.vigencia_desde and pc.vigencia_hasta
		and pc.emp_codigo = $EMPRESA_LOGUEADA$ 
		and rownum = 1 ) 	"Costo S.Imp.", 	
	(select decode (pc.mon_codigo, 
						(select valor from parametros where codigo = 'MONEDA_DEF'), pc.costo_ultimo,  
						pc.costo_ultimo * (select c.valor 
											from cambios c 
											where c.mon_codigo = pc.mon_codigo
											and :FechaStock between c.fecha and c.vigencia_hasta
											and c.emp_codigo = $EMPRESA_LOGUEADA$))
		from precios_de_costos_articulos pc
		where pc.loc_codigo = l.codigo
		and pc.art_codigo = ae.art_codigo
		and :FechaStock between pc.vigencia_desde and pc.vigencia_hasta
		and pc.emp_codigo = $EMPRESA_LOGUEADA$ 
		and rownum = 1 ) 	"Costo c.Imp.", 
	(select decode (pv.mon_codigo, 
						(select  valor from parametros where codigo = 'MONEDA_DEF'), pv.precio, 
						pv.precio * (select c.valor 
									from cambios c 
									where c.mon_codigo = pv.mon_codigo
									and :FechaStock between c.fecha and c.vigencia_hasta
									and c.emp_codigo = $EMPRESA_LOGUEADA$))
		from precios_de_articulos_ventas pv, listas_de_precios_ventas lp, listas_pre_ventas_locales lpl
		where pv.loc_lis_loc_codigo = lpl.loc_codigo
		and pv.loc_lis_lis_pre_ve_codigo = lpl.lis_pre_ve_codigo
		and lp.codigo = (select valor from parametros_empresas where par_codigo = 'LIS_PRE_VE_DEF' and emp_codigo = $EMPRESA_LOGUEADA$)
		and lp.codigo = lpl.lis_pre_ve_codigo
		and lp.emp_codigo = $EMPRESA_LOGUEADA$
		and lpl.loc_codigo = al.loc_codigo
		and pv.art_codigo = al.art_codigo
		and pv.emp_codigo = $EMPRESA_LOGUEADA$
		and :FechaStock between pv.vigencia and pv.vigencia_hasta) "Venta"
from 
	iposs.articulos_empresas ae, 
	iposs.articulos_locales al, 
	iposs.rubros r, 
	iposs.depositos d, 
	iposs.locales l, 
	iposs.stocks s
where 
	-- Transcripto de Discoverer
	( ( d.loc_codigo = s.dep_loc_codigo and 
	    d.codigo = s.dep_codigo ) and 
	  ( l.codigo = d.loc_codigo ) and 
	  ( s.art_codigo = ae.art_codigo ) and 
	  ( ae.emp_codigo = al.emp_codigo and 
	    ae.art_codigo = al.art_codigo ) and 
	  ( al.art_codigo = s.art_codigo and 
	    al.dim_con_numero = s.dim_con_numero and 
	    al.loc_codigo = s.dep_loc_codigo ) and 
	  ( l.codigo = al.loc_codigo ) and 
	  ( r.codigo = ae.rub_codigo ) ) and
	( ae.emp_codigo <> 1 ) and
-- 	( ae.est_codigo <> 2 ) and 
-- 	( al.fecha_borrado is null ) and
	( al.fecha_borrado is null or ( al.fecha_borrado is not null and s.stock <> 0 )) and
	( l.codigo in :Local ) and 
	( trim (upper (':Rubro')) = 'NULL' or r.codigo in :Rubro ) and 
	( :FechaStock between s.fecha and s.vigencia_hasta ) and
	ae.emp_codigo = $EMPRESA_LOGUEADA$ and
	al.emp_codigo = $EMPRESA_LOGUEADA$ and
	l.emp_codigo = $EMPRESA_LOGUEADA$
group by 
	ae.art_codigo, 
	ae.art_codigo_externo, 
	ae.descripcion, 
	ae.emp_codigo, 
	ae.veo_barras_modelo, 
	al.loc_codigo, 
	al.art_codigo, 
	r.descripcion, 
	d.codigo, 
	d.descripcion, 
	l.codigo, 
	l.loc_Cod_empresa, 
	l.descripcion 
