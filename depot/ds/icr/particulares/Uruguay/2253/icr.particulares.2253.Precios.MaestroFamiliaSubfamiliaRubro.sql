select
	l.codigo "Cod.Local (I)",
	l.loc_cod_empresa "Cod.Local",
	l.descripcion "Local",
	p.razon_social "Proveedor",
	lis.descripcion "Lista precios",
	al.art_codigo_Externo "Cod.Artículo",
	ae.descripcion "Artículo",
	r.descripcion "Rubro",
	f.descripcion "Familia",
	(select s.descripcion from iposs.subfamilias s where s.fam_codigo = ae.fam_codigo 	and s.codigo = ae.sub_codigo) "SubFamilia",
	(select i.descripcion from iposs.ivas i where i.codigo = ae.iva_codigo) "IVA Compra",
	(select i.descripcion from iposs.ivas i where i.codigo = nvl (ae.iva_vta_codigo, ae.iva_codigo)) "IVA Venta",
	nvl ((select cb.codigo_barra from iposs.codigos_barras cb where cb.art_codigo = ae.art_codigo and (cb.emp_codigo = ae.emp_codigo or (cb.emp_codigo = 1 and ae.veo_barras_modelo = 1)) and rownum = 1), '') "Cod.Barra",
	(select pv.precio
		from iposs.precios_de_articulos_ventas pv
		where pv.art_codigo = al.art_codigo
		and pv.loc_lis_lis_pre_ve_codigo = lis.codigo
		and pv.loc_lis_loc_codigo = al.loc_codigo
		and pv.dim_con_numero = 0
		and nvl(:Fecha, sysdate) between pv.vigencia and pv.vigencia_hasta
		and pv.vigencia_hasta > nvl(:Fecha, sysdate)
		and pv.emp_codigo = $EMPRESA_LOGUEADA$
		and rownum = 1 ) "Precio Venta",
	(select pc.precio_sin_imp
		from iposs.precios_de_compras_articulos pc
		where pc.prov_art_art_codigo = al.art_codigo
		and pc.loc_codigo = al.loc_codigo
		and pc.prov_art_prov_Codigo = p.codigo
		and pc.prov_art_dim_con_numero = 0
		and nvl(:Fecha, sysdate) between pc.vigencia and pc.vigencia_hasta
		and pc.vigencia_hasta > nvl(:Fecha, sysdate)
		and pc.prov_art_emp_codigo = $EMPRESA_LOGUEADA$
		and rownum = 1) "Precio Compra s.Imp",
	(select pc.costo_imp
		from iposs.precios_de_costos_articulos pc
		where pc.art_codigo = al.art_codigo
		and pc.loc_codigo = al.loc_codigo
		and pc.dim_con_numero = 0
		and nvl(:Fecha, sysdate) between pc.vigencia_desde and pc.vigencia_hasta
		and pc.vigencia_hasta > nvl(:Fecha, sysdate)
		and pc.emp_codigo = $EMPRESA_LOGUEADA$
		and rownum = 1) "Precio Costo",
	(select alpd.margen_ganancia
		from iposs.artlocprov_descuentos alpd
		where alpd.emp_codigo = $EMPRESA_LOGUEADA$
		and alpd.art_loc_pr_art_codigo = al.art_codigo
		and alpd.art_loc_pr_prov_Codigo = p.codigo
		and alpd.art_loc_pr_loc_codigo = al.loc_codigo
		and alpd.loc_lis_lis_pre_ve_codigo = lis.codigo
		and alpd.art_loc_pr_dim_con_numero = 0
		and nvl(:Fecha, sysdate) between alpd.vigencia and alpd.vigencia_hasta
		and rownum = 1) "Margen Ganancia"
from
	iposs.locales l,
	iposs.proveedores p,
	iposs.articulos_locales al,
	iposs.articulos_empresas ae,
	iposs.rubros r,
	iposs.familias f, 
	iposs.proveedores_empresas pe,
	iposs.proveedores_articulos pa,
	iposs.articulos_locales_proveedores alp,
	iposs.listas_de_precios_ventas lis,
	iposs.listas_pre_ventas_locales lisl
where ae.art_codigo = al.art_Codigo
and ae.emp_codigo = al.emp_codigo
and al.loc_Codigo = l.codigo
and ae.art_codigo = pa.art_codigo
and ae.emp_codigo = pe.emp_codigo
and al.art_codigo = alp.art_loc_art_codigo
and al.loc_codigo = alp.art_loc_loc_codigo
and al.emp_codigo = alp.emp_codigo
and pa.prov_Codigo = pe.prov_codigo
and pa.emp_codigo = pe.emp_codigo
and alp.prov_codigo = pe.prov_codigo
and alp.emp_codigo = pe.emp_codigo
and ae.rub_codigo = r.codigo
and ae.fam_codigo = f.codigo 
and pe.prov_codigo = p.codigo
and lis.codigo = lisl.lis_pre_ve_codigo
and l.codigo = lisl.loc_Codigo
and al.fecha_borrado is null
and pa.fecha_borrado is null
and alp.fecha_borrado is null
and ae.fecha_borrado is null
and pe.fecha_borrado is null
and (f.codigo in :Familia )
and (trim(upper(':Rubro'))='NULL' or r.codigo in :Rubro )
and (trim(upper(':Local'))='NULL' or l.codigo in :Local )
and (ae.emp_codigo=$EMPRESA_LOGUEADA$
and al.emp_codigo=$EMPRESA_LOGUEADA$
and pe.emp_codigo=$EMPRESA_LOGUEADA$
and l.emp_codigo=$EMPRESA_LOGUEADA$
and pa.emp_codigo=$EMPRESA_LOGUEADA$
and alp.emp_codigo=$EMPRESA_LOGUEADA$
and lis.emp_codigo=$EMPRESA_LOGUEADA$)
