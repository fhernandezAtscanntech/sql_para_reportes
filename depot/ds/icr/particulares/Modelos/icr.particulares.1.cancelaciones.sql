select 
	m.emp_codigo 		"Emp.Codigo", 
	m.loc_codigo 		"Loc.Codigo", 
	m.caj_codigo 		"Caja", 
	m.fecha_comercial 	"Fecha Comercial", 
	m.observacion 		"Observación",
	dp.config 		"Configuración", 
	count(*) 		"Cantidad"
from 
	movimientos m, 
	distribuciones_pendientes dp
where 
	tipo_operacion = 'VENTA ANULADA'
and 
	m.emp_codigo = dp.emp_codigo
and 	m.loc_codigo = dp.loc_codigo
and 	m.caj_codigo = dp.caj_codigo
and 
	( m.fecha_comercial between :FechaDesde and :FechaHasta 
and 	  :FechaHasta - :FechaDesde <= 31 )
and 
	exists 
		(select 1
		from iposs.funcionarios
		where codigo = $CODIGO_FUNCIONARIO_LOGUEADO$
		and emp_codigo = 1
		and fecha_borrado is null
		and login like 'SC%') 
group by 
	m.emp_codigo, 
	m.loc_codigo, 
	m.caj_codigo, 
	fecha_comercial, 
	observacion, 
	config
