select 
	l.descripcion 		"Local", 
	l.codigo 			"Cod.Local", 
	l.emp_codigo 		"Emp.Codigo", 
	e.descripcion 		"Empresa", 
	m.caj_codigo 		"Cod.Caja", 
	m.fecha_comercial 	"Fecha", 
	m.fecha_operacion 	"Fecha operación", 
	m.numero_operacion 	"Nro.Operación", 
	m.observacion 		"Observación", 
	m.total 			"Total Ticket", 
	coalesce (d.linea, s.linea) 	"Linea Det.",
	coalesce (d.art_codigo, to_char(d.rub_codigo), s.art_codigo, to_char(s.ser_codigo))  "Codigo art-rub-ser",
	coalesce (d.descripcion, d.descripcion_rubro, s.descripcion, 'Servicio') "Artículo-rubro-servicio",
	coalesce (td1.descripcion, td2.descripcion)   "Tipo Detalle",
	coalesce (d.cantidad, s.cantidad)    "Cantidad",
	coalesce (d.importe, s.importe)    "Importe"
from 
	iposs.locales l, 
	iposs.empresas e, 
	IPOSS.movimientos m, 
	IPOSS.movimientos_detalles d, 
	iposs.movimientos_det_servs s, 
	iposs.tipos_de_Detalles td1, 
	iposs.tipos_de_Detalles td2
where 	( m.numero_mov = d.mov_numero_mov (+)
and 	  m.fecha_comercial = d.fecha_comercial (+)
and 	  m.emp_codigo = d.mov_emp_codigo (+) )
and 
		( m.numero_mov = s.mov_numero_mov (+)
and 	  m.fecha_comercial = s.fecha_comercial (+)
and 	  m.emp_codigo = s.mov_emp_codigo (+) )
and 
		( l.codigo = m.loc_codigo 
and 	  l.emp_codigo = m.emp_codigo )
and 	( e.codigo = m.emp_codigo )
and 
		( m.tipo_operacion = 'VENTA ANULADA' )
and  
		( d.tip_Det_codigo = td1.codigo 
and 	  s.tip_Det_codigo = td2.codigo  )
and 
		( m.fecha_comercial between :FechaDesde and :FechaHasta 
and 	  :FechaHasta - :FechaDesde <= 31 )
and 	l.codigo in :Local
and 	( m.emp_codigo = $EMPRESA_LOGUEADA$
and 	  e.codigo = $EMPRESA_LOGUEADA$
and 	  l.emp_codigo = $EMPRESA_LOGUEADA$ )

################################################################################################################
################################################################################################################
################################################################################################################
################################################################################################################
################################################################################################################
################################################################################################################
################################################################################################################

-- Segunda opción

select 
	l.descripcion 		"Local", 
	l.codigo 			"Cod.Local", 
	l.emp_codigo 		"Emp.Codigo", 
	e.descripcion 		"Empresa", 
	m.caj_codigo 		"Cod.Caja", 
	m.fecha_comercial 	"Fecha", 
	m.fecha_operacion 	"Fecha operación", 
	m.numero_operacion 	"Nro.Operación", 
	m.observacion 		"Observación", 
	m.total 			"Total Ticket", 
	d.linea 			"Linea Det.", 
	coalesce (d.art_codigo, to_char(d.rub_codigo))  "Codigo art-rub-ser",
	coalesce (d.descripcion, d.descripcion_rubro) "Artículo-rubro-servicio",
	td1.descripcion   "Tipo Detalle",
	d.cantidad    "Cantidad",
	d.importe    "Importe"
from 
	iposs.locales l, 
	iposs.empresas e, 
	IPOSS.movimientos m 
		left join IPOSS.movimientos_detalles d 
		on m.numero_mov = d.mov_numero_mov 
		and m.fecha_comercial = d.fecha_comercial 
		and m.emp_codigo = d.mov_emp_codigo, 
	iposs.tipos_de_Detalles td1
where 	
		( l.codigo = m.loc_codigo 
and 	  l.emp_codigo = m.emp_codigo )
and 	( e.codigo = m.emp_codigo )
and 
		( m.tipo_operacion = 'VENTA ANULADA' )
and  
		( d.tip_Det_codigo = td1.codigo )
and 
		( m.fecha_comercial between :FechaDesde and :FechaHasta 
and 	  :FechaHasta - :FechaDesde <= 31 )
and 	l.codigo in :Local
and 	d.linea is not null
and 	( m.emp_codigo = $EMPRESA_LOGUEADA$
and 	  e.codigo = $EMPRESA_LOGUEADA$
and 	  l.emp_codigo = $EMPRESA_LOGUEADA$ )
UNION
select 
	l.descripcion 		"Local", 
	l.codigo 			"Cod.Local", 
	l.emp_codigo 		"Emp.Codigo", 
	e.descripcion 		"Empresa", 
	m.caj_codigo 		"Cod.Caja", 
	m.fecha_comercial 	"Fecha", 
	m.fecha_operacion 	"Fecha operación", 
	m.numero_operacion 	"Nro.Operación", 
	m.observacion 		"Observación", 
	m.total 			"Total Ticket", 
	s.linea 			"Linea Det.", 
	coalesce (s.art_codigo, to_char(s.ser_codigo))  "Codigo art-rub-ser",
	coalesce (s.descripcion, 'Servicio') "Artículo-rubro-servicio",
	td2.descripcion   "Tipo Detalle",
	s.cantidad    "Cantidad",
	s.importe    "Importe"
from 
	iposs.locales l, 
	iposs.empresas e, 
	IPOSS.movimientos m 
		left join iposs.movimientos_det_servs s 
		on m.numero_mov = s.mov_numero_mov 
		and m.fecha_comercial = s.fecha_comercial 
		and m.emp_codigo = s.mov_emp_codigo, 
	iposs.tipos_de_Detalles td2
where 	
		( l.codigo = m.loc_codigo 
and 	  l.emp_codigo = m.emp_codigo )
and 	( e.codigo = m.emp_codigo )
and 
		( m.tipo_operacion = 'VENTA ANULADA' )
and  
		( s.tip_Det_codigo = td2.codigo )
and 
		( m.fecha_comercial between :FechaDesde and :FechaHasta 
and 	  :FechaHasta - :FechaDesde <= 31 )
and 	l.codigo in :Local
and 	s.linea is not null 
and 	( m.emp_codigo = $EMPRESA_LOGUEADA$
and 	  e.codigo = $EMPRESA_LOGUEADA$
and 	  l.emp_codigo = $EMPRESA_LOGUEADA$ )
