select 
	e.codigo 		"Cod.Empresa", 
	l.codigo 		"Cod.Local", 
	e.descripcion 	"Empresa", 
	l.descripcion 	"Local", 
	m.caj_codigo 	"Caja", 
	m.fecha_comercial 	"Fecha Comercial", 
	m.numero_operacion 	"Nro.Operación" ,
	m.fecha_operacion 	"Fecha Operación", 
	m.total 		"Total Ticket", 
	m.resultado 	"Cod.Resultado", 
	r.descripcion 	"Encontrado", 
	cajero.login 	"Cajero", 
	autoriza.login 	"Autoriza"
from 
	iposs.aud_tickets_negativos m
	join iposs.empresas e
		on e.codigo = m.emp_codigo
	join iposs.locales l 
		on l.codigo = m.loc_codigo
	join iposs.aud_tickneg_resultados r
		on r.codigo = bitand (m.resultado, r.codigo)
	join iposs.movimientos mo
		on m.numero_mov = mo.numero_mov
		and m.emp_codigo = mo.emp_codigo
		and m.fecha_comercial = mo.fecha_comercial
	join iposs.funcionarios cajero
		on mo.fun_codigo_cajera = cajero.fun_cod_empresa
		and mo.emp_codigo = cajero.emp_codigo
	left join iposs.funcionarios autoriza
		on mo.fun_codigo_autoriza = autoriza.fun_cod_empresa
		and mo.emp_codigo = autoriza.emp_codigo
where 	m.resultado <> 0
and 	( trim (upper(':Local')) = 'NULL' or l.codigo in :Local )
and 	m.fecha_comercial between :FechaDesde and :FechaHasta
and 	:FechaHasta - :FechaDesde < 31
