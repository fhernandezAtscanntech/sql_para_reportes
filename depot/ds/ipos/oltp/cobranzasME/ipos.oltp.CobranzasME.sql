select 
	l.descripcion 		"Local", 
	l.loc_cod_empresa 	"Cod.Local", 
	m.caj_codigo 		"Cod.caja", 
	c.descripcion 		"Caja", 
	m.fecha_comercial 	"Fecha Comercial", 
	m.fecha_operacion 	"Fecha Operación", 
	m.numero_operacion 	"Nro.Operacion", 
	m.total 			"Total Ticket", 
	mp.importe 			"Importe", 
	mp.importe_pago 	"Importe pago", 
	mo.simbolo 			"Moneda", 
	mp.cotiza_compra 	"Cotización compra"
from 
	iposs.movimientos m, 
	iposs.movimientos_de_pagos mp, 
	iposs.locales l, 
	iposs.cajas c, 
	iposs.monedas mo
where 	m.numero_mov = mp.mov_numero_mov
and 	m.emp_codigo = mp.mov_emp_codigo
and 	m.fecha_comercial = mp.fecha_comercial
and 	m.loc_codigo = l.codigo
and 	m.caj_codigo = c.codigo
and 	m.loc_codigo = c.loc_codigo
and 	mp.mon_codigo = mo.codigo
and 	mp.mon_codigo <> (select valor from iposs.parametros where codigo = 'MONEDA_DEF')
and 	m.fecha_comercial between :FechaDesde and :FechaHasta
and 	:FechaHasta - :FechaDesde <= 31
and 	m.loc_codigo in :Local
and 	
		m.emp_codigo = $EMPRESA_LOGUEADA$
and 	mp.mov_emp_codigo = $EMPRESA_LOGUEADA$
and 	l.emp_codigo = $EMPRESA_LOGUEADA$
