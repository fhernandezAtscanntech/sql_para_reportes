
/* REP-611 / IDC-512
 * Piden fecha, receptor: ruc, razón social, serie número, tipo de comprobante.
 * 
 * SQL> desc comprobantes_borrados
 * Nombre                                    ¿Nulo?   Tipo
 * ----------------------------------------- -------- ----------------------------
 * CODIGO                                             NUMBER(30)
 * EMP_CODIGO                                         NUMBER(5)
 * RAZ_SOC_CODIGO                                     NUMBER(5)
 * PROV_CODIGO                                        NUMBER(9)
 * TIP_DOC_IN_CODIGO                                  VARCHAR2(2)
 * SERIE                                              VARCHAR2(2)
 * NUMERO                                             NUMBER(10)
 * FECHA_EMISION                                      DATE
 * EST_CODIGO                                         NUMBER(5)
 * AUDIT_USER                                         VARCHAR2(30)
 * AUDIT_DATE                                         DATE
 * LOC_CODIGO                                         NUMBER(5)
 * DEP_CODIGO                                         NUMBER(5)
 *
 * La tabla no tienen ninguna constraint, ni fk, ni nada
 * Supongo que los códigos refieren a las mismas fk de comprobantes
 * EMP_CODIGO		 -- empresas
 * RAZ_SOC_CODIGO 	-- razones sociales
 * PROV_CODIGO 		-- proveedores
 * TIP_DOC_IN_CODIGO 	-- tipos documentos inventarios
 * LOC_CODIGO 		-- locales / depositos
 * DEP_CODIGO       -- depositos
 
*/

select 
	cb.fecha_emision 	"Fecha Emisión", 
	cb.audit_date 		"Fecha Borrado",  
	p.ruc 				"Prov. Ruc", 
	p.razon_social 		"Proveedor", 
	r.descripcion 		"Razón Social", 
	cb.serie 			"Serie", 
	cb.numero 			"Numero", 
	tdi.descripcion 	"Tipo Doc.", 
	l.descripcion 		"Local", 
	d.descripcion 		"Depósito", 
	e.descripcion 		"Estado"
from 
	iposs.comprobantes_borrados cb, 
	iposs.proveedores p, 
	iposs.razones_sociales r, 
	iposs.tipos_documentos_inventarios tdi, 
	iposs.locales l, 
	iposs.depositos d, 
	iposs.estados e
where 	cb.loc_codigo = l.codigo
and 	cb.loc_codigo = d.loc_codigo
and 	cb.dep_codigo = d.codigo
and 	cb.prov_codigo = p.codigo
and 	cb.raz_soc_codigo = r.codigo
and 	cb.est_codigo = e.codigo
and 	cb.tip_doc_in_codigo = tdi.codigo
and 	l.codigo in :Local
and 	cb.fecha_emision between :FechaDesde and :FechaHasta + 1 - (1/24/60/60)
