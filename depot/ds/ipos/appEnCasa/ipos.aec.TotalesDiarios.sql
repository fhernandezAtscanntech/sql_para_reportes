select 
	l.loc_cod_empresa 	"Cod.Local", 
	l.descripcion 		"Local", 
	r.descripcion 		"Razón Social", 
	r.ruc 				"RUT", 
	m.fecha_comercial 	"Fecha", 
	m.caj_codigo 		"Caja", 
	f.fun_cod_empresa 	"Cod.Funcionario", 
	f.apellido 			"Cajero", 
	sum (m.total) 		"Total", 
	count(*) 			"Cant.Tickets"
from 
	iposs.movimientos m 
	join iposs.movimientos_appencasa ma
		on m.numero_mov = ma.numero_mov
		and m.emp_codigo = ma.emp_codigo
		and m.fecha_comercial = ma.fecha_comercial
	join iposs.locales l
		on m.loc_codigo = l.codigo
	join iposs.razones_sociales r
		on r.codigo = l.raz_soc_codigo
	join iposs.funcionarios f
		on f.fun_cod_empresa = m.fun_codigo_cajera
		and f.emp_codigo = m.emp_codigo
where 	m.loc_codigo = l.codigo
and 	m.fecha_comercial between :FechaDesde and :FechaHasta
and 	l.codigo in :Local
and 	m.emp_codigo = $EMPRESA_LOGUEADA$
and 	f.emp_codigo = $EMPRESA_LOGUEADA$
group by 
	l.loc_cod_empresa, 
	l.descripcion, 
	m.fecha_comercial, 
	r.descripcion, 
	r.ruc, 
	m.caj_codigo, 
	f.fun_cod_empresa, 
	f.apellido
