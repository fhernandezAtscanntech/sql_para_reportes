multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
		select 
			l.codigo 			"Cod.Local", 
			l.emp_codigo 		"Cod.Empresa", 
			l.loc_cod_empresa 	"Cod.Sucursal", 
			l.descripcion 		"Local", 
			r.descripcion 		"Razon Social", 
			r.ruc 				"RUT", 
			sum (m.total) 		"Total Tickets", 
			count(*) 			"Cant.Tickets"
		from 
			iposs.movimientos m 
			join iposs.movimientos_appencasa ma
				on m.numero_mov = ma.numero_mov
				and m.emp_codigo = ma.emp_codigo
				and m.fecha_comercial = ma.fecha_comercial
			join iposs.locales l
				on m.loc_codigo = l.codigo
			join iposs.razones_sociales r
				on r.codigo = l.raz_soc_codigo
			join iposs.funcionarios f
				on f.fun_cod_empresa = m.fun_codigo_cajera
				and f.emp_codigo = m.emp_codigo
		where 	m.fecha_comercial 
				between to_date('''||:FechaDesde||''', '''||'dd/mm/yyyy'||''') 
				and to_date('''||:FechaHasta||''', '''||'dd/mm/yyyy'||''') 
		group by 
			l.codigo, 
			l.emp_codigo, 
			l.loc_cod_empresa, 
			l.descripcion, 
			r.descripcion, 
			r.ruc
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
	select 
		l.codigo 			"Cod.Local", 
		l.emp_codigo 		"Cod.Empresa", 
		l.loc_cod_empresa 	"Cod.Sucursal", 
		l.descripcion 		"Local", 
		r.descripcion 		"Razon Social", 
		r.ruc 				"RUT", 
		sum (m.total) 		"Total Tickets", 
		count(*) 			"Cant.Tickets"
	from 
		iposs.movimientos m 
		join iposs.movimientos_appencasa ma
			on m.numero_mov = ma.numero_mov
			and m.emp_codigo = ma.emp_codigo
			and m.fecha_comercial = ma.fecha_comercial
		join iposs.locales l
			on m.loc_codigo = l.codigo
		join iposs.razones_sociales r
			on r.codigo = l.raz_soc_codigo
		join iposs.funcionarios f
			on f.fun_cod_empresa = m.fun_codigo_cajera
			and f.emp_codigo = m.emp_codigo
	where 	1=2
	group by 
		l.codigo, 
		l.emp_codigo, 
		l.loc_cod_empresa, 
		l.descripcion, 
		r.descripcion, 
		r.ruc
)

######################
consulta
######################
select 1 from #reco#


