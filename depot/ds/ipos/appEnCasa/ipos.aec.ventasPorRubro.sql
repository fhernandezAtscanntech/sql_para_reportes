select 
	l.loc_descripcion 		"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	fu.fun_cod_empresa 		"Cod.Cajero", 
	fu.fun_descripcion 		"Cajero", 
	f.fecha_comercial 		"Fecha", 
	p.prod_codigo 			"Codigo", 
	p.prod_descripcion 		"Articulo", 
	p.codigo_barras 		"Cod.Barra", 
	r.rub_cod_empresa 		"Cod.Rubro", 
	p.rub_descripcion 		"Rubro", 
	p.fam_descripcion 		"Familia", 
	p.sub_descripcion 		"Sub Familia", 
	p.tip_prod_descripcion 		"Tipo producto", 
	sum (nvl (f.ven_cantidad, 0)) - sum (nvl (f.dev_cantidad, 0)) 	"Unidades", 
	sum (nvl (f.mov_cantidad, 0)) 	"Cant.Tickets", 
	sum (nvl (f.ven_nac_importe, 0)) 	"Venta Total", 
	sum (nvl (f.dev_nac_importe, 0)) 	"Devol", 
	sum (nvl (f.bon_nac_importe, 0)) 	"Bonif", 
	sum (nvl (f.des_prom_nac_importe, 0)) 	"Prom", 
	sum (nvl (f.ven_nac_importe, 0)) - 
		sum (nvl (f.dev_nac_importe, 0))  -
		(sum (nvl (f.cos_nac_ult_impuestos, 0))) 	"Utilidad", 
	decode (sum (nvl (f.cos_nac_ult_impuestos, 0)), 
		0, null, 
		(((sum (nvl (f.ven_nac_importe, 0)) - sum (nvl (f.dev_nac_importe, 0))) 
		/ sum (nvl (f.cos_nac_ult_impuestos, 0))) 
		- 1) * 100) "Utilidad Porc.", 
	sum (nvl (f.cos_nac_ult_impuestos, 0))	"Costo Ultimo", 
	sum (nvl (f.cos_nac_ult_importe, 0)) 	"Costo Ultimo S/I", 
	sum (nvl (f.iva_importe, 0)) 	"IVA", 
	sum (nvl (f.ven_nac_importe, 0)) - 
		sum (nvl (f.dev_nac_importe, 0)) "Venta"
from 
	dwm_prod.f_ventas_min f 
	join iposs.movimientos_appEnCasa ma
		on f.numero_mov = ma.numero_mov
		and f.emp_codigo = ma.emp_codigo
		and f.fecha_comercial = ma.fecha_comercial
	join dwm_prod.l_ubicaciones l
		on l.codigo = f.l_ubi_codigo
		and l.emp_codigo = f.emp_codigo
	join dwm_prod.l_productos p
		on p.codigo = f.l_pro_codigo
		and p.emp_codigo = f.emp_codigo 
	join dwm_prod.l_funcionarios fu
		on fu.codigo = f.l_fun_codigo_cajero
	left join iposs.rubros r 		-- por los servicios
		on r.codigo = p.rub_codigo
where
	( f.fecha_comercial between :FechaDesde and :FechaHasta)
and
	( l.loc_codigo in :Local )
and
	( p.tip_prod_codigo in (1, 3) )
and 
	( :FechaHasta - :FechaDesde < 31 )
and 
	( trim (upper (':Rubro')) = 'NULL' or p.rub_codigo in :Rubro )
and 
	( l.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  p.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  f.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.loc_descripcion , 
	l.loc_codigo 		, 
	l.emp_codigo 		, 
	fu.fun_cod_empresa 	, 
	fu.fun_descripcion 	, 
	f.fecha_comercial 	, 
	p.prod_codigo 		, 
	p.prod_descripcion 	, 
	p.codigo_barras 	, 
	p.rub_descripcion 	, 
	p.fam_descripcion 	, 
	p.sub_descripcion 	, 
 	p.tip_prod_descripcion, 
	r.rub_cod_empresa
	