SELECT
	fvm.emp_codigo 		"Cod.Empresa",
	l.loc_codigo 		"Cod.Local", 
	l.loc_cod_empresa 	"Cod.Sucursal", 
	l.loc_descripcion 	"Local", 
	prom.prom_cod_empresa 	"Cod.Promoción",
	prom.prom_descripcion 	"Promoción",
	sum (nvl (fvm.ven_cantidad, 0) - nvl (fvm.dev_cantidad, 0)) as "Cantidad Ventas",
	sum (nvl (fvm.reintegro_importe, 0)) as "Importe Reintegro"
FROM
	dwm_prod.f_ventas_promo_diario_min fvm,
	dwm_prod.l_productos p,
	dwm_prod.l_promociones prom,
	dwm_prod.l_ubicaciones l
WHERE 	p.emp_codigo = fvm.emp_codigo 
AND 	p.codigo = fvm.l_pro_codigo 
AND 	prom.codigo = fvm.l_prom_codigo 
AND 	l.codigo = fvm.l_ubi_codigo 
AND 	l.emp_codigo = fvm.emp_codigo 
AND 	p.tip_prod_codigo = 1 
AND 	prom.prom_codigo <> 0 
AND 	fvm.fecha_comercial >= :FechaDesde 
AND 	fvm.fecha_comercial <= :FechaHasta
GROUP BY
	fvm.emp_codigo 		,
	l.loc_codigo 		,
	l.loc_cod_empresa 	,
	l.loc_descripcion 	, 
	prom.prom_cod_empresa,
	prom.prom_descripcion 	
