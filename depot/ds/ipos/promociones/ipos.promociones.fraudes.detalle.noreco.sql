select 
		e.codigo 		"Cod.Empresa", 
		l.codigo 		"Cod.Local", 
		e.descripcion 	"Empresa", 
		l.descripcion 	"Local", 
		mp.caj_codigo	 "Caja", 
		mp.numero_operacion 	"Nro.Operaci�n", 
		mp.numero_operacion_fiscal "Nro.Op.Fiscal", 
		mp.fecha_operacion 	"Fecha Operacion", 
		mp.hora_operacion 	"Hora Operaci�n", 
		mp.criterio_fraude 	"Criterio", 
		mp.total 	"Total"
	from 
		iposs.empresas e, 
		iposs.locales l, 
		iposs.movimientos m, 
		dwm_prod.movimientos_promociones_min mp
	where	mp.emp_codigo = m.emp_codigo
	and 	mp.fecha_comercial = m.fecha_comercial
	and 	mp.numero_mov = m.numero_mov
	and 	mp.emp_codigo = e.codigo
	and 	m.loc_codigo = l.codigo
	and 	mp.fecha_comercial between trunc (last_day(add_months(sysdate, -2)+1)) -- primer d�a del mes pasado
			and trunc(sysdate)
	and 	nvl (mp.fraude, 0) <> 0
