/*
Automatizaci�n de env�o de reporte agendado con los fraudes y los cierres sin movimientos 
correspondientes al mes corriente y anterior.

de esto:

fraudes y los cierres sin movimientos correspondientes al mes corriente y anterior.
*/


-- Detalle

multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select
	'select 
		e.codigo 		"Cod.Empresa", 
		l.codigo 		"Cod.Local", 
		e.descripcion 	"Empresa", 
		l.descripcion 	"Local", 
		mp.caj_codigo	 "Caja", 
		mp.numero_operacion 	"Nro.Operaci�n", 
		mp.numero_operacion_fiscal "Nro.Op.Fiscal", 
		mp.fecha_operacion 	"Fecha Operacion", 
		mp.hora_operacion 	"Hora Operaci�n", 
		mp.criterio_fraude 	"Criterio", 
		mp.total 	"Total"
	from 
		iposs.empresas e, 
		iposs.locales l, 
		iposs.movimientos m, 
		dwm_prod.movimientos_promociones_min mp
	where	mp.emp_codigo = m.emp_codigo
	and 	mp.fecha_comercial = m.fecha_comercial
	and 	mp.numero_mov = m.numero_mov
	and 	mp.emp_codigo = e.codigo
	and 	m.loc_codigo = l.codigo
	and 	mp.fecha_comercial between trunc (last_day(add_months(sysdate, -2)+1)) -- primer d�a del mes pasado
			and trunc(sysdate)
	and 	nvl (mp.fraude, 0) <> 0
	' as sql
	from dual
)

######################
tabla
######################

create table $$tabla$$ as (
select 
		e.codigo 		"Cod.Empresa", 
		l.codigo 		"Cod.Local", 
		e.descripcion 	"Empresa", 
		l.descripcion 	"Local", 
		mp.caj_codigo	 "Caja", 
		mp.numero_operacion 	"Nro.Operaci�n", 
		mp.numero_operacion_fiscal "Nro.Op.Fiscal", 
		mp.fecha_operacion 	"Fecha Operacion", 
		mp.hora_operacion 	"Hora Operaci�n", 
		mp.criterio_fraude 	"Criterio", 
		mp.total 	"Total"
	from 
		iposs.empresas e, 
		iposs.locales l, 
		iposs.movimientos m, 
		dwm_prod.movimientos_promociones_min mp
	where 1 = 2
)

######################
consulta
######################
select 1 from #reco#
