select  
		l.CODIGO_UNICO_LOCAL,
		p.TITULO, 
        p.id AS "ID_PROMOCAO",
        p.VIGENCIA_DESDE, 
        p.VIGENCIA_HASTA, 
        pp.ESTADO, 
        l.DESC_LOCAL AS "NOME LOJA",  
        ppr.FECHA_RECHAZO, 
        CASE pptr.DESCRIPCION
        	WHEN 'Sin datos' THEN 'Sem dados'
            WHEN 'Poco reintegro' THEN 'Ressarcimento menor que o desconto'
            WHEN 'No trabajo con este producto' THEN 'Dificuldade de entrega do fabricante'
            WHEN 'Otro' THEN 'Outro'
            WHEN 'Sin stock' THEN 'Sem estoque'  
            WHEN 'Producto con baja salida' THEN 'Produto com baixa saída'
            WHEN 'Promocion interna del mismo producto' THEN 'Promoção interna do mesmo produto'
            WHEN 'Promocion de producto de la competencia' THEN 'Promoção de produto concorrente'
            WHEN 'Proveedor tiene pendientes con minorista' THEN 'Indústria pendente com estabelecimento'
            WHEN 'No me gusto la dinamica de la promocion' THEN 'Não gostei da dinâmica da promoção'
            ELSE pptr.DESCRIPCION 
        END as DESCRIPCION, 
        substring(ppcr.COMENTARIO for 1500) as Comentario,
        e.RAZON_SOCIAL AS FABRICANTE
FROM 		   iposs_mp.promociones p 
    INNER JOIN iposs_mp.empresas e ON (p.ID_AUTOR = e.ID)
    INNER JOIN iposs_mp.propuestas_promociones pp ON (pp.ID_PROMOCION = p.ID)
    INNER JOIN iposs_mp.locales l ON (pp.ID_LOCAL_OFERIDO = l.ID)
    INNER JOIN iposs_mp.pro_pro_prop_prom_rechazos ppppr ON (pp.ID = ppppr.PROPUESTA_ID)
    INNER JOIN iposs_mp.pro_pro_rechazos ppr ON (ppppr.RECHAZO_ID = ppr.ID)
    INNER JOIN iposs_mp.pro_pro_tipos_rechazos pptr ON (ppr.TIPO_ID = pptr.ID)
    LEFT JOIN iposs_mp.pro_pro_comentarios_rechazos ppcr ON (ppcr.RECHAZO_ID = ppr.ID)
WHERE     	p.VIGENCIA_HASTA > current_date
		AND pp.ESTADO = 'RECHAZADA'
		AND pp.FECHA_BORRADO is null
