/*
Automatizaci�n de env�o de reporte agendado con los fraudes y los cierres sin movimientos 
correspondientes al mes corriente y anterior.

de esto:

fraudes y los cierres sin movimientos correspondientes al mes corriente y anterior.
*/

-- Resumen

multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select
	'select 
		to_char(mp.fecha_comercial, '''||'mm/yyyy'||''') "Mes", 
		e.codigo 		"Cod.Empresa", 
		l.codigo 		"Cod.Local", 
		e.descripcion 	"Empresa", 
		l.descripcion 	"Local", 
		pr.prom_descripcion 	"Promoci�n", 
		pr.cpa_descripcion 		"Campa�a", 
		mp.criterio_fraude 	"Criterio", 
		count(*) 		"Cantidad Tickets ", 
		sum (fv.reintegro_importe) 	"Reintegro"
	from 
		iposs.empresas e, 
		iposs.locales l, 
		iposs.movimientos m, 
		dwm_prod.movimientos_promociones_min mp, 
		dwm_prod.f_ventas_min fv, 
		dwm_prod.l_promociones pr
	where	fv.numero_mov = mp.numero_mov
	and 	fv.emp_codigo = mp.emp_codigo 
	and 	fv.fecha_comercial = mp.fecha_comercial 
	and 	mp.emp_codigo = m.emp_codigo
	and 	mp.fecha_comercial = m.fecha_comercial
	and 	mp.numero_mov = m.numero_mov
	and 	fv.l_prom_codigo = pr.codigo
	and 	mp.emp_codigo = e.codigo
	and 	m.loc_codigo = l.codigo
	and 	mp.fecha_comercial between trunc (last_day(add_months(sysdate, -2)+1)) -- primer d�a del mes pasado
			and trunc(sysdate)
	and 	nvl (mp.fraude, 0) <> 0
	and 	fv.l_prom_codigo <> 0
	group by 
		to_char(mp.fecha_comercial, '''||'mm/yyyy'||'''), 
		e.codigo,
		l.codigo,
		e.descripcion,
		l.descripcion,
		pr.prom_descripcion, 
		pr.cpa_descripcion, 
		mp.criterio_fraude
	' as sql 
	from dual
)

######################
tabla
######################

create table $$tabla$$ as (
	select 
		to_char(mp.fecha_comercial, 'mm/yyyy') "Mes", 
		e.codigo 		"Cod.Empresa", 
		l.codigo 		"Cod.Local", 
		e.descripcion 	"Empresa", 
		l.descripcion 	"Local", 
		pr.prom_descripcion 	"Promoci�n", 
		pr.cpa_descripcion 		"Campa�a", 
		mp.criterio_fraude 	"Criterio", 
		count(*) 		"Cantidad Tickets ", 
		sum (fv.reintegro_importe) 	"Reintegro"
	from 
		iposs.empresas e, 
		iposs.locales l, 
		iposs.movimientos m, 
		dwm_prod.movimientos_promociones_min mp, 
		dwm_prod.f_ventas_min fv, 
		dwm_prod.l_promociones pr
	where 1 = 2
	group by 
		to_char(mp.fecha_comercial, 'mm/yyyy'), 
		e.codigo,
		l.codigo,
		e.descripcion,
		l.descripcion,
		pr.prom_descripcion, 
		pr.cpa_descripcion, 
		mp.criterio_fraude	
)

######################
consulta
######################
select 1 from #reco#
