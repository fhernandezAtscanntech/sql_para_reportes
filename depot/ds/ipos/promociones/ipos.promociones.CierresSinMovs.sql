/*
Automatización de envío de reporte agendado con los fraudes y los cierres sin movimientos 
correspondientes al mes corriente y anterior.

de esto:

cierres sin movimientos correspondientes al mes corriente y anterior.
*/

-- Resumen

multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select
		'select 		
			e.codigo	"Cod.Empresa", 
			l.codigo	"Cod.Local", 
			e.descripcion	"Empresa", 
			l.descripcion	"Local", 
			p.caj_codigo	"Caja", 
			prom.codigo 	"Cod.Promoción", 
			prom.descripcion	"Promoción", 
			p.audit_date	"Audit Date", 
			p.fecha_comercial	"Fecha Comercial"
		from 		
			iposs.reporte_promo_br p, 	
			iposs.empresas e, 	
			iposs.locales l, 	
			iposs.promociones prom	
		where	p.emp_codigo = e.codigo	
		and 	p.loc_codigo = l.codigo	
		and 	p.prom_codigo = prom.codigo	
		and 	p.fecha_comercial between trunc (last_day(add_months(sysdate, -2)+1)) -- primer día del mes pasado	
			and trunc(sysdate)	
		and not exists		
			(select 1	
			from dwm_prod.f_ventas_promo_diario_min f, dwm_prod.l_ubicaciones u, dwm_prod.l_cajas ca, dwm_prod.l_promociones pr	
			where f.l_ubi_codigo = u.codigo	
			and f.l_caj_codigo = ca.codigo	
			and f.l_prom_codigo = pr.codigo	
			and u.emp_codigo = f.emp_codigo	
			and ca.emp_codigo = f.emp_codigo	
			and f.emp_codigo = p.emp_codigo	
			and u.emp_codigo = p.emp_codigo	
			and u.loc_codigo = p.loc_codigo	
			and ca.emp_codigo = p.emp_codigo	
			and ca.loc_codigo = p.loc_codigo	
			and ca.caj_codigo = p.caj_codigo	
			and f.fecha_comercial = p.fecha_comercial	
			and pr.prom_codigo = p.prom_codigo)	
	' as sql 
	from dual
)

######################
tabla
######################

create table $$tabla$$ as (
	select 		
		e.codigo	"Cod.Empresa", 
		l.codigo	"Cod.Local", 
		e.descripcion	"Empresa", 
		l.descripcion	"Local", 
		p.caj_codigo	"Caja", 
		prom.codigo 	"Cod.Promoción", 
		prom.descripcion	"Promoción", 
		p.audit_date	"Audit Date", 
		p.fecha_comercial	"Fecha Comercial"
	from 		
		iposs.reporte_promo_br p, 	
		iposs.empresas e, 	
		iposs.locales l, 	
		iposs.promociones prom	
	where 1 = 2)

######################
consulta
######################
select 1 from #reco#
