select 
	l.loc_codigo||l.emp_codigo 	"C�digo Unico", 
	l.emp_codigo 				"Cod.Empresa", 
	l.loc_codigo 				"Cod.Local", 
	l.loc_ruc 					"RUC", 
	l.loc_descripcion 			"Local", 
	l.loc_direccion 			"Direcci�n", 
	l.loca_descripcion 			"Localidad", 
	pp.descripcion 				"Proveedor", 
	lp.prom_descripcion 				"Promoci�n", 
	pr.prod_descripcion 		"Producto", 
	pr.codigo_barras 			"C�digo de Barras", 
	sum (nvl (f.ven_cantidad, 0) - nvl (f.dev_cantidad, 0)) 	"Cantidad", 
	sum (nvl (f.ven_nac_importe, 0) - nvl (f.dev_nac_importe, 0)) 	"Importe", 
	sum (nvl (f.des_prom_nac_importe, 0)) 	"Descuento promo", 
	sum (nvl (f.reintegro_importe, 0)) 		"Reintegro", 
	sum ((select nvl (f2.reintegro_importe, 0)
		from 
			dwm_prod.f_ventas_min f2, 
			dwm_prod.movimientos_promociones_min mpm
		where 	mpm.emp_codigo = f2.emp_codigo
		and 	mpm.numero_mov = f2.numero_mov
		and 	mpm.fecha_comercial = f2.fecha_comercial
		and 	f2.l_prom_codigo = f.l_prom_codigo
		and 	f2.l_pro_codigo = f.l_pro_codigo
		and 	mpm.fraude is not null
		and 	f.emp_codigo = mpm.emp_codigo
		and 	f.numero_mov = mpm.numero_mov
		and 	f.fecha_comercial = mpm.fecha_comercial
	)) 										"Fraude a descontar"
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.l_productos pr, 
	dwm_prod.l_promociones lp, 
	iposs.promociones_proveedores pp, 
	iposs.promociones p, 
	dwm_prod.f_ventas_min f
where 	f.l_ubi_codigo = l.codigo
and 	f.l_pro_codigo = pr.codigo
and 	f.l_prom_codigo = lp.codigo
and 	lp.prom_codigo = p.codigo
and 	p.prom_prov_codigo = pp.codigo
and 	f.l_prom_codigo <> 0 			-- con promociones
and 	lp.prom_codigo in :Promocion
and 	f.fecha_comercial between :FechaDesde and :FechaHasta
group by 
	l.emp_codigo, 
	l.loc_codigo, 
	l.loc_ruc, 
	l.loc_descripcion, 
	l.loc_direccion, 
	l.loca_descripcion, 
	pp.descripcion, 
	lp.prom_descripcion, 
	pr.prod_descripcion, 
	pr.codigo_barras
