select 		
	ae.art_codigo_externo 	"Cod.Art�culo",
	ae.descripcion 	"Art�culo", 
	r.rub_cod_empresa 	"Cod.Rubro", 
	r.descripcion 		"Rubro", 
	dep_ori.descripcion 	"Dep�sito Origen", 
	dep_des.descripcion 	"Dep�sito Destino", 
	tmi.descripcion 	"Tipo Movimiento", 
	mi.fecha 	"Fecha", 
	mi.numero_documento 	"Numero", 
	mi.observaciones 		"Observaciones", 
	mi.audit_user 	"Usuario", 
	mid.costo_imp 	"Costo c.Imp",
	mid.costo 		"Costo s.Imp", 
	mid.precio_unitario 	"Precio Unitario", 
	sum (decode (
			tmi.codigo, 
			1, mid.cantidad_destino*mid.unidades, 
			3, mid.cantidad_origen*mid.unidades)) "Cantidad", 
	mi.total 	"Total Movimiento", 
	(select p.precio
		from iposs.precios_de_articulos_ventas p
		where p.art_codigo = ae.art_codigo
		and p.emp_codigo = ae.emp_codigo
		and p.loc_lis_loc_codigo = lv.loc_codigo
		and p.loc_lis_lis_pre_ve_codigo = 
			(select valor 
			from iposs.parametros_empresas pe 
			where pe.emp_codigo = ae.emp_codigo 
			and pe.par_codigo = 'LIS_PRE_VE_DEF')
		and mi.fecha between p.vigencia and p.vigencia_hasta) 	"Precio venta"
from 		
	iposs.articulos_empresas ae, 	
	iposs.rubros r, 
	iposs.depositos dep_ori, 	
	iposs.depositos dep_des, 	
	iposs.inv_movimientos mi, 	
	iposs.inv_movimientos_detalles mid, 	
	iposs.inv_tipos_movimientos tmi, 
	#LocalesVisibles# lv
where 		
	( ( dep_des.loc_codigo(+) = mi.loc_codigo_destino and 	
	    dep_des.codigo(+) = mi.dep_codigo_destino ) and 	
	  ( dep_ori.loc_codigo(+) = mi.loc_codigo_origen and 	
	    dep_ori.codigo(+) = mi.dep_codigo_origen ) and 	
	  ( mi.fecha_comercial = mid.fecha_comercial and 	
	    mi.emp_codigo = mid.emp_codigo and 	
	    mi.numero = mid.inv_mov_numero ) and 	
	  ( ae.art_codigo = mid.art_codigo and 	
	    ae.emp_codigo = mid.emp_codigo ) and 	
	  ( tmi.codigo = mi.inv_tip_mov_codigo ) and 
	  ( ae.rub_codigo = r.codigo )) and 	
	( mi.fecha_comercial between :FechaDesde and :FechaHasta ) and 	
	( tmi.codigo in ( 1, 3 ) ) and 	
	( mi.comp_codigo is null ) and 		-- ignoro los ingresos/egresos con comprobantes asociados
	( nvl (mi.loc_codigo_origen, mi.loc_codigo_destino) = lv.loc_codigo and
	  ae.emp_codigo = $EMPRESA_LOGUEADA$ and	
	  mi.emp_codigo = $EMPRESA_LOGUEADA$	
	)
group by 		
	ae.art_codigo_externo, 	
	ae.descripcion, 	
	ae.emp_codigo, 
 	ae.art_codigo, 
	r.rub_cod_empresa, 
	r.descripcion, 
	dep_ori.descripcion, 	
	dep_des.descripcion, 	
	tmi.descripcion, 	
	mi.fecha, 	
	mi.numero_documento, 	
	mi.observaciones, 
	mi.audit_user, 
	mid.costo_imp, 
	mid.costo, 	
	mid.precio_unitario
	mi.total, 
	lv.loc_codigo
