select 
	ae.art_codigo 	"Art.Codigo(I)", 
	ae.art_codigo_externo 	"Art.Codigo", 
	ae.descripcion 	"Articulo", 
	r.descripcion 	"Rubro", 
	f.descripcion 	"Familia", 
	(select cb.codigo_barra 
		from iposs.codigos_barras cb 
		where cb.art_codigo = ae.art_codigo 
		and (cb.emp_Codigo = ae.emp_codigo or (cb.emp_codigo = 1 and ae.veo_barras_modelo = 1)) 
		and rownum = 1) "Codigo Barra", 
	l.codigo 	"Loc.Codigo(I)", 
	l.loc_Cod_empresa 	"Loc.Codigo", 
	l.descripcion 	"Local", 
	u.descripcion 	"Ubicacion", 
	sum(cid.cantidad) 	"Stock", 
	(select decode (pc.mon_codigo, 
			(select valor from parametros where codigo = 'MONEDA_DEF'), pc.costo_imp, 
			pc.costo_imp * (select c.valor 
							from cambios c 
							where c.mon_codigo = pc.mon_codigo
							and :FechaConcilia between c.fecha and c.vigencia_hasta
							and c.emp_codigo = $EMPRESA_LOGUEADA$))
		from precios_de_costos_articulos pc
		where pc.loc_codigo = l.codigo
		and pc.art_codigo = ae.art_codigo
		and :FechaConcilia between pc.vigencia_desde and pc.vigencia_hasta
		and pc.emp_codigo = $EMPRESA_LOGUEADA$ 
		and rownum = 1 ) 	"Costo c.Imp.", 	
	(select decode (pc.mon_codigo, 
			(select valor from parametros where codigo = 'MONEDA_DEF'), pc.costo_ultimo,  
			pc.costo_ultimo * (select c.valor 
								from cambios c 
								where c.mon_codigo = pc.mon_codigo
								and :FechaConcilia between c.fecha and c.vigencia_hasta
								and c.emp_codigo = $EMPRESA_LOGUEADA$))
		from precios_de_costos_articulos pc
		where pc.loc_codigo = l.codigo
		and pc.art_codigo = ae.art_codigo
		and :FechaConcilia between pc.vigencia_desde and pc.vigencia_hasta
		and pc.emp_codigo = $EMPRESA_LOGUEADA$ 
		and rownum = 1 ) 	"Costo S.Imp.", 
	(select decode (pv.mon_codigo, 
			(select  valor from parametros where codigo = 'MONEDA_DEF'), pv.precio, 
			pv.precio * (select c.valor 
						from cambios c 
						where c.mon_codigo = pv.mon_codigo
						and :FechaConcilia between c.fecha and c.vigencia_hasta
						and c.emp_codigo = $EMPRESA_LOGUEADA$))
		from precios_de_articulos_ventas pv
		where pv.loc_lis_lis_pre_ve_codigo = (select valor from parametros_empresas where par_codigo = 'LIS_PRE_VE_DEF' and emp_codigo = $EMPRESA_LOGUEADA$)
		and pv.loc_lis_loc_codigo = al.loc_codigo
		and pv.art_codigo = al.art_codigo
		and pv.emp_codigo = $EMPRESA_LOGUEADA$
		and :FechaConcilia between pv.vigencia and pv.vigencia_hasta) "Venta"
from 
	iposs.articulos_empresas ae, 
	iposs.articulos_locales al, 
	iposs.rubros r, 
	iposs.familias f, 
	iposs.locales l, 
	iposs.conciliacion_inventarios ci, 
	iposs.conciliacion_inv_Detalles cid, 
	iposs.ubicaciones u
where 
	( ( ci.numero = cid.con_inv_numero) and
	  ( ci.loc_codigo = l.codigo  and
	    ci.emp_codigo = l.emp_codigo ) and 
	  ( cid.con_ubi_codigo = u.codigo (+) ) and 
	  ( cid.art_codigo = al.art_codigo ) and 
	  ( al.art_codigo = ae.art_Codigo and 
	    al.emp_codigo = ae.emp_codigo ) and 
	  ( l.codigo = al.loc_codigo ) and 
	  ( r.codigo = ae.rub_codigo ) and 
	  ( ae.fam_codigo = f.codigo (+) ) ) and
	( ae.emp_codigo <> 1 ) and
	( ae.est_codigo <> 2 ) and 
	( cid.cantidad <> 0 ) and 
	( l.codigo in :Local ) and 
	( ci.fecha = :FechaConcilia ) and 
	( trim (upper (':Rubro')) = 'NULL' or r.codigo in :Rubro ) and 
	ae.emp_codigo = $EMPRESA_LOGUEADA$ and
	al.emp_codigo = $EMPRESA_LOGUEADA$ and
	l.emp_codigo = $EMPRESA_LOGUEADA$
group by 
	ae.art_codigo, 
	ae.art_codigo_externo, 
	ae.descripcion, 
	ae.emp_codigo, 
	ae.veo_barras_modelo, 
	al.loc_codigo, 
	al.art_codigo, 
	r.descripcion, 
	f.descripcion, 
	l.codigo, 
	l.loc_Cod_empresa, 
	l.descripcion, 
	u.descripcion
