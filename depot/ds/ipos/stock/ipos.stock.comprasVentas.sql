select 
	ae.art_codigo_externo 	"Cod.Artículo",
	ae.descripcion 		"Articulo",
	r.descripcion 		"Rubro", 
	(select f.descripcion 	
		from iposs.familias f
		where ae.fam_codigo = f.codigo) 	"Familia", 
	l.loc_cod_empresa 		"Cod.Local", 
	l.descripcion 		"Local", 
	dep.codigo 		"Cod. Depósito", 
	dep.descripcion 	"Depósito", 
	(select s.stock
		from iposs.stocks s
		where s.art_codigo = al.art_codigo
		and s.dep_loc_codigo = al.loc_codigo
		and s.dep_loc_codigo = dep.loc_codigo
		and s.dep_codigo = dep.codigo
		and sysdate between s.fecha and s.vigencia_hasta
		and s.vigencia_hasta > sysdate) 		"Stock Actual", 
	(select 
--		sum (decode (
--			m.inv_tip_mov_codigo, 
--				1, nvl (d.cantidad_destino * d.unidades * nvl (d.costo_imp, d.precio_unitario) * nvl ((select tc.tipo_cambio from iposs.tipos_de_cambios tc where tc.emp_codigo = comp.emp_codigo and tc.codigo = comp.tip_cam_codigo), 1), 0), 
--				3, nvl (-d.cantidad_origen * d.unidades * nvl (d.costo_imp, d.precio_unitario) * nvl ((select tc.tipo_cambio from iposs.tipos_de_cambios tc where tc.emp_codigo = comp.emp_codigo and tc.codigo = comp.tip_cam_codigo), 1), 0), 
--				0))  
		sum (nvl (decode (
			m.inv_tip_mov_codigo, 
			1, d.cantidad_destino, 
			3, -d.cantidad_origen, 
			0) * d.unidades * nvl (d.costo_imp, d.precio_unitario) * nvl ((select tc.tipo_cambio from iposs.tipos_de_cambios tc where tc.emp_codigo = comp.emp_codigo and tc.codigo = comp.tip_cam_codigo), 1), 0))
		from iposs.inv_movimientos m, iposs.inv_movimientos_Detalles d, iposs.comprobantes comp
		where m.numero = d.inv_mov_numero
		and m.emp_codigo = d.emp_codigo
		and m.fecha_comercial = d.fecha_comercial
		and d.emp_codigo = al.emp_codigo
		and al.loc_Codigo = nvl (m.loc_codigo_destino, m.loc_codigo_origen)
		and m.inv_tip_mov_codigo in (1, 3)
		and d.art_codigo = al.art_codigo
		and m.comp_codigo = comp.codigo
		and m.comp_fecha_emision = comp.fecha_emision
		and m.comp_emp_codigo = comp.emp_codigo
		and m.comp_codigo is not null
		and d.fecha_comercial between :FechaDesde and :FechaHasta) 		"Compras $", 
	(select 
--		sum (decode (
--			m.inv_tip_mov_codigo, 
--				1, nvl (d.cantidad_destino * d.unidades, 0), 
--				3, nvl (-d.cantidad_origen * d.unidades, 0), 
--				0))
		sum (nvl (decode (m.inv_tip_mov_codigo, 
				1, d.cantidad_destino, 
				3, -d.cantidad_origen, 
				0) * d.unidades, 0))
		from iposs.inv_movimientos m, iposs.inv_movimientos_Detalles d, iposs.comprobantes comp
		where m.numero = d.inv_mov_numero
		and m.emp_codigo = d.emp_codigo
		and m.fecha_comercial = d.fecha_comercial
		and d.emp_codigo = al.emp_codigo
		and al.loc_Codigo = nvl (m.loc_codigo_destino, m.loc_codigo_origen)
		and m.inv_tip_mov_codigo in (1, 3)
		and d.art_codigo = al.art_codigo
		and m.comp_codigo = comp.codigo
		and m.comp_fecha_emision = comp.fecha_emision
		and m.comp_emp_codigo = comp.emp_codigo
		and m.comp_codigo is not null
		and d.fecha_comercial between :FechaDesde and :FechaHasta) 	"Compras Qt", 
	(select 
--		sum (decode (
--			m.inv_tip_mov_codigo, 
--				6, nvl (d.cantidad_origen * d.unidades * d.precio_unitario, 0), 
--				8, nvl (-d.cantidad_destino * d.unidades * d.precio_unitario, 0), 
--				0))
		sum (nvl (decode (m.inv_tip_mov_codigo, 
				6, d.cantidad_origen, 
				8, -d.cantidad_destino, 
				0) * d.unidades * d.precio_unitario, 0))
		from iposs.inv_movimientos m, iposs.inv_movimientos_Detalles d
		where m.numero = d.inv_mov_numero
		and m.emp_codigo = d.emp_codigo
		and m.fecha_comercial = d.fecha_comercial
		and d.emp_codigo = al.emp_codigo
		and al.loc_Codigo = nvl (m.loc_codigo_destino, m.loc_codigo_origen)
		and m.inv_tip_mov_codigo in (6, 8)
		and d.art_codigo = al.art_codigo
		and d.fecha_comercial between :FechaDesde and :FechaHasta) 							"Ventas $", 
	(select 
--		sum (decode (
--			m.inv_tip_mov_codigo, 
--				6, nvl (d.cantidad_origen * d.unidades, 0), 
--				8, nvl (-d.cantidad_destino * d.unidades, 0), 
--				0))
		sum (nvl (decode (m.inv_tip_mov_codigo, 
				6, d.cantidad_origen, 
				8, -d.cantidad_destino, 
				0) * d.unidades, 0))
		from iposs.inv_movimientos m, iposs.inv_movimientos_Detalles d
		where m.numero = d.inv_mov_numero
		and m.emp_codigo = d.emp_codigo
		and m.fecha_comercial = d.fecha_comercial
		and d.emp_codigo = al.emp_codigo
		and al.loc_Codigo = nvl (m.loc_codigo_destino, m.loc_codigo_origen)
		and m.inv_tip_mov_codigo in (6, 8)
		and d.art_codigo = al.art_codigo
		and d.fecha_comercial between :FechaDesde and :FechaHasta)		"Ventas Qt"
from 
	iposs.articulos_empresas ae, 
	iposs.articulos_locales al, 
	iposs.rubros r, 
	iposs.locales l, 
 	iposs.depositos dep
where
	ae.art_codigo = al.art_codigo
and ae.emp_codigo = al.emp_codigo
and 
	al.loc_codigo = l.codigo
and 
 	dep.loc_codigo = l.codigo
and 
	ae.rub_codigo = r.codigo
and
	( trim (upper (':Proveedor')) = 'NULL' or ae.art_codigo in 
		(select pa.art_codigo
		from iposs.proveedores_articulos pa, articulos_locales_proveedores alp
		where alp.art_loc_art_codigo = pa.art_codigo
		and alp.prov_codigo = pa.prov_codigo
		and alp.emp_codigo = pa.emp_codigo
		and alp.art_loc_loc_codigo = al.loc_codigo
		and pa.emp_codigo = al.emp_codigo
		and pa.fecha_borrado is null
		and alp.fecha_borrado is null
		and pa.prov_codigo in :Proveedor))
and 
	( trim (upper (':Rubro')) = 'NULL' or ae.rub_codigo in :Rubro)
and 
	l.codigo in :Local
and 
	:FechaHasta - :FechaDesde <= 31
and
	ae.emp_codigo = $EMPRESA_LOGUEADA$
and l.emp_codigo = $EMPRESA_LOGUEADA$
