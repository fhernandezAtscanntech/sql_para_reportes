select 
	l.loc_descripcion 	"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	(select distinct f.ano_nombre 
		from dwm_prod.l_fechas f 
		where f.mes_codigo = fvm.mes_codigo) 	"A�o", 
	(select distinct f.mes_nombre_corto 
		from dwm_prod.l_fechas f 
		where f.mes_codigo = fvm.mes_codigo) 	"Mes", 
	i.iva_descripcion 		"IVA", 
	p.rub_descripcion 		"Rubro", 
	m.tip_descripcion 		"Tipo M Pago", 
	m.sub_descripcion 		"SubTipo M Pago", 
	sum (nvl (fvm.ven_cantidad, 0)) - sum (nvl (fvm.dev_cantidad, 0)) 	"Unidades", 
	sum (nvl (fvm.ven_nac_importe, 0)) 	"Venta Total", 
	sum (nvl (fvm.dev_nac_importe, 0)) 	"Devol", 
	sum (nvl (fvm.bon_nac_importe, 0)) 	"Bonif", 
	sum (nvl (fvm.redondeo_importe, 0)) 	"Redondeo", 
	sum (nvl (fvm.des_prom_nac_importe, 0)) 	"Prom", 
	sum (nvl (fvm.ven_nac_importe, 0)) - 
		sum (nvl (fvm.dev_nac_importe, 0)) -
		(sum (nvl (fvm.cos_nac_ult_importe, 0)) +
		sum (nvl (fvm.cos_nac_ult_impuestos, 0)))	"Utilidad", 
	sum (nvl (fvm.cos_nac_ult_importe, 0)) +
	sum (nvl (fvm.cos_nac_ult_impuestos, 0)) 	"Costo Ultimo", 
	sum (nvl (fvm.iva_importe, 0)) 	"Monto IVA", 
	sum (nvl (fvm.imp_1_importe, 0)) 	"Monto Imp1", 
	sum (nvl (fvm.imp_2_importe, 0)) 	"Monto Imp2", 
	sum (nvl (fvm.imp_3_importe, 0)) 	"Monto Imp3", 
	sum (nvl (fvm.imp_4_importe, 0)) 	"Monto Imp4", 
	sum (nvl (fvm.imp_5_importe, 0)) 	"Monto Imp5", 
	sum (nvl (fvm.ven_nac_importe, 0)) - 
		sum (nvl (fvm.dev_nac_importe, 0))  	"Venta"
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.l_productos p, 
	dwm_prod.l_impuestos i, 
	dwm_prod.l_medios m, 
	dwm_prod.f_ventas_mensual fvm
where
	( l.codigo = fvm.l_ubi_codigo 
and 	l.emp_codigo = fvm.emp_codigo ) 
and
	( i.codigo = fvm.l_imp_codigo 
and 	  i.emp_codigo = fvm.emp_codigo )
and
	( p.codigo = fvm.l_pro_codigo 
and 	  p.emp_codigo = fvm.emp_codigo )
and
	( m.codigo = fvm.l_med_codigo 
and 	  m.emp_codigo = fvm.emp_codigo)
and 
	( fvm.mes_codigo in :Mes )
and
	( l.loc_codigo in :Local )
and
	( p.tip_prod_codigo in (1, 3) )
and 
	( l.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  p.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  i.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  m.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  fvm.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.loc_descripcion , 
	l.loc_codigo 		, 
	l.emp_codigo 		, 
	p.rub_descripcion 	, 
	fvm.mes_codigo 	, 
	i.iva_descripcion, 
	m.tip_descripcion, 
	m.sub_descripcion
 