select 
	l.loc_descripcion 	"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	c.caj_descripcion 		"Caja", 
	f.ano_nombre 			"A�o", 
	f.mes_nombre_corto  		"Mes", 
	f.dia_del_mes 			"D�a", 
	i.iva_descripcion 		"IVA", 
	sum (nvl (fvd.ven_cantidad, 0)) - sum (nvl (fvd.dev_cantidad, 0)) 	"Unidades", 
	sum (nvl (fvd.ven_nac_importe, 0)) 	"Venta Total", 
	sum (nvl (fvd.dev_nac_importe, 0)) 	"Devol", 
	sum (nvl (fvd.bon_nac_importe, 0)) 	"Bonif", 
	sum (nvl (fvd.des_prom_nac_importe, 0)) 	"Prom", 
	sum (nvl (fvd.ven_nac_importe, 0)) - 
		sum (nvl (fvd.dev_nac_importe, 0))  -
		(sum (nvl (fvd.cos_nac_ult_impuestos, 0)))	"Utilidad", 
	sum (nvl (fvd.cos_nac_ult_impuestos, 0)) 	"Costo Ultimo", 
	sum (nvl (fvd.iva_importe, 0)) 	"Monto IVA", 
	sum (nvl (fvd.imp_1_importe, 0)) 	"Monto Imp1", 
	sum (nvl (fvd.imp_2_importe, 0)) 	"Monto Imp2", 
	sum (nvl (fvd.imp_3_importe, 0)) 	"Monto Imp3", 
	sum (nvl (fvd.imp_4_importe, 0)) 	"Monto Imp4", 
	sum (nvl (fvd.imp_5_importe, 0)) 	"Monto Imp5", 
	sum (nvl (fvd.ven_nac_importe, 0)) - 
		sum (nvl (fvd.dev_nac_importe, 0))  	"Venta"
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.l_productos p, 
	dwm_prod.l_impuestos i, 
	dwm_prod.l_fechas f, 
	dwm_prod.l_cajas c, 
	dwm_prod.f_ventas_diario fvd
where
	( l.codigo = fvd.l_ubi_codigo 
and 	l.emp_codigo = fvd.emp_codigo ) 
and
	( i.codigo = fvd.l_imp_codigo 
and 	  i.emp_codigo = fvd.emp_codigo )
and
	( p.codigo = fvd.l_pro_codigo 
and 	  p.emp_codigo = fvd.emp_codigo )
and 
	( c.codigo = fvd.l_caj_codigo 
and 	  c.emp_codigo = fvd.emp_codigo )
and
	( f.codigo = fvd.fecha_comercial )
and
	( fvd.fecha_comercial between :FechaDesde and :FechaHasta )
and 
	( :FechaHasta - :FechaDesde <= 31)
and
	( l.loc_codigo in :Local )
and 
	( trim (upper(':Caja')) = 'NULL' or c.caj_codigo in :Caja )
and
	( p.tip_prod_codigo in (1, 3) )
and 
	( l.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  p.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  i.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  fvd.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.loc_descripcion , 
	l.loc_codigo 		, 
	l.emp_codigo 		, 
	c.caj_descripcion 	, 
	f.ano_nombre 		, 
	f.mes_nombre_corto 	, 
	f.dia_del_mes 		, 
	i.iva_descripcion
 