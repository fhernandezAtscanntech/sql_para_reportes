select 
	l.loc_codigo		"Cod.Local", 
	l.emp_codigo 		"Emp.Codigo", 
	l.loc_descripcion 	"Local", 
	c.caj_descripcion 	"Caja", 
	fv.fecha_comercial 	"Fecha", 
	f.fun_cod_empresa 	"Cod.Cajero", 
	f.fun_descripcion 	"Cajero", 
	nvl (sum (fv.ven_nac_importe), 0) - 
		nvl (sum (fv.dev_nac_importe), 0)  	"Venta", 
	nvl (sum (fv.mov_cantidad), 0) 		"Cantidad Tickets", 
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.l_cajas c, 
	dwm_prod.l_funcionarios f, 
	dwm_prod.f_ventas_diario_min fv
where	f.codigo = fv.l_fun_codigo_cajero
and 	l.codigo = fv.l_ubi_codigo 
and 	c.codigo = fv.l_caj_codigo
and 	l.emp_codigo = fv.emp_codigo 
and 	fv.fecha_comercial between :FechaDesde and :FechaHasta
and 	:FechaHasta - :FechaDesde <= 31
and 	( l.loc_codigo in :Local )
and 	( l.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  c.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  fv.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
		l.loc_codigo,
		l.emp_codigo,
		l.loc_descripcion,
		c.caj_descripcion,
		fv.fecha_comercial,
		f.fun_cod_empresa,
		f.fun_descripcion
