select 
	l.loc_descripcion 	"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	fv.fecha_comercial 		"Fecha", 
	v.fun_cod_empresa 		"Cod.Vendedor", 
	v.fun_descripcion 		"Vendedor", 
	v.fun_tipo_vendedor 		"Tipo Vendedor", 
	nvl (sum (fv.ven_nac_importe), 0) - 
		nvl (sum (fv.dev_nac_importe), 0)  	"Venta", 
	nvl (sum (fv.mov_cantidad), 0) 		"Cantidad Ticket"
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.l_funcionarios v, 
	dwm_prod.f_ventas_diario fv
where	v.codigo = fv.l_fun_codigo_vendedor 
and 	l.codigo = fv.l_ubi_codigo 
and 	l.emp_codigo = fv.emp_codigo 
and 	fv.fecha_comercial between :FechaDesde and :FechaHasta
and 	:FechaHasta - :FechaDesde <= 31
and 	( trim (upper (':Local')) = 'NULL' or l.loc_codigo in :Local )
and 	( trim (upper (':Vendedor')) = 'NULL' or v.fun_codigo in :Vendedor )
and 	( l.emp_codigo = $EMPRESA_LOGUEADA$ 
-- and 	  v.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  fv.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.loc_descripcion, 
	l.loc_codigo, 
	l.emp_codigo, 
	fv.fecha_comercial, 
	v.fun_cod_empresa, 
	v.fun_descripcion, 
	v.fun_tipo_vendedor
