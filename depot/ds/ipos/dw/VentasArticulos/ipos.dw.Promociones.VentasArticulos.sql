select 
	l.loc_descripcion 	"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	f.ano_nombre 			"A�o", 
	f.mes_nombre_corto 		"Mes", 
	f.dia_del_mes 			"D�a", 
	p.prod_codigo 			"Codigo", 
	p.prod_descripcion 		"Articulo", 
	c.comb_descripcion 		"Combo", 
	p.rub_descripcion 		"Rubro", 
	pr.cpa_descripcion 		"Campa�a", 
	pr.prom_descripcion 	"Promoci�n", 
	sum (nvl (fvpd.ven_cantidad, 0)) - sum (nvl (fvpd.dev_cantidad, 0)) 	"Unidades", 
	sum (nvl (fvpd.ven_nac_importe, 0)) 	"Venta Total", 
	sum (nvl (fvpd.dev_nac_importe, 0)) 	"Devol", 
	sum (nvl (fvpd.bon_nac_importe, 0)) 	"Bonif", 
	sum (nvl (fvpd.des_prom_nac_importe, 0)) 	"Prom", 
	sum (nvl (fvpd.ven_nac_importe, 0)) - 
		sum (nvl (fvpd.dev_nac_importe, 0))  -
		(sum (nvl (fvpd.cos_nac_ult_impuestos, 0))) 	"Utilidad", 
		sum (nvl (fvpd.cos_nac_ult_impuestos, 0))	"Costo Ultimo", 
	sum (nvl (fvpd.iva_importe, 0)) 	"IVA", 
	sum (nvl (fvpd.ven_nac_importe, 0)) - 
		sum (nvl (fvpd.dev_nac_importe, 0))  	"Venta", 
	sum (nvl (fvpd.reintegro_importe, 0)) 		"Reintegro"
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.l_fechas f, 
	dwm_prod.l_productos p, 
	dwm_prod.l_combos c, 
	dwm_prod.l_promociones pr, 
	dwm_prod.f_ventas_promo_diario_min fvpd
where
	( l.codigo = fvpd.l_ubi_codigo 
and 	  l.emp_codigo = fvpd.emp_codigo ) 
and
	( p.codigo = fvpd.l_pro_codigo 
and 	  p.emp_codigo = fvpd.emp_codigo )
and
	( pr.codigo = fvpd.l_prom_codigo )
and 
	( c.codigo = fvpd.l_comb_codigo 
and 	  c.emp_codigo = fvpd.emp_codigo )
and 
	( fvpd.fecha_comercial = f.codigo )
and 
	( fvpd.fecha_comercial between :FechaDesde and :FechaHasta)
and 
	( :FechaHasta - :FechaDesde <= 62 )
and
	( p.tip_prod_codigo in (1, 3) )
and 
	( l.loc_codigo in :Local
and 	  l.emp_codigo = $EMPRESA_LOGUEADA$
and 	  p.emp_codigo = $EMPRESA_LOGUEADA$
and 	  fvpd.emp_codigo = $EMPRESA_LOGUEADA$)
group by 
	l.loc_descripcion , 
	l.loc_codigo 		, 
	l.emp_codigo 		, 
	f.ano_nombre 		, 
	f.mes_nombre_corto 	, 
	f.dia_del_mes 		,
	p.prod_codigo 		, 
	p.prod_descripcion 	, 
	c.comb_descripcion 	, 
	p.rub_descripcion 	, 
	pr.cpa_descripcion 	, 
	pr.prom_descripcion
	