select 
	l.loc_descripcion 	"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	f.ano_nombre 			"A�o", 
	f.mes_nombre_corto 		"Mes", 
	f.dia_del_mes 			"Dia", 
	f.dia_nombre_largo 		"Dia Nombre", 
	f.dia_de_la_semana		"Nro Dia Semana", 
	p.prod_codigo 			"Cod.Art�culo", 
	p.prod_descripcion 		"Art�culo", 
	p.rub_descripcion 		"Rubro", 
	to_char(h.hor_hora, '09') 		"Hora", 
	nvl (sum (fvm.ven_nac_importe), 0) 	"Venta Total", 
	nvl (sum (fvm.dev_nac_importe), 0) 	"Devol Total", 
	nvl (sum (fvm.bon_nac_importe), 0) 	"Bon Total", 
	nvl (sum (fvm.des_nac_importe), 0) 	"Desc  Total", 
	nvl (sum (fvm.des_prom_Nac_importe), 0) 	"Prom Total", 
	nvl (sum (fvm.ven_cantidad), 0) - 
		nvl (sum (fvm.dev_cantidad), 0) 	"Cantidad", 
	nvl (sum (fvm.ven_nac_importe), 0) - 
		nvl (sum (fvm.dev_nac_importe), 0)  	"Venta"
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.l_productos p, 
	dwm_prod.l_fechas f, 
	dwm_prod.l_horas h, 
-- 	dwm_prod.f_local_hora_diario flhd
	dwm_prod.f_ventas_min fvm
where	( l.codigo = fvm.l_ubi_codigo 
and 	  l.emp_codigo = fvm.emp_codigo ) 		
and 	( p.codigo = fvm.l_pro_codigo 
and 	  p.emp_codigo = fvm.emp_codigo )
and 	( h.codigo = fvm.l_hor_codigo )
and 	( fvm.fecha_comercial = f.codigo )
and 	( p.tip_prod_codigo = 1 ) 		-- Articulos 
and 	( fvm.fecha_comercial between :FechaDesde and :FechaHasta
and 	  :FechaHasta - :FechaDesde <= 31 )
and 	( trim (upper (':Rubro')) = 'NULL' or p.rub_codigo in :Rubro )
and 	( l.loc_codigo in :Local )
and 	( l.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.loc_descripcion, 
	l.loc_codigo, 
	l.emp_codigo, 
	f.ano_nombre, 
	f.mes_nombre_corto, 
	f.dia_del_mes, 
	f.dia_nombre_largo, 
	f.dia_de_la_semana,
	p.prod_codigo, 
	p.prod_descripcion, 
	p.rub_descripcion, 
	to_char(h.hor_hora, '09')
