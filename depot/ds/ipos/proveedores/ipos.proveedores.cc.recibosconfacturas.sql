select 
	pe.prov_Cod_Externo 	"Cod.Proveedor", 
	p.razon_social 		"Proveedor", 
	g.codigo 			"Cod.Gru.Pag.", 
	g.descripcion 		"Grupo de Pago", 
	rec.fecha 			"Fecha rec.", 
	rec.rec_serie 		"Serie rec.", 
	rec.rec_numero 		"Número rec.", 
	mr.simbolo 			"Mon. Recibo", 
	abs(rec.importe) 	"Importe Recibo", 
	docs.fecha 			"Fecha comp.", 
	docs.rec_serie 		"Serie comp.", 
	docs.rec_numero 	"Número comp.", 
	tdi.descripcion 	"Tipo Doc.", 
	abs(docs.importe) 		"Importe", 
	abs(docs.importe_pago) 	"Importe Pago comp.", 
	mc.simbolo 				"Mon. Pago", 
	abs(pagos.importe) 		"Importe Pago recibo", 
	pagos.tipo_cambio 		"Tipo Cambio"
from 
	iposs.cuentas_corrientes_prov cc, 
	iposs.movimientos_cc_prov rec, 
	iposs.movimientos_cc_prov docs, 
	iposs.movimientos_cc_prov_recibos pagos, 
	iposs.proveedores p, 
	iposs.proveedores_empresas pe, 
	iposs.tipos_documentos_inventarios tdi, 
	iposs.grupos_de_pagos g, 
	iposs.monedas mc, 
	iposs.monedas mr
where 	cc.prov_codigo = p.codigo
and 	cc.prov_codigo = pe.prov_codigo
and 	
		cc.gru_pag_codigo = g.codigo
and 	cc.emp_codigo = g.emp_codigo
and 
		cc.prov_codigo = rec.prov_codigo
and 	cc.emp_codigo = rec.emp_codigo
and 	cc.gru_pag_codigo = rec.gru_pag_codigo
and 	
		rec.tip_doc_in_codigo = 11 		-- recibo
and 	
		pagos.rec_numero = rec.numero
and 	pagos.rec_fecha = rec.fecha
and 
		pagos.mov_cc_pr_numero = docs.numero
and 	pagos.mov_cc_pr_fecha = docs.fecha
and 
		docs.tip_doc_in_codigo = tdi.codigo
and 
		rec.mon_codigo = mr.codigo
and 	pagos.mon_codigo = mc.codigo
and 	
		rec.fecha between :FechaDesde and :FechaHasta
and 	
		pe.prov_codigo = :Proveedor
and 
		cc.emp_codigo = $EMPRESA_LOGUEADA$
and 	rec.emp_codigo = $EMPRESA_LOGUEADA$
and 	docs.emp_codigo = $EMPRESA_LOGUEADA$
and 	pagos.emp_codigo = $EMPRESA_LOGUEADA$
and 	pe.emp_codigo = $EMPRESA_LOGUEADA$




/*
Recibo:
	* en movimientos_cc_prov 
		tip_doc_in_codigo = 11
		rec_serie y rec_numero
		fecha
		moneda del recibo
		importe
	* en movimienots_cc_prov_recibos
		una línea por cada comprobante afectado por el recibo
		no hay datos del recibo, solo la referencia a movimientos_cc_prov (rec_fecha, rec_numero)

comprobante
	* en movimienots_cc_prov_recibos
		además de la referencia al comprobante (mov_cc_pr_fecha, mov_cc_pr_numero)
		- importe
		- mon_codigo
		- tipo_cambio (?)
	* en movimientos_cc_prov
		tip_doc_in_codigo = x
		fecha
		rec_serie y rec_numero son los del comprobante (?!)
		comp_codigo y cop_fecha_emision referencian al comprobante (necesario para el total del comprobante)
		moneda del comprobante
		importe e importe_pago

*/

