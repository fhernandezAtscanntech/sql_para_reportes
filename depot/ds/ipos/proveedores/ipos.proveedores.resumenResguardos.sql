select
	ret.mov_cc_pr_fecha "Fecha",
	ret.tipo_comprobante_e_resg "Tipo comp",
	ret.serie_e_resg "Serie",
	ret.numero_e_resg "Nro resg",
	ret.porcentaje "Porc. ret.",
	ret.imp_retencion "Importe ret.",
	tr.descripcion "Tipo retencion",
	ret.tipo_cambio_e_resg "Tipo cambio eResguardo"
from
	mov_cc_prov_rec_ret ret
join tipos_retenciones tr
	on ret.tipo_retencion = tr.codigo
	and ret.emp_codigo = $EMPRESA_LOGUEADA$
	and ret.TIPO_CAMBIO_E_RESG is not null
	and ret.tipo_cambio_e_resg > 0
where 	ret.mov_cc_pr_fecha >= :FechaDesde
and 	ret.mov_cc_pr_fecha <= :FechaHasta + 1 - (1/24/60/60)
