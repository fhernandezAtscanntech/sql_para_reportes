select 
	g.descripcion 		"Grupo de Pago", 
	pe.prov_Cod_Externo 	"Cod.Proveedor", 
	p.razon_social 		"Proveedor", 
	p.nombre_fantasia 	"Nombre Fantas�a", 
	m1.simbolo 			"Moneda 1", 
	m2.simbolo 			"Moneda 2", 
	cc.saldo - 
		(
		select nvl (sum (mcc.importe), 0)	-- tiene signo
		from iposs.movimientos_cc_prov mcc
		where mcc.prov_codigo = cc.prov_codigo
		and mcc.emp_codigo = cc.emp_codigo
		and mcc.gru_pag_codigo = cc.gru_pag_codigo
		and mcc.mon_codigo = cc.mon_codigo
		and fecha > (:FechaLimite + 1 - (1/24/60/60))
		) 				"Saldo 1", 
	cc.saldo2 - 
		(select nvl (sum (mcc.importe), 0)	-- tiene signo
		from iposs.movimientos_cc_prov mcc
		where mcc.prov_codigo = cc.prov_codigo
		and mcc.emp_codigo = cc.emp_codigo
		and mcc.gru_pag_codigo = cc.gru_pag_codigo
		and mcc.mon_codigo = cc.mon_codigo2
		and fecha > (:FechaLimite + 1 - (1/24/60/60))
		) 				"Saldo 2"
from 
	iposs.grupos_de_pagos g, 
	iposs.cuentas_corrientes_prov cc, 
	iposs.proveedores p, 
	iposs.proveedores_empresas pe, 
	iposs.monedas m1, 
	iposs.monedas m2
where 	cc.prov_codigo = p.codigo
and 	cc.prov_codigo = pe.prov_codigo
and 	cc.gru_pag_codigo = g.codigo
and 	cc.mon_codigo = m1.codigo
and 	cc.mon_codigo2 = m2.codigo
and 	cc.gru_pag_codigo = :GrupoPago
and 	pe.emp_codigo = $EMPRESA_LOGUEADA$
and 	cc.emp_codigo = $EMPRESA_LOGUEADA$
and 	g.emp_codigo = $EMPRESA_LOGUEADA$
