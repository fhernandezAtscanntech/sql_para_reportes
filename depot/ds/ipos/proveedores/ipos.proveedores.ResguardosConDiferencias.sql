/*
 * Requerimiento:
 * Contar con un reporte que liste eFacturas y eResguardos que se cerraron con diferencias y dieron lugar a movimiento de ajuste de CC.
 * 
 * Se plantea:
 * Desarrollar el reporte basándose en los movimientos de CC de tipo "ajuste por diferencia" incluyendo tipo de ajuste, monto y comprobantes afectados. 
 * 
 * select * from tipos_documentos_inventarios;
 * CO DESCRIPCION                                                  
 * -- ------------------------------------------------------------
 * :
 * 17 RESGUARDO                                                    
 * :
 * 21 IMEBA                                                        
 * 22 DIF CABEZAL RESGUARDO IMEBA                                  
 * 23 DIF CABEZAL RESGUARDO IVA                                    
 * :
 * 
 * 
 * las parejas son 17-23 y 21-22
*/


select 
	p.razon_social 		"Proveedor", 
	p.ruc 		 		"RUT", 
	pe.prov_cod_externo "Cod.Proveedor", 
	l.emp_codigo 		"Cod.Empresa", 
	l.loc_cod_empresa 	"Cod.Local", 
	l.descripcion 		"Local", 
	comp.codigo 		"Cod.Comprobante", 
	comp.fecha_emision 	"Fecha Emision", 
	comp.serie 			"Serie", 
	comp.numero 		"Número", 
	comp.total_calculado 	"Total Calculado", 
	comp.total_ingresado 	"Total Ingresado", 
	comp.total_iva 			"IVA", 
	comp.imeba 				"IMEBA", 
	mcc2.importe 			"Imp. declarado DGI", 
	mcc1.importe 			"Imp. Ajuste", 
	tdi.descripcion  		"Tipo Ajuste"
from 
	iposs.cuentas_corrientes_prov cc, 
	iposs.proveedores p, 
	iposs.proveedores_empresas pe, 
	iposs.movimientos_cc_prov mcc1,  		-- diferencias
	iposs.movimientos_cc_prov mcc2,  		-- originales
	iposs.comprobantes comp, 
	iposs.locales l ,
	iposs.tipos_documentos_inventarios tdi
where 	cc.prov_codigo = p.codigo
and 	cc.prov_codigo = pe.prov_codigo
and 	cc.emp_codigo = pe.emp_codigo
and 	cc.prov_codigo = mcc1.prov_codigo
and 	cc.emp_codigo = mcc1.emp_codigo
and 	cc.gru_pag_codigo = mcc1.gru_pag_codigo
and 	
		( ( mcc1.tip_doc_in_codigo = 23 and mcc2.tip_doc_in_codigo = 17) -- Dif. Iva e IVA
		or
		( mcc1.tip_doc_in_codigo = 22 and mcc2.tip_doc_in_codigo = 21) ) -- Dif. Imeba e IMEBA
and 	mcc1.comp_codigo = mcc2.comp_codigo
and 	mcc1.emp_codigo = mcc2.emp_codigo
and 	mcc1.comp_fecha_emision = mcc2.comp_fecha_emision
and 	mcc1.comp_codigo = comp.codigo
and 	mcc1.comp_fecha_emision = comp.fecha_emision
and 	mcc1.emp_codigo = comp.emp_codigo
and 	mcc1.tip_doc_in_codigo = tdi.codigo
and 	mcc1.fecha between :FechaDesde and :FechaHasta
and 	comp.loc_codigo = l.codigo 
and 	( trim (upper (':Prov')) = 'NULL' or p.codigo in :Prov )
and 	
		( cc.emp_codigo = $EMPRESA_LOGUEADA$
and 	pe.emp_codigo = $EMPRESA_LOGUEADA$
and 	mcc1.emp_codigo = $EMPRESA_LOGUEADA$
and 	mcc2.emp_codigo = $EMPRESA_LOGUEADA$
and 	comp.emp_codigo = $EMPRESA_LOGUEADA$ )

