select
	l.emp_codigo "Cod.Empresa",
	l.codigo "Cod.Local",
	l.descripcion "Local",
	cre.descripcion  "Crédito",
	cs.descripcion   "Sello",
	p.nombre_1 ||decode (length(p.nombre_2), 0, '', ' ')||
	p.nombre_2 ||decode (length(p.apellido_1), 0, '', ' ')||
	p.apellido_1 ||decode (length(p.apellido_2), 0, '', ' ')||
	p.apellido_2 "Cliente",
	c.cli_cod_externo  "Cod.Cliente",
	nvl ((select g.descripcion
		from iposs.grupos_de_clientes g
		where g.codigo = c.gru_cli_codigo), 'Sin Grupo Asociado')  "Grupo de Clientes",
	t.numero_tarjeta "Tarjeta",
	m.caj_codigo 	"Caja", 
	trunc(m.fecha_comercial) "Fecha",
	tm.descripcion "Tipo Movimiento",
	m.v0_numero_operacion||':'||m.numero_operacion "Comprobante",
	m.importe * (decode(tm.signo, 1, 1, -1)) "Total", 
	mdo.art_codigo 					"Cód.Artículo", 
	(select ae.descripcion
		from articulos_empresas ae, articulos_locales al
		where ae.art_codigo = al.art_codigo
		and ae.emp_codigo = al.emp_codigo
		and al.loc_codigo = mdo.loc_Codigo
		and al.art_Codigo_Externo = mdo.art_codigo) 	"Artículo", 
	mdo.rub_codigo 					"Cód.Rubro", 
	(select r.descripcion
		from rubros r, rubros_locales rl
		where r.codigo = rl.rub_codigo
		and mdo.loc_codigo = rl.loc_codigo
		and rl.rub_codigo_Externo = mdo.rub_codigo) 	"Rubro", 
	mdo.porcentaje_iva 				"IVA Tasa", 
	sum (decode (
		mdo.tip_det_codigo, 
		4, mdo.cantidad, 
		5, -mdo.cantidad, 
		6, mdo.cantidad, 
		7, -mdo.cantidad)) 	"Cantidad", 
	sum (decode (
		mdo.tip_det_codigo, 
		4, mdo.importe, 
		5, -mdo.importe, 
		6, mdo.importe, 
		7, -mdo.importe)) 	"Importe", 
	sum (decode (
		mdo.tip_det_codigo, 
		4, mdo.monto_iva, 
		5, -mdo.monto_iva, 
		6, mdo.monto_iva, 
		7, -mdo.monto_iva))		"IVA"	
from
	iposs.clientes c,
	iposs.cre_clientes c2,
	iposs.personas p,
	iposs.cre_sellos_empresas cse,
	iposs.cre_sellos cs,
	iposs.creditos cre,
	iposs.cre_tarjetas t,
	iposs.cre_tipos_movimientos tm,
	iposs.cre_cuentas_corrientes cc,
	iposs.cre_movimientos_cc m,
	iposs.locales l, 
	iposs.movimientos mo, 
	iposs.movimientos_detalles mdo 
where 	c.per_codigo = p.codigo
and 	c2.per_codigo = p.codigo
and 	c2.cre_sel_codigo = cse.cre_sel_codigo
and 	c2.codigo = cc.cli_codigo
and 	cse.cre_sel_codigo = cs.codigo
and 	cs.cre_codigo = cre.codigo
and 	cs.codigo = cc.cre_sel_codigo
and 	m.cre_cc_codigo = cc.codigo
and 	m.cre_tar_codigo = t.codigo(+)
and 	m.loc_codigo = l.codigo (+)
and 	tm.codigo = m.cre_tm_codigo
and 	m.fecha_comercial between :FechaDesde and :FechaHasta + (1 - (1/24/60/60))
and 	( t.codigo = :Tarjeta )
and 	( cre.codigo = to_number(nvl (iposs.f_param ('CRE_AFINIDAD', l.codigo), 0)) )
and 	mo.emp_codigo = m.emp_codigo
and 	mo.fecha_comercial = trunc(m.fecha_comercial)
and 	mo.v0_numero_operacion = m.v0_numero_operacion
and 	mo.numero_operacion = m.numero_operacion
and 	mo.numero_mov = mdo.mov_numero_mov
and 	mo.fecha_comercial = mdo.fecha_comercial
and 	mo.emp_codigo = mdo.mov_emp_codigo
and 	cse.emp_codigo = $EMPRESA_LOGUEADA$
and 	m.emp_codigo = $EMPRESA_LOGUEADA$
and 	c.emp_codigo = $EMPRESA_LOGUEADA$
and 	mo.emp_codigo = $EMPRESA_LOGUEADA$
and 	mdo.mov_emp_codigo = $EMPRESA_LOGUEADA$
group by
	l.emp_codigo,
	l.codigo,
	l.descripcion,
	cre.descripcion,
	cs.descripcion,
	p.nombre_1 ||decode (length(p.nombre_2), 0, '', ' ')||
	p.nombre_2 ||decode (length(p.apellido_1), 0, '', ' ')||
	p.apellido_1 ||decode (length(p.apellido_2), 0, '', ' ')||
	p.apellido_2,
	c.cli_cod_externo,
	c.gru_cli_codigo, 
	t.numero_tarjeta,
	m.caj_codigo, 
	trunc(m.fecha_comercial),
	tm.descripcion,
	m.v0_numero_operacion||':'||m.numero_operacion,
	m.importe * (decode(tm.signo, 1, 1, -1)), 
	mdo.art_codigo,
	mdo.loc_Codigo, 
	mdo.rub_codigo, 
	mdo.rub_codigo, 
	mdo.porcentaje_iva
