select 
	l.emp_codigo "Cod.Empresa", 
	l.codigo "Cod.Local", 
	l.descripcion "Local", 
	cre.descripcion 	"Crédito", 
	cs.descripcion 		"Sello", 
	p.nombre_1	||decode (length(p.nombre_2), 0, '', ' ')||
	p.nombre_2	||decode (length(p.apellido_1), 0, '', ' ')||
	p.apellido_1	||decode (length(p.apellido_2), 0, '', ' ')||
	p.apellido_2 "Cliente", 
	c.cli_cod_externo 	"Cod.Cliente", 
	nvl ((select g.descripcion 
		from iposs.grupos_de_clientes g
		where g.codigo = c.gru_cli_codigo), 'Sin Grupo Asociado') 	"Grupo de Clientes", 
	t.numero_tarjeta "Tarjeta", 
	trunc(m.fecha_comercial) "Fecha", 
	tm.descripcion "Tipo Movimiento", 
	m.v0_numero_operacion||':'||m.numero_operacion "Comprobante", 
	m.notas 		"Notas", 
	m.importe * (decode(tm.signo, 1, 1, -1)) "Importe"
from 
	iposs.clientes c, 
	iposs.cre_clientes c2, 
	iposs.personas p, 
	iposs.cre_sellos_empresas cse, 
	iposs.cre_sellos cs, 
	iposs.creditos cre, 
	iposs.cre_tarjetas t, 
	iposs.cre_tipos_movimientos tm, 
	iposs.cre_cuentas_corrientes cc, 
	iposs.cre_movimientos_cc m, 
	iposs.locales l
where	c.per_codigo = p.codigo
and 	c2.per_codigo = p.codigo
and 	c2.cre_sel_codigo = cse.cre_sel_codigo
and 	c2.codigo = cc.cli_codigo
and 	cse.cre_sel_codigo = cs.codigo 
and 	cs.cre_codigo = cre.codigo 
and 	cs.codigo = cc.cre_sel_codigo
and 	m.cre_cc_codigo = cc.codigo
and 	m.cre_tar_codigo = t.codigo(+)
and 	m.loc_codigo = l.codigo (+)
and 	tm.codigo = m.cre_tm_codigo
and 	m.fecha_comercial between :FechaDesde and :FechaHasta + (1 - (1/24/60/60))
and 	( trim (upper (':Tarjetas')) = 'NULL' or t.codigo in :Tarjetas )
and 	cre.codigo = to_number(nvl (iposs.f_param ('CRE_AFINIDAD', 9), 0))
and 	cse.emp_codigo = $EMPRESA_LOGUEADA$
and 	m.emp_codigo = $EMPRESA_LOGUEADA$
and 	c.emp_codigo = $EMPRESA_LOGUEADA$
