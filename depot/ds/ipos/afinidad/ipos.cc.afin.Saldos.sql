select 
	l.emp_codigo "Cod.Empresa", 
	l.codigo "Cod.Local", 
	l.descripcion "Local", 
	cre.descripcion 	"Crédito", 
	cs.descripcion 		"Sello", 
	p.nombre_1 ||decode (length(p.nombre_2), 0, '', ' ')||
	p.nombre_2 ||decode (length(p.apellido_1), 0, '', ' ')||
	p.apellido_1 ||decode (length(p.apellido_2), 0, '', ' ')||
	p.apellido_2 "Cliente", 
	c.cli_cod_externo 	"Cod.Cliente", 
	nvl ((select g.descripcion 
		from iposs.grupos_de_clientes g
		where g.codigo = c.gru_cli_codigo), 'Sin Grupo Asociado') 	"Grupo de Clientes", 
	t.numero_tarjeta "Tarjeta", 
	cc.saldo 	"Saldo"
from 
	iposs.clientes c, 
	iposs.clientes_locales cl, 
	iposs.cre_clientes c2, 
	iposs.personas p, 
	iposs.cre_sellos_empresas cse, 
	iposs.cre_sellos cs, 
	iposs.creditos cre, 
	iposs.cre_tarjetas t, 
	iposs.cre_cuentas_corrientes cc, 
	iposs.locales l
where	c.per_codigo = p.codigo
and 	c2.per_codigo = p.codigo
and 	c2.cre_sel_codigo = cse.cre_sel_codigo
and 	c2.codigo = cc.cli_codigo
and 	cse.cre_sel_codigo = cs.codigo 
and 	cs.cre_codigo = cre.codigo 
and 	cs.codigo = cc.cre_sel_codigo
and 	cl.cli_codigo = c.codigo
and 	cl.loc_codigo = l.codigo
and 	t.cre_sel_codigo = cse.cre_sel_codigo
and 	t.cli_codigo = c2.codigo
and 	cre.codigo = to_number(nvl (iposs.f_param ('CRE_AFINIDAD', l.codigo), 0))
and 	l.codigo in :Local
and 	cse.emp_codigo = $EMPRESA_LOGUEADA$
and 	c.emp_codigo = $EMPRESA_LOGUEADA$
