multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
		select
			l.CODIGO_UNICO_LOCAL,
			p.TITULO, 
			p.ID AS "ID_PROMOCION", 
			cast (p.VIGENCIA_DESDE as date) as VIGENCIA_DESDE, 
			cast (p.VIGENCIA_HASTA as date) as VIGENCIA_HASTA, 
			pp.ESTADO, 
			l.DESC_LOCAL AS "NOMBRE LOCAL",
			cast (pp.FECHA_ACEPTACION as date) as FECHA_ACEPTACION,
			cast (pp.FECHA_RECHAZO as date) as FECHA_RECHAZO,
			pp.MODIF_USER,
			e.RAZON_SOCIAL AS FABRICANTE, 
			coalesce (cmp.descripcion, '''||'Sin Campaña'||''') as Campana
		FROM 
			iposs_mp.promociones p 
			INNER JOIN iposs_mp.empresas e ON (p.ID_AUTOR = e.ID)
			INNER JOIN iposs_mp.propuestas_promociones pp ON (pp.ID_PROMOCION = p.ID)
			INNER JOIN iposs_mp.locales l ON (pp.ID_LOCAL_OFERIDO = l.ID)
			LEFT JOIN iposs_mp.campanas cmp on cmp.id = p.id_campana 
		WHERE 	p.VIGENCIA_HASTA >= sysdate
		AND 	
				((pp.FECHA_ACEPTACION >= trunc(sysdate) - '||:Dias||' and pp.fecha_aceptacion < trunc(sysdate))
				OR 
				(pp.FECHA_RECHAZO >= trunc(sysdate) - '||:Dias||' and pp.fecha_rechazo < trunc(sysdate)))
		AND 	pp.FECHA_BORRADO is null
		' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
	select
		l.CODIGO_UNICO_LOCAL,
		p.TITULO, 
		p.ID AS "ID_PROMOCION", 
		cast (p.VIGENCIA_DESDE as date)	as VIGENCIA_DESDE, , 
		cast (p.VIGENCIA_HASTA as date)	as VIGENCIA_HASTA, , 
		pp.ESTADO, 
		l.DESC_LOCAL AS "NOMBRE LOCAL",
		cast (pp.FECHA_ACEPTACION as date) as FECHA_ACEPTACION,
		cast (pp.FECHA_RECHAZO as date) as FECHA_RECHAZO,
		pp.MODIF_USER,
		e.RAZON_SOCIAL AS FABRICANTE
	FROM 
		iposs_mp.promociones p 
			INNER JOIN iposs_mp.empresas e ON (p.ID_AUTOR = e.ID)
			INNER JOIN iposs_mp.propuestas_promociones pp ON (pp.ID_PROMOCION = p.ID)
			INNER JOIN iposs_mp.locales l ON (pp.ID_LOCAL_OFERIDO = l.ID)
			LEFT JOIN iposs_mp.campanas cmp on cmp.id = p.id_campana 
	WHERE 	1 = 2
)

######################
consulta
######################
select 1 from #reco#

