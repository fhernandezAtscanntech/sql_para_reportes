SELECT
    CAST(audit_date AS DATE) as date,
    MAX(audit_date) AS fechaUltimoRecibido,
    empresa,
    usuario, 
    case
        when empresa >= 26000 and empresa <= 27099 then 'ipossb2br'
        when empresa >= 24000 and empresa <= 25999 then 'ipossb3br'
        when empresa >= 22000 and empresa <= 23599 then 'ipossb4br'
        when empresa >= 27100 and empresa <= 28500 then 'ipossb5br'
        else CAST (empresa as varchar)
     end as base,
    metodo,
    status_code,
    jsonb_res->>'appErrorCode' as appErrorCode,
    count(*)
FROM auditoria_integradores
WHERE
        audit_date >= :Fecha AND
        audit_date <= :Fecha + 1 - (1 / 24 / 60 / 60) AND
        empresa = :Empresa AND
        base not in ('bkpbr2')
GROUP BY CAST(audit_date AS DATE), empresa, usuario,  base, metodo, status_code, jsonb_res->>'appErrorCode'
ORDER BY CAST(audit_date AS DATE), empresa, usuario, base, metodo, status_code, jsonb_res->>'appErrorCode'
