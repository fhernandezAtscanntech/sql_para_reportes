select
	p.id 					"Prom - Id" ,
	p.codigo_interno_be 	"Prom - codigo_interno_be",
	p.id_externo 			"Prom - id_externo",
	p.estereotipo 			"Prom - estereotipo",
	p.titulo 				"Prom - titulo",
	p.estado 				"Prom - estado",
	p.estado_be 			"Prom - estado_be",
	p.tipo_promocion 		"Prom - tipo_promocion",
	pp.id 					"Prop.Prom - Id", 
	pp.estado 				"Prop.Prom - estado", 
	pp.estado_distribucion 	"P.P. - estado_distribucion", 
	pp.id_local_oferido 	"Prop.Prom - id_local_oferido", 
	pp.id_promocion 		"Prop.Prom - id_promocion", 
	pp.fecha_propuesta 		"Prop.Prom - fecha_propuesta", 
	pp.fecha_aceptacion_terminos 	"P.P - fec_acepta_terminos", 
	pp.fecha_aceptacion 	"Prop.Prom - fecha_aceptacion", 
	pp.fecha_rechazo 		"Prop.Prom - fecha_rechazo"
from
	iposs_mp.propuestas_promociones pp,
	iposs_mp.promociones p
where	pp.estado_distribucion not in ('DISTRIBUIDA', 'PENDIENTE')
and 	p.vigencia_hasta > sysdate
and 	p.fecha_borrado is null
and 	p.id = pp.id_promocion
and not exists (select 1 from iposs_mp.op_promociones opp where opp.id_promocion=p.id)
and not exists (select 1 from iposs_mp.op_propuestas_promociones oppp where oppp.id_propuesta=pp.id)