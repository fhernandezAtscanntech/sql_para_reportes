/*
Reporte creado para Valeria Correa

Los clientes que particiapn de promociones pero no contrataron BE necesitan poder acceder para aprobar las promociones a la página de eMarket.
Ernesto crea los usuarios "a granel" asociándolos a un grupo "MARKETPLACE", y tengo que mostrar usuario y contraseña para que Valeria 
se las pueda pasar y accedan a la página

*/

multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
		select 
			e.codigo 	"Cod.Empresa", 
			e.descripcion 	"Empresa", 
			l.codigo 		"Cod.Local", 
			l.loc_cod_empresa 	"Sucursal", 
			l.descripcion 	"Local", 
			r.ruc 			"RUT", 
			r.descripcion 	"Razón Social", 
			f.login 		"Login", 
			f.contraseña 	"Contraseña"
		from 
			iposs.empresas e, 
			iposs.locales l, 
			iposs.razones_sociales r, 
			iposs.funcionarios f, 
			iposs.funcionarios_locales fl, 
			iposs.grupos_de_funcionarios g
		where 	f.codigo = fl.fun_codigo
		and 	l.codigo = fl.loc_codigo
		and 	e.codigo = l.emp_codigo
		and 	r.codigo = l.raz_soc_codigo
		and 	f.gru_fun_codigo = g.codigo
		and 	g.descripcion = '''||'MARKETPLACE'||'''
		and 	l.coord_x is null
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
	select
		e.codigo 	"Cod.Empresa", 
		e.descripcion 	"Empresa", 
		l.codigo 		"Cod.Local", 
		l.loc_cod_empresa 	"Sucursal", 
		l.descripcion 	"Local", 
		r.ruc 			"RUT", 
		r.descripcion 	"Razón Social", 
		f.login 		"Login", 
		f.contraseña 	"Contraseña"
	from 
		iposs.empresas e, 
		iposs.locales l, 
		iposs.razones_sociales r, 
		iposs.funcionarios f, 
		iposs.funcionarios_locales fl, 
		iposs.grupos_de_funcionarios g
	where 1 = 2
)

######################
consulta
######################
select 1 from #reco#

