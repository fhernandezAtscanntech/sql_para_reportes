select 
	a.emp_codigo, 
	a.loc_codigo, 
	a.caj_codigo, 
	e.descripcion as Empresa, 
	l.descripcion as Local, 
	a.fecha, 
	a.tipo, 
	a.solicitud, 
	a.numero_operacion, 
	a.autorizacion, 
	a.monto, 
	a.mon_codigo, 
	m.simbolo, 
	a.documento, 
	case a.estado
		when 41 then 'DENEGADO'
		when 40 then 'ERROR' 
		when 30 then 'ANULADO'
		when 10 then 'REVERSADO'
		when 1  then 'APROBADO'
		when 0  then 'PENDIENTE'
		else 'DESCONOCIDO'
	end as estado, 
	a.codigo_respuesta, 
	a.numero_mov_cc
from 
	(select m.EMP_CODIGO, m.LOC_CODIGO, m.CAJ_CODIGO, 
		t.audit_date as fecha,
		'DESEMBOLSO' as tipo,             
		t.SOLICITUD,
		m.NUMERO_OPERACION,
		t.AUTORIZACION,
		t.MONTO,
		m.MON_CODIGO,
		t.DOCUMENTO,
		t.estado, 
		t.cod_respuesta as codigo_respuesta, 
		mcc.numero_mov_cc
	from ACAC_DESEMBOLSO t
	inner join iposs.mt_movimientos_cc mcc
		on mcc.reference_number = t.autorizacion
		and mcc.numero_documento = t.solicitud
		and mcc.importe = t.monto
	inner join IPOSS.RC_CON_MOV_CAJA m
		on t.CON_MOV_CAJ_CODIGO = m.CON_MOV_CODIGO
	where mcc.sello = 'ACAC'
	and t.audit_date BETWEEN nvl(to_date('&1', 'dd/mm/yyyy'), trunc(sysdate-1)) AND nvl(to_date('&2', 'dd/mm/yyyy'), trunc(sysdate-1))+1-(1/24/60)
	and t.estado = 1
	union
	select m.EMP_CODIGO, m.LOC_CODIGO, m.CAJ_CODIGO, 
		t.audit_date as fecha,
		'COBRANZA' as tipo,             
		0 as "SOLICITUD",
		m.NUMERO_OPERACION,
		t.AUTORIZACION,
		t.MONTO,
		m.MON_CODIGO,
		t.DOCUMENTO,
		t.estado, 
		t.cod_respuesta as codigo_respuesta, 
		mcc.numero_mov_cc
	from ACAC_COBRANZA t
	inner join IPOSS.RC_CON_MOV_CAJA m
		on t.CON_MOV_CAJ_CODIGO = m.CON_MOV_CODIGO
	inner join iposs.rc_movimientos_cc mcc
		on mcc.con_mov_codigo = m.con_mov_codigo
	where mcc.compañia = 'ACA'
	and t.audit_date BETWEEN nvl(to_date('&1', 'dd/mm/yyyy'), trunc(sysdate-1)) AND nvl(to_date('&2', 'dd/mm/yyyy'), trunc(sysdate-1))+1-(1/24/60)
	and t.estado = 1
	union
	select m.EMP_CODIGO, m.LOC_CODIGO, m.CAJ_CODIGO, 
		t.audit_date as fecha,
		'CANCELACION ANTICIPADA' as tipo,             
		t.SOLICITUD,
		m.NUMERO_OPERACION,
		t.AUTORIZACION,
		t.MONTO,
		m.MON_CODIGO,
		t.DOCUMENTO,
		t.estado, 
		t.cod_respuesta as codigo_respuesta, 
		mcc.numero_mov_cc
	from IPOSS.ACAC_CANCELACION_ANTICIPADA t
	inner join IPOSS.RC_CON_MOV_CAJA m
		on t.CON_MOV_CAJ_CODIGO = m.CON_MOV_CODIGO
	inner join iposs.rc_movimientos_cc mcc
		on mcc.con_mov_codigo = m.con_mov_codigo
	where mcc.compañia = 'ACA'
	and t.audit_date BETWEEN nvl(to_date('&1', 'dd/mm/yyyy'), trunc(sysdate-1)) AND nvl(to_date('&2', 'dd/mm/yyyy'), trunc(sysdate-1))+1-(1/24/60)
	and t.estado = 1
	) a 
inner join iposs.empresas e on e.codigo = a.emp_codigo
inner join iposs.locales l on l.codigo = a.loc_codigo
left join iposs.monedas m on m.codigo = a.mon_codigo

#############################
multiple
#############################

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select
	'select 
		a.emp_codigo, 
		a.loc_codigo, 
		a.caj_codigo, 
		e.descripcion as Empresa, 
		l.descripcion as Local, 
		a.fecha, 
		a.tipo, 
		a.solicitud, 
		a.numero_operacion, 
		a.autorizacion, 
		a.monto, 
		a.mon_codigo, 
		m.simbolo, 
		a.documento, 
		case a.estado
			when 41 then '''||'DENEGADO'  ||'''
			when 40 then '''||'ERROR'     ||'''
			when 30 then '''||'ANULADO'   ||'''
			when 10 then '''||'REVERSADO' ||'''
			when 1  then '''||'APROBADO'  ||'''
			when 0  then '''||'PENDIENTE' ||'''
			else '''||'DESCONOCIDO'||'''
		end as estado, 
		a.codigo_respuesta, 
		a.numero_mov_cc
	from 
		(select m.EMP_CODIGO, m.LOC_CODIGO, m.CAJ_CODIGO, 
			t.audit_date as fecha,
			'''||'DESEMBOLSO'||''' as tipo,             
			t.SOLICITUD,
			m.NUMERO_OPERACION,
			t.AUTORIZACION,
			t.MONTO,
			m.MON_CODIGO,
			t.DOCUMENTO,
			t.estado, 
			t.cod_respuesta as codigo_respuesta, 
			mcc.numero_mov_cc
		from ACAC_DESEMBOLSO t
		inner join IPOSS.RC_CON_MOV_CAJA m
			on t.CON_MOV_CAJ_CODIGO = m.CON_MOV_CODIGO
		inner join iposs.mt_movimientos_cc mcc
			on mcc.reference_number = t.autorizacion
			and mcc.numero_documento = t.solicitud
			and mcc.importe = t.monto
		where mcc.sello = '''||'ACAC'||'''
		and t.audit_date BETWEEN nvl(to_date('''||:FechaDesde||''', '''||'dd/mm/yyyy'||'''), trunc(sysdate-1)) AND nvl(to_date('''||:FechaHasta||''', '''||'dd/mm/yyyy'||'''), trunc(sysdate-1))+1-(1/24/60/60)
		and t.estado = 1
		union
		select m.EMP_CODIGO, m.LOC_CODIGO, m.CAJ_CODIGO, 
			t.audit_date as fecha,
			'''||'COBRANZA'||''' as tipo,             
			0 as "SOLICITUD",
			m.NUMERO_OPERACION,
			t.AUTORIZACION,
			t.MONTO,
			m.MON_CODIGO,
			t.DOCUMENTO,
			t.estado, 
			t.cod_respuesta as codigo_respuesta, 
			mcc.numero_mov_cc
		from ACAC_COBRANZA t
		inner join IPOSS.RC_CON_MOV_CAJA m
			on t.CON_MOV_CAJ_CODIGO = m.CON_MOV_CODIGO
		inner join iposs.rc_movimientos_cc mcc
			on mcc.con_mov_codigo = m.con_mov_codigo
		where mcc.compañia = '''||'ACA'||'''
		and t.audit_date BETWEEN nvl(to_date('''||:FechaDesde||''', '''||'dd/mm/yyyy'||'''), trunc(sysdate-1)) AND nvl(to_date('''||:FechaHasta||''', '''||'dd/mm/yyyy'||'''), trunc(sysdate-1))+1-(1/24/60/60)
		and t.estado = 1
		union
		select m.EMP_CODIGO, m.LOC_CODIGO, m.CAJ_CODIGO, 
			t.audit_date as fecha,
			'''||'CANCELACION ANTICIPADA'||''' as tipo,             
			t.SOLICITUD,
			m.NUMERO_OPERACION,
			t.AUTORIZACION,
			t.MONTO,
			m.MON_CODIGO,
			t.DOCUMENTO,
			t.estado, 
			t.cod_respuesta as codigo_respuesta, 
			mcc.numero_mov_cc
		from IPOSS.ACAC_CANCELACION_ANTICIPADA t
		inner join IPOSS.RC_CON_MOV_CAJA m
			on t.CON_MOV_CAJ_CODIGO = m.CON_MOV_CODIGO
		inner join iposs.rc_movimientos_cc mcc
			on mcc.con_mov_codigo = m.con_mov_codigo
		where mcc.compañia = '''||'ACA'||'''
		and t.audit_date BETWEEN nvl(to_date('''||:FechaDesde||''', '''||'dd/mm/yyyy'||'''), trunc(sysdate-1)) AND nvl(to_date('''||:FechaHasta||''', '''||'dd/mm/yyyy'||'''), trunc(sysdate-1))+1-(1/24/60/60)
		and t.estado = 1
		) a 
	inner join iposs.empresas e on e.codigo = a.emp_codigo
	inner join iposs.locales l on l.codigo = a.loc_codigo
	left join iposs.monedas m on m.codigo = a.mon_codigo
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
	select 
		a.emp_codigo, 
		a.loc_codigo, 
		a.caj_codigo, 
		e.descripcion as Empresa, 
		l.descripcion as Local, 
		a.fecha, 
		a.solicitud, 
		a.tipo, 
		a.numero_operacion, 
		a.autorizacion, 
		a.monto, 
		a.mon_codigo, 
		m.simbolo, 
		a.documento, 
		'DESCONOCIDO' estado, 
		a.codigo_respuesta, 
		a.numero_mov_cc
	from 
		(select m.EMP_CODIGO, m.LOC_CODIGO, m.CAJ_CODIGO, 
			t.audit_date as fecha,
			'DESEMBOLSO' as tipo,             
			t.SOLICITUD,
			m.NUMERO_OPERACION,
			t.AUTORIZACION,
			t.MONTO,
			m.MON_CODIGO,
			t.DOCUMENTO,
			t.estado, 
			t.cod_respuesta as codigo_respuesta, 
			mcc.numero_mov_cc
		from ACAC_DESEMBOLSO t
		inner join iposs.mt_movimientos_cc mcc
			on mcc.reference_number = t.autorizacion
			and mcc.numero_documento = t.solicitud
			and mcc.importe = t.monto
		inner join IPOSS.RC_CON_MOV_CAJA m
			on t.CON_MOV_CAJ_CODIGO = m.CON_MOV_CODIGO
		where 1 = 2 
		union
		select m.EMP_CODIGO, m.LOC_CODIGO, m.CAJ_CODIGO, 
			t.audit_date as fecha,
			'COBRANZA' as tipo,             
			0 as "SOLICITUD",
			m.NUMERO_OPERACION,
			t.AUTORIZACION,
			t.MONTO,
			m.MON_CODIGO,
			t.DOCUMENTO,
			t.estado, 
			t.cod_respuesta as codigo_respuesta, 
			mcc.numero_mov_cc
		from ACAC_COBRANZA t
		inner join IPOSS.RC_CON_MOV_CAJA m
			on t.CON_MOV_CAJ_CODIGO = m.CON_MOV_CODIGO
		inner join iposs.rc_movimientos_cc mcc
			on mcc.con_mov_codigo = m.con_mov_codigo
		where 1 = 2 
		union
		select m.EMP_CODIGO, m.LOC_CODIGO, m.CAJ_CODIGO, 
			t.audit_date as fecha,
			'CANCELACION ANTICIPADA' as tipo,             
			t.SOLICITUD,
			m.NUMERO_OPERACION,
			t.AUTORIZACION,
			t.MONTO,
			m.MON_CODIGO,
			t.DOCUMENTO,
			t.estado, 
			t.cod_respuesta as codigo_respuesta, 
			mcc.numero_mov_cc
		from IPOSS.ACAC_CANCELACION_ANTICIPADA t
		inner join IPOSS.RC_CON_MOV_CAJA m
			on t.CON_MOV_CAJ_CODIGO = m.CON_MOV_CODIGO
		inner join iposs.rc_movimientos_cc mcc
			on mcc.con_mov_codigo = m.con_mov_codigo
		where 1 = 2 
		) a 
	inner join iposs.empresas e on e.codigo = a.emp_codigo
	inner join iposs.locales l on l.codigo = a.loc_codigo
	left join iposs.monedas m on m.codigo = a.mon_codigo
	where 1 = 2 
	)

######################
consulta
######################
select 1 from #reco#

