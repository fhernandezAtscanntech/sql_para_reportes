select
	cre.emp_codigo 	 		"Cod.Empresa",
	l.codigo 				"Cod.Local", 
	l.loc_cod_empresa		"Cod.Sucursal", 
	e.descripcion 			"Empresa", 
	l.descripcion 			"Local", 
	cre.caj_codigo 			"Caja", 
	cre.nro_remito 			"Nro.Remito", 
	cre.cantidad_unidades 	"Unidades", 
	cre.fecha_operacion 	"Fecha Op.", 
	cre.audit_date 			"Audit.Date", 
	cre.audit_user 			"Audit.User", 
	cre.audit_number 		"Nro.Auditoria", 
	cre.tipo 				"Tipo", 
	cre.estado 				"Estado"
from
	iposs.cre_conaprole cre, 
	iposs.empresas e, 
	iposs.locales l
where 	cre.emp_codigo = e.codigo
and 	cre.emp_codigo = l.emp_codigo
and 	cre.loc_codigo = l.loc_cod_empresa
and 	cre.audit_date >= :FechaDesde
and 	cre.audit_date <= :FechaHasta + (1 - (1/24/60/60))
and 	(:empCodigo is null or cre.EMP_CODIGO = :empCodigo)
