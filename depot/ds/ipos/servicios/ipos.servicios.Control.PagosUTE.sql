select
	rmc.FECHA_OPERACION 	"Fecha",
	rmc.EMP_CODIGO 			"Cod.Empresa",
	CODIGO_AGENCIA 			"Cod.Agencia",
	REFERENCIA_PAGO 		"Referencia Pago",
	CUENTA 					"Cuenta",
	FACTURA 				"Factura",
	IMPORTE_TOTAL 			"Importe",
	ID_CONFIRMACION 		"Id Confirmaci�n"
from
	ute_pagos_factura upf
	join rc_movimientos_cc rmc
		on rmc.con_mov_codigo = upf.con_mov_caj_codigo
where
	rmc.compa�ia = 'UTE'
and 	rmc.fecha >= :FechaDesde
and 	rmc.fecha <= :FechaHasta + (1 - (1/24/60/60))
and 	(:empCodigo is null or rmc.EMP_CODIGO = :empCodigo)
/
