#######################################################################################################################################
#######################################################################################################################################
original (recibida https://scanntech.atlassian.net/browse/RME-3202)
#######################################################################################################################################
#######################################################################################################################################

select /*csv*/ distinct svServiciosCom.AUDIT_DATE as "Fecha Alta", loc.emp_codigo||loc.CODIGO as "CodigoSC",tabla.CAJ_CODIGO as "Caja", loc.DESCRIPCION as "Nombre Fantasia", raz.DESCRIPCION as "Razon_Social", loc.DIRECCION as "Direccion",loc.TELEFONO as "Telefono",dep.DESCRIPCION as "Departamento", crediConf.TERMINAL as "Terminal"
from distribuciones_pendientes tabla join locales loc on tabla.loc_codigo = loc.CODIGO
 full join LOCALIDADES loca on loc.LOCA_CODIGO = loca.CODIGO
 full join DEPARTAMENTOS dep on loca.DEPA_CODIGO = dep.CODIGO
 full join RAZONES_SOCIALES raz on loc.RAZ_SOC_CODIGO = raz.CODIGO
 full join RC_CUENTAS_CORRIENTES_LOCALES rcCuentasLoc on rcCuentasLoc.EMP_CODIGO =loc.emp_codigo and rcCuentasLoc.LOC_CODIGO = loc.CODIGO and rcCuentasLoc."COMPA�IA" = 'ANC'
 full join RC_CUENTAS_CORRIENTES rccc on rcCuentasLoc.CUE_COR_CODIGO = rccc.CODIGO
 full join CREDITOS_CONF_CAJAS crediConf on tabla.EMP_CODIGO = crediConf.EMP_CODIGO and tabla.LOC_CODIGO= crediConf.LOC_CODIGO and tabla.CAJ_CODIGO = crediConf.CAJ_CODIGO and crediConf.CRE_CODIGO = 725
 full join SV_SERVICIOS_AUDIT svServiciosCom on svServiciosCom.LOC_CODIGO = tabla.LOC_CODIGO and svServiciosCom.CAJ_CODIGO = tabla.CAJ_CODIGO
where tabla.FECHA_ULT_CONSULTA > sysdate - 7 and rccc.SALDO > 1000 and svServiciosCom.campo in ('Terminal') and trunc(svServiciosCom.AUDIT_DATE) > trunc(sysdate) - 100 and svServiciosCom.SV_CODIGO = 46 and crediConf.COMERCIO = '20691221'
      and tabla.EMP_CODIGO not in (5000,5001,5002,5003,5004,5005)
order by 1,2,3; 

27/03/2017: nueva original:

select /*csv*/ distinct svServiciosCom.AUDIT_DATE as "Fecha Alta", loc.emp_codigo||loc.CODIGO as "CodigoSC",tabla.CAJ_CODIGO as "Caja", loc.DESCRIPCION as "Nombre Fantasia",	raz.DESCRIPCION as "Razon_Social",	loc.DIRECCION as "Direccion",loc.TELEFONO as "Telefono",dep.DESCRIPCION as "Departamento", crediConf.TERMINAL as "Terminal", case when rccc.SALDO <1000 then 'No' else 'Si' end as "+1000 Saldo"
from distribuciones_pendientes tabla join locales loc on tabla.loc_codigo = loc.CODIGO
 full join LOCALIDADES loca on loc.LOCA_CODIGO = loca.CODIGO
 full join DEPARTAMENTOS dep on loca.DEPA_CODIGO = dep.CODIGO
 full join RAZONES_SOCIALES raz on loc.RAZ_SOC_CODIGO = raz.CODIGO
 full join RC_CUENTAS_CORRIENTES_LOCALES rcCuentasLoc on rcCuentasLoc.EMP_CODIGO =loc.emp_codigo and rcCuentasLoc.LOC_CODIGO =  loc.CODIGO and rcCuentasLoc."COMPA�IA" = 'ANC'
 full join RC_CUENTAS_CORRIENTES rccc on rcCuentasLoc.CUE_COR_CODIGO = rccc.CODIGO
 full join CREDITOS_CONF_CAJAS crediConf on tabla.EMP_CODIGO = crediConf.EMP_CODIGO and tabla.LOC_CODIGO= crediConf.LOC_CODIGO and tabla.CAJ_CODIGO = crediConf.CAJ_CODIGO and crediConf.CRE_CODIGO = 725
 full join SV_SERVICIOS_AUDIT svServiciosCom on  svServiciosCom.LOC_CODIGO = tabla.LOC_CODIGO and svServiciosCom.CAJ_CODIGO = tabla.CAJ_CODIGO
where tabla.FECHA_ULT_CONSULTA > sysdate - 7 and svServiciosCom.campo in ('Terminal') and  trunc(svServiciosCom.AUDIT_DATE) > trunc(sysdate) - 100 and svServiciosCom.SV_CODIGO = 46 and  crediConf.COMERCIO = '20691221'
      and tabla.EMP_CODIGO not in (5000,5001,5002,5003,5004,5005)
order by 10 desc,1,2,3;



#######################################################################################################################################
#######################################################################################################################################
modificada
#######################################################################################################################################
#######################################################################################################################################


select /*csv*/ distinct 
	svServiciosCom.AUDIT_DATE as "Fecha Alta", 
	loc.emp_codigo||loc.CODIGO as "CodigoSC", 
	tabla.CAJ_CODIGO as "Caja", 
	loc.DESCRIPCION as "Nombre Fantasia", 
	raz.DESCRIPCION as "Razon_Social", 
	loc.DIRECCION as "Direccion", 
	loc.TELEFONO as "Telefono", 
	dep.DESCRIPCION as "Departamento", 
	crediConf.TERMINAL as "Terminal", 
	case when rccc.SALDO < 1000 then 'No' else 'Si' end as "+1000 Saldo"
from 
	iposs.distribuciones_pendientes tabla, 
	iposs.locales loc, 
	iposs.localidades loca, 
	iposs.departamentos dep, 
	iposs.razones_sociales raz, 
	iposs.rc_cuentas_corrientes_locales rcCuentasLoc, 
	iposs.rc_cuentas_corrientes rccc, 
	iposs.creditos_conf_cajas crediConf, 
	iposs.sv_servicios_audit svServiciosCom
where 	tabla.loc_codigo = loc.CODIGO
and 	loc.LOCA_CODIGO = loca.CODIGO (+)
and 	loca.DEPA_CODIGO = dep.CODIGO (+)
and 	loc.RAZ_SOC_CODIGO = raz.CODIGO (+)
and 	
		rcCuentasLoc.EMP_CODIGO =loc.emp_codigo (+)
and 	rcCuentasLoc.LOC_CODIGO = loc.CODIGO (+)
and 	rcCuentasLoc.COMPA�IA = 'ANC' 
and 
		rcCuentasLoc.CUE_COR_CODIGO = rccc.CODIGO (+)
and 	
		tabla.EMP_CODIGO = crediConf.EMP_CODIGO (+)
and 	tabla.LOC_CODIGO= crediConf.LOC_CODIGO (+)
and 	tabla.CAJ_CODIGO = crediConf.CAJ_CODIGO (+)
and 	crediConf.CRE_CODIGO = 725
and 
		svServiciosCom.LOC_CODIGO = tabla.LOC_CODIGO (+)
and 	svServiciosCom.CAJ_CODIGO = tabla.CAJ_CODIGO(+)
and 
		tabla.FECHA_ULT_CONSULTA > sysdate - 7 
-- and 	rccc.SALDO > 1000 
and 	svServiciosCom.campo in ('Terminal') 
and 	trunc (svServiciosCom.AUDIT_DATE) > trunc (sysdate) - 100 
and 	svServiciosCom.SV_CODIGO = 46 
and 	crediConf.COMERCIO = '20691221'
and 	tabla.EMP_CODIGO not in (5000,5001,5002,5003,5004,5005)
order by 1,2,3; 

########################################################################################
########################################################################################
origen de datos definitivo:
########################################################################################
########################################################################################

multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select
	'select /*csv*/ distinct 
		svServiciosCom.AUDIT_DATE as "Fecha Alta", 
		loc.emp_codigo||loc.CODIGO as "CodigoSC", 
		tabla.CAJ_CODIGO as "Caja", 
		loc.DESCRIPCION as "Nombre Fantasia", 
		raz.DESCRIPCION as "Razon_Social", 
		loc.DIRECCION as "Direccion", 
		loc.TELEFONO as "Telefono", 
		dep.DESCRIPCION as "Departamento", 
		crediConf.TERMINAL as "Terminal", 
		case when rccc.SALDO < 1000 then '''||'No'||''' else '''||'Si'||''' end as "Saldo +1000"
	from 
		iposs.distribuciones_pendientes tabla, 
		iposs.locales loc, 
		iposs.localidades loca, 
		iposs.departamentos dep, 
		iposs.razones_sociales raz, 
		iposs.rc_cuentas_corrientes_locales rcCuentasLoc, 
		iposs.rc_cuentas_corrientes rccc, 
		iposs.creditos_conf_cajas crediConf, 
		iposs.sv_servicios_audit svServiciosCom
	where 	tabla.loc_codigo = loc.CODIGO
	and 	loc.LOCA_CODIGO = loca.CODIGO (+)
	and 	loca.DEPA_CODIGO = dep.CODIGO (+)
	and 	loc.RAZ_SOC_CODIGO = raz.CODIGO (+)
	and 	
			rcCuentasLoc.EMP_CODIGO =loc.emp_codigo (+)
	and 	rcCuentasLoc.LOC_CODIGO = loc.CODIGO (+)
	and 	rcCuentasLoc.COMPA�IA = '''||'ANC'||'''
	and 
			rcCuentasLoc.CUE_COR_CODIGO = rccc.CODIGO (+)
	and 	
			tabla.EMP_CODIGO = crediConf.EMP_CODIGO (+)
	and 	tabla.LOC_CODIGO= crediConf.LOC_CODIGO (+)
	and 	tabla.CAJ_CODIGO = crediConf.CAJ_CODIGO (+)
	and 	crediConf.CRE_CODIGO = 725
	and 
			svServiciosCom.LOC_CODIGO = tabla.LOC_CODIGO (+)
	and 	svServiciosCom.CAJ_CODIGO = tabla.CAJ_CODIGO(+)
	and 
			tabla.FECHA_ULT_CONSULTA > sysdate - 7 
	and 	svServiciosCom.campo in ('''||'Terminal'||''') 
	and 	trunc (svServiciosCom.AUDIT_DATE) > trunc (sysdate) - 100 
	and 	svServiciosCom.SV_CODIGO = 46 
	and 	crediConf.COMERCIO = '''||'20691221'||'''
	and 	tabla.EMP_CODIGO not in (5000,5001,5002,5003,5004,5005)
	order by 1,2,3
	' as sql from dual
)


######################
tabla
######################

create table $$tabla$$ as (
	select  /*csv*/ distinct 
		svServiciosCom.AUDIT_DATE as "Fecha Alta", 
		loc.emp_codigo||loc.CODIGO as "CodigoSC", 
		tabla.CAJ_CODIGO as "Caja", 
		loc.DESCRIPCION as "Nombre Fantasia", 
		raz.DESCRIPCION as "Razon_Social", 
		loc.DIRECCION as "Direccion", 
		loc.TELEFONO as "Telefono", 
		dep.DESCRIPCION as "Departamento", 
		crediConf.TERMINAL as "Terminal", 
		'Si' as "Saldo +1000"
	from 
		iposs.distribuciones_pendientes tabla, 
		iposs.locales loc, 
		iposs.localidades loca, 
		iposs.departamentos dep, 
		iposs.razones_sociales raz, 
		iposs.rc_cuentas_corrientes_locales rcCuentasLoc, 
		iposs.rc_cuentas_corrientes rccc, 
		iposs.creditos_conf_cajas crediConf, 
		iposs.sv_servicios_audit svServiciosCom
	where 1 = 2
)

######################
consulta
######################
select 1 from #reco#
