select 
	CASE cob.ESTATUS 
		WHEN 1 then 'PENDIENTE' 
		WHEN 2 then 'REVERSADA' 
		WHEN 3 then 'CONFIRMADA' 
		WHEN 4 then 'CON ERROR' 
		WHEN 5 then 'ANULADA POR CLIENTE' 
		WHEN 6 then 'ERROR AL ANULAR' 
		WHEN 7 then 'NECESITA REVISION MANUAL' 
	END 		"Estado",
	CASE 
		WHEN SUBSTR(cob.CODIGO_BARRAS, 2, 1) = 1  THEN 'MOVIL' 
	ELSE 
		CASE 
			WHEN SUBSTR(cob.CODIGO_BARRAS,2,1) = 2  THEN 'FIJO'
		ELSE 'DESCONOCIDO' 
		END 
	END 		"Tipo Servicio", 
	cob.FECHA_CONF 		"Fecha", 
	mov.EMP_CODIGO 		"Cod.Empresa", 
	mov.LOC_CODIGO 		"Cod.Local", 
	mov.CAJ_CODIGO 		"Caja", 
	cob.IMPORTE 		"Importe", 
	substr (cob.CODIGO_BARRAS,52,8) "Contrato", 
	cob.NRO_TRANSACCION 	"Nro.Transacción", 
	cob.ERROR_CODE 			"Cod. Error", 
	cob.NRO_TRANSAC_ANTEL 	"Nro.Transacción Antel", 
	ID_CONCILIACION 		"Id Conciliacion", 
	cob.CODIGO_BARRAS 		"Codigo Barras"
from 
	ANTEL_COBRANZAS cob 
	join RC_MOVIMIENTOS_CC mov on mov.CON_MOV_CODIGO = cob.CON_MOV_CODIGO
where 	mov.fecha > :FechaDesde 
and  	mov.fecha < :FechaHasta + (1 - (1/24/60/60))
and 	(:empCodigo is null or mov.emp_codigo = :empCodigo)
