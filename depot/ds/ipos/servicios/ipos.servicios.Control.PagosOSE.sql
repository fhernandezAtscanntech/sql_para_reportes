select 
	CASE cob.ESTADO_COBRANZA 
		WHEN 1 then 'PENDIENTE' 
		WHEN 2 then 'REVERSADA' 
		WHEN 3 then 'CONFIRMADA' 
		WHEN 4 then 'CON ERROR' 
		WHEN 5 then 'ANULADA POR CLIENTE' 
		WHEN 6 then 'CONCILIADA' 
	END 		"Estado",
	mov.EMP_CODIGO 		"Cod.Empresa", 
	mov.LOC_CODIGO 		"Cod.Local", 
	mov.CAJ_CODIGO 		"Caja", 
	cob.ID_FACTURA 		"Id Factura", 
	cob.NRO_CUENTA 		"Nro.Cuenta", 
	cob.FECHA_PAGO 		"Fecha Pago", 
	cob.IMPORTE 		"Importe", 
	cob.NRO_CHEQUE 		"Nro.Cheque", 
	cob.ID_TRANSACCION 	"Id Transacci�n", 
	CASE cob.CONSUMIDOR_FINAL
		WHEN 1 then 'SI'
		ELSE 'NO'
	END 				"Consumidor Final", 
	CASE cob.METODO_PAGO 
		WHEN 1 then 'Efectivo'
		WHEN 3 then 'Cr�dito'
		WHEN 4 then 'D�bito'
		ELSE 'Otros'
	END 				"M�todo de pago", 
	cob.MONTO_GRAVADO 	"Monto Gravado", 
	cob.IMPORTE_RETENIDO 	"Importe Retenido", 
	cob.FECHA_ANULACION 	"Fecha Anulaci�n", 
	cob.NRO_FACTURA 		"Nro.Factura", 
	cob.FECHA_CONCILIACION 	"Fecha Concil.", 
	cob.ERROR_CODE 			"Cod.Error", 
	cob.ERROR_DESC 			"Error"
from 
	OSE_COBRANZAS cob 
	join RC_MOVIMIENTOS_CC mov on mov.CON_MOV_CODIGO = cob.CON_MOV_CODIGO
where 	mov.fecha > :FechaDesde 
and  	mov.fecha < :FechaHasta + (1 - (1/24/60/60))
and 	(:empCodigo is null or mov.emp_codigo = :empCodigo)

/*
---METODO_PAGO

EFECTIVO(1),
TARJETA_CREDITO(3),
TARJETA_DEBITO(4);

---ESTADOS

PENDIENTE(1),
REVERSADA(2),
CONFIRMADA(3),
ERROR(4),
ANULADA(5),
CONCILIADA(6);

Nombre                  �Nulo?   Tipo
 ----------------------- -------- -------------
 CON_MOV_CODIGO          NOT NULL NUMBER(10)

 ERROR_CODE                       NUMBER(10)
 ERROR_DESC                       VARCHAR2(200)

 ID_FACTURA                       NUMBER(10)
 NRO_CUENTA                       NUMBER(10)
 FECHA_PAGO                       DATE
 IMPORTE                          NUMBER(20,5)
 NRO_CHEQUE                       VARCHAR2(10)
 ID_TRANSACCION                   NUMBER(25)
 CONSUMIDOR_FINAL                 NUMBER(1)
 MONTO_GRAVADO                    NUMBER(20,5)
 METODO_PAGO                      NUMBER(1)
 IMPORTE_RETENIDO                 NUMBER(20,5)
 ESTADO_COBRANZA                  NUMBER(2)
 FECHA_ANULACION                  DATE
 AUDIT _DATE             NOT NULL DATE
 AUDIT_USER              NOT NULL VARCHAR2(30)
 MODIF_DATE                       DATE
 MODIF_USER                       VARCHAR2(30)
 NRO_FACTURA                      NUMBER(15)
 FECHA_CONCILIACION               DATE

*/
