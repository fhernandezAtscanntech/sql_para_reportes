multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select
	'select 
		e.codigo 	"Cod.Empresa", 
		e.descripcion 	"Empresa", 
		l.codigo 	"Cod.Local", 
		l.descripcion	"Local", 
		c.codigo 	"Cod.Caja", 
		c.descripcion 	"Caja", 
		dp.config 		"Config.Caja", 
		dp.version 		"Version Caja", 
		s.codigo 		"Cod.Servicio", 
		s.descripcion 	"Servicio", 
		se.descripcion 	"Estado Servicio"
	from 
		iposs.empresas e, 
		iposs.locales l, 
		iposs.cajas c, 
		iposs.distribuciones_pendientes dp, 
		iposs.sv_servicios s, 
		iposs.sv_estados se, 
		iposs.sv_servicios_estados svse 
	where
			svse.emp_codigo = e.codigo
	and 	
			svse.loc_codigo = l.codigo
	and 	svse.emp_codigo = l.emp_codigo
	and 	
			svse.caj_codigo = c.codigo
	and 	svse.loc_codigo = c.loc_codigo
	and 
			svse.sv_ser_codigo = s.codigo
	and 	
			svse.sv_est_codigo = se.codigo
	and
			dp.loc_codigo(+) = c.loc_codigo
	and 	dp.caj_codigo(+) = c.codigo
	and 	l.coord_x is null		
	and		c.inactiva = 0 			
	' as sql from dual
)



######################
tabla
######################

create table $$tabla$$ as (
select 
		e.codigo 	"Cod.Empresa", 
		e.descripcion 	"Empresa", 
		l.codigo 	"Cod.Local", 
		l.descripcion	"Local", 
		c.codigo 	"Cod.Caja", 
		c.descripcion 	"Caja", 
		dp.config 		"Config.Caja", 
		dp.version 		"Version Caja", 
		s.codigo 		"Cod.Servicio", 
		s.descripcion 	"Servicio", 
		se.descripcion 	"Estado Servicio"
	from 
		iposs.empresas e, 
		iposs.locales l, 
		iposs.cajas c, 
		iposs.distribuciones_pendientes dp, 
		iposs.sv_servicios s, 
		iposs.sv_estados se, 
		iposs.sv_servicios_estados svse 
	where
	1 = 2
)

######################
consulta
######################
select 1 from #reco#
