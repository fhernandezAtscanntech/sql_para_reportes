select  
	mov.EMP_CODIGO 	"Cod.Empresa",
	l.loc_cod_empresa "Nro.Sucursal", 
	l.descripcion 	"Local", 
	mov.CAJ_CODIGO 	"Caja",
	to_char(mov.audit_date,'YYYY-MM-DD HH24:MI:SS') "Fecha",
	tipos.DESCRIPCION 	"Tipo Mov.",
	CASE WHEN mov.monto_aceptado is null 
		THEN mov.monto_entregar 
		ELSE mov.monto_aceptado 
	END 			"Monto",
	mov.audit_number "Id Terminal Trans.",
	mov.id_transa_int "Id Transaccion"
from 	iposs.pr_movimientos mov 
		join iposs.pr_tipos_movimientos tipos 
			on mov.TIP_MOV_CC = tipos.codigo
		join iposs.locales l
			on mov.loc_codigo = l.codigo
			and mov.emp_codigo = l.emp_codigo
where 	mov.audit_date BETWEEN :FechaDesde AND :FechaHasta + (1-(1/24/60/60))
and 	tipos.DESCRIPCION in ('ORDEN_DE_COMPRA', 'VENTA', 'ANULACION_ORDEN_DE_COMPRA')
and 	l.codigo in :Local
and 	l.emp_codigo = $EMPRESA_LOGUEADA$
and 	mov.EMP_CODIGO = $EMPRESA_LOGUEADA$
order by 
	mov.fecha_pos
