select 
	r.res_codigo		"Cod.Resguardo", 
	r.res_fecha_emision "Fecha Emisi�n", 
	r.linea 		"L�nea", 
	r.fecha 		"Fecha Comp.", 
	r.serie 		"Serie Comp.", 
	r.numero 		"Nro. Comp.", 
	r.descripcion 	"Descripci�n", 
	r.total 		"Total", 
	r.total_iva 	"IVA", 
	i.descripcion 	"Tipo IVA", 
	r.porc_retencion * 100	"Porcentaje Ret.", 
	r.total_linea_retencion "Total Linea Ret.", 
	r.tipo_doc_ef 	"Tipo Doc. EF"
from 			
	iposs.resguardos_servicios_detalles r, 
	iposs.ivas i 
where	r.iva_codigo = i.codigo 
and 	r.fecha_emision between :FechaDesde and :FechaHasta 
and 	:FechaHasta - :FechaDesde <= 92 
and 	r.res_emp_codigo = $EMPRESA_LOGUEADA$ 
