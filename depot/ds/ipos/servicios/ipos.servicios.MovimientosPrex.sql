#######################################################################################################################################
#######################################################################################################################################
original (recibida https://scanntech.atlassian.net/browse/REP-531)
#######################################################################################################################################
#######################################################################################################################################

select trunc(c.audit_date) as "FECHA_PAGO",
      decode(c.estado, 0, 'pendiente', 1, 'reservado', 5, 'verificado', 20, 'cargado', 21, 'reintentar', 22, 'cargado reintento', 30, 'reversado', c.estado) Estado,
      decode(c.tipo_documento, 0, 'ci', 1, 'pas', 2, 'tel', 3,'acc') as "TIPO_DOCUMENTO",
      c.documento as "DOCUMENTO",
      c.cuenta as "CUENTA",
      (select max(monto_minorista) from PREX_VALORES_COMISION WHERE c.AUDIT_DATE between vigencia_desde and vigencia_hasta) as "COMISION",
      decode(c.moneda, 0, 'U$S', 1, '$') as "MONEDA",
      c.monto as "IMPORTE",
      mcc.NUMERO_OPERACION as "TALON",
      mcc.comercio||'-'||mcc.terminal as "LOCAL",
      c.id_Funcionario as "FUNCIONARIO"
from prex_cargas c
JOIN rc_movimientos_cc mcc on mcc.con_mov_codigo = c.con_mov_caj_codigo
order by c.audit_Date desc;


#######################################################################################################################################
#######################################################################################################################################
modificada
#######################################################################################################################################
#######################################################################################################################################

select 
	l.loc_cod_empresa 		"Cod.Local", 
	l.descripcion 			"Local", 
	rcc.codigo 				"Cod. CC", 
	rcc.descripcion 		"Cuenta Corriente", 
	trunc (c.audit_date) as "Fecha Pago",
	decode 
		(c.estado, 
			0, 'Pendiente', 
			1, 'Reservado', 
			5, 'Verificado', 
			20, 'Cargado', 
			21, 'Reintentar', 
			22, 'Cargado reintento', 
			30, 'Reversado', 
			'Estado no previsto - '||c.estado) 	as "Estado",
	decode 
		(c.tipo_documento, 
			0, 'CI', 
			1, 'PAS', 
			2, 'TEL', 
			3, 'ACC', 
			'N/A('||c.tipo_documento||')') 	as "Tipo Documento",
	c.documento 		as "Documento",
	c.cuenta 			as "Cuenta",
	(select max(monto_minorista) 
		from iposs.prex_valores_comision 
		where c.audit_date between vigencia_desde and vigencia_hasta) as "Comision",
	decode 
		(c.moneda, 
			0, 'U$S', 
			1, '$', 
			'N/A('||c.moneda||')') 	as "Moneda",
	c.monto as "Importe",
	mcc.NUMERO_OPERACION as "Talon",
	mcc.comercio ||'-'|| mcc.terminal as "Comercio-Terminal",
	c.id_Funcionario as "Funcionario"
from 
	iposs.prex_cargas c, 
	iposs.rc_movimientos_cc mcc, 
	iposs.rc_cuentas_corrientes rcc, 
	iposs.locales l
where 	mcc.con_mov_codigo = c.con_mov_caj_codigo
and 	mcc.cue_cor_codigo = rcc.codigo
and 	mcc.loc_codigo = l.codigo
and 	mcc.emp_codigo = l.emp_codigo
and 	rcc.emp_codigo = l.emp_codigo
and 	mcc.fecha between :FechaDesde and :FechaHasta
and 	:FechaHasta - :FechaDesde <= 92
and 	l.codigo in :Local
and 	
		rcc.emp_codigo = $EMPRESA_LOGUEADA$
and 	rcc.emp_codigo = $EMPRESA_LOGUEADA$
and 	l.emp_codigo = $EMPRESA_LOGUEADA$
