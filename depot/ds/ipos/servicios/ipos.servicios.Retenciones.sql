select 
	l.codigo 		"Codigo", 
	l.descripcion 	"Local", 
	p.razon_social 	"Proveedor", 
	p.ruc 			"RUT Prov.", 
	r.fecha_emision "Fecha Emisi�n", 
	r.total_retencion 	"Total Retenci�n", 	
	r.audit_user 	"Audit.User", 
	r.audit_date 	"Audit.Fecha", 
	t.descripcion 	"Tipo Retenci�n", 
	r.numero 		"Nro. Retenci�n", 
	decode (r.enviado_a_dgi, 1, 'SI', 'NO') 	"Enviado DGI", 
	r.cod_envio_sobre_dgi_e_resg 	"Cod.envio sobre DGI", 
	r.cod_envio_sobre_recep_e_resg 	"Cod.envio sobre recep.", 
	r.cod_seguridad_e_resg 			"Cod.seguridad", 
	r.tipo_comprobante_e_resg 		"Tipo comp.", 
	r.serie_e_resg 					"Serie", 
	r.numero_e_resg 				"N�mero", 
	r.fecha_firma_e_resg 			"Fecha Firma", 
	r.rut_emisor_e_resg 			"RUT Emisor", 
	r.monto_e_resg 					"Monto resguardo", 
	replace(pe.nombre_1||' '||pe.nombre_2||', '||pe.apellido_1||' '||pe.apellido_2, '  ', ' ') 	"Persona f�sica"
from 
	iposs.resguardos_servicios r, 
	iposs.proveedores p, 
	iposs.tipos_retenciones t, 
	iposs.locales l, 
	iposs.personas pe 
where	r.loc_codigo = l.codigo 
and 	r.prov_codigo = p.codigo (+) 
and 	r.per_codigo = pe.codigo (+) 
and 	r.tip_ret_codigo = t.codigo 
and 	r.fecha_emision between :FechaDesde and :FechaHasta 
and 	:FechaHasta - :FechaDesde <= 92 
and 	r.emp_codigo = $EMPRESA_LOGUEADA$ 

