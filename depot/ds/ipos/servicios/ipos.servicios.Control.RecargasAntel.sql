select 
	movs.FECHA 			"Fecha", 
	movs.EMP_CODIGO 	"Cod.Empresa", 
	movs.LOC_CODIGO 	"Cod.Local", 
	movs.CAJ_CODIGO 	"Caja", 
	rec.CON_MOV_CODIGO 	"Cod.Movimiento", 
	rec.ERROR_CODE 		"Cod.Error", 
	rec.ERROR_DESC 		"Error", 
	rec.FECHA_CONF 		"Fecha Confirmacion", 
	rec.NRO_TRANSACCION "Nro.Transaccion", 
	rec.TIPO_PRODUCTO 	"Tipo Producto", 
	rec.NUMERO_TEL 		"Telefono", 
	rec.IMPORTE 		"Importe", 
	rec.NRO_TRAMITE_WS1 	"WS1", 
	rec.NRO_TRAMITE_WS2 	"WS2",
CASE 
	rec.ESTADO_ACTUAL
		WHEN 1 THEN 'PENDIENTE(1)'
		WHEN 2 THEN 'REVERSADO(2)'
		WHEN 3 THEN 'CONFIRMADO(3)'
		WHEN 4 THEN 'ERROR_WS1(4)'
		WHEN 5 THEN 'ERROR_WS2(5)'
		WHEN 6 THEN 'EN_WS1(6)'
		WHEN 7 THEN 'EN_WS2(7)'
		WHEN 8 THEN 'PROCESANDO_OFFLINE(8)'
		WHEN 9 THEN 'OFFLINE_WS1(9)'
		WHEN 10 THEN 'OFFLINE_WS2(10)'
		WHEN 11 THEN 'OFFLINE_WS1_RETRY(11)'
		WHEN 12 THEN 'OFFLINE_WS2_RETRY(12)'
		WHEN 13 THEN 'REINT_MANUAL(13). Avisar a Antel'
		ELSE 'ERROR.AVISAR A DESARROLLO' 
	END "Estado Actual"
from 
	ANTEL_RECARGAS rec join RC_MOVIMIENTOS_CC movs on rec.CON_MOV_CODIGO = movs.CON_MOV_CODIGO
where 	movs.fecha >= :FechaDesde 
and 	movs.fecha <= (:FechaHasta + 1 - (1/24/60/60))
and 	(:EmpCodigo is null or movs.EMP_CODIGO = :EmpCodigo)
and 	(:NumeroTel is null or rec.NUMERO_TEL like '%'||:NumeroTel||'%')
and 	(:WS1 is null or rec.nro_tramite_ws1 = :WS1 )
and 	:FechaHasta - :FechaDesde < 30