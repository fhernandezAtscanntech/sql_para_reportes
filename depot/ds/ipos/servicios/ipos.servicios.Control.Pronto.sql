select  
	mov.EMP_CODIGO 	"Cod.Empresa",
	mov.LOC_CODIGO 	"Cod.Local",
	mov.CAJ_CODIGO 	"Caja",
	mov.terminal 	"Terminal",
	to_char(mov.audit_date,'YYYY-MM-DD HH24:MI:SS') "Fecha",
	tipos.DESCRIPCION 	"Tipo Mov.",
	CASE WHEN mov.monto_aceptado is null 
		THEN mov.monto_entregar 
		ELSE mov.monto_aceptado 
	END 			"Monto",
	mov.CEDULA 		"Cedula",
	mov.audit_number "Id Terminal Trans.",
	mov.id_transa_int "Id Transaccion"
from 
	pr_movimientos mov 
join PR_TIPOS_MOVIMIENTOS tipos 
	on mov.TIP_MOV_CC = tipos.codigo
where 	mov.audit_date BETWEEN :FechaDesde AND :FechaHasta + (1-(1/24/60/60))
and 	mov.EMP_CODIGO = :EmpCodigo
order by 
	mov.fecha_pos
