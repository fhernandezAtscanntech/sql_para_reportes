#####
Primer intento: suponemos que siempre vienen todos los datos
#####
select 
	l.emp_codigo 			"Emp.Codigo", 
	l.descripcion 			"Local", 
	l.codigo 				"Cod.Local", 
	m.fecha_comercial 		"Fecha Comercial", 
	m.caj_codigo 			"Caja", 
	m.v0_numero_operacion 	"V0 Nro. Operación", 
	m.numero_operacion 		"Nro. Operación", 
	s.ser_cod_empresa 		"Cod.Servicio", 
	s.descripcion 			"Servicio", 
	r.rub_cod_empresa 		"Cod.Rubro", 
	r.descripcion 			"Rubro", 
	md.art_codigo 			"Cod.Artículo", 
	ae.descripcion 			"Artículo", 
	md.cantidad 			"Cantidad", 
	md.importe 				"Importe"
from 
	iposs.locales l, 
	iposs.movimientos m, 
	iposs.movimientos_det_servs md, 
	iposs.articulos_empresas ae, 
	iposs.articulos_locales al, 
	iposs.rubros r, 
	iposs.rubros_locales rl, 
	iposs.servicios s, 
	iposs.servicios_locales sl
where 	-- movimiento / local
		l.codigo = m.loc_codigo 
and 	l.emp_codigo = m.emp_codigo
and 	-- movimiento / movimientos_detalles
		m.emp_codigo = md.mov_emp_codigo
and 	m.fecha_comercial = md.fecha_comercial
and 	m.numero_mov = md.mov_numero_mov
and 	-- movimientos_detalles / artículos_locales / articulos_empresas
		md.art_codigo = al.art_codigo_Externo
and 	md.loc_codigo = al.loc_codigo
and 	al.emp_codigo = ae.emp_codigo
and 	al.art_codigo = ae.art_codigo
and 	-- movimientos_detalles / rubros_locales / rubros
		md.rub_codigo = rl.rub_codigo_externo
and 	md.loc_codigo = rl.loc_codigo
and 	rl.rub_codigo = r.codigo
and 	-- movimientos_detalles / servicios_locales / servicios
		md.ser_codigo = sl.ser_codigo_Externo
and 	md.loc_codigo = sl.loc_codigo
and 	sl.ser_codigo = s.codigo
and		-- filtros de parámetros
		m.fecha_comercial between :FechaDesde and :FechaHasta
and 	:FechaHasta - :FechaDesde <= 31 
and 	l.codigo in :Local 
and 	m.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	md.mov_emp_codigo = $EMPRESA_LOGUEADA$ 
and 	ae.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	al.emp_codigo = $EMPRESA_LOGUEADA$ 



################################################################################################################
################################################################################################################
################################################################################################################
################################################################################################################
################################################################################################################

select 
	l.emp_codigo 			"Emp.Codigo", 
	l.descripcion 			"Local", 
	l.codigo 				"Cod.Local", 
	m.fecha_comercial 		"Fecha Comercial", 
	m.caj_codigo 			"Caja", 
	m.v0_numero_operacion 	"V0 Nro. Operación", 
	m.numero_operacion 		"Nro. Operación", 
	md.ser_codigo  			"Cod.Servicio", 
	s.descripcion 			"Servicio", 
	md.rub_codigo  			"Cod.Rubro", 
	(select r.descripcion 
		from iposs.rubros r, iposs.rubros_locales rl
		where r.codigo = rl.rub_codigo
		and rl.rub_codigo_Externo = md.rub_codigo
		and rl.loc_codigo = md.loc_codigo) 	"Rubro", 
	md.art_codigo 			"Cod.Artículo", 
	(select ae.descripcion 
		from iposs.articulos_empresas ae, iposs.articulos_locales al
		where ae.art_codigo = al.art_codigo
		and ae.emp_codigo = al.emp_codigo
		and al.art_codigo_Externo = md.art_codigo
		and al.loc_codigo = md.loc_Codigo
		and al.emp_codigo = $EMPRESA_LOGUEADA$ 
		and ae.emp_codigo = $EMPRESA_LOGUEADA$ )	"Artículo", 
	(select i.descripcion
		from iposs.articulos_empresas ae, iposs.articulos_locales al, iposs.ivas i
		where ae.art_codigo = al.art_codigo
		and ae.emp_codigo = al.emp_codigo
		and al.art_codigo_Externo = md.art_codigo
		and al.loc_codigo = md.loc_Codigo
		and ae.iva_codigo = i.codigo
		and al.emp_codigo = $EMPRESA_LOGUEADA$ 
		and ae.emp_codigo = $EMPRESA_LOGUEADA$ )	"IVA", 
	(md.cantidad * decode (md.tip_det_codigo, 17, 1, 18, -1))			"Cantidad", 
	(md.importe * decode (md.tip_det_codigo, 17, 1, 18, -1))				"Importe"
from 
	iposs.locales l, 
	iposs.movimientos m, 
	iposs.movimientos_det_servs md, 
	iposs.servicios s, 
	iposs.servicios_locales sl
where 	-- movimiento / local
		l.codigo = m.loc_codigo 
and 	l.emp_codigo = m.emp_codigo
and 	-- movimiento / movimientos_detalles
		m.emp_codigo = md.mov_emp_codigo
and 	m.fecha_comercial = md.fecha_comercial
and 	m.numero_mov = md.mov_numero_mov
and 	-- movimientos_detalles / servicios_locales / servicios
		md.ser_codigo = sl.ser_codigo_Externo
and 	md.loc_codigo = sl.loc_codigo
and 	sl.ser_codigo = s.codigo
and 	-- otros
		m.tipo_operacion = 'VENTA'
and		-- filtros de parámetros
		m.fecha_comercial between :FechaDesde and :FechaHasta
and 	:FechaHasta - :FechaDesde <= 31 
and 	l.codigo in :Local 
and 	( trim (upper(':Servicio')) = 'NULL' or s.codigo in :Servicio )
and 	m.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	md.mov_emp_codigo = $EMPRESA_LOGUEADA$ 
