select 
-- 	to_char(ope.fecha_conf, 'DD/MM/YYYY') "Fecha Confirmacion", 
	to_char(mov.fecha_operacion, 'DD/MM/YYYY') "Fecha Confirmacion", 
	detalles.NRO_FACTURA 	"Nro.Factura", 
	detalles.NRO_CLIENTE 	"Nro.Cliente", 
	detalles.SMART_CARD 	"SmartCard", 
	mov.EMP_CODIGO  		"Cod.Empresa", 
	mov.LOC_CODIGO 			"Cod.Local", 
	mov.CAJ_CODIGO 			"Caja", 
	detalles.monto 			"Monto", 
	detalles.producto 		"Producto", 
	ope.error_code 			"Cod.Error", 
	ope.id_cobranza 		"ID Cobranza"
from
	(select 
		CON_MOV_CODIGO, 
		0 as NRO_FACTURA, 
		0 as NRO_CLIENTE, 
		SMART_CARD, 
		MONTO_TOTAL as MONTO, 
		tipo_operacion as PRODUCTO 
	from 
		DTV_OPERAC_COBRO_PREPAGO 
	union 
	select 
		CON_MOV_CODIGO, 
		NRO_FACTURA,
		NRO_CLIENTE, 
		'0' as SMART_CARD, 
		MONTO, 
		'Cobranza con factura' as PRODUCTO 
	from 
		DTV_OPERAC_COBRO_CON_FACTURA
	union
	select 
		CON_MOV_CODIGO, 
		0 as NRO_FACTURA, 
		NRO_CLIENTE, 
		'0' as SMART_CARD, 
		monto, 'Cobranza sin factura' as PRODUCTO 
	from 
		DTV_OPERAC_COBRO_SIN_FACTURA) detalles
join DTV_OPERACIONES ope on ope.CON_MOV_CODIGO = detalles.CON_MOV_CODIGO
join RC_MOVIMIENTOS_CC mov on mov.CON_MOV_CODIGO = detalles.CON_MOV_CODIGO
where 	fecha > :FechaDesde 
and 	fecha < :FechaHasta
and 	(:EmpCodigo is null or mov.EMP_CODIGO = :EmpCodigo)
and 	(:FechaHasta - :FechaDesde) <= 31