multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
		select /*csv*/ distinct 
			tabla.AUDIT_DATE, 
			tabla.EMP_CODIGO||tabla.LOC_CODIGO as "Codigo Cliente SC",
			razSoc.RUC as "RUT",
			razSoc.DESCRIPCION as "Raz�n Social", 
			loc.DESCRIPCION as "Nombre Fantasia", 
			loc.DIRECCION,  
			loc.TELEFONO, 
			cre.descripcion as "Credito", 
			crediConf.COMERCIO as "Comercio", 
			crediConf.TERMINAL as "Terminal",
			tabla.CAJ_CODIGO as "Caja" 
		from 
			SV_SERVICIOS_AUDIT tabla 
			full join SV_SERVICIOS serv 
				on serv.CODIGO = tabla.SV_CODIGO
			full join CREDITOS_CONF_CAJAS crediConf 
				on tabla.EMP_CODIGO = crediConf.EMP_CODIGO 
				and tabla.LOC_CODIGO= crediConf.LOC_CODIGO 
				and tabla.CAJ_CODIGO = crediConf.CAJ_CODIGO 
				and crediConf.CRE_CODIGO = serv.CRE_CODIGO 
			full join locales loc 
				on loc.CODIGO = tabla.LOC_CODIGO 
				and loc.EMP_CODIGO = tabla.EMP_CODIGO
			join RAZONES_SOCIALES razSoc 
				on loc.RAZ_SOC_CODIGO = razSoc.CODIGO
			join CREDITOS cre
				on cre.codigo = crediConf.cre_codigo
		where 	tabla.campo in ('''||'Comercio'||''', '''||'Terminal'||''') 
		and 	trunc(tabla.AUDIT_DATE) = trunc(sysdate) - nvl (:Dias, 1)
		and  	serv.CRE_CODIGO = :Credito
		order by 
			tabla.AUDIT_DATE' 
	as sql from dual
)


######################
tabla
######################

create table $$tabla$$ as (
		select /*csv*/ distinct 
			tabla.AUDIT_DATE, 
			tabla.EMP_CODIGO||tabla.LOC_CODIGO as "Codigo Cliente SC",
			razSoc.RUC as "RUT",
			razSoc.DESCRIPCION as "Raz�n Social", 
			loc.DESCRIPCION as "Nombre Fantasia", 
			loc.DIRECCION,  
			loc.TELEFONO, 
			cre.descripcion as "Credito"
			crediConf.COMERCIO as "Comercio", 
			crediConf.TERMINAL as "Terminal",
			tabla.CAJ_CODIGO as "Caja" 
		from 
			SV_SERVICIOS_AUDIT tabla 
			full join SV_SERVICIOS serv 
				on serv.CODIGO = tabla.SV_CODIGO
			full join CREDITOS_CONF_CAJAS crediConf 
				on tabla.EMP_CODIGO = crediConf.EMP_CODIGO 
				and tabla.LOC_CODIGO= crediConf.LOC_CODIGO 
				and tabla.CAJ_CODIGO = crediConf.CAJ_CODIGO 
				and crediConf.CRE_CODIGO = serv.CRE_CODIGO 
			full join locales loc 
				on loc.CODIGO = tabla.LOC_CODIGO 
				and loc.EMP_CODIGO = tabla.EMP_CODIGO
			join RAZONES_SOCIALES razSoc 
				on loc.RAZ_SOC_CODIGO = razSoc.CODIGO
			join CREDITOS cre
				on cre.codigo = crediConf.cre_codigo
		where 1 = 2
)

######################
consulta
######################
select 1 from #reco#
