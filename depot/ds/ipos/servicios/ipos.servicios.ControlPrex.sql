select
	l.loc_cod_empresa "Cod.Local",
	l.descripcion "Local",
	rcc.codigo "Cod. CC",
	rcc.descripcion "Cuenta Corriente",
	trunc (c.audit_date) as "Fecha Pago",
	decode
		(c.estado,
			0, 'Pendiente',
			1, 'Reservado',
			5, 'Verificado',
			20, 'Cargado',
			21, 'Reintentar',
			22, 'Cargado reintento',
			30, 'Reversado',
			'Estado no previsto - '||c.estado) as "Estado",
	decode
		(c.tipo_documento,
			0, 'CI',
			1, 'PAS',
			2, 'TEL',
			3, 'ACC',
			'N/A('||c.tipo_documento||')') as "Tipo Documento",
	c.documento as "Documento",
	c.cuenta as "Cuenta",
	(select max(monto_minorista)
		from iposs.prex_valores_comision
		where c.audit_date between vigencia_desde and vigencia_hasta) as "Comision",
	decode
		(c.moneda,
			0, 'Pes',
			1, 'Dol',
			'N/A('||c.moneda||')') as "Moneda",
	c.monto as "Importe",
	mcc.NUMERO_OPERACION as "Talon",
	mcc.comercio ||'-'|| mcc.terminal as "Comercio-Terminal",
	c.id_Funcionario as "Funcionario"
from
	iposs.prex_cargas c,
	iposs.rc_movimientos_cc mcc,
	iposs.rc_cuentas_corrientes rcc,
	iposs.locales l
where 	mcc.con_mov_codigo = c.con_mov_caj_codigo
and 	mcc.cue_cor_codigo = rcc.codigo
and 	mcc.loc_codigo = l.codigo
and 	mcc.emp_codigo = l.emp_codigo
and 	rcc.emp_codigo = l.emp_codigo
and 	mcc.fecha between '09/04/2019' and '10/04/2019'
and 	(6480 is null or rcc.emp_codigo = 6480)
