select 
	al.art_codigo_Externo 		"Cod.Artículo", 
	ae.descripcion 			"Artículo", 
	cb.codigo_barra 			"EAN", 
	p.precio 				"Precio", 
	100 - (p.precio / decode (zp.precio, 0, p.precio, zp.precio) * 100 ) 	"Desviación"
from 
	iposs.articulos_empresas ae, 
	iposs.articulos_locales al, 
	iposs.codigos_barras cb, 
	iposs.precios_de_articulos_ventas p, 
	iposs.listas_de_precios_ventas lp, 
 	iposs.local_zona zl, 
	iposs.precios_por_zona zp, 
	iposs.locales l
where 	ae.art_codigo = al.art_codigo
and 	ae.emp_codigo = al.emp_codigo
and 	
		al.loc_codigo = l.codigo
and 	
		p.loc_lis_loc_codigo = al.loc_codigo
and 	p.emp_codigo = al.emp_codigo
and 	p.art_codigo = al.art_codigo
and 	
		p.loc_lis_lis_pre_ve_codigo = lp.codigo
and 	lp.lis_pre_ve_cod_empresa = 1
and 	ae.art_codigo = cb.art_codigo
and 	(cb.emp_codigo = ae.emp_codigo or (cb.emp_codigo = 1 and ae.veo_barras_modelo = 1))
and 	( al.loc_codigo||al.emp_codigo = zl.codigo_unico )
and 	zp.zona = zl.zona
and 	zp.zona = 
			(select zl.zona
			from iposs.local_zona zl
			where ( al.loc_codigo||al.emp_codigo = zl.codigo_unico ))	
and 	zp.codigo_barras = cb.codigo_barra
and 	ae.emp_codigo = p.emp_codigo 
and 	sysdate between p.vigencia and p.vigencia_hasta
and 	ae.emp_codigo = $EMPRESA_LOGUEADA$
and 	al.emp_codigo = $EMPRESA_LOGUEADA$
and 	p.emp_codigo = $EMPRESA_LOGUEADA$
and 	lp.emp_codigo = $EMPRESA_LOGUEADA$
and 	l.emp_codigo = $EMPRESA_LOGUEADA$
and 	al.loc_codigo = :Local


/*

INTENTO 2

*/

select 
	precios_actuales.art_codigo_Externo 	"Cod.Artículo", 
	precios_actuales.descripcion 			"Artículo", 
	precios_actuales.codigo_barra 			"EAN", 
	precios_actuales.precio 				"Precio", 
	100 - (precios_actuales.precio / decode (precios_zona.precio, 0, precios_actuales.precio, precios_zona.precio) * 100 ) 	"Desviación"
from 
	(select 
		al.loc_codigo, 
		al.emp_codigo, 
		al.art_codigo_Externo 	, 
		ae.descripcion 			, 
		cb.codigo_barra 		, 
		p.precio 				 
	from 
		iposs.articulos_empresas ae, 
		iposs.articulos_locales al, 
		iposs.codigos_barras cb, 
		iposs.precios_de_articulos_ventas p, 
		iposs.listas_de_precios_ventas lp, 
		iposs.locales l
	where 	ae.art_codigo = al.art_codigo
	and 	ae.emp_codigo = al.emp_codigo
	and 	
			al.loc_codigo = l.codigo
	and 	
			p.loc_lis_loc_codigo = al.loc_codigo
	and 	p.emp_codigo = al.emp_codigo
	and 	p.art_codigo = al.art_codigo
	and 	p.loc_lis_lis_pre_ve_codigo = lp.codigo
	and 	al.fecha_borrado is null
	and 	ae.fecha_borrado is null
	and 	lp.lis_pre_ve_cod_empresa = 1
	and 	ae.art_codigo = cb.art_codigo
	and 	(cb.emp_codigo = ae.emp_codigo or (cb.emp_codigo = 1 and ae.veo_barras_modelo = 1))
	and 	ae.emp_codigo = p.emp_codigo 
	and 	sysdate between p.vigencia and p.vigencia_hasta
	and 	ae.emp_codigo = $EMPRESA_LOGUEADA$
	and 	al.emp_codigo = $EMPRESA_LOGUEADA$
	and 	p.emp_codigo = $EMPRESA_LOGUEADA$
	and 	lp.emp_codigo = $EMPRESA_LOGUEADA$
	and 	l.emp_codigo = $EMPRESA_LOGUEADA$
	and 	al.loc_codigo = :Local
	) precios_actuales, 
	(select 
		zp.codigo_barras 		, 
		zp.precio 				
	from 
		iposs.precios_por_zona zp, 
		iposs.local_zona zl, 
		iposs.locales l
	where 	zl.zona = zp.zona
	and 	l.codigo||l.emp_codigo = zl.codigo_unico
	and 	l.emp_codigo = $EMPRESA_LOGUEADA$
	and 	l.codigo = :Local) precios_zona, 
	iposs.locales l
where 	precios_actuales.codigo_barra = precios_zona.codigo_barras
and 	l.emp_codigo = precios_actuales.emp_codigo
and 	l.codigo = precios_actuales.loc_codigo
and 	l.emp_codigo = $EMPRESA_LOGUEADA$
and 	l.codigo = :Local

/*

INTENTO 3

*/

select /*+ INDEX (P PRE_ART_VEN_VIG_I) */
	ae.art_codigo_Externo 		"Cod.Artículo", 
	ae.descripcion 			"Artículo", 
	cb.codigo_barra 			"EAN", 
	p.precio 				"Precio", 
	zp.precio 				"Precio Referencia", 
	- (100 - (p.precio / decode (zp.precio, 0, p.precio, zp.precio) * 100 )) 	"Desviación"
from 
	iposs.articulos_empresas ae, 
	iposs.codigos_barras cb, 
	iposs.precios_de_articulos_ventas p, 
	iposs.listas_de_precios_ventas lp, 
 	iposs.local_zona zl, 
	iposs.precios_por_zona zp
where 	p.loc_lis_loc_codigo = :Local
and 	p.emp_codigo = ae.emp_codigo
and 	p.art_codigo = ae.art_codigo
and 	p.loc_lis_lis_pre_ve_codigo = lp.codigo
and 	lp.lis_pre_ve_cod_empresa = 1
and 	ae.art_codigo = cb.art_codigo
and 	(cb.emp_codigo = ae.emp_codigo or (cb.emp_codigo = 1 and ae.veo_barras_modelo = 1))
and 	zl.codigo_unico = (:Local * power (10, length($EMPRESA_LOGUEADA$))) + $EMPRESA_LOGUEADA$
and 	zp.zona = zl.zona
and 	zp.codigo_barras = cb.codigo_barra
and 	ae.emp_codigo = p.emp_codigo 
and 	sysdate between p.vigencia and p.vigencia_hasta
and 	p.vigencia_hasta >= sysdate
and 	abs (100 - (p.precio / decode (zp.precio, 0, p.precio, zp.precio) * 100 )) > :Desviacion
and 	p.dim_con_numero = 0
and 	ae.emp_codigo = $EMPRESA_LOGUEADA$
and 	p.emp_codigo = $EMPRESA_LOGUEADA$
and 	lp.emp_codigo = $EMPRESA_LOGUEADA$

CREATE INDEX PPZ_I1 ON PRECIOS_POR_ZONA (CODIGO_BARRAS, ZONA);

