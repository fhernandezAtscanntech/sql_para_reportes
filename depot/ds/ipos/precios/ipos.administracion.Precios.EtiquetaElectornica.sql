/*

REP-602
######################
Desarrollar un nuevo reporte Maestro de precios etiqueta electrónica para Unilever. Los datos que debe contener el reporte son:
Codigo Interno
Codigo de barra
Descripcion
Precio de venta
Precio de oferta
Inicio
Fin
Medida del producto
Unidad de medida
Precio por medida 
######################

No hay indicación de rubro, lista de precios, local, moneda ni vigencia

Parametrizo por Rubro, Lista, Local y Fecha de vigencia

*/

select
	l.codigo 		"Cod.Local (I)", 
	l.loc_cod_empresa	"Cod.Local", 
	l.descripcion 		"Local", 
	r.descripcion  		"Rubro", 
	lis.descripcion 	"Lista de precios", 
	al.art_codigo_Externo 	"Cod.Artículo", 
	nvl (
		(select cb.codigo_barra
		from codigos_barras cb
		where cb.art_codigo = ae.art_codigo
		and (cb.emp_codigo = ae.emp_codigo or (cb.emp_codigo = 1 and ae.veo_barras_modelo = 1))
		and rownum = 1), '') 	"Cod.Barra", 
	ae.descripcion 		"Artículo", 
	iposs.f_precio_venta_2 (al.art_codigo, al.loc_Codigo, lis.codigo, nvl (:Fecha, sysdate), al.emp_codigo) 	"Precio Venta", 
	o.precio 			"Precio Oferta", 
	o.inicio 			"Inicio Oferta", 
	o.fin 				"Fin Oferta", 
	(select m.descripcion 
		from iposs.medidas m
		where m.codigo = ae.eti_med_codigo) 	"Medida", 
	nvl (ae.eti_factor_precio, 1) 		"Cantidad medida", 
	nvl (o.precio, iposs.f_precio_venta_2 (al.art_codigo, al.loc_Codigo, lis.codigo, nvl (:Fecha, sysdate), al.emp_codigo)) / decode (nvl (ae.eti_factor_precio, 1), 0, 1, nvl (ae.eti_factor_precio, 1)) 	"Precio por medida"
from 
	iposs.locales l, 
	iposs.rubros r, 
	iposs.articulos_locales al, 
	iposs.articulos_empresas ae, 
	iposs.listas_de_precios_ventas lis, 
	iposs.ofertas o
where 	ae.art_codigo = al.art_Codigo
and 	ae.emp_codigo = al.emp_codigo
and 
		ae.rub_codigo = r.codigo
and 	
		al.loc_Codigo = l.codigo
and 
		al.art_codigo = o.art_codigo (+)
and 	al.loc_codigo = o.loc_codigo (+)
and 	nvl (:Fecha, sysdate) between nvl (o.inicio, nvl (:Fecha, sysdate)) and nvl (o.fin, nvl (:Fecha, sysdate))
and 
		al.fecha_borrado is null
and 	ae.fecha_borrado is null
and 
	( l.codigo in :Local )
and
	( trim (upper (':Rubro')) = 'NULL' or r.codigo in :Rubro )
and 
	( lis.codigo = :ListaPre )
and 	
		( ae.emp_codigo  = $EMPRESA_LOGUEADA$
and 	  al.emp_codigo  = $EMPRESA_LOGUEADA$
and 	  l.emp_codigo   = $EMPRESA_LOGUEADA$
and 	  lis.emp_codigo = $EMPRESA_LOGUEADA$
and 	  nvl (o.emp_codigo, $EMPRESA_LOGUEADA$)	 = $EMPRESA_LOGUEADA$ )
