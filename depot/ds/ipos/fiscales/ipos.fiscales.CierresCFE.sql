SELECT
	c.CodEmpresa 		"Cod.Empresa",
	c.CodLocEmpresa 	"Cod.Local", 
	l.descripcion 		"Local", 
	c.Codigo 			"Cod.Cierre", 
	c.CodigoDet 		"Cod.Detalle", 
	c.FechaEmision 		"Fecha Emision",
	c.Secuencia 		"Secuencia",
	c.NroDocumento 		"Nro.Doc.Sujeto",
	c.NombreSuj 		"Sujeto",
	tc.codigo||' - '||tc.descripcion 	"TipoCFE", 
	lpad(td.codigo, 2, '0')||' - '||td.descripcion 	"TipoDetalleCFE", 
	c.Importe 			"Importe",
	c.FechaLlegada 		"Fecha Llegada",
	c.FechaEmisionDetalle 	"Fecha Emision Detalle"
FROM 
	iposs.cfe_informes_cierres c, 
	iposs.cfe_tipos_comprobantes tc, 
	iposs.cfe_tipos_detalles td, 
	iposs.locales l
WHERE 	c.codTipoCFE = tc.codigo
AND 	c.codTipoDetalle = td.codigo
AND 	c.codEmpresa = l.emp_codigo
AND 	c.codLocEmpresa = l.loc_cod_empresa
AND 	c.fechaLlegada >= :FechaDesde
AND 	c.fechaLlegada <= :FechaHasta + 1 - (1/24/60/60)
AND 	l.codigo in :Local