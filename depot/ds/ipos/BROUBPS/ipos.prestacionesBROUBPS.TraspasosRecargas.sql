select  
	e.descripcion 		as "Empresa",
	rcc.descripcion 	as "Cuenta Corriente Recargas", 
	mtcc.descripcion 	as "Cuenta Corriente BPS", 
	mtrct.fecha 		as "Fecha", 
	sum (mtrct.importe) as "Importe"
from 
	iposs.empresas e, 
	iposs.rc_cuentas_corrientes rcc, 
	iposs.mt_cuentas_corrientes mtcc, 
	iposs.mt_rc_traspasos mtrct
where 
	e.codigo = mtrct.emp_codigo
and 	mtcc.codigo = mtrct.cue_cor_mt_codigo 
and 	mtcc.emp_codigo = mtrct.emp_codigo
and 	rcc.codigo = mtrct.cue_cor_rc_codigo 
and 	rcc.emp_codigo = mtrct.emp_codigo
and 	mtrct.fecha between :FechaDesde and :FechaHasta
and 	e.codigo = $EMPRESA_LOGUEADA$
and 	rcc.emp_codigo = $EMPRESA_LOGUEADA$
and 	mtcc.emp_codigo = $EMPRESA_LOGUEADA$
and 	mtrct.emp_codigo = $EMPRESA_LOGUEADA$
group by 
	e.descripcion, 
	rcc.descripcion, 
	mtcc.descripcion, 
	mtrct.fecha
