select 
	l.descripcion 	"Local", 
	mcc.audit_number 	"Audit Number", 
	mtmcc.descripcion 	"Tipo Mov.", 
	mcc.estado_lote 	"Estado Lote", 
	mcc.fecha_disponible 	"Fecha Disponible", 
	mcc.nro_lote 		"Nro Lote", 
	mcc.numero_autorizacion 	"Número Autorización", 
	mcc.numero_documento 		"Número Documento", 
	mcc.numero_mov_cc_anu 		"Número Mov cc Anu", 
	mcc.reference_number 		"Reference Number", 
	cc.sello 			"Sello", 
	trunc (mcc.fecha) 	"Fecha", 
	sum (mcc.importe_acr) 	"Importe Acreditado", 
	sum (mcc.importe_pos) 	"Importe POS", 
	sum (mcc.importe_arancel) 	"Importe Arancel" , 
	sum (mcc.importe_comision) 	"Importe Comisión", 
	sum (mcc.importe) 		"Importe"
from 
	iposs.locales l, 
	iposs.mt_cuentas_corrientes cc, 
	iposs.mt_movimientos_cc mcc, 
	iposs.mt_tipos_movimientos_cc mtmcc 
where 	l.codigo = mcc.loc_codigo 
and     l.emp_codigo = mcc.emp_codigo 
and 	cc.codigo = mcc.cue_cor_codigo 
and 	mtmcc.codigo = mcc.tip_mov_cc 
and 	trunc(mcc.fecha) between :FechaDesde and :FechaHasta 
and 	l.codigo in :Local
group by 
	l.descripcion, 
	mcc.audit_number, 
	mtmcc.descripcion, 
	mcc.estado_lote, 
	mcc.fecha_disponible, 
	mcc.nro_lote, 
	mcc.numero_autorizacion, 
	mcc.numero_documento, 
	mcc.numero_mov_cc_anu, 
	mcc.reference_number, 
	cc.sello, 
	trunc(mcc.fecha)
