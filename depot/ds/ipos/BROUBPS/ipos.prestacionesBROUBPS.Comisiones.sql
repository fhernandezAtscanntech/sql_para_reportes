select 
	e.descripcion 	as "Empresa",
	l.descripcion 	as "Local",
	tm.descripcion 	as "Tipo Movimiento",
	mcc.sello 		as "Sello",
	sum( 
		decode (
			mcc.tip_mov_cc, 
			2, mcc.importe_comision, 
			4, -1 * mcc.importe_comision, 
			1, mcc.importe_comision, 
			3, -1 * mcc.importe_comision, 
			7, mcc.importe_comision)) as "Importe Comisión",
	count(mcc.importe) as "Cantidad transacciones",
	sum( 
		decode (
			mcc.tip_mov_cc, 
			2, mcc.importe, 
			4, -1 * mcc.importe, 
			1, mcc.importe, 
			3, -1 * mcc.importe)) as "Importe"
from 
	iposs.empresas e, 
	iposs.locales l, 
	iposs.mt_cuentas_corrientes mtcc, 
	iposs.mt_tipos_movimientos_cc tm, 
	iposs.mt_movimientos_cc mcc
where
	e.codigo = mcc.emp_codigo
and 	l.codigo = mcc.loc_codigo 
and  	l.emp_codigo = mcc.emp_codigo
and 	mtcc.codigo = mcc.cue_cor_codigo
and 	tm.codigo = mcc.tip_mov_cc
and 	mcc.tip_mov_cc in (1,2,3,4,7)
and 	trunc(mcc.fecha) between :FechaDesde and :FechaHasta
and 	e.codigo = $EMPRESA_LOGUEADA$
and 	l.emp_codigo = $EMPRESA_LOGUEADA$
and 	mtcc.emp_codigo = $EMPRESA_LOGUEADA$
and 	mcc.emp_codigo = $EMPRESA_LOGUEADA$
group by 
	e.descripcion, 
	l.descripcion, 
	tm.descripcion, 
	mcc.sello
