select  
	e.descripcion 	"Empresa", 
	cc.descripcion 	"Cuenta Corriente", 
	tmcc.descripcion 	"Tipo Mov.", 
	mcc.fecha_disponible 	"Fecha Disponible", 
	trunc(mcc.fecha) 		"Fecha", 
	sum (mcc.importe)  		"Importe"
from 
	iposs.empresas e, 
	iposs.mt_cuentas_corrientes cc, 
	iposs.mt_tipos_movimientos_cc tmcc, 
	iposs.mt_movimientos_cc mcc
where 	e.codigo = mcc.emp_codigo 
and 	cc.codigo = mcc.cue_cor_codigo 
and 	tmcc.codigo = mcc.tip_mov_cc 
and 	trunc(mcc.fecha) between :FechaDesde and :FechaHasta 
and 	e.codigo = $EMPRESA_LOGUEADA$
group by 
	e.descripcion,  
	cc.descripcion,  
	tmcc.descripcion,  
	mcc.fecha_disponible,  
	trunc(mcc.fecha)
order by 
	trunc(mcc.fecha) ,
	tmcc.descripcion asc 
