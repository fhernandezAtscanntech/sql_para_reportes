select 
	l.descripcion 		"Local", 
	c.descripcion 		"Caja", 
	m.fecha_comercial 	"Fecha", 
	trunc (m.fecha_comercial) 	"Fecha Comercial", 
	m.numero			"N�mero", 
	tm.descripcion 		"Tipo Mov.", 
	(select fc.apellido||', '||fc.nombre from iposs.funcionarios fc where emp_codigo = m.emp_codigo and codigo = m.fun_codigo) 	"Funcionario", 
	(select fa.apellido||', '||fa.nombre from iposs.funcionarios fa where emp_codigo = m.emp_codigo and codigo = m.fun_codigo_autoriza) 	"Funcionario Autoriza", 
	m.total 			"Total"
from 
	iposs.env_movimientos m, 
	iposs.env_tipos_movimientos tm, 
	iposs.locales l, 
	iposs.cajas c
where 	m.emp_codigo = l.emp_codigo
and 	m.loc_codigo = l.codigo
and 	m.loc_codigo = c.loc_codigo
and 	m.caj_codigo = c.codigo
and 	m.env_tip_mov_codigo = tm.codigo
and 	m.emp_codigo = $EMPRESA_LOGUEADA$
and 	m.fecha_comercial >= :FechaDesde
and 	m.fecha_comercial <= :FechaHasta + (1 - (1/24/60/60))
and 	m.loc_codigo in :Local