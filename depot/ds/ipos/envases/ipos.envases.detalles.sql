select 
	l.descripcion 		"Local", 
	c.descripcion 		"Caja", 
	m.fecha_comercial 	"Fecha", 
	trunc (m.fecha_comercial) 	"Fecha Comercial", 
	tm.descripcion 		"Tipo Mov.", 
	m.numero 			"Numero Mov.", 
	md.linea 			"L�nea", 
	md.cantidad 		"Cantidad", 
	ae.art_codigo_externo 	"Cod.Art�culo", 
	ae.descripcion 			"Art�culo", 
	ma.descripcion  	"Marca", 
	mt.descripcion 		"Tama�o"
from 
	iposs.env_movimientos m, 
	iposs.env_movimientos_detalles md, 
	iposs.env_tipos_movimientos tm, 
	iposs.locales l, 
	iposs.cajas c, 
	iposs.env_mar_tam_articulos mta, 
	iposs.env_mar_tamanios mt, 
	iposs.env_marcas ma, 
	iposs.articulos_empresas ae
where
		m.numero = md.env_mov_numero
and 	m.fecha_comercial = md.fecha_comercial
and 	m.emp_codigo = md.emp_codigo
and 	m.loc_codigo = l.codigo
and 	m.emp_codigo = l.emp_codigo
and 	m.loc_codigo = c.loc_codigo
and 	m.caj_codigo = c.codigo
and 	m.env_tip_mov_codigo = tm.codigo
and 	md.env_mar_tam_codigo = mta.env_mar_tam_codigo
and 	ae.art_codigo = mta.art_codigo
and 	ae.emp_codigo = m.emp_codigo
and 	md.env_mar_tam_codigo = mt.codigo
and 	ma.codigo = mt.env_mar_codigo
and 	m.fecha_comercial >= :FechaDesde
and 	m.fecha_comercial <= :FechaHasta + (1 - (1/24/60/60))
and 	m.loc_codigo in :Local
and 	m.emp_codigo = $EMPRESA_LOGUEADA$
and 	md.emp_codigo = $EMPRESA_LOGUEADA$
and 	ae.emp_codigo = $EMPRESA_LOGUEADA$
and 	mta.emp_codigo = $EMPRESA_LOGUEADA$
and 	mt.emp_codigo = $EMPRESA_LOGUEADA$