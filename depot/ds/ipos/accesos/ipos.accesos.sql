select 
	f.login						"Login", 
	f.apellido||', '||f.nombre 	"Funcionario", 
	a.fecha 					"Fecha Acceso", 
	m.descripcion 				"Módulo"
from 
	iposs.fx_accesos a, 
	iposs.funcionarios f, 
	iposs.fx_modulos_idi m
where 	a.fun_codigo = f.codigo
and 	m.mod_codigo = 
			(select fm.codigo
			from 	iposs.fx_modulos fm
			where 	lower(fm.url_app) like '%'||a.modulo||'%'
			and 	rownum = 1)
and 	m.idioma = 'es_ES'
and 	f.emp_codigo <> 1
and 	a.fecha between :FechaDesde and :FechaHasta
and 	:FechaHasta - :FechaDesde <= 62
and 	a.emp_codigo = $EMPRESA_LOGUEADA$
UNION -- esto es necesario porque no existe un módulo para indicar el acceso al sistema
select 
	f.login						"Login", 
	f.apellido||', '||f.nombre 	"Funcionario", 
	a.fecha 					"Fecha Acceso", 
	to_nchar('Acceso al sistema') 		"Módulo"
from 
	iposs.fx_accesos a, 
	iposs.funcionarios f
where 	a.fun_codigo = f.codigo
and 	a.modulo = 'products.ipossbe.server'
and 	f.emp_codigo <> 1
and 	a.fecha between :FechaDesde and :FechaHasta
and 	:FechaHasta - :FechaDesde <= 62
and 	a.emp_codigo = $EMPRESA_LOGUEADA$
order by 
	"Fecha Acceso", 
	"Login"