select 
	l.emp_codigo 	"Cod.Empresa", 
	l.codigo 		"Cod.Local", 
	l.descripcion 	"Local", 
	cre.descripcion 	"Credito", 
	c.cli_cod_Externo 	"Cod.Cliente", 
	c.numero_doc 	"Doc.", 
	c.dir_calle
		||decode (length(c.dir_numero), 0, '', ' '||c.dir_numero)
		||decode (length(c.dir_apto), 0, '', ' '||c.dir_apto)
		||decode (length(c.dir_cpostal), 0, '', ' '||c.dir_cpostal) "Direcci�n", 
	p.nombre_1||decode (length(p.nombre_2), 0, '', ' ')||
	p.nombre_2||decode (length(p.apellido_1), 0, '', ' ')||
	p.apellido_1||decode (length(p.apellido_2), 0, '', ' ')||
	p.apellido_2 	"Cliente", 
	g.descripcion 	"Grupo de Clientes", 
	p.telefono 		"Tel�fono", 
	t.numero_tarjeta "Tarjeta", 
	t.nombre 		"Nombre en Tarjeta", 
	t.fecha_vencimiento 	"Vencimiento", 
	(select con.descripcion from iposs.cre_convenios con where cc.conv_codigo = con.codigo) "Convenio", 
	cc.tope 		"Tope", 
	nvl (cc.saldo, 0) - 
		(select nvl (sum (importe * (decode (tm.signo, 1, 1, 0, -1))), 0)
		from iposs.cre_movimientos_cc mcc, iposs.cre_tipos_movimientos tm
		where 	mcc.cre_tm_codigo = tm.codigo
		and 	mcc.cre_cc_codigo = cc.codigo
		and 	mcc.emp_codigo = cs.emp_codigo
		and 	mcc.mon_codigo = cc.mon_codigo
		and 	mcc.fecha_comercial > trunc(:Fecha)+1-(1/24/60/60)) "Saldo 1", 
	nvl (cc.saldo2, 0) - 
		(select nvl (sum (importe * (decode (tm.signo, 1, 1, 0, -1))), 0)
		from iposs.cre_movimientos_cc mcc, iposs.cre_tipos_movimientos tm
		where 	mcc.cre_tm_codigo = tm.codigo
		and 	mcc.cre_cc_codigo = cc.codigo
		and 	mcc.emp_codigo = cs.emp_codigo
		and 	mcc.mon_codigo = cc.mon_codigo2
		and 	mcc.fecha_comercial > trunc(:Fecha)+1-(1/24/60/60)) "Saldo 2", 
	(select max(fecha_comercial)
		from cre_movimientos_cc mcc
		where mcc.cre_cc_codigo = cc.codigo
		and mcc.emp_codigo = cs.emp_codigo
		and mcc.mon_codigo = cc.mon_codigo
		and mcc.cre_tm_codigo in (5, 13) -- Pago
		) 			"Fecha Ult.Pag.", 
		(select importe
		from cre_movimientos_cc mcc
		where mcc.cre_cc_codigo = cc.codigo
		and mcc.emp_codigo = cs.emp_codigo
		and mcc.mon_codigo = cc.mon_codigo
		and mcc.cre_tm_codigo in (5, 13) -- Pago
		and mcc.fecha_comercial = 
			(select max(mcc2.fecha_comercial)
			from iposs.cre_movimientos_cc mcc2
			where mcc2.cre_cc_codigo = cc.codigo
			and mcc2.emp_codigo = cs.emp_codigo
			and mcc2.cre_tm_codigo in (5, 13))
		and rownum = 1) "Monto Ult.Pag."
from 
	iposs.clientes c, 
	iposs.cre_clientes c2, 
	iposs.grupos_de_clientes g, 
	iposs.personas p, 
	iposs.cre_sellos_empresas cs, 
	iposs.cre_sellos s, 
	iposs.creditos cre, 
	iposs.cre_tarjetas t, 
	iposs.cre_cuentas_corrientes cc, 
	iposs.clientes_locales cl, 
	iposs.locales l
where	c.per_codigo = p.codigo
and 	c.codigo = cl.cli_codigo
and 	c2.per_codigo = p.codigo
and 	c2.cre_sel_codigo = cs.cre_sel_codigo
and 	cs.cre_sel_codigo = s.codigo
and 	cre.codigo = s.cre_codigo
and 	c2.codigo = t.cli_codigo
and 	c.gru_cli_codigo = g.codigo(+)
and 	cs.cre_sel_codigo = t.cre_sel_codigo
and 	cs.cre_sel_codigo = cc.cre_sel_codigo
and 	c2.codigo = cc.cli_codigo
and 	cl.loc_codigo = l.codigo
and 	c.nombre between 
			decode (upper(:ClienteDesde), '', chr(1), :ClienteDesde) and 
			decode (upper(:ClienteHasta), '', chr(255), :ClienteHasta)
and 	(nvl(:Borrados, 0) = 1 or (c.fecha_borrado is null and cl.fecha_borrado is null))
and 	l.codigo in :Local
and 	cs.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	l.emp_codigo = $EMPRESA_LOGUEADA$ 