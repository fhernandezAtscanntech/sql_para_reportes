select 
	l.codigo 		"Cod.Local (I)", 
	l.loc_cod_empresa 	"Cod.Local", 
	l.descripcion 		"Local", 
	cli.cli_cod_externo 		"Cli.C�digo", 
	p.nombre_1||decode (length(p.nombre_2), 0, '', ' ')||
	p.nombre_2||decode (length(p.apellido_1), 0, '', ' ')||
	p.apellido_1||decode (length(p.apellido_2), 0, '', ' ')||
	p.apellido_2 	"Cliente", 
	cli.numero_doc 		"N�mero Doc.", 
	cli.telefono 			"Tel�fono",
	cre.descripcion  		"Cr�dito", 
	t.numero_tarjeta 		"N�mero Tarjeta", 
	t.fecha_vencimiento 	"Vencimiento", 
	nvl (g.descripcion, 'Sin grupo asociado') 	"Grupo de Clientes", 
	(select mo.Simbolo from iposs.monedas mo where mo.codigo = cc.mon_codigo) 			"Moneda 1", 
	cc.saldo 			"Saldo 1", 
	(select mo.Simbolo from iposs.monedas mo where mo.codigo = cc.mon_codigo2) 			"Moneda 2", 
	cc.saldo2 			"Saldo 2", 
	cc.tope 		"Tope", 
	(select max(mcc.fecha_comercial)
		from iposs.cre_movimientos_cc mcc
		where 	mcc.cre_cc_codigo = cc.codigo
		and 	mcc.cre_tar_codigo = t.codigo
		and 	mcc.emp_codigo = se.emp_codigo
		and 	mcc.cre_tm_codigo in (5, 13, 14)) 	"Fecha Ult.Pago"
from 
	iposs.clientes cli, 
	iposs.clientes_locales cl, 
	iposs.grupos_de_clientes g, 
	iposs.locales l, 
	iposs.cre_clientes c, 
	iposs.personas p, 
	iposs.cre_cuentas_corrientes cc, 
	iposs.cre_tarjetas t, 
	iposs.cre_sellos_empresas se, 
	iposs.cre_sellos s, 
	iposs.creditos cre
where
		cli.per_codigo = p.codigo
and 	c.per_codigo = p.codigo
and		c.cre_sel_codigo = se.cre_sel_codigo
and 
		cli.gru_cli_codigo = g.codigo (+)
and 
		s.codigo = se.cre_sel_codigo
and
		cli.codigo = cl.cli_codigo
and 	l.codigo = cl.loc_codigo
and
		cc.cli_codigo = c.codigo
and 	t.cli_codigo = c.codigo
and 	
		s.cre_codigo = cre.codigo
and 	( trim (upper (':GrupoClientes')) = 'NULL' or g.codigo in :GrupoClientes )
and
		l.codigo in :Local
and
		cli.emp_codigo = $EMPRESA_LOGUEADA$
and 	se.emp_codigo = $EMPRESA_LOGUEADA$
and 	p.emp_codigo = $EMPRESA_LOGUEADA$
