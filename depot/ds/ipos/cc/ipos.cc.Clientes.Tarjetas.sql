select 
	l.emp_codigo 	"Cod.Empresa", 
	l.codigo 		"Cod.Local", 
	l.descripcion 	"Local", 
	c.cli_cod_Externo 	"Cod.Cliente", 
	c.numero_doc 	"Doc.", 
	c.dir_calle
		||decode (length(c.dir_numero), 0, '', ' '||c.dir_numero)
		||decode (length(c.dir_apto), 0, '', ' '||c.dir_apto)
		||decode (length(c.dir_cpostal), 0, '', ' '||c.dir_cpostal) "Direcci�n", 
	p.nombre_1||decode (length(p.nombre_2), 0, '', ' ')||
	p.nombre_2||decode (length(p.apellido_1), 0, '', ' ')||
	p.apellido_1||decode (length(p.apellido_2), 0, '', ' ')||
	p.apellido_2 	"Cliente", 
	p.telefono 		"Tel�fono", 
	t.numero_tarjeta "Tarjeta", 
	t.nombre 		"Nombre en Tarjeta", 
	t.fecha_vencimiento 	"Vencimiento"
from 
	iposs.clientes c, 
	iposs.cre_clientes c2, 
	iposs.personas p, 
	iposs.cre_sellos_empresas s, 
	iposs.cre_tarjetas t, 
	iposs.clientes_locales cl, 
	iposs.locales l
where	c.per_codigo = p.codigo
and 	c.codigo = cl.cli_codigo
and 	c2.per_codigo = p.codigo
and 	c2.cre_sel_codigo = s.cre_sel_codigo
and 	c2.codigo = t.cli_codigo
and 	s.cre_sel_codigo = t.cre_sel_codigo
and 	cl.loc_codigo = l.codigo
and 	(nvl(:Borrados, 0) = 1 or (c.fecha_borrado is null and cl.fecha_borrado is null))
and 	l.codigo in :Local
and 	s.emp_codigo = $EMPRESA_LOGUEADA$
and 	l.emp_codigo = $EMPRESA_LOGUEADA$