select distinct 
	comercios.EMP_CODIGO, 
	comercios.LOC_CODIGO, 
	comercios.CAJ_CODIGO, 
	servicios.DESCRIPCION, 
	comercios.AUDIT_DATE 		as "FECHA COMERCIO", 
	comercios.AUDIT_USER 		as "USUARIO COMERCIO", 
	comercios.VALOR_ACTUAL 		as "COMERCIO", 
	comercios.VALOR_ANTERIOR 	as "COMERCIO VALOR ANTERIOR", 
	terminales.AUDIT_DATE 		as "FECHA TERMINAL", 
	terminales.AUDIT_USER 		as "USUARIO TERMINAL", 
	terminales.VALOR_ACTUAL 	as "TERMINAL", 
	terminales.VALOR_ANTERIOR 	as "TERMINAL VALOR ANTERIOR"
from 
	SV_SERVICIOS_AUDIT comercios
		join SV_SERVICIOS servicios 
			on servicios.CODIGO = comercios.SV_CODIGO
		full join SV_SERVICIOS_AUDIT terminales 
			on terminales.LOC_CODIGO = comercios.LOC_CODIGO 
			and terminales.CAJ_CODIGO = comercios.CAJ_CODIGO 
			and comercios.SV_CODIGO = terminales.SV_CODIGO 
			and terminales.campo = 'Terminal'
where 	servicios.CONFIG_BACKEND_FACADE = 1 
and 	comercios.CAMPO = 'Comercio'
and 	(:EmpCodigo is null or comercios.EMP_CODIGO = :EmpCodigo)
and 	(:LocCodigo is null or comercios.LOC_CODIGO = :LocCodigo)
and 	(:CajCodigo is null or comercios.CAJ_CODIGO = :CajCodigo)
/*and 	(nvl (:Servicio, '') = '' 		or trim (upper (servicios.DESCRIPCION)) 	like '%'||trim (upper (:Servicio))||'%' )
and 	(nvl (:FechaComercio, '') = '' 	or trunc (comercios.AUDIT_DATE) 			= to_date(:FechaComercio, 'dd/mm/yyyy'))
and 	(nvl (:UserComercio, '') = '' 	or trim (upper (comercios.AUDIT_USER)) 		like '%'||trim (upper (:UserComercio))||'%' )
and 	(nvl (:Comercio, '') = '' 		or trim (upper (comercios.VALOR_ACTUAL)) 	like '%'||trim (upper (:Comercio))||'%' )
and 	(nvl (:ComercioValAnt, '') = '' or trim (upper (comercios.VALOR_ANTERIOR)) 	like '%'||trim (upper (:ComercioValAnt))||'%' )
and 	(nvl (:FechaTerminal, '') = '' 	or trunc (terminales.AUDIT_DATE) 			= to_date(:FechaTerminal, 'dd/mm/yyyy'))
and 	(nvl (:UserTerminal, '') = '' 	or trim (upper (terminales.AUDIT_USER)) 	like '%'||trim (upper (:UserTerminal))||'%' )
and 	(nvl (:Terminal, '') = '' 		or trim (upper (terminales.VALOR_ACTUAL)) 	like '%'||trim (upper (:Terminal))||'%' )
and 	(nvl (:TerminalValAnt, '') = '' or trim (upper (terminales.VALOR_ANTERIOR)) like '%'||trim (upper (:TerminalValAnt))||'%' )
*/
and 	(:Servicio is null 		or trim (upper (servicios.DESCRIPCION)) 	like '%'||trim (upper (:Servicio))||'%' )
and 	(:FechaComercio is null or trunc (comercios.AUDIT_DATE) 			= to_date(:FechaComercio, 'dd/mm/yyyy'))
and 	(:UserComercio is null 	or trim (upper (comercios.AUDIT_USER)) 		like '%'||trim (upper (:UserComercio))||'%' )
and 	(:Comercio is null 		or trim (upper (comercios.VALOR_ACTUAL)) 	like '%'||trim (upper (:Comercio))||'%' )
and 	(:ComercioValAnt is null or trim (upper (comercios.VALOR_ANTERIOR)) like '%'||trim (upper (:ComercioValAnt))||'%' )
and 	(:FechaTerminal is null or trunc (terminales.AUDIT_DATE) 			= to_date(:FechaTerminal, 'dd/mm/yyyy'))
and 	(:UserTerminal is null 	or trim (upper (terminales.AUDIT_USER)) 	like '%'||trim (upper (:UserTerminal))||'%' )
and 	(:Terminal is null 		or trim (upper (terminales.VALOR_ACTUAL)) 	like '%'||trim (upper (:Terminal))||'%' )
and 	(:TerminalValAnt is null or trim (upper (terminales.VALOR_ANTERIOR)) like '%'||trim (upper (:TerminalValAnt))||'%' )
and not	(:EmpCodigo 	is null 
and 	:LocCodigo 		is null 
and 	:CajCodigo 		is null 
and 	:Servicio 		is null 
and 	:FechaComercio  is null 
and 	:UserComercio   is null 
and 	:Comercio       is null 
and 	:ComercioValAnt is null 
and 	:FechaTerminal  is null 
and 	:UserTerminal   is null 
and 	:Terminal       is null 
and 	:TerminalValAnt is null)