multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select
	'SELECT DISTINCT
		M.emp_codigo Empresa,
		M.LOC_COD_EMPRESA LOCAL,
		M.caj_codigo Caja,
		M.fecha_comercial Fecha,
		SUM (M.TOTAL) Total
	FROM
		movimientos M
	INNER JOIN empresas E ON (E.codigo = M.EMP_CODIGO)
	WHERE
		E.INT_CODIGO = :Parceiro 
	AND M.fecha_comercial > TRUNC (SYSDATE) - 15
	AND M.fecha_comercial < TRUNC (SYSDATE)
	AND tipo_operacion = '''||'VENTA'||'''
	AND NOT EXISTS (
		SELECT
			1
		FROM
			reporte_z_br rep_z
		WHERE
			rep_z.EMP_CODIGO = M.EMP_CODIGO
		AND rep_z.LOC_CODIGO = M.LOC_CODIGO
		AND rep_z.CAJ_CODIGO = M.CAJ_CODIGO
		AND rep_z.FECHA_COMERCIAL = M.FECHA_COMERCIAL
	)
	GROUP BY
		M.emp_codigo ,
		M.LOC_COD_EMPRESA ,
		M.caj_codigo ,
		M.fecha_comercial 
	ORDER BY
		M.EMP_CODIGO,
		M.LOC_COD_EMPRESA,
		M.FECHA_COMERCIAL,
		M.CAJ_CODIGO DESC
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
SELECT DISTINCT
		M.emp_codigo Empresa,
		M.LOC_COD_EMPRESA LOCAL,
		M.caj_codigo Caja,
		M.fecha_comercial Fecha,
		SUM (M.TOTAL) Total
	FROM
		movimientos M
	INNER JOIN empresas E ON (E.codigo = M.EMP_CODIGO)
	WHERE 1 = 2
	GROUP BY 
		M.emp_codigo,
		M.LOC_COD_EMPRESA,
		M.caj_codigo,
		M.fecha_comercial
)

######################
consulta
######################
select 1 from #reco#