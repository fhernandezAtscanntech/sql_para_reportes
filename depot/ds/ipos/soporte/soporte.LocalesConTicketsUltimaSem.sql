/*
 * Juan Buonafina me pasa esta consulta para agendarla todos los lunes a la ma�ana:
 *
 * select tabla.emp_codigo||tabla.loc_codigo codigo_unico,loc.DESCRIPCION Nombre_Fantasia,raz.DESCRIPCION Razon_Social, loca.DESCRIPCION Localidad, dep.DESCRIPCION Departamento, Sem_1 as "SEM-1", Sem
 * from (
 * select emp_codigo,  loc_codigo,  max(fecha_comercial) Ultimo,
 *     SUM(CASE WHEN fecha_comercial between trunc(sysdate)-21 and trunc(sysdate)-8 THEN 1 ELSE 0 END) as Sem_1,
 *     SUM(CASE WHEN fecha_comercial between trunc(sysdate)-7 and trunc(sysdate)-1 THEN 1 ELSE 0 END) as Sem
 *     from movimientos
 *     where fecha_comercial between trunc(sysdate)-21 and trunc(sysdate)-1
 *     group by emp_codigo, loc_codigo
 * ) tabla join locales loc on tabla.loc_codigo = loc.CODIGO
 *  full join LOCALIDADES loca on loc.LOCA_CODIGO = loca.CODIGO
 *  full join DEPARTAMENTOS dep on loca.DEPA_CODIGO = dep.CODIGO
 *  full join RAZONES_SOCIALES raz on loc.RAZ_SOC_CODIGO = raz.CODIGO
 * where  loc.LOC_COD_EMPRESA < 99 and Sem_1 = 0 and Sem > 0
 * order by sem desc
 * 
 *
 * Preparo un reporte para agendarlo
 *
*/

multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
		select 
			tabla.emp_codigo||tabla.loc_codigo codigo_unico, 
			loc.DESCRIPCION Nombre_Fantasia, 
			raz.DESCRIPCION Razon_Social, 
			loca.DESCRIPCION Localidad, 
			dep.DESCRIPCION Departamento, 
			Sem_1 as "SEM-1", Sem
		from (
			select 
				emp_codigo,  
				loc_codigo,  
				max(fecha_comercial) Ultimo,
				SUM(CASE WHEN fecha_comercial between trunc(sysdate)-21 and trunc(sysdate)-8 THEN 1 ELSE 0 END) as Sem_1,
				SUM(CASE WHEN fecha_comercial between trunc(sysdate)-7 and trunc(sysdate)-1 THEN 1 ELSE 0 END) as Sem
			from movimientos
			where fecha_comercial between trunc(sysdate)-21 and trunc(sysdate)-1
			group by emp_codigo, loc_codigo) tabla 
		join locales loc 
			on tabla.loc_codigo = loc.CODIGO
		full join LOCALIDADES loca 
			on loc.LOCA_CODIGO = loca.CODIGO
		full join DEPARTAMENTOS dep 
			on loca.DEPA_CODIGO = dep.CODIGO
		full join RAZONES_SOCIALES raz 
			on loc.RAZ_SOC_CODIGO = raz.CODIGO
		where 	loc.LOC_COD_EMPRESA < 99 
		and 	Sem_1 = 0 
		and 	Sem > 0
		order by sem desc
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
select 
			tabla.emp_codigo||tabla.loc_codigo codigo_unico, 
			loc.DESCRIPCION Nombre_Fantasia, 
			raz.DESCRIPCION Razon_Social, 
			loca.DESCRIPCION Localidad, 
			dep.DESCRIPCION Departamento, 
			Sem_1 as "SEM-1", Sem
		from (
			select 
				emp_codigo,  
				loc_codigo,  
				max(fecha_comercial) Ultimo,
				SUM(CASE WHEN fecha_comercial between trunc(sysdate)-21 and trunc(sysdate)-8 THEN 1 ELSE 0 END) as Sem_1,
				SUM(CASE WHEN fecha_comercial between trunc(sysdate)-7 and trunc(sysdate)-1 THEN 1 ELSE 0 END) as Sem
			from movimientos
			where fecha_comercial between trunc(sysdate)-21 and trunc(sysdate)-1
			group by emp_codigo, loc_codigo) tabla 
		join locales loc 
			on tabla.loc_codigo = loc.CODIGO
		full join LOCALIDADES loca 
			on loc.LOCA_CODIGO = loca.CODIGO
		full join DEPARTAMENTOS dep 
			on loca.DEPA_CODIGO = dep.CODIGO
		full join RAZONES_SOCIALES raz 
			on loc.RAZ_SOC_CODIGO = raz.CODIGO
	where 1 = 2
)

######################
consulta
######################
select 1 from #reco#

