multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
		select 
			trunc(p.audit_date) "Fecha", 
			i.codigo 		"Cod.Parceiro", 
			i.descripcion 	"Parceiro", 
			count(*) 		"Cantidad Mov."
		from 
			iposs.integradores i, 
			iposs.empresas e, 
			dwm_prod.procesos_pendientes_dw p
		where 	e.int_codigo = i.codigo
		and 	p.mov_emp_codigo = e.codigo
		and 	p.mov_fecha_comercial < trunc(sysdate)
		and 	p.mov_fecha_comercial >= trunc(sysdate - 30)
		and 	p.audit_date < trunc(sysdate)
-- 		and 	p.estado = '''||'PENDIENTE'||'''
		group by 
			trunc(p.audit_date), 
			i.codigo, 
			i.descripcion
	' as sql from dual
)


######################
tabla
######################

create table $$tabla$$ as (
		select 
			trunc(p.audit_date) "Fecha", 
			i.codigo 			"Cod.Integrador", 
			i.descripcion 		"Integrador", 
			count(*) 			"Cantidad Mov."
		from 
			iposs.integradores i, 
			iposs.empresas e, 
			dwm_prod.procesos_pendientes_dw p
		where 1 = 2
		group by 
			trunc(p.audit_date), 
			i.codigo, 
			i.descripcion
)

######################
consulta
######################
select 1 from #reco#
