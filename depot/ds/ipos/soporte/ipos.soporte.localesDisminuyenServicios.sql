multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
		select 	
			emp_codigo, 
			CODIGO, 
			LOC_COD_EMPRESA, 
			Nombre_Fantasia, 
			Razon_Social, 
			DIRECCION, 
			hace_2_meses, 
			utimo_mes
		from (
			select 
			loc.emp_codigo, 
			loc.CODIGO, 
			loc.LOC_COD_EMPRESA, 
			loc.DESCRIPCION Nombre_Fantasia, 
			raz.DESCRIPCION Razon_Social, 
			loc.DIRECCION, 
			SUM (CASE 
				WHEN movCC.fecha >= trunc(sysdate-60) and movCC.fecha < trunc(sysdate-30) 
				THEN movCC.importe_pos 
				ELSE NULL 
			END) as hace_2_meses, 
			SUM (CASE 
				WHEN movCC.fecha >= trunc(sysdate-30) 
				THEN movCC.importe_pos 
				ELSE NULL 
			END) as utimo_mes
			from distribuciones_pendientes dist 
			join locales loc 
				on dist.loc_codigo = loc.CODIGO
			full join LOCALIDADES loca 
				on loc.LOCA_CODIGO = loca.CODIGO
			full join DEPARTAMENTOS dep 
				on loca.DEPA_CODIGO = dep.CODIGO
			full join RAZONES_SOCIALES raz 
				on loc.RAZ_SOC_CODIGO = raz.CODIGO
			join RC_MOVIMIENTOS_CC movCC 
				on movCC.EMP_CODIGO = dist.EMP_CODIGO 
				and movCC.LOC_CODIGO = loc.CODIGO 
				and dist.CAJ_CODIGO = movCC.CAJ_CODIGO
			where 	dist.FECHA_ULT_CONSULTA > sysdate - 7 
			and 	dep.CODIGO = 1 
			and 	movCC.fecha >= trunc(sysdate-60) 
			and 	movCC."COMPAÑIA" in 
				('''||'ANC'||''', 
				'''||'MVS'||''', 
				'''||'CLR'||''', 
				'''||'ETM'||''', 
				'''||'UTE'||''', 
				'''||'DTVUY'||''', 
				'''||'ANT'||''', 
				'''||'OSE'||''')
			group by 
				loc.emp_codigo, 
				loc.CODIGO, 
				loc.LOC_COD_EMPRESA, 
				loc.DESCRIPCION, 
				raz.DESCRIPCION, 
				loc.DIRECCION
			) tabla 
		where 	tabla.utimo_mes / nvl(tabla.hace_2_meses, 1) < 0.6 
		and 	tabla.hace_2_meses > 2000
		order by 
			1, 2, 3 desc
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
	select 	
		emp_codigo, 
		CODIGO, 
		LOC_COD_EMPRESA, 
		Nombre_Fantasia, 
		Razon_Social, 
		DIRECCION, 
		hace_2_meses, 
		utimo_mes
	from (
		select 
		loc.emp_codigo, 
		loc.CODIGO, 
		loc.LOC_COD_EMPRESA, 
		loc.DESCRIPCION Nombre_Fantasia, 
		raz.DESCRIPCION Razon_Social, 
		loc.DIRECCION, 
		SUM (CASE 
			WHEN movCC.fecha >= trunc(sysdate-60) and movCC.fecha < trunc(sysdate-30) 
			THEN movCC.importe_pos 
			ELSE NULL 
		END) as hace_2_meses, 
		SUM (CASE 
			WHEN movCC.fecha >= trunc(sysdate-30) 
			THEN movCC.importe_pos 
			ELSE NULL 
		END) as utimo_mes
		from distribuciones_pendientes dist 
		join locales loc 
			on dist.loc_codigo = loc.CODIGO
		full join LOCALIDADES loca 
			on loc.LOCA_CODIGO = loca.CODIGO
		full join DEPARTAMENTOS dep 
			on loca.DEPA_CODIGO = dep.CODIGO
		full join RAZONES_SOCIALES raz 
			on loc.RAZ_SOC_CODIGO = raz.CODIGO
		join RC_MOVIMIENTOS_CC movCC 
			on movCC.EMP_CODIGO = dist.EMP_CODIGO 
			and movCC.LOC_CODIGO = loc.CODIGO 
			and dist.CAJ_CODIGO = movCC.CAJ_CODIGO
		where 	dist.FECHA_ULT_CONSULTA > sysdate - 7 
		and 	dep.CODIGO = 1 
		and 	movCC.fecha >= trunc(sysdate-60) 
		and 	movCC."COMPAÑIA" in ('ANC','MVS','CLR','ETM','UTE','DTVUY','ANT','OSE')
		group by 
			loc.emp_codigo, 
			loc.CODIGO, 
			loc.LOC_COD_EMPRESA, 
			loc.DESCRIPCION, 
			raz.DESCRIPCION, 
			loc.DIRECCION
		) tabla 
	where 1 = 2 
)


######################
consulta
######################
select 1 from #reco#

