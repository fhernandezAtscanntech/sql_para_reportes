select
	g.descripcion 			"Grupo Funcionario", 
	f.fun_cod_empresa 		"Cod.Funcionario", 
	f.nombre 				"Nombre", 
	f.apellido 				"Apellido", 
	f.login 				"Login", 
	substr(nd.texto, 1, 60) "Directorio", 
	substr(nr.texto, 1, 60) "Reporte", 
	es.fecha_ejecucion 		"Fecha",
	es.tiempo_ejecucion 	"Tiempo"
from
	iposs_rep.i18n_etiquetas nr,
	iposs_rep.i18n_etiquetas nd,
	iposs_rep.dir_archivos a,
	iposs_rep.dir_directorios d,
	iposs_rep.nr_componente_reporte cr,
	iposs_rep.nr_componente_generico cg, 
	iposs_rep.nr_estadisticas es, 
	iposs.funcionarios f, 
	iposs.grupos_de_funcionarios g
where 	a.nombre = nr.codigo
and 	d.nombre = nd.codigo
and 	cr.codigo = cg.codigo
and 	cg.cod_reporte = a.codigo
and 	a.cod_directorio = d.codigo
and 	nr.idioma = 'es'
and 	nd.idioma = 'es'
and 	es.cod_reporte = a.codigo
and 	es.cod_usuario_logueado = f.codigo
and 	f.gru_fun_codigo = g.codigo
and 	f.emp_codigo = $EMPRESA_LOGUEADA$
and 	es.fecha_ejecucion between :FechaDesde and :FechaHasta
