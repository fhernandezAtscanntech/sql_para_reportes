--		AND 	pp.FECHA_ACEPTACION >= sysdate -1 OR pp.FECHA_RECHAZO >= sysdate -1

multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
		select
			l.CODIGO_UNICO_LOCAL,
			p.TITULO, 
			p.ID AS "ID_PROMOCION", 
			p.VIGENCIA_DESDE, 
			p.VIGENCIA_HASTA, 
			pp.ESTADO, 
			l.DESC_LOCAL AS "NOMBRE LOCAL",
			pp.FECHA_ACEPTACION,
			pp.FECHA_RECHAZO,
			pp.MODIF_USER,
			e.RAZON_SOCIAL AS FABRICANTE, 
			coalesce (ic.descripcion, '''||'Sin Campaña'||''') as Campana
		FROM 
			iposs_mp.promociones p 
			INNER JOIN iposs_mp.empresas e 
				ON (p.ID_AUTOR = e.ID)
			INNER JOIN iposs_mp.propuestas_promociones pp 
				ON (pp.ID_PROMOCION = p.ID)
			INNER JOIN iposs_mp.locales l 
				ON (pp.ID_LOCAL_OFERIDO = l.ID)
			INNER JOIN iposs.promociones ip 
				ON (ip.prom_cod_empresa = p.codigo_interno_be)
			LEFT JOIN iposs.campañas ic
				ON (ip.cpa_codigo = ic.codigo)
		WHERE 	p.VIGENCIA_HASTA >= sysdate
		AND 	nvl (pp.fecha_rechazo, pp.fecha_aceptacion) >= trunc(sysdate)-1
		and 	nvl (pp.fecha_rechazo, pp.fecha_aceptacion) < trunc(sysdate)
		AND 	pp.FECHA_BORRADO is null
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
	select
		l.CODIGO_UNICO_LOCAL,
		p.TITULO, 
		p.ID AS "ID_PROMOCION", 
		p.VIGENCIA_DESDE, 
		p.VIGENCIA_HASTA, 
		pp.ESTADO, 
		l.DESC_LOCAL AS "NOMBRE LOCAL",
		pp.FECHA_ACEPTACION,
		pp.FECHA_RECHAZO,
		pp.MODIF_USER,
		e.RAZON_SOCIAL AS FABRICANTE
	FROM 
		iposs_mp.promociones p 
		INNER JOIN iposs_mp.empresas e 
			ON (p.ID_AUTOR = e.ID)
		INNER JOIN iposs_mp.propuestas_promociones pp 
			ON (pp.ID_PROMOCION = p.ID)
		INNER JOIN iposs_mp.locales l 
			ON (pp.ID_LOCAL_OFERIDO = l.ID)
	WHERE 1 = 2
)

######################
consulta
######################
select 1 from #reco#
