multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select
	'SELECT
		ctrl.EMP_CODIGO as EMPRESA,
		l.LOC_COD_EMPRESA as LOCAL,
		ctrl.FECHA_COMERCIAL as FECHA,
		ctrl.CAJ_CODIGO as CAJA,
		SUM(CTRL.VENTAS_MOVS) as VENTA_MOVIMIENTOS,
		SUM(CTRL.VENTAS_CIERRE_DIARIO) as VENTA_Z,
		SUM(-1 * CTRL.DIFERENCIA) as DIFERENCIA
	FROM
		IPOSS.CTRL_CIERRES_DIARIOS ctrl
		INNER JOIN IPOSS.EMPRESAS e 
			on (e.CODIGO = ctrl.EMP_CODIGO)
		INNER JOIN IPOSS.LOCALES l 
			on (l.CODIGO = ctrl.LOC_CODIGO)
	WHERE
		e.INT_CODIGO = :Parceiro AND
		ctrl.FECHA_COMERCIAL > SYSDATE - 15 AND
		abs(ctrl.DIFERENCIA) > 1
	GROUP BY ctrl.EMP_CODIGO, l.LOC_COD_EMPRESA, ctrl.FECHA_COMERCIAL, ctrl.CAJ_CODIGO
	ORDER BY ctrl.EMP_CODIGO, l.LOC_COD_EMPRESA, ctrl.FECHA_COMERCIAL, ctrl.CAJ_CODIGO DESC
	' as sql from dual
)

######################
tabla
######################

	create table $$tabla$$ as (
	SELECT
		ctrl.EMP_CODIGO as EMPRESA,
		l.LOC_COD_EMPRESA as LOCAL,
		ctrl.FECHA_COMERCIAL as FECHA,
		ctrl.CAJ_CODIGO as CAJA,
		SUM(CTRL.VENTAS_MOVS) as VENTA_MOVIMIENTOS,
		SUM(CTRL.VENTAS_CIERRE_DIARIO) as VENTA_Z,
		SUM(-1 * CTRL.DIFERENCIA) as DIFERENCIA
	FROM
		IPOSS.CTRL_CIERRES_DIARIOS ctrl
		INNER JOIN IPOSS.EMPRESAS e 
			on (e.CODIGO = ctrl.EMP_CODIGO)
		INNER JOIN IPOSS.LOCALES l 
			on (l.CODIGO = ctrl.LOC_CODIGO)
	WHERE 1 = 2
		group by 
			ctrl.EMP_CODIGO,
			l.LOC_COD_EMPRESA,
			ctrl.FECHA_COMERCIAL,
			ctrl.CAJ_CODIGO
	)

######################
consulta
######################
select 1 from #reco#
