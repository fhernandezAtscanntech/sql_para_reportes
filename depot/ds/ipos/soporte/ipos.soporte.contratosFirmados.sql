multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
		select 
			e.codigo	"Cod.Empresa",
			e.descripcion	"Empresa",
			r.descripcion	"Raz�n Social",
			r.ruc	"RUT",
			l.codigo	"Cod.Local",
			l.loc_cod_empresa	"Nro.Suc",
			l.descripcion	"Local",
			tc.descripcion 	"Tipo de Contrato",
			vc.descripcion 	"Vendedor",
			ct.nombre	"Nom. Contrato",
			ct.descripcion	"Desc. Contrato",
			ct.vigencia_hasta	"Vigencia Contrato",
			ct.nombre_arch	"Nom. Archivo Contr",
			decode (ct.es_por_local, 0, '''||'NO'||''', '''||'SI'||''')	"Por Local",
			decode (ct.es_por_caja, 0, '''||'NO'||''', '''||'SI'||''')	"Por caja",
			ct.proc_carga	"Proc. de carga",
			decode (ct.requiere_datos, 0, '''||'NO'||''', '''||'SI'||''')	"Req. datos",
			ct.habilitado	"Habilitado",
			cf.caj_codigo 	"Caja",
			cf.audit_date	"Fecha Firma",
			ce.descripcion 	"Estado Contrato Firmado",
			cf.nombre_arch	"Nombre Archivo"
		from 
			iposs.empresas e,
			iposs.locales l,
			iposs.razones_sociales r,
			iposs.tipos_de_contratos tc,
			iposs.contratos_estados ce , 
			iposs.vendedores_contratos vc, 
			iposs.contratos_tipos ct,
			iposs.contratos_firmados cf
		where 	e.codigo = l.emp_codigo
		and 	r.codigo = l.raz_soc_codigo
		and 	cf.emp_codigo = l.emp_codigo
		and 	cf.loc_codigo = l.codigo
		and 	cf.con_codigo = ct.codigo
		and 	ct.tipo_cont_codigo = tc.codigo(+)
		and 	cf.vend_codigo = vc.codigo(+)
		and 	cf.estado = ce.codigo
		and 	cf.audit_date >= to_date('''||:FechaDesde||''', '''||'dd/mm/yyyy'||''')
		and 	cf.audit_date <= to_date('''||:FechasHasta||''', '''||'dd/mm/yyyy'||''') + (1 - (1/24/60/60))
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
	select 
		e.codigo	"Cod.Empresa",
		e.descripcion	"Empresa",
		r.descripcion	"Raz�n Social",
		r.ruc	"RUT",
		l.codigo	"Cod.Local",
		l.loc_cod_empresa	"Nro.Suc",
		l.descripcion	"Local",
		tc.descripcion 	"Tipo de Contrato",
		vc.descripcion 	"Vendedor",
		ct.nombre	"Nom. Contrato",
		ct.descripcion	"Desc. Contrato",
		ct.vigencia_hasta	"Vigencia Contrato",
		ct.nombre_arch	"Nom. Archivo Contr",
		'NO' "Por Local",
		'NO' "Por caja",
		ct.proc_carga	"Proc. de carga",
		'NO' "Req. datos",
		ct.habilitado	"Habilitado",
		cf.caj_codigo 	"Caja",
		cf.audit_date	"Fecha Firma",
		ce.descripcion 	"Estado Contrato Firmado",
		cf.nombre_arch	"Nombre Archivo"
	from 
		iposs.empresas e,
		iposs.locales l,
		iposs.razones_sociales r,
		iposs.tipos_de_contratos tc,
		iposs.contratos_estados ce , 
		iposs.vendedores_contratos vc, 
		iposs.contratos_tipos ct,
		iposs.contratos_firmados cf
	where 1 = 2
)


######################
consulta
######################
select 1 from #reco#

