select 
	fm.emp_codigo 	"Cod.Empresa", 
	fm.loc_codigo 	"Cod.Local", 
	l.codigo 		"Cod.Local(I)", 
	e.descripcion 	"Empresa", 
	l.descripcion 	"Local", 
	fm.caj_codigo 	"Cod.Caja", 
	fo.descripcion 	"Operación", 
	fm.identificador 	"Identificador", 
	f.fun_cod_empresa 	"Cod.Funcionario", 
	f.apellido 			"Apellido", 
	fm.fecha_proceso 	"Fecha Proceso", 
	fm.audit_date 		"Fecha Audit", 
	decode (
		fm.fecha_proceso, 
		to_date('01/01/2100', 'dd/mm/yyyy'), 'Sin Procesar', 
		'Procesado') 	"Procesado", 
	case nvl (fm.fac_cod_error, -1)
		when 0 then 'Error general'
		when 1 then 'Error aplicación'
		when -1 then 'Sin Error'
		else 'Error '||to_char(fm.fac_cod_error)
	end 			"Error", 
	case nvl (fm.fac_cod_error, -1)
		when -1 then fm.dis_numero
		else null
	end 					"Distribución", 
 	fm.fac_msg_error  		"Mensaje"
from 
	iposs.facade_mensajes fm, 
	iposs.facade_operaciones fo, 
	iposs.empresas e, 
	iposs.locales l, 
	iposs.funcionarios f
where 	fm.emp_codigo = e.codigo
and 	fm.loc_codigo = l.loc_cod_empresa 
and 	fm.emp_codigo = l.emp_codigo
and 	fm.fun_codigo = f.codigo
and 	fm.fac_ope_codigo = fo.codigo
and 	(:Empresa is null or fm.emp_codigo = :Empresa)
and 	(:Local is null or fm.loc_codigo = :Local)
and 	(:Caja is null or fm.caj_codigo = :Caja)
and 	fm.fecha_proceso >= :Fecha
