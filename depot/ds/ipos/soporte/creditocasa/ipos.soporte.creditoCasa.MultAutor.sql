select
	trunc(fecha_comercial) 	"Fecha Comercial", 
	emp_codigo 				"Cod.Empresa", 
	loc_codigo 				"Cod.Local (I)", 
	caj_codigo 				"Caja", 
	numero_operacion 		"Nro.Operación", 
	cre_tm_codigo 			"Cod.Tipo Movimiento", 
	importe 				"Importe", 
	cre_cc_codigo 			"Cod.CC", 
	count(*) 				"Cantidad"
from
	iposs.cre_movimientos_cc
where 	fecha_comercial between :FechaDesde and :FechaHasta
-- 	where 	fecha_comercial between '01-NOV-2015' and '15-DEC-2015'
and 	numero_operacion is not null
group by
	trunc(fecha_comercial), 
	emp_codigo, 
	loc_codigo, 
	caj_codigo, 
	numero_operacion, 
	cre_tm_codigo, 
	importe, 
	cre_cc_codigo
having count(*) > 1
order by
	emp_codigo asc, 
	trunc(fecha_comercial) asc
