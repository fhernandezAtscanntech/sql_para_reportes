select
	mov.emp_codigo 			"Cod.Empresa", 
	mov.fecha_comercial 	"Fecha Comercial", 
	mov.tipo_operacion 		"Tipo Operación", 
	mov.loc_cod_empresa 	"Cod.Local", 
	mov.caj_codigo 			"Caja", 
	mov.hora_operacion 		"Hora", 
	mov.numero_operacion 	"Nro.Operación", 
	mov.total 				"Total", 
	mov.observacion 		"Observacion"
from
	movimientos mov, 
	movimientos_det_servs mdet
-- where 	mov.emp_codigo = 2032
-- and 	mov.fecha_comercial between '05-JAN-2016' and '16-FEB-2016'
where 	mov.fecha_comercial between :FechaDesde and :FechaHasta
and 	mov.tipo_operacion = 'VENTA'
and 	mdet.fecha_comercial = mov.fecha_comercial
and 	mdet.mov_emp_codigo = mov.emp_codigo
and 	mdet.mov_numero_mov = mov.numero_mov
and 	mdet.ser_codigo = 328
and 	mov.numero_operacion not in
			(select mvcc.numero_operacion from cre_movimientos_cc mvcc
			where 	mvcc.emp_codigo = mov.emp_codigo
			and 	trunc(mvcc.fecha_comercial) = mov.fecha_comercial
			and 	mvcc.loc_codigo = mov.loc_codigo
			and 	mvcc.caj_codigo = mov.caj_codigo
			and 	mvcc.numero_operacion is not null)
