select
	m.emp_codigo 			"Cod.Empresa", 
	m.loc_cod_empresa 		"Cod.Local", 
	m.fecha_comercial 		"Fecha Comercial", 
	m.numero_mov 			"Numero Mov.", 
	m.tipo_operacion 		"Tipo Operación", 
	m.numero_operacion 		"Nro. Operación", 
	m.hora_operacion 		"Hora", 
	m.registradora 			"Registradora", 
	m.total 				"Total Mov.", 
	m.observacion			"Observación",
	c.codigo 				"Cod.Mov.CC", 
	c.cre_tm_codigo 		"Cod.Tipo Movimiento", 
	tm.descripcion 			"Tipo Movimiento CC", 
	c.importe_pago 			"Importe Pago",
	cli.nombre 				"Nombre Cliente", 
	cli.apellido 			"Apellido Cliente", 
	cli.numero_doc 			"Numero Doc. Cliente", 
	cli.cli_cod_externo 	"Cod.Cliente"
from
	iposs.movimientos m, 
	iposs.cre_movimientos_cc c, 
	iposs.cre_tipos_movimientos tm, 
	iposs.cre_cuentas_corrientes cc, 
	iposs.cre_clientes crecli, 
	iposs.clientes cli
where 	m.fecha_comercial between :FechaDesde and :FechaHasta
-- where 	m.fecha_comercial between '01-DEC-2015' and '08-DEC-2015'
and 	m.tipo_operacion = 'VENTA ANULADA'
and 	m.emp_codigo = c.emp_codigo
and 	m.loc_codigo = c.loc_codigo
and 	m.registradora = m.caj_codigo
and 	(m.v0_numero_operacion = c.v0_numero_operacion 
		or c.v0_numero_operacion is null)
and 	m.numero_operacion = c.numero_operacion
and 	c.cre_tm_codigo = tm.codigo
and 	c.cre_cc_codigo = cc.codigo
and 	cc.cli_codigo = crecli.codigo
and 	cli.emp_codigo = m.emp_codigo
and 	crecli.per_codigo = cli.per_codigo