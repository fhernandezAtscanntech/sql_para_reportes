select
	e.codigo 				"Cod.Empresa", 
	e.descripcion 			"Empresa", 
	cc.codigo 	"Cod.CC", 
	cc.saldo 	"Saldo", 
	sum (DECODE (
			CTM.SIGNO, 
			0, nvl(CM.IMPORTE, 0), 
			1, nvl (CM.IMPORTE, 0) * -1) ) * (-1) as "Suma Mov.", 
	(sum (DECODE (
			CTM.SIGNO, 
			0, nvl (CM.IMPORTE, 0), 
			1, CM.IMPORTE*-1) ) * (-1) - nvl(cc.saldo, 0)) as "Diferencia"
from
	iposs.CRE_MOVIMIENTOS_CC CM, 
	iposs.CRE_TIPOS_MOVIMIENTOS CTM, 
	iposs.cre_cuentas_corrientes cc, 
	iposs.cre_sellos_empresas cse, 		-- Fredy
	iposs.empresas e 					-- Fredy
where	CM.CRE_TM_CODIGO not in (6, 7, 8) -- Fredy
and 	CM.CRE_TM_CODIGO = CTM.CODIGO
and 	cc.codigo = cm.cre_cc_codigo
and 	cc.cre_sel_codigo = cse.cre_sel_codigo 	-- Fredy
and 	e.codigo = cse.emp_codigo  				-- Fredy
group by 
	e.codigo, 
	e.descripcion, 
	cc.codigo, 
	cc.saldo
having 				-- Fredy
	sum (DECODE 
		(CTM.SIGNO, 
		0, nvl (CM.IMPORTE, 0), 
		1, nvl (CM.IMPORTE, 0) * -1) ) * (-1) - nvl(cc.saldo, 0) <> 0
