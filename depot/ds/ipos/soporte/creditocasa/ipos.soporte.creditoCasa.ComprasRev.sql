select
	cm.fecha_comercial 		"Fecha Comercial", 
	cm.emp_codigo 			"Cod.Empresa", 
	m.loc_cod_empresa 		"Cod.Local", 
	cm.caj_codigo 			"Caja", 
	cm.v0_numero_operacion 	"Nro.Operación (V0)", 
	cm.numero_operacion 	"Nro.Operación", 
	cm.importe 				"Importe", 
	cm.cre_tm_codigo 		"Cod.Tipo Movimiento", 
	cm.importe_pago 		"Importe Pago",
	m.numero_mov 			"Numero Mov.", 
	m.tipo_operacion 		"Tipo Operación", 
	m.hora_operacion 		"Hora", 
	m.observacion 			"Observación",
	cl.nombre 				"Nombre Cliente", 
	cl.apellido 			"Apellido Cliente", 
	cl.cli_cod_externo 		"Cod.Cliente"
from
	iposs.cre_movimientos_cc cm, 
	iposs.movimientos m, 
	iposs.cre_cuentas_corrientes cc, 
	iposs.cre_clientes crcl, 
	iposs.clientes cl
where 	m.fecha_comercial between :FechaDesde and :FechaHasta
-- 	where 	m.fecha_comercial between '01-DEC-2015' and '31-DEC-2015'
and 	cm.cre_tm_codigo = 6
and 	m.tipo_operacion = 'VENTA'
and 	m.fecha_comercial = trunc(cm.fecha_comercial)
and 	m.emp_codigo = cm.emp_codigo
and 	m.loc_codigo = cm.loc_codigo
and 	m.caj_codigo = cm.caj_codigo
and 	m.numero_operacion = cm.numero_operacion
and 	m.v0_numero_operacion = cm.v0_numero_operacion
and 	cm.cre_cc_codigo = cc.codigo
and 	cc.cli_codigo = crcl.codigo
and 	crcl.per_codigo = cl.per_codigo
order by 
	m.fecha_comercial desc
