select distinct 
	f.codigo, 
	f.fun_cod_empresa, 
	f.fun_cod_empresa || ' - ' || f.fun_descripcion Funcionario
from 
	dem_prod.l_funcionarios f, 
	iposs.funcionarios_locales fl, 
	#LocalesVisibles# lv
where 
	(f.fun_codigo = fl.fun_codigo
and 	 fl.loc_codigo = lv.loc_codigo)
order by 
	f.fun_cod_empresa