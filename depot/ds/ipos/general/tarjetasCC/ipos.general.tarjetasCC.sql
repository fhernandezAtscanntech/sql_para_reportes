select
	t.codigo,
	t.numero_tarjeta ||' - '||c.cli_cod_externo||' - '|| c.nombre
from 
	iposs.clientes c, 
	iposs.cre_tarjetas t, 
	iposs.cre_clientes cc, 
	iposs.cre_sellos cs, 
	iposs.cre_sellos_empresas cse
where 	cc.per_codigo = c.per_codigo
and 	cc.codigo = t.cli_codigo
and 	cs.codigo = cse.cre_sel_codigo
and 	cs.cre_codigo = (select to_number(nvl (k_param.f_param('COD_CRE_CUE'), 0)) from dual)
and 	cc.cre_sel_codigo = cs.codigo
and 	t.cre_sel_codigo = cs.codigo
and 	c.emp_codigo = $EMPRESA_LOGUEADA$
and 	cse.emp_codigo = $EMPRESA_LOGUEADA$