select
	c.codigo,
	ce.cre_cod_externo||' - '|| c.descripcion Credito
from 
	iposs.creditos c, 
	iposs.creditos_empresas ce, 
	iposs.cre_sellos s, 
	iposs.cre_sellos_empresas se
where 	c.codigo = ce.cre_codigo
and 	c.codigo = s.cre_codigo
and 	s.codigo = se.cre_sel_codigo
and 	se.emp_codigo = $EMPRESA_LOGUEADA$
and 	ce.emp_codigo = $EMPRESA_LOGUEADA$
