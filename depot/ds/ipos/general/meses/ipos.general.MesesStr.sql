select 
	to_char(f1.codigo, 'mm/yyyy') as MesString, 
	f1.mes_nombre_corto||'/'||f1.ano_nombre as Meses
from 
	dwm_prod.l_fechas f1
where f1.codigo = 
	(select min(codigo) 
	from dwm_prod.l_fechas f2
	where f1.mes_codigo = f2.mes_codigo)
order by f1.codigo;