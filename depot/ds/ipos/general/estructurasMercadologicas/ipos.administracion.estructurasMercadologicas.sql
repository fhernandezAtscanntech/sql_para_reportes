select 
	e.nivel_1_desc	"Nivel 1", 
	e.nivel_2_desc	"Nivel 2", 
	e.nivel_3_desc	"Nivel 3",
	e.nivel_4_desc	"Nivel 4",
	e.nivel_5_desc	"Nivel 5",
	e.nivel_6_desc	"Nivel 6",
	e.nivel_7_desc	"Nivel 7",
	e.nivel_8_desc	"Nivel 8",
	e.nivel_9_desc	"Nivel 9",
	e.nivel_10_desc "Nivel 10"
from 
	iposs.est_mer_plana e
	

/*
creacion de 0

create table fredy_est_mer_plana_back as (select * from est_mer_plana);

delete from est_mer_plana
/

insert into est_mer_plana
	(est_mer_codigo, nivel_1) 
	(select codigo, 99 
	from estructuras_mercadologicas 
	where emp_codigo = 1)
/

begin
	for es in (select * from est_mer_plana where nivel_1 = 99)
	loop
		for arbol in 
			(select e.codigo, e.nivel, e.descripcion 
			from estructuras_mercadologicas e
            start with e.codigo = es.est_mer_codigo
			connect by prior e.est_mer_codigo = codigo) 
		loop
			update est_mer_plana set
				nivel_1 = decode(arbol.nivel,1,arbol.codigo,nivel_1),
				nivel_2 = decode(arbol.nivel,2,arbol.codigo,nivel_2),
				nivel_3 = decode(arbol.nivel,3,arbol.codigo,nivel_3),
				nivel_4 = decode(arbol.nivel,4,arbol.codigo,nivel_4),
				nivel_5 = decode(arbol.nivel,5,arbol.codigo,nivel_5),
				nivel_6 = decode(arbol.nivel,6,arbol.codigo,nivel_6),
				nivel_7 = decode(arbol.nivel,7,arbol.codigo,nivel_7),
				nivel_8 = decode(arbol.nivel,8,arbol.codigo,nivel_8),
				nivel_9 = decode(arbol.nivel,9,arbol.codigo,nivel_9),
				nivel_10 = decode(arbol.nivel,10,arbol.codigo,nivel_10),
				nivel_1_desc = decode(arbol.nivel,1,arbol.descripcion,nivel_1_desc),
				nivel_2_desc = decode(arbol.nivel,2,arbol.descripcion,nivel_2_desc),
				nivel_3_desc = decode(arbol.nivel,3,arbol.descripcion,nivel_3_desc),
				nivel_4_desc = decode(arbol.nivel,4,arbol.descripcion,nivel_4_desc),
				nivel_5_desc = decode(arbol.nivel,5,arbol.descripcion,nivel_5_desc),
				nivel_6_desc = decode(arbol.nivel,6,arbol.descripcion,nivel_6_desc),
				nivel_7_desc = decode(arbol.nivel,7,arbol.descripcion,nivel_7_desc),
				nivel_8_desc = decode(arbol.nivel,8,arbol.descripcion,nivel_8_desc),
				nivel_9_desc = decode(arbol.nivel,9,arbol.descripcion,nivel_9_desc),
				nivel_10_desc = decode(arbol.nivel,10,arbol.descripcion,nivel_10_desc)
            where est_mer_codigo = es.est_mer_codigo;
		end loop;
	end loop;
end;
/

*/
