select 
	c.descripcion 		"Cr�dito", 
	e.descripcion 		"Empresa", 
	l.descripcion 		"Local", 
	ccc.emp_codigo 		"Cod.Empresa", 
	ccc.loc_codigo 		"Cod.Local", 
	ccc.caj_codigo 		"Caja", 
	ccc.comercio		"Comercio", 
	ccc.comercio_cierre 	"Comercio cierre", 
	ccc.terminal		"Terminal", 
	ccc.usa_iso 		"Usa ISO", 
	ccc.ip				"IP", 
	ccc.puerto	 		"Puerto", 
	ccc.send_timeout 	"Send Timeout", 
	ccc.receive_timeout "Receive Timeout", 
	ccc.connect_timeout "Connect Timeout", 
	ccc.pos_MasterKey 	"Pos Master Key", 
	ccc.workingKey 		"Working Key", 
	ccc.usa_ssl 		"Usa SSL", 
	ccc.ticket_voucher 	"Ticket Voucher", 
	ccc.ingreso_manual 	"Ingreso Manual", 
	ccc.comercio_debito "Comercio Debito"
from 
	iposs.creditos_conf_cajas ccc, 
	iposs.empresas e, 
	iposs.locales l, 
	iposs.creditos c
where 	ccc.cre_codigo = c.codigo
and 	ccc.emp_codigo = e.codigo
and 	ccc.loc_codigo = l.codigo
and 	e.descripcion not like '%BASE2%'
and 	e.descripcion not like '%NO_USAR%'
and 	l.descripcion not like '%NO_USAR%'
and 
		(:EmpCodigo is null or ccc.emp_codigo = :EmpCodigo)
and 	(:LocCodigo is null or ccc.loc_codigo = :LocCodigo)
and 	(:CajCodigo is null or ccc.caj_codigo = :CajCodigo)
and 	( trim (upper (':Credito')) = 'NULL' or c.codigo in :Credito )
and 	(trim (:Comercio) is null or trim (upper (ccc.comercio)) like '%'||trim (upper (:Comercio))||'%' )
and 	(trim (:Terminal) is null or trim (upper (ccc.terminal)) like '%'||trim (upper (:Terminal))||'%' )
and 	(:EmpCodigo is not null or
		 :LocCodigo is not null or
		 :CajCodigo is not null or
		 :Comercio is not null or
		 :Terminal is not null or
		 trim (upper (':Credito')) <> 'NULL')