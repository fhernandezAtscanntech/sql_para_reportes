select 
	l.loc_descripcion 	"Local", 
	l.loc_codigo 			"Cod.Local", 
	l.emp_codigo 			"Emp.Codigo", 
	fvd.fecha_comercial 		"Fecha", 
	i.iva_descripcion 		"Tipo IVA", 
	nvl (sum (fvd.iva_importe), 0)  "IVA", 
	nvl (sum (fvd.ven_nac_importe), 0) - 
		nvl (sum (fvd.dev_nac_importe), 0) - 
		nvl (sum (fvd.iva_importe), 0)  	"Venta s/IVA", 
	nvl (sum (fvd.mov_cantidad), 0) 		"Cantidad Tickets"
from 
	dwm_prod.l_ubicaciones l, 
	dwm_prod.l_impuestos i, 
	dwm_prod.f_ventas_pu_diario fvd
where
	( l.codigo = fvd.l_ubi_codigo ) 
and
	( i.codigo = fvd.l_imp_codigo )
and
	( fvd.fecha_comercial between :FechaDesde and :FechaHasta )
and
	( l.loc_codigo in :Local)
and 
	( l.emp_codigo = $EMPRESA_LOGUEADA$
and 	  fvd.emp_codigo = $EMPRESA_LOGUEADA$
and 	  i.emp_codigo in ($EMPRESA_LOGUEADA$, $EMPRESA_MODELO$) )
group by 
	l.loc_descripcion, 
	l.loc_codigo, 
	l.emp_codigo, 
	fvd.fecha_comercial, 
	i.iva_descripcion
