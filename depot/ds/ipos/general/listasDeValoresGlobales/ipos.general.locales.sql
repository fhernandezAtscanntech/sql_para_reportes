select
	l.codigo,
	l.emp_codigo||' - '|| l.loc_cod_empresa || ' - ' || l.descripcion descripcion
from 
	locales l
where 	coord_x is null
and 	fecha_borrado is null
order by l.emp_codigo, l.loc_cod_empresa