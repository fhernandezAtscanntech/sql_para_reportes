select
	distinct nvl (l.prom_codigo, p.codigo) as Codigo,
	decode (l.prom_descripcion||'('||l.cpa_descripcion||')', 
			'()', p.descripcion||'('||c.descripcion||')', 
			l.prom_descripcion||'('||l.cpa_descripcion||')') as Descripcion
from
	dwm_prod.l_promociones l,
	iposs.promociones p, 
	iposs.campañas c
where 	p.codigo = l.prom_codigo (+)
and 	p.cpa_codigo = c.codigo
order by 2
-- la descripción del DW es mejor, pero si no hay ventas igual tengo que mostrar la promo

