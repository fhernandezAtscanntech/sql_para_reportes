/*
SELECT
        l.emp_codigo            ,
        l.loc_codigo            ,
        loc.DESCRIPCION         ,
        SUM (mov_cantidad)                                                                              Total,
        SUM(CASE WHEN dist.CONFIG LIKE 'producto=SC%' THEN mov_cantidad ELSE 0 END )/SUM (mov_cantidad) SC   ,
        SUM(CASE WHEN dist.CONFIG LIKE 'producto=IA%' THEN mov_cantidad ELSE 0 END )/SUM (mov_cantidad) IA   ,
        SUM(CASE WHEN dist.CONFIG LIKE 'producto=IM%' THEN mov_cantidad ELSE 0 END )/SUM (mov_cantidad) IM
FROM
        dwm_prod.l_cajas                   l   ,
        dwm_prod.f_ventas_diario_min       f   ,
        iposs.locales                      loc ,
        iposs.LOCALIDADES                  loca,
        iposs.DISTRIBUCIONES_PENDIENTES    dist
WHERE
        f.l_caj_codigo      = l.codigo
AND     loc.CODIGO          = l.loc_codigo
AND     loc.LOC_COD_EMPRESA < 99
AND     loc.LOCA_CODIGO     = loca.CODIGO
AND     dist.LOC_CODIGO     = l.loc_codigo
AND     dist.CAJ_CODIGO     = l.caj_codigo
AND     dist.CONFIG      LIKE 'producto=%'
AND     f.fecha_comercial   > SYSDATE - 7
AND 	f.fecha_comercial   < trunc(SYSDATE)
AND     dist.CONFIG  NOT LIKE '%HTML%'
AND     l.emp_codigo IN
        (
                SELECT
                        dist2.emp_codigo
                FROM
                        iposs.DISTRIBUCIONES_PENDIENTES dist2
                WHERE
                        dist2.config          LIKE 'producto=SC%'
                AND     dist2.FECHA_ULT_CONSULTA > SYSDATE-30)
GROUP BY
        l.emp_codigo,
        l.loc_codigo,
        loc.DESCRIPCION
ORDER BY
        5 DESC;
*/

multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
		SELECT
				l.emp_codigo            ,
				l.loc_codigo            ,
				loc.DESCRIPCION         ,
				SUM (mov_cantidad)                                                                              Total,
				SUM(CASE WHEN dist.CONFIG LIKE '''||'producto=SC%'||''' THEN mov_cantidad ELSE 0 END )/SUM (mov_cantidad) SC   ,
				SUM(CASE WHEN dist.CONFIG LIKE '''||'producto=IA%'||''' THEN mov_cantidad ELSE 0 END )/SUM (mov_cantidad) IA   ,
				SUM(CASE WHEN dist.CONFIG LIKE '''||'producto=IM%'||''' THEN mov_cantidad ELSE 0 END )/SUM (mov_cantidad) IM
		FROM
				dwm_prod.l_cajas                   l   ,
				dwm_prod.f_ventas_diario_min       f   ,
				iposs.locales                      loc ,
				iposs.LOCALIDADES                  loca,
				iposs.DISTRIBUCIONES_PENDIENTES    dist
		WHERE
				f.l_caj_codigo      = l.codigo
		AND     loc.CODIGO          = l.loc_codigo
		AND     loc.LOC_COD_EMPRESA < 99
		AND     loc.LOCA_CODIGO     = loca.CODIGO
		AND     dist.LOC_CODIGO     = l.loc_codigo
		AND     dist.CAJ_CODIGO     = l.caj_codigo
		AND     dist.CONFIG      LIKE '''||'producto=%'||'''
		AND     f.fecha_comercial   > SYSDATE - 7
		AND 	f.fecha_comercial   < trunc(SYSDATE)
		AND     dist.CONFIG  NOT LIKE '''||'%HTML%'||'''
		AND     l.emp_codigo||l.loc_codigo IN 
				(
						SELECT dist2.emp_codigo||dist2.loc_codigo
						FROM
								iposs.DISTRIBUCIONES_PENDIENTES dist2
						WHERE
								dist2.config          LIKE '''||'producto=SC%'||'''
						AND     dist2.FECHA_ULT_CONSULTA > SYSDATE-30)
		GROUP BY
				l.emp_codigo,
				l.loc_codigo,
				loc.DESCRIPCION
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
SELECT
        l.emp_codigo            ,
        l.loc_codigo            ,
        loc.DESCRIPCION         ,
        SUM (mov_cantidad)                                                                              Total,
        SUM(CASE WHEN dist.CONFIG LIKE 'producto=SC%' THEN mov_cantidad ELSE 0 END )/SUM (mov_cantidad) SC   ,
        SUM(CASE WHEN dist.CONFIG LIKE 'producto=IA%' THEN mov_cantidad ELSE 0 END )/SUM (mov_cantidad) IA   ,
        SUM(CASE WHEN dist.CONFIG LIKE 'producto=IM%' THEN mov_cantidad ELSE 0 END )/SUM (mov_cantidad) IM
FROM
        dwm_prod.l_cajas                   l   ,
        dwm_prod.f_ventas_diario_min       f   ,
        iposs.locales                      loc ,
        iposs.LOCALIDADES                  loca,
        iposs.DISTRIBUCIONES_PENDIENTES    dist
WHERE
        1=2
group by 
		l.emp_codigo            ,
        l.loc_codigo            ,
        loc.DESCRIPCION         
)

######################
consulta
######################
select 1 from #reco#


