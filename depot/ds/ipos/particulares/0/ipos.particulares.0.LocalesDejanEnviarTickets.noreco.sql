select 
	tabla.emp_codigo||tabla.loc_codigo 	Codigo_unico, 
	loc.descripcion 					Nombre_Fantasia, 
	raz.descripcion 					Razon_Social, 
	loca.descripcion 					Localidad, 
	dep.descripcion 					Departamento, 
	Sem_2 								SEM_2, 
	Sem_1 								SEM_1, 
	Sem 								SEM, 
	to_char (1 - (Sem_1 / nullif (Sem_2, 0)), '9999.99') 	Var1, 
	to_char (1 - (Sem / nullif(Sem_1, 0)), '9999.99') 		Var, 
	Ultimo 								Ultimo
from (
	select 
		emp_codigo, 
		loc_codigo, 
		max (fecha_comercial) Ultimo,
		sum (case when fecha_comercial 
			between trunc(sysdate) - 21 and trunc(sysdate) - 15 
			THEN 1 
			ELSE 0 
			END) as Sem_2,
		sum (case when fecha_comercial 
			between trunc(sysdate) - 14 and trunc(sysdate) - 8 
			THEN 1 
			ELSE 0 
			END) as Sem_1,
		sum(case when fecha_comercial 
			between trunc(sysdate) - 7 and trunc(sysdate) - 1 
			THEN 1 
			ELSE 0 
			END) as Sem
	from 
		movimientos
	where 	fecha_comercial between trunc(sysdate) - 21 and trunc(sysdate) - 1
	group by 
		emp_codigo, 
		loc_codigo) tabla 
join 
	locales loc 
	on tabla.loc_codigo = loc.CODIGO
full join 
	localidades loca 
	on loc.loca_codigo = loca.codigo
full join 
	departamentos dep 
	on loca.depa_codigo = dep.codigo
full join 
	razones_sociales raz 
	on loc.raz_soc_codigo = raz.codigo
where 	sem / nullif (sem_1, 0) < 0.7 
and 	loc.loc_cod_empresa < 99
order by var desc
