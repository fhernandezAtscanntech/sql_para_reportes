/*SELECT
        confCre.TERMINAL   ,
        dis.EMP_CODIGO     ,
        dis.LOC_CODIGO     ,
        loc.LOC_COD_EMPRESA,
        dis.CAJ_CODIGO     ,
        dis.CONFIG         ,
        loc.DESCRIPCION
FROM
        DISTRIBUCIONES_PENDIENTES dis
JOIN    LOCALES loc
		ON	dis.LOC_CODIGO = loc.CODIGO
FULL JOIN 	PARAMETROS_CAJAS_FE paramCaj
		ON 	paramCaj.LOC_CODIGO    = dis.LOC_CODIGO
		AND paramCaj.CAJ_CODIGO    = dis.CAJ_CODIGO
		AND paramCaj.PAR_FE_NUMERO = 3093
FULL JOIN 	PARAMETROS_LOCALES_FE paramLoc
		ON 	paramLoc.LOC_CODIGO    = dis.LOC_CODIGO
		AND paramLoc.PAR_FE_NUMERO = 3093
		AND paramLoc.PAR_FE_TIPO   ='BANDERA'
FULL JOIN 	PARAMETROS_EMPRESAS_FE paramEmp
		ON 	paramEmp.EMP_CODIGO    = dis.EMP_CODIGO
		AND paramEmp.PAR_FE_NUMERO = 3093
		AND paramEmp.PAR_FE_TIPO   ='BANDERA'
FULL JOIN 	IPOSS.CREDITOS_CONF_CAJAS confCre
		ON 	confCre.LOC_CODIGO = dis.LOC_CODIGO
		AND	confCre.CAJ_CODIGO = dis.CAJ_CODIGO
		AND	confCre.CRE_CODIGO = 24
WHERE 	dis.FECHA_ULT_CONSULTA > SYSDATE-10
AND     (dis.CONFIG LIKE 'producto=IA%'
        OR dis.CONFIG LIKE 'producto=SC%')
AND     (paramCaj.VALOR = 1
        OR paramLoc.VALOR = 1
        OR paramEmp.VALOR = 1 )
AND     dis.EMP_CODIGO NOT IN (5000,5001)
ORDER BY
        2,
        3,
        5
08/11/2019 cambia la condición: van todos los productos
		dis.CONFIG LIKE 'producto=%'
*/
multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
		SELECT
				confCre.TERMINAL   ,
				dis.EMP_CODIGO     ,
				dis.LOC_CODIGO     ,
				loc.LOC_COD_EMPRESA,
				dis.CAJ_CODIGO     ,
				dis.CONFIG         ,
				loc.DESCRIPCION
		FROM
				DISTRIBUCIONES_PENDIENTES dis
		JOIN    LOCALES loc
				ON	dis.LOC_CODIGO = loc.CODIGO
		FULL JOIN 	PARAMETROS_CAJAS_FE paramCaj
				ON 	paramCaj.LOC_CODIGO    = dis.LOC_CODIGO
				AND paramCaj.CAJ_CODIGO    = dis.CAJ_CODIGO
				AND paramCaj.PAR_FE_NUMERO = 3093
		FULL JOIN 	PARAMETROS_LOCALES_FE paramLoc
				ON 	paramLoc.LOC_CODIGO    = dis.LOC_CODIGO
				AND paramLoc.PAR_FE_NUMERO = 3093
				AND paramLoc.PAR_FE_TIPO   = '''||'BANDERA'||'''
		FULL JOIN 	PARAMETROS_EMPRESAS_FE paramEmp
				ON 	paramEmp.EMP_CODIGO    = dis.EMP_CODIGO
				AND paramEmp.PAR_FE_NUMERO = 3093
				AND paramEmp.PAR_FE_TIPO   = '''||'BANDERA'||'''
		FULL JOIN 	IPOSS.CREDITOS_CONF_CAJAS confCre
				ON 	confCre.LOC_CODIGO = dis.LOC_CODIGO
				AND	confCre.CAJ_CODIGO = dis.CAJ_CODIGO
				AND	confCre.CRE_CODIGO = 24
		WHERE 	dis.FECHA_ULT_CONSULTA > SYSDATE-10
		AND     dis.CONFIG LIKE '''||'producto=%'||'''
		AND     (paramCaj.VALOR = 1
				OR paramLoc.VALOR = 1
				OR paramEmp.VALOR = 1 )
		AND     dis.EMP_CODIGO NOT IN (5000,5001)
		ORDER BY
				2,
				3,
				5
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
	SELECT
			confCre.TERMINAL   ,
			dis.EMP_CODIGO     ,
			dis.LOC_CODIGO     ,
			loc.LOC_COD_EMPRESA,
			dis.CAJ_CODIGO     ,
			dis.CONFIG         ,
			loc.DESCRIPCION
	FROM
			DISTRIBUCIONES_PENDIENTES dis
	JOIN    LOCALES loc
			ON	dis.LOC_CODIGO = loc.CODIGO
	FULL JOIN 	PARAMETROS_CAJAS_FE paramCaj
			ON 	paramCaj.LOC_CODIGO    = dis.LOC_CODIGO
			AND paramCaj.CAJ_CODIGO    = dis.CAJ_CODIGO
			AND paramCaj.PAR_FE_NUMERO = 3093
	FULL JOIN 	PARAMETROS_LOCALES_FE paramLoc
			ON 	paramLoc.LOC_CODIGO    = dis.LOC_CODIGO
			AND paramLoc.PAR_FE_NUMERO = 3093
			AND paramLoc.PAR_FE_TIPO   = 'BANDERA'
	FULL JOIN 	PARAMETROS_EMPRESAS_FE paramEmp
			ON 	paramEmp.EMP_CODIGO    = dis.EMP_CODIGO
			AND paramEmp.PAR_FE_NUMERO = 3093
			AND paramEmp.PAR_FE_TIPO   =  'BANDERA'
	FULL JOIN 	IPOSS.CREDITOS_CONF_CAJAS confCre
			ON 	confCre.LOC_CODIGO = dis.LOC_CODIGO
			AND	confCre.CAJ_CODIGO = dis.CAJ_CODIGO
			AND	confCre.CRE_CODIGO = 24
	WHERE 1 = 2
)

######################
consulta
######################
select 1 from #reco#


