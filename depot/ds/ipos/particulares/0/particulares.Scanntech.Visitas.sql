-- consulta recibida de desarrollo
/*
original:
select visita.documento, visita.nombre, visita.apellido, marcas.fecha_inicio, marcas.fecha_fin, marcas.fecha_equipo,
marcas.emp_codigo, marcas.loc_codigo, marcas.caj_codigo
from inf_marcas marcas, inf_visitas visita where
marcas.inf_vis_codigo = visita.codigo
order by fecha_equipo desc;
*/

select 
	visita.documento 	"Documento", 
	visita.nombre 		"Nombre", 
	visita.apellido 	"Apellido", 
	marcas.fecha_inicio "Fecha Inicio", 
	marcas.fecha_fin 	"Fecha Fin", 
	marcas.fecha_equipo "Fecha Equipo",
	marcas.emp_codigo 	"Cod.Empresa", 
	e.descripcion 		"Empresa", 
	marcas.loc_codigo 	"Cod.Local", 
	l.descripcion 		"Local", 
	marcas.caj_codigo 	"Cod.Caja"
from 
	iposs.inf_marcas marcas, 
	iposs.inf_visitas visita, 
	iposs.empresas e, 
	iposs.locales l
where 	marcas.inf_vis_codigo = visita.codigo
and 	marcas.emp_codigo = e.codigo
and 	marcas.emp_codigo = l.emp_codigo
and 	marcas.loc_codigo = l.codigo
-- and 	marcas.fecha_equipo between :FechaDesde and :FechaHasta
order by 
	marcas.fecha_equipo desc
