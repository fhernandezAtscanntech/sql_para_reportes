select 
	f.ano_nombre 			"Año", 
	f.mes_nombre_corto 		"Mes", 
	u.loc_modelo_descripcion "Local", 
	p.prod_codigo 		"Cod.Producto", 
	p.codigo_barras 	"Cod.Barras", 
	p.prod_descripcion 	"Producto", 
	p.rub_descripcion 	"Rubro", 
	pv.precio 			"Precio", 
	nvl (sum (fvpu.ven_cantidad), 0) - nvl (sum (fvpu.dev_cantidad), 0) "Cantidad"
from 
	dwm_prod.l_fechas f, 
	dwm_prod.l_productos p, 
	dwm_prod.l_ubicaciones u, 
	iposs.precios_de_articulos_ventas pv, 
	dwm_prod.f_ventas_pu_mensual_min fvpu
where 	fvpu.mes_codigo = f.mes_codigo
and 	fvpu.l_ubi_codigo = u.codigo
and 	fvpu.l_pro_codigo = p.codigo
and 	fvpu.emp_codigo = p.emp_codigo
and 	pv.art_codigo = p.prod_cod_interno
and 	pv.loc_lis_loc_codigo = u.loc_codigo 
and 	pv.loc_lis_lis_pre_ve_codigo =
			(select codigo 
			from iposs.listas_de_precios_ventas lpv 
			where lis_pre_ve_cod_empresa = 1 
			and lpv.emp_codigo = u.emp_codigo)
and 	pv.emp_codigo = u.emp_codigo
and 	pv.vigencia <= (f.mes_fecha_fin - (f.mes_time_span - 1))
and 	pv.vigencia_hasta > (f.mes_fecha_fin - (f.mes_time_span - 1))
and 	p.fecha_prim_mov < (f.mes_fecha_fin - (f.mes_time_span - 1))
and 	f.codigo = (select max(f2.codigo) from dwm_prod.l_fechas f2 where f2.mes_codigo = f.mes_codigo)
and 	f.mes_fecha_fin < sysdate
and 	p.tip_prod_codigo = 1
and 	fvpu.mes_codigo = :Mes
and 	u.loc_codigo in :Local
and 	p.rub_codigo in :Rubro
and 	
		fvpu.emp_codigo = $EMPRESA_LOGUEADA$
and 	u.emp_codigo = $EMPRESA_LOGUEADA$
and 	p.emp_codigo = $EMPRESA_LOGUEADA$
group by 
	f.ano_nombre, 
	f.mes_nombre_corto, 
	u.loc_modelo_descripcion, 
	p.prod_codigo, 
	p.codigo_barras, 
	p.prod_descripcion, 
	p.rub_descripcion, 
	pv.precio
having 
	nvl (sum (fvpu.ven_cantidad), 0) - nvl (sum (fvpu.dev_cantidad), 0) > 0
