select 
	l.loc_descripcion 	"Local", 
	p.prod_descripcion 		"Articulo", 
	p.fam_descripcion 		"Familia", 
	sum (nvl (fvpu.ven_cantidad, 0)) - sum (nvl (fvpu.dev_cantidad, 0)) 	"Unidades"
from 
	dwm_prod.l_ubicaciones l, 
	iposs.locales lo, 
	dwm_prod.l_fechas f, 
	dwm_prod.l_productos p, 
	dwm_prod.f_ventas_pu_diario_min fvpu
where
	( l.codigo = fvpu.l_ubi_codigo 
and 	l.emp_codigo = fvpu.emp_codigo ) 
and
	( p.codigo = fvpu.l_pro_codigo 
and 	  p.emp_codigo = fvpu.emp_codigo )
and
	( fvpu.fecha_comercial = f.codigo )
and
	( fvpu.fecha_comercial between :FechaDesde and :FechaHasta)
and
	( :FechaHasta - :FechaDesde <= 31 )
and
	( nvl (lo.coord_x, lo.codigo) in :Local )
and 
	( l.loc_codigo = lo.codigo )
and 
-- 	( p.fam_codigo in :Familia )
	exists
		(select 1
		from iposs.articulos_empresas ae
		where ae.emp_codigo = $EMPRESA_MODELO$ 
		and ae.art_codigo = p.prod_cod_interno
		and ae.fam_codigo in :Familia)
and
	( p.tip_prod_codigo in (1, 3) )
and 
	( l.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  p.emp_codigo = $EMPRESA_LOGUEADA$ 
and 	  fvpu.emp_codigo = $EMPRESA_LOGUEADA$ 
abd 		lo.emp_codigo = $EMPRESA_LOGUEADA$ )
group by 
	l.loc_descripcion , 
	p.prod_descripcion , 
	p.fam_descripcion
order by 4 desc