multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
		select 				
			l.emp_codigo		"Cod_Empresa", 	
			l.loc_codigo		"Cod_Local_int", 	
			l.loc_cod_empresa		"Cod_Local", 	
			l.loc_razon_social		"Razon_social", 	
			l.loc_ruc		"RUT", 	
			l.loc_descripcion		"Local", 	
			l.depa_descripcion		"Departamento", 	
			l.loca_descripcion		"Localidad", 	
			l.fecha_prim_mov		"Fecha_Creacion", 	
			l.fecha_ult_mov		"Fecha_Ult_Mov", 	
			trunc(cc.fecha)		"Fecha", 	
			sum (decode (cc.tip_mov_cc, 3, -1, 4, -1, 7, 0, 1))		"Cantidad_Transas", 	
			sum (decode (cc.tip_mov_cc, 7, 1, 0))		"Cantidad_Consultas", 	
			sum (importe * decode (cc.tip_mov_cc, 3, -1, 4, -1, 7, 0, 1))		"Importe"	
		from 				
			dwm_prod.l_ubicaciones l, 			
			iposs.mt_movimientos_cc cc			
		where	cc.emp_codigo = l.emp_codigo			
		and 	cc.loc_codigo = l.loc_codigo		
		and 	cc.sello = '''||'BROUATM'||'''
		and 	cc.fecha 
			between to_date('''||:FechaDesde||''', '''||'dd/mm/yyyy'||''') 
			and to_date('''||:FechaHasta||''', '''||'dd/mm/yyyy'||''') + 1 - (1/24/60/60)	
		and 	to_date('''||:FechaDesde||''', '''||'dd/mm/yyyy'||''')  - to_date('''||:FechaHasta||''', '''||'dd/mm/yyyy'||''') <= 92			
		group by 				
			l.emp_codigo	, 		
			l.loc_codigo	, 		
			l.loc_cod_empresa	, 		
			l.loc_razon_social	, 		
			l.loc_ruc	, 		
			l.loc_descripcion	, 		
			l.depa_descripcion	, 		
			l.loca_descripcion	, 		
			l.fecha_prim_mov	, 		
			l.fecha_ult_mov	, 		
			trunc(cc.fecha)			
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
	select 				
			l.emp_codigo		"Cod_Empresa", 	
			l.loc_codigo		"Cod_Local_int", 	
			l.loc_cod_empresa		"Cod_Local", 	
			l.loc_razon_social		"Razon_social", 	
			l.loc_ruc		"RUT", 	
			l.loc_descripcion		"Local", 	
			l.depa_descripcion		"Departamento", 	
			l.loca_descripcion		"Localidad", 	
			l.fecha_prim_mov		"Fecha_Creacion", 	
			l.fecha_ult_mov		"Fecha_Ult_Mov", 	
			trunc(cc.fecha)		"Fecha", 	
			sum (decode (cc.tip_mov_cc, 3, -1, 4, -1, 7, 0, 1))		"Cantidad_Transas", 	
			sum (decode (cc.tip_mov_cc, 7, 1, 0))		"Cantidad_Consultas", 	
			sum (importe * decode (cc.tip_mov_cc, 3, -1, 4, -1, 7, 0, 1))		"Importe"	
		from 				
			dwm_prod.l_ubicaciones l, 			
			iposs.mt_movimientos_cc cc			
		where 1 = 2
		group by 
			l.emp_codigo	, 		
			l.loc_codigo	, 		
			l.loc_cod_empresa	, 		
			l.loc_razon_social	, 		
			l.loc_ruc	, 		
			l.loc_descripcion	, 		
			l.depa_descripcion	, 		
			l.loca_descripcion	, 		
			l.fecha_prim_mov	, 		
			l.fecha_ult_mov	, 		
			trunc(cc.fecha)			
)


######################
consulta
######################
select 1 from #reco#
