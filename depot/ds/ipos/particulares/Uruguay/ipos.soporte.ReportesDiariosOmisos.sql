/*
La información que se muestra es la siguiente:

 - Código de empresa
 - RUT
 - Razón social
 - Fecha (primer omiso)
 - Cantidad de reportes omisos para el cliente dado

*/                                                                
select 
	t1.codigo, 
	t1.rut, 
	t1.razon_social, 
	min (t1.fecha) as fecha_1er_omiso, 
	count(*) as cantidad_omisos
from 
	(select 
		si.emp_codigo as codigo, 
		s.numero_documento as rut, 
		r.descripcion as razon_social, 
		p.fecha
	from te_sujetos s 
	join te_sujetos_internos si 
		on s.codigo = si.suj_codigo,
	(select 
		fecha::date 
		from 
		generate_series(CURRENT_DATE - 360, CURRENT_DATE - 1, '1 day'::interval) fecha) p, 
		razones_sociales r
	where 	(si.envia_informes_diarios = true or 
              -- Clientes que tienen integradores y no envian informes  
			s.numero_documento in ('211223740010')) 
	and 	not exists 
				(select 1
				from te_envios_informes_diarios eid
				join te_informes_diarios ind 
					on eid.inf_dia_codigo = ind.men_codigo
				where 	ind.suj_codigo = s.codigo 
				and 	ind.fecha_emision = p.fecha 
				and 	eid.estado in (1, 5)) -- Estado 5: EXISTE EN DGI
				and 	p.fecha >= si.fecha_inicio
				and 	s.numero_documento = r.ruc) AS t1
group by 
	t1.codigo, 
	t1.rut, 
	t1.razon_social	
order by 
	t1.codigo, 
	t1.rut, 
	t1.razon_social
