multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select
		'select 
			l.emp_codigo 		"Cod.Empresa", 
			l.codigo 			"Cod.Local", 
			l.loc_cod_empresa 	"Cod.Sucursal", 
			m.caj_codigo 		"Cod.Caja", 
			raz.descripcion 	"Razón Social", 
			l.descripcion 		"Local", 
			dist.config 		"Config", 
			count(*) 			"Cant.Movimientos"
		from 
			iposs.movimientos m, 
			iposs.locales l, 
			iposs.departamentos depa, 
			iposs.localidades loca, 
			iposs.distribuciones_pendientes dist, 
			iposs.razones_sociales raz 
		where 	m.loc_codigo = l.codigo 
		and 	m.emp_codigo = l.emp_codigo 
		and 	depa.codigo = l.depa_codigo
		and 	loca.codigo = l.loca_codigo
		and 	dist.LOC_CODIGO = m.loc_codigo 
		and 	dist.CAJ_CODIGO = m.caj_codigo 
		and 	l.RAZ_SOC_CODIGO = raz.CODIGO  
		and 	m.tipo_operacion = '''||'VENTA'||'''
		and 	dist.CONFIG like '''||'%'||:Config||'%'||'''
		and 	m.fecha_comercial >= trunc(sysdate) - 8 
		and 	m.fecha_comercial <= trunc(sysdate) - 1
		group by  
			l.emp_codigo, 
			l.codigo, 
			l.LOC_COD_EMPRESA, 
			m.caj_codigo, 
			raz.DESCRIPCION, 
			l.DESCRIPCION, 
			dist.CONFIG
		order by 8 desc
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
	select 
			l.emp_codigo 		"Cod.Empresa", 
			l.codigo 			"Cod.Local", 
			l.loc_cod_empresa 	"Cod.Sucursal", 
			m.caj_codigo 		"Cod.Caja", 
			raz.descripcion 	"Razón Social", 
			l.descripcion 		"Local", 
			dist.config 		"Config", 
			count(*) 			"Cant.Movimientos"
		from 
			iposs.movimientos m, 
			iposs.locales l, 
			iposs.departamentos depa, 
			iposs.localidades loca, 
			iposs.distribuciones_pendientes dist, 
			iposs.razones_sociales raz 
		where 	1 = 2
		group by  
			l.emp_codigo, 
			l.codigo, 
			l.loc_cod_empresa, 
			m.caj_codigo, 
			raz.descripcion, 
			l.descripcion, 
			dist.config
)


######################
consulta
######################
select 1 from #reco#


/*

Consulta original (estaba basada en el DW y era muy lenta):


select l.emp_codigo,l.loc_codigo,loc.LOC_COD_EMPRESA , l.caj_codigo,raz.DESCRIPCION, loc.DESCRIPCION, dist.CONFIG ,sum (mov_cantidad)
from dwm_prod.l_cajas l, dwm_prod.f_ventas_diario_min f, locales loc, DEPARTAMENTOS dep, LOCALIDADES loca, DISTRIBUCIONES_PENDIENTES dist, RAZONES_SOCIALES raz
where f.l_caj_codigo = l.codigo and loc.CODIGO =  l.loc_codigo  and loca.DEPA_CODIGO = dep.CODIGO and loc.LOCA_CODIGO = loca.CODIGO
and dist.LOC_CODIGO = l.loc_codigo and dist.CAJ_CODIGO =  l.caj_codigo and loc.RAZ_SOC_CODIGO = raz.CODIGO  and dist.CONFIG like XXXXXXXX
and f.fecha_comercial > XXXXXXXX
group by  l.emp_codigo,l.loc_codigo,loc.LOC_COD_EMPRESA , l.caj_codigo,raz.DESCRIPCION, loc.DESCRIPCION,dist.CONFIG
order by 8 desc;

*/