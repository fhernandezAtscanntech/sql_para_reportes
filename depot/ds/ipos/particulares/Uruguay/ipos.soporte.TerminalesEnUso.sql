select 
	cre.descripcion 		"Credito", 
	cre_conf_term.rango_terminal_ini 	"Rango Term. Ini.", 
	cre_conf_term.rango_terminal_fin 	"Rango Term. Fin", 
	cre_conf_term.ult_terminal_asig 	"�ltima Asignada"
from 
	creditos_conf_terminales cre_conf_term, 
	creditos cre
where 
	cre_conf_term.cre_codigo = cre.codigo
	