SELECT 
	al.art_codigo_externo 	"Cod.Artículo",
	ae.descripcion 			"Descripción Larga",
	ae.descripcion_corta 	"Descripción Corta",
	rub.descripcion 		"Rubro",
	rub.rub_cod_empresa 	"Cod.Rubro",
	pv.precio 				"Precio Venta",
	m.simbolo 				"Moneda",
	aee.codigo_barra_img 	"Cod.Barra img.",
	decode (
		nvl(aee.visible_app_pedidos, 0), 
		1, 'Visible' , 
		2, 'Apagado Temporalmente' , 
		0, 'Apagado' , 
		' Estado Desconocido' ) 	"Visible app. pedidos", 
	rubext.imagen 			"Imagen",
	decode (
		ae.venta_fraccionada, 
		0, 'Venta Entera', 
		1, 'Venta Fraccionada', 
		'Estado desconocido') "Venta Fraccionada",
	decode (
		ae.bonificable, 
		0, 'No bonificable' , 
		1, 'Bonificable', 
		'Estado desconocido') 	"Bonificable"
FROM 
	iposs.articulos_locales al,
	iposs.articulos_empresas ae,
	iposs.articulos_empresas_ext aee,
	iposs.rubros rub,
	iposs.rubros_ext rubext,
	iposs.precios_de_articulos_ventas pv ,
	iposs.monedas m
WHERE 	al.art_codigo				= ae.art_codigo
AND 	al.emp_codigo				= ae.emp_codigo
AND 	ae.art_codigo				= aee.art_codigo(+)
AND 	ae.emp_codigo				= aee.emp_codigo(+)
AND 	rub.codigo					= rubext.codigo(+)
AND 	rub.codigo 					= ae.rub_codigo
AND 	pv.art_codigo 				= ae.art_codigo
AND 	m.codigo 					= pv.mon_codigo
AND 	al.fecha_borrado			is null
AND 	ae.fecha_borrado			is null
AND 	al.emp_codigo 				= 6974
AND 	al.loc_codigo 				= 14795
AND 	rub.emp_codigo 				= 6974
AND 	pv.emp_codigo 				= 6974
AND 	pv.loc_lis_loc_codigo 		= 14795
AND 	pv.loc_lis_lis_pre_ve_codigo = 30153
AND 	pv.vigencia 				<= sysdate
AND 	pv.vigencia_hasta 			> sysdate
AND 	pv.dim_con_numero 			= 0
AND 	pv.precio 					> 0
AND 	aee.visible_app_pedidos 	in (1,2)
AND 	((:Bonificable = 'Bonificable' and ae.bonificable = 1)
		or
		(:Bonificable = 'No Bonificable' and ae.bonificable = 0)
		or
		(:Bonificable is null))
AND 	((:Visible = 'Visible' and aee.visible_app_pedidos = 1)
		or
		(:Visible = 'Apagado Temporalmente' and aee.visible_app_pedidos = 2)
		or
		(:Visible is null))
AND 	((:VtaFrac = 'Venta Entera' and ae.venta_fraccionada = 0)
		or
		(:VtaFrac = 'Venta Fraccionada' and ae.venta_fraccionada = 1)
		or 
		(:VtaFrac is null))
AND 	ae.emp_codigo = $EMPRESA_LOGUEADA$
AND 	al.emp_codigo = $EMPRESA_LOGUEADA$
AND 	aee.emp_codigo = $EMPRESA_LOGUEADA$
AND 	pv.emp_codigo = $EMPRESA_LOGUEADA$