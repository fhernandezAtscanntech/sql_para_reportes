select
	Local 				"Local", 
 	loc_codigo 			"Cod.Local", 
	emp_codigo 			"Emp.Codigo", 
	Prod_Cod_Interno  "Cod Scanntech",
	prod_codigo 			"Codigo",
	Prod_Rubro        "Rubro",
	prod_descripcion 		"Articulo", 
	codigo_barra		"Cod.Barra", 
	case  Prod_Imagen
		when 1 then 'Con Imagen'
-- 		when 0 then 'Sin Imagen' 
		else ''
	end 	"Imagen",
	Unidades 			"Unidades", 
	Venta_Total 			"Venta Total", 
	Devol 				"Devol", 
	Bonif 				"Bonif", 
	Prom 				"Prom", 
	Utilidad 			"Utilidad", 
	Costo_Ultimo 			"Costo Ultimo", 
	IVA 				"IVA", 
	Venta 				"Venta", 
	ranking_pesos 			"Ranking Pesos", 
	ranking_unidades 		"Ranking Unidades"
from
(
	select
		l.loc_modelo_descripcion 	as Local, 
		l.loc_codigo 			as Loc_Codigo, 
		l.emp_codigo 			as Emp_Codigo,
		p.prod_cod_interno		as Prod_Cod_Interno,
		p.prod_codigo 			as Prod_Codigo, 
		p.rub_descripcion   as Prod_Rubro,
		p.prod_descripcion 		as Prod_Descripcion, 
		(select cb.codigo_barra
			from iposs.codigos_barras cb
			where cb.art_codigo = p.prod_cod_interno
			and cb.emp_codigo in (l.emp_codigo, 1)
			and rownum = 1) 		as Codigo_Barra, 
    ( select 1
      from dual
      where exists (
          select ai.codigo_barra
          from articulos_imagenes ai
          where ai.codigo_barra in (
            select cb.codigo_barra
            from codigos_barras cb join articulos_empresas ae on
              cb.art_codigo = ae.art_codigo
            where
              ae.emp_codigo = l.emp_codigo
              and ae.art_codigo = p.prod_cod_interno
              and (
                (ae.veo_barras_modelo = 1 and cb.emp_codigo in (1,l.emp_codigo))
                or
                (ae.veo_barras_modelo = 0 and cb.emp_codigo = l.emp_codigo)
              ) 
          )
        )
    ) as Prod_Imagen,
		sum (nvl (fvpu.ven_cantidad, 0)) - sum (nvl (fvpu.dev_cantidad, 0)) as Unidades, 
		sum (nvl (fvpu.ven_nac_importe, 0)) as Venta_Total, 
		sum (nvl (fvpu.dev_nac_importe, 0)) as Devol, 
		sum (nvl (fvpu.bon_nac_importe, 0)) as Bonif, 
		sum (nvl (fvpu.des_prom_nac_importe, 0)) as Prom, 
		sum (
			nvl (fvpu.ven_nac_importe, 0) - 
			nvl (fvpu.dev_nac_importe, 0)  -
			nvl (fvpu.cos_nac_ult_impuestos, 0)) as Utilidad, 
		sum (nvl (fvpu.cos_nac_ult_impuestos, 0)) as Costo_Ultimo, 
		sum (nvl (fvpu.iva_importe, 0)) as IVA, 
		sum (
			nvl (fVpu.ven_nac_importe, 0) - 
			nvl (fvpu.dev_nac_importe, 0) ) as Venta, 
		rank () over 
			(order by sum (
				nvl (fVpu.ven_nac_importe, 0) - 
				nvl (fvpu.dev_nac_importe, 0) ) desc)	as ranking_pesos, 
		rank () over 
			(order by sum (
				nvl (fVpu.ven_cantidad, 0) - 
				nvl (fvpu.dev_cantidad, 0)) desc)		as ranking_unidades
	from
		dwm_prod.l_ubicaciones l, 
		dwm_prod.l_productos p, 
		dwm_prod.f_ventas_pu_mensual_min fvpu
	where
		( l.codigo = fvpu.l_ubi_codigo 
	and 	l.emp_codigo = fvpu.emp_codigo ) 
	and
		( p.codigo = fvpu.l_pro_codigo 
	and 	  p.emp_codigo = fvpu.emp_codigo )
	and
		( fvpu.mes_codigo in :Meses )
	and
		( l.loc_codigo in :Local )
	and 
		( p.tip_prod_codigo = 1 )
	and 
		( trim (upper (':Rubro')) = 'NULL' or p.rub_codigo in :Rubro )
	and 	( l.emp_codigo = $EMPRESA_LOGUEADA$
	and 	  p.emp_codigo = $EMPRESA_LOGUEADA$
	and 	  fvpu.emp_codigo = $EMPRESA_LOGUEADA$)
	group by 
		l.loc_modelo_descripcion , 
 		l.loc_codigo 		, 
		l.emp_codigo 		, 
    p.prod_cod_interno  ,
		p.prod_codigo 		, 
		p.prod_descripcion 	, 
		p.rub_descripcion)
where
	ranking_unidades <= nvl (:Ranking, 0)