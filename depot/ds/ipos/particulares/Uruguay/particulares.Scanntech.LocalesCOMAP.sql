multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select
	'select 
		l.emp_codigo	"Cod.Empresa", 
		nvl (lo.coord_x, lo.codigo)	"Cod.Local", 
		l.emp_descripcion	"Empresa", 
		l.loc_modelo_descripcion	"Local", 
		l.loc_razon_social 		"Razon Social", 
		l.loc_RUC 			"RUT", 
		l.depa_descripcion 		"Departamento", 
		l.loca_descripcion 		"Localidad", 
		lo.telefono 			"Telf.", 
		sum (nvl ((select sum (nvl (f.mov_cantidad, 0)) 
			from dwm_prod.f_ventas_diario_min f 
			where f.emp_codigo = l.emp_codigo 
			and f.l_ubi_codigo = l.codigo 
			and f.fecha_comercial between trunc(sysdate) - 15 and trunc(sysdate) - 1 
			having sum (nvl (f.mov_cantidad, 0)) > 2), 0))	"Cant.Tickets", 
		max (d.fecha_ult_consulta) 	"Fecha Ult. Consulta" 
	from 
		dwm_prod.l_ubicaciones l, 
		iposs.locales lo , 
		iposs.distribuciones_pendientes d
	where	l.loc_codigo = lo.codigo 
	and 	l.emp_codigo between 18000 and 25000 
	and 	lo.fecha_borrado is null
	and 	lo.emp_codigo = d.emp_codigo
	and 	nvl (lo.coord_x, lo.codigo) = d.loc_codigo
	group by 
		l.emp_codigo, 
		nvl (lo.coord_x, lo.codigo), 
		l.emp_descripcion, 
		l.loc_modelo_descripcion, 
		l.loc_razon_social, 
		l.loc_RUC, 
		l.depa_descripcion, 
		l.loca_descripcion, 
		lo.telefono
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
select 
	l.emp_codigo	"Cod.Empresa", 
	nvl (lo.coord_x, lo.codigo)	"Cod.Local", 
	l.emp_descripcion	"Empresa", 
	l.loc_modelo_descripcion	"Local", 
	l.loc_razon_social 		"Razon Social", 
	l.loc_RUC 				"RUT", 
	l.depa_descripcion 		"Departamento", 
	l.loca_descripcion 		"Localidad", 
	lo.telefono 			"Telef.", 
	sum (nvl ((select sum (nvl (f.mov_cantidad, 0)) 
		from dwm_prod.f_ventas_diario_min f 
		where f.emp_codigo = l.emp_codigo 
		and f.l_ubi_codigo = l.codigo 
		and f.fecha_comercial between trunc(sysdate) - 15 and trunc(sysdate) - 1 
		having sum (nvl (f.mov_cantidad, 0)) > 2), 0))	"Cant.Tickets", 
	max (d.fecha_ult_consulta) 	"Fecha Ult. Consulta"
from 
	dwm_prod.l_ubicaciones l, 
	iposs.locales lo , 
	iposs.distribuciones_pendientes d
where	1 = 2
	group by 
		l.emp_codigo, 
		nvl (lo.coord_x, lo.codigo), 
		l.emp_descripcion, 
		l.loc_modelo_descripcion, 
		l.loc_razon_social, 
		l.loc_RUC, 
		l.depa_descripcion, 
		l.loca_descripcion, 
		lo.telefono
)

######################
consulta
######################
select 1 from #reco#
