** código artículo, 
** artículo, 
** tipo promocion, 
cantidad en promocion, 
precio promo, 
** stock, 
** fecha ultima compra, 
** cantidad ultima compra, 
** cantidad vendida, 
** promo desde, 
** promo hasta, 
** cod.proveedor, 
** proveedor

-- hay que sacar los datos del DW en la medida que sea posible


select 
	al.art_codigo_externo 		"Cod.Artículo",  
	ae.descripcion 				"Artículo", 
-- 	dw.prom_descripcion 		"Promoción", 
-- 	dw.tip_prom_descripcion 	"Tipo Promoción", 
	pr.descripcion 				"Promoción", 
	tp.descripcion 				"Tipo Promoción", 
	pa.razon_social 			"Proveedor", 
	pa.codigo_art_proveedor 	"Cod.Art.Proveedor", 
	s.stock 					"Stock", 
	cd.comp_fecha_emision 	"Fecha Ult.Compra", 
	cd.cantidad_recibida 	"Cantidad compra", 
	md.cantidad 			"Unidades Vendidas", 
	pv.vigencia_desde 		"Desde", 
	pv.vigencia_hasta 		"Hasta"
from 
	iposs.articulos_empresas ae 
	join iposs.articulos_locales al 
		on ae.art_codigo = al.art_codigo and ae.emp_codigo = al.emp_codigo and al.fecha_borrado is null
	join iposs.prom_datos pd 
		on pd.art_codigo_hijo = al.art_codigo 
		and pd.emp_codigo = al.emp_codigo
	join iposs.promociones pr
		on pd.prom_codigo = pr.codigo
	join iposs.tipos_de_promociones tp
		on pr.tip_pro_codigo = tp.codigo
	join iposs.prom_locales_habilitados pl 
		on pl.prom_codigo = pd.prom_codigo
		and pl.loc_codigo = al.loc_codigo 
		and pl.emp_codigo = al.emp_codigo 
		and pl.estado in (2, 4) -- aprobadas y finalizadas
	join iposs.prom_vigencias pv
		on pv.prom_codigo = pd.prom_codigo
		and pv.emp_codigo = pd.emp_codigo
		and pv.vigencia_hasta > sysdate
		/*and pv.vigencia_desde < :FechaHasta
		and pv.vigencia_hasta > :FechaDesde  */ -- solo promociones vigentes al momento de sacar el reporte (pedido del usuario)
	left join (select p.prod_cod_interno as art_codigo, p.emp_codigo, l.loc_codigo, pr.prom_codigo, sum (nvl (ven_cantidad, 0)) - sum (nvl(dev_cantidad, 0)) as cantidad
			from dwm_prod.l_productos p, dwm_prod.l_promociones pr, dwm_prod.l_ubicaciones l, dwm_prod.f_ventas_pu_diario_min f
			where f.l_prom_codigo = pr.codigo
			and f.l_pro_codigo = p.codigo
			and f.emp_codigo = p.emp_codigo
			and f.l_ubi_codigo = l.codigo
			and f.emp_codigo = $EMPRESA_LOGUEADA$
			and f.fecha_comercial between :FechaDesde and :FechaHasta
			and f.l_prom_codigo <> 0
			group by p.prod_cod_interno, p.emp_codigo, l.loc_codigo, pr.prom_codigo) dw
		on dw.art_codigo = al.art_codigo 
		and dw.emp_codigo = al.emp_codigo
		and dw.prom_codigo = pd.prom_codigo
		and dw.loc_codigo = al.loc_codigo
	left join (select p.razon_social, pa.codigo_art_proveedor, pa.art_codigo, pa.prov_codigo, pa.emp_codigo
			from iposs.proveedores p 
				join iposs.proveedores_articulos pa 
				on p.codigo = pa.prov_codigo 
			where pa.emp_codigo = $EMPRESA_LOGUEADA$
			and pa.fecha_borrado is null
			and pa.proveedor_principal = 1) pa 		-- (no) importa si es el proveedor principal
		on pa.art_codigo = al.art_codigo
		and pa.emp_codigo = al.emp_codigo
	left join iposs.stocks s
		on s.art_codigo = al.art_codigo
		and s.dep_loc_codigo = al.loc_codigo
		and s.emp_codigo = al.emp_codigo
		and sysdate between nvl (s.fecha, sysdate) and nvl (s.vigencia_hasta, sysdate) -- limito al stock vigente
	left join (select loc_codigo, art_codigo, comp_prov_codigo, comp_fecha_emision, sum (cd.cantidad_recibida) as cantidad_recibida
			from iposs.comprobantes_detalles cd
			where cd.comp_emp_codigo = $EMPRESA_LOGUEADA$
			and cd.comp_tip_doc_in_codigo in (1, 4) -- solo compras
			and cd.comp_fecha_emision > sysdate - 90
			and cd.comp_fecha_emision = 
				(select max(cd2.comp_fecha_emision)
				from iposs.comprobantes_detalles cd2
				where cd2.comp_emp_codigo = $EMPRESA_LOGUEADA$
				and cd2.comp_tip_doc_in_codigo in (1, 4)
				and cd2.comp_fecha_emision > sysdate - 90
				and cd2.art_codigo = cd.art_codigo
				and cd2.loc_codigo = cd.loc_codigo)
			group by loc_codigo, art_codigo, comp_prov_codigo, comp_fecha_emision) cd
		on cd.art_codigo = al.art_codigo
		and cd.loc_codigo = al.loc_codigo
/* DOC-- 
tengo que agrupar por si hay más de una compra en la fecha en que se compró por última vez
*/ 
	left join (select md.loc_codigo, md.art_codigo, 
			substr(md.txt_descuentos, instr(md.txt_descuentos, 'DescXLineaCombo') + 16, length(md.txt_descuentos)) as prom_codigo, 
			sum (decode (md.tip_det_codigo, 4, 1, -1) * md.cantidad) as cantidad
			from iposs.movimientos m
				join iposs.movimientos_detalles md
				on m.numero_mov = md.mov_numero_mov
				and m.emp_codigo = md.mov_emp_codigo
				and m.fecha_comercial = md.fecha_comercial
			where m.tipo_operacion = 'VENTA'
			and m.fecha_comercial between :FechaDesde and :FechaHasta
			and m.emp_codigo = $EMPRESA_LOGUEADA$
			and md.tip_det_codigo in (4, 5)
			and md.txt_descuentos like '%DescXLineaCombo%'
			group by md.loc_codigo, md.art_codigo, substr(md.txt_descuentos, instr(md.txt_descuentos, 'DescXLineaCombo') + 16, length(md.txt_descuentos))) md 
		on md.loc_codigo = al.loc_codigo
		and md.art_codigo = al.art_codigo_Externo
		and md.prom_codigo = pr.prom_cod_empresa
where 
/* DOC --
si tiene compras en los últimos 90 días, muestra solo ese proveedor
si no tiene proveedor asociado, o no tiene compras, muestra todo lo que tenga
*/
		(nvl (cd.comp_prov_codigo, pa.prov_codigo) = nvl (pa.prov_codigo, cd.comp_prov_codigo) 
		or
		(cd.comp_prov_codigo is null and pa.prov_codigo is null)
		)
and 	
		ae.tipo <> 3 		-- no muestro los combos
and 
		al.loc_codigo in :Local
and 	:FechaHasta - :FechaDesde <=15 		
and 
		ae.emp_codigo = $EMPRESA_LOGUEADA$
and 	al.emp_codigo = $EMPRESA_LOGUEADA$
