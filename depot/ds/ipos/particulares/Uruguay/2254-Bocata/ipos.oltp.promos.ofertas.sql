select 
	al.art_codigo_externo 		"Cod.Artículo",  
	ae.descripcion 				"Artículo", 
	o.inicio 					"Oferta Desde", 
	o.fin 						"Oferta Hasta", 
	o.precio 					"Precio Oferta", 
	p.precio 					"Precio Regular", 
	pa.razon_social 			"Proveedor", 
	pa.codigo_art_proveedor 	"Cod.Art.Proveedor", 
	s.stock 					"Stock", 
	cd.comp_fecha_emision 	"Fecha Ult.Compra", 
	cd.cantidad_recibida 	"Cantidad compra", 
	md.cantidad 			"Unidades Vendidas"
from 
	iposs.articulos_empresas ae 
	join iposs.articulos_locales al 
		on ae.art_codigo = al.art_codigo and ae.emp_codigo = al.emp_codigo and al.fecha_borrado is null
	join iposs.precios_de_articulos_ventas p
		on p.art_codigo = al.art_codigo
		and p.loc_lis_loc_codigo = al.loc_codigo
		and p.loc_lis_lis_pre_ve_codigo = to_number(iposs.f_param('LIS_PRE_VE_DEF', al.loc_codigo))
	join iposs.ofertas o
		on o.art_codigo = al.art_codigo
		and o.loc_codigo = al.loc_codigo
	left join (select p.razon_social, pa.codigo_art_proveedor, pa.art_codigo, pa.prov_codigo, pa.emp_codigo
			from iposs.proveedores p 
				join iposs.proveedores_articulos pa 
				on p.codigo = pa.prov_codigo 
			where pa.emp_codigo = $EMPRESA_LOGUEADA$
			and pa.fecha_borrado is null
			and pa.proveedor_principal = 1) pa 		-- (no) importa si es el proveedor principal
		on pa.art_codigo = ae.art_codigo
		and pa.emp_codigo = ae.emp_codigo
	left join iposs.stocks s
		on s.art_codigo = al.art_codigo
		and s.dep_loc_codigo = al.loc_codigo
		and s.emp_codigo = al.emp_codigo
		and sysdate between nvl (s.fecha, sysdate) and nvl (s.vigencia_hasta, sysdate) -- limito al stock vigente
	left join (select loc_codigo, art_codigo, comp_prov_codigo, comp_fecha_emision, sum (cd.cantidad_recibida) as cantidad_recibida
			from iposs.comprobantes_detalles cd
			where cd.comp_emp_codigo = $EMPRESA_LOGUEADA$
			and cd.comp_tip_doc_in_codigo in (1, 4) -- solo compras
			and cd.comp_fecha_emision > sysdate - 90
			and cd.comp_fecha_emision = 
				(select max(cd2.comp_fecha_emision)
				from iposs.comprobantes_detalles cd2
				where cd2.comp_emp_codigo = $EMPRESA_LOGUEADA$
				and cd2.comp_tip_doc_in_codigo in (1, 4)
				and cd2.comp_fecha_emision > sysdate - 90
				and cd2.art_codigo = cd.art_codigo
				and cd2.loc_codigo = cd.loc_codigo)
			group by loc_codigo, art_codigo, comp_prov_codigo, comp_fecha_emision) cd
		on cd.art_codigo = al.art_codigo
		and cd.loc_codigo = al.loc_codigo
	left join (select md.loc_codigo, md.art_codigo, sum (decode (md.tip_det_codigo, 4, 1, -1) * md.cantidad) as cantidad
			from iposs.movimientos m
				join iposs.movimientos_detalles md
				on m.numero_mov = md.mov_numero_mov
				and m.emp_codigo = md.mov_emp_codigo
				and m.fecha_comercial = md.fecha_comercial
			where m.tipo_operacion = 'VENTA'
			and m.fecha_comercial between :FechaDesde and :FechaHasta
			and m.emp_codigo = $EMPRESA_LOGUEADA$
			and md.tip_det_codigo in (4, 5)
			and md.txt_descuentos like '%DescXLineaOferta%'
			group by md.loc_codigo, md.art_codigo) md 
		on md.loc_codigo = al.loc_codigo
		and md.art_codigo = al.art_codigo_Externo
/* DOC-- 
tengo que agrupar por si hay más de una compra en la fecha en que se compró por última vez
*/ 
where 
/* DOC --
si tiene compras en los últimos 90 días, muestra solo ese proveedor
si no tiene proveedor asociado, o no tiene compras, muestra todo lo que tenga
*/
		(nvl (cd.comp_prov_codigo, pa.prov_codigo) = nvl (pa.prov_codigo, cd.comp_prov_codigo) 
		or
		(cd.comp_prov_codigo is null and pa.prov_codigo is null)
		)
and 	
		al.loc_codigo in :Local
and 	
/*		o.inicio < :FechaHasta + 1 - (1/24/60/60)
and 	o.fin > :FechaDesde
*/
		o.inicio < sysdate
and 	o.fin > sysdate
/* DOC --
todas las ofertas que empiezan antes de la fecha Hasta, Y terminan después de la fecha Desde
Esto incluye todas las promociones que estuvieron vigentes algún día del período indicado
*/
and 	:FechaHasta + 1 - (1/24/60/60) between p.vigencia and p.vigencia_hasta
/* DOC --
precios vigentes a la finalización del período. Si uso la fecha de inicio, hay artículos que aún no tienen precio de venta y no aparecen
*/
and 	:FechaHasta - :FechaDesde <=15 		
and 
		ae.emp_codigo = $EMPRESA_LOGUEADA$
and 	al.emp_codigo = $EMPRESA_LOGUEADA$
and 	p.emp_codigo  = $EMPRESA_LOGUEADA$
and 	o.emp_codigo  = $EMPRESA_LOGUEADA$
