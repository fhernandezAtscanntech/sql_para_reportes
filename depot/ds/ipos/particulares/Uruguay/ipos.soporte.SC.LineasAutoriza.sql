select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select
		'select 
			mov.EMP_CODIGO 	as "Cod.Empresa",  
			loc.DESCRIPCION 	as "Local", 
			raz.DESCRIPCION 	as "Razón Social", 
			mov.LOC_CODIGO 	as "Cod.Local", 
			mov.CAJ_CODIGO 	as "Caja", 
			count (*) as "Lineas", 
			SUM (CASE WHEN 
				det.FUN_CODIGO_AUTORIZA is null  
					THEN 0 
					ELSE 1 
				END) as "Con_Autorizacion", 
			ROUND (SUM (CASE WHEN 
				det.FUN_CODIGO_AUTORIZA is null  
					THEN 0 
					ELSE 1 
				END) / count (*), 3) as "indice"
		from 
			MOVIMIENTOS_DETALLES det 
		join MOVIMIENTOS mov 
			on mov.FECHA_COMERCIAL = det.FECHA_COMERCIAL 
			and mov.NUMERO_MOV = det.MOV_NUMERO_MOV
			and mov.emp_codigo = det.mov_emp_codigo
		join locales loc 
			on mov.loc_codigo = loc.CODIGO
		join RAZONES_SOCIALES raz 
			on loc.RAZ_SOC_CODIGO = raz.CODIGO
		join DISTRIBUCIONES_PENDIENTES dist 
			on dist.LOC_CODIGO = mov.LOC_CODIGO 
			and dist.CAJ_CODIGO = mov.CAJ_CODIGO 
			and dist.FECHA_ULT_CONSULTA > sysdate - 7
		where 	det.FECHA_COMERCIAL = to_date('''||:Fecha||''', '''||'dd/mm/yyyy'||''')
		and 	dist.CONFIG like '''||'producto=SC%'||'''
		group by 
			mov.EMP_CODIGO, 
			loc.DESCRIPCION, 
			raz.DESCRIPCION, 
			mov.LOC_CODIGO, 
			mov.CAJ_CODIGO
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
	select 
		mov.EMP_CODIGO 	as "Cod.Empresa",  
		loc.DESCRIPCION 	as "Local", 
		raz.DESCRIPCION 	as "Razón Social", 
		mov.LOC_CODIGO 	as "Cod.Local", 
		mov.CAJ_CODIGO 	as "Caja", 
		count (*) as "Lineas", 
		SUM (CASE WHEN 
			det.FUN_CODIGO_AUTORIZA is null  
				THEN 0 
				ELSE 1 
			END) as "Con_Autorizacion", 
		ROUND (SUM (CASE WHEN 
			det.FUN_CODIGO_AUTORIZA is null  
				THEN 0 
				ELSE 1 
			END) / count (*), 3) as "indice"
	from 
		MOVIMIENTOS_DETALLES det 
	join MOVIMIENTOS mov 
		on mov.FECHA_COMERCIAL = det.FECHA_COMERCIAL 
		and mov.NUMERO_MOV = det.MOV_NUMERO_MOV
		and mov.emp_codigo = det.mov_emp_codigo
	join locales loc 
		on mov.loc_codigo = loc.CODIGO
	join RAZONES_SOCIALES raz 
		on loc.RAZ_SOC_CODIGO = raz.CODIGO
	join DISTRIBUCIONES_PENDIENTES dist 
		on dist.LOC_CODIGO = mov.LOC_CODIGO 
		and dist.CAJ_CODIGO = mov.CAJ_CODIGO 
		and dist.FECHA_ULT_CONSULTA > sysdate - 7
	where 1 = 2
	group by 
		mov.EMP_CODIGO, 
		loc.DESCRIPCION, 
		raz.DESCRIPCION, 
		mov.LOC_CODIGO, 
		mov.CAJ_CODIGO)


######################
consulta
######################
select 1 from #reco#

/*

Consulta original 
select mov.EMP_CODIGO,loc.DESCRIPCION,raz.DESCRIPCION, mov.LOC_CODIGO,mov.CAJ_CODIGO,  count (*) as "Lineas",SUM(CASE WHEN det.FUN_CODIGO_AUTORIZA is null  THEN 0 ELSE 1 END) as "Con_Autorizacion",ROUND(SUM(CASE WHEN det.FUN_CODIGO_AUTORIZA is null  THEN 0 ELSE 1 END)/ count (*),3) as "indice"
from MOVIMIENTOS_DETALLES det join MOVIMIENTOS mov on mov.FECHA_COMERCIAL = det.FECHA_COMERCIAL and mov.NUMERO_MOV = det.MOV_NUMERO_MOV
join locales loc on mov.loc_codigo = loc.CODIGO
join RAZONES_SOCIALES raz on loc.RAZ_SOC_CODIGO = raz.CODIGO
join DISTRIBUCIONES_PENDIENTES dist on dist.LOC_CODIGO = mov.LOC_CODIGO and dist.CAJ_CODIGO = mov.CAJ_CODIGO and dist.FECHA_ULT_CONSULTA > sysdate-7
where  det.FECHA_COMERCIAL = trunc(sysdate) and  dist.CONFIG like 'producto=SC%' 
group by mov.EMP_CODIGO,loc.DESCRIPCION,raz.DESCRIPCION, mov.LOC_CODIGO,mov.CAJ_CODIGO
order by 1,2,3;

*/
