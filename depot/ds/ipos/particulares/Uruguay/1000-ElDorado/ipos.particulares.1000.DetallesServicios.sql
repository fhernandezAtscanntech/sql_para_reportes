select 
	'ANTEL' 		"Comp", 
	mov.numero_mov_cc 	"Cod", 
	mov.cue_cor_codigo 	"Cod. CC", 
	cob.Importe 	"Importe", 
	trunc (mov.fecha) 	"Fecha"
from 
	ANTEL_COBRANZAS cob 
	join RC_MOVIMIENTOS_CC mov 
		on mov.CON_MOV_CODIGO = cob.CON_MOV_CODIGO
where 	mov.fecha > :FechaDesde 
and  	mov.fecha < :FechaHasta + (1 - (1/24/60/60))
and 	cob.estatus IN (1, 3)
and 	mov.emp_codigo = :Empresa
UNION
select 
	'OSE' 		"Comp", 
	mov.numero_mov_cc 	"Cod", 
	mov.cue_cor_codigo 	"Cod. CC", 
	cob.Importe 	"Importe", 
	trunc (mov.fecha) 	"Fecha"
from 
	OSE_COBRANZAS cob 
	join RC_MOVIMIENTOS_CC mov 
		on mov.CON_MOV_CODIGO = cob.CON_MOV_CODIGO
where 	mov.fecha > :FechaDesde 
and  	mov.fecha < :FechaHasta + (1 - (1/24/60/60))
and 	cob.estado_cobranza  IN (1, 3, 6)
and 	mov.emp_codigo = :Empresa
UNION
select 
	'UTE' 		"Comp", 
	mov.numero_mov_cc 	"Cod", 
	mov.cue_cor_codigo 	"Cod. CC", 
	cob.Importe_total 	"Importe", 
	trunc (mov.fecha) 	"Fecha"
from
	ute_pagos_factura cob
	join rc_movimientos_cc mov
		on mov.con_mov_codigo = cob.con_mov_caj_codigo
where	mov.compañia = 'UTE'
and 	cob.id_confirmacion is not null
and 	mov.fecha > :FechaDesde 
and  	mov.fecha < :FechaHasta + (1 - (1/24/60/60))
and 	mov.emp_codigo = :Empresa
