/* previo:
	create table iposs.aux_credconfcajas_polakof (
		emp_codigo number(5) not null, 
		codigo number(5) not null, 
		raz_soc_descripcion	varchar2(30), 
		ruc	varchar2(20), 
		loc_descripcion	varchar2(30),  
		departamento varchar2(30), 
		localidad varchar2(30), 
		direccion varchar2(40), 
		CRE_CODIGO number(3) not null, 
		CAJ_CODIGO number(5) not null, 
		COMERCIO   varchar2(80) not null, 
		TERMINAL   varchar2(80) not null
	)
	/
	
	alter table iposs.aux_credconfcajas_polakof add constraint aux_ccc_pol_pk primary key 
		(emp_codigo, 
		codigo, 
		cre_codigo, 
		caj_codigo, 
		comercio, 
		terminal)
	/
	
	grant select on iposs.aux_credconfcajas_polakof to iposs_rep, iposs_fx;
	
*/
multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
		select distinct		
			l.emp_codigo	as Cod_Empresa, 
			l.codigo	as Cod_Local, 
			r.descripcion	as Razon_Social, 
			r.ruc	as RUT, 
			l.descripcion	as Local, 
			(select d.descripcion from iposs.departamentos d where d.codigo = l.depa_codigo)	as Departamento, 
			(select lo.descripcion from iposs.localidades lo where lo.codigo = l.loca_codigo)	as Localidad, 
			l.direccion	as Direccion, 
			ccc.terminal	as Terminal, 
			(select count(*) from mt_movimientos_cc mcc where mcc.sello = '''||'BROUATM'||''' and mcc.emp_codigo = l.emp_codigo and mcc.loc_codigo = l.codigo and mcc.terminal = ccc.terminal and mcc.fecha > trunc(sysdate)-90)	as Cant_Transas_90_dias, 
			(select lo.fecha_ult_mov from dwm_prod.l_ubicaciones lo where lo.loc_codigo = l.codigo and lo.emp_codigo = l.emp_codigo)	as Fecha_Ult_Ticket
		from 		
			iposs.locales l, 	
			iposs.razones_sociales r, 	
			iposs.creditos_conf_cajas ccc	
		where		
			l.raz_soc_codigo = r.codigo	
		and 	ccc.emp_codigo = l.emp_codigo	
		and 	ccc.loc_codigo = l.codigo	
		and 	ccc.cre_codigo = 638	
		and 	ccc.emp_codigo <> 1000
		union
		select distinct		
			ccc.emp_codigo	as Cod_Empresa, 
			ccc.codigo	as Cod_Local, 
			ccc.raz_soc_descripcion	as Razon_Social, 
			ccc.ruc	as RUT, 
			'''||'Local '||'''||ccc.codigo	as Local, 
			ccc.Departamento, 
			ccc.Localidad, 
			ccc.direccion 	as Direccion, 
			ccc.terminal	as Terminal, 
			(select count(*) from mt_movimientos_cc mcc where mcc.sello = '''||'BROUATM'||''' and mcc.emp_codigo = ccc.emp_codigo and mcc.terminal = ccc.terminal and mcc.fecha > trunc(sysdate)-90)	as Cant_Transas_90_dias, 
			null	as Fecha_Ult_Ticket
		from 		
			iposs.aux_credconfcajas_polakof ccc	
		where 	ccc.cre_codigo = 638
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
		select distinct		
			l.emp_codigo	as Cod_Empresa, 
			l.codigo	as Cod_Local, 
			r.descripcion	as Razon_Social, 
			r.ruc	as RUT, 
			l.descripcion	as Local, 
			(select d.descripcion from iposs.departamentos d where d.codigo = l.depa_codigo)	as Departamento, 
			(select lo.descripcion from iposs.localidades lo where lo.codigo = l.loca_codigo)	as Localidad, 
			l.direccion	as Direccion, 
			ccc.terminal	as Terminal, 
			(select count(*) from mt_movimientos_cc mcc where mcc.sello = 'BROUATM' and mcc.emp_codigo = l.emp_codigo and mcc.loc_codigo = l.codigo and mcc.terminal = ccc.terminal and mcc.fecha > trunc(sysdate)-90)	as Cant_Transas_90_dias, 
			(select lo.fecha_ult_mov from dwm_prod.l_ubicaciones lo where lo.loc_codigo = l.codigo and lo.emp_codigo = l.emp_codigo)	as Fecha_Ult_Ticket
		from 		
			iposs.locales l, 	
			iposs.razones_sociales r, 	
			iposs.creditos_conf_cajas ccc	
		where 1 = 2
)


######################
consulta
######################
select 1 from #reco#
