select 
	ae.art_codigo_Externo 	"Codigo", 
	ae.descripcion  		"Articulo", 
	ranking.prod_Rubro		"Rubro", 
 	ranking.Venta 				"Venta", 
	ranking.Unidades 			"Unidades", 
	round (ranking.Unidades*(ranking.Precio/(1+(ih.tasa/100))), 2) "Unidades x PVP", 
	round (ranking.Precio/(1+(ih.tasa/100)), 2) 			"PVP", 
	nvl (pc.costo_ultimo * nvl (ca.valor, 1), 0) 		"Costo Ultimo", 
	round ((1 - (nvl (pc.costo_ultimo * nvl (ca.valor, 1), 0)/(ranking.Precio/(1+(ih.tasa/100))))) * 100, 2)	"M Bruto %", 
	round ((ranking.Precio/(1+(ih.tasa/100))) - nvl (pc.costo_ultimo * nvl (ca.valor, 1), 0), 2) 	"M Bruto Uni", 
	round (ranking.Unidades * ((ranking.Precio/(1+(ih.tasa/100))) - nvl (pc.costo_ultimo * nvl (ca.valor, 1), 0)), 2) "MB Total", 
	ranking_pesos 			"Ranking Venta" 
from 
(
	select 
		l.emp_codigo 			as Emp_codigo, 
		p.prod_cod_interno 		as Prod_cod_interno, 
		p.rub_descripcion 		as Prod_Rubro, 
		iposs.f_precio_venta_2 (
			p.prod_cod_interno, 
			(select codigo from iposs.locales loc where loc.emp_codigo = l.emp_codigo  and loc.loc_cod_empresa = iposs.f_param('LOCAL_CENTRAL', loc.codigo)), 
			to_number(iposs.f_param('LIS_PRE_VE_DEF', (select codigo from iposs.locales loc where loc.emp_codigo = l.emp_codigo  and loc.loc_cod_empresa = iposs.f_param('LOCAL_CENTRAL', loc.codigo)))), 
			:FechaHasta, 
			l.emp_codigo) as Precio, 
		sum (nvl (fvpu.ven_cantidad, 0)) - sum (nvl (fvpu.dev_cantidad, 0)) as Unidades, 
		sum (nvl (fvpu.ven_nac_importe, 0) - nvl (fvpu.dev_nac_importe, 0)) as Venta, 
		rank () over 
			(order by sum (
				nvl (fvpu.ven_nac_importe, 0) - 
				nvl (fvpu.dev_nac_importe, 0) ) desc)	as ranking_pesos
	from
		dwm_prod.l_ubicaciones l, 
		dwm_prod.l_productos p, 
		dwm_prod.f_ventas_pu_diario_min fvpu
	where
		( l.codigo = fvpu.l_ubi_codigo 
	and 	l.emp_codigo = fvpu.emp_codigo ) 
	and
		( p.codigo = fvpu.l_pro_codigo 
	and 	  p.emp_codigo = fvpu.emp_codigo )
	and
		( fvpu.fecha_comercial between :FechaDesde and :FechaHasta )
	and
		( l.loc_codigo in :Local )
	and 
		( p.tip_prod_codigo = 1 )
	and 
		( l.emp_codigo = $EMPRESA_LOGUEADA$
	and 	  p.emp_codigo = $EMPRESA_LOGUEADA$
	and 	  fvpu.emp_codigo = $EMPRESA_LOGUEADA$ )
	group by 
		l.emp_codigo 		, 
		p.prod_cod_interno	, 
		p.rub_descripcion) ranking
join iposs.articulos_empresas ae 
	on ae.art_codigo = ranking.Prod_cod_interno 
	and ae.emp_codigo = ranking.emp_codigo
join iposs.articulos_locales al 
	on al.art_codigo = ranking.Prod_cod_interno
	and al.emp_codigo = ranking.emp_codigo 
	and al.loc_codigo = (select codigo from iposs.locales l where l.emp_codigo = ranking.emp_codigo  and loc_cod_empresa = iposs.f_param('LOCAL_CENTRAL', l.codigo))
join iposs.ivas i 
	on ae.iva_codigo = i.codigo
join iposs.impuestos_hists ih 
	on i.codigo = ih.imp_codigo 
	and ih.fecha < :FechaHasta 
	and ih.vigencia_hasta > :FechaHasta
left join iposs.precios_de_costos_articulos pc 
	on pc.art_codigo = ranking.Prod_cod_interno
	and pc.emp_codigo = ranking.emp_codigo 
	and pc.loc_codigo = al.loc_codigo
	and pc.vigencia_desde < sysdate
	and pc.vigencia_hasta > sysdate
left join iposs.cambios ca
	on ca.mon_codigo = pc.mon_codigo
	and ca.emp_codigo = pc.emp_codigo
	and ca.fecha < sysdate
	and ca.vigencia_hasta > sysdate
where	ranking_pesos <= nvl (:Ranking, 130)
and 	:FechaHasta - :FechaDesde <= 31
