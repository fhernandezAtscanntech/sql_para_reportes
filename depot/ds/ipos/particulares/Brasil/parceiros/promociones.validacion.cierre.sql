SELECT
  nvl(ventasPromocion.EMP_CODIGO, cierrePromocion.EMP_CODIGO) as EMP_CODIGO,
  nvl(ventasPromocion.LOC_COD_EMPRESA, cierrePromocion.LOC_COD_EMPRESA) AS LOC_COD_EMPRESA,
  nvl(TO_CHAR(ventasPromocion.FECHA_COMERCIAL, 'DD/MM/YYYY'), TO_CHAR(cierrePromocion.FECHA_COMERCIAL, 'DD/MM/YYYY')) AS FECHA_COMERCIAL,
  nvl(ventasPromocion.PROM_COD_EMPRESA, cierrePromocion.PROM_COD_EMPRESA) AS PROM_COD_EMPRESA,
  nvl(ventasPromocion.PROD_CODIGO, cierrePromocion.ART_CODIGO_EXTERNO) AS PROD_CODIGO,
  nvl(ventasPromocion.CODIGO_BARRAS, cierrePromocion.CODIGO_BARRA) AS CODIGO_BARRA,
  ventasPromocion.CANTIDAD as cantidadVentas,
  cierrePromocion.CANTIDAD as cantidadCierres,
  (nvl(ventasPromocion.CANTIDAD, 0) - nvl(cierrePromocion.CANTIDAD, 0)) as conciliacion
FROM 
    (
      SELECT 
        fvm.emp_codigo,
        l.loc_cod_empresa,
        prom.prom_cod_empresa, 
        fvm.fecha_comercial,
        p.PROD_CODIGO,
        p.CODIGO_BARRAS,
        sum (nvl(fvm.ven_cantidad, 0) - nvl(fvm.dev_cantidad, 0)) as CANTIDAD
      FROM
        dwm_prod.f_ventas_promo_diario_min fvm, 
        dwm_prod.l_productos p, 
        dwm_prod.l_promociones prom, 
        dwm_prod.l_ubicaciones l,
        IPOSS.EMPRESAS e
  WHERE 
        e.CODIGO = p.emp_codigo AND
        p.emp_codigo = fvm.emp_codigo AND 
        p.codigo = fvm.l_pro_codigo AND
        prom.codigo = fvm.l_prom_codigo AND
        l.codigo = fvm.l_ubi_codigo AND
        l.emp_codigo = fvm.emp_codigo AND 
        p.tip_prod_codigo = 1 AND 
        prom.prom_codigo <> 0 AND
        e.INT_CODIGO = :Integrador AND
        fvm.fecha_comercial >= :FechaDesde AND 
        fvm.fecha_comercial <= :FechaHasta + 1 - (1/24/60/60)
      GROUP BY
        fvm.emp_codigo,
        l.loc_cod_empresa, 
        fvm.fecha_comercial,
        prom.prom_cod_empresa, 
        p.PROD_CODIGO,
        p.CODIGO_BARRAS
      ) ventasPromocion FULL OUTER JOIN
      (
        SELECT 
          rpb.EMP_CODIGO, 
          rpb.LOC_COD_EMPRESA, 
          rpb.FECHA_COMERCIAL,
          rpb.PROM_COD_EMPRESA, 
          rpb.ART_CODIGO_EXTERNO, 
          rpb.CODIGO_BARRA, 
          SUM(CASE WHEN rpb.TIPO_DE_LINEA = 4 THEN rpb.CANTIDAD ELSE -1*rpb.CANTIDAD END)as CANTIDAD
        FROM 
          IPOSS.REPORTE_PROMO_BR rpb, 
          IPOSS.EMPRESAS e
        WHERE 
          rpb.FECHA_COMERCIAL >= :FechaDesde AND 
          rpb.FECHA_COMERCIAL <= :FechaHasta + 1 - (1/24/60/60) AND
          e.CODIGO = rpb.EMP_CODIGO AND
          e.INT_CODIGO = :Integrador
        GROUP BY 
          rpb.EMP_CODIGO, 
          rpb.LOC_COD_EMPRESA, 
          rpb.FECHA_COMERCIAL,
          rpb.PROM_COD_EMPRESA, 
          rpb.ART_CODIGO_EXTERNO, 
          rpb.CODIGO_BARRA
      ) cierrePromocion ON (
            ventasPromocion.EMP_CODIGO = cierrePromocion.EMP_CODIGO AND
            ventasPromocion.LOC_COD_EMPRESA = cierrePromocion.LOC_COD_EMPRESA AND
            TO_CHAR(ventasPromocion.FECHA_COMERCIAL, 'DD/MM/YYYY') = TO_CHAR(cierrePromocion.FECHA_COMERCIAL, 'DD/MM/YYYY') AND
            ventasPromocion.PROM_COD_EMPRESA = cierrePromocion.PROM_COD_EMPRESA AND
            ventasPromocion.PROD_CODIGO = cierrePromocion.ART_CODIGO_EXTERNO AND
            ventasPromocion.CODIGO_BARRAS = cierrePromocion.CODIGO_BARRA)
WHERE (nvl(ventasPromocion.CANTIDAD, 0) - nvl(cierrePromocion.CANTIDAD, 0)) != 0
ORDER BY 1, 2, 3