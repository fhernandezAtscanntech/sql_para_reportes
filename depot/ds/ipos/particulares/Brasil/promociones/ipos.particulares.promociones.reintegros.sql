multiple

select 
	t.*, 
	dense_rank () over (order by "TotalVenta" desc) "Ranking2"
from iposcla.$$tabla$$ t


######################
sql
######################
create table $$sql$$ as (
	select '
		select 		
			p.prom_descripcion	"Promoci�n", 
			p.prom_cod_empresa	"Cod.prom.Emp", 
			p.cpa_descripcion 	"Campa�a", 
			l.loc_codigo||l.emp_codigo 	"C�digo �nico local", 
			l.emp_descripcion	"Empresa", 
			l.loc_modelo_descripcion 	"Local", 
			f.fecha_comercial	"Fecha", 
			sum (nvl (f.reintegro_importe, 0)) 	"Reintegro", 
			sum (nvl (f.des_prom_nac_importe, 0)) 	"Descuento Prom", 
			(select nvl (sum (ven_nac_importe), 0) - nvl (sum (dev_nac_importe), 0) 
				from dwm_prod.f_ventas_caja_cli_diario_min v
				where v.fecha_comercial between to_date('''||:FechaDesde||''', '''||'dd/mm/yyyy'||''')
				and to_date('''||:FechaHasta||''', '''||'dd/mm/yyyy'||''') and v.l_ubi_codigo = f.l_ubi_codigo) 		"TotalVenta"
		from 		
			dwm_prod.l_ubicaciones l, 	
			dwm_prod.l_promociones p, 	
			dwm_prod.f_ventas_promo_diario_min f	
		where	f.l_prom_codigo = p.codigo	
		and 	f.l_ubi_codigo = l.codigo	
		and 	f.fecha_comercial between to_date('''||:FechaDesde||''', '''||'dd/mm/yyyy'||''')
				and to_date('''||:FechaHasta||''', '''||'dd/mm/yyyy'||''')
		and 	to_date('''||:FechaHasta||''', '''||'dd/mm/yyyy'||''') - to_date('''||:FechaDesde||''', '''||'dd/mm/yyyy'||''') <= 93
		group by 		
			p.prom_descripcion	, 
			p.prom_cod_empresa	, 
			p.cpa_descripcion 	, 
			l.emp_codigo 	, 
			l.loc_codigo 	, 
			l.emp_descripcion	, 
			l.loc_modelo_descripcion 	, 
			f.fecha_comercial	, 
			f.l_ubi_codigo
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
		select 		
			p.prom_descripcion	"Promoci�n", 
			p.prom_cod_empresa	"Cod.prom.Emp", 
			p.cpa_descripcion 	"Campa�a", 
			l.loc_codigo||l.emp_codigo 	"C�digo �nico local", 
			l.emp_descripcion	"Empresa", 
			l.loc_modelo_descripcion 	"Local", 
			f.fecha_comercial	"Fecha", 
			sum (nvl (f.reintegro_importe, 0)) 	"Reintegro", 
			sum (nvl (f.des_prom_nac_importe, 0)) 	"Descuento Prom", 
			(select nvl (sum (ven_nac_importe), 0) - nvl (sum (dev_nac_importe), 0) 
				from dwm_prod.f_ventas_caja_cli_diario_min v
				where v.fecha_comercial between to_date('''||:FechaDesde||''', '''||'dd/mm/yyyy'||''')
				and to_date('''||:FechaHasta||''', '''||'dd/mm/yyyy'||''') and v.l_ubi_codigo = f.l_ubi_codigo) 		"TotalVenta"
		from 		
			dwm_prod.l_ubicaciones l, 	
			dwm_prod.l_promociones p, 	
			dwm_prod.f_ventas_promo_diario_min f	
		where 1 = 2
		group by 		
			p.prom_descripcion	, 
			p.prom_cod_empresa	, 
			p.cpa_descripcion 	, 
			l.emp_codigo 	, 
			l.loc_codigo 	, 
			l.emp_descripcion	, 
			l.loc_modelo_descripcion 	, 
			f.fecha_comercial			, 
			f.l_ubi_codigo
)

######################
consulta
######################
select 1 from #reco#
