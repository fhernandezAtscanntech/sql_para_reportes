multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
		select
			l.codigo_unico_local as codigo_unico_local,
			l.emp_codigo as empresa,
			l.desc_local as local,
			p.titulo as titulo,
			p.descripcion as descripcion,
			p.vigencia_desde as vigencia_desde,
			p.vigencia_hasta as vigencia_hasta,
			ppm.antes_max as precio_antes,
			ppm.ahora_max as precio_ahora,
			ppm.descuento_max as descuento,
			ppm.reintegro_max as reintegro,
			pp.fecha_propuesta as fecha_propuesta
		from 
			iposs_mp.pro_pro_importes ppm
		join iposs_mp.propuestas_promociones pp 
			on pp.id = ppm.pro_pro_id
		join iposs_mp.promociones p 
			on pp.id_promocion = p.id
		join iposs_mp.locales l 
			on pp.id_local_oferido = l.id
		where	p.vigencia_hasta >= sysdate
		and 	pp.estado = '''||'PENDIENTE'||'''
		and 	pp.fecha_borrado is null
		and 	p.fecha_borrado is null
		and		(ppm.antes_max <= 0
			or ppm.ahora_max <=0
			or ppm.descuento_max < 0
			or ppm.reintegro_max <= 0)
		order by 
			l.codigo_unico_local
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
		select
			l.codigo_unico_local as codigo_unico_local,
			l.emp_codigo as empresa,
			l.desc_local as local,
			p.titulo as titulo,
			p.descripcion as descripcion,
			p.vigencia_desde as vigencia_desde,
			p.vigencia_hasta as vigencia_hasta,
			ppm.antes_max as precio_antes,
			ppm.ahora_max as precio_ahora,
			ppm.descuento_max as descuento,
			ppm.reintegro_max as reintegro,
			pp.fecha_propuesta as fecha_propuesta
		from 
			iposs_mp.pro_pro_importes ppm
		join iposs_mp.propuestas_promociones pp 
			on pp.id = ppm.pro_pro_id
		join iposs_mp.promociones p 
			on pp.id_promocion = p.id
		join iposs_mp.locales l 
			on pp.id_local_oferido = l.id
		where 1=2
)

######################
consulta
######################
select 1 from #reco#
