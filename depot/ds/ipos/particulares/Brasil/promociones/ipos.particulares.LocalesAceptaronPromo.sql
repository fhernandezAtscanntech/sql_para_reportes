multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
		SELECT 
			l.CODIGO_UNICO_LOCAL AS "COD UNICO", 
			decode (nvl (pp.cant, 0), 0, '''||'NÃO'||''', '''||'SIM'||''') AS "ACEITOU PROMO" 
		FROM 
			IPOSS_MP.LOCALES l, 
			(select pp.id_local_oferido, count(*) as cant
				from iposs_mp.propuestas_promociones pp, iposs_mp.promociones p
				where pp.id_promocion = p.id
				and pp.fecha_aceptacion is not null
				and 
					((p.vigencia_desde >= trunc(sysdate) - '||:Dias||' and p.vigencia_desde < trunc(sysdate))
					or
					(p.vigencia_hasta >= trunc(sysdate) - '||:Dias||' and p.vigencia_hasta < trunc(sysdate)))
				and pp.estado = '''||'ACEPTADA'||'''
				and pp.fecha_borrado is null
				group by pp.id_local_oferido) pp
		WHERE l.fecha_borrado is null
		AND l.id = pp.id_local_oferido (+)
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
	SELECT 
		l.CODIGO_UNICO_LOCAL AS "COD UNICO", 
		'ESTADO' AS "ACEITOU PROMO" 
	FROM 
		IPOSS_MP.LOCALES l
	WHERE 
	   1=2
)

######################
consulta
######################
	

