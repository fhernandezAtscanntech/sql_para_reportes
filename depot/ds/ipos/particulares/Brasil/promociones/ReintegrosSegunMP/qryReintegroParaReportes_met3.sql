create or replace function iposs.qryReintegroParaReportes_met3 (
p_emp_codigo IPOSS.EMPRESAS.CODIGO%type, 
p_loc_codigo IPOSS.LOCALES.CODIGO%type, 
p_prom_codigo IPOSS.PROMOCIONES.CODIGO%type, 
p_fec_desde date,
p_fec_hasta date) 
return number
is
	v_reintegro number;
begin
	select nvl (sum (imp_reint), 0)
	into v_reintegro
	from dw_rint.f_reintegros_diario f, iposs.promociones p
	where f.prom_cod_empresa = p.prom_cod_empresa 
	and p.emp_codigo = 1
	and p.codigo = p_prom_codigo
	and f.emp_codigo = p_emp_codigo
	and f.loc_codigo = p_loc_codigo
	and f.fecha_comercial >= p_fec_desde
	and f.fecha_comercial <= p_fec_hasta;

	return (v_reintegro);
	end;
/
