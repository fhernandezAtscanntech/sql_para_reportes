multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
		select 
			l.emp_codigo 				"Cod.Empresa", 
			l.loc_codigo 				"Cod.Local", 
			l.emp_descripcion			"Empresa", 
			l.loc_modelo_descripcion 	"Local", 
			p.prom_cod_empresa 			"Cod.prom.Emp", 
			p.prom_descripcion 			"Promoci�n", 
			p.cpa_descripcion 			"Campa�a", 
			iposs.ReintegroParaReportes (
				l.emp_codigo, 
				l.loc_codigo, 
				p.prom_codigo, 
				to_date('''||:FechaDesde||''', '''||'dd/mm/yyyy'||'''), 
				to_date('''||:FechaHasta||''', '''||'dd/mm/yyyy'||''')) "Reintegro", 
			(select nvl (sum (ven_nac_importe), 0) - nvl (sum (dev_nac_importe), 0) 
				from dwm_prod.f_ventas_caja_cli_diario_min v
				where v.fecha_comercial between to_date('''||:FechaDesde||''', '''||'dd/mm/yyyy'||''')
				and to_date('''||:FechaHasta||''', '''||'dd/mm/yyyy'||''')				
				and v.l_ubi_codigo = l.codigo) 		"TotalVenta"
		from 
			dwm_prod.l_ubicaciones l, 
			dwm_prod.l_promociones p, 
			iposs.prom_locales_habilitados pl, 
			iposs.prom_vigencias pv
		where 	pl.prom_codigo = pv.prom_codigo
		and 	pl.prom_codigo = p.prom_codigo
		and 	pl.emp_codigo = l.emp_codigo
		and 	pl.loc_codigo = l.loc_codigo
		and 	pv.vigencia_desde <= to_date('''||:FechaHasta||''', '''||'dd/mm/yyyy'||''')
		and 	pv.vigencia_hasta >= to_date('''||:FechaDesde||''', '''||'dd/mm/yyyy'||''')' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
		select 
			l.emp_codigo 				"Cod.Empresa", 
			l.loc_codigo 				"Cod.Local", 
			l.emp_descripcion			"Empresa", 
			l.loc_modelo_descripcion 	"Local", 
			p.prom_cod_empresa 			"Cod.prom.Emp", 
			p.prom_descripcion 			"Promoci�n", 
			p.cpa_descripcion 			"Campa�a", 
			0.0 						"Reintegro"
		from 
			dwm_prod.l_ubicaciones l, 
			dwm_prod.l_promociones p, 
			iposs.prom_locales_habilitados pl, 
			iposs.prom_vigencias pv
		where 1 = 2
)

######################
consulta
######################
select 1 from #reco#

