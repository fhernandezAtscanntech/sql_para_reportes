create or replace function iposs.qryReintegroParaReportes_met1 (
p_emp_codigo IPOSS.EMPRESAS.CODIGO%type, 
p_loc_codigo IPOSS.LOCALES.CODIGO%type, 
p_prom_codigo IPOSS.PROMOCIONES.CODIGO%type, 
p_fec_desde date,
p_fec_hasta date) 
return number
is
	v_reintegro number;
	v_estimado number;
	v_fechaInicioPagoCierres date := to_date('01/08/2016', 'dd/mm/yyyy');
	v_toleranciaReintegroCierre number := 0.01;
begin
		SELECT 
			NVL (SUM (CASE
				WHEN NVL (reintegros_dw.FECHA_COMERCIAL, reintegros_cierres.FECHA_COMERCIAL) < 
							(select least (nvl (min(fecha_comercial), to_date('01/04/2016', 'dd/mm/yyyy')), to_date('01/04/2016', 'dd/mm/yyyy')) 
							from iposs.reporte_promo_br 
							where emp_codigo = p_emp_codigo
							and loc_codigo = nvl (p_loc_codigo, loc_codigo))
					THEN NVL (reintegros_dw.cantidad, 0) * COALESCE (pdr.IMPORTE_REINTEGRO, pdr2.IMPORTE_REINTEGRO, 0)
				WHEN mp.vigencia_desde  < v_fechaInicioPagoCierres
					THEN
					GREATEST (
						NVL (reintegros_dw.cantidad * COALESCE (pdr.IMPORTE_REINTEGRO, pdr2.IMPORTE_REINTEGRO, 0), -999999999),
						NVL (reintegros_cierres.cantidad * COALESCE (pdr.IMPORTE_REINTEGRO, pdr2.IMPORTE_REINTEGRO, 0), -999999999))
				ELSE
-- 					NVL (reintegros_cierres.cantidad, 0) * COALESCE (pdr.IMPORTE_REINTEGRO, pdr2.IMPORTE_REINTEGRO, 0)
					CASE (	-- tomo en cuenta los casos de los minoristas que nunca enviaron un cierre, como los de base 1
						select count(*) 
						from iposs.reporte_promo_br 
						where emp_codigo = p_emp_codigo
						and loc_codigo = nvl (p_loc_codigo, loc_codigo))
					when 0 then NVL (reintegros_dw.cantidad, 0) * COALESCE (pdr.IMPORTE_REINTEGRO, pdr2.IMPORTE_REINTEGRO, 0)
					else NVL (reintegros_cierres.cantidad, 0) * COALESCE (pdr.IMPORTE_REINTEGRO, pdr2.IMPORTE_REINTEGRO, 0)
					end
				END), 0) as importe,
			CASE 
				WHEN SUM(
					CASE WHEN NVL (reintegros_dw.FECHA_COMERCIAL, reintegros_cierres.FECHA_COMERCIAL) >= 
									(select greatest (nvl (min(fecha_comercial), to_date('01/04/2016', 'dd/mm/yyyy')), to_date('01/04/2016', 'dd/mm/yyyy')) 
									from iposs.reporte_promo_br 
									where emp_codigo = p_emp_codigo)
						and NVL (reintegros_dw.cantidad, 0) > NVL (reintegros_cierres.cantidad, 0) + v_toleranciaReintegroCierre
					THEN 1 
					ELSE 0 
					END) > 0 
				THEN 1 
				ELSE 0 
				END as estimado
		into v_reintegro, v_estimado
		from 
			(SELECT
				vpdm.emp_codigo,
				u.loc_codigo,
				trunc (vpdm.fecha_comercial) as fecha_comercial,
				prom.PROM_CODIGO,
				prom.PROM_COD_EMPRESA,
				prod.CODIGO_BARRAS,
				prod.PROD_CODIGO as art_codigo_externo,
				SUM (NVL (vpdm.ven_cantidad, 0) - NVL (vpdm.dev_cantidad, 0)) AS cantidad
			FROM
				dwm_prod.f_ventas_promo_diario_min vpdm
				join dwm_prod.l_productos prod 
					on prod.codigo = vpdm.l_pro_codigo 
					and prod.tip_prod_codigo = 1 
					and prod.EMP_CODIGO = vpdm.EMP_CODIGO
				join dwm_prod.l_promociones prom 
					on prom.codigo = vpdm.l_prom_codigo 
					and prom.prom_codigo <> 0
				join dwm_prod.l_ubicaciones u 
					on u.codigo = vpdm.l_ubi_codigo 
					and u.EMP_CODIGO = vpdm.EMP_CODIGO
			WHERE
				vpdm.emp_codigo = p_emp_codigo
				and trunc(vpdm.FECHA_COMERCIAL) >= p_fec_desde
				and trunc(vpdm.FECHA_COMERCIAL) <= p_fec_hasta
				and prom.prom_codigo = nvl (p_prom_codigo, prom.prom_codigo)
				and u.loc_codigo = nvl (p_loc_codigo, u.loc_codigo)
			GROUP BY 
				vpdm.emp_codigo, 
				u.loc_codigo,
				trunc (vpdm.fecha_comercial),
				prom.prom_codigo,
				prom.prom_cod_empresa,
				prod.CODIGO_BARRAS,
				prod.PROD_CODIGO) reintegros_dw
		FULL OUTER JOIN 
			(SELECT
				rpb.EMP_CODIGO,
				rpb.LOC_CODIGO,
				trunc (rpb.FECHA_COMERCIAL) as fecha_comercial,
				rpb.PROM_CODIGO,
				rpb.PROM_COD_EMPRESA,
				rpb.CODIGO_BARRA as codigo_barras,
				rpb.ART_CODIGO_EXTERNO,
				SUM (CASE 
						WHEN rpb.TIPO_DE_LINEA = 4 
							THEN NVL(rpb.CANTIDAD, 0) 
						ELSE - 1 * NVL(rpb.CANTIDAD, 0) 
					END) AS CANTIDAD
			FROM
				iposs.reporte_promo_br rpb
			join iposs.promociones p 
				on p.PROM_COD_EMPRESA = rpb.PROM_COD_EMPRESA 
				and p.EMP_CODIGO = 1
			WHERE
				rpb.EMP_CODIGO = p_emp_codigo
				and trunc(rpb.FECHA_COMERCIAL) >= p_fec_desde
				and trunc(rpb.FECHA_COMERCIAL) <= p_fec_hasta
				and rpb.prom_codigo = nvl (p_prom_codigo, rpb.prom_codigo)
				and rpb.loc_codigo = nvl (p_loc_codigo, rpb.loc_codigo)
			GROUP BY
				rpb.EMP_CODIGO,
				rpb.LOC_CODIGO,
				trunc (rpb.FECHA_COMERCIAL),
				rpb.PROM_CODIGO,
				rpb.PROM_COD_EMPRESA,
				rpb.CODIGO_BARRA,
				rpb.ART_CODIGO_EXTERNO) reintegros_cierres
			on reintegros_cierres.EMP_CODIGO = reintegros_dw.EMP_CODIGO
			and reintegros_cierres.LOC_CODIGO = reintegros_dw.LOC_CODIGO
			and reintegros_cierres.FECHA_COMERCIAL = reintegros_dw.FECHA_COMERCIAL
			and reintegros_cierres.PROM_CODIGO = reintegros_dw.PROM_CODIGO
			and reintegros_cierres.PROM_COD_EMPRESA = reintegros_dw.PROM_COD_EMPRESA
			and reintegros_cierres.art_codigo_externo = reintegros_dw.art_codigo_externo
			left join iposs_mp.promociones mp 
				on NVL(reintegros_dw.prom_cod_empresa, reintegros_cierres.prom_cod_empresa) = mp.codigo_interno_be
			left join 
				(IPOSS.PROM_DATOS_REINTEGRO pdr   
				join IPOSS.PROM_DATOS pd 
					on pd.CODIGO = pdr.PROM_DAT_CODIGO)
				on pd.emp_codigo = 1
				and pdr.ART_CODIGO_EXT_HIJO = NVL (reintegros_dw.art_codigo_externo, reintegros_cierres.art_codigo_externo)
				and pd.PROM_CODIGO = NVL (reintegros_dw.prom_codigo, reintegros_cierres.prom_codigo)
				and pdr.LOC_CODIGO = NVL (reintegros_dw.loc_codigo, reintegros_cierres.loc_codigo)
			left join 
				(IPOSS.PROM_DATOS_REINTEGRO pdr2 
				join IPOSS.PROM_DATOS pd2 
					on pd2.CODIGO = pdr2.PROM_DAT_CODIGO)
				on pd.codigo is null
				and pd2.emp_codigo = 1
				and pd2.PROM_CODIGO = NVL (reintegros_dw.prom_codigo, reintegros_cierres.prom_codigo)
				and pdr2.LOC_CODIGO = NVL (reintegros_dw.loc_codigo, reintegros_cierres.loc_codigo)
				and (pdr2.art_codigo_ext_hijo = reintegros_dw.ART_CODIGO_EXTERNO 
					OR pdr2.art_codigo_ext_hijo = reintegros_cierres.ART_CODIGO_EXTERNO)
			where mp.TIPO_PROMOCION = 'PROMO_PROVEEDOR';
	
		return (v_reintegro);
end;
/
