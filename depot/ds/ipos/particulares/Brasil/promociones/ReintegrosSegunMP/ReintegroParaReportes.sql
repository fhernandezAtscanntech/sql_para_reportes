create or replace function iposs.ReintegroParaReportes (
p_emp_codigo IPOSS.EMPRESAS.CODIGO%type, 
p_loc_codigo IPOSS.LOCALES.CODIGO%type, 
p_prom_codigo IPOSS.PROMOCIONES.CODIGO%type, 
p_fec_desde date,
p_fec_hasta date) 
return number
is
begin
-- 		return (iposs.qryReintegroParaReportes_met1(p_emp_codigo, p_loc_codigo, p_prom_codigo, p_fec_desde, p_fec_hasta));
--		return (iposs.qryReintegroParaReportes_met2(p_emp_codigo, p_loc_codigo, p_prom_codigo, p_fec_desde, p_fec_hasta));
	return (iposs.qryReintegroParaReportes_met3(p_emp_codigo, p_loc_codigo, p_prom_codigo, p_fec_desde, p_fec_hasta));
end;
/

grant execute on iposs.ReintegroParaReportes to iposs_rep, iposs_fx
/

