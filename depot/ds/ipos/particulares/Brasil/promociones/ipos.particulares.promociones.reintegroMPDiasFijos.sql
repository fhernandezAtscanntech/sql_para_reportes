multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select 
		'SELECT cc.LOC_CODIGO||cc.EMP_CODIGO as COD_UNICO,  c.DESCRIPCION as campana,  CC.REINTEGRO 
		FROM 
			(SELECT 
			rd.EMP_CODIGO, 
			rd.LOC_CODIGO, 
			pbe.CPA_CODIGO, 
			SUM(NVL(rd.IMP_REINT, 0)) Reintegro
		from
			DW_RINT.f_reintegros_diario rd    
				INNER JOIN IPOSS.PROMOCIONES pbe ON (rd.PROM_COD_EMPRESA = pbe.PROM_COD_EMPRESA)
		WHERE  
			rd.FECHA_COMERCIAL >= trunc(sysdate) - '||:Dias||' AND
			rd.FECHA_COMERCIAL <= trunc(sysdate)
		GROUP BY rd.EMP_CODIGO, rd.LOC_CODIGO, pbe.CPA_CODIGO
		HAVING SUM(NVL(rd.IMP_REINT, 0)) != 0) cc
			INNER JOIN  iposs.campañas c ON (cc.CPA_CODIGO = c.CODIGO)
' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
		SELECT cc.LOC_CODIGO||cc.EMP_CODIGO as COD_UNICO,  c.DESCRIPCION as campana,  CC.REINTEGRO 
		FROM 
			(SELECT 
			rd.EMP_CODIGO, 
			rd.LOC_CODIGO, 
			pbe.CPA_CODIGO, 
			SUM(NVL(rd.IMP_REINT, 0)) Reintegro
		from
			DW_RINT.f_reintegros_diario rd    
				INNER JOIN IPOSS.PROMOCIONES pbe ON (rd.PROM_COD_EMPRESA = pbe.PROM_COD_EMPRESA)
		WHERE  
			1 = 2
		GROUP BY rd.EMP_CODIGO, rd.LOC_CODIGO, pbe.CPA_CODIGO
		HAVING SUM(NVL(rd.IMP_REINT, 0)) != 0) cc
			INNER JOIN  iposs.campañas c ON (cc.CPA_CODIGO = c.CODIGO)
)

######################
consulta
######################
select 1 from #reco#

