multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
		select 			
			p.prom_cod_empresa	"Promoción", 	
			p.descripcion	"Cod.prom.Emp", 	
			pv.vigencia_desde	"Vigencia Desde", 	
			pv.vigencia_hasta	"Vigencia hasta"	
		from 			
			iposs.promociones p, 		
			iposs.prom_vigencias pv		
		where	p.codigo = pv.prom_codigo		
		and 	sysdate between pv.vigencia_desde and pv.vigencia_hasta		
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
		select 			
			p.prom_cod_empresa	"Promoción", 	
			p.descripcion	"Cod.prom.Emp", 	
			pv.vigencia_desde	"Vigencia Desde", 	
			pv.vigencia_hasta	"Vigencia hasta"	
		from 			
			iposs.promociones p, 		
			iposs.prom_vigencias pv		
		where 1 = 2
)

######################
consulta
######################
select 1 from #reco#
