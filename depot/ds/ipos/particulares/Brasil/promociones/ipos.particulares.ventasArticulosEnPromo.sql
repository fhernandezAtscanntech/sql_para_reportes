multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
		select 
			c.descripcion 		"Campana", 
			p.prom_cod_empresa 	"Cod.Promoción", 
			p.descripcion 	 	"Promoción", 
			l.emp_codigo 		"Cod.Empresa", 
			l.loc_codigo 		"Cod.Local", 
			l.loc_codigo||l.emp_codigo 	"Código único local", 
			l.emp_descripcion 	"Empresa", 
			l.loc_descripcion 	"Local", 
			pm.prod_descripcion 	"Producto", 
			pm.codigo_barras 		"Cod.Barra", 
			f.fecha_comercial 		"Fecha comercial", 
			case f.l_prom_codigo 
				when 0 then '''||'Sin Promo'||''' 
				else '''||'Con Promo'||''' 
			end 				"En Promoción", 
			case pl.estado 
				when 1 then '''||'PENDIENTE'||''' 
				when 2 then '''||'APROBADO'||''' 
				when 3 then '''||'RECHAZADO'||''' 
				when 4 then '''||'FINALIZADO'||''' 
				when 5 then '''||'REPROCESAR'||''' 
				when 6 then '''||'PENDIENTE DE ACEPTAR'||''' 
				when 7 then '''||'PENDIENTE DE ARTICULOS'||''' 
				else '''||'NO IDENTIFICAD'||''' 
			end 				"Estado Local", 
			nvl(sum (f.ven_nac_importe), 0) - nvl (sum (dev_nac_importe), 0)	"Venta", 
			nvl(sum (f.des_prom_nac_importe), 0) 			"Descuento Promo"
		from 
			iposs.promociones p, 
			iposs.campañas c, 
			iposs.prom_locales_habilitados pl, 
			iposs.prom_vigencias pv, 
			dwm_prod.F_VENTAS_ARTS_PROMO_DIARIO_MIN f, 
			dwm_prod.l_ubicaciones l, 
			dwm_prod.l_productos_modelo pm
		where	p.cpa_codigo = c.codigo
		and 	pl.prom_codigo = p.codigo
		and 	pv.prom_codigo = p.codigo
		and 	pl.emp_codigo = l.emp_codigo
		and 	pl.loc_codigo = l.loc_codigo
		and 	f.l_ubi_codigo = l.codigo
		and 	f.l_pro_mod_codigo = pm.codigo
		and 	pm.prod_cod_interno in 
			(select distinct ae.art_codigo
			from iposs.articulos_empresas ae
			where ae.emp_codigo = 1
			and ae.art_codigo in 
				(select distinct ae2.art_codigo
				from articulos_empresas ae2
				where ae2.emp_codigo <> 1
				and (ae2.emp_codigo, ae2.art_codigo_externo) in
					(select pdr.emp_codigo, pdr.art_codigo_ext_hijo
					from prom_datos_reintegro pdr
					where pdr.prom_dat_codigo in
						(select pd.codigo
						from prom_datos pd
						where pd.prom_codigo = p.codigo))))
		and 	c.descripcion = '''||:Campana||'''
		and 	f.fecha_comercial >= pv.vigencia_desde
		and 	f.fecha_comercial <= pv.vigencia_hasta
		group by 
			c.descripcion	, 
			p.prom_cod_empresa	, 
			p.descripcion	, 
			l.emp_codigo	, 
			l.loc_codigo	, 
			l.emp_descripcion 	, 
			l.loc_descripcion	, 
			pm.prod_descripcion	, 
			pm.codigo_barras	, 
			f.fecha_comercial	, 
			f.l_prom_codigo	, 
			pl.estado
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
		select 
			c.descripcion 		"Campana", 
			p.prom_cod_empresa 	"Cod.Promoción", 
			p.descripcion 	 	"Promoción", 
			l.emp_codigo 		"Cod.Empresa", 
			l.loc_codigo 		"Cod.Local", 
			l.loc_codigo||l.emp_codigo 	"Código único local", 
			l.emp_descripcion 	"Empresa", 
			l.loc_descripcion 	"Local", 
			pm.prod_descripcion 	"Producto", 
			pm.codigo_barras 		"Cod.Barra", 
			f.fecha_comercial 		"Fecha comercial", 
			case f.l_prom_codigo 
				when 0 then 'Sin Promo' 
				else 'Con Promo' 
			end 				"En Promoción", 
			case pl.estado 
				when 1 then 'PENDIENTE' 
				when 2 then 'APROBADO' 
				when 3 then 'RECHAZADO' 
				when 4 then 'FINALIZADO' 
				when 5 then 'REPROCESAR' 
				when 6 then 'PENDIENTE DE ACEPTAR' 
				when 7 then 'PENDIENTE DE ARTICULOS' 
				else 'NO IDENTIFICAD' 
			end 				"Estado Local", 
			nvl(sum (f.ven_nac_importe), 0) - nvl (sum (dev_nac_importe), 0)	"Venta", 
			nvl(sum (f.des_prom_nac_importe), 0) 			"Descuento Promo"
		from 
			iposs.promociones p, 
			iposs.campañas c, 
			iposs.prom_locales_habilitados pl, 
			iposs.prom_vigencias pv, 
			dwm_prod.F_VENTAS_ARTS_PROMO_DIARIO_MIN f, 
			dwm_prod.l_ubicaciones l, 
			dwm_prod.l_productos_modelo pm
		where	1=2
		group by 
			c.descripcion	, 
			p.prom_cod_empresa	, 
			p.descripcion	, 
			l.emp_codigo	, 
			l.loc_codigo	, 
			l.emp_descripcion 	, 
			l.loc_descripcion	, 
			pm.prod_descripcion	, 
			pm.codigo_barras	, 
			f.fecha_comercial	, 
			f.l_prom_codigo	, 
			pl.estado
)

######################
consulta
######################
select 1 from #reco#


