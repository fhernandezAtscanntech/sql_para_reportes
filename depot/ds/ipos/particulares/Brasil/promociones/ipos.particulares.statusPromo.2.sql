multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
		select 			
			p.prom_cod_empresa	"Cod.prom.Emp", 	
			p.descripcion	"Promoci�n", 	
			c.descripcion 	"Campa�a", 	
			pv.vigencia_desde	"Vigencia Desde", 	
			pv.vigencia_hasta	"Vigencia hasta", 	
			l.emp_descripcion 	"Empresa", 	
			l.loc_modelo_descripcion 	"Local", 	
			l.loc_codigo||l.emp_codigo 	"C�digo �nico local", 
			case pl.estado		
				when 1 then '''||'PENDIENTE'||'''
				when 2 then '''||'ACEPTADA'||'''
				when 3 then '''||'RECHAZADA'||'''
				when 4 then '''||'FINALIZADA'||'''
				when 5 then '''||'A REPROCESAR'||'''
				when 6 then '''||'PENDIENTE ACEPTAR'||'''
				when 7 then '''||'A DEFINIR'||'''
			end	"Estado local"	
		from 			
			iposs.promociones p, 		
			iposs.prom_vigencias pv, 		
			iposs.campa�as c, 		
			iposs.prom_locales_habilitados pl, 		
			dwm_prod.l_ubicaciones l		
		where	p.codigo = pv.prom_codigo		
		and 	p.cpa_codigo = c.codigo		
		and 	p.codigo = pl.prom_codigo		
		and 	pl.emp_codigo = l.emp_codigo		
		and 	pl.loc_codigo = l.loc_codigo		
		and 	sysdate between pv.vigencia_desde and pv.vigencia_hasta		
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
		select 			
			p.prom_cod_empresa	"Cod.prom.Emp", 	
			p.descripcion	"Promoci�n", 	
			c.descripcion 	"Campa�a", 	
			pv.vigencia_desde	"Vigencia Desde", 	
			pv.vigencia_hasta	"Vigencia hasta", 	
			l.emp_descripcion 	"Empresa", 	
			l.loc_modelo_descripcion 	"Local", 	
			l.loc_codigo||l.emp_codigo 	"C�digo �nico local", 
			case pl.estado		
				when 1 then 'PENDIENTE'
				when 2 then 'ACEPTADA'
				when 3 then 'RECHAZADA'
				when 4 then 'FINALIZADA'
				when 5 then 'A REPROCESAR'
				when 6 then 'PENDIENTE ACEPTAR'
				when 7 then 'A DEFINIR'
			end	"Estado local"	
		from 			
			iposs.promociones p, 		
			iposs.prom_vigencias pv, 		
			iposs.campa�as c, 		
			iposs.prom_locales_habilitados pl, 		
			dwm_prod.l_ubicaciones l		
		where 1 = 2
)

######################
consulta
######################
select 1 from #reco#
