multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
		select 		
			p.prom_cod_empresa	"Cod.prom.Emp", 
			p.descripcion	"Promoción", 
			c.descripcion 	"Campaña", 
			pv.vigencia_desde	"Vigencia Desde", 
			pv.vigencia_hasta	"Vigencia hasta", 
			l.emp_descripcion 	"Empresa", 
			l.loc_modelo_descripcion 	"Local", 
			l.loc_codigo||l.emp_codigo 	"Código único local", 
			rp.codigo_barra 	"Codigo Barra", 
			rp.art_codigo_externo 	"Cod.artículo", 
			ae.descripcion 		"Artículo", 
			rp.fecha_comercial 	"Fecha", 
			sum (decode (rp.tipo_de_linea, 4, rp.cantidad, 5, -cantidad)) 	"Cantidad", 
			sum (decode (rp.tipo_de_linea, 4, rp.importe_descuento, 5, -importe_descuento)) 	"Descuento"
		from 		
			iposs.promociones p, 	
			iposs.prom_vigencias pv, 	
			iposs.campañas c, 	
			iposs.prom_locales_habilitados pl, 	
			iposs.reporte_promo_br rp, 
			dwm_prod.l_ubicaciones l, 
			iposs.articulos_empresas ae
		where	p.codigo = pv.prom_codigo	
		and 	p.cpa_codigo = c.codigo	
		and 	p.codigo = pl.prom_codigo	
		and 	pl.emp_codigo = l.emp_codigo	
		and 	pl.loc_codigo = l.loc_codigo	
		and 	rp.prom_codigo = p.codigo
		and 	rp.emp_codigo = l.emp_codigo
		and 	rp.loc_codigo = l.loc_codigo		
		and 	rp.art_codigo_externo = ae.art_codigo_externo
		and 	rp.emp_codigo = ae.emp_codigo
		and 	pl.estado = 2	
		and 	sysdate between pv.vigencia_desde and pv.vigencia_hasta	
		group by 
			p.prom_cod_empresa, 
			p.descripcion, 
			c.descripcion, 
			pv.vigencia_desde, 
			pv.vigencia_hasta, 
			l.emp_descripcion, 
			l.loc_modelo_descripcion, 
			l.loc_codigo, 
			l.emp_codigo, 
			rp.codigo_barra, 
			rp.art_codigo_externo, 
			ae.descripcion, 
			rp.fecha_comercial
		' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
		select 		
			p.prom_cod_empresa	"Cod.prom.Emp", 
			p.descripcion	"Promoción", 
			c.descripcion 	"Campaña", 
			pv.vigencia_desde	"Vigencia Desde", 
			pv.vigencia_hasta	"Vigencia hasta", 
			l.emp_descripcion 	"Empresa", 
			l.loc_modelo_descripcion 	"Local", 
			l.loc_codigo||l.emp_codigo 	"Código único local", 
			rp.codigo_barra 	"Codigo Barra", 
			rp.art_codigo_externo 	"Cod.artículo", 
			ae.descripcion 		"Artículo", 
			rp.fecha_comercial 	"Fecha", 
			sum (decode (rp.tipo_de_linea, 4, rp.cantidad, 5, -cantidad)) 	"Cantidad", 
			sum (decode (rp.tipo_de_linea, 4, rp.importe_descuento, 5, -importe_descuento)) 	"Descuento"
		from 		
			iposs.promociones p, 	
			iposs.prom_vigencias pv, 	
			iposs.campañas c, 	
			iposs.prom_locales_habilitados pl, 	
			iposs.reporte_promo_br rp, 
			dwm_prod.l_ubicaciones l, 
			iposs.articulos_empresas ae
		where	1 = 2
		group by 
			p.prom_cod_empresa, 
			p.descripcion, 
			c.descripcion, 
			pv.vigencia_desde, 
			pv.vigencia_hasta, 
			l.emp_descripcion, 
			l.loc_modelo_descripcion, 
			l.loc_codigo, 
			l.emp_codigo, 
			rp.codigo_barra, 
			rp.art_codigo_externo, 
			ae.descripcion, 
			rp.fecha_comercial
)

######################
consulta
######################
select 1 from #reco#
