multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
		select 
			c.descripcion 		"Campana", 
			p.prom_cod_empresa 	"Cod.Promoción", 
			p.descripcion 	 	"Promoción", 
			l.emp_codigo 		"Cod.Empresa", 
			l.loc_codigo 		"Cod.Local", 
			l.loc_codigo||l.emp_codigo 	"Código único local", 
			l.emp_descripcion 	"Empresa", 
			l.loc_descripcion 	"Local", 
			case pl.estado 
				when 1 then '''||'PENDIENTE'||''' 
				when 2 then '''||'APROBADO'||''' 
				when 3 then '''||'RECHAZADO'||''' 
				when 4 then '''||'FINALIZADO'||''' 
				when 5 then '''||'REPROCESAR'||''' 
				when 6 then '''||'PENDIENTE DE ACEPTAR'||''' 
				when 7 then '''||'PENDIENTE DE ARTICULOS'||''' 
				else '''||'NO IDENTIFICADO'||''' 
			end 				"Estado Local"
		from 
			iposs.promociones p, 
			iposs.campañas c, 
			iposs.prom_locales_habilitados pl, 
			iposs.prom_vigencias pv, 
			dwm_prod.l_ubicaciones l
		where	p.cpa_codigo = c.codigo
		and 	pl.prom_codigo = p.codigo
		and 	pv.prom_codigo = p.codigo
		and 	pl.emp_codigo = l.emp_codigo
		and 	pl.loc_codigo = l.loc_codigo
		and 	pl.fecha_borrado is null
		and 	c.descripcion = '''||:Campana||'''
		and not exists
			(select 1
			from 
				dwm_prod.F_VENTAS_ARTS_PROMO_DIARIO_MIN f, 
				dwm_prod.l_promociones dwp
			where  	f.l_ubi_codigo = l.codigo
			and 	f.l_prom_codigo = dwp.codigo
			and 	dwp.prom_codigo = p.codigo
			and 	f.fecha_comercial >= pv.vigencia_desde - 15 
			and 	f.fecha_comercial <= pv.vigencia_hasta)
		group by 
			c.descripcion	, 
			p.prom_cod_empresa	, 
			p.descripcion	, 
			l.emp_codigo	, 
			l.loc_codigo	, 
			l.emp_descripcion 	, 
			l.loc_descripcion	, 
			pl.estado
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
		select 
			c.descripcion 		"Campana", 
			p.prom_cod_empresa 	"Cod.Promoción", 
			p.descripcion 	 	"Promoción", 
			l.emp_codigo 		"Cod.Empresa", 
			l.loc_codigo 		"Cod.Local", 
			l.loc_codigo||l.emp_codigo 	"Código único local", 
			l.emp_descripcion 	"Empresa", 
			l.loc_descripcion 	"Local", 
			case pl.estado 
				when 1 then 'PENDIENTE' 
				when 2 then 'APROBADO' 
				when 3 then 'RECHAZADO' 
				when 4 then 'FINALIZADO' 
				when 5 then 'REPROCESAR' 
				when 6 then 'PENDIENTE DE ACEPTAR'
				when 7 then 'PENDIENTE DE ARTICULOS'
				else 'NO IDENTIFICADO'
			end 				"Estado Local"
		from 
			iposs.promociones p, 
			iposs.campañas c, 
			iposs.prom_locales_habilitados pl, 
			iposs.prom_vigencias pv, 
			dwm_prod.l_ubicaciones l
		where	1 = 2
)

######################
consulta
######################
select 1 from #reco#


