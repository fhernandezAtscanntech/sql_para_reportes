select 
	l.codigo_unico_local, 
	l.emp_codigo, 
	trunc(codigo_unico_local/100000) as codigo, 
	e.razon_social empresa, 
	l.desc_local local, 
	case when c.fecha_aceptacion is null then 'N' else 'S' end as "onContrato", 
	c.fecha_aceptacion
from 
	iposs_mp.empresas e, 
	iposs_mp.locales l 
	left outer join  iposs_mp.leg_contratos_aceptados c
		on c.loc_codigo = l.codigo_local
		and c.emp_codigo = l.emp_codigo
where 	e.id_minorista = l.emp_codigo
and 	e.tipo_empresa = 'MINORISTA'
and 	l.fecha_borrado is null
and 	(l.loc_est_codigo is null or loc_est_codigo <> 2)
