multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
		SELECT 
			l.CODIGO_UNICO_LOCAL AS "COD UNICO", 
			l.DESC_LOCAL AS "NOME LOJA", 
			p.ID_EXTERNO AS "CODIGO PROMOCAO",
			i.DESCRIPCION AS "PARCEIRO", 
			prov.RAZON_SOCIAL AS "FORNECEDOR", 
			c.DESCRIPCION AS "CAMPANHA", 
			p.TITULO AS "PROMOCAO", 
			cast (p.VIGENCIA_DESDE as date) AS "VIGENCIA_DESDE", 
			cast (p.VIGENCIA_HASTA as date) AS "VIGENCIA_HASTA", 
			pp.ESTADO AS STATUS,
			cast (pp.FECHA_ACEPTACION as date) AS "FECHA_ACEPTACION", 
			art.nombre AS "ARTIGO", 
			art.codigo_barras AS "EAN"
		FROM 
			iposs_mp.promociones p 
				INNER JOIN IPOSS_MP.PROPUESTAS_PROMOCIONES pp ON (pp.ID_PROMOCION = p.ID)
				INNER JOIN IPOSS_MP.LOCALES l ON (pp.ID_LOCAL_OFERIDO = l.ID) 
				INNER JOIN IPOSS.PROMOCIONES promo ON (promo.PROM_COD_EMPRESA = p.CODIGO_INTERNO_BE)
				INNER JOIN IPOSS_MP.EMPRESAS prov ON (p.ID_AUTOR = prov.ID)
				INNER JOIN IPOSS."CAMPAÑAS" c ON (promo.CPA_CODIGO = c.CODIGO)
				INNER JOIN IPOSS.LOCALES ll ON (l.CODIGO_LOCAL = ll.CODIGO) 
				INNER JOIN IPOSS.EMPRESAS e ON (e.CODIGO = ll.EMP_CODIGO) 
				INNER JOIN IPOSS.INTEGRADORES i ON (e.INT_CODIGO = i.CODIGO)
				inner join IPOSS_MP.BENEFICIOS_PROMOCIONES b on (b.ID_PROMOCION = p.id)
				inner join IPOSS_MP.COMBOS co on (co.ID = b.COMBO_ID)
				inner join IPOSS_MP.ITEMS_COMBOS ic on (ic.COMBO_ID = co.id)
				inner join IPOSS_MP.ITEMS_COMBOS_ARTICULOS ica on (ica.ID_ITEM = ic.id)
				inner join IPOSS_MP.ARTICULOS art on (art.id = ica.ID_ARTICULO)		  
		WHERE 	p.VIGENCIA_HASTA >= to_date('''||:Fecha||''', '''||'dd/mm/yyyy'||''')
		AND 	trunc(sysdate) - to_date('''||:Fecha||''', '''||'dd/mm/yyyy'||''') <= 62
		AND 	pp.FECHA_BORRADO is null
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
	SELECT 
		l.CODIGO_UNICO_LOCAL AS "COD UNICO", 
		l.DESC_LOCAL AS "NOME LOJA", 
		p.ID_EXTERNO AS "CODIGO PROMOCAO",
		i.DESCRIPCION AS "PARCEIRO", 
		prov.RAZON_SOCIAL AS "FORNECEDOR", 
		c.DESCRIPCION AS "CAMPANHA", 
		p.TITULO AS "PROMOCAO", 
		cast (p.VIGENCIA_DESDE as date) AS "VIGENCIA_DESDE", 
		cast (p.VIGENCIA_HASTA as date) AS "VIGENCIA_HASTA", 
		pp.ESTADO AS STATUS,
		cast (pp.FECHA_ACEPTACION as date) AS "FECHA_ACEPTACION", 
		art.nombre AS "ARTIGO", 
		art.codigo_barras AS "EAN"
	FROM 
		iposs_mp.promociones p 
		  INNER JOIN IPOSS_MP.PROPUESTAS_PROMOCIONES pp ON (pp.ID_PROMOCION = p.ID)
		  INNER JOIN IPOSS_MP.LOCALES l ON (pp.ID_LOCAL_OFERIDO = l.ID) 
		  INNER JOIN IPOSS.PROMOCIONES promo ON (promo.PROM_COD_EMPRESA = p.CODIGO_INTERNO_BE)
		  INNER JOIN IPOSS_MP.EMPRESAS prov ON (p.ID_AUTOR = prov.ID)
		  INNER JOIN IPOSS."CAMPAÑAS" c ON (promo.CPA_CODIGO = c.CODIGO)
		  INNER JOIN IPOSS.LOCALES ll ON (l.CODIGO_LOCAL = ll.CODIGO) 
		  INNER JOIN IPOSS.EMPRESAS e ON (e.CODIGO = ll.EMP_CODIGO) 
		  INNER JOIN IPOSS.INTEGRADORES i ON (e.INT_CODIGO = i.CODIGO)
				inner join IPOSS_MP.BENEFICIOS_PROMOCIONES b on (b.ID_PROMOCION = p.id)
				inner join IPOSS_MP.COMBOS co on (co.ID = b.COMBO_ID)
				inner join IPOSS_MP.ITEMS_COMBOS ic on (ic.COMBO_ID = co.id)
				inner join IPOSS_MP.ITEMS_COMBOS_ARTICULOS ica on (ica.ID_ITEM = ic.id)
				inner join IPOSS_MP.ARTICULOS art on (art.id = ica.ID_ARTICULO)		  
	WHERE 
	   1=2
)

######################
consulta
######################
select 1 from #reco#

