select				
	i.descripcion 		as Integrador, 
	l.emp_codigo 		as Cod_Empresa, 
	l.codigo 			as Cod_Local, 
	e.descripcion 		as Empresa, 
	nvl (p.nome, l.descripcion)	as Local, 
	p.cnpj				as CNPJ, 
	d.calle    as Calle,
	d.numero   as Numero,
	d.complemento   as Complemento,
	d.barrio    as Barrio,
	mu.nombre			as Municipio, 
	(select max(fecha_comercial)		
		from iposs.movimientos m
		where m.emp_codigo = l.emp_codigo
		and m.loc_codigo = l.codigo
		and m.fecha_comercial >= trunc(sysdate) - 30)	as Fecha_Ult_Mov, 
	m.fecha_comercial 	as Fecha, 
	m.cant 		as CantTickets
from 
	iposs.locales l, 
	iposs.empresas e, 
	iposs.personas_br p, 
	iposs.direcciones_br d, 
	iposs.municipios_br mu, 
	iposs.integradores i, 
	(select m.emp_codigo, m.loc_codigo, m.fecha_comercial, count(*) cant
		from iposs.movimientos m
		where m.tipo_operacion = 'VENTA'
		and m.fecha_comercial > trunc(sysdate)-16
		group by m.emp_codigo, m.loc_codigo, m.fecha_comercial) m
where 	l.emp_codigo = e.codigo 
and 	l.per_codigo_local = p.per_codigo (+) 
and 	p.dir_br_codigo = d.codigo (+) 
and 	d.mun_codigo = mu.codigo (+) 
and 	e.int_codigo = i.codigo (+)
and 	l.fecha_borrado is null 
and 	l.emp_codigo = m.emp_codigo (+)
and 	l.codigo = m.loc_codigo (+)
