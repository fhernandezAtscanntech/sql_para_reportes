SELECT 
	inc.id_razon 	"Id Razon",
	r_inc.razon 	"Razon",
	inc.descripcion 	"Inconformidad",
	inc.precio 		"Precio",
	prom.estado 	"Estado Promo",
	inc.id_promo 	"Id Promo",
	prom.titulo 	"Titulo Promo",
	prom.descripcion 	"Promocion",
	loc.id 		"Id Local",
	loc.desc_local 	"Local",
	inc.audit_date 	"Fecha"
FROM 
	iposs_mp.inconformidad_prom_min as inc
INNER JOIN iposs_mp.razones_inconf as r_inc 
	on (r_inc.id=inc.id_razon)
INNER JOIN iposs_mp.promociones as prom 
	on (prom.id=inc.id_promo)
INNER JOIN iposs_mp.locales as loc 
	on (loc.emp_codigo=inc.emp_codigo)
where 
	(inc.audit_date >= :FechaDesde) 
and (inc.audit_date < :FechaHasta)

