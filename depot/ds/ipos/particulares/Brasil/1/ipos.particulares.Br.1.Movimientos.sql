/*
orden
	nro_mov
	nro.operacion
	caja
	total
	total bonificable
	tipo operacion
	fecha operacion
	hora operación
	vo_nro_opeacion
	fecha_llego_pos
** 	nombre cliente
	moneda
	registradora
	cotiza_compra
	nombre cajera
	vendedor
	lista de precios
	cotiza_venta
	numero_tarjeta
	bonus_total
	porcentaje_descuento
	observacion
	redondeo
	nombre factura
	ruc factura
	direccion factura
	obervacion_factura
	tipo de factura
	nro operacion fiscal
	cantidad cupones
	cantidad cabezales
	sorteado
	cupones serie
	val serie
	val nro
	tipo ingreso
	tipo egreso
	audit user
	nro z
	telefono
	nro cierre
	percepcion iva
	funcionario que autoriza
	multiplica fiscal
	situacion fiscal
	nro cupon
	serie ecf

	Movimientos para un día y una empresa
*/

select 
	l.codigo 			"Cod.Local(I)", 
	l.loc_cod_empresa	"Cod.Local", 
	l.descripcion 		"Local", 
	m.emp_codigo 		"Cod.Empresa", 
	m.fecha_comercial 	"Fecha Comercial", 
	m.numero_mov 		"Numero Mov.", 
	m.numero_operacion 	"Nro.Operacion", 
	m.caj_codigo 		"Caja", 
	m.total 			"Total", 
	m.total_bonificable 	"Total Bonif", 
	m.tipo_operacion 	"Tipo Operacion", 
	m.fecha_operacion	"Fecha Operación", 
	m.hora_operacion 	"Hora Operación", 
	m.v0_numero_operacion 	"V0 nro.op.", 
	m.fecha_llego_pos 	"Fecha llego POS", 
	mo.descripcion 		"Moneda", 
	m.registradora 		"Registradora", 
	m.cotiza_compra	 	"Cotiz. Compra", 
	(select f.apellido||' ,'||f.nombre 
		from iposs.funcionarios f 
		where f.fun_cod_empresa = m.fun_codigo_cajera 
		and f.emp_codigo = m.emp_codigo) 	"Cajero", 
	(select f.apellido||' ,'||f.nombre 
		from iposs.funcionarios f 
		where f.fun_cod_empresa = m.fun_codigo
		and f.emp_codigo = m.emp_codigo) 	"Vendedor", 
	lis.descripcion 	"Lista precios", 
	m.cotiza_venta 		"Cotiz. Venta", 
	m.numero_tarjeta 	"Nro.Tarjeta", 
	m.bonus_total 		"Puntos", 
	m.porcentaje_descuento 	"Porc.Descuento", 
	m.observacion 		"Observaciones", 
	m.redondeo 			"Redondeo", 
	m.nombre_factura 	"Nombre factura", 
	m.ruc_factura 		"RUC factura", 
	m.direccion_factura 	"Dirección factura", 
	m.observacion_factura 	"Observación factura", 
	m.tipo_documento 	"Tipo Factura", 
	m.numero_operacion_fiscal 	"Nro.Op.Fiscal", 
	m.cantidad_cupones 	"Cantidad cupones", 
	m.cantidad_cabezales 	"Cant.Cabezales", 
	m.sorteado 			"Sorteado", 
	m.cup_serie 		"Cup.Serie", 
	m.val_serie 		"Val.Serie", 
	m.val_nro 			"Val.Nro", 
	(select i.descripcion 
		from iposs.tipos_de_ingresos i, iposs.tipos_de_ingresos_locales il
		where i.codigo = il.tip_ing_codigo
		and m.ing_codigo = i.tip_ing_cod_empresa
		and m.loc_codigo = il.loc_codigo) "Tipo Ingreso", 
	(select e.descripcion 
		from iposs.tipos_de_egresos e, iposs.tipos_de_egresos_locales el
		where e.codigo = el.tip_egr_codigo
		and m.egr_codigo = e.tip_egr_cod_empresa
		and m.loc_codigo = el.loc_codigo) "Tipo Egreso", 
	m.audit_user 		"Audit. User", 
	m.nro_z 			"Nro. Z", 
	m.telefono 			"Telefono", 
	m.nro_cierre 		"Nro.Cierre", 
	m.percepcion_iva 	"Percepción IVA", 
	(select f.apellido||' ,'||f.nombre 
		from iposs.funcionarios f 
		where f.fun_cod_empresa = m.fun_codigo_autoriza
		and f.emp_codigo = m.emp_codigo) 	"Func. que autoriza", 
	m.multiplica_fiscal 	"Mult. Fiscal", 
	m.situacion_fiscal 		"Sit. Fiscal", 
	m.numero_de_cupon  		"Nro. de cupón", 
	m.serie_ecf 			"Serie ECF"
from 
	iposs.movimientos m, 
	iposs.monedas mo, 
	iposs.listas_de_precios_ventas lis, 
	iposs.locales l
where 
		m.loc_codigo = l.codigo
and 	m.mon_codigo = mo.codigo
and 	m.lis_pre_ve_codigo = lis.lis_pre_ve_cod_empresa
and 	m.emp_codigo = lis.emp_codigo
and 	m.fecha_comercial = :Fecha
and 	m.emp_codigo = :Empresa
order by 
	m.loc_codigo, 
	m.caj_codigo, 
	m.numero_operacion
	