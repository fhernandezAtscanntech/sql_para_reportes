multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
		SELECT 
			l.CODIGO_UNICO_LOCAL AS "COD UNICO", 
			l.DESC_LOCAL AS "NOME LOJA", 
			p.ID_EXTERNO AS "CODIGO PROMOCAO",
			i.DESCRIPCION AS "PARCEIRO", 
			prov.RAZON_SOCIAL AS "FORNECEDOR", 
			c.DESCRIPCION AS "CAMPANHA", 
			p.TITULO AS "PROMOCAO", 
			p.VIGENCIA_DESDE, 
			p.VIGENCIA_HASTA, 
			pp.ESTADO AS STATUS,
			pp.FECHA_ACEPTACION
		FROM 
			iposs_mp.promociones p 
			  INNER JOIN IPOSS_MP.PROPUESTAS_PROMOCIONES pp ON (pp.ID_PROMOCION = p.ID)
			  INNER JOIN IPOSS_MP.LOCALES l ON (pp.ID_LOCAL_OFERIDO = l.ID) 
			  INNER JOIN IPOSS.PROMOCIONES promo ON (promo.PROM_COD_EMPRESA = p.CODIGO_INTERNO_BE)
			  INNER JOIN IPOSS_MP.EMPRESAS prov ON (p.ID_AUTOR = prov.ID)
			  INNER JOIN IPOSS."CAMPA�AS" c ON (promo.CPA_CODIGO = c.CODIGO)
			  INNER JOIN IPOSS.LOCALES ll ON (l.CODIGO_LOCAL = ll.CODIGO) 
			  INNER JOIN IPOSS.EMPRESAS e ON (e.CODIGO = ll.EMP_CODIGO) 
			  INNER JOIN IPOSS.INTEGRADORES i ON (e.INT_CODIGO = i.CODIGO)
		WHERE 	p.VIGENCIA_HASTA >= to_date('''||:Fecha||''', '''||'dd/mm/yyyy'||''')
		AND 	trunc(sysdate) - to_date('''||:Fecha||''', '''||'dd/mm/yyyy'||''') <= 62
		AND 	pp.FECHA_BORRADO is null
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
	SELECT 
		l.CODIGO_UNICO_LOCAL AS "COD UNICO", 
		l.DESC_LOCAL AS "NOME LOJA", 
		p.ID_EXTERNO AS "CODIGO PROMOCAO",
		i.DESCRIPCION AS "PARCEIRO", 
		prov.RAZON_SOCIAL AS "FORNECEDOR", 
		c.DESCRIPCION AS "CAMPANHA", 
		p.TITULO AS "PROMOCAO", 
		p.VIGENCIA_DESDE, 
		p.VIGENCIA_HASTA, 
		pp.ESTADO AS STATUS,
		pp.FECHA_ACEPTACION
	FROM 
		iposs_mp.promociones p 
		  INNER JOIN IPOSS_MP.PROPUESTAS_PROMOCIONES pp ON (pp.ID_PROMOCION = p.ID)
		  INNER JOIN IPOSS_MP.LOCALES l ON (pp.ID_LOCAL_OFERIDO = l.ID) 
		  INNER JOIN IPOSS.PROMOCIONES promo ON (promo.PROM_COD_EMPRESA = p.CODIGO_INTERNO_BE)
		  INNER JOIN IPOSS_MP.EMPRESAS prov ON (p.ID_AUTOR = prov.ID)
		  INNER JOIN IPOSS."CAMPA�AS" c ON (promo.CPA_CODIGO = c.CODIGO)
		  INNER JOIN IPOSS.LOCALES ll ON (l.CODIGO_LOCAL = ll.CODIGO) 
		  INNER JOIN IPOSS.EMPRESAS e ON (e.CODIGO = ll.EMP_CODIGO) 
		  INNER JOIN IPOSS.INTEGRADORES i ON (e.INT_CODIGO = i.CODIGO)
	WHERE 
	   1=2
)

######################
consulta
######################
select 1 from #reco#

