multiple

select * from iposcla.$$tabla$$

######################
sql
######################
create table $$sql$$ as (
	select '
		select 
			e.codigo 			"Cod.Empresa", 
			e.descripcion 		"Empresa", 
			l.codigo 			"Cod.Local", 
			l.descripcion 		"Local", 
			i.descripcion 		"Integrador", 
			c.fecha_comercial 	"Fecha Comercial", 
			c.cierre_prom_cantidad 		"Cierre_Prom.Cantidad", 
			c.cierre_prom_importe 		"Cierre_Prom.Importe", 
			c.cierre_prom_imp_dto 		"Cierre_prom_Imp.Dto",
			c.movs_prom_cantidad 		"Movs_Prom.Cantidad", 
			c.movs_prom_importe 		"Movs_Prom.Importe",  
			c.movs_prom_imp_dto 		"Movs_Prom.Imp.Dto", 
			c.cierre_movs_importe 		"Cierre_Movs.Importe", 
			c.movs_importe 				"Movs.Importe",  
			c.rechazos_cantidad 		"Rechazos"
		FROM 
			iposs.empresas e, 
			iposs.locales l, 
			iposs.integradores i
			iposs.resumenes_cierres_ventas c
		WHERE 	e.codigo = c.emp_codigo
		and 	l.codigo = c.loc_codigo
		and 	e.int_codigo = i.codigo (+)
		and 	c.fecha_comercial  <= to_date('''||:FechaHasta||''', '''||'dd/mm/yyyy'||''')
		and 	c.fecha_comercial  >= to_date('''||:FechaDesde||''', '''||'dd/mm/yyyy'||''')
	' as sql from dual
)

######################
tabla
######################

create table $$tabla$$ as (
	select 
		e.codigo 			"Cod.Empresa", 
		e.descripcion 		"Empresa", 
		l.codigo 			"Cod.Local", 
		l.descripcion 		"Local", 
		i.descripcion 		"Integrador", 
		c.fecha_comercial 	"Fecha Comercial", 
		c.cierre_prom_cantidad 		"Cierre_Prom.Cantidad", 
		c.cierre_prom_importe 		"Cierre_Prom.Importe", 
		c.cierre_prom_imp_dto 		"Cierre_prom_Imp.Dto",
		c.movs_prom_cantidad 		"Movs_Prom.Cantidad", 
		c.movs_prom_importe 		"Movs_Prom.Importe",  
		c.movs_prom_imp_dto 		"Movs_Prom.Imp.Dto", 
		c.cierre_movs_importe 		"Cierre_Movs.Importe", 
		c.movs_importe 				"Movs.Importe",  
		c.rechazos_cantidad 		"Rechazos"
	FROM 
		iposs.empresas e, 
		iposs.locales l, 
		iposs.integradores i
		iposs.resumenes_cierres_ventas c
	WHERE 1=2
)

######################
consulta
######################
select 1 from #reco#

