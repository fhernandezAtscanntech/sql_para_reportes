select 
	p.id 			"Prom. ID", 
	p.titulo 		"T�tulo", 
	p.descripcion 	"Promoci�n", 
	p.vigencia_desde 	"Vigencia Desde", 
	vigencia_hasta 	"Vigencia Hasta", 
	e.razon_social 	"Fabricante"
from 
	iposs_mp.promociones p, 
	iposs_mp.empresas e
where 	p.fecha_borrado is null
and 	p.id_autor = e.id
and 	cast (p.vigencia_hasta as date) >= :FechaDesde
and 	cast (p.vigencia_desde as date) <= :FechaHasta