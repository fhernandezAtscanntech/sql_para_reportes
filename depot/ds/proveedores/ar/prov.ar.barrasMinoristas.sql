select 
	p.emp_codigo	"Cod.Empresa", 
	(select empresa_descripcion from dw.d_punto_venta l where l.empresa_codigo  =p.emp_codigo and rownum = 1)	"Empresa", 
	p.codigo_barras	"SKU", 
	p.fecha_ult_mov	"Fecha Ult.Mov", 
	p.rubro	"Rubro", 
	p.descripcion	"Descripcion Minorista", 
	pm.descripcion 	"Descripcion Modelo"
from 
	dw.d_producto_empresa p 
	left join  dw.d_producto pm on p.codigo_barras = pm.codigo_barras 
-- where 	p.codigo_barras in :Barras
